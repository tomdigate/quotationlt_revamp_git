/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: PropertyUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: Utility class which is used to retrieve the values for the keys 
 *       specified from the property file specified i.e., 
 *       quotation.properties, quotationterms.properties or rbc.properties
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import in.com.rbc.quotation.common.constants.QuotationConstants;

import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

public class PropertyUtility {
    /**
     * Property bundle to retrieve values from quotation.properties, 
     * quotationterms.properties and rbc.properties file
     */
    public static PropertyResourceBundle rbcConfigBundle;
    public static PropertyResourceBundle quotationConfigBundle;
    public static PropertyResourceBundle quotationtermsConfigBundle;
    public static PropertyResourceBundle quotationemailsConfigBundle;

    
    static {
        try {
            rbcConfigBundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(
                                                         QuotationConstants.RBC_PROPERTYFILENAME);
            quotationConfigBundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(
                                                         QuotationConstants.QUOTATION_PROPERTYFILENAME);
            quotationtermsConfigBundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(
                                                         QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME);
            quotationemailsConfigBundle = (PropertyResourceBundle)PropertyResourceBundle.getBundle(
                                                         QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME);
            		
        } catch (Exception ex) {
            LoggerUtility.log("DEBUG", "PropertyUtility", "Static block", "Properties files not found");
        }
    }
    
    /**
     * This method returns the property value from the property file that is specified
     * @param a_msgProperty The property whose value should be found out
     * @return Returns the property value
     * @throws MissingResourceException if no object for the given key can be found
     * @throws ClassCastException if the object found for the given key is not a string
     * @throws NullPointerException if key is null
     */
    public static String getKeyValue(String a_propertyFilename, 
                                     String a_msgProperty) throws MissingResourceException, 
                                     ClassCastException, NullPointerException {
        String sValue = null;
        if (a_propertyFilename.equalsIgnoreCase(QuotationConstants.RBC_PROPERTYFILENAME)) {
            if (rbcConfigBundle != null)                
                sValue = rbcConfigBundle.getString(a_msgProperty);
        }
        else if (a_propertyFilename.equalsIgnoreCase(QuotationConstants.QUOTATION_PROPERTYFILENAME)) {
            if (quotationConfigBundle != null)               
                sValue = quotationConfigBundle.getString(a_msgProperty);
        }
        else if (a_propertyFilename.equalsIgnoreCase( QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME)) {

            if (quotationtermsConfigBundle != null)               
                sValue = quotationtermsConfigBundle.getString(a_msgProperty);
        }
        else if (a_propertyFilename.equalsIgnoreCase(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME)) {
            if (quotationemailsConfigBundle != null)               
                sValue = quotationemailsConfigBundle.getString(a_msgProperty);
            
        }
        return  CommonUtility.isNullValue(sValue)?"":sValue.trim();
    }
    
    public static String getKeyValue(String a_msgProperty){
		return quotationConfigBundle.getString(a_msgProperty);
	}

}