/**
 * *****************************************************************************
 * Project Name: Global Employee Management System
 * Document Name: ExcelServlet.java
 * Package: in.com.rbc.quotation.enquiry.action
 * Desc: Servlet implementation class for Servlet: ExcelServlet
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: September 11, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

import java.awt.Dimension;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

 public class ExcelServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
    /*HSSFPatriarch hssfPatriarch = null;
    
    String sIconSigned =  "icon_signed.gif";
    String sIconSignedLate = "icon_signedlate.gif";
    String sIconPending = "icon_pending.gif";
    String sIconOverDue = "icon_overdue.gif";
    String sIconNA = "icon_nbya.gif";
    
     (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 
	public ExcelServlet() {
		super();
	}   	
	
	 (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doGet(HttpServletRequest request, 
                         HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
	}  	
	
	 (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	protected void doPost(HttpServletRequest request, 
                          HttpServletResponse response) throws ServletException, IOException {
        
        String action = null;
        HSSFWorkbook workBook = null;
        
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=Employee Compliance Dashboard.xls"); 
        
        if ( request.getParameter("action")!=null ) {
            action = (String)request.getParameter("action");
            
            if ( action.equalsIgnoreCase("DASHBOARD") ) {
                workBook = exportDashboardToExcel(request, response);
            } else if ( action.equalsIgnoreCase("HISTORY") ) {
                
                workBook = exportEmpHistoryToExcel(request, response);
            }
            
            if ( workBook!=null ) {
                OutputStream out = response.getOutputStream();
                workBook.write(out);
                out.flush();
                out.close();
            } else {
                LoggerUtility.log("INFO", this.getClass().getName(), "doPost", "Session expired! Could not complete the requested action.");
            }
        } else {
            LoggerUtility.log("INFO", this.getClass().getName(), "doPost", "Invalid parameters. Could not complete the requested action.");
        }
	}

    *//**
     * Adds an image to the given cell.
     *
     * @param sheet The sheet to add the image to.
     * @param workBook The workbook to add the image to.
     * @param row The row index (0-based)
     * @param columnIndex The column index (0-based)
     * @throws IOException 
     *//*
    private void addThumbnail(HSSFSheet sheet, 
                              HSSFWorkbook workBook, 
                              HSSFRow row,
                              int columnIndex, 
                              String path) throws IOException {
        
        int pictureIndex;
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        
        try {
            fis = new FileInputStream(path);
            bos = new ByteArrayOutputStream();
            int c;
            
             Retrieving the image as bytes 
            while ((c = fis.read()) != -1)
                bos.write(c);
            
             Adding picture to workbook 
            pictureIndex = workBook.addPicture(bos.toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG);
            
             Adding the picture to required cell 
            HSSFClientAnchor hssfClientAnchor = new HSSFClientAnchor(0, 0, 1023, 255, 
                                                                     (short) columnIndex, row.getRowNum(), 
                                                                     (short) columnIndex, row.getRowNum());
            HSSFPicture hssfPicture = hssfPatriarch.createPicture(hssfClientAnchor, pictureIndex);
            
             Resizing the picture to its original size 
            Dimension dimension = hssfPicture.getImageDimension();
            row.setHeightInPoints((short) dimension.getHeight());
            hssfPicture.resize();
            float anchorHeight = hssfClientAnchor.getAnchorHeightInPoints(sheet);
            row.setHeightInPoints(anchorHeight);
            hssfPicture.resize();
            
        } finally {
            if (fis != null)
                fis.close();
            if (bos != null)
                bos.close();
        }
    }
    
    private HSSFWorkbook exportDashboardToExcel(HttpServletRequest request, 
                                                HttpServletResponse response) throws ServletException, IOException {
        
        ComplianceDto oComplianceDto = null;
        KeyValueVo oKeyValueVo = null;
        PolicyDto oPolicyDto = null;
        
        ArrayList policyDetailsList = null;
        ArrayList resultsList = null;
        ArrayList policyResultList = null;
        ArrayList scorePolicyList = null;
        
        String employeeTotalScore = null;
        String sIconPath = null;
        String imageFolderPath = null;
        
        int rownum = 0;
        int cellnum = 0;
        
        HSSFWorkbook workBook = null;  Creating a new workbook 
        HSSFSheet sheet = null;  Creating a new sheet 
        HSSFRow row = null;  Declaring a row object reference 
        HSSFCell cell = null;  Declaring a cell object reference 
        
        HttpSession session = request.getSession();
        
        if ( session.getAttribute("oComplianceDto")!=null ) {
            oComplianceDto = (ComplianceDto)session.getAttribute("oComplianceDto");

            imageFolderPath = getServletContext().getRealPath(".") + "\\html\\images\\";
            
            resultsList = oComplianceDto.getSearchResultsList();
            policyResultList = oComplianceDto.getPolicyResultList();
            scorePolicyList = oComplianceDto.getScorePolicyList();
            employeeTotalScore = oComplianceDto.getEmployeeScore();
            sIconPath = null;
            
            workBook = new HSSFWorkbook();
            sheet = workBook.createSheet();
            
             Declaring cell style object reference 
            HSSFCellStyle cellStyleHeader = workBook.createCellStyle();
            HSSFCellStyle cellStyleContent = workBook.createCellStyle();
            HSSFCellStyle cellStyleImage = workBook.createCellStyle();
            HSSFCellStyle cellStyleBlackLine = workBook.createCellStyle();
            
             Declaring font object reference 
            HSSFFont fontHeader = workBook.createFont();
            HSSFFont fontContent = workBook.createFont();
            
             Setting header font style 
            fontHeader.setFontHeightInPoints((short) 10);
            fontHeader.setColor((short) HSSFFont.COLOR_NORMAL);
            fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
             Setting content font style 
            fontContent.setFontHeightInPoints((short) 10);
            fontContent.setColor((short) HSSFFont.COLOR_NORMAL);
            fontContent.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            
             Setting header cell style 
            cellStyleHeader.setBorderBottom(cellStyleHeader.BORDER_MEDIUM);
            cellStyleHeader.setDataFormat(HSSFDataFormat.getBuiltinFormat("text")); // setting the cell format to text
            cellStyleHeader.setFont(fontHeader); // setting cell font style
            cellStyleHeader.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
            cellStyleHeader.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
            cellStyleHeader.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
            cellStyleHeader.setVerticalAlignment(cellStyleHeader.ALIGN_CENTER);
            cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_LEFT);
            
             Setting content cell style 
            cellStyleContent.setBorderBottom(cellStyleContent.BORDER_THIN);
            cellStyleContent.setDataFormat(HSSFDataFormat.getBuiltinFormat("text")); // setting the cell format to text
            cellStyleContent.setFont(fontContent); // setting cell font style
            
             Setting black line cell style 
            cellStyleImage.setBorderBottom(cellStyleContent.BORDER_THIN);
            cellStyleImage.setDataFormat((short)HSSFWorkbook.PICTURE_TYPE_JPEG);
            
             Setting the sheet name in Unicode 
            workBook.setSheetName(0, "Compliance Dashboard", HSSFWorkbook.ENCODING_COMPRESSED_UNICODE);
            
            
             * Starting of code to create the header row
             
            rownum = 0;
            cellnum = 0;
            // create header row
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Employee");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Position");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Location");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Country");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Supervisor/Manager");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("One Over One Manager");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Functional/Org VP");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Organization");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Legal Entity");
            cellnum++;
            
            for ( int i=0; i<policyResultList.size(); i++ ) {
                oKeyValueVo = (KeyValueVo)policyResultList.get(i);
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleHeader);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oKeyValueVo.getValue2());
                cellnum++;
            }
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Total Score (%)");
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            
             * End of code to create the header row
             
            
            
             * Starting of code to create the content rows
             
            hssfPatriarch = sheet.createDrawingPatriarch();
            HSSFClientAnchor anchor = null;
            HSSFPicture picture = null;
            
            for (rownum = (short) 1; rownum <= resultsList.size(); rownum++) {
                oComplianceDto = (ComplianceDto)resultsList.get(rownum-1);
                cellnum = 0;
                
                // create a row
                row = sheet.createRow(rownum);
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getFullName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getPositionName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getLocationName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getCountryName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getManagerName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getOooManagerName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getFunctionalManagerName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getOrganizationName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;

                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oComplianceDto.getEntityName());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;
                
                policyDetailsList = (ArrayList)oComplianceDto.getPolicyDetailsList();
                for ( int i=0; i<policyResultList.size(); i++ ) {
                    oPolicyDto = (PolicyDto)policyDetailsList.get(i);
                    
                    cell = row.createCell((short) cellnum);
                    cell.setCellStyle(cellStyleImage);
                    cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                    
                    if ( oPolicyDto.getPolicyStatus().equals("1") ) {
                        sIconPath = imageFolderPath + sIconSigned;
                    } else if ( oPolicyDto.getPolicyStatus().equals("2") ) {
                        sIconPath = imageFolderPath + sIconSignedLate;
                    } else if ( oPolicyDto.getPolicyStatus().equals("3") ) {
                        sIconPath = imageFolderPath + sIconPending;
                    } else if ( oPolicyDto.getPolicyStatus().equals("4") ) {
                        sIconPath = imageFolderPath + sIconOverDue;
                    } else if ( oPolicyDto.getPolicyStatus().equals("0") ) {
                        sIconPath = imageFolderPath + sIconNA;
                    } else {
                        sIconPath = imageFolderPath + sIconNA;
                    }
                    addThumbnail(sheet, workBook, row, cellnum, sIconPath);
                    sheet.setColumnWidth((short)cellnum, (short) ((15 * 8) / ((double) 1 / 20)));
                    
                    cellnum++;
                }
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue("0%");
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                
                row.setHeight((short) ((1.5 * 8) / ((double) 1 / 20)));
            }
            
             * End of code to create the content rows
             
            
            
             * Starting of code to create the Totals row
             
            cellnum = 0;
            row = sheet.createRow(rownum);
            rownum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Total");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;

            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(" ");
            cellnum++;
            
            for ( int i=0; i<scorePolicyList.size(); i++ ) {
                oKeyValueVo = (KeyValueVo)scorePolicyList.get(i);
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleHeader);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oKeyValueVo.getValue2());
                cellnum++;
            }
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(employeeTotalScore);
            cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            
             * End of code to create the Totals row
             
            
            
             * Starting of code to draw thick black border
             
            row = sheet.createRow(rownum);
            
            
             * Defining the third style to be the default
             * except with a thick black border at the bottom
             
            cellStyleBlackLine.setBorderBottom(cellStyleBlackLine.BORDER_THICK);
            // create 20 cells
            for (short j = (short) 0; j < cellnum; j++) {
                 Creating a blank type cell (no value) 
                cell = row.createCell(j);
                 Set it to the thick black border style 
                cell.setCellStyle(cellStyleBlackLine);
            }
            
             * End of code to draw thick black border
             
        } else {
            
        }
        
        return workBook;
    }
    
    private HSSFWorkbook exportEmpHistoryToExcel(HttpServletRequest request, 
                                                 HttpServletResponse response) throws ServletException, IOException {
        
        DashboardDto oDashboardDto = null;
        KeyValueVo oKeyValueVo = null;
        PolicyDto oPolicyDto = null;
        
        ArrayList policyTitlesList = null;
        ArrayList resultsList = null;
        ArrayList policyDetailsList = null;
        
        String employeeTotalScore = null;
        String sIconPath = null;
        String imageFolderPath = null;
        
        int rownum = 0;
        int cellnum = 0;
        
        HSSFWorkbook workBook = null;  Creating a new workbook 
        HSSFSheet sheet = null;  Creating a new sheet 
        HSSFRow row = null;  Declaring a row object reference 
        HSSFCell cell = null;  Declaring a cell object reference 
        
        HttpSession session = request.getSession();
        
        if ( session.getAttribute("ResultDashboardDto")!=null ) {
            oDashboardDto = (DashboardDto)session.getAttribute("ResultDashboardDto");

            imageFolderPath = getServletContext().getRealPath(".") + "\\html\\images\\";
            
            resultsList = oDashboardDto.getSetSearchResult(); //in.com.rbc.quotation.enquiry.dto.DashboardDto
            policyTitlesList = oDashboardDto.getPolicyList(); //in.com.rbc.quotation.common.vo.KeyValueVo
            //policyDetailsList = oDashboardDto.getPolicyDetails(); //in.com.rbc.quotation.admin.dto.PolicyDto
            sIconPath = null;
            
            workBook = new HSSFWorkbook();
            sheet = workBook.createSheet();
            
             Declaring cell style object reference 
            HSSFCellStyle cellStyleHeader = workBook.createCellStyle();
            HSSFCellStyle cellStyleSubHeader = workBook.createCellStyle();
            HSSFCellStyle cellStyleContent = workBook.createCellStyle();
            HSSFCellStyle cellStyleImage = workBook.createCellStyle();
            HSSFCellStyle cellStyleBlackLine = workBook.createCellStyle();
            
             Declaring font object reference 
            HSSFFont fontHeader = workBook.createFont();
            HSSFFont fontContent = workBook.createFont();
            
             Setting header font style 
            fontHeader.setFontHeightInPoints((short) 10);
            fontHeader.setColor((short) HSSFFont.COLOR_NORMAL);
            fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
             Setting content font style 
            fontContent.setFontHeightInPoints((short) 10);
            fontContent.setColor((short) HSSFFont.COLOR_NORMAL);
            fontContent.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            
             Setting header cell style 
            //cellStyleHeader.setBorderBottom(cellStyleHeader.BORDER_MEDIUM);
            cellStyleHeader.setDataFormat(HSSFDataFormat.getBuiltinFormat("text")); // setting the cell format to text
            cellStyleHeader.setFont(fontHeader); // setting cell font style
            cellStyleHeader.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
            cellStyleHeader.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
            cellStyleHeader.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
            cellStyleHeader.setVerticalAlignment(cellStyleHeader.ALIGN_CENTER);
            cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_LEFT);
            
             Setting sub-header cell style 
            cellStyleSubHeader.setDataFormat(HSSFDataFormat.getBuiltinFormat("text")); // setting the cell format to text
            cellStyleSubHeader.setFont(fontHeader); // setting cell font style
            cellStyleSubHeader.setVerticalAlignment(cellStyleSubHeader.ALIGN_CENTER);
            cellStyleSubHeader.setAlignment(cellStyleSubHeader.ALIGN_LEFT);
            
             Setting content cell style 
            //cellStyleContent.setBorderBottom(cellStyleContent.BORDER_THIN);
            cellStyleContent.setDataFormat(HSSFDataFormat.getBuiltinFormat("text")); // setting the cell format to text
            cellStyleContent.setFont(fontContent); // setting cell font style
            
             Setting black line cell style 
            //cellStyleImage.setBorderBottom(cellStyleContent.BORDER_THIN);
            cellStyleImage.setDataFormat((short)HSSFWorkbook.PICTURE_TYPE_JPEG);
            
             Setting the sheet name in Unicode 
            workBook.setSheetName(0, "Employee History", HSSFWorkbook.ENCODING_COMPRESSED_UNICODE);
            
            rownum = 0;
            
             Header row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Employee Information");
            //cellnum++;
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Header row - End 
            
            
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             * Start - Code to display employee information
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             
            
             Employee Information - First Row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Employee: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmployeename());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Organization: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpOrganization());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            //cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Employee Information - First Row - End 
            
             Employee Information - Second Row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Position: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpPosition());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Legal Entity: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpGRE());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            //cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Employee Information - Second Row - End 
            
             Employee Information - Third Row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Location: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpLocation());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Supervisor or Manager: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpManager());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            //cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Employee Information - Third Row - End 
            
             Employee Information - Fourth Row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Country: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpCountry());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("One Over One Manager: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpManagerovo());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            //cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Employee Information - Fourth Row - End 
            
             Employee Information - Fifth Row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cellnum++;
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleSubHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Functional/Org VP: ");
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue(oDashboardDto.getEmpOrgVP());
            cellnum++;
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleContent);
            //cellnum++;
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Employee Information - Fifth Row - End 
            
            
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             * End - Code to display employee information
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             
            
            
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             * Start - Code to display employee's scorecard
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             
            cellnum = 0;
            row = sheet.createRow(rownum);
            rownum++;
            
             Header row - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Employee Compliance Dashboard");
            //cellnum++;
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            rownum++;
             Header row - End 
            
             Employee Dashboard - Headers - Start 
            cellnum = 0;
            row = sheet.createRow(rownum);
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Year");
            cellnum++;
            
            for ( int i=0; i<policyTitlesList.size(); i++ ) {
                oKeyValueVo = (KeyValueVo)policyTitlesList.get(i);
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleHeader);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oKeyValueVo.getValue());
                
                //Region(int rowFrom, short colFrom, int rowTo, short colTo)
                sheet.addMergedRegion(new Region(rownum, (short)cellnum, rownum, (short)(cellnum+1)));
                
                cellnum++;
                cellnum++;
            }
            
            cell = row.createCell((short) cellnum);
            cell.setCellStyle(cellStyleHeader);
            cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
            cell.setCellValue("Total Score (%)");
            
            row.setHeight((short) ((2 * 8) / ((double) 1 / 20)));
            
            rownum++;
             Employee Dashboard - Headers - End 
            
            
             Starting of code to create the content rows 
            hssfPatriarch = sheet.createDrawingPatriarch();
            HSSFClientAnchor anchor = null;
            HSSFPicture picture = null;
            
            for (int k=0; k<resultsList.size(); k++) {
                oDashboardDto = (DashboardDto)resultsList.get(k);
                cellnum = 0;
                row = sheet.createRow(rownum);
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oDashboardDto.getPolicyYear());
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                cellnum++;
                
                policyDetailsList = (ArrayList)oDashboardDto.getPolicyDetails();
                for ( int i=0; i<policyDetailsList.size(); i++ ) {
                    oPolicyDto = (PolicyDto)policyDetailsList.get(i);
                    
                    cell = row.createCell((short) cellnum);
                    cell.setCellStyle(cellStyleImage);
                    cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                    
                    if ( oPolicyDto.getPolicyAckStatus().equals("1") ) {
                        sIconPath = imageFolderPath + sIconSigned;
                    } else if ( oPolicyDto.getPolicyAckStatus().equals("2") ) {
                        sIconPath = imageFolderPath + sIconSignedLate;
                    } else if ( oPolicyDto.getPolicyAckStatus().equals("3") ) {
                        sIconPath = imageFolderPath + sIconPending;
                    } else if ( oPolicyDto.getPolicyAckStatus().equals("4") ) {
                        sIconPath = imageFolderPath + sIconOverDue;
                    } else if ( oPolicyDto.getPolicyAckStatus().equals("0") ) {
                        sIconPath = imageFolderPath + sIconNA;
                    } else {
                        sIconPath = imageFolderPath + sIconNA;
                    }
                    addThumbnail(sheet, workBook, row, cellnum, sIconPath);
                    sheet.setColumnWidth((short)cellnum, (short) ((15 * 8) / ((double) 1 / 20)));
                    cellnum++;
                    
                    cell = row.createCell((short) cellnum);
                    cell.setCellStyle(cellStyleContent);
                    cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                    cell.setCellValue(oPolicyDto.getPolicyAckOn());
                    sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                    cellnum++;
                }
                
                cell = row.createCell((short) cellnum);
                cell.setCellStyle(cellStyleContent);
                cell.setEncoding(HSSFCell.ENCODING_COMPRESSED_UNICODE);
                cell.setCellValue(oDashboardDto.getEmpPolicyScore() + "%");
                sheet.setColumnWidth((short)cellnum, (short) ((50 * 8) / ((double) 1 / 20)));
                
                row.setHeight((short) ((1.5 * 8) / ((double) 1 / 20)));
                
                rownum++;
            }
             End of code to create the content rows 
            
            
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             * End - Code to display employee's scorecard
             * # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
             
            
             Starting of code to draw thick black border 
            row = sheet.createRow(rownum);
            
            
             * Defining the third style to be the default
             * except with a thick black border at the bottom
             
            cellStyleBlackLine.setBorderBottom(cellStyleBlackLine.BORDER_THICK);
            // create 20 cells
            for (short j = (short) 0; j <= cellnum; j++) {
                 Creating a blank type cell (no value) 
                cell = row.createCell(j);
                 Set it to the thick black border style 
                cell.setCellStyle(cellStyleBlackLine);
            }
             End of code to draw thick black border 
        } else {
            LoggerUtility.log("INFO", this.getClass().getName(), "exportEmpHistoryToExcel", "Session expired!");
        }
        
        return workBook;
    }*/
 }