/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: ListDBColumnUtil.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: List DB Column Utililty class which gives column name by passing column
 * index, This used for Pagination and Sorting the results in Listing component.
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

public class ListDBColumnUtil {
	
	
	 /**
     * Method to retrieve the DB column name, corresponding to the associated
     * column number specified in Lookup.jsp page.
     * This method is used in Lookup Employee Action method to sort the search
     * results based on the selected column.
     * @param sDbField Number specifying the column index for which the DB column name is required
     * @return Returns the DB column name for the specified column index
     * @author 100002865 (Shanthi Chinta)
     */
    public static String getlookupSearchResultDBField(String sDbField) {
        switch(Integer.parseInt(sDbField.trim())) {
            case 1: sDbField = "EMP_SSO";
                    break;
            case 2: sDbField = "EMP_NAMEFULL";
                    break;
            case 3: sDbField = "EMP_NAMEFIRST";
                    break;
            case 4: sDbField = "EMP_NAMELAST";
                    break;
            case 5: sDbField = "EMP_NAMEMIDDLE";
                    break;
            case 6: sDbField = "EMP_EMAIL";
                    break;
            case 7: sDbField = "EMP_TITLE";
                    break;
            case 8: sDbField = "EMP_GENDER";
                    break;
            case 9: sDbField = "EMP_ORGANIZATION";
                    break;
            case 10: sDbField = "EMP_LOCATION";
                    break;
            case 11: sDbField = "EMP_COUNTRY";
                    break;
        }
        return sDbField;       
    }
 
    /**
     * Method to retrieve the DB column name, corresponding to the associated
     * column number specified in search customer page.
     * This method is used in customer search result page to sort the search
     * results based on the selected column.
     * @param sDbField
     * @return Returns the DB column name for the specified column index
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 3, 2008
     */
    public static String getCustomerSearchResultDBField(String sDbField) {
        switch(Integer.parseInt(sDbField.trim())) {
            case 1: sDbField = "QCU_NAME";
                    break;
            case 2: sDbField = "QCU_CONTACTPERSON";
                    break;
            case 3: sDbField = "QLO_LOCATIONNAME";
                    break;
            case 4: sDbField = "QCU_EMAIL";
                    break;
            case 5: sDbField = "QCU_PHONE";
                    break;
        }
        return sDbField;       
    }
    
    /**
     * Method to retrieve the DB column name, corresponding to the associated
     * column number specified in search location page.
     * This method is used in location search result page to sort the search
     * results based on the selected column.
     * @param sDbField
     * @return Returns the DB column name for the specified column index
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 3, 2008
     */
    public static String getLocationSearchResultDBField(String sDbField) {
    	switch(Integer.parseInt(sDbField.trim())) {
	    case 1: sDbField = "QLO_LOCATIONNAME";
	    break;
		case 2: sDbField = "QRE_REGIONNAME";
		    break;
		case 3: sDbField = "QLO_ISACTIVE";
		    break;
		}
		return sDbField;
    }
    
    /**
     * 
     * @param sDbField
     * @return
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 7, 2008
     */
    public static String getSalesManagerSearchResultDBField(String sDbField) {
        switch(Integer.parseInt(sDbField.trim())) {
            case 1: sDbField = "SALESMANAGER_NAME";
                    break;
            case 2: sDbField = "QRE_REGIONNAME";
                    break;
            case 3: sDbField = "PRIMARYRSM_NAME";
                    break;
            case 4: sDbField = "ADDITIONALRSM_NAME";
                    break;
        }
        return sDbField;       
    }
    
    /**
     * 
     * @param sDbField
     * @return
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 17, 2008
     */
    public static String getDesignEngineerSearchResultDBField(String sDbField) {
        switch(Integer.parseInt(sDbField.trim())) {
            case 1: sDbField = "DESIGNENGINEER_NAME";
                    break;
        }
        return sDbField;       
    }
    
    /**
     * Method to retrieve the DB column name, corresponding to the associated
     * column number specified in searchresults.jsp page.
     * This method is used in Search Enquiry Action method to sort the search
     * results based on the selected column.
     * @param sDbField Number specifying the column index for which the DB column name is required
     * @return Returns the DB column name for the specified column index
     * @author 100002865 (Shanthi Chinta)
     */
	public static String getSearchResultDBField(String sDbField) {
		 switch(Integer.parseInt(sDbField.trim())) 
		 {
		 		case 1: sDbField = "a.QEM_ENQUIRYNO";
		 			break;
				case 2: sDbField = "a.QEM_CUSTOMERNAME";
				    break;
				case 3: sDbField = "a.QEM_LOCATIONNAME";
				    break;
				case 4: sDbField = "TO_CHAR (a.QEM_CREATEDDATE,'MM/dd/yyyy')";
				    break;
				case 5: sDbField = "a.QEM_CREATEDBYNAME";
			    	break;    
				case 6: sDbField = "a.QEM_STATUSDESC";
				    break;
				case 7: sDbField = "b.QRD_POLEDESC";
		 			break;
				case 8: sDbField = "b.QRD_KW";
				    break;
				case 9: sDbField = "b.QRD_TO_FRAMESIZE";
					break;
				case 10: sDbField = "b.QRD_VOLTS";
				    break;
				case 11: sDbField = "b.QRD_MOUNTINGDESC";
				    break;
				
		 }
		 return sDbField;       
	}

	public static Object getSearchMasterDBField(String sDbField) {
		 switch(Integer.parseInt(sDbField.trim())) 
		 {
		 		case 1: sDbField = "a.QEM_ENGMID";
		 			break;
				case 2: sDbField = "a.QEM_DESC";
				    break;
		 }
		 return sDbField;    
	}

/*	public static Object getMasterDataDBField(String sDbField) {
		switch(Integer.parseInt(sDbField.trim())) 
		 {
		 		case 1: sDbField = "QED_DESC";
		 		break;
		 		case 2: sDbField = "QED_ISACTIVE";
			    break;
		 }
	 return sDbField;   
		
	}*/
	public static Object getMasterDataDBField(String sDbField) {
		switch(Integer.parseInt(sDbField.trim())) 
		 {
		 		case 1: sDbField = "qem_value";
		 		break;
		 		case 2: sDbField = "qem_isactive";
			    break;
		 }
	 return sDbField;   
		
	}
	
	 /**
     * Method to retrieve the DB column name, corresponding to the associated
     * column number specified in search location page.
     * This method is used in technical data search result page to sort the search
     * results based on the selected column.
     * @param sDbField
     * @return Returns the DB column name for the specified column index
     * @author 900008798 (Abhilash Moola)
     * Created on: Nov 20, 2020
     */
    public static String getTechnicalDataResultDBField(String sDbField) {
    	switch(Integer.parseInt(sDbField.trim())) {
	    case 1: sDbField = "MANUFACTURING_LOCATION";
	    break;
		case 2: sDbField = "PRODUCT_GROUP";
		    break;
		case 3: sDbField = "PRODUCT_LINE";
		    break;
		}
		return sDbField;
    }
	
}
