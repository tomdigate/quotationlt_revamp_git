/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: ConverterUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: This class is responsible to copy all the values from 
 *       Struts form bean to VO or otherwise.
 * Implementation: ConverterUtility.copyProperties(objSearchVO, objSearchFormBean);
 * *****************************************************************************
 * @author 100002865(Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

public class ConverterUtility {
    /**
     * This method is to copy all the values from destination object to target object
     * @param Object destinationObject
     * @param Object OriginalObject
     * @return void
     * @throws IllegalAccessException, InvocationTargetException
     */
    public final static void copyProperties(Object destinationObject,
            Object OriginalObject) throws IllegalAccessException, 
            InvocationTargetException {
        
        BeanUtils.copyProperties(destinationObject, OriginalObject);
    }
}
