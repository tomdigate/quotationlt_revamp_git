package in.com.rbc.quotation.common.utility;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

public class CurrencyConvertUtility {
    public static String convertToIndianCurrency(String num) {
    	String finalString="";
    	if((num!="" && num!=null) && (num!="NA" && num!="0") )
    	{
        BigDecimal bd = new BigDecimal(num);
        long number = bd.longValue();
        long no = bd.longValue();
        int decimal = (int) (bd.remainder(BigDecimal.ONE).doubleValue() * 100);
        int digits_length = String.valueOf(no).length();
        int i = 0;
        ArrayList<String> str = new ArrayList<>();
        HashMap<Integer, String> words = new HashMap<>();
        words.put(0, "");
        words.put(1, "One");
        words.put(2, "Two");
        words.put(3, "Three");
        words.put(4, "Four");
        words.put(5, "Five");
        words.put(6, "Six");
        words.put(7, "Seven");
        words.put(8, "Eight");
        words.put(9, "Nine");
        words.put(10, "Ten");
        words.put(11, "Eleven");
        words.put(12, "Twelve");
        words.put(13, "Thirteen");
        words.put(14, "Fourteen");
        words.put(15, "Fifteen");
        words.put(16, "Sixteen");
        words.put(17, "Seventeen");
        words.put(18, "Eighteen");
        words.put(19, "Nineteen");
        words.put(20, "Twenty");
        words.put(30, "Thirty");
        words.put(40, "Forty");
        words.put(50, "Fifty");
        words.put(60, "Sixty");
        words.put(70, "Seventy");
        words.put(80, "Eighty");
        words.put(90, "Ninety");
        String digits[] = {"", "Hundred", "Thousand", "Lakh", "Crore"};
        while (i < digits_length) {
            int divider = (i == 2) ? 10 : 100;
            number = no % divider;
            no = no / divider;
            i += divider == 10 ? 1 : 2;
            if (number > 0) {
                int counter = str.size();
                String plural = (counter > 0 && number > 9) ? "s" : "";
                String tmp = (number < 21) ? words.get(Integer.valueOf((int) number)) + " " + digits[counter] + plural : words.get(Integer.valueOf((int) Math.floor(number / 10) * 10)) + " " + words.get(Integer.valueOf((int) (number % 10))) + " " + digits[counter] + plural;                
                str.add(tmp);
            } else {
                str.add("");
            }
        }

        Collections.reverse(str);
        
        String FinalString="";
        for(int j=0;j<str.size();j++)
        {

        	FinalString=FinalString+" "+str.get(j);
        }
      //  String Rupees = String.join(" ", str).trim();
        String Rupees=FinalString;

        String paise = (decimal) > 0 ? " And Paise " + words.get(Integer.valueOf((int) (decimal - decimal % 10))) + " " + words.get(Integer.valueOf((int) (decimal % 10))) : "";
        finalString="Rupees " + Rupees + paise + " Only";
         
    }
    	 else
    	    {
    	        finalString="Rupees " ;
	
    	    }
    	return finalString;
    }
   
    
    public static String buildPriceFormat(String value) {
		if("0".equals(value) || "0.0".equals(value)) {
			return "";
		} else {
			if(value.length() > 3) {
				String sLastThree = value.substring(value.length() - 3);
			    String sOtherNumbers = value.substring(0, value.length() - 3);
			    if(StringUtils.isNotBlank(sOtherNumbers))
			    	sLastThree = ',' + sLastThree;
			    
			    String regex = "(\\d)(?=(\\d{2})+$)";
			    return sOtherNumbers.replaceAll(regex, "$1,") + sLastThree;
			} else {
				return value;
			}
			
		}
	}
    
    public static double parsePriceString(String value) throws Exception {
    	double priceVal = 0d;
    	
    	if(StringUtils.isBlank(value) || "0".equalsIgnoreCase(value) || "0.0".equalsIgnoreCase(value)) {
    		return priceVal;
    	} 
    	else {
    		value = value.replaceAll(",", "");
    		NumberFormat numberFormat = NumberFormat.getNumberInstance();
            numberFormat.setMaximumFractionDigits(2);		
            priceVal = numberFormat.parse(value).doubleValue();
            
    		return priceVal;
    	}
    }
    
}
