/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: CommonUtil.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: Common Utility class which hold common functions that are used 
 *      across the project
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public class CommonUtility {
    
    /**
     * Checks and returns true if the value is null. Else returns false
     * @param a_param Parameter name to retrieve parameter value
     * @return Returns the parameter value
     */
    public static boolean isNullValue(String sParam) {
       
        return sParam==null?true:(sParam.length()==0?true:false);
    }
    
    /**
     * Checks and returns true if the value is null. Else returns false
     * @param a_request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value
     */
    public static boolean isNullValue(HttpServletRequest request, String sParam) {
        
        return request.getParameter(sParam) == null?true:
            (request.getParameter(sParam).trim().equals("")?true:false);
    }
    
    /**
     * Replaces a string value to null 
     * @param sParam String to check for NULL value
     * @return Returns a string value without NULL
     */
    public static String replaceNull(String sParam) {
        return (sParam == null )?"":sParam.trim();
        
        //|| isNaN(sParam.trim()) 
    }
    
    /**
     * Replaces a null or NA to empty
     * @param sParam String to check for NULL or NA value
     * @return Returns a string value without NULL
     */
    public static String replaceNullorNA(String sParam) {
        
    	if (sParam != null && sParam.trim().equalsIgnoreCase("NA"))
    		sParam = "";
        return sParam == null?"":sParam.trim();
    }
    
    /***
     * 
     * Replace a null or NAN to empty
     */
    
    public static String replaceNullorNAN(String sParam) {
        
    	if (sParam != null && sParam.trim().equals("NaN"))
    		sParam = "";
        return sParam == null?"":sParam.trim();
    }

    /**
     * Replaces a string value to null 
     * @param sParam String to check for NULL value
     * @return Returns a string value without NULL
     */
    public static StringBuffer replaceNull(StringBuffer sParam) {
        
        return sParam==null ? new StringBuffer("") : sParam;
    }
    
    /**
     * Sets the string value from request
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value in String format
     */
    public static String getStringParameter(HttpServletRequest request, 
            String sParam) {
       
        return (request.getParameter(sParam) == null || 
                request.getParameter(sParam).equals(""))?"":
                    request.getParameter(sParam).trim();
    }
    
    /**
     * Sets the string value from request to integer
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to int
     */
    public static int getIntParameter(HttpServletRequest request, 
            String sParam) {
        
        return (request.getParameter(sParam) == null || 
                request.getParameter(sParam).equals(""))?0:
                    Integer.parseInt(request.getParameter(sParam).trim());
    }
    
    /**
     * Sets the string value from request to float
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to float
     */
    public static float getFloatParameter(HttpServletRequest request, String sParam) {
       
        return (request.getParameter(sParam) == null || 
                request.getParameter(sParam).equals(""))?0:
                    Float.parseFloat(request.getParameter(sParam).trim());
    }
    
    /**
     * Sets the string value from request to double
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to double
     */
    public static double getDoubleParameter(HttpServletRequest request, 
            String sParam) {
        
        return (request.getParameter(sParam) == null || 
                request.getParameter(sParam).equals(""))?0:
                    Double.parseDouble(request.getParameter(sParam).trim());
    }
    
    /**
     * Sets the string value from request to long
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to double
     */
    public static long getLongParameter(HttpServletRequest request, String sParam) {
        
    	 return (request.getParameter(sParam) == null || 
                 request.getParameter(sParam).equals(""))?0:
            Long.parseLong(request.getParameter(sParam).trim());
    }
    
    /**
     * This method accepts the ResultSet object and column name/index, and 
     * retrieves the column value.
     * It also handles the NULL value.
     * @param rs ResultSet object to retrieve value from database
     * @param columnName column name/column index for retrieving the value 
     * @return returns the column value in string format
     * @throws SQLException in case of invalid column name, ResultSet object, etc
     */
    public static String getString(ResultSet rs, String columnName) 
                            throws SQLException {
        
        String temp = rs.getString(columnName);
        return rs.wasNull() ? " " : temp.trim();
    }
    
    /*
     * This method returns empty String is passed column is null in the database.
     * @param rs ResultSet object to retrieve value from database
     * @param columnName column name/column index for retrieving the value 
     * @return returns the empty
     */
    public static String getStringNoException(ResultSet rs, String columnName){
        try {
            String temp = rs.getString(columnName);
            return rs.wasNull() ? " " : temp.trim();
        }catch(SQLException sqlEx) {
            return " ";
        }
        catch(Exception ex){
            return " ";
        }
    }
    
    /*
     * This method returns empty String is passed column is null in the database.
     * @param rs ResultSet object to retrieve value from database
     * @param columnName column name/column index for retrieving the value 
     * @return returns the empty
     */
    public static String getStringNoException(ResultSet rs, int columnNumber){
        
        try {
            
            String temp = rs.getString(columnNumber);
            return rs.wasNull() ? " " : temp.trim();
            
        }catch(SQLException sqlEx) {
            
            return " ";
        }
        catch(Exception ex){
            
            return " ";
        }
    }
    
    /**
     * This method accepts the ResultSet object and column name/index, and 
     * retrieves the column value.
     * It also handles the NULL value.
     * @param rs ResultSet object to retrieve value from database
     * @param columnName column name/column index for retrieving the value 
     * @return returns the column value in int format
     * @throws SQLException in case of invalid column name, ResultSet object, etc
     */
    public static int getInt(ResultSet rs, String columnName) 
                            throws SQLException {
        
        int temp = rs.getInt(columnName);
        return rs.wasNull() ? 0:temp;
    }
    
    /**
     * This method accepts the ResultSet object and column name/index, and 
     * retrieves the column value.
     * It also handles the NULL value.
     * @param rs ResultSet object to retrieve value from database
     * @param columnName column name/column index for retrieving the value 
     * @return returns the column value in float format
     * @throws SQLException in case of invalid column name, ResultSet object, etc
     */
    public static float getFloat(ResultSet rs, String columnName) 
                                    throws SQLException {
        
        float temp = rs.getFloat(columnName);
        return rs.wasNull()?0:temp;
    }
    
    /**
     * Converts from <i>string</i> to <i>int</i>
     * @param sParam String to be converted to int
     * @return Returns the int value
     */
    public static int getIntValue(String sParam) {
       
        return (sParam == null || sParam.equals("")) ? 0:
            (isNaN(sParam.trim())?0:Integer.parseInt(sParam.trim()));
    }
    
    /**
     * Converts from <i>float</i> to <i>int</i>
     * @param sParam Float value to be converted to int
     * @return Returns the int value
     */
    public static int getIntValue(float sParam) {
        
        return Integer.parseInt(String.valueOf(sParam));
    }
    
    /**
     * Converts from <i>string</i> to <i>float</i>
     * @param sParam String value to be converted to float
     * @return Returns the float value
     */
    public static float getFloatValue(String sParam) {
        
        return (sParam == null || sParam.equals(""))?0:
            Float.valueOf(sParam).floatValue();
    }
    
    /**
     * Converts from <i>double</i> to <i>float</i>
     * @param sParam Double value to be converted to float
     * @return Returns the float value
     */
    public static float getFloatValue(double sParam) {
        
        return new Float(sParam).floatValue();
    }

    /**
     * Converts from <i>String</i> to <i>double</i>
     * @param sParam String value to be converted to double
     * @return Returns the double value
     */
    public static double getDoubleValue(String sParam) {
        return (sParam == null || sParam.equals("")) ? 0 : new Double(sParam).doubleValue();
    }
    
    /**
     * Converts from <i>string</i> to <i>string</i>
     * @param sParam String value to be converted to string
     * @return Returns the string value
     */
    public static String getStringValue(String sParam) {
        
        return (sParam == null || sParam.equals(""))?"":sParam.trim();
    }
    
    /**
     * Converts from <i>int</i> to <i>string</i>
     * @param sParam Int value to be converted to string
     * @return Returns the string value
     */
    public static String getStringValue(int sParam) {
        
        return sParam==0?"":String.valueOf(sParam);
    }
    
    /**
     * Sets the string value from request to long
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to double
     */
    public static long getLongValue(String sParam) {
        
    	return (sParam == null || sParam.equals(""))?0:
            Long.valueOf(sParam).longValue();
    }
    
    /**
     * Sets the string value from request to boolean
     * @param request Request to retrieve parameter value
     * @param sParam Parameter name to retrieve parameter value
     * @return Returns the parameter value type-casted to double
     */
    public static boolean getBooleanValue(String sParam) {
        
    	return (sParam == null || sParam.equals(""))? false:
            Boolean.valueOf(sParam).booleanValue();
    }
    
    /**
     * Checks if the string value is NULL or not
     * @param sParam string value to check if it is a number or not
     * @return Returns <i>true</i> if value is a number; Else returns <i>false</i>
     */
    public static boolean isNaN(String sParam){
        try {
            Double.parseDouble(sParam);
            return false;
        }catch(NumberFormatException numEx) {
            return true;
        }
        catch(Exception ex){
            return true;
        }
    }
    
    /**
     * Returns rounded value of the value specified, rounded to the decimal 
     * position specified
     * @param unroundedValue double value to round
     * @param decimalPosition int value specifiying the decimal position to round
     * @return returns the rounded value as a string
     */
    public static String round(double unroundedValue, int decimalPosition) {
        
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(decimalPosition);
        
        return replaceString(numberFormat.format(unroundedValue), ",", "");
    }
    
    /**
     * Returns rounded value of the value specified, rounded to the decimal 
     * position specified
     * @param unroundedValue float value to round
     * @param decimalPosition int value specifiying the decimal position to round
     * @return returns the rounded value as a string
     */
    public static String round(float unroundedValue, int decimalPosition) {
        
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(decimalPosition);
        
        return replaceString(numberFormat.format(unroundedValue), ",", "");
    }
    
    /**
     * This method accepts a string, delimiter and an index; splits the string 
     * using the specified delimiter
     * and returns the value available at the specified index, in the retrieved array
     * @param splitString string value to split
     * @param delimiter string delimiter to split the specified string
     * @param index int index based on which value at the specified index in the 
     * array is returned 
     * @return returns a string value available at the specified index <br>
     *         returns a blank string - if splitString is empty or if any 
     *         exception is raised
     */
    public static String safeSplit(String splitString, String delimiter, int index) {
       
        String returnValue = "";
        
        try {
            
            returnValue = isNullValue(splitString)?"":splitString.split(delimiter)[index];
        } catch (Exception ex) {
            returnValue = "";
        }
        return returnValue;
        
    }   
    /**
     * Function to replace the string
     * @author 100002235
     */
    public static String replace(String replaceString){
        return replaceString.replaceAll("#", "\n");
    }
    
    
    /**
     * This method accepts a string to be split, a delimiter to perform split, 
     * and returns the first value that is retrieved after the split.
     * 
     * @param splitString String to be split.
     * @param delimiter String delimiter to perform split.
     * @return Returns the first value that is retrieved after the split.
     */
    public static String splitString(String splitString,String delimiter) {
        return (splitString!=null && !splitString.trim().equalsIgnoreCase(delimiter))? splitString.split(delimiter)[0] : "";
    }
    
    /**
     * This method accepts a string and replace it with _ if the string is blank.
     * Else the string will not be replaced.
     * 
     * @param replaceString String to be replaced
     * @return Returns the replaced string, if it is blank. Else returns the same string.
     */
    public static String replacewith_(String replaceString) {
        
        return (replaceString == null || replaceString.trim().equals(""))?"-":replaceString;
    }
    
    /**
     * This method accepts a string and replaces it with blank string, if it is null.
     * Else returns the same string. 
     * 
     * @param replaceString String to be replaced
     * @return Returns the replaced string, if it is null. Else returns the same string.
     */
    public static String replaceNullWith_(String replaceString) {
        
        return (replaceString == null || replaceString.trim().equals(""))?"":replaceString;
    }
    
    /**
     * This method accepts a string and replaces it with 0, if it is null or blank.
     * Else returns the same string. 
     * 
     * @param replaceString String to be replaced
     * @return Returns the replaced string, if it is null. Else returns the same string.
     */
    public static String replaceNullWithZero(String replaceString) {
        return (replaceString == null || replaceString.trim().equals("")) ? 
                                                            "0" : replaceString;
    }
    
    /**
     * This method accepts three string values, searches for the second string 
     * in the first string and replaces it with the third string.
     * 
     * @param replaceString String to be searched-in.
     * @param firstString String to be replaced.
     * @param secondString String to be replaced-with.
     * @return Returns the replaced string.
     */
    public static String replaceString(String replaceString,
                                       String firstString,
                                       String secondString) {
        return (replaceString == null || replaceString.trim().equals(""))?
                replaceString:replaceString.replaceAll(firstString, secondString);
    }
    
    /**
     * This method returns the power factor for the specified number.
     * 
     * @param stringItem Number in string format, for which power factor has to be retrieved
     * @return Returns the power factor of the specified number
     */
    public static String formatPowerFactor(String stringItem) {
        String pf = "";
        if (stringItem == null || stringItem.trim().equals("" ))
            pf = "";
        else if (stringItem != null && stringItem.equalsIgnoreCase("NA") || 
                stringItem.equalsIgnoreCase("N/A") || stringItem.equalsIgnoreCase("none"))
              pf = "NA";
        else if (stringItem != null && Float.parseFloat(stringItem) > 100)
              pf = "ERROR: VALUE CANNOT BE > 100";
        else    
            pf = stringItem + "(" + Float.parseFloat(stringItem)/100 + ")";
        return pf;
    }
    
    /**
     * This method returns the deformatted power factor for the specified string.
     * 
     * @param stringItem String to be deformatted.
     * @return Returns the deformatted string.
     */
    public static String deformatPowerFactor(String stringItem)  {
        String pf = "";
        if (stringItem == null || stringItem.trim().equals("" ))
            pf = "";
        else if (stringItem != null && stringItem.equalsIgnoreCase("NA") || 
                stringItem.equalsIgnoreCase("N/A") || stringItem.equalsIgnoreCase("none"))
              pf = "NA";
        else if(stringItem.indexOf("(") != -1) {   
            pf = stringItem.substring(0,stringItem.indexOf("("));
        }
        return pf;
    }
    
    /**
     * This method accepts an email id, validates the same and returns a boolean
     * value depending on the validation.
     * 
     * @param email_address Email id to be validated.
     * @return Returns a boolean value, true/false, depending on the validation.
     * <br>Returns True - if email id is valid.
     * <br>Returns False - if email id is not valid.
     */
    public static boolean emailAddressValidator(String email_address) {
		
		boolean isValid = true;
		
		if (email_address == null) {			
			isValid = false;
		}
		
		if (email_address.length() < 6) {
			isValid = false;
		}
				
		if (isValid) {
			StringTokenizer stTokenizer = new StringTokenizer(email_address, "@");
			if (stTokenizer == null){
				isValid = false;
			}else if (stTokenizer.countTokens() == 2){
				String sUsername = stTokenizer.nextToken();
				String sHostname = stTokenizer.nextToken();
				
				//now validate the host name
				StringTokenizer stHostToken = new StringTokenizer(sHostname,".");
				if (stHostToken == null){
					isValid = false;
				}else if (stHostToken.countTokens() > 0){
					isValid = true;
				}else{
					isValid = false;
				}				
			}else{
				isValid = false;
			}			
		}
		return isValid;
    }
    
    /* (non-Javadoc)
     * @see java.lang.String#generateOptionsXML(java.util.ArrayList, java.lang.String, java.lang.String, java.lang.String)
     * This method is used to generate the options tag
     */
    public static String generateOptionsXML(ArrayList alOptionsList, String sTextValue, String sTargetObjects, 
    		String sTargetObjectName) {
    	StringBuffer sbBuiltXML = new StringBuffer("");
    	
    	try{    	
    		KeyValueVo oTargetDetails = getTargetByName(sTargetObjects, sTargetObjectName);
        
    		if (oTargetDetails.getKey() != null && oTargetDetails.getKey().trim().length() > 0){    			
    			if (oTargetDetails.getValue().equals(QuotationConstants.QUOTATION_AJAXLOOKUP_TARGETTYPE_COMBO)) {
    				if (alOptionsList.size() > 0){
    					for(int i = 0; i < alOptionsList.size(); i++) {	            		
    						KeyValueVo oKeyValueVo = (KeyValueVo)alOptionsList.get(i);
    						sbBuiltXML.append("<options-"+oTargetDetails.getKey()+"><opname>"
    				        + URLEncoder.encode(oKeyValueVo.getValue()+"&&"+oKeyValueVo.getKey(), "UTF-8")
        		            + "</opname></options-"+oTargetDetails.getKey()+">\r\n");
    					}
    					
    					sbBuiltXML.append("<params>");
    		    		sbBuiltXML.append("<target>"+oTargetDetails.getKey()+"</target>");
    		    		sbBuiltXML.append("<objType>"+oTargetDetails.getValue()+"</objType>");
    		    		sbBuiltXML.append("</params>");
    				}    		
    			} else if (oTargetDetails.getValue().equals(QuotationConstants.QUOTATION_AJAXLOOKUP_TARGETTYPE_TEXT)) {
    				
    				if (sTextValue != null && sTextValue.trim().length() > 0){
    					sbBuiltXML.append("<options-"+oTargetDetails.getKey()+"><opname>"
    							+ sTextValue + "</opname></options-"+oTargetDetails.getKey()+">\r\n");
    					
    					sbBuiltXML.append("<params>");
    		    		sbBuiltXML.append("<target>"+oTargetDetails.getKey()+"</target>");
    		    		sbBuiltXML.append("<objType>"+oTargetDetails.getValue()+"</objType>");
    		    		sbBuiltXML.append("</params>");
    				}
    			}
    		}   	
    		
    	}catch(Exception e){};
		
    	return sbBuiltXML.toString();
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.common.vo.KeyValueVo#getTargetByName(java.lang.String, java.lang.String)
     * This method will look out if targetName name exists in targetObjects string
     */
    public static KeyValueVo getTargetByName(String sTargetObjects, String sLookUpTargetName){
    	KeyValueVo oKeyValueVo = new KeyValueVo();
		if (sTargetObjects != null && sTargetObjects.trim().length() > 0){
			// Tokenize the target objects by ","
			StringTokenizer stToken = new StringTokenizer(sTargetObjects, ",");
			if (stToken != null){				
				while (stToken.hasMoreTokens()){
					String sTokenValue = stToken.nextToken();
					// check if targetType has been specified or not
					if (sTokenValue.indexOf(":") != -1){
						//Tokenizing the token value with ":" - to get targetName and targetType 					
						StringTokenizer stTypeComboToken = new StringTokenizer(sTokenValue,":");						
						if (stTypeComboToken != null && stTypeComboToken.countTokens() > 0){			
							if (stTypeComboToken.hasMoreTokens()){
								String sTokenValue2 = stTypeComboToken.nextToken();
								// check if it matches the lookup target name value
								if (sTokenValue2.toUpperCase().indexOf(sLookUpTargetName.toUpperCase()) != -1){
									oKeyValueVo.setKey(sTokenValue2);
									if (stTypeComboToken.hasMoreTokens()){
										oKeyValueVo.setValue(stTypeComboToken.nextToken());
									}
								}
							}
						}
					}else{						
						if (sTokenValue.toUpperCase().indexOf(sLookUpTargetName.toUpperCase()) != -1){
							oKeyValueVo.setKey(sTokenValue);
							oKeyValueVo.setValue(QuotationConstants.QUOTATION_AJAXLOOKUP_TARGETTYPE_DEFAULT);							
						}						
					}
				}
			}
		}
		return oKeyValueVo;
	}
    
    
    /* (non-Javadoc)
     * @see java.lang.String#fomatPrice(java.lang.String, java.lang.String)
     * This method will convert the price to the give format
     * If format is - #,##,###.# and the value is - 91938.2111 Then this method will return 91,938.2
     */
    public static String formatPrice(String sPattern, double dValue){
    	String sFormattedValue = "";
    	try{
    		if ( (sPattern != null && sPattern.trim().length()> 0) &&
    				(dValue > 0)){
    			DecimalFormat oDecimalFormat = new DecimalFormat(sPattern);
    			sFormattedValue = oDecimalFormat.format(dValue);
    			
    			if (sFormattedValue != null){    				
    				if (sFormattedValue.trim().indexOf(".") == -1 || 
    						sFormattedValue.trim().indexOf(".") < 1){
    					sFormattedValue = sFormattedValue + ".00";  
    				}
    			
    				sFormattedValue = sFormattedValue +" INR";
    			}
    		}
    	}catch(Exception e){
    		sFormattedValue = "";
    	}    	
    	return sFormattedValue;
    }
    
    public static String formatPrice(double dValue) {
        //NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT);
        numberFormat.setCurrency(Currency.getInstance(QuotationConstants.QUOTATION_PRICE_CURRENCYFORMAT));
        return numberFormat.format(dValue);
    }
    
    /**
     * This method accepts three string values, searches for the second string 
     * in the first string and replaces it with the third string.
     * 
     * @param replaceString String to be searched-in.
     * @param firstString String to be replaced.
     * @param secondString String to be replaced-with.
     * @return Returns the replaced string.
     */
    public static String replaceString1(String replaceString,
                                       String firstString,
                                       String secondString) {
        return (replaceString == null || replaceString.trim().equals(""))?
                replaceString:replaceString.replace(firstString, secondString);
    }
    
    public static String fetchBaseFrameType(String value) {
		StringBuffer baseValue = new StringBuffer();
		
		char[] chArr = value.toCharArray();
		
		for(int i=0; i<chArr.length; i++) {
			if(Character.isDigit(chArr[i])) {
				baseValue.append(chArr[i]);
			} else {
				break;
			}
		}
		return baseValue.toString();
	}
    
    public static String buildDateString(String dateVal) {
    	String formattedDateVal = "";
    	
    	try {
    		SimpleDateFormat dbFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date = dbFormatter.parse(dateVal);
        	
        	formattedDateVal = new SimpleDateFormat("dd-MM-yyyy").format(date);
    	} catch(Exception e) {
    		LoggerUtility.log("DEBUG", "CommonUtility", "buildDateString", ExceptionUtility.getStackTraceAsString(e));
    	}
    	
    	return formattedDateVal;
    }
    
    public static boolean checkIfRowHasEmptyValues(HSSFRow row, int lastCellNum) {
    	
    	if (row == null) {
            return true;
        }
    	
    	if(row.getFirstCellNum() != 0 && row.getFirstCellNum() > lastCellNum) {
    		return true;
    	}
    	
    	boolean isRowWithBlanks = true;
    	for (int cellNum = 0; cellNum < lastCellNum; cellNum++) {
    		HSSFCell cell = row.getCell(cellNum);
    		if (cell != null && cell.getCellType() != HSSFCell.CELL_TYPE_BLANK && StringUtils.isNotBlank(cell.toString())) {
    			isRowWithBlanks = false;
    			break;
    		}
    	}
    	
    	if(isRowWithBlanks) {
    		return true;
    	}
    	
    	return false;
    }
    
    public static int compareDecimalValues(String sValue1, String sValue2) {
    	int iCompareResult = 0;
    	
    	BigDecimal bdValue1 = new BigDecimal(sValue1);
    	BigDecimal bdValue2 = new BigDecimal(sValue2);
    	
    	iCompareResult = bdValue1.compareTo(bdValue2);
    	
    	return iCompareResult;
    }
    
    public static boolean compareStrings(String sValue1, String sValue2) {
    	if(StringUtils.isBlank(sValue1) || StringUtils.isBlank(sValue2)) {
    		return false;
    	}
    	if(sValue1.equalsIgnoreCase(sValue2)) {
    		return false;
    	}
    	return false;
    }
    
    
}
