/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: Security.java
 * Package: in.com.rbc.quotation.common.taglib
 * Desc: This class is used while displaying roles, in JSP pages
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.taglib;



import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class Security extends BodyTagSupport {
    
    private int id = 0;
    private String multipleids = "";
    
    public void setId(int id){
        this.id = id;
    }
    
    public void setMultipleids(String multipleids){
        this.multipleids = multipleids.trim();
    }
    
    /*
     *  (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     * 
     */
    public int doStartTag() throws JspException{
    	boolean isEvaluate = false;
        
        try{        
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            if( id > 0 ){
                if(id > 0 && BaseAction.isMemberOfGroup(request, ""+id) ){
                	isEvaluate = true;
                }
            }else if(multipleids.length() > 0 ){
            	 /*
                 * In GEMS we have to restrict System Admin from viewing some links, like - View My Employees link
                 * but the below statement would allow the admin to view everything.
                 * for this reason the below the statement is commented 
                 */
                StringTokenizer stMultipleRoleIds = new StringTokenizer(multipleids,"," );
            
                while( stMultipleRoleIds.hasMoreTokens()){
                    
                    String sToken = stMultipleRoleIds.nextToken() ;
                    
                    if( BaseAction.isMemberOfGroup(request, sToken) ){
                    	isEvaluate = true;   
                        break;
                    }
                                        
                   
                }
            }
              
        }catch(Exception e){
        	
            LoggerUtility.log("DEBUG", "Security", "doStartTag", ExceptionUtility.getStackTraceAsString(e));

        }
        
        if (isEvaluate){
        	return EVAL_BODY_INCLUDE;
        }else{
        	return SKIP_BODY;
        }
    }
    
    public int doEndTag() throws JspException {
        // Evaluate the remainder of this page
    	return (EVAL_PAGE);
    }

}
