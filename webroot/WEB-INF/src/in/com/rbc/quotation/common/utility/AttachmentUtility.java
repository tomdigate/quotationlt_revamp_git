package in.com.rbc.quotation.common.utility;

import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.dto.AttachmentDto;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;

public class AttachmentUtility {	
	public static final int applicationId = CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID));		
	public static final int iDmApplicationId = CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID));
	public static final int iIntendApplicationId = CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_INTENDATTACHMENT_APPLICATIONID));
	/* (non-Javadoc)
     * @see java.util.ArrayList#getAttachmentDetails(in.com.rbc.quotation.enquiry.dto.EnquiryDto)  
     */
	public static ArrayList getAttachmentDetails(EnquiryDto oEnquiryDto) throws Exception{	 
	   oEnquiryDto.setEnquiryId(oEnquiryDto.getEnquiryId());
	   ArrayList arAttachmentList  = new ArrayList();
	   if (oEnquiryDto.getEnquiryId() > 0){			    
		    URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
            Service service = new Service();            
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "getAttachmentList"));
			call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
			call.addParameter("uniqueId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
			call.setReturnType( org.apache.axis.Constants.SOAP_ARRAY, String[].class );
			
			Object obj = call.invoke( new Object[] {applicationId,oEnquiryDto.getEnquiryId()});
							
			String[] sAttachmentDetails = (String[])obj;
			
			if (sAttachmentDetails!=null &&  sAttachmentDetails.length > 0){
				KeyValueVo oKeyValueVo = null;
				for (int i=0; i<sAttachmentDetails.length; i++){
					oKeyValueVo = new KeyValueVo();
					String sDetails = sAttachmentDetails[i];					
					
					if (sDetails != null && sDetails.trim().length() > 0){
						StringTokenizer stToken = new StringTokenizer(sDetails, "!@");
						if (stToken != null && stToken.countTokens() > 0){
							oKeyValueVo.setKey(stToken.nextToken());
							if (stToken.hasMoreTokens()){
								String sFileName = stToken.nextToken();
								StringTokenizer stToken1 = new StringTokenizer(sFileName, ".");
								if (stToken1 != null && stToken1.countTokens() > 0)
									sFileName = stToken1.nextToken();
								
								oKeyValueVo.setValue(sFileName);
							}
							if (stToken.hasMoreTokens())
								oKeyValueVo.setValue2(stToken.nextToken());
							arAttachmentList.add(oKeyValueVo);
						}
						
					}
					
				}
			}
	   }
	   return arAttachmentList;
   }
	
	/**
	 * To fetch DM uploaded attachments by calling attachment service 
	 * @param oEnquiryDto EnquiryDto object with enquiry details		
	 * @return ArrayList Object that contains list of files attached
	 * @throws Exception If any error occurs during the process
	 * @author 900010540(Gangadhar)
	 * created on Feb 05, 2019
	 */
	public static ArrayList getDMUploadedAttachmentDetails(EnquiryDto oEnquiryDto) throws Exception {

		ArrayList alAttachmentList = new ArrayList();
		String sDetails = null;
		KeyValueVo oKeyValueVo = null;
		StringTokenizer oStoken = null;
		String sFileName = null;
		StringTokenizer oStoken1 = null;
		String[] saAttachmentDetails = null;
		Object oObject = null;
		Call oCall = null;
		URL oUrl = null;
		
		if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
			oUrl = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
			Service service = new Service();
			oCall = (Call) service.createCall();
			oCall.setTargetEndpointAddress(oUrl);
			oCall.setOperationName(new QName("AttachmentService", "getAttachmentList"));
			oCall.addParameter("applicationId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			oCall.addParameter("uniqueId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			oCall.setReturnType(org.apache.axis.Constants.SOAP_ARRAY, String[].class);

			oObject = oCall.invoke(new Object[] { iDmApplicationId, oEnquiryDto.getEnquiryId() });

			saAttachmentDetails = (String[]) oObject;

			if (saAttachmentDetails != null && saAttachmentDetails.length > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				oKeyValueVo = null;
				for (int idx = 0; idx < saAttachmentDetails.length; idx++) {
					oKeyValueVo = new KeyValueVo();
					sDetails = saAttachmentDetails[idx];

					if (sDetails != null && sDetails.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
						oStoken = new StringTokenizer(sDetails, "!@");
						if (oStoken != null && oStoken.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
							oKeyValueVo.setKey(oStoken.nextToken());
							
							if (oStoken.hasMoreTokens()) {
								sFileName = oStoken.nextToken();
								
								oStoken1 = new StringTokenizer(sFileName, ".");
								
								if (oStoken1 != null && oStoken1.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO)
									sFileName = oStoken1.nextToken();

								oKeyValueVo.setValue(sFileName);
							}
							
							if (oStoken.hasMoreTokens())
								oKeyValueVo.setValue2(oStoken.nextToken());

							alAttachmentList.add(oKeyValueVo);
						}
					}

				}
			}
		}

		return alAttachmentList;
	}

	/**
	 * To fetch New Enquiry - DM uploaded attachments 
	 * @param 	oEnquiryDto EnquiryDto object with enquiry details		
	 * @return 	ArrayList Object that contains list of files attached
	 * @throws 	Exception If any error occurs during the process
	 */
	public static ArrayList getNewEnqDMUploadedAttachmentDetails(NewEnquiryDto oNewEnquiryDto) throws Exception {

		ArrayList alAttachmentList = new ArrayList();
		String sDetails = null;
		KeyValueVo oKeyValueVo = null;
		StringTokenizer oStoken = null;
		String sFileName = null;
		StringTokenizer oStoken1 = null;
		String[] saAttachmentDetails = null;
		Object oObject = null;
		Call oCall = null;
		URL oUrl = null;
		
		if (oNewEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
			oUrl = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
			Service service = new Service();
			oCall = (Call) service.createCall();
			oCall.setTargetEndpointAddress(oUrl);
			oCall.setOperationName(new QName("AttachmentService", "getAttachmentList"));
			oCall.addParameter("applicationId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			oCall.addParameter("uniqueId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			oCall.setReturnType(org.apache.axis.Constants.SOAP_ARRAY, String[].class);

			oObject = oCall.invoke(new Object[] { iDmApplicationId, oNewEnquiryDto.getEnquiryId() });

			saAttachmentDetails = (String[]) oObject;

			if (saAttachmentDetails != null && saAttachmentDetails.length > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				oKeyValueVo = null;
				for (int idx = 0; idx < saAttachmentDetails.length; idx++) {
					oKeyValueVo = new KeyValueVo();
					sDetails = saAttachmentDetails[idx];

					if (sDetails != null && sDetails.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
						oStoken = new StringTokenizer(sDetails, "!@");
						if (oStoken != null && oStoken.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
							oKeyValueVo.setKey(oStoken.nextToken());
							
							if (oStoken.hasMoreTokens()) {
								sFileName = oStoken.nextToken();
								
								oStoken1 = new StringTokenizer(sFileName, ".");
								
								if (oStoken1 != null && oStoken1.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO)
									sFileName = oStoken1.nextToken();

								oKeyValueVo.setValue(sFileName);
							}
							
							if (oStoken.hasMoreTokens())
								oKeyValueVo.setValue2(oStoken.nextToken());

							alAttachmentList.add(oKeyValueVo);
						}
					}

				}
			}
		}

		return alAttachmentList;
	}
	
	public static ArrayList getAttachmentDetails(String sUniqueIds) throws Exception{	 

		   ArrayList arAttachmentList  = new ArrayList();		  
		   if (sUniqueIds != null && sUniqueIds.trim().length() > 0){			    
			    URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	            Service service = new Service();            
				Call call = (Call) service.createCall();
				call.setTargetEndpointAddress(url);
				call.setOperationName(new QName("AttachmentService", "getAttachmentList"));
				
				call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
				call.addParameter("uniqueId",org.apache.axis.encoding.XMLType.XSD_STRING,ParameterMode.IN);
				
				call.setReturnType( org.apache.axis.Constants.SOAP_ARRAY, String[].class );
								
					
				Object obj = call.invoke( new Object[] {applicationId, sUniqueIds});
								
				String[] sAttachmentDetails = (String[])obj;
				if (sAttachmentDetails!=null &&  sAttachmentDetails.length > 0){
					KeyValueVo oKeyValueVo = null;
					for (int i=0; i<sAttachmentDetails.length; i++){
						oKeyValueVo = new KeyValueVo();
						String sDetails = sAttachmentDetails[i];					
						if (sDetails != null && sDetails.trim().length() > 0){
							StringTokenizer stToken = new StringTokenizer(sDetails, "!@");
							if (stToken != null && stToken.countTokens() > 0){
								oKeyValueVo.setKey(stToken.nextToken());
								if (stToken.hasMoreTokens()){
									String sFileName = stToken.nextToken();
									StringTokenizer stToken1 = new StringTokenizer(sFileName, ".");
									if (stToken1 != null && stToken1.countTokens() > 0)
										sFileName = stToken1.nextToken();
									
									oKeyValueVo.setValue(sFileName);
								}
								if (stToken.hasMoreTokens())
									oKeyValueVo.setValue2(stToken.nextToken());
								arAttachmentList.add(oKeyValueVo);
							}
							
						}
						
					}
				}
		   }
		   return arAttachmentList;
		   
	   }
	
	
		public static ArrayList getAttachmentsData(String sUniqueIds) throws Exception{	 
		  // oEnquiryDto.setEnquiryId(oEnquiryDto.getEnquiryId());
		   ArrayList arAttachmentList  = new ArrayList();		  
		   if (sUniqueIds != null && sUniqueIds.trim().length() > 0){			    
			    URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	            Service service = new Service();            
				Call call = (Call) service.createCall();
				call.setTargetEndpointAddress(url);
				call.setOperationName(new QName("AttachmentService", "getAttachmentsData"));
				
				call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
				call.addParameter("uniqueId",org.apache.axis.encoding.XMLType.XSD_STRING,ParameterMode.IN);
				
				QName qn = new QName("urn:http://localhost:8080/attachments", "AttachmentDto");
				QName bean_qn = new QName("myNS:AttachmentDto", "AttachmentDto");
				
				call.registerTypeMapping(AttachmentDto.class, qn,
						new org.apache.axis.encoding.ser.BeanSerializerFactory(AttachmentDto.class, qn),        
						new org.apache.axis.encoding.ser.BeanDeserializerFactory(AttachmentDto.class, qn));        
				call.setReturnType(bean_qn,AttachmentDto[].class);
				//call.setReturnType( org.apache.axis.Constants.SOAP_ARRAY, String[].class );
								
					
				AttachmentDto[] oAttachmentDto = (AttachmentDto[]) call.invoke( new Object[] {applicationId, sUniqueIds});
								
				
				if (oAttachmentDto != null){
					for (int i=0; i<oAttachmentDto.length; i++){
						arAttachmentList.add(oAttachmentDto[i]);
					}
				}
		   }
		   return arAttachmentList;
		   
	   }
	/* (non-Javadoc)
     * @see java.lang.String#validateAttachment(int, org.apache.struts.upload.FormFile)  
     */
	public static String validateAttachment(int iApplicationUniqueId, FormFile oImageFile) throws Exception{
		String sSuccessMessage = "";
		String sFileType = "";
		if (oImageFile != null){
			// now start the process.
			URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	        Service service = new Service();            
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "getApplicationData"));
			
			QName qn = new QName("urn:http://localhost:8080/attachments", "AttachmentDto");
			QName bean_qn = new QName("myNS:AttachmentDto", "AttachmentDto");
		
			call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
			call.addParameter("uniqueid",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
			
			call.registerTypeMapping(AttachmentDto.class, qn,
					new org.apache.axis.encoding.ser.BeanSerializerFactory(AttachmentDto.class, qn),        
					new org.apache.axis.encoding.ser.BeanDeserializerFactory(AttachmentDto.class, qn));        
			call.setReturnType(bean_qn,AttachmentDto.class);
			
			AttachmentDto oAttachmentDto = (AttachmentDto) call.invoke( new Object[] {applicationId, iApplicationUniqueId});
			
			if (oAttachmentDto != null){
				if (oAttachmentDto.getAttachPath().trim().length() < 1){
					sSuccessMessage = "Upload Failed - Application data could not be retrieved";
				}else{
					// now start the file validation					
					String sFileTypes[] = oImageFile.getFileName().split("\\.");
					if (sFileTypes.length == 2){
						sFileType = sFileTypes[1]; 
					}
					
					//1. see if it exceeds max size
					if (oImageFile.getFileSize() > oAttachmentDto.getMaxFileSize()*1024){//						
						sSuccessMessage = "Upload failed - File size has exceed the max limit: "+oAttachmentDto.getMaxFileSize()+" KB";
						return sSuccessMessage;
					}
					
					
					
					StringTokenizer stToken = new StringTokenizer(oAttachmentDto.getFileTypes(), ",");
					
					boolean isValid = false;
					if (stToken != null && stToken.hasMoreTokens()){
						while (stToken.hasMoreTokens()){
							String sTokenVal = stToken.nextToken();
							if (sTokenVal.equalsIgnoreCase(sFileType))
								isValid = true;	
						}						
						if (! isValid){
							sSuccessMessage = "Upload Failed - Invalid file type.";
							return sSuccessMessage;
						}
					}
					
					//3. check for maximum limit
					if (iApplicationUniqueId > 0){						
						if (oAttachmentDto.getAttachmentCount() == oAttachmentDto.getMaxAttachments()){
							sSuccessMessage = "Upload Failed - For each enquiry only "+oAttachmentDto.getMaxAttachments()+" attachments can be uploaded";
							return sSuccessMessage;
						}
					}
					
					sSuccessMessage = "valid";
				}
			}
		
		}	

		return sSuccessMessage;
	}
	
	/* (non-Javadoc)
     * @see java.lang.String#validateAttachment(int, org.apache.struts.upload.FormFile)  
     */
	public static AttachmentDto getApplicationData() throws Exception{
		
		// now start the process.
		URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
        Service service = new Service();            
		Call call = (Call) service.createCall();
		call.setTargetEndpointAddress(url);
		call.setOperationName(new QName("AttachmentService", "getApplicationData"));
		
		QName qn = new QName("urn:http://localhost:8080/attachments", "AttachmentDto");
		QName bean_qn = new QName("myNS:AttachmentDto", "AttachmentDto");
	
		call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
		
		call.registerTypeMapping(AttachmentDto.class, qn,
				new org.apache.axis.encoding.ser.BeanSerializerFactory(AttachmentDto.class, qn),        
				new org.apache.axis.encoding.ser.BeanDeserializerFactory(AttachmentDto.class, qn));        
		call.setReturnType(bean_qn,AttachmentDto.class);
		
		AttachmentDto oAttachmentDto = (AttachmentDto) call.invoke( new Object[] {applicationId});	
		return oAttachmentDto;
	}
	
	/* (non-Javadoc)
     * @see boolean#uploadAttachment(in.com.rbc.quotation.common.dto.AttachmentDto)  
     */
	public static boolean uploadAttachment(AttachmentDto oAttachmentDto) throws Exception{
		boolean isUploaded = false;
		String sFileType = "";
		int iApplicationUniqueId = Integer.parseInt(oAttachmentDto.getApplicationUniqueId());
		if (iApplicationUniqueId > 0){
			// now start the process.
			URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	        Service service = new Service();            
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "uploadAttachment"));
			
			QName qn_dto = new QName("urn:http://localhost:8080/attachments", "AttachmentDto");
			QName bean_qn_dto = new QName("myNS:AttachmentDto", "AttachmentDto");
			
			call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);			
		 
			
			call.registerTypeMapping(AttachmentDto.class, qn_dto,
					new org.apache.axis.encoding.ser.BeanSerializerFactory(AttachmentDto.class, qn_dto),        
					new org.apache.axis.encoding.ser.BeanDeserializerFactory(AttachmentDto.class, qn_dto));   
			
			
			call.addParameter("AttachmentDto", bean_qn_dto, AttachmentDto.class, ParameterMode.IN);
			
			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_BOOLEAN);		
			
			Object ret = call.invoke(new Object[]{
						applicationId,
						oAttachmentDto}
						);
			Boolean success = (Boolean)ret;			
			isUploaded = success.booleanValue();

		}		
		return isUploaded;
	}
	
	/* (non-Javadoc)
     * @see boolean#uploadAttachment(in.com.rbc.quotation.common.dto.AttachmentDto)  
     */
	public static boolean deleteAttachment(String sUniqueId) throws Exception{
		boolean isDeleted = false;		
		if (applicationId > 0 && sUniqueId != null){
			// now start the process.
			URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	        Service service = new Service();            
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "deleteAttachments"));
			
			call.addParameter("applicationId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
			call.addParameter("uniqueId",org.apache.axis.encoding.XMLType.XSD_STRING,ParameterMode.IN);	
			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_BOOLEAN);		
			
			Object ret = call.invoke(new Object[]{
						applicationId,
						sUniqueId}
						);
			Boolean success = (Boolean)ret;			
			isDeleted = success.booleanValue();

		}		
		return isDeleted;
	}
	
	
	/* (non-Javadoc)
     * @see java.lang.String#getUserRoles(java.lang.String)  
     */
	 public static String getUserRoles(String sUserSSOId) throws Exception{
		 
        String sUserRoles = "";   

        if (sUserSSOId.trim().length() > 0) {
            // now start the process.
        	URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
            Service service = new Service();
            
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(url);
            call.setOperationName(new QName("AttachmentService", "getUserRoles"));            
            call.addParameter("userId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
            call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);     
            
            sUserRoles = (String) call.invoke( new Object[] {sUserSSOId});
            if (sUserRoles == null)
                sUserRoles = "";
        } 

        return sUserRoles;
    }
	 
	 /**
		 * To fetch Indent Page uploaded attachments by calling attachment service 
		 * @param oEnquiryDto EnquiryDto object with enquiry details		
		 * @return ArrayList Object that contains list of files attached
		 * @throws Exception If any error occurs during the process
		 * @author 900010540(Gangadhar)
		 * created on Feb 27, 2019
		 */
		
	public static ArrayList getIntendUploadedAttachmentDetails(EnquiryDto oEnquiryDto) throws Exception {
		oEnquiryDto.setEnquiryId(oEnquiryDto.getEnquiryId());
		ArrayList alAttachmentList = new ArrayList();
		String sDetails = null;
		KeyValueVo oKeyValueVo = null;
		StringTokenizer oStoken = null;
		String sFileName = null;
		StringTokenizer oStoken1 = null;
		String[] saAttachmentDetails = null;
		Object oObject = null;
		Call oCall = null;
		URL oURL = null;
		if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
			oURL = new URL(CommonUtility.getStringValue(
					PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
			Service service = new Service();
			oCall = (Call) service.createCall();
			oCall.setTargetEndpointAddress(oURL);
			oCall.setOperationName(new QName("AttachmentService", "getAttachmentList"));

			oCall.addParameter("applicationId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			oCall.addParameter("uniqueId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);

			oCall.setReturnType(org.apache.axis.Constants.SOAP_ARRAY, String[].class);

			oObject = oCall.invoke(new Object[] { iIntendApplicationId, oEnquiryDto.getEnquiryId() });

			saAttachmentDetails = (String[]) oObject;

			if (saAttachmentDetails != null && saAttachmentDetails.length > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				oKeyValueVo = null;
				for (int idx = 0; idx < saAttachmentDetails.length; idx++) {
					oKeyValueVo = new KeyValueVo();
					sDetails = saAttachmentDetails[idx];

					if (sDetails != null && sDetails.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
						oStoken = new StringTokenizer(sDetails, "!@");
						if (oStoken != null && oStoken.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
							oKeyValueVo.setKey(oStoken.nextToken());
							if (oStoken.hasMoreTokens()) {
								sFileName = oStoken.nextToken();
								oStoken1 = new StringTokenizer(sFileName, ".");
								if (oStoken1 != null
										&& oStoken1.countTokens() > QuotationConstants.QUOTATION_LITERAL_ZERO)
									sFileName = oStoken1.nextToken();

								oKeyValueVo.setValue(sFileName);
							}
							if (oStoken.hasMoreTokens())
								oKeyValueVo.setValue2(oStoken.nextToken());

							alAttachmentList.add(oKeyValueVo);
						}

					}

				}
			}
		}

		return alAttachmentList;
	}

	public static ArrayList getNewEnquiryAttachmentDetails(NewEnquiryDto oNewEnquiryDto) throws Exception {
		oNewEnquiryDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
		ArrayList arAttachmentList = new ArrayList();
		if (oNewEnquiryDto.getEnquiryId() > 0) {
			URL url = new URL(CommonUtility.getStringValue(
					PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "getAttachmentList"));
			call.addParameter("applicationId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			call.addParameter("uniqueId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
			call.setReturnType(org.apache.axis.Constants.SOAP_ARRAY, String[].class);

			Object obj = call.invoke(new Object[] { applicationId, oNewEnquiryDto.getEnquiryId() });

			String[] sAttachmentDetails = (String[]) obj;

			if (sAttachmentDetails != null && sAttachmentDetails.length > 0) {
				KeyValueVo oKeyValueVo = null;
				for (int i = 0; i < sAttachmentDetails.length; i++) {
					oKeyValueVo = new KeyValueVo();
					String sDetails = sAttachmentDetails[i];

					if (sDetails != null && sDetails.trim().length() > 0) {
						StringTokenizer stToken = new StringTokenizer(sDetails, "!@");
						if (stToken != null && stToken.countTokens() > 0) {
							oKeyValueVo.setKey(stToken.nextToken());
							if (stToken.hasMoreTokens()) {
								String sFileName = stToken.nextToken();
								StringTokenizer stToken1 = new StringTokenizer(sFileName, ".");
								if (stToken1 != null && stToken1.countTokens() > 0)
									sFileName = stToken1.nextToken();

								oKeyValueVo.setValue(sFileName);
							}
							if (stToken.hasMoreTokens())
								oKeyValueVo.setValue2(stToken.nextToken());
							arAttachmentList.add(oKeyValueVo);
						}
					}
				}
			}
		}
		return arAttachmentList;
	}
	
	
}
