/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: DBUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: DB Utility class which holds all the methods related to database connections
 * ******************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ********************************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import in.com.rbc.quotation.common.constants.QuotationConstants;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.MissingResourceException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBUtility {
    /**
     * Context object to store the InitialContext class instance, 
     * which is used to lookup database
     */
    private static Context initCtx = null;
    
    /**
     * Object to store datasource instance that is used to establish 
     * database connection
     */
    public static javax.sql.DataSource ds;
    
    /**
     * String variable to store the database DSN name that is defined in 
     * the application/web server
     * This is used to establish connection with the database
     */
    public static String source = null;
    
    /**
     * String variable to store the connection object
     */
    public static Connection conn = null;
    
    /**
     * Constructor of DBUtility class
     */
    public DBUtility(){}
    
    /**
     * Static block to retrieve the datasource using which the database connection
     * object is retrieved.
     */
    static {
        try{
            // Retrieving the database DSN name provided in acmotor.properties file
        	source = PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_DB_SOURCE);
        	            
            // Creating the initial naming context
            initCtx = new InitialContext();
            
            // Retreiving datasource from JNDI lookup 
            ds = (DataSource)initCtx.lookup("java:comp/env/" + source); 
        }catch(Exception e){
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", "DBUtility", "Static Block", ExceptionUtility.getStackTraceAsString(e));
        }
    }
    
    /**
     * Function to retrieve database connection object
     * @return Returns the connection object
     * @throws NamingException if datasource is not found
     * @throws SQLException if connection could not be established
     * @throws MissingResourceException if no object for the given key can be found
     * @throws NullPointerException if key is null
     */
    public Connection getDBConnection() throws NamingException, 
                                               SQLException, 
                                               MissingResourceException, 
                                               NullPointerException {
        // Get a datasource connection
        conn = ds.getConnection();
        if (conn != null)
            LoggerUtility.log("INFO", this.getClass().getName(), "getDBConnection", "Connection established....");
        
        return conn;
    }
    
    /**
     * This method is used to set connection's auto-commit property to false
     * @param conn Connection object to set auto commit to false
     * @throws SQLException if connection is not available
     */
    public void setAutoCommitFalse(Connection conn) throws SQLException {
        
        if (conn != null) 
            conn.setAutoCommit(false);      
    }
    
    /**
     * This method is used to set connection's auto-commit property to true
     * @param conn Connection object to set auto commit to true
     * @throws SQLException if connection is not available
     */
    public void setAutoCommitTrue(Connection conn) throws SQLException {
        
        if (conn != null) 
            conn.setAutoCommit(true);
    }
    
    /**
     * This method is used to commit a transaction
     * @param conn Connection object to commit the transaction
     * @throws SQLException if connection is not available
     */
    public void commit(Connection conn) throws SQLException {
        
        if (conn != null) 
            conn.commit();
        
    }
    
    /**
     * This method is used to rollback a transaction
     * @param conn Connection object to rollback the transaction
     * @throws SQLException if connection is not available
     */
    public void rollback(Connection conn) throws SQLException {
        
        if (conn != null) 
            conn.rollback();
        
    }
    
    /**
     * This method is used to close ResultSet, Statement & Connection and 
     * release them to the pool
     * @param rs ResultSet object to close
     * @param pStmt Statement object to close
     * @param conn Connection object to close
     * @throws SQLException if either Connection, Statement or ResultSet 
     * objects are not available
     */
    public void releaseResources(ResultSet rs, PreparedStatement pStmt, 
            Connection conn) throws SQLException {
        
        if (rs!=null)
            rs.close();
        
        if (pStmt!=null)
            pStmt.close();
        
        if (conn!=null)
            conn.close();
    }
    
    /**
     * This method is used to close ResultSet & Statement and release them to the pool
     * @param rs ResultSet object to close
     * @param pStmt PreparedStatement object to close
     * @throws SQLException if either Statement or ResultSet objects are not available
     */
    public void releaseResources(ResultSet rs, PreparedStatement pStmt) 
                                        throws SQLException {
        if (rs!=null)
            rs.close();
        
        if (pStmt!=null)
            pStmt.close();
    }
    
    /**
     * This method is used to close Statement & Connection and release them to the pool
     * @param pStmt Statement object to close
     * @param conn Connection object to close
     * @throws SQLException if either Connection or Statement objects are 
     * not available
     */
    public void releaseResources(PreparedStatement pStmt, Connection conn) 
                                    throws SQLException {
        if (pStmt!=null)
            pStmt.close();
        
        if (conn!=null)
            conn.close();
    }
    
    /**
     * This method is used to close ResultSet and release it to the pool
     * @param rs ResultSet object to close
     * @throws SQLException if either Connection, Statement or ResultSet 
     * objects are not available
     */
    public void releaseResources(ResultSet rs) throws SQLException {
        
        if (rs!=null)
            rs.close();
    }
    
    /**
     * This method is used to close Statement and release it to the pool
     * @param pStmt Statement object to close
     * @throws SQLException if either Connection, Statement or ResultSet 
     * objects are not available
     */
    public void releaseResources(PreparedStatement pStmt) throws SQLException {
        
        if (pStmt!=null)
            pStmt.close();
    }
    
    /**
     * This method is used to close Connection and release it to the pool
     * @param conn Connection object to close
     * @throws SQLException if either Connection, Statement or ResultSet 
     * objects are not available
     */
    public void releaseResources(Connection conn) throws SQLException {
        
        if (conn!=null)
            conn.close();
    }
    
    /**
     * This method is used to close Statement and release it to the pool
     * @param cStmt CallableStatement object to close
     * @throws SQLException if either Connection, Statement or ResultSet 
     * objects are not available
     */
    public void releaseResources(CallableStatement cStmt) throws SQLException {
        
        if (cStmt!=null)
        	cStmt.close();
    }
    
}
