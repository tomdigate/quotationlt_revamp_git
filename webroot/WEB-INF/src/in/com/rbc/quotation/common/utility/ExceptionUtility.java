/**
 * ********************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: ExceptionUtility.java
 * Package: in.com.rbc.acmotor.common.utility
 * Desc: Exception Utility class which hold methods related to exception handling
 * ********************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ********************************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class ExceptionUtility {
    /**
     * This method accepts an exception object and returns the exception 
     * details in string format
     * @param e exception object that is passed from catch block of an action class
     * @return returns the raised exception details in string format
     */
    public static String getStackTraceAsString(Exception e) {
        ByteArrayOutputStream oByteArrayOutputStream = new ByteArrayOutputStream();
        PrintWriter oPrintWriter = new PrintWriter(oByteArrayOutputStream, true);
        e.printStackTrace(oPrintWriter);
        return "Exception Details: " + oByteArrayOutputStream.toString();
    }
}
