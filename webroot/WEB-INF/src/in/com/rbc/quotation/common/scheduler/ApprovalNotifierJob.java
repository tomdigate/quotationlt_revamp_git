package in.com.rbc.quotation.common.scheduler;

import java.util.ArrayList;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import in.com.rbc.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.utility.ProcessNewEnquiryRecordsManager;

public class ApprovalNotifierJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		//System.out.println("ApprovalNotifierJob.execute ... START ");
		LoggerUtility.log("INFO", this.getClass().getName(), "ApprovalNotifierJob.execute ... ", "START");
		
		ArrayList<NewEnquiryDto> alProcessNotifierList = null;
		ProcessNewEnquiryRecordsManager oProcessEnquiriesManager = new ProcessNewEnquiryRecordsManager();
		
		try {
			// Current System Date (FORMAT : MM/dd/yyyy ).
			String sCurrentDateValue = DateUtility.getRBCStandardDate(new Date());
			
			// APP URL.
			String sAppUrl = PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_APP_URL) 
								+ QuotationConstants.QUOTAION_FILEPATH_SEPARATOR + QuotationConstants.QUOTATION_APP_NAME + QuotationConstants.QUOTAION_FILEPATH_SEPARATOR;
			
			// :::::::::::::::::::::::::::::::::::::::: CASE 1. EMAIL SENT FLAG = N :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			// Step 1. Check Enquiry Status. Fetch No Mail Sent Enquiries. (for All 'Status' - With 'Pending' Approval/Request Processing.) 
			// Step 2. If Difference Between 'CREATED_DATE' and 'SYSDATE' > 2 - Send Email Notification.
			// Step 3. Update Enquiry Master - (1) Email Sent FLag = 'Y'  (2) Email Sent To = 'Current Assigned User' (3) Email Sent Date = SYSDATE.
			ArrayList<NewEnquiryDto> alNoMailSentList = oProcessEnquiriesManager.fetchNoMailSentEnquiriesList();
			if(alNoMailSentList.size() > 0) {
				//System.out.println("ApprovalNotifierJob.execute ... Emails from CASE : 1 ");
				oProcessEnquiriesManager.processNotifierEnquiries(alNoMailSentList, sAppUrl);
			}
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			
			// :::::::::::::::::::::::::::::::::::::::: CASE 2. EMAIL SENT FLAG = Y,  Current_Assignee = Notifier_Email_SSO ::::::::::::::::::::::::
			
			// Step 1. Fetch Enquiries where : Mail Sent = 'Y' AND Current_Assignee = Notifier_Email_SSO.
			// 			Check Enquiry Status. (for All 'Status' - With 'Pending' Approval/Request Processing.)
			// Step 2. If Difference Between 'NOTIFIER_MAILSENT_DATE' and 'SYSDATE' > 2 - Send Email Notification.
			// Step 3. Update Enquiry Master - (1) Email Sent FLag = 'Y'  (2) Email Sent To = 'Current Assigned User' (3) Email Sent Date = SYSDATE.
			ArrayList<NewEnquiryDto> alMailSent_CurrentAssignee_List = oProcessEnquiriesManager.fetchMailSentCurrentAssigneeEnquiriesList();
			if(alMailSent_CurrentAssignee_List.size() > 0) {
				//System.out.println("ApprovalNotifierJob.execute ... Emails from CASE : 1 ");
				oProcessEnquiriesManager.processNotifierEnquiries(alMailSent_CurrentAssignee_List, sAppUrl);
			}
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
			// :::::::::::::::::::::::::::::::::::::::: CASE 3. EMAIL SENT FLAG = Y,  Current_Assignee != Notifier_Email_SSO :::::::::::::::::::::::
			
			// Step 1. Fetch Enquiries where : Mail Sent = 'Y' AND Current_Assignee != Notifier_Email_SSO.
			// 			Check Enquiry Status. (for All 'Status' - With 'Pending' Approval/Request Processing.)
			// Step 2. If Difference Between 'USER_ASSIGNED_DATE' and 'SYSDATE' > 2 - Send Email Notification.
			// Step 3. Update Enquiry Master - (1) Email Sent FLag = 'Y'  (2) Email Sent To = 'Current Assigned User' (3) Email Sent Date = SYSDATE.
			ArrayList<NewEnquiryDto> alMailNotSent_CurrentAssignee_List = oProcessEnquiriesManager.fetchMailNotSentCurrentAssigneeEnquiriesList();
			if(alMailNotSent_CurrentAssignee_List.size() > 0) {
				//System.out.println("ApprovalNotifierJob.execute ... Emails from CASE : 1 ");
				oProcessEnquiriesManager.processNotifierEnquiries(alMailNotSent_CurrentAssignee_List, sAppUrl);
			}
			// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
			
		} catch(Exception e) {
			//e.printStackTrace();
			//System.out.println("ApprovalNotifierJob.execute ... Exception :: " + ExceptionUtility.getStackTraceAsString(e));
			LoggerUtility.log("INFO", this.getClass().getName(), "ApprovalNotifierJob.execute ... ", 
                    "ApprovalNotifierJob failed, to Send Email Notification - ERRORED: \n" + ExceptionUtility.getStackTraceAsString(e));
		}
		
		//System.out.println("ApprovalNotifierJob.execute ... END ");
		LoggerUtility.log("INFO", this.getClass().getName(), "ApprovalNotifierJob.execute ... ", "END");
	}
}
