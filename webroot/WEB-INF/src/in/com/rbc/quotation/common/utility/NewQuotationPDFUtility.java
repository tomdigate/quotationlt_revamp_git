package in.com.rbc.quotation.common.utility;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.AddOnDto;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto;

public class NewQuotationPDFUtility extends javax.swing.text.html.HTMLEditorKit.ParserCallback {

	Font oBlueHeader = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD,new Color(79,120,175));
	Font oTextFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, new Color(102,102,102));
	Font oTermsTextFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, new Color(102,102,102));
	Font oTermsHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, Color.BLACK);
	Font oTableHeardingFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDEFINED, Color.WHITE);
	
	Font oHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, Color.BLUE);
	Font oSubHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(79,120,175));
	
	 Font oAddOnHeardingFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDEFINED, new Color(153,0,0));
	
	Font oTableBlueText = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0,69,138));
	Font oTableBigBlueText = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0,69,138));
	Font oTableSmallText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, Color.BLACK);
	Font oTableSmallBoldText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, Color.BLACK);
	Font oTableSmallRedText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, new Color(153,0,0));
	Font oTableSubscriptText = FontFactory.getFont(FontFactory.HELVETICA, 5, Font.UNDEFINED, Color.BLACK);
	
	Font oTableMiniText = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.UNDEFINED, Color.BLACK);
	Font oTableMiniBoldText = FontFactory.getFont(FontFactory.HELVETICA, 6, Font.BOLD, Color.BLACK);
	
	public static StringBuffer  sbData = new StringBuffer(); 
	
	String sFullPath = "";
	
	
	public String generateQuotationPDF(NewEnquiryDto oNewEnquiryDto, HttpServletRequest request, String sPDFType) throws Exception {
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sCustomer = oNewEnquiryDto.getCustomerName();
		String[] oCustomerArray = null;
		String spathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = spathArr[0];
		String sFinalEnquiryId = oNewEnquiryDto.getEnquiryNumber().substring(15);
		String sPdfFile = "";
		
		if (sCustomer != null && !sCustomer.equals("")) {
			oCustomerArray = sCustomer.split(" ");
			if (oCustomerArray.length >= QuotationConstants.QUOTATION_LITERAL_2)
				sCustomer = oCustomerArray[0] + " " + oCustomerArray[1];
			else
				sCustomer = oCustomerArray[0];
		}

		//System.out.println("Value of sFullPath : " + sFullPath.toString());
		if(sFullPath.contains("C:")) {
			sPdfFile = "C:/PROJECTS/Tomcat9_Workspace/Git/quotationlt_Abhilash/pdfFiles" + QuotationConstants.QUOTAION_FILEPATH_SEPARATOR
					+ QuotationConstants.QUOTATION_STRING_QUOTATION + QuotationConstants.QUOTATION_STRING_UNDERSC
					+ sFinalEnquiryId + QuotationConstants.QUOTATION_STRING_UNDERSC + sCustomer
					+ QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentYear()
					+ QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentMonth();
		} else {
			sPdfFile = sFullPath.replace("\\", File.separator) + File.separator
				+ QuotationConstants.QUOTATION_STRING_HTML + File.separator
				+ QuotationConstants.QUOTATION_STRING_QUOTATION + QuotationConstants.QUOTATION_STRING_UNDERSC
				+ sFinalEnquiryId + QuotationConstants.QUOTATION_STRING_UNDERSC + sCustomer
				+ QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentYear()
				+ QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentMonth();
		}
		
		if(QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL.equalsIgnoreCase(sPDFType)) {
			sPdfFile = sPdfFile + QuotationConstants.QUOTATION_STRING_UNDERSC + QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL + QuotationConstants.QUOTATION_PDFEXT;
		} else if(QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL.equalsIgnoreCase(sPDFType)) {
			sPdfFile = sPdfFile + QuotationConstants.QUOTATION_STRING_UNDERSC + QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL + QuotationConstants.QUOTATION_PDFEXT;
		}
		//System.out.println("sPdfFile : " + sPdfFile);
		
		File oFile = new File(sPdfFile);
		FileOutputStream oFileOutputStream = null;
		
		ByteArrayOutputStream oByteArrayOutputStream = null;
		try {

			oFileOutputStream = new FileOutputStream(oFile);
			
			// Fetch the PDF Content to Output Stream.
			if(QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL.equalsIgnoreCase(sPDFType)) {
				oByteArrayOutputStream = generateCommercialPDF(oNewEnquiryDto, request);
			} else if(QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL.equalsIgnoreCase(sPDFType)) {
				oByteArrayOutputStream = generateTechnicalPDF(oNewEnquiryDto, request);
			}
			
			oByteArrayOutputStream.writeTo(oFileOutputStream);
		} catch (IOException e) {
			
		} finally {
			if (oFileOutputStream != null) {
				try {
					oFileOutputStream.close();
				} catch (IOException e) {
				}
			}
		}

		return sPdfFile;
	}
	
	public String generateNewEnqDmUploadedAttachments(NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		String sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String spathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = spathArr[0];
		int iAttachmentId = 0;
		byte[] returnBytes = null;

		String sAttachmentFileName = sFullPath.replace("\\", File.separator) + File.separator
				+ QuotationConstants.QUOTATION_STRING_HTML + File.separator + oNewEnquiryDto.getAttachmentFileName();

		File oFile = new File(sAttachmentFileName);
		FileOutputStream oFileOutputStream = null;
		ByteArrayOutputStream oByteArrayOutputStream = null;
		try {
			iAttachmentId = oNewEnquiryDto.getAttachmentId();
			if (iAttachmentId > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				URL url = new URL(CommonUtility.getStringValue(
						PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
				Service service = new Service();
				Call call = (Call) service.createCall();
				call.setTargetEndpointAddress(url);
				call.setOperationName(new QName("AttachmentService", "getAttachmentByBytes"));
				call.addParameter("attachmentId", org.apache.axis.encoding.XMLType.XSD_INT, ParameterMode.IN);
				call.setReturnType(org.apache.axis.encoding.XMLType.XSD_BASE64);

				returnBytes = (byte[]) call.invoke(new Object[] { iAttachmentId });
				if (returnBytes != null && returnBytes.length > QuotationConstants.QUOTATION_LITERAL_ZERO) {
					oFileOutputStream = new FileOutputStream(oFile);
					oFileOutputStream.write(returnBytes);
				}
			}

		} catch (IOException e) {
			
		} finally {
			if (oFileOutputStream != null) {
				try {
					oFileOutputStream.close();
				} catch (IOException e) {
					
				}
			}
		}

		return sAttachmentFileName;
	}
	
	
	public void deletePDF(String sFilePath) throws Exception {
		boolean isAttachmentExists = false;
		String[] sAttachmentFile = null;
		File oFile = new File(sFilePath);
		boolean isTrue = false;

		if ((sFilePath != null && sFilePath.trim().length() > 0)) {

			isAttachmentExists = sFilePath.contains(QuotationConstants.QUOTATION_SPLITQSYMBOL);

			if (isAttachmentExists) {
				sAttachmentFile = sFilePath.split(QuotationConstants.QUOTATION_FORWARDSLASHSYMBOL + QuotationConstants.QUOTATION_SPLITQSYMBOL);

				for (int i = 0; i < sAttachmentFile.length; i++) {
					oFile = new File(sAttachmentFile[i]);
					isTrue = oFile.delete();
				}
			} else {
				oFile = new File(sFilePath);
				isTrue = oFile.delete();
			}
		}
	}
	
	public ByteArrayOutputStream generateCommercialPDF(NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		ByteArrayOutputStream oByteArrayOutputStream = new ByteArrayOutputStream();
		Document oDocument = new Document(PageSize.A4, 20, 20, 70, 70);
		
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sPathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = sPathArr[0];
		
		String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATIONLT_MOTOR2_IMG;
		
		/*String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR1IMG;*/
		
		String sImagePath4=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR2IMG;
		
		String sIntroNote = "", sField1_PriceBasis = "", sField2_Taxes = "", sField3_DeliveryPeriod = "", sField4_PaymentTerms = "", sField5_WarrantyTerms = "";
        String sField6_Packaging = "",  sField7_OfferValidity = "", sField8_PriceValidity = "", sField9_Supervision = "";
        
        Table oTable =null;
		PdfWriter oWriter =null;		
		Image oBmp3 =null;
		Image oBmp4 =null;
		Paragraph oReqParagraph =null;
		Paragraph oP=null;
		Table oTable1 =null;
		Paragraph oParagraph =null;
		Paragraph oParagraph1 = new Paragraph();
		Paragraph oParagraph2 = new Paragraph();
		
		try {
			
			/*Creating instance of PDFWrite Object*/
			oWriter = PdfWriter.getInstance(oDocument, oByteArrayOutputStream );
			
			HeaderFooterPageEvent oEvent = new HeaderFooterPageEvent();
			oWriter.setPageEvent(oEvent);			
			
			oDocument.open();
			
			// ------------------------------------------------------- HeadNote : RFQ Details : START -------------------------------------------------
			/* ................. Heading ............................ */
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_TOCUSTOMER, oBlueHeader);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. RFQ # ............................ */
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_RFQ + oNewEnquiryDto.getEnquiryNumber() + "  ", oTextFont);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. Heading ............................ */
			oP = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oP.setAlignment(Element.ALIGN_LEFT);
			oDocument.add(oP);
			/* ................. Blank Space ............................ */
			oDocument.add(new Paragraph(" "));
			/* ................. Customer Information ................... */
			oDocument.add(addCustomerInformation(oNewEnquiryDto));
			// ------------------------------------------------------- HeadNote : RFQ Details : END ---------------------------------------------------

			// ------------------------------------------------------- Motor Images : START -----------------------------------------------------------
			oBmp3 = Image.getInstance(sImagePath3);
			oBmp3.scalePercent(80);

			oParagraph1.add(new Chunk(oBmp3, 20, -250));
			// oParagraph1.add(new Chunk(oBmp4, QuotationConstants.QUOTATION_LITERAL_ZERO,
			// QuotationConstants.QUOTATION_LITERAL_M250));
			oParagraph1.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_250);
			oDocument.add(oParagraph1);
			// ------------------------------------------------------- Motor Images : END -------------------------------------------------------------

			// ------------------------------------------------------- FootNote : Signature : START ---------------------------------------------------
			oParagraph2 = new Paragraph("");
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_REGARDS, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(oNewEnquiryDto.getSm_name(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_MEMI, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_EMAIL + oNewEnquiryDto.getSmEmail(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2
					.add(new Chunk(QuotationConstants.QUOTATION_CONTACT + oNewEnquiryDto.getSmContactNo(), oTextFont));
			oParagraph2.setSpacingBefore(QuotationConstants.QUOTATION_LITERAL_150);
			oDocument.add(oParagraph2);
			// ------------------------------------------------------- FootNote : Signature : END -----------------------------------------------------
			
			oTable = new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			oTable.setBackgroundColor(new Color(255,255,255));
			
			// ----------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Quotation : Commercial Terms : START -------------------------------------------
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			insertTableCell(oTable1, QuotationConstants.QUOTATION_PDF_ADDRESSTO, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oDocument.add(new Paragraph(" "));
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sIntroNote = "With reference to your RFQ# " + oNewEnquiryDto.getEnquiryNumber() + ", we are pleased to offer you the motors based on following terms and condition along with Technical details: ";
			insertTableCell(oTable1, sIntroNote, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField1_PriceBasis =  "1)  Price Basis : " + oNewEnquiryDto.getDeliveryTerm();
			insertTableCell(oTable1, sField1_PriceBasis, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField2_Taxes = "2) Taxes : GST Shall be charged extra " + oNewEnquiryDto.getGstValue();
			insertTableCell(oTable1, sField2_Taxes, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField3_DeliveryPeriod = "3) Delivery Period:  Starting " + oNewEnquiryDto.getOrderCompletionLeadTime() + " Weeks. ( Kindly refer item wise lead time in Technical details.) ";
			insertTableCell(oTable1, sField3_DeliveryPeriod, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField4_PaymentTerms = "4) Payment Terms : ";
			insertTableCell(oTable1, sField4_PaymentTerms, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			// Payment Terms Table Display
			oDocument.add(buildPaymentTermsDetails(oNewEnquiryDto));
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField5_WarrantyTerms = "5) Warranty Terms : " + oNewEnquiryDto.getWarrantyCommissioningDate() + " Months from the date of comissioning or "
									+ oNewEnquiryDto.getWarrantyDispatchDate() + " Months from the date of dispatch, whichever is earlier.";
			insertTableCell(oTable1, sField5_WarrantyTerms, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			if("Standard".equalsIgnoreCase(oNewEnquiryDto.getPackaging())) {
				sField6_Packaging = "6) Packaging and Forwarding: Inclusive Standard Packing.";
			} else {
				sField6_Packaging = "6) Packaging and Forwarding: Inclusive Seaworthy Packing.";
			}
			insertTableCell(oTable1, sField6_Packaging, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField7_OfferValidity = "7) Offer Validity : " + oNewEnquiryDto.getOfferValidity() + " Days.";
			insertTableCell(oTable1, sField7_OfferValidity, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField8_PriceValidity = "8) Price Validity : " + oNewEnquiryDto.getOfferValidity() + " Days from PO Date (for Drawing Approval / Mfg. Clearance).";
			insertTableCell(oTable1, sField8_PriceValidity, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			String sValue = ( StringUtils.isNotBlank(oNewEnquiryDto.getSupervisionDetails()) && QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails()) ) ? "Inclusive" : "Non-Inclusive";
			sField9_Supervision = "9) Supervision for Erection and Comissioning : " + sValue;
			insertTableCell(oTable1, sField9_Supervision, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			//If Supervision = Inclusive : Then Display the Supervision No. of Man Days value
			if( StringUtils.isNotBlank(oNewEnquiryDto.getSupervisionDetails()) && QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails()) ) {
				insertTableCell(oTable1, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_LEFT, 1, false, false, oTextFont);
				String sNoOfManDaysValue = " Supervision No. of Man Days : " + oNewEnquiryDto.getSuperviseNoOfManDays() + " days.";
				insertTableCell(oTable1, sNoOfManDaysValue, Element.ALIGN_LEFT, 4, false, false, oTextFont);
				insertTableCell(oTable1, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_LEFT, 5, false, false, oTextFont);
				String sSupervisionNote = "Note: To & Fro travelling charges, Fooding, Lodging & Local conveyance at site are at customer's account.";
				insertTableCell(oTable1, sSupervisionNote, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			}
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
							
			oDocument.add(new Paragraph(" "));
			// ------------------------------------------------------- Quotation : Commercial Terms : END ---------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Commercial Offer Section : START -----------------------------------------------
			oParagraph = new Paragraph();
			
			oTable = new Table(4);
			oTable.setWidth(100);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			insertTableCell(oTable, QuotationConstants.QUOTATION_COMOFFER, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			
			oDocument.add(addCommercialOffer(oNewEnquiryDto));
			
			oDocument.add(new Paragraph(" "));
			
			// ------------------------------------------------------- Commercial Offer Section : END ------------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ---------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument = buildStandardNotes(oDocument);
			
			// ---------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument.close();
			
		} finally{
			
		}
		
		return oByteArrayOutputStream;
	}
	
	public ByteArrayOutputStream generateTechnicalPDF(NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		ByteArrayOutputStream oByteArrayOutputStream = new ByteArrayOutputStream();
		Document oDocument = new Document(PageSize.A4, 20, 20, 70, 70);
		
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sPathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = sPathArr[0];
		
		String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATIONLT_MOTOR2_IMG;
		
		/*String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR1IMG;*/
		
		String sImagePath4=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR2IMG;
        
		NewRatingDto oNewRatingDto = null;
		
		Table oTable =null;
		PdfWriter oWriter =null;		
		Image oBmp3 =null;
		Paragraph oReqParagraph =null;
		Paragraph oP=null;
		Table oTable1 =null;
		Paragraph oParagraph =null;
		Paragraph oParagraph1 = new Paragraph();
		Paragraph oParagraph2 = new Paragraph();
		
		try {
			
			/*Creating instance of PDFWrite Object*/
			oWriter = PdfWriter.getInstance(oDocument, oByteArrayOutputStream );
			
			HeaderFooterPageEvent oEvent = new HeaderFooterPageEvent();
			oWriter.setPageEvent(oEvent);			
			
			oDocument.open();
			
			// ------------------------------------------------------- HeadNote : RFQ Details : START -------------------------------------------------
			/* ................. Heading ............................ */
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_TOCUSTOMER, oBlueHeader);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. RFQ # ............................ */
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_RFQ + oNewEnquiryDto.getEnquiryNumber() + "  ", oTextFont);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. Heading ............................ */
			oP = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oP.setAlignment(Element.ALIGN_LEFT);
			oDocument.add(oP);
			/* ................. Blank Space ............................ */
			oDocument.add(new Paragraph(" "));
			/* ................. Customer Information ................... */
			oDocument.add(addCustomerInformation(oNewEnquiryDto));
			// ------------------------------------------------------- HeadNote : RFQ Details : END ---------------------------------------------------

			// ------------------------------------------------------- Motor Images : START -----------------------------------------------------------
			oBmp3 = Image.getInstance(sImagePath3);
			oBmp3.scalePercent(80);

			oParagraph1.add(new Chunk(oBmp3, 20, -250));
			// oParagraph1.add(new Chunk(oBmp4, QuotationConstants.QUOTATION_LITERAL_ZERO,
			// QuotationConstants.QUOTATION_LITERAL_M250));
			oParagraph1.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_250);
			oDocument.add(oParagraph1);
			// ------------------------------------------------------- Motor Images : END -------------------------------------------------------------

			// ------------------------------------------------------- FootNote : Signature : START ---------------------------------------------------
			oParagraph2 = new Paragraph("");
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_REGARDS, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(oNewEnquiryDto.getSm_name(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_MEMI, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_EMAIL + oNewEnquiryDto.getSmEmail(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2
					.add(new Chunk(QuotationConstants.QUOTATION_CONTACT + oNewEnquiryDto.getSmContactNo(), oTextFont));
			oParagraph2.setSpacingBefore(QuotationConstants.QUOTATION_LITERAL_150);
			oDocument.add(oParagraph2);
			// ------------------------------------------------------- FootNote : Signature : END -----------------------------------------------------
			
			oTable = new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			oTable.setBackgroundColor(new Color(255,255,255));
			
			// ----------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Technical Ratings Section : START ----------------------------------------------
			
			if( CollectionUtils.isNotEmpty(oNewEnquiryDto.getNewRatingsList()) ) {
				// Fetch NewRatingDto List.
				ArrayList<NewRatingDto> alRatingsList = oNewEnquiryDto.getNewRatingsList();
				// Build Each Technical Rating Detail and Add to PDF : 
				int iRatingIdx = 1;
				for(int i = 0; i < alRatingsList.size(); i++) {
					oNewRatingDto = alRatingsList.get(i);
					
					// Check if NewRatingDto ProductLine is present in alPDFProdLinesList.
					if(oNewRatingDto.getNewTechnicalOfferDto() != null) {
						// Build the Rating Content
						oDocument.add(addTechnicalRatingContent(oNewRatingDto, iRatingIdx));
						iRatingIdx = iRatingIdx + 1;
						
						// Add Each Rating in a New Page
						oDocument.newPage();
						oDocument.add(oP);
					}
				}
			}
			
			// ------------------------------------------------------- Technical Ratings Section : END -----------------------------------------------
			
			// ------------------------------------------------------- Comments / Deviations Section : START -----------------------------------------
			if(StringUtils.isNotBlank(oNewEnquiryDto.getCommentsDeviations().trim())) {
				oParagraph = new Paragraph();
				
				oTable = new Table(4);
				oTable.setWidth(100);
				oTable.setSpacing((float).75);
				oTable.setPadding((float).75);
				oTable.setBorder(0);
				insertTableCell(oTable, QuotationConstants.QUOTATION_COMMENTSDEVIATIONS, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
				oParagraph.add(oTable);
				oDocument.add(oParagraph);
				
				oDocument.add(buildCommentsDeviationsContent(oNewEnquiryDto));
				
				oDocument.add(new Paragraph(" "));
				
				// Add Next Content in New Page.
				oDocument.newPage();
				oDocument.add(oP);
			}
			// ------------------------------------------------------- Comments / Deviations Section : END -------------------------------------------
			
			// ---------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument = buildStandardNotes(oDocument);
			
			// ---------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument.close();
						
		} finally{
			
		}
		
		return oByteArrayOutputStream;
	}
	
	
	
	public ByteArrayOutputStream generatePDF(NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		
		ByteArrayOutputStream oByteArrayOutputStream = new ByteArrayOutputStream();
		Document oDocument = new Document(PageSize.A4, 20, 20, 70, 70);
		
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sPathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = sPathArr[0];
		
		String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATIONLT_MOTOR2_IMG;
		
		/*String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR1IMG;*/
		
		String sImagePath4=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR2IMG;
        
		String sDocSubmission="";
        String sPackForward="";
        String oTaxDuties="";
        String sSpecialNote="";
        String sTerm1="";
		String sTerm2="";
		String sTerm3="";
		String sTerm4="";
		String sTerm5="";
		String sTerm6="";
		String sTerm7="";
		String sTerm8="";
		String sTerm9="";
		String sTerm10="";
		String sTerm11="";
		String sTerm12="";
		String sTerm13="";
		String sTerm14="";
		
		ArrayList arlRatings =null;
		
		NewRatingDto oNewRatingDto = null;
		TechnicalOfferDto oTechnicalOfferDto = null;
		PdfPCell oCell = null;
		PdfPTable oMainTable =null;
		String sRating=null;
		PdfPCell oFirstTableCell =null;
		PdfPTable oFirstTable =null;
		Table oTable =null;
		PdfWriter oWriter =null;		
		Image oBmp3 =null;
		Image oBmp4 =null;
		Paragraph oReqParagraph =null;
		Paragraph oP=null;
		Table oTable1 =null;
		Paragraph oParagraph =null;
		Paragraph oRegardsParagraph =null;
		Cell oCellc =null;
		Cell oCell1=null;
		String sKw="";
		String sBkw="";
        PdfPCell ooSecondTableCell = null;
        PdfPTable ooSecondTable = null;
        String sFinalString=null;
        String sRatedVoltage=null;
        String sFrequency="";
        String sRpm="";
        String sFlcAmps="";
        String sNlCurrent="";
        String sFltSQCAGE="";
        String sSTHot="";
        String sSTCold="";
        String sStartTimeRV="";
        String sStartTimeRV80="";
        String sAmbience="";
        String sAltitude="";
        String sNoiseLevel="";
        String sMoment1=QuotationConstants.QUOTATION_MOMENT1;
        String sMoment2=QuotationConstants.QUOTATION_MOMENT2;
        String sRtGD2="";
        String sMotorTotalWgt="";
        String sDevClassification="";
        String sRgrdsPrgph="";
        String sDefintions ="";
        String sValidity ="";
        String sScope ="";
        String sPriceBasis ="";
        String sFreightInsurance ="";
        String sPackFrwd ="";
        String sTermsPymt ="";
        String sDelivery ="";
        String sDrawingDocsapprvl ="";
        String sDelayMnfclrnce ="";         
        String sFMajeure ="";
        String sStorage ="";
        String sWrnty ="";
        String sWgtsndmsns ="";
        String sTests ="";         
        String sStandards ="";
        String sLimtOfLiability ="";
        String sConseqLosses ="";
        String sArbitration ="";
        String sLang ="";         
        String sGovLaw ="";
        String sViqty ="";
        String sTot ="";
        String sCtas ="";
        String sPobar ="";         
        String sCdbd ="";
        String sSuspension ="";
        String sTermination ="";
        String sBankrtcy ="";
        String sAcceptance ="";         
        String sLos ="";
        String sCis ="";
        String sCfc ="";
        String sGeneral ="";
        String sIntroNote = "";
        String sField1_PriceBasis = "";
        String sField2_Taxes = "";
        String sField3_DeliveryPeriod = "";
        String sField4_PaymentTerms = "";
        String sField5_WarrantyTerms = "";
        String sField6_Packaging = "";
        String sField7_OfferValidity = "";
        String sField8_PriceValidity = "";
        String sField9_Supervision = "";
        
		int iNumber = QuotationConstants.QUOTATION_NUMBER1;
		char c = (char)iNumber;
		
		int iNumber2=QuotationConstants.QUOTATION_NUMBER2;
		char c2=(char)iNumber2;
		
		
		PdfPTable oPdfPTable = new PdfPTable(2);
		PdfPCell oPdfPCell = new PdfPCell();
		Paragraph oParagraph1 = new Paragraph();
		Paragraph oParagraph2 = new Paragraph();
		
		PdfTemplate oPdfTemplate1 = null;
		
		try {
						
			/*Creating instance of PDFWrite Object*/
			oWriter = PdfWriter.getInstance(oDocument, oByteArrayOutputStream );
			
			HeaderFooterPageEvent oEvent = new HeaderFooterPageEvent();
			oWriter.setPageEvent(oEvent);			
			
			oDocument.open();
			
			// ------------------------------------------------------- HeadNote : RFQ Details : START -------------------------------------------------
			/* ................. Heading ............................*/
			oReqParagraph= new Paragraph(QuotationConstants.QUOTATION_TOCUSTOMER,oBlueHeader);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. RFQ # ............................*/
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_RFQ + oNewEnquiryDto.getEnquiryNumber() +"  ", oTextFont);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			/* ................. Heading ............................*/
			oP = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oP.setAlignment(Element.ALIGN_LEFT);
			oDocument.add(oP);
			/* ................. Blank Space ............................*/
			oDocument.add(new Paragraph(" "));
			/* ................. Customer Information ...................*/
			oDocument.add(addCustomerInformation(oNewEnquiryDto));
			
			// ------------------------------------------------------- HeadNote : RFQ Details : END ---------------------------------------------------
			
			// ------------------------------------------------------- Motor Images : START -----------------------------------------------------------
			
			oBmp3 = Image.getInstance(sImagePath3);
			oBmp3.scalePercent(80);			
			/*oBmp4 = Image.getInstance(sImagePath4);
			oBmp4.scalePercent(QuotationConstants.QUOTATION_LITERAL_60);*/
			
			oParagraph1.add(new Chunk(oBmp3, 20, -250));
			//oParagraph1.add(new Chunk(oBmp4, QuotationConstants.QUOTATION_LITERAL_ZERO, QuotationConstants.QUOTATION_LITERAL_M250));	
			oParagraph1.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_250);
			oDocument.add(oParagraph1);	
			
			// ------------------------------------------------------- Motor Images : END -------------------------------------------------------------
			
			// ------------------------------------------------------- FootNote : Signature : START ---------------------------------------------------
			
			oParagraph2 = new Paragraph("");
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_REGARDS, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(oNewEnquiryDto.getSm_name(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_MEMI, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_EMAIL + oNewEnquiryDto.getSmEmail(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_CONTACT + oNewEnquiryDto.getSmContactNo(), oTextFont));
			oParagraph2.setSpacingBefore(QuotationConstants.QUOTATION_LITERAL_150);
			oDocument.add(oParagraph2);
			
			// ------------------------------------------------------- FootNote : Signature : END -----------------------------------------------------
			
			oTable = new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			oTable.setBackgroundColor(new Color(255,255,255));
			
			// ----------------------------------------------------------------------------------------------------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Quotation : Commercial Terms : START -------------------------------------------
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			insertTableCell(oTable1, QuotationConstants.QUOTATION_PDF_ADDRESSTO, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oDocument.add(new Paragraph(" "));
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sIntroNote = "With reference to your RFQ# " + oNewEnquiryDto.getEnquiryNumber() + ", we are pleased to offer you the motors based on following terms and condition along with Technical details: ";
			insertTableCell(oTable1, sIntroNote, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField1_PriceBasis =  "1)  Price Basis : " + oNewEnquiryDto.getDeliveryTerm();
			insertTableCell(oTable1, sField1_PriceBasis, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField2_Taxes = "2) Taxes : GST Shall be charged extra " + oNewEnquiryDto.getGstValue();
			insertTableCell(oTable1, sField2_Taxes, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField3_DeliveryPeriod = "3) Delivery Period:  Starting " + oNewEnquiryDto.getOrderCompletionLeadTime() + " Weeks. ( Kindly refer item wise lead time in Technical details.) ";
			insertTableCell(oTable1, sField3_DeliveryPeriod, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField4_PaymentTerms = "4) Payment Terms : ";
			insertTableCell(oTable1, sField4_PaymentTerms, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			// Payment Terms Table Display
			oDocument.add(buildPaymentTermsDetails(oNewEnquiryDto));
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField5_WarrantyTerms = "5) Warranty Terms : " + oNewEnquiryDto.getWarrantyCommissioningDate() + " Months from the date of comissioning or "
										+ oNewEnquiryDto.getWarrantyDispatchDate() + " Months from the date of dispatch, whichever is earlier.";
			insertTableCell(oTable1, sField5_WarrantyTerms, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			if("Standard".equalsIgnoreCase(oNewEnquiryDto.getPackaging())) {
				sField6_Packaging = "6) Packaging and Forwarding: Inclusive Standard Packing.";
			} else {
				sField6_Packaging = "6) Packaging and Forwarding: Inclusive Seaworthy Packing.";
			}
			insertTableCell(oTable1, sField6_Packaging, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField7_OfferValidity = "7) Offer Validity : " + oNewEnquiryDto.getOfferValidity() + " Days.";
			insertTableCell(oTable1, sField7_OfferValidity, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			sField8_PriceValidity = "8) Price Validity : " + oNewEnquiryDto.getOfferValidity() + " Days from PO Date (for Drawing Approval / Mfg. Clearance).";
			insertTableCell(oTable1, sField8_PriceValidity, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			oParagraph = new Paragraph();
			oTable1 = new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			String sValue = ( StringUtils.isNotBlank(oNewEnquiryDto.getSupervisionDetails()) && QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails()) ) ? "Inclusive" : "Non-Inclusive";
			sField9_Supervision = "9) Supervision for Erection and Comissioning : " + sValue;
			insertTableCell(oTable1, sField9_Supervision, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			//If Supervision = Inclusive : Then Display the Supervision No. of Man Days value
			if( StringUtils.isNotBlank(oNewEnquiryDto.getSupervisionDetails()) && QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails()) ) {
				insertTableCell(oTable1, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_LEFT, 1, false, false, oTextFont);
				String sNoOfManDaysValue = " Supervision No. of Man Days : " + oNewEnquiryDto.getSuperviseNoOfManDays() + " days.";
				insertTableCell(oTable1, sNoOfManDaysValue, Element.ALIGN_LEFT, 4, false, false, oTextFont);
				insertTableCell(oTable1, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_LEFT, 5, false, false, oTextFont);
				String sSupervisionNote = "Note: To & Fro travelling charges, Fooding, Lodging & Local conveyance at site are at customer's account.";
				insertTableCell(oTable1, sSupervisionNote, Element.ALIGN_LEFT, 5, false, false, oTextFont);
			}
			oParagraph.add(oTable1);
			oDocument.add(oParagraph);
			
			
			oDocument.add(new Paragraph(" "));
			
			// ------------------------------------------------------- Quotation : Commercial Terms : END ---------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Commercial Offer Section : START -----------------------------------------------
			
			oParagraph = new Paragraph();
			
			oTable = new Table(4);
			oTable.setWidth(100);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			insertTableCell(oTable, QuotationConstants.QUOTATION_COMOFFER, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			
			oDocument.add(addCommercialOffer(oNewEnquiryDto));
			
			oDocument.add(new Paragraph(" "));
			
			// ------------------------------------------------------- Commercial Offer Section : END ------------------------------------------------
			
			oDocument.newPage();
			oDocument.add(oP);
			
			// ------------------------------------------------------- Technical Ratings Section : START ----------------------------------------------
			
			if( CollectionUtils.isNotEmpty(oNewEnquiryDto.getNewRatingsList()) ) {
				// Fetch NewRatingDto List.
				ArrayList<NewRatingDto> alRatingsList = oNewEnquiryDto.getNewRatingsList();
				// Build Each Technical Rating Detail and Add to PDF : 
				int iRatingIdx = 1;
				for(int i = 0; i < alRatingsList.size(); i++) {
					oNewRatingDto = alRatingsList.get(i);
					
					// Check if NewRatingDto ProductLine is present in alPDFProdLinesList.
					if(oNewRatingDto.getNewTechnicalOfferDto() != null) {
						// Build the Rating Content
						oDocument.add(addTechnicalRatingContent(oNewRatingDto, iRatingIdx));
						iRatingIdx = iRatingIdx + 1;
						
						// Add Each Rating in a New Page
						oDocument.newPage();
						oDocument.add(oP);
					}
				}
			}
			
			// ------------------------------------------------------- Technical Ratings Section : END -----------------------------------------------
			
			// ------------------------------------------------------- Comments / Deviations Section : START -----------------------------------------
			if(StringUtils.isNotBlank(oNewEnquiryDto.getCommentsDeviations().trim())) {
				oParagraph = new Paragraph();
				
				oTable = new Table(4);
				oTable.setWidth(100);
				oTable.setSpacing((float).75);
				oTable.setPadding((float).75);
				oTable.setBorder(0);
				insertTableCell(oTable, QuotationConstants.QUOTATION_COMMENTSDEVIATIONS, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
				oParagraph.add(oTable);
				oDocument.add(oParagraph);
				
				oDocument.add(buildCommentsDeviationsContent(oNewEnquiryDto));
				
				oDocument.add(new Paragraph(" "));
				
				// Add Next Content in New Page.
				oDocument.newPage();
				oDocument.add(oP);
			}
			// ------------------------------------------------------- Comments / Deviations Section : END -------------------------------------------
			
			
			// ------------------------------------------------------- Final Page : Notes Standard T & C : START -------------------------------------
			
			sDocSubmission=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DOCUMENT_SUBMISSIONS);
	        sPackForward=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKAGE_FORWARDINGS);
	        oTaxDuties=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIESS);
	        sSpecialNote=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SPECIAL_NOTES);

			sTerm1 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1).replaceAll("</li></ul>",QuotationConstants.QUOTATION_EMPTY);
			sTerm2 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm3 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm4 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm5 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm6 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm7 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm8 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm9 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm10 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm11 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm12 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm13 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm14 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			
			sDefintions = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sValidity = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY);
			sScope = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE);
			sPriceBasis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sFreightInsurance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE);
			sPackFrwd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING);
			sTermsPymt = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sDelivery = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sDrawingDocsapprvl = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL);
			sDelayMnfclrnce = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE);
			
			sFMajeure = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sStorage = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE);
			sWrnty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY);
			sWgtsndmsns = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS);
			sTests = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS);
			
			sStandards = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS);
			sLimtOfLiability = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY);
			sConseqLosses = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES);
			sArbitration = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION);
			sLang = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE);
			
			sGovLaw = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW);
			sViqty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY);
			sTot = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sCtas = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY);
			sPobar = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK);
			
			sCdbd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT);
			sSuspension = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sSuspension= sSuspension.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sSuspension= sSuspension.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sSuspension= sSuspension.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTermination = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sTermination = sTermination.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sTermination = sTermination.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sTermination = sTermination.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sBankrtcy = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY);
			sAcceptance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE);
			
			sLos= PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY);
			sCis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sCfc = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sGeneral = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			
			
			/* Adding the General Terms and Notes */
			oTable=new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);

			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_NOTES, oTableBlueText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			oParagraph = new Paragraph();
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			
			oDocument.add(new Paragraph(" "));
	
			oTable=new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			
			
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);

			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm1.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm2.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    


			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm3.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm4.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm5.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm6.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm7.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
		
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm8.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
		
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm9.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm10.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm11.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm12.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			
			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm13.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			

			oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm14.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
			oCellc.setColspan(3);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			
			oParagraph = new Paragraph();
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			oDocument.newPage();
			
			
			oParagraph = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oParagraph.setAlignment(Element.ALIGN_LEFT);
			
			oDocument.add(oParagraph);	
	        

	        oTable=new Table(4);
	        oTable.setWidth(95);
	        oTable.setBorder(0);
	        oTable.setSpacing((float).75);
	        oTable.setPadding((float).75);
	        oTable.setBorder(0);

			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SGTCO, oTableBlueText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DEF, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sDefintions.replaceAll(QuotationConstants.QUOTATION_NBSP, " "), oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			


			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_VALIDITYS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			oCellc = new Cell(new Phrase(sValidity, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SCOPES, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
		   
			
			oCellc = new Cell(new Phrase(sScope, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PRICEANDBASISAS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(sPriceBasis, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_FRIN, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sFreightInsurance, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);	
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PACKING, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			

			oCellc = new Cell(new Phrase(sPackFrwd, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TERMSOFPAY, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			

			oCellc = new Cell(new Phrase(sTermsPymt, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			

			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DELIVERYS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			
			oCellc = new Cell(new Phrase(sDelivery, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			
			oCellc = new Cell(new Phrase(sDrawingDocsapprvl, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DELAYMANFCLEARANCE, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			
			oCellc = new Cell(new Phrase(sDelayMnfclrnce, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_FORCEMEASURE, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sFMajeure, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

				
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_STORAGES, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sStorage, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			


			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_WARRANTYS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sWrnty, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

				
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_WAIT, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sWgtsndmsns, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TEST, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sTests, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

					
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_STANDARD, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sStandards, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

	
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LIMITEDLIABILITY, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sLimtOfLiability, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);


			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CONSEQ_LOSS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sConseqLosses, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);


			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_ARB, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sArbitration, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);


			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LNG, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sLang, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

				
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_GL, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sGovLaw, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

				
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_VQ, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			
			oCellc = new Cell(new Phrase(sViqty, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TRAN_TITL, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sTot, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CONFID, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			

			
			
			oCellc = new Cell(new Phrase(sCtas, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PASSBEFRISK, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			

			
			
			oCellc = new Cell(new Phrase(sPobar, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PASSBEFRISK2, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			

			
			
			oCellc = new Cell(new Phrase(sCdbd, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			


			
			

			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SUSPEN, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SUSPENSION, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(sSuspension, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TERMINATION, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			

			oCellc = new Cell(new Phrase(sTermination, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);


			
			



			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_BANKRUPTCYS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sBankrtcy, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			

			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_ACCEPTANCES, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sAcceptance, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			

			
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LIMITS, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sLos, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			


			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CHSCOPE, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			
			
			oCellc = new Cell(new Phrase(sCis, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CHCOMM, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);						
			
			oCellc = new Cell(new Phrase(sCfc, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);				
			
			oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_GENERAL, oTableSmallBoldText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oCellc = new Cell(new Phrase(sGeneral, oTableSmallText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
			
			oParagraph = new Paragraph();
			oParagraph.add(oTable);
			
			oDocument.add(oParagraph);

			// ------------------------------------------------------- Final Page : Notes Standard T & C : START -------------------------------------
		
			oDocument.close();
			
		} finally{
			
		}
		
		return oByteArrayOutputStream;
	}
	
	
	private Paragraph addCustomerInformation(NewEnquiryDto oNewEnquiryDto) throws DocumentException {
		
		Paragraph oParagraph = new Paragraph();
		PdfPTable oMainTable =null;
		PdfPCell oCell = null;
		PdfPCell oFirstTableCell =null;
		PdfPTable oFirstTable =null;

		PdfPCell oSecondTableCell =null;
		PdfPTable oSecondTable =null;
		
		if(oNewEnquiryDto!=null)
		{
			oMainTable = new PdfPTable(2);
	        oMainTable.setWidthPercentage(95f); 
	        oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	   
	        oFirstTableCell= new PdfPCell();
	        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
	        oFirstTable= new PdfPTable(2);
	   
	        oFirstTable.setWidthPercentage(85f);
	        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
	        if(oNewEnquiryDto.getCustomerName()!="") {
	        	insertPdfPTableCell(oFirstTable, QuotationConstants.QUOATION_CUST_NAME, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        	insertPdfPTableCell(oFirstTable, oNewEnquiryDto.getCustomerName(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
	        }
	        if(oNewEnquiryDto.getConcernedPerson()!="") {
	        	insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_CUST_CONCERNED_PERSON, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        	insertPdfPTableCell(oFirstTable, oNewEnquiryDto.getConcernedPerson(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
	        }
	        if(oNewEnquiryDto.getRevision()!="") {
	        	insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_REVISION.toLowerCase(), Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        	insertPdfPTableCell(oFirstTable, oNewEnquiryDto.getRevision(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
	        }
	        
	        oFirstTableCell.addElement(oFirstTable);
	        oMainTable.addCell(oFirstTableCell);
	        
	        // --------------------------------------------------------------------------------------------------------------------
	        
	        oSecondTableCell = new PdfPCell();
	        oSecondTableCell.setBorder(Rectangle.NO_BORDER);

	        oSecondTable = new PdfPTable(2);
	        oSecondTable.setWidthPercentage(85f);
	        oSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        
	        if(oNewEnquiryDto.getCustomerEnqReference() != "") {
	        	insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_CUST_ENQ_REFERENCE, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        	insertPdfPTableCell(oSecondTable, oNewEnquiryDto.getCustomerEnqReference(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
	        }
	        if(oNewEnquiryDto.getReceiptDate() != "") {
	        	insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_ENQ_RECEIPT_DATE, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        	insertPdfPTableCell(oSecondTable, oNewEnquiryDto.getReceiptDate(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
	        }
	        
	        oSecondTableCell.addElement(oSecondTable);
	        oMainTable.addCell(oSecondTableCell);
	        
	        // --------------------------------------------------------------------------------------------------------------------
	        
	        oParagraph.add(oMainTable);
		}
		
		return oParagraph;
	}
	
	
	private Table addCommercialOffer(NewEnquiryDto enquiryDto) throws Exception {
		
		BaseFont baseFont = BaseFont.createFont(PropertyUtility.getKeyValue("QUOTATIONLT_BASEFONT_LOC")+"arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		Font oCurrencyBoldFont = new Font(baseFont, 8, Font.BOLD);
		Font oCurrencyNormalFont = new Font(baseFont, 8, Font.NORMAL);
		
		Cell oCell = null;
		Table oTable = new Table(9);
		oTable.setWidth(100);
		oTable.setBorder(0);
		oTable.setSpacing((float).75);
		oTable.setPadding((float).75);
		oTable.setBorder(0);
		oTable.setBackgroundColor(new Color(255,255,255));
		
		Double quantity=new Double(0);
		BigDecimal totalprice=new BigDecimal("0");
		DecimalFormat oDecimalFormat = new DecimalFormat("###,###");
		DecimalFormat oDecimalFormat1 = new DecimalFormat("#0");
		
		NewRatingDto oRatingDto =null;
		TechnicalOfferDto oTechnicalOfferDto =null;
		
		ArrayList<NewRatingDto> alRatings =null;
		ArrayList alAddOnList = null;
		AddOnDto oAddOnDto =null;
		
		if(enquiryDto.getNewRatingsList() != null)
		{
			alRatings = enquiryDto.getNewRatingsList();
			
			if(alRatings.size() > 0)
			{
				insertTableCell(oTable, QuotationConstants.QUOTATION_ITEMNO, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_TAGNUMBER, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_RATEDOUTPUT, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_POLE, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_VOLTAGE, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_FRAME, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_UNITPRICES, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_QUANTITY, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_TOTALPRICE, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				
				/* -----------------  Displaying the Values -------------------------- */
				
				for(int i = 0;i < alRatings.size();i++)
				{
					oRatingDto = (NewRatingDto)alRatings.get(i);
					
					insertTableCell(oTable, String.valueOf(i+1), Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, oRatingDto.getTagNumber(), Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, oRatingDto.getLtKWId() + " kW", Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, oRatingDto.getLtPoleId(), Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, oRatingDto.getLtVoltId(), Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, oRatingDto.getLtFrameId()+oRatingDto.getLtFrameSuffixId(), Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, "\u20B9 " + oRatingDto.getLtPricePerUnit(), Element.ALIGN_CENTER, 1, true, false, oCurrencyNormalFont);
					String qnt = String.valueOf(oRatingDto.getQuantity());
					insertTableCell(oTable, qnt, Element.ALIGN_CENTER, 1, true, false, oTableSmallText);
					insertTableCell(oTable, "\u20B9 " + oRatingDto.getLtTotalOrderPrice(), Element.ALIGN_CENTER, 1, true, false, oCurrencyNormalFont);
					
					String totalpriceMultiplier_rsm = oRatingDto.getLtTotalOrderPrice();
					if(totalpriceMultiplier_rsm!="" && totalpriceMultiplier_rsm != QuotationConstants.QUOTATION_STRING_ZERO)
				    {
				        if(totalpriceMultiplier_rsm.indexOf(",") != QuotationConstants.QUOTATION_LITERAL_MINUSONE) {
				        	String unitprice = totalpriceMultiplier_rsm.replaceAll(",", "");
				        	BigDecimal oBigDecimal = new BigDecimal(unitprice);
				        	totalprice = totalprice.add(oBigDecimal);
				        } else {
				        	 BigDecimal oBigDecimal = new BigDecimal(totalpriceMultiplier_rsm);
						     totalprice = totalprice.add(oBigDecimal);
				        }
				    }
					
					if (qnt != "" && qnt != QuotationConstants.QUOTATION_STRING_ZERO) {
						quantity = quantity + Double.parseDouble(String.valueOf(qnt));
					}
					
				}
				// Insert Empty Row.
				insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 9, true, false, oTableSmallText);
				
				// If Supervision Charges Apply - Then Add the Supervision Charges to "totalPrice".
				if(QuotationConstants.QUOTATION_YES.equalsIgnoreCase(enquiryDto.getSupervisionDetails())) {
					int iSuperviseManDays = Integer.parseInt(enquiryDto.getSuperviseNoOfManDays());
					long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
					BigDecimal oBigDecimal = new BigDecimal(supervisionCharges);
					totalprice = totalprice.add(oBigDecimal);
					
					// Display Supervision Charges Details.
					insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 4, true, false, oTableSmallText);
					insertTableCell(oTable, QuotationConstants.QUOTATIONLT_SUPERVISION_CHARGES, Element.ALIGN_RIGHT, 2, true, false, oTableSmallBoldText);
					insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
					insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
					insertTableCell(oTable, "\u20B9 " + CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges)), Element.ALIGN_CENTER, 1, true, false, oCurrencyBoldFont);
				}
				
				insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 5, true, false, oTableSmallText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_TOTAL, Element.ALIGN_RIGHT, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, QuotationConstants.QUOTATION_EMPTY, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, oDecimalFormat1.format(quantity), Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, "\u20B9 " + CurrencyConvertUtility.buildPriceFormat(totalprice.toString()), Element.ALIGN_CENTER, 1, true, false, oCurrencyBoldFont);
				
				insertTableCell(oTable, QuotationConstants.QUOTATION_AMOUNTINWORDS, Element.ALIGN_CENTER, 1, true, false, oTableSmallBoldText);
				insertTableCell(oTable, CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(totalprice)), Element.ALIGN_LEFT, 8, true, false, oTableSmallBoldText);
				
			}
			
			if(StringUtils.isNotBlank(enquiryDto.getCustomerDealerLink()) && QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(enquiryDto.getCustomerDealerLink())) {
				insertTableCell(oTable, " ", Element.ALIGN_CENTER, 9, true, false, oTableSmallBoldText);
				insertTableCell(oTable, " ", Element.ALIGN_CENTER, 9, true, false, oTableSmallBoldText);
				
				insertTableCell(oTable, QuotationConstants.QUOTATIONLT_CUSTOMER_DEALER_LINK, Element.ALIGN_LEFT, 9, true, false, oTableSmallBoldText);
			}
		}
		
		return oTable;
	}
	
	private Paragraph buildPaymentTermsDetails(NewEnquiryDto oNewEnquiryDto) throws DocumentException {
		Paragraph oParagraph = new Paragraph();
		
		// -------------------------------------------------------Payment Terms Table -----------------------------------------------------------
		
        PdfPTable oPayTermMainTable = new PdfPTable(3);
        oPayTermMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        oPayTermMainTable.setTotalWidth(500f);
        oPayTermMainTable.setLockedWidth(true);
        float[] widths = new float[] {100f, 300f, 100f };
        oPayTermMainTable.setWidths(widths);
        
        PdfPCell oPayTermsFirstCell = new PdfPCell();
        oPayTermsFirstCell.setBorder(Rectangle.BOX);
		PdfPTable oPayTermsFirstTable = new PdfPTable(1);
		oPayTermsFirstTable.setWidthPercentage(100f);
		oPayTermsFirstTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_PAY_TERMS, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) > 0) {
			insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_ADVANCE_PAYMENT1, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) > 0) {
			insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_ADVANCE_PAYMENT2, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) > 0) {
			insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_MAIN_PAYMENT, Element.ALIGN_LEFT, 1, true, true,  oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) > 0) {
			insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_RETENTION_PAYMENT1, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) > 0) {
			insertPdfPTableCell(oPayTermsFirstTable, QuotationConstants.QUOTATION_PDF_RETENTION_PAYMENT2, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		
		oPayTermsFirstCell.addElement(oPayTermsFirstTable);
		oPayTermMainTable.addCell(oPayTermsFirstCell);
		
		// ------------------
		
		PdfPCell oPayTermsSecondCell = new PdfPCell();
		oPayTermsSecondCell.setBorder(Rectangle.BOX);
		PdfPTable oPayTermsSecondTable = new PdfPTable(1);
		oPayTermsSecondTable.setWidthPercentage(100f);
		oPayTermsSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		String sPaymentDetailsValue = "";
		
		insertPdfPTableCell(oPayTermsSecondTable, QuotationConstants.QUOTATION_PDF_PAYMENT_DETAILS, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) > 0) {
			sPaymentDetailsValue = StringUtils.isNotBlank(buildPaymentDetailsString(oNewEnquiryDto, 1)) ? buildPaymentDetailsString(oNewEnquiryDto, 1) : "NA";
			insertPdfPTableCell(oPayTermsSecondTable, sPaymentDetailsValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) > 0) {
			sPaymentDetailsValue = StringUtils.isNotBlank(buildPaymentDetailsString(oNewEnquiryDto, 2)) ? buildPaymentDetailsString(oNewEnquiryDto, 2) : "NA";
			insertPdfPTableCell(oPayTermsSecondTable, sPaymentDetailsValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) > 0) {
			sPaymentDetailsValue = StringUtils.isNotBlank(buildPaymentDetailsString(oNewEnquiryDto, 3)) ? buildPaymentDetailsString(oNewEnquiryDto, 3) : "NA";
			insertPdfPTableCell(oPayTermsSecondTable, sPaymentDetailsValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) > 0) {
			sPaymentDetailsValue = StringUtils.isNotBlank(buildPaymentDetailsString(oNewEnquiryDto, 4)) ? buildPaymentDetailsString(oNewEnquiryDto, 4) : "NA";
			insertPdfPTableCell(oPayTermsSecondTable, sPaymentDetailsValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) > 0) {
			sPaymentDetailsValue = StringUtils.isNotBlank(buildPaymentDetailsString(oNewEnquiryDto, 5)) ? buildPaymentDetailsString(oNewEnquiryDto, 5) : "NA";
			insertPdfPTableCell(oPayTermsSecondTable, sPaymentDetailsValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		
		oPayTermsSecondCell.addElement(oPayTermsSecondTable);
		oPayTermMainTable.addCell(oPayTermsSecondCell);
		
		// ------------------
		
		PdfPCell oPayTermsThirdCell = new PdfPCell();
		oPayTermsThirdCell.setBorder(Rectangle.BOX);
		PdfPTable oPayTermsThirdTable = new PdfPTable(1);
		oPayTermsThirdTable.setWidthPercentage(100f);
		oPayTermsThirdTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		String sInstrumentValue = "";
		
		insertPdfPTableCell(oPayTermsThirdTable, QuotationConstants.QUOTATION_PDF_INSTRUMENT, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) > 0) {
			sInstrumentValue = StringUtils.isNotBlank(oNewEnquiryDto.getPt1Instrument()) ? oNewEnquiryDto.getPt1Instrument() : "NA";
			insertPdfPTableCell(oPayTermsThirdTable, sInstrumentValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) > 0) {
			sInstrumentValue = StringUtils.isNotBlank(oNewEnquiryDto.getPt2Instrument()) ? oNewEnquiryDto.getPt2Instrument() : "NA";
			insertPdfPTableCell(oPayTermsThirdTable, sInstrumentValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) > 0) {
			sInstrumentValue = StringUtils.isNotBlank(oNewEnquiryDto.getPt3Instrument()) ? oNewEnquiryDto.getPt3Instrument() : "NA";
			insertPdfPTableCell(oPayTermsThirdTable, sInstrumentValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) > 0) {
			sInstrumentValue = StringUtils.isNotBlank(oNewEnquiryDto.getPt4Instrument()) ? oNewEnquiryDto.getPt4Instrument() : "NA";
			insertPdfPTableCell(oPayTermsThirdTable, sInstrumentValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		if(StringUtils.isNotBlank(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) > 0) {
			sInstrumentValue = StringUtils.isNotBlank(oNewEnquiryDto.getPt5Instrument()) ? oNewEnquiryDto.getPt5Instrument() : "NA";
			insertPdfPTableCell(oPayTermsThirdTable, sInstrumentValue, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		}
		
		oPayTermsThirdCell.addElement(oPayTermsThirdTable);
		oPayTermMainTable.addCell(oPayTermsThirdCell);
      
        oParagraph.add(oPayTermMainTable);
        
        // -------------------------------------------------------Payment Terms Table -----------------------------------------------------------
        
		return oParagraph;
	}
	
	private Paragraph buildCommentsDeviationsContent(NewEnquiryDto oNewEnquiryDto) throws DocumentException {
		Paragraph oParagraph = new Paragraph();
		
		// -------------------------------------------------------Comments DEviations Table -----------------------------------------------------
		
        PdfPTable oCDMainTable = new PdfPTable(1);
        oCDMainTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        oCDMainTable.setTotalWidth(1100f);
        //oCDMainTable.setLockedWidth(true);
        //float[] widths = new float[] {500f};
        //oCDMainTable.setWidths(widths);
        
        insertPdfPTableCell(oCDMainTable, " ", Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
        
        PdfPCell oCDContentCell = new PdfPCell();
        oCDContentCell.setBorder(Rectangle.BOX);
		PdfPTable oCDContentTable = new PdfPTable(1);
		oCDContentTable.setWidthPercentage(100f);
		oCDContentTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		insertPdfPTableCell(oCDContentTable, oNewEnquiryDto.getCommentsDeviations(), Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
        
		oCDContentCell.addElement(oCDContentTable);
		oCDMainTable.addCell(oCDContentCell);
		
		oParagraph.add(oCDMainTable);
        // -------------------------------------------------------Comments DEviations Table -----------------------------------------------------
        
        return oParagraph;
	}
	
	/*private Paragraph addTotalDetails(NewEnquiryDto oNewEnquiryDto) throws DocumentException {
		
		String sDateOfDispatch="";
		String sDateOfComm="";
		String sGST = "";
		
		Paragraph oParagraph = new Paragraph();
		
		PdfPTable oMainTable = new PdfPTable(2);
		oMainTable.setWidthPercentage(100f); 
		oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell oFirstTableCell = new PdfPCell();
		oFirstTableCell.setBorder(Rectangle.NO_BORDER);
		
		PdfPTable oFirstTable = new PdfPTable(2);
		oFirstTable.setWidthPercentage(95f);
		oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_DELIVERY_TERM, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		insertPdfPTableCell(oFirstTable, oNewEnquiryDto.getDeliveryTerm(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		
		insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_WARRANTYDATEDISP, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		if(StringUtils.isNotBlank(oNewEnquiryDto.getWarrantyDispatchDate())) {
        	sDateOfDispatch = oNewEnquiryDto.getWarrantyDispatchDate() + QuotationConstants.QUOTATION_MONTHS;
        }
		insertPdfPTableCell(oFirstTable, sDateOfDispatch, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		
		insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_GST, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		if(StringUtils.isNotBlank(oNewEnquiryDto.getGstValue())) {
        	sGST = oNewEnquiryDto.getGstValue() + " %";
        }
		insertPdfPTableCell(oFirstTable, sGST, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		
		insertPdfPTableCell(oFirstTable, QuotationConstants.QUOTATION_DELIVERY_LOT, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
		insertPdfPTableCell(oFirstTable, oNewEnquiryDto.getDeliveryInFull(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
		
        oFirstTableCell.addElement(oFirstTable);
        oMainTable.addCell(oFirstTableCell);
        
        PdfPCell oSecondTableCell = new PdfPCell();
        oSecondTableCell.setBorder(Rectangle.NO_BORDER);
        
        PdfPTable oSecondTable = new PdfPTable(2);
        oSecondTable.setWidthPercentage(95f);
        oSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_ORD_COMPLETE_LEAD_TIME, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
        insertPdfPTableCell(oSecondTable, oNewEnquiryDto.getOrderCompletionLeadTime(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
        
        insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_WARRANTYDATECOMM, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
        if(StringUtils.isNotBlank(oNewEnquiryDto.getWarrantyCommissioningDate())) {
        	sDateOfComm = oNewEnquiryDto.getWarrantyCommissioningDate()+" Months";
        }
        insertPdfPTableCell(oSecondTable, sDateOfComm, Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
        
        insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_PACKAGING, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
        insertPdfPTableCell(oSecondTable, oNewEnquiryDto.getPackaging(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
        
        insertPdfPTableCell(oSecondTable, QuotationConstants.QUOTATION_LIQUIDATED_DAMAGES, Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
        insertPdfPTableCell(oSecondTable, oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay(), Element.ALIGN_LEFT, 1, true, true, oTableSmallText);
        
        oSecondTableCell.addElement(oSecondTable);
        oMainTable.addCell(oSecondTableCell);
        
        oParagraph.add(oMainTable);
        
        // --------------------------------------------------------------------------------------------------------------------------------------
		return oParagraph;
	}*/
	
	private Paragraph addTechnicalRatingContent(NewRatingDto oNewRatingDto, int iRatingIndex) throws Exception {
		
		NewTechnicalOfferDto oNewTechnicalOfferDto = oNewRatingDto.getNewTechnicalOfferDto();
		
		Paragraph oParagraph = new Paragraph();
		
		PdfPTable oMainRatingTable = null, ocellElement = null;
		PdfPCell oParamsCell = null, oTableCell = null, oCell = null;
		Chunk oChunk = null;
		StringBuilder strChunk = new StringBuilder();
		
		// ------------------------------------------------------- Technical Rating Heading -----------------------------------------------------------
			oMainRatingTable = new PdfPTable(1);
			oMainRatingTable.setHorizontalAlignment(Element.ALIGN_CENTER);
			oMainRatingTable.setWidthPercentage(100f);
		
			PdfPCell oHeadingCell = new PdfPCell();
			oHeadingCell.setBorder(Rectangle.NO_BORDER);
			PdfPTable oHeadingTable = new PdfPTable(1);
			oHeadingTable.setWidthPercentage(100f);
			oHeadingTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			insertPdfPTableCell(oHeadingTable, QuotationConstants.QUOTATION_TECHREQ + QuotationConstants.QUOTATION_RATING + iRatingIndex, Element.ALIGN_LEFT, 1, false, false, oTableBlueText);
			
			oHeadingCell.addElement(oHeadingTable);
			oMainRatingTable.addCell(oHeadingCell);
			oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_20);
			
			oParagraph.add(oMainRatingTable);
		// ------------------------------------------------------- Technical Rating Heading -----------------------------------------------------------
			
		// ------------------------------------------------------- Technical Model Number Table -------------------------------------------------------
			oParagraph.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_50);
			
			oMainRatingTable = new PdfPTable(1);
			oMainRatingTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			oMainRatingTable.setWidthPercentage(100f);
			
			PdfPCell oModelNumberCell = new PdfPCell();
			oModelNumberCell.setBorder(Rectangle.NO_BORDER);
			PdfPTable oModelNumberTable = new PdfPTable(1);
			oModelNumberTable.setWidthPercentage(100f);
			oModelNumberTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			insertPdfPTableCell(oModelNumberTable, QuotationConstants.QUOTATION_PDF_MODEL_NUM +" "+ oNewTechnicalOfferDto.getModelNumber(), Element.ALIGN_LEFT, 1, false, false, oTableSmallBoldText);
			insertPdfPTableCell(oModelNumberTable, QuotationConstants.QUOTATION_DOTED_LINE_TECH, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
			oModelNumberCell.addElement(oModelNumberTable);
			oMainRatingTable.addCell(oModelNumberCell);
			oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_5);
			oParagraph.add(oMainRatingTable);
		// ------------------------------------------------------- Technical Model Number Table -------------------------------------------------------
			
		// ------------------------------------------------------- Technical Rating Params Table ------------------------------------------------------
			oMainRatingTable = new PdfPTable(7);
			oMainRatingTable.setHorizontalAlignment(Element.ALIGN_CENTER);
			oMainRatingTable.setTotalWidth(515f);
			oMainRatingTable.setLockedWidth(true);
	        float[] widths = new float[] {50f, 130f, 25f, 100f, 75f, 85f, 50f};
	        oMainRatingTable.setWidths(widths);
			
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oFirstParamsTable = new PdfPTable(2);
	        oFirstParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oFirstParamsTable.setTotalWidth(50f);
	        oFirstParamsTable.setLockedWidth(true);
	        float[] firstCellwidths = new float[] {25f, 25f};
	        oFirstParamsTable.setWidths(firstCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 1
	        insertPdfPTableCell(ocellElement, "U", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "(V)", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getVoltage(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFirstParamsTable.addCell(oTableCell);
			
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 2
	        insertPdfPTableCell(ocellElement, "f", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[Hz]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getFrequency(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFirstParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFirstParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oSecondParamsTable = new PdfPTable(5);
	        oSecondParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oSecondParamsTable.setTotalWidth(130f);
	        oSecondParamsTable.setLockedWidth(true);
	        float[] secondCellwidths = new float[] {25f, 25f, 25f, 30f, 25f};
	        oSecondParamsTable.setWidths(secondCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 3
	        insertPdfPTableCell(ocellElement, "P", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[kW]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPowerRating_KW(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 4
	        insertPdfPTableCell(ocellElement, "P", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[hp]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPowerRating_HP(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 5
	        insertPdfPTableCell(ocellElement, "I", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[A]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getCurrent_A(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 6
	        insertPdfPTableCell(ocellElement, "n", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[RPM]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getMaxSpeed(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 7
	        insertPdfPTableCell(ocellElement, "T", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[Nm]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getTorque(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oSecondParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oThirdParamsTable = new PdfPTable(1);
	        oThirdParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oThirdParamsTable.setTotalWidth(25f);
	        oThirdParamsTable.setLockedWidth(true);
	        float[] thirdCellwidths = new float[] {25f};
	        oThirdParamsTable.setWidths(thirdCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 8
	        insertPdfPTableCell(ocellElement, "IE", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Class", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getEfficiencyClass(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oThirdParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oThirdParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oFourthParamsTable = new PdfPTable(4);
	        oFourthParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oFourthParamsTable.setTotalWidth(100f);
	        oFourthParamsTable.setLockedWidth(true);
	        float[] fourthCellwidths = new float[] {25f, 25f, 25f, 25f};
	        oFourthParamsTable.setWidths(fourthCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setColspan(4);
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "% EFF at --- load", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 9
	        insertPdfPTableCell(ocellElement, "5/4FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getEfficiency_100Above(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 10
	        insertPdfPTableCell(ocellElement, "FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getEfficiency_100(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 11
	        insertPdfPTableCell(ocellElement, "3/4FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getEfficiency_75(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 12
	        insertPdfPTableCell(ocellElement, "1/2FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getEfficiency_50(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFourthParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oFifthParamsTable = new PdfPTable(3);
	        oFifthParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oFifthParamsTable.setTotalWidth(75f);
	        oFifthParamsTable.setLockedWidth(true);
	        float[] fifthCellwidths = new float[] {25f, 25f, 25f};
	        oFifthParamsTable.setWidths(fifthCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setColspan(3);
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "PF at --- load", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFifthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 13
	        insertPdfPTableCell(ocellElement, "FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPowerFactor_100(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFifthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 14
	        insertPdfPTableCell(ocellElement, "3/4FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPowerFactor_75(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFifthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 15
	        insertPdfPTableCell(ocellElement, "1/2FL", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPowerFactor_50(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFifthParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFifthParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oSixthParamsTable = new PdfPTable(3);
	        oSixthParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oSixthParamsTable.setTotalWidth(85f);
	        oSixthParamsTable.setLockedWidth(true);
	        float[] sixthCellwidths = new float[] {25f, 30f, 30f};
	        oSixthParamsTable.setWidths(sixthCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);
	        strChunk.append(new Chunk("I").toString());
	        oChunk = new Chunk("A");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());
	        strChunk.append(new Chunk("/I").toString());
	        oChunk = new Chunk("N");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());	// 16
	        insertPdfPTableCell(ocellElement, strChunk.toString(), Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[pu]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getStartingCurrent(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSixthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);
	        strChunk = new StringBuilder();
	        strChunk.append(new Chunk("T").toString());
	        oChunk = new Chunk("A");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());
	        strChunk.append(new Chunk("/T").toString());
	        oChunk = new Chunk("N");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());	// 17
	        insertPdfPTableCell(ocellElement, strChunk.toString(), Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[pu]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getStarting_torque(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSixthParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);
	        strChunk = new StringBuilder();
	        strChunk.append(new Chunk("T").toString());
	        oChunk = new Chunk("K");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());
	        strChunk.append(new Chunk("/T").toString());
	        oChunk = new Chunk("N");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());	// 18
	        insertPdfPTableCell(ocellElement, strChunk.toString(), Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[pu]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getPullout_torque(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSixthParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oSixthParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.BOX);
	        PdfPTable oSeventhParamsTable = new PdfPTable(2);
	        oSeventhParamsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
	        oSeventhParamsTable.setTotalWidth(50f);
	        oSeventhParamsTable.setLockedWidth(true);
	        float[] seventhCellwidths = new float[] {25f, 25f};
	        oSeventhParamsTable.setWidths(seventhCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 19
	        insertPdfPTableCell(ocellElement, "RA", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[A]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getRaValue(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSeventhParamsTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_CENTER);
	        ocellElement.setWidthPercentage(100f);	// 20
	        insertPdfPTableCell(ocellElement, "RV", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "[V]", Element.ALIGN_CENTER, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, oNewTechnicalOfferDto.getRvValue(), Element.ALIGN_CENTER, 1, true, true, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_CENTER, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSeventhParamsTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oSeventhParamsTable);
	        oMainRatingTable.addCell(oParamsCell);
	        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_TEN);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParagraph.add(oMainRatingTable);
		// ------------------------------------------------------- Technical Rating Params Table ------------------------------------------------------
	        
	        
	    // ------------------------------------------------------- Technical AddOn Params Table -------------------------------------------------------
	        oMainRatingTable = new PdfPTable(3);
			oMainRatingTable.setHorizontalAlignment(Element.ALIGN_CENTER);
			oMainRatingTable.setTotalWidth(500f);
			oMainRatingTable.setLockedWidth(true);
	        float[] widths1 = new float[] {225f, 50f, 225f};
	        oMainRatingTable.setWidths(widths1);
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oFirstAddOnTable = new PdfPTable(2);
	        oFirstAddOnTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oFirstAddOnTable.setTotalWidth(225f);
	        oFirstAddOnTable.setLockedWidth(true);
	        float[] firstCellwidths1 = new float[] {125f, 100f};
	        oFirstAddOnTable.setWidths(firstCellwidths1);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Motor Type", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);	
	        insertPdfPTableCell(ocellElement, "Enclosure", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Frame Material", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Frame Size", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Duty", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Voltage variation *", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Frequency variation *", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Combined variation *", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Design", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Service factor", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Insulation Class / Temp. Rise Class", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Ambient temperature (+/-)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Temperature rise (by resistance)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Altitude above sea level", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Area classification", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        
	        oCell = new PdfPCell(new Phrase("Zone classification", oTableMiniBoldText));
	        oCell.setBorder(0);
	        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oCell.setPaddingLeft(20f);
	        ocellElement.addCell(oCell);
	        
	        oCell = new PdfPCell(new Phrase("Gas group", oTableMiniBoldText));
	        oCell.setBorder(0);
	        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oCell.setPaddingLeft(20f);
	        ocellElement.addCell(oCell);
	        
	        oCell = new PdfPCell(new Phrase("Temperature class", oTableMiniBoldText));
	        oCell.setBorder(0);
	        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oCell.setPaddingLeft(20f);
	        ocellElement.addCell(oCell);
	        
	        insertPdfPTableCell(ocellElement, "Rotor Type", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Bearing Type (DE / NDE)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "DE / NDE Size", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Lubrication method", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Type of grease", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Motor weight - approx.", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Gross weight - approx.", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFirstAddOnTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        // 21
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getProductGroup() + " " + oNewTechnicalOfferDto.getProductLine(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        // 22
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getEnclosureType(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getFrameMaterial(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getFrameType()+oNewTechnicalOfferDto.getFrameSuffix(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getDuty(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". +" + oNewTechnicalOfferDto.getVoltageAddVariation() + "%    " + "-" + oNewTechnicalOfferDto.getVoltageRemoveVariation() + "% ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". +" + oNewTechnicalOfferDto.getFrequencyAddVariation() + "%    " + "-" + oNewTechnicalOfferDto.getFrequencyRemoveVariation() + "% ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getCombinedVariation() + "% ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getDesign(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getServiceFactor(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getInsulationClass(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getAmbientAddTemp() + " Deg C    " + ". " + oNewTechnicalOfferDto.getAmbientRemoveTemp() + " Deg C    " , Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getTempRise() + " K", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getAltitude() + " meter", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getHazArea(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getZoneClassification(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getGasGroup(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getTemperatureClass(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getRotortype(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getDeBearingType() + " / " + oNewTechnicalOfferDto.getOdeBearingType(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        
	        if(StringUtils.isBlank(oNewTechnicalOfferDto.getDeBearingSize()) || StringUtils.isBlank(oNewTechnicalOfferDto.getOdeBearingSize())) {
	        	insertPdfPTableCell(ocellElement, ". ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        } else {
	        	insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getDeBearingSize() + " / " + oNewTechnicalOfferDto.getOdeBearingSize(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        }
	        
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getLubMethod(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getTypeOfGrease(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        
	        if(StringUtils.isBlank(oNewTechnicalOfferDto.getMotorWeight()) || StringUtils.isBlank(oNewTechnicalOfferDto.getGrossWeight()) ) {
	        	insertPdfPTableCell(ocellElement, ". ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
		        // 45
		        insertPdfPTableCell(ocellElement, ". ", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        } else {
	        	insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getMotorWeight() + " Kg", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
		        // 45
		        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getGrossWeight() + " Kg", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        }
	        
	        oTableCell.addElement(ocellElement);
	        oFirstAddOnTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFirstAddOnTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setRowspan(25);
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oSecondAddOnTable = new PdfPTable(1);
	        oSecondAddOnTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oSecondAddOnTable.setTotalWidth(50f);
	        oSecondAddOnTable.setLockedWidth(true);
	        float[] secondCellwidths1 = new float[] {50f};
	        oSecondAddOnTable.setWidths(secondCellwidths1);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oSecondAddOnTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oSecondAddOnTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oThirdAddOnTable = new PdfPTable(2);
	        oThirdAddOnTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oThirdAddOnTable.setTotalWidth(225f);
	        oThirdAddOnTable.setLockedWidth(true);
	        float[] thirdCellwidths1 = new float[] {125f, 100f};
	        oThirdAddOnTable.setWidths(thirdCellwidths1);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Stator Connection", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Rotor Connection", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "CDF", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "No. of starts", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Degree of Protection (IP)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Mounting type", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Cooling method", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Motor inertia", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Load inertia", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Vibration level", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Noise level (1meter distance from motor)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "No. of starts hot/cold/Equally spread", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Starting method", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Type of coupling", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "LR withstand time (hot/cold)", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Direction of rotation", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Standard rotation", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Paint shade", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "RTD", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "BTD", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Thermister", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Space heater", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Terminal Box position", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Maximum cable size/conduit size", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Auxiliary Terminal Box", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oThirdAddOnTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        // 46
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getStatorConnection(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        // 47
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getRotorConnection(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getCdf(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getNoOfStarts_HR(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getIpCode(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getMotorOrientation(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getCoolingMethod(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getMotorInertia() + " kgm2", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getLoadInertia() + " kgm2", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getVibration() + " mm/s", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getNoise() + " dB(A)", Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getNoOfStarts(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getStartingType(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewRatingDto.getLtMethodOfCoupling(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getLrWithstandTime(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getDirectionOfRotation(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getRotation(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + (StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeId()) ? oNewRatingDto.getLtPaintShadeId() : "NA"), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + (StringUtils.isNotBlank(oNewRatingDto.getLtRTDId()) ? oNewRatingDto.getLtRTDId() : "NA"), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + (StringUtils.isNotBlank(oNewRatingDto.getLtBTDId()) ? oNewRatingDto.getLtBTDId() : "NA"), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + (StringUtils.isNotBlank(oNewRatingDto.getLtThermisterId()) ? oNewRatingDto.getLtThermisterId() : "NA"), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + (StringUtils.isNotBlank(oNewRatingDto.getLtSpaceHeaterId()) ? oNewRatingDto.getLtSpaceHeaterId() : "NA"), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getTbPosition(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, ". " + oNewTechnicalOfferDto.getCableSize(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        // 69
	        insertPdfPTableCell(ocellElement, ". " + oNewRatingDto.getLtAuxTerminalBoxId(), Element.ALIGN_LEFT, 1, true, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oThirdAddOnTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oThirdAddOnTable);
	        oMainRatingTable.addCell(oParamsCell);
	        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_TEN);
	        // -------------------------------------------------------------------------------------------------
	        
	        oParagraph.add(oMainRatingTable);
	    // ------------------------------------------------------- Technical AddOn Params Table -------------------------------------------------------
	    
	    // ------------------------------------------------------- Technical AddOn Remarks Table ------------------------------------------------------
	        
	        
	        oMainRatingTable = new PdfPTable(1);
			oMainRatingTable.setHorizontalAlignment(Element.ALIGN_LEFT);
			oMainRatingTable.setTotalWidth(500f);
			oMainRatingTable.setLockedWidth(true);
	        widths1 = new float[] {500f};
	        oMainRatingTable.setWidths(widths1);
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oFirstRemarksTable = new PdfPTable(1);
	        oFirstRemarksTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oFirstRemarksTable.setTotalWidth(500f);
	        oFirstRemarksTable.setLockedWidth(true);
	        float[] firstRemarksCellwidths = new float[] {500f};
	        oFirstRemarksTable.setWidths(firstRemarksCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        
	        insertPdfPTableCell(ocellElement, "NA - Not Applicable", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        
	        strChunk = new StringBuilder();
	        strChunk.append(new Chunk("I").toString());
	        oChunk = new Chunk("A");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk);
	        strChunk.append(new Chunk("/I").toString());
	        oChunk = new Chunk("N");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk);
	        strChunk.append(new Chunk(" - Locked Rotor Current / Rated Current").toString());
	        insertPdfPTableCell(ocellElement, strChunk.toString(), Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        
	        strChunk = new StringBuilder();
	        strChunk.append(new Chunk("T").toString());
	        oChunk = new Chunk("A");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());
	        strChunk.append(new Chunk("/T").toString());
	        oChunk = new Chunk("N");
	        oChunk.setTextRise(-5f);
	        oChunk.setFont(oTableSubscriptText);
	        strChunk.append(oChunk.toString());
	        strChunk.append(new Chunk(" - Locked Rotor Torque / Rated Torque").toString());
	        insertPdfPTableCell(ocellElement, strChunk.toString(), Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_DOTED_LINE_TECH, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        
	        oTableCell.addElement(ocellElement);
	        oFirstRemarksTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFirstRemarksTable);
	        oMainRatingTable.addCell(oParamsCell);
	        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_40);
	     
	     // -------------------------------------------------------------------------------------------------
	        
	        ArrayList<KeyValueVo> alRemarksList = oNewRatingDto.getAlRemarksList();
	        
	        if(alRemarksList.size() > 0) {
	        	oParamsCell = new PdfPCell();
		        oParamsCell.setBorder(Rectangle.NO_BORDER);
		        PdfPTable oSecondRemarksTable = new PdfPTable(1);
		        oSecondRemarksTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		        oSecondRemarksTable.setTotalWidth(500f);
		        oSecondRemarksTable.setLockedWidth(true);
		        float[] secondRemarksCellwidths = new float[] {500f};
		        oSecondRemarksTable.setWidths(secondRemarksCellwidths);
		        
	        	oTableCell = new PdfPCell();
		        oTableCell.setBorder(Rectangle.NO_BORDER);
		        ocellElement = new PdfPTable(1);
		        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
		        ocellElement.setWidthPercentage(100f);
		        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_PDF_REMARKS, Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
		        
		        int iRemarkIdx = 1;
		        for(KeyValueVo oRemark : alRemarksList) {
		        	insertPdfPTableCell(ocellElement, iRemarkIdx + ") " + oRemark.getValue(), Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
		        	iRemarkIdx = iRemarkIdx + 1;
		        }
		        // TO DO :: Get Remarks List and Iterate the List and display the Remarks. 
		        //	  insertPdfPTableCell(ocellElement, "1) ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
			    //    insertPdfPTableCell(ocellElement, "2) ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
			    //    insertPdfPTableCell(ocellElement, "3) ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
			    // TO DO :: Get Remarks List and Iterate the List and display the Remarks. 
			    
			    oTableCell.addElement(ocellElement);
			    oSecondRemarksTable.addCell(oTableCell);
			    oParamsCell.addElement(oSecondRemarksTable);
		        oMainRatingTable.addCell(oParamsCell);
		        
		        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_15);
	        }
	     
	     // -------------------------------------------------------------------------------------------------
	        
	        if(oNewRatingDto.getLtAdditionalComments().length() > 0) {
	        	oParamsCell = new PdfPCell();
		        oParamsCell.setBorder(Rectangle.NO_BORDER);
		        PdfPTable oSecondAddlCommentsTable = new PdfPTable(1);
		        oSecondAddlCommentsTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		        oSecondAddlCommentsTable.setTotalWidth(500f);
		        oSecondAddlCommentsTable.setLockedWidth(true);
		        float[] thirdRemarksCellwidths = new float[] {500f};
		        oSecondAddlCommentsTable.setWidths(thirdRemarksCellwidths);
		        /*
		        if(alRemarksList.size() > 0) {
		        	oTableCell = new PdfPCell();
			        oTableCell.setColspan(7);
			        oTableCell.setBorder(Rectangle.NO_BORDER);
			        ocellElement = new PdfPTable(1);
			        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
			        ocellElement.setWidthPercentage(100f);
			        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_DOTED_LINE_TECH, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
			        oTableCell.addElement(ocellElement);
			        oSecondAddlCommentsTable.addCell(oTableCell);
		        }
		        */
		        oTableCell = new PdfPCell();
		        oTableCell.setBorder(Rectangle.NO_BORDER);
		        ocellElement = new PdfPTable(1);
		        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
		        ocellElement.setWidthPercentage(100f);
		        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_PDF_ADDL_COMMENTS, Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
		        
		        insertPdfPTableCell(ocellElement, oNewRatingDto.getLtAdditionalComments(), Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
		        
		        oTableCell.addElement(ocellElement);
		        oSecondAddlCommentsTable.addCell(oTableCell);
			    oParamsCell.addElement(oSecondAddlCommentsTable);
		        oMainRatingTable.addCell(oParamsCell);
		        
		        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_15);
	        }
	        
	     // -------------------------------------------------------------------------------------------------
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oThirdRemarksTable = new PdfPTable(1);
	        oThirdRemarksTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oThirdRemarksTable.setTotalWidth(500f);
	        oThirdRemarksTable.setLockedWidth(true);
	        float[] thirdRemarksCellwidths = new float[] {500f};
	        oThirdRemarksTable.setWidths(thirdRemarksCellwidths);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_PDF_NOTES, Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        
	        insertPdfPTableCell(ocellElement, "All performance values at rated voltage and frequency. ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
	        insertPdfPTableCell(ocellElement, "Voltage, Frequency and combine variation are as per IEC60034-1 ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
	        insertPdfPTableCell(ocellElement, "Technical data are subject to change. There may be discrepancies between calculated and name plate values. ", Element.ALIGN_LEFT, 1, false, false, oTableMiniText);
	        
	        oTableCell.addElement(ocellElement);
	        oThirdRemarksTable.addCell(oTableCell);
		        
		    oParamsCell.addElement(oThirdRemarksTable);
	        oMainRatingTable.addCell(oParamsCell);
	        
	        oMainRatingTable.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_5);
	        
	        oParamsCell = new PdfPCell();
	        oParamsCell.setBorder(Rectangle.NO_BORDER);
	        PdfPTable oFourthRemarksTable = new PdfPTable(7);
	        oFourthRemarksTable.setHorizontalAlignment(Element.ALIGN_LEFT);
	        oFourthRemarksTable.setTotalWidth(490f);
	        oFourthRemarksTable.setLockedWidth(true);
	        float[] fourthRemarksCellwidths = new float[] {80f, 80f, 80f, 80f, 80f, 80f, 80f};
	        oFourthRemarksTable.setWidths(fourthRemarksCellwidths);
	        // -------------------------------------------------------------------------------------------------
	        oTableCell = new PdfPCell();
	        oTableCell.setColspan(7);
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_DOTED_LINE_TECH, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Efficiency", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "Standards", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Europe", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "IEC: 60034-30", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "China", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "GB", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "India", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "IS: 12615-2018", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Aus/Nz", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Brazil", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, " ", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, "Global IEC", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        insertPdfPTableCell(ocellElement, "IEC: 60034-30", Element.ALIGN_LEFT, 1, false, false, oTableMiniBoldText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oTableCell = new PdfPCell();
	        oTableCell.setColspan(7);
	        oTableCell.setBorder(Rectangle.NO_BORDER);
	        ocellElement = new PdfPTable(1);
	        ocellElement.setHorizontalAlignment(Element.ALIGN_LEFT);
	        ocellElement.setWidthPercentage(100f);
	        insertPdfPTableCell(ocellElement, QuotationConstants.QUOTATION_DOTED_LINE_TECH, Element.ALIGN_LEFT, 1, false, false, oTableSmallText);
	        oTableCell.addElement(ocellElement);
	        oFourthRemarksTable.addCell(oTableCell);
	        
	        oParamsCell.addElement(oFourthRemarksTable);
	        oMainRatingTable.addCell(oParamsCell);
	        // -------------------------------------------------------------------------------------------------
	        
	        
	        oParagraph.add(oMainRatingTable);
	    // ------------------------------------------------------- Technical AddOn Remarks Table ------------------------------------------------------
	    
	    oParagraph.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_20);
	        
		return oParagraph;
	}
	
	private Document buildStandardNotes(Document oDocument) throws Exception {
		
		Table oTable =null;
		Cell oCellc =null;
		Paragraph oParagraph =null;
		
		//String sDocSubmission="", sPackForward="", oTaxDuties="", sSpecialNote="";
        String sTerm1="", sTerm2="", sTerm3="", sTerm4="", sTerm5="", sTerm6="", sTerm7="", sTerm8="", sTerm9="", sTerm10="";
		String sTerm11="", sTerm12="", sTerm13="", sTerm14="", sDefintions ="", sValidity ="", sScope ="", sPriceBasis ="";
        String sFreightInsurance ="", sPackFrwd ="", sTermsPymt ="", sDelivery ="", sDrawingDocsapprvl ="", sDelayMnfclrnce ="";   
        String sFMajeure ="", sStorage ="", sWrnty ="", sWgtsndmsns ="", sTests ="", sStandards ="", sLimtOfLiability ="";
        String sConseqLosses ="", sArbitration ="", sLang ="", sGovLaw ="", sViqty ="", sTot ="", sCtas ="", sPobar ="";         
        String sCdbd ="", sSuspension ="", sTermination ="", sBankrtcy ="", sAcceptance ="", sLos ="", sCis ="", sCfc ="", sGeneral ="";
        
        int iNumber = QuotationConstants.QUOTATION_NUMBER1;
		char c = (char)iNumber;
		
		try {
			//sDocSubmission=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DOCUMENT_SUBMISSIONS);
	        //sPackForward=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKAGE_FORWARDINGS);
	        //oTaxDuties=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIESS);
	        //sSpecialNote=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SPECIAL_NOTES);

			sTerm1 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1).replaceAll("</li></ul>",QuotationConstants.QUOTATION_EMPTY);
			sTerm2 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm3 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm4 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm5 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm6 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm7 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm8 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm9 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm10 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm11 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm12 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm13 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTerm14 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			
			sDefintions = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sValidity = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY);
			sScope = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE);
			sPriceBasis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sFreightInsurance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE);
			sPackFrwd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING);
			sTermsPymt = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sDelivery = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sDrawingDocsapprvl = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL);
			sDelayMnfclrnce = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE);
			
			sFMajeure = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sStorage = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE);
			sWrnty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY);
			sWgtsndmsns = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS);
			sTests = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS);
			
			sStandards = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS);
			sLimtOfLiability = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY);
			sConseqLosses = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES);
			sArbitration = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION);
			sLang = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE);
			
			sGovLaw = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW);
			sViqty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY);
			sTot = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sCtas = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY);
			sPobar = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK);
			
			sCdbd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT);
			sSuspension = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sSuspension= sSuspension.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sSuspension= sSuspension.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sSuspension= sSuspension.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sTermination = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sTermination = sTermination.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sTermination = sTermination.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
			sTermination = sTermination.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
			sBankrtcy = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY);
			sAcceptance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE);
			
			sLos= PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY);
			sCis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sCfc = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			sGeneral = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
			
			/* ###################################################	Adding the General Terms and Notes ################################################  */
			
			// --------------------------------------------------------------------------------------------------------------------------------------------
			oTable=new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			
			insertTableCell(oTable, QuotationConstants.QUOTATION_NOTES, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
			
			oParagraph = new Paragraph();
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			
			oDocument.add(new Paragraph(" "));
			
			oTable=new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm1.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm2.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm3.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm4.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm5.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm6.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm7.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm8.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm9.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm10.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm11.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm12.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm13.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			insertTableCell(oTable, String.valueOf(c)+" "+sTerm14.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
			
			oParagraph = new Paragraph();
			oParagraph.add(oTable);
			oDocument.add(oParagraph);
			
			// --------------------------------------------------------------------------------------------------------------------------------------------
			oDocument.newPage();
			
			oParagraph = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oParagraph.setAlignment(Element.ALIGN_LEFT);
			
			oDocument.add(oParagraph);
			
			oTable=new Table(4);
	        oTable.setWidth(95);
	        oTable.setBorder(0);
	        oTable.setSpacing((float).75);
	        oTable.setPadding((float).75);
	        oTable.setBorder(0);

	        insertTableCell(oTable, QuotationConstants.QUOTATION_SGTCO, Element.ALIGN_LEFT, 4, false, false, oTableBlueText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_DEF, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sDefintions.replaceAll(QuotationConstants.QUOTATION_NBSP, " "), Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_VALIDITYS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sValidity, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_SCOPES, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sScope, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_PRICEANDBASISAS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sPriceBasis, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_FRIN, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sFreightInsurance, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_PACKING, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sPackFrwd, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_TERMSOFPAY, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sTermsPymt, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_DELIVERYS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sDelivery, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sDrawingDocsapprvl, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sDelayMnfclrnce, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_FORCEMEASURE, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sFMajeure, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_STORAGES, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sStorage, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_WARRANTYS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sWrnty, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_WAIT, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sWgtsndmsns, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_TEST, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sTests, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_STANDARD, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sStandards, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_LIMITEDLIABILITY, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sLimtOfLiability, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_CONSEQ_LOSS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sConseqLosses, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_ARB, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sArbitration, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_LNG, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sLang, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_GL, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sGovLaw, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_VQ, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sViqty, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_TRAN_TITL, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sTot, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_CONFID, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sCtas, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_PASSBEFRISK, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sPobar, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_PASSBEFRISK2, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sCdbd, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_SUSPEN, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_SUSPENSION, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sSuspension, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_TERMINATION, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sTermination, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_BANKRUPTCYS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sBankrtcy, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_ACCEPTANCES, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sAcceptance, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_LIMITS, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sLos, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_CHSCOPE, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sCis, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_CHCOMM, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sCfc, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        insertTableCell(oTable, QuotationConstants.QUOTATION_GENERAL, Element.ALIGN_LEFT, 4, false, false, oTableSmallBoldText);
	        insertTableCell(oTable, sGeneral, Element.ALIGN_LEFT, 4, false, false, oTableSmallText);
	        
	        oParagraph = new Paragraph();
			oParagraph.add(oTable);
			
			oDocument.add(oParagraph);
			
		} finally {
			
		}
		
		return oDocument;
	}
	
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// Utility Methods : For Processing PDF Cell Data.
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	private void insertPdfPTableCell(PdfPTable table, String cellValue, int align, int colspan, boolean isField, boolean isBorder, Font font) {
		//create a new cell with the specified Text and Font
		PdfPCell oCell = new PdfPCell(new Phrase(cellValue.trim(), font));
		
		if(isBorder) {
			oCell.setBorder(1);
		} else {
			oCell.setBorder(0);
		}
		
		if(isField) {
			oCell.setBackgroundColor(new Color(242,242,255));
		}
		
		oCell.setHorizontalAlignment(align);
		
		oCell.setColspan(colspan);
		
		table.addCell(oCell);
	}
	
	private void insertTableCell(Table table, String cellValue, int align, int colspan, boolean isField, boolean isBorder, Font font) throws Exception {
		//create a new cell with the specified Text and Font
		Cell oCell = new Cell(new Phrase(cellValue.trim(), font));
		
		if(isBorder) {
			oCell.setBorder(1);
		} else {
			oCell.setBorder(0);
		}
		
		if(isField) {
			oCell.setBackgroundColor(new Color(242,242,255));
		}
		
		oCell.setHorizontalAlignment(align);
		
		oCell.setColspan(colspan);
		
		table.addCell(oCell);
	}
	
	private String buildPaymentDetailsString(NewEnquiryDto oNewEnquiryDto, int rowNum) {
		StringBuilder sPaymentDetails = new StringBuilder();
		
		if(rowNum == 1 && StringUtils.isNotBlank(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt1PercentAmtNetorderValue()) > 0) {
			sPaymentDetails.append(oNewEnquiryDto.getPt1PercentAmtNetorderValue() + "%");
			
			if(QuotationConstants.QUOTATION_PAYMENT_IMMEDIATELY.equalsIgnoreCase(oNewEnquiryDto.getPt1PaymentDays())) {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt1PaymentDays().toLowerCase());
			} else {
				sPaymentDetails.append(" within " + oNewEnquiryDto.getPt1PaymentDays().trim() + " days");
			}
			
			if("PBG".equalsIgnoreCase(oNewEnquiryDto.getPt1PayableTerms())) {
				sPaymentDetails.append(" Against " + oNewEnquiryDto.getPt1PayableTerms());
			} else {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt1PayableTerms());
			}
		}
		else if(rowNum == 2 && StringUtils.isNotBlank(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt2PercentAmtNetorderValue()) > 0) {
			sPaymentDetails.append(oNewEnquiryDto.getPt2PercentAmtNetorderValue() + "%");
			
			if(QuotationConstants.QUOTATION_PAYMENT_IMMEDIATELY.equalsIgnoreCase(oNewEnquiryDto.getPt2PaymentDays())) {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt2PaymentDays().toLowerCase());
			} else {
				sPaymentDetails.append(" within " + oNewEnquiryDto.getPt2PaymentDays().trim() + " days");
			}
			
			if("PBG".equalsIgnoreCase(oNewEnquiryDto.getPt2PayableTerms())) {
				sPaymentDetails.append(" Against " + oNewEnquiryDto.getPt2PayableTerms());
			} else {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt2PayableTerms());
			}
		}
		else if(rowNum == 3 && StringUtils.isNotBlank(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt3PercentAmtNetorderValue()) > 0) {
			sPaymentDetails.append(oNewEnquiryDto.getPt3PercentAmtNetorderValue() + "%");
			
			if(QuotationConstants.QUOTATION_PAYMENT_IMMEDIATELY.equalsIgnoreCase(oNewEnquiryDto.getPt3PaymentDays())) {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt3PaymentDays().toLowerCase());
			} else {
				sPaymentDetails.append(" within " + oNewEnquiryDto.getPt3PaymentDays().trim() + " days");
			}
			
			if("PBG".equalsIgnoreCase(oNewEnquiryDto.getPt3PayableTerms())) {
				sPaymentDetails.append(" Against " + oNewEnquiryDto.getPt3PayableTerms());
			} else {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt3PayableTerms());
			}
		}
		else if(rowNum == 4 && StringUtils.isNotBlank(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt4PercentAmtNetorderValue()) > 0) {
			sPaymentDetails.append(oNewEnquiryDto.getPt4PercentAmtNetorderValue() + "%");
			
			if(QuotationConstants.QUOTATION_PAYMENT_IMMEDIATELY.equalsIgnoreCase(oNewEnquiryDto.getPt4PaymentDays())) {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt4PaymentDays().toLowerCase());
			} else {
				sPaymentDetails.append(" within " + oNewEnquiryDto.getPt4PaymentDays().trim() + " days");
			}
			
			if("PBG".equalsIgnoreCase(oNewEnquiryDto.getPt4PayableTerms())) {
				sPaymentDetails.append(" Against " + oNewEnquiryDto.getPt4PayableTerms());
			} else {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt4PayableTerms());
			}
		}
		else if(rowNum == 5 && StringUtils.isNotBlank(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) && Integer.parseInt(oNewEnquiryDto.getPt5PercentAmtNetorderValue()) > 0) {
			sPaymentDetails.append(oNewEnquiryDto.getPt5PercentAmtNetorderValue() + "%");
			
			if(QuotationConstants.QUOTATION_PAYMENT_IMMEDIATELY.equalsIgnoreCase(oNewEnquiryDto.getPt5PaymentDays())) {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt5PaymentDays().toLowerCase());
			} else {
				sPaymentDetails.append(" within " + oNewEnquiryDto.getPt5PaymentDays().trim() + " days");
			}
			
			if("PBG".equalsIgnoreCase(oNewEnquiryDto.getPt5PayableTerms())) {
				sPaymentDetails.append(" Against " + oNewEnquiryDto.getPt5PayableTerms());
			} else {
				sPaymentDetails.append(" " + oNewEnquiryDto.getPt5PayableTerms());
			}
		}
		
		return sPaymentDetails.toString();
	}
	
	
	
	
	
	/**
	 * Inner class to add header and footer on every pdf page implementing PdfPageEventHelper
	 * @param conn Connection object to connect to database
	 * @param NewEnquiryDto oNewEnquiryDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: 19 March 2019
	 */
	class HeaderFooterPageEvent extends PdfPageEventHelper {
		private Image oBmp1 = null;
		private Image oBmp2 = null;

		@Override
		/**
		 * overridden super class method to add header and footer on every pdf page
		 * @param oWriter		PdfWriter object to add content to pdf
		 * @param oDocument		Document object of pdf writer
		 */
		public void onEndPage(PdfWriter oWriter, Document oDocument) {
			addHeaderFooter(oWriter, oDocument);

		}

		/**
		 * Method called from overriden method of helper class to include logic of adding header and footer on every pdf page
		 * @param oWriter		PdfWriter object to add content to pdf
		 * @param oDocument		Document object of pdf writer
		 */
		private void addHeaderFooter(PdfWriter oWriter, Document oDocument) {
			PdfContentByte oPdfContentByte1 = null;
			PdfTemplate oPdfTemplate1 = null;
			Phrase oPhrase1 = null;
			PdfContentByte oPdfContentByte2 = null;
			PdfTemplate oPdfTemplate2 = null;
			Phrase oPhrase2 = null;

			try {

				String sImagePath1 = sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator)
						+ File.separator + QuotationConstants.QUOTATION_STRING_HTML + File.separator
						+ QuotationConstants.QUOTATION_STRING_IMAGES + File.separator
						+ QuotationConstants.QUOTATION_MELOGO_NEW_IMG;

				String sImagePath2 = sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator)
						+ File.separator + QuotationConstants.QUOTATION_STRING_HTML + File.separator
						+ QuotationConstants.QUOTATION_STRING_IMAGES + File.separator
						+ QuotationConstants.QUOTATION_REGALLOGO_NEW_IMG;

				oBmp1 = Image.getInstance(sImagePath1);
				oBmp1.scalePercent(QuotationConstants.QUOTATION_LITERAL_20);
				oBmp1.setAbsolutePosition(QuotationConstants.QUOTATION_LITERAL_TEN,
						QuotationConstants.QUOTATION_LITERAL_780);
				oPdfContentByte1 = oWriter.getDirectContent();
				oPdfContentByte1.addImage(oBmp1);
				
				oBmp2 = Image.getInstance(sImagePath2);
				oBmp2.scalePercent(QuotationConstants.QUOTATION_LITERAL_8);
				oBmp2.setAbsolutePosition(QuotationConstants.QUOTATION_LITERAL_485,
						QuotationConstants.QUOTATION_LITERAL_20);
				oPdfContentByte2 = oWriter.getDirectContent();
				oPdfContentByte2.addImage(oBmp2);

			} catch (DocumentException de) {
				throw new ExceptionConverter(de);
			} catch (IOException e) {
				throw new ExceptionConverter(e);
			}
		}
	}
	
	
	
	
}
