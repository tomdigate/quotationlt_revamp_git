/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: PDFUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: This class generates PDF containing the enquiry details.
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 8, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.enquiry.dto.AddOnDto;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.RatingDto;
import in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.servlet.http.HttpServletRequest;










import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.io.FileDeleteStrategy;

import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

import java.nio.file.*; 


public class PDFUtility extends javax.swing.text.html.HTMLEditorKit.ParserCallback {
    
	Font oBlueHeader = FontFactory.getFont(FontFactory.HELVETICA, 15, Font.BOLD,new Color(79,120,175));
	Font oTextFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, new Color(102,102,102));
	Font oTermsTextFont = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, new Color(102,102,102));
	Font oTermsHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, Color.BLACK);
	Font oTableHeardingFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDEFINED, Color.WHITE);
	
	Font oHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, Color.BLUE);
	Font oSubHeadingFont = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD, new Color(79,120,175));
	
	 Font oAddOnHeardingFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDEFINED, new Color(153,0,0));
	
	Font oTableBlueText = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new Color(0,69,138));
	Font oTableBigBlueText = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD, new Color(0,69,138));
	Font oTableSmallText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, Color.BLACK);
	Font oTableSmallBoldText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, Color.BLACK);
	Font oTableSmallRedText = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.UNDEFINED, new Color(153,0,0));
    
	public static StringBuffer  sbData = new StringBuffer(); 
	
	String sFullPath = "";
	
    

	public String generateQuotationPDF(EnquiryDto oEnquiryDto, HttpServletRequest request) throws Exception {
		
	   
		   

		   
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sCustomer = oEnquiryDto.getCustomerName();
		String[] oCustomerArray = null;
		String spathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = spathArr[0];

        
        if(sCustomer!=null&&!sCustomer.equals("")){
        	oCustomerArray =sCustomer.split(" ");
        	if(oCustomerArray.length>=QuotationConstants.QUOTATION_LITERAL_2)
        		sCustomer = oCustomerArray[0]+" "+oCustomerArray[1];
        	else
        		sCustomer = oCustomerArray[0];
        } 
		
            String sFinalEnquiryId=oEnquiryDto.getEnquiryNumber().substring(15);
            String sPdfFile=sFullPath.replace("\\", File.separator) + File.separator
					+ QuotationConstants.QUOTATION_STRING_HTML + File.separator
            		+QuotationConstants.QUOTATION_STRING_QUOTATION+QuotationConstants.QUOTATION_STRING_UNDERSC+sFinalEnquiryId+QuotationConstants.QUOTATION_STRING_UNDERSC+sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
            		+DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentMonth()+QuotationConstants.QUOTATION_PDFEXT;
			
			
		File oFile = new File(sPdfFile);
		FileOutputStream oFileOutputStream=null;
		ByteArrayOutputStream oByteArrayOutputStream =null;
        try {

        	oFileOutputStream = new FileOutputStream(oFile);
        	oByteArrayOutputStream = generatePDF(oEnquiryDto,request);
        	oByteArrayOutputStream.writeTo(oFileOutputStream);
        } catch (IOException e) {
        } finally {
            if (oFileOutputStream != null) {
                try {
                	oFileOutputStream.close();
                } catch (IOException e) {
                }
            }
        }

        
		return sPdfFile;
	}
	
	/* This method is used to Fetch DM Uploaded Data And Save it that File 
     * @param EnquiryDto EnquiryDto object to handle the DTO elements
     * @return returns String Which Contains FilePath
     * This Method is used by
     * EnquiryAction.getDMUploadedAttachmentDetails()
     * @author 900010540 (Gangadhara Rao) 
     * on 06 Feb 2019
      */

	public String generateDmUploadedAttachments(EnquiryDto oEnquiryDto, HttpServletRequest request) throws Exception {
		
		   
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		String sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String spathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = spathArr[0];
		int iAttachmentId=0;
		byte[] returnBytes=null;
	    
        String sAttachmentFileName=sFullPath.replace("\\", File.separator) + File.separator
						+ QuotationConstants.QUOTATION_STRING_HTML +  File.separator
						+oEnquiryDto.getAttachmentFileName();
		
	File oFile = new File(sAttachmentFileName);
	FileOutputStream oFileOutputStream=null;
	ByteArrayOutputStream oByteArrayOutputStream =null;
    try {
    	iAttachmentId = oEnquiryDto.getAttachmentId();
		if (iAttachmentId > QuotationConstants.QUOTATION_LITERAL_ZERO){
   				URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
   				Service service = new Service();            
   				Call call = (Call) service.createCall();
   				call.setTargetEndpointAddress(url);
   				call.setOperationName(new QName("AttachmentService", "getAttachmentByBytes"));   				
   				call.addParameter("attachmentId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
   				call.setReturnType( org.apache.axis.encoding.XMLType.XSD_BASE64 );
   				
   				returnBytes = (byte[])call.invoke( new Object[] {iAttachmentId});
   				if (returnBytes != null && returnBytes.length > QuotationConstants.QUOTATION_LITERAL_ZERO){
   					oFileOutputStream = new FileOutputStream(oFile);
   					oFileOutputStream.write(returnBytes);
   					
   				}

			}
    	
    } catch (IOException e) {
    } finally {
        if (oFileOutputStream != null) {
            try {
            	oFileOutputStream.close();
            } catch (IOException e) {
            }
        }
    }

    
	return sAttachmentFileName;
}
	
	/**
	 * Method to Delete Files From Project Path
	 * @param sFilePath    String contains FilePath
	* @return Returns void Means Nothing It Will not Return
	 * Modified By Gangadhar 900010540
	 * on 06 Feb 2019
	 */

	public void deletePDF(String sFilePath) throws Exception {
		

           boolean isAttachmentExists=false;
           String[] sAttachmentFile=null;
	       File oFile = new File(sFilePath);
           boolean isTrue=false;
	                
	                  if((sFilePath != null && sFilePath.trim().length() > 0) ) 
	                      {
	                	  
	                    	isAttachmentExists=sFilePath.contains(QuotationConstants.QUOTATION_SPLITQSYMBOL);
	                    	
	                    	if(isAttachmentExists)
	                    	{
	                    		sAttachmentFile=sFilePath.split(QuotationConstants.QUOTATION_FORWARDSLASHSYMBOL+QuotationConstants.QUOTATION_SPLITQSYMBOL);
	                    		
	                    		for(int i=0;i<sAttachmentFile.length;i++)
	                    		{
	                                oFile = new File(sAttachmentFile[i]);
	                                
	                                isTrue=oFile.delete();

	                    		}
	                    	}
	                    	else
	                    	{
	                    		oFile = new File(sFilePath);
	                    		isTrue=oFile.delete();
	                    	}
	                    	
	                    	
	                    	
	                    }
	                    
	                  

          
	}
	
	public ByteArrayOutputStream generatePDF(EnquiryDto oEnquiryDto, HttpServletRequest request) throws Exception {
        
		Document oDocument = new Document(PageSize.A4, 50, 50, 70, 70);
		
		ByteArrayOutputStream oByteArrayOutputStream = new ByteArrayOutputStream();
        
        
		
		String sPath = this.getClass().getClassLoader().getResource("").getPath();
		sFullPath = URLDecoder.decode(sPath, "UTF-8");
		String sPathArr[] = sFullPath.split("/WEB-INF/classes/");
		sFullPath = sPathArr[0];
		
		String sImagePath3=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR1IMG;
		
		String sImagePath4=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MOTOR2IMG;
        
        
        String sDocSubmission="";
        String sPackForward="";
        String oTaxDuties="";
        String sSpecialNote="";
        String sTerm1="";
		String sTerm2="";
		String sTerm3="";
		String sTerm4="";
		String sTerm5="";
		String sTerm6="";
		String sTerm7="";
		String sTerm8="";
		String sTerm9="";
		String sTerm10="";
		String sTerm11="";
		String sTerm12="";
		String sTerm13="";
		String sTerm14="";
		
		ArrayList arlRatings =null;
		
		RatingDto oRatingDto = null;
		TechnicalOfferDto oTechnicalOfferDto = null;
		PdfPCell oCell = null;
		PdfPTable oMainTable =null;
		String sRating=null;
		PdfPCell oFirstTableCell =null;
		PdfPTable oFirstTable =null;
		Table oTable =null;
		PdfWriter oWriter =null;		
		Image oBmp3 =null;
		Image oBmp4 =null;
		Paragraph oReqParagraph =null;
		Paragraph oP=null;
		Table oTable1 =null;
		Paragraph oParagraph =null;
		Paragraph oRegardsParagraph =null;
		Cell oCellc =null;
		Cell oCell1=null;
		String sKw="";
		String sBkw="";
        PdfPCell ooSecondTableCell = null;
        PdfPTable ooSecondTable = null;
        String sFinalString=null;
        String sRatedVoltage=null;
        String sFrequency="";
        String sRpm="";
        String sFlcAmps="";
        String sNlCurrent="";
        String sFltSQCAGE="";
        String sSTHot="";
        String sSTCold="";
        String sStartTimeRV="";
        String sStartTimeRV80="";
        String sAmbience="";
        String sAltitude="";
        String sNoiseLevel="";
        String sMoment1=QuotationConstants.QUOTATION_MOMENT1;
        String sMoment2=QuotationConstants.QUOTATION_MOMENT2;
        String sRtGD2="";
        String sMotorTotalWgt="";
        String sDevClassification="";
        String sRgrdsPrgph="";
        String sDefintions ="";
        String sValidity ="";
        String sScope ="";
        String sPriceBasis ="";
        String sFreightInsurance ="";
        String sPackFrwd ="";
        String sTermsPymt ="";
        String sDelivery ="";
        String sDrawingDocsapprvl ="";
        String sDelayMnfclrnce ="";         
        String sFMajeure ="";
        String sStorage ="";
        String sWrnty ="";
        String sWgtsndmsns ="";
        String sTests ="";         
        String sStandards ="";
        String sLimtOfLiability ="";
        String sConseqLosses ="";
        String sArbitration ="";
        String sLang ="";         
        String sGovLaw ="";
        String sViqty ="";
        String sTot ="";
        String sCtas ="";
        String sPobar ="";         
        String sCdbd ="";
        String sSuspension ="";
        String sTermination ="";
        String sBankrtcy ="";
        String sAcceptance ="";         
        String sLos ="";
        String sCis ="";
        String sCfc ="";
        String sGeneral ="";
		int iNumber = QuotationConstants.QUOTATION_NUMBER1;
		char c = (char)iNumber;
		
		int iNumber2=QuotationConstants.QUOTATION_NUMBER2;
		char c2=(char)iNumber2;
		
		
		PdfPTable oPdfPTable = new PdfPTable(2);
		PdfPCell oPdfPCell = new PdfPCell();
		Paragraph oParagraph1 = new Paragraph();
		Paragraph oParagraph2 = new Paragraph();
		
		PdfTemplate oPdfTemplate1 = null;

		try {
			/*Creating instance of PDFWrite Object*/
			oWriter = PdfWriter.getInstance(oDocument, oByteArrayOutputStream );
			
			HeaderFooterPageEvent oEvent = new HeaderFooterPageEvent();
			oWriter.setPageEvent(oEvent);			
			oDocument.open();			
			oBmp3 = Image.getInstance(sImagePath3);
			oBmp3.scalePercent(QuotationConstants.QUOTATION_LITERAL_40);			
			oBmp4 = Image.getInstance(sImagePath4);
			oBmp4.scalePercent(QuotationConstants.QUOTATION_LITERAL_60);
			oReqParagraph= new Paragraph(QuotationConstants.QUOTATION_TOCUSTOMER,oBlueHeader);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			
			oReqParagraph = new Paragraph(QuotationConstants.QUOTATION_RFQ+oEnquiryDto.getEnquiryNumber()+"           ",oTextFont);
			oReqParagraph.setAlignment(Paragraph.ALIGN_RIGHT);
			oDocument.add(oReqParagraph);
			
			oP= new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
			oP.setAlignment(Element.ALIGN_LEFT);
             
			oDocument.add(oP);	
			oDocument.add(new Paragraph(" "));
			oDocument.add(addCustomerInformation(oEnquiryDto)); 
			
			oParagraph1.add(new Chunk(oBmp3, QuotationConstants.QUOTATION_LITERAL_TEN,QuotationConstants.QUOTATION_LITERAL_M300));
			oParagraph1.add(new Chunk(oBmp4, QuotationConstants.QUOTATION_LITERAL_ZERO, QuotationConstants.QUOTATION_LITERAL_M250));	
			oParagraph1.setSpacingAfter(QuotationConstants.QUOTATION_LITERAL_250);
			
			oDocument.add(oParagraph1);		
			
			oParagraph2 = new Paragraph("");
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_REGARDS, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(oEnquiryDto.getSm_name(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_MEMI, oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_EMAIL+oEnquiryDto.getSmEmail(), oTextFont));
			oParagraph2.add(new Chunk(Chunk.NEWLINE));
			oParagraph2.add(new Chunk(QuotationConstants.QUOTATION_CONTACT+oEnquiryDto.getSmContactNo(), oTextFont));
			
			oParagraph2.setSpacingBefore(QuotationConstants.QUOTATION_LITERAL_150);
			
			oDocument.add(oParagraph2);
			
			
			
			oTable =new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);
			
			oTable1 =new Table(5);
			oTable1.setWidth(100);
			oTable1.setBorder(0);
			oTable1.setSpacing((float).75);
			oTable1.setPadding((float).75);
			oTable1.setBorder(0);			
			oCell1=null;



			oTable.setBackgroundColor(new Color(255,255,255));
			
			if(oEnquiryDto.getRatingObjects()!=null)
			{
				arlRatings = oEnquiryDto.getRatingObjects();
				if(arlRatings.size()>QuotationConstants.QUOTATION_LITERAL_ZERO)
				{
					for(int i = 0;i<arlRatings.size();i++)
					{	

						oRatingDto = (RatingDto)arlRatings.get(i);
						oTechnicalOfferDto = oRatingDto.getTechnicalDto();

						oParagraph = new Paragraph();					
					    
						oDocument.newPage();	
						oP.setSpacingBefore(QuotationConstants.QUOTATION_LITERAL_50);
						oDocument.add(oP);
				         oCell = null;				        
				        oMainTable = new PdfPTable(2);
				        oMainTable.setWidthPercentage(95f);       
				        
				        oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);				        
			        
				        sRating=QuotationConstants.QUOTATION_RATING+oRatingDto.getRatingNo();      
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_TECHREQ+sRating, oTableBlueText));
				        oCell.setColspan(2);
				        oCell.setBorder(0);
				        oCell.setVerticalAlignment(Element.ALIGN_LEFT);
				        oMainTable.addCell(oCell);
				        oFirstTableCell = new PdfPCell();
				        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
				        oFirstTable=new PdfPTable(2);
				        oFirstTable.setWidthPercentage(85f);
				        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
				        
				        if(String.valueOf(oRatingDto.getQuantity())!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_QNTYREQ, oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
	
				        oCell = new PdfPCell(new Phrase(String.valueOf(oRatingDto.getQuantity()), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getApplicationName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_APPLICATION,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getApplicationName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getScsrName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_QTYPE,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getScsrName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getAreaClassification()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ARC,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getAreaClassification(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getZone()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ZONE,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getZone(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getGasGroup()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_GASGP,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getGasGroup(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getEnclosureName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ENCLOSURE,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getEnclosureName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getFrameSize()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_FRAME,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getFrameSize(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
					   if(oRatingDto.getMountingName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_MOUNTING,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getMountingName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        
				        if(oRatingDto.getKw()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RATEDOUPTUPN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	oFirstTable.addCell(oCell);
						
						sKw=oRatingDto.getKw();
						if(oRatingDto.getKw()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sKw=sKw+oRatingDto.getRatedOutputUnit();
						}

						oCell = new PdfPCell(new Phrase(sKw, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getServiceFactor()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SERVICEFACTOR,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	oFirstTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getServiceFactor(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getBkw()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_BKW,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	oFirstTable.addCell(oCell);
						
						sBkw=oTechnicalOfferDto.getBkw();
						if(oTechnicalOfferDto.getBkw()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sBkw=sBkw+QuotationConstants.QUOTATION_KW;
						}

						oCell = new PdfPCell(new Phrase(sBkw, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getDutyName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DUTY,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	oFirstTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oRatingDto.getDutyName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        }
				        oFirstTableCell.addElement(oFirstTable);
				        
				        oMainTable.addCell(oFirstTableCell);
				        
				        ooSecondTableCell = new PdfPCell();
				        ooSecondTableCell.setBorder(Rectangle.NO_BORDER);
		
				        ooSecondTable = new PdfPTable(2);
				        ooSecondTable.setWidthPercentage(85f);
				        ooSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
				        if(oRatingDto.getVolts()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RATEDVOTUN,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        sRatedVoltage=oRatingDto.getVolts();
						if(oRatingDto.getVolts()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sRatedVoltage=sRatedVoltage+" +/-"+oRatingDto.getVoltsVar()+" %";
						}

						oCell = new PdfPCell(new Phrase(sRatedVoltage, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getFrequency()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RATEDFREQUN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
						
						sFrequency=oRatingDto.getFrequency();
						if(oRatingDto.getFrequency()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sFrequency=sFrequency+QuotationConstants.QUOTATION_HZ;
						}
						oCell = new PdfPCell(new Phrase(sFrequency, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getPoleName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_POLE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oRatingDto.getPoleName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
	
				        if(oTechnicalOfferDto.getRpm()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RATEDSPEEDNN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
						
						sRpm=oTechnicalOfferDto.getRpm();
						if(oTechnicalOfferDto.getRpm()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sRpm=sRpm+QuotationConstants.QUOTATION_RMIN;
						}


						oCell = new PdfPCell(new Phrase(sRpm, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getFlcAmps()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RATEDCURRENTIN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
						
						sFlcAmps=oTechnicalOfferDto.getFlcAmps();
						if(oTechnicalOfferDto.getFlcAmps()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sFlcAmps=sFlcAmps+QuotationConstants.QUOTATION_STRING_A;
						}
						oCell = new PdfPCell(new Phrase(sFlcAmps, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
	
				        if(oTechnicalOfferDto.getNlCurrent()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_NOLOADCURRENT,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
						
						sNlCurrent=oTechnicalOfferDto.getNlCurrent();
						if(oTechnicalOfferDto.getNlCurrent()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sNlCurrent=sNlCurrent+QuotationConstants.QUOTATION_STRING_A;
						}

						oCell = new PdfPCell(new Phrase(sNlCurrent, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getStrtgCurrent()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_STARCURENTISIN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getStrtgCurrent(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getFltSQCAGE()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_NOMTORQUETN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
						
				        	sFltSQCAGE=oTechnicalOfferDto.getFltSQCAGE();
						if(oTechnicalOfferDto.getFltSQCAGE()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sFltSQCAGE=sFltSQCAGE+QuotationConstants.QUOTATION_NM;
						}

						oCell = new PdfPCell(new Phrase(sFltSQCAGE, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
	
				        if(oTechnicalOfferDto.getLrTorque()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_LOCKEDROTOR,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getLrTorque(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getPotFLT()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_MAXIMUMTORQUE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getPotFLT(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getRv()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RV,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getRv(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getRa()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_RA,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getRa(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
					    ooSecondTableCell.addElement(ooSecondTable);
				        oMainTable.addCell(ooSecondTableCell);			      
				        
				        oParagraph.add(oMainTable);
			
				        oTable1=new Table(5);
						oTable1.setWidth(100);
						oTable1.setBorder(0);
						oTable1.setSpacing((float).75);
						oTable1.setPadding((float).75);
						oTable1.setBorder(0);

						if(oTechnicalOfferDto.getPllCurr1()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff1()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllPf1()!=QuotationConstants.QUOTATION_EMPTY || 
						   oTechnicalOfferDto.getPllCurr2()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff2()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllPf2()!=QuotationConstants.QUOTATION_EMPTY ||
						   oTechnicalOfferDto.getPllCurr3()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff3()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllpf3()!=QuotationConstants.QUOTATION_EMPTY ||
						   oTechnicalOfferDto.getPllCurr4()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllpf4()!=QuotationConstants.QUOTATION_EMPTY)
						{
						 oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_DOTED_LINE, oTableSmallText));
						 oCell1.setBorder(0);
						 oCell1.setColspan(5);
						 oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						 oTable1.addCell(oCell1);
		
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_LOAD, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_LOADPER, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_CURRENTA, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EFFICIENCY, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_POWERFACTOR, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						}

						if(oTechnicalOfferDto.getPllCurr1()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff1()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllPf1()!=QuotationConstants.QUOTATION_EMPTY )
						{
							
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_PLL, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_STRING_100, oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllCurr1(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllEff1(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllPf1(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						}


						if(oTechnicalOfferDto.getPllCurr2()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff2()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllPf2()!=QuotationConstants.QUOTATION_EMPTY)
						{
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);

						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_STRING_75, oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
		
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllCurr2(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllEff2(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllPf2(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						}


						if(oTechnicalOfferDto.getPllCurr3()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff3()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllpf3()!=QuotationConstants.QUOTATION_EMPTY)
						{
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_STRING_50, oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllCurr3(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllEff3(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllpf3(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						}

						if(oTechnicalOfferDto.getPllCurr4()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllpf4()!=QuotationConstants.QUOTATION_EMPTY)
						{
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);

						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_START, oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllCurr4(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
						oCell1.setBorder(1);
						
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);


						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllpf4(), oTableSmallText));
						oCell1.setBorder(1);
						
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						}
						if(oTechnicalOfferDto.getPllCurr5()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllEff4()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getPllPf5()!=QuotationConstants.QUOTATION_EMPTY)
						{
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_STRING_DUTYPOINT, oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllCurr5(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						
						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllEff4(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);

						oCell1 = new Cell(new Phrase(oTechnicalOfferDto.getPllPf5(), oTableSmallText));
						oCell1.setBorder(1);
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setBackgroundColor(new Color(242,242,255));
						oCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
						oTable1.addCell(oCell1);
						}

						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_DOTED_LINE, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setColspan(5);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
						
						oParagraph.add(oTable1);
		
				        
						oMainTable = new PdfPTable(2);
				        oMainTable.setWidthPercentage(95f); 
				       
				        oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
				       			       
						 
				        oFirstTableCell = new PdfPCell();
				        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
				        
				        oFirstTable = new PdfPTable(2);
				        
				        oFirstTable.setWidthPercentage(85f);
				        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
				        
				        if(oTechnicalOfferDto.getSstHot()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_HOT,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
				        sSTHot=oTechnicalOfferDto.getSstHot();
						if(oTechnicalOfferDto.getSstHot()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sSTHot=sSTHot+QuotationConstants.QUOTATION_S;
						}


						oCell = new PdfPCell(new Phrase(sSTHot, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getSstCold()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_COLD,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sSTCold=oTechnicalOfferDto.getSstCold();
						if(oTechnicalOfferDto.getSstCold()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sSTCold=sSTCold+QuotationConstants.QUOTATION_S;
						}

						oCell = new PdfPCell(new Phrase(sSTCold, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getStartTimeRV()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_STARTTIME_RATEDV,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
				        sStartTimeRV=oTechnicalOfferDto.getStartTimeRV();
						if(oTechnicalOfferDto.getStartTimeRV()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sStartTimeRV=sStartTimeRV+QuotationConstants.QUOTATION_S;
						}
						oCell = new PdfPCell(new Phrase(sStartTimeRV, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getStartTimeRV80()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_STARTTIME_RATEDV_80,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sStartTimeRV80=oTechnicalOfferDto.getStartTimeRV80();
						if(oTechnicalOfferDto.getStartTimeRV80()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sStartTimeRV80=sStartTimeRV80+QuotationConstants.QUOTATION_S;
						}
						oCell = new PdfPCell(new Phrase(sStartTimeRV80, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getInsulationName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_INSULATION_CLASS,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
				        oCell = new PdfPCell(new Phrase(oRatingDto.getInsulationName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getTempRaiseDesc()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_TEMPERATURE_RISE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
				        oCell = new PdfPCell(new Phrase(oRatingDto.getTempRaiseDesc(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
			
				        if(oRatingDto.getAmbience()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_AMBIENT_TEMP,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sAmbience= oRatingDto.getAmbience();
						if( oRatingDto.getAmbience()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sAmbience=sAmbience+QuotationConstants.QUOTATION_STRING_C;
						}

						
						oCell = new PdfPCell(new Phrase(sAmbience, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getAltitude()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ALTITUDE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sAltitude= oRatingDto.getAltitude();
						if( oRatingDto.getAltitude()!=QuotationConstants.QUOTATION_EMPTY)
						{
							sAltitude=sAltitude+QuotationConstants.QUOTATION_MASL;
						}

						
						oCell = new PdfPCell(new Phrase(sAltitude, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oRatingDto.getDegreeOfProName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DP,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getDegreeOfProName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getCoolingSystem()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_CS,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getCoolingSystem(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getBearingDENDE()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_BDENDE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getBearingDENDE(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getLubrication()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_LUBRICATION,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);

				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getLubrication(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(!oTechnicalOfferDto.getNoiseLevel().equals(QuotationConstants.QUOTATION_STRING_ZERO) && !oTechnicalOfferDto.getNoiseLevel().equals(QuotationConstants.QUOTATION_EMPTY))
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SOUNDPRESSURE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sNoiseLevel=oTechnicalOfferDto.getNoiseLevel();
						if(!sNoiseLevel.equals(QuotationConstants.QUOTATION_STRING_ZERO) && !sNoiseLevel.equals(QuotationConstants.QUOTATION_EMPTY))
						{
							sNoiseLevel=sNoiseLevel+QuotationConstants.QUOTATION_DBA;
						}

						oCell = new PdfPCell(new Phrase(sNoiseLevel, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getRtGD2()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        		
				        	oCell = new PdfPCell(new Phrase(sMoment1,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
						
						sRtGD2=oTechnicalOfferDto.getRtGD2();
						if(sRtGD2!=QuotationConstants.QUOTATION_EMPTY)
						{
							sRtGD2=sRtGD2+QuotationConstants.QUOTATION_KGM2;
						}

						oCell = new PdfPCell(new Phrase(sRtGD2, oTableSmallText));
						oCell.setBorder(1);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getMigD2Load()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        
				        oCell = new PdfPCell(new Phrase(sMoment2,oTableSmallText));

				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getMigD2Load(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_BALANCING,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ISOGRADE, oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
						

				        oFirstTableCell.addElement(oFirstTable);
				        
				        oMainTable.addCell(oFirstTableCell);

				        
				        ooSecondTableCell = new PdfPCell();
				        ooSecondTableCell.setBorder(Rectangle.NO_BORDER);

				        ooSecondTable = new PdfPTable(2);
				        ooSecondTable.setWidthPercentage(85f);
				        ooSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
				        
				        if(oTechnicalOfferDto.getVibrationLevel()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_VIBECLASSS,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getVibrationLevel(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getMainTermBoxPS()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_MAINTERM,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getMainTermBoxPS(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getNeutralTermBox()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_NEUTRTBOX,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getNeutralTermBox(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getCableEntry()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_CABLE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getCableEntry(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }

				        if(oTechnicalOfferDto.getAuxTermBox()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_AUXTERMINAL,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getAuxTermBox(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getStatorConn()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_STATORCONN,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getStatorConn(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getNoBoTerminals()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_NOTERM,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getNoBoTerminals(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getRotation()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ROT,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getRotation(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getDirectionOfRotName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DROT,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getDirectionOfRotName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getMotorTotalWgt()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_TOTWGT,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        sMotorTotalWgt=oTechnicalOfferDto.getMotorTotalWgt();
				        if(sMotorTotalWgt!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	sMotorTotalWgt=sMotorTotalWgt+QuotationConstants.QUOTATION_KG;
				        }
				        oCell = new PdfPCell(new Phrase(sMotorTotalWgt, oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getSpaceHeater()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SH,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getSpaceHeater(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getWindingRTD()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_WINDING,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getWindingRTD(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getBearingRTD()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_BEARING,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        	ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getBearingRTD(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						ooSecondTable.addCell(oCell);
				        }
				        if(!oTechnicalOfferDto.getMinStartVolt().equals(QuotationConstants.QUOTATION_STRING_ZERO) && !oTechnicalOfferDto.getMinStartVolt().equals(QuotationConstants.QUOTATION_EMPTY))
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_MINSTART,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getMinStartVolt(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getMethodOfStg()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_METHODSTART,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getMethodOfStg(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getTermBoxName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_MAINTERMBOX,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getTermBoxName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        ooSecondTableCell.addElement(ooSecondTable);
				        
				        oMainTable.addCell(ooSecondTableCell);
				        
				        oParagraph.add(oMainTable);
				        
						
				      if(oTechnicalOfferDto.getBearingThermoDT()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getAirThermo()!=QuotationConstants.QUOTATION_EMPTY ||
				      oTechnicalOfferDto.getVibeProbMPads()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getEncoder()!=QuotationConstants.QUOTATION_EMPTY ||	 
				      oTechnicalOfferDto.getSpeedSwitch()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getSurgeSuppressors()!=QuotationConstants.QUOTATION_EMPTY ||
				      oTechnicalOfferDto.getCurrTransformers()!=QuotationConstants.QUOTATION_EMPTY || oTechnicalOfferDto.getVibProbes()!=QuotationConstants.QUOTATION_EMPTY ||
				      oTechnicalOfferDto.getSpmNipple()!=QuotationConstants.QUOTATION_EMPTY ||  oTechnicalOfferDto.getKeyPhasor()!=QuotationConstants.QUOTATION_EMPTY ||
				      oRatingDto.getShaftExtName()!=QuotationConstants.QUOTATION_EMPTY || oRatingDto.getPaint()!=QuotationConstants.QUOTATION_EMPTY ||
				    		  oTechnicalOfferDto.getPaintColor()!=QuotationConstants.QUOTATION_EMPTY
				      )  
				      {
				    	  oTable1=new Table(4);
				    	  oTable1.setWidth(95);
				    	  oTable1.setBorder(0);
				    	  oTable1.setSpacing((float).75);
				    	  oTable1.setPadding((float).75);
				    	  oTable1.setBorder(0);

							oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_DOTED_LINE, oTableSmallText));
							oCell1.setBorder(0);
							oCell1.setColspan(4);
							oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
							oTable1.addCell(oCell1);
							
							oParagraph.add(oTable1);
	  
				      }
				        
						oMainTable = new PdfPTable(2);
				        oMainTable.setWidthPercentage(95f); 
				        oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
				       			       
				        oFirstTableCell = new PdfPCell();
				        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
				        oFirstTable = new PdfPTable(2);
				        oFirstTable.setWidthPercentage(85f);
				        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
				        
				        if(oTechnicalOfferDto.getBearingThermoDT()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	
				        	
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DIALTYPEBEARING,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getBearingThermoDT(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				       
				        if(oTechnicalOfferDto.getAirThermo()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_AIRTHERMO,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getAirThermo(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				 	
				        if(oTechnicalOfferDto.getVibeProbMPads()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_VIBPROB,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getVibeProbMPads(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
			
				        if(oTechnicalOfferDto.getEncoder()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ENCODER,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getEncoder(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
			
				        if(oTechnicalOfferDto.getSpeedSwitch()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SPEEDSWITCH,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getSpeedSwitch(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				        if(oTechnicalOfferDto.getSurgeSuppressors()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SURGESUPRESSOR,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getSurgeSuppressors(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }

				        if(oTechnicalOfferDto.getCurrTransformers()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_CURRENTTRANSFORMER,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getCurrTransformers(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
	
				        if(oTechnicalOfferDto.getVibProbes()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_VIBRPROBES,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        oFirstTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getVibProbes(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
				        oFirstTable.addCell(oCell);
				        }
				     
				        oFirstTableCell.addElement(oFirstTable);
				        
				        oMainTable.addCell(oFirstTableCell);



				        ooSecondTableCell = new PdfPCell();
				        ooSecondTableCell.setBorder(Rectangle.NO_BORDER);
				       
				        ooSecondTable = new PdfPTable(2);
				        ooSecondTable.setWidthPercentage(85f);
				        ooSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
				        
				        

				        if(oTechnicalOfferDto.getSpmNipple()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SPMNIPPLE,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getSpmNipple(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
					if(oTechnicalOfferDto.getKeyPhasor()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_KEYPHASOR,oTableSmallText));
				        oCell.setBorder(0);
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getKeyPhasor(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getShaftExtName()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_SHAFTEXT,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getShaftExtName(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
				        if(oRatingDto.getPaint()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_PAINTTYPE,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oRatingDto.getPaint(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
			
				        if(oTechnicalOfferDto.getPaintColor()!=QuotationConstants.QUOTATION_EMPTY)
				        {
				        	oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_PAINTCOLOR,oTableSmallText));
				        	oCell.setBorder(0);
				        	oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        oCell = new PdfPCell(new Phrase(oTechnicalOfferDto.getPaintColor(), oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				        ooSecondTable.addCell(oCell);
				        }
							        
				        ooSecondTableCell.addElement(ooSecondTable);
				        
				        oMainTable.addCell(ooSecondTableCell);
				        
				        oParagraph.add(oMainTable);
		
				        oTable1 =new Table(5);
				        oTable1.setWidth(100);
				        oTable1.setBorder(0);
				        oTable1.setSpacing((float).75);
				        oTable1.setPadding((float).75);
				        oTable1.setBorder(0);

						
				        if(!oTechnicalOfferDto.getDeviation_clarification().equals(QuotationConstants.QUOTATION_EMPTY))
				        {
				        	
						oCell1 = new Cell(new Phrase(QuotationConstants.QUOTATION_DOTED_LINE, oTableSmallText));
						oCell1.setBorder(0);
						oCell1.setColspan(5);
						oCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						oTable1.addCell(oCell1);
								
						oParagraph.add(oTable1);
		
						oMainTable = new PdfPTable(1);
					    oMainTable.setWidthPercentage(95f); 
					       
					    oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
					        
					        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DEVCLAR, oTableBlueText));
					        oCell.setColspan(2);
					        oCell.setBorder(0);
					        oCell.setVerticalAlignment(Element.ALIGN_LEFT);
					        oMainTable.addCell(oCell);

					        oFirstTableCell = new PdfPCell();
					        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
		
					        oFirstTable = new PdfPTable(2);
					        
		
					        oFirstTable.setWidthPercentage(95f);
					        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);

						
					        sDevClassification=oTechnicalOfferDto.getDeviation_clarification();
						

				        oCell = new PdfPCell(new Phrase(sDevClassification, oTableSmallText));
				        oCell.setBorder(1);
				        oCell.setColspan(2);
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setBackgroundColor(new Color(242,242,255));
				        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				       
						oFirstTable.addCell(oCell);
				        }
				      
				        oFirstTableCell.addElement(oFirstTable);
				        
				        oMainTable.addCell(oFirstTableCell);
				      				        
				        oParagraph.add(oMainTable);


				        oDocument.add(oParagraph);
				        
					}
				}		    
			}

			
			oDocument.newPage();
			
			oDocument.add(oP);
			
			
			
		    oTable=new Table(4);
			oTable.setWidth(95);
			oTable.setBorder(0);
			oTable.setSpacing((float).75);
			oTable.setPadding((float).75);
			oTable.setBorder(0);

			 oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_COMOFFER, oTableBlueText));
			oCellc.setColspan(4);
			oCellc.setBorder(0);
			oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
			oTable.addCell(oCellc);
		    
			oParagraph = new Paragraph();
			oParagraph.add(oTable);

			
			
			oDocument.add(oParagraph);
			oDocument.add(addCommercialOffer(oEnquiryDto));
			oDocument.add(new Paragraph(" "));
			
			oDocument.add(addTotalDetails(oEnquiryDto));
			
			oDocument.newPage();
				
			oDocument.add(oP);
			
				
				
		        sDocSubmission=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DOCUMENT_SUBMISSIONS);
		       
		        
		        sPackForward=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKAGE_FORWARDINGS);
		        
		        oTaxDuties=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIESS);
		        
		        sSpecialNote=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SPECIAL_NOTES);

				
				sTerm1 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1).replaceAll("</li></ul>",QuotationConstants.QUOTATION_EMPTY);
				
				
				sTerm2 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm3 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm4 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm5 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm6 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm7 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm8 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm9 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm10 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm11 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm12 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm13 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sTerm14 = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14).replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				
				sDefintions = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sValidity = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY);
				sScope = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE);
				sPriceBasis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sFreightInsurance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE);
				sPackFrwd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING);
				sTermsPymt = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sDelivery = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sDrawingDocsapprvl = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL);
				sDelayMnfclrnce = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE);
				
				sFMajeure = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sStorage = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE);
				sWrnty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY);
				sWgtsndmsns = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS);
				sTests = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS);
				
				sStandards = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS);
				sLimtOfLiability = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY);
				sConseqLosses = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES);
				sArbitration = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION);
				sLang = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE);
				
				sGovLaw = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW);
				sViqty = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY);
				sTot = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sCtas = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY);
				sPobar = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK);
				
				sCdbd = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT);
				sSuspension = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sSuspension= sSuspension.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
				sSuspension= sSuspension.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
				sSuspension= sSuspension.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				sTermination = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sTermination = sTermination.replace("<ul><li>", QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
				sTermination = sTermination.replaceAll("</li><li>", QuotationConstants.QUOTATION_NEWLINE+QuotationConstants.QUOTATION_TAB+" "+String.valueOf(c)+" ");
				sTermination = sTermination.replaceAll("</li></ul>", QuotationConstants.QUOTATION_EMPTY);
				sBankrtcy = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY);
				sAcceptance = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE);
				
				sLos= PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY);
				sCis = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sCfc = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				sGeneral = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL).replaceAll("<br/>", QuotationConstants.QUOTATION_NEWLINE);
				
				oTable=new Table(4);
				oTable.setWidth(95);
				oTable.setBorder(0);
				oTable.setSpacing((float).75);
				oTable.setPadding((float).75);
				oTable.setBorder(0);

				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_NOTES, oTableBlueText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				oParagraph = new Paragraph();
				oParagraph.add(oTable);
				oDocument.add(oParagraph);
				
				oDocument.add(new Paragraph(" "));
		
				oTable=new Table(4);
				oTable.setWidth(95);
				oTable.setBorder(0);
				oTable.setSpacing((float).75);
				oTable.setPadding((float).75);
				oTable.setBorder(0);
				
				
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);

				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm1.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm2.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    


				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm3.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm4.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm5.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm6.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm7.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
			
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm8.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
			
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm9.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm10.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm11.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm12.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				
				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm13.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
			    
				

				oCellc = new Cell(new Phrase(String.valueOf(c)+" "+sTerm14.replaceAll("<ul><li>",QuotationConstants.QUOTATION_EMPTY),oTableSmallText));
				oCellc.setColspan(3);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				
				oParagraph = new Paragraph();
				oParagraph.add(oTable);
				oDocument.add(oParagraph);
				oDocument.newPage();
				
				
				oParagraph = new Paragraph(QuotationConstants.QUOTATION_LINE, oTermsHeadingFont);
				oParagraph.setAlignment(Element.ALIGN_LEFT);
				
				oDocument.add(oParagraph);	
		        

		        oTable=new Table(4);
		        oTable.setWidth(95);
		        oTable.setBorder(0);
		        oTable.setSpacing((float).75);
		        oTable.setPadding((float).75);
		        oTable.setBorder(0);

				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SGTCO, oTableBlueText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DEF, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sDefintions.replaceAll(QuotationConstants.QUOTATION_NBSP, " "), oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				


				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_VALIDITYS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				oCellc = new Cell(new Phrase(sValidity, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SCOPES, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
			   
				
				oCellc = new Cell(new Phrase(sScope, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PRICEANDBASISAS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(sPriceBasis, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_FRIN, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sFreightInsurance, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);	
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PACKING, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				

				oCellc = new Cell(new Phrase(sPackFrwd, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TERMSOFPAY, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				

				oCellc = new Cell(new Phrase(sTermsPymt, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
	
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DELIVERYS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				
				oCellc = new Cell(new Phrase(sDelivery, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				
				oCellc = new Cell(new Phrase(sDrawingDocsapprvl, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_DELAYMANFCLEARANCE, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				
				oCellc = new Cell(new Phrase(sDelayMnfclrnce, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_FORCEMEASURE, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sFMajeure, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

					
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_STORAGES, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sStorage, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				


				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_WARRANTYS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sWrnty, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

					
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_WAIT, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sWgtsndmsns, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TEST, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sTests, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

						
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_STANDARD, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sStandards, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

		
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LIMITEDLIABILITY, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sLimtOfLiability, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

	
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CONSEQ_LOSS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sConseqLosses, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

	
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_ARB, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sArbitration, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

	
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LNG, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sLang, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

					
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_GL, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sGovLaw, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

					
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_VQ, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				
				oCellc = new Cell(new Phrase(sViqty, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TRAN_TITL, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sTot, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CONFID, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				

				
				
				oCellc = new Cell(new Phrase(sCtas, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PASSBEFRISK, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				

				
				
				oCellc = new Cell(new Phrase(sPobar, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_PASSBEFRISK2, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				

				
				
				oCellc = new Cell(new Phrase(sCdbd, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				


				
				

				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SUSPEN, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_SUSPENSION, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(sSuspension, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_TERMINATION, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				

				oCellc = new Cell(new Phrase(sTermination, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);


				
				



				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_BANKRUPTCYS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sBankrtcy, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				

				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_ACCEPTANCES, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sAcceptance, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				

				
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_LIMITS, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sLos, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				


				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CHSCOPE, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				
				
				oCellc = new Cell(new Phrase(sCis, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_CHCOMM, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);						
				
				oCellc = new Cell(new Phrase(sCfc, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);				
				
				oCellc = new Cell(new Phrase(QuotationConstants.QUOTATION_GENERAL, oTableSmallBoldText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oCellc = new Cell(new Phrase(sGeneral, oTableSmallText));
				oCellc.setColspan(4);
				oCellc.setBorder(0);
				oCellc.setHorizontalAlignment(Element.ALIGN_LEFT);
				oTable.addCell(oCellc);
				
				oParagraph = new Paragraph();
				oParagraph.add(oTable);
				oDocument.add(oParagraph);

			
				oDocument.close();

		} finally{
			
		}
		
		return oByteArrayOutputStream;
	}

	
private Paragraph addCustomerInformation(EnquiryDto oEnquiryDto) throws DocumentException {
	Paragraph oParagraph = new Paragraph();
	PdfPTable oMainTable =null;
	PdfPCell oCell = null;
	PdfPCell oFirstTableCell =null;
	PdfPTable oFirstTable =null;

	PdfPCell oSecondTableCell =null;
	PdfPTable oSecondTable =null;
	if(oEnquiryDto!=null)
	{
		


        
       
        oMainTable = new PdfPTable(2);
        oMainTable.setWidthPercentage(95f); 
        oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
   
        oFirstTableCell= new PdfPCell();
        oFirstTableCell.setBorder(Rectangle.NO_BORDER);
        oFirstTable= new PdfPTable(2);
   
        oFirstTable.setWidthPercentage(85f);
        oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        if(oEnquiryDto.getCustomerName()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOATION_CUST_NAME, oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        

        
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getCustomerName(), oTableSmallText));
        oCell.setBorder(1);
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		oFirstTable.addCell(oCell);
        }

        if(oEnquiryDto.getEndUser()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOATION_ENDUSER_NAME,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getEndUser(), oTableSmallText));
        oCell.setBorder(1);
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        }
        

        if(oEnquiryDto.getRevision()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_REVISION.toLowerCase(),oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getRevision(), oTableSmallText));
        oCell.setBorder(1);
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		oFirstTable.addCell(oCell);
        }

        oFirstTableCell.addElement(oFirstTable);
        oMainTable.addCell(oFirstTableCell);
        
        oSecondTableCell = new PdfPCell();
        oSecondTableCell.setBorder(Rectangle.NO_BORDER);

        oSecondTable = new PdfPTable(2);
        oSecondTable.setWidthPercentage(85f);
        oSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        if(oEnquiryDto.getProjectName()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_PROJNAME,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getProjectName(), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        }
        if(oEnquiryDto.getEnquiryReferenceNumber()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DESIGNREF,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getEnquiryReferenceNumber(), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        }
        if(oEnquiryDto.getLastModifiedDate()!="")
        {
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DATEOFISSUE,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(oEnquiryDto.getLastModifiedDate(), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        }
        oSecondTableCell.addElement(oSecondTable);
        oMainTable.addCell(oSecondTableCell);
        oParagraph.add(oMainTable);
        
		
	}
	return oParagraph;
}


private Table addCommercialOffer(EnquiryDto enquiryDto) throws DocumentException {
	Table oTable = new Table(9);
	oTable.setWidth(85);
	oTable.setBorder(0);
	oTable.setSpacing((float).75);
	oTable.setPadding((float).75);
	oTable.setBorder(0);
	oTable.setBackgroundColor(new Color(255,255,255));
	
	



	Cell oCell =null;
	Double quantity=new Double(0);
	BigDecimal totalprice=new BigDecimal("0");
	DecimalFormat oDecimalFormat = new DecimalFormat("###,###");
	DecimalFormat oDecimalFormat1 = new DecimalFormat("#0");
	RatingDto oRatingDto =null;
	TechnicalOfferDto oTechnicalOfferDto =null;
	ArrayList alRatings =null;
	ArrayList alAddOnList = null;
	AddOnDto oAddOnDto =null;
	if(enquiryDto.getRatingObjects()!=null)
	{
		alRatings = enquiryDto.getRatingObjects();
		
		if(alRatings.size()>0)
		{
			for(int i = 0;i<alRatings.size();i++)
			{	
				oRatingDto = (RatingDto)alRatings.get(i);
				oTechnicalOfferDto = oRatingDto.getTechnicalDto();
				
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_ITEMNO, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_RATEDOUTPUT, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_UNITS, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_POLE, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_VOLTAGE, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_FRAME, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				

				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_UNITPRICES, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_QUANTITY, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_TOTALPRICE, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

				
				oCell = new Cell(new Phrase(String.valueOf(i+1), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

				
				oCell = new Cell(new Phrase(oRatingDto.getKw(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(oRatingDto.getRatedOutputUnit(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(oRatingDto.getPoleName(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

				oCell = new Cell(new Phrase(oRatingDto.getVolts(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(oTechnicalOfferDto.getFrameSize(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				oCell = new Cell(new Phrase(oTechnicalOfferDto.getUnitpriceMultiplier_rsm(), oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);
				
				String qnt=String.valueOf(oRatingDto.getQuantity());
				oCell = new Cell(new Phrase(qnt, oTableSmallText));
				oCell.setBorder(0);
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setBackgroundColor(new Color(242,242,255));
				oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

                 String totalpriceMultiplier_rsm=oTechnicalOfferDto.getTotalpriceMultiplier_rsm();
                 
                 oCell = new Cell(new Phrase(oTechnicalOfferDto.getTotalpriceMultiplier_rsm(), oTableSmallText));
                 oCell.setBorder(0);
                 oCell.setBackgroundColor(new Color(242,242,255));
                 oCell.setBackgroundColor(new Color(242,242,255));
                 oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				oTable.addCell(oCell);

			    if(totalpriceMultiplier_rsm!="" && totalpriceMultiplier_rsm!=QuotationConstants.QUOTATION_STRING_ZERO)
			    {
			        if(totalpriceMultiplier_rsm.indexOf(",") != QuotationConstants.QUOTATION_LITERAL_MINUSONE){
			      	 String unitprice=totalpriceMultiplier_rsm.replaceAll(",", "");
			      	 BigDecimal oBigDecimal=new BigDecimal(unitprice);
			      	totalprice=totalprice.add(oBigDecimal);
			        }
			        else{
			        	 BigDecimal oBigDecimal=new BigDecimal(totalpriceMultiplier_rsm);
					      	totalprice=totalprice.add(oBigDecimal);
			        	
			        }

			    	
			    }
			    if(qnt!="" && qnt!=QuotationConstants.QUOTATION_STRING_ZERO)
			   {
				   quantity=quantity+Double.parseDouble(String.valueOf(qnt));
			   }
			    

				alAddOnList = oRatingDto.getAddOnList();

		
		
				for(int j=0;j<alAddOnList.size();j++)
				{
					oAddOnDto = (AddOnDto) alAddOnList.get(j);
					
					if(j==0)
					{
						oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_ADDONNO, oTableSmallText));
						oCell.setBorder(0);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						oCell.setColspan(2);
						oTable.addCell(oCell);
						
						oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_ADDONTYPE, oTableSmallText));
						oCell.setBorder(0);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						oCell.setColspan(2);
						oTable.addCell(oCell);
						
						oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_QUANTITY, oTableSmallText));
						oCell.setBorder(0);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						oCell.setColspan(2);
						oTable.addCell(oCell);
						
						oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_UNITPRICES, oTableSmallText));
						oCell.setBorder(0);
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setBackgroundColor(new Color(242,242,255));
						oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						oCell.setColspan(3);
						oTable.addCell(oCell);
						

					}
					
					
					oCell = new Cell(new Phrase(String.valueOf(j+1), oTableSmallText));
					oCell.setBorder(0);
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					oCell.setColspan(2);
					oTable.addCell(oCell);
					
					oCell = new Cell(new Phrase(oAddOnDto.getAddOnTypeText(), oTableSmallText));
					oCell.setBorder(0);
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					oCell.setColspan(2);
					oTable.addCell(oCell);
					
					oCell = new Cell(new Phrase(String.valueOf(oAddOnDto.getAddOnQuantity()), oTableSmallText));
					oCell.setBorder(0);
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					oCell.setColspan(2);
					oTable.addCell(oCell);
					
					oCell = new Cell(new Phrase(String.valueOf(oAddOnDto.getAddOnUnitPrice()), oTableSmallText));
					oCell.setBorder(0);
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setBackgroundColor(new Color(242,242,255));
					oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					oCell.setColspan(3);
					oTable.addCell(oCell);	
					
				}

			

			}

			oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_EMPTY, oTableSmallText));
			oCell.setBorder(0);
			oCell.setColspan(6);
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);
			
			
			oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_TOTAL, oTableSmallText));
			oCell.setBorder(0);
			
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);
			
			oCell = new Cell(new Phrase(oDecimalFormat1.format(quantity), oTableSmallText));
			oCell.setBorder(0);
			
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);

			oCell = new Cell(new Phrase(oDecimalFormat.format(totalprice), oTableSmallText));
			oCell.setBorder(0);
			
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);
			
			
			oCell = new Cell(new Phrase(QuotationConstants.QUOTATION_AMOUNTINWORDS, oTableSmallText));
			oCell.setBorder(0);
			
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);

			oCell = new Cell(new Phrase(CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(totalprice)), oTableSmallText));
			oCell.setBorder(0);
			oCell.setColspan(8);
			
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setBackgroundColor(new Color(242,242,255));
			oCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			oTable.addCell(oCell);
			

			

		}
	}

return oTable;
}
private Paragraph addTotalDetails(EnquiryDto enquiryDto) throws DocumentException {
	Table oTable = new Table(4);
	Table oLeftTable = new Table(1);
	Table oRightTable=new Table(1);
	oTable.setWidth(85);
	oTable.setBorder(0);
	oTable.setSpacing((float).75);
	oTable.setPadding((float).75);
	oTable.setBorder(0);
	oTable.setBackgroundColor(new Color(255,255,255));
	
	oLeftTable.setWidth(85);
	oLeftTable.setBorder(0);
	oLeftTable.setSpacing((float).75);
	oLeftTable.setPadding((float).75);
	oLeftTable.setBorder(0);

	oRightTable.setWidth(85);
	oRightTable.setBorder(0);
	oRightTable.setSpacing((float).75);
	oRightTable.setPadding((float).75);
	oRightTable.setBorder(0);
	Paragraph oParagraph=new Paragraph();
	PdfPTable oMainTable =null; 
	PdfPCell oFirstTableCell =null;
	PdfPTable oFirstTable=null;
	PdfPCell oCell = null;
	String sDateOfDispatch=null;
	String sAdvancePayment=null;
	String sPaymentTerms=null;
	String sDateOfComm=null;


	if(enquiryDto.getRatingObjects()!=null)
	{
		
		oMainTable= new PdfPTable(2);
		oMainTable.setWidthPercentage(95f); 
		oMainTable.setHorizontalAlignment(Element.ALIGN_CENTER);
       			       

		oFirstTableCell= new PdfPCell();
		oFirstTableCell.setBorder(Rectangle.NO_BORDER);
   
		oFirstTable = new PdfPTable(2);
        
		oFirstTable.setWidthPercentage(85f);
		oFirstTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        
		
	     
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_EXPDELIVERYDATE,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(String.valueOf(enquiryDto.getExpectedDeliveryMonth()), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
       
		oFirstTable.addCell(oCell);
       
	
		oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_WARRANTYDATEDISP,oTableSmallText));
		oCell.setBorder(0);
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        sDateOfDispatch=enquiryDto.getWarrantyDispatchDate()+QuotationConstants.QUOTATION_MONTHS;
        oCell = new PdfPCell(new Phrase(String.valueOf(sDateOfDispatch), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
       
		oFirstTable.addCell(oCell);
		
		oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_ADVPAYMENT,oTableSmallText));
		oCell.setBorder(0);
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oFirstTable.addCell(oCell);
        sAdvancePayment=enquiryDto.getAdvancePayment()+QuotationConstants.QUOTATION_STRING_PERCENTAGE;
        oCell = new PdfPCell(new Phrase(sAdvancePayment, oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
       
		oFirstTable.addCell(oCell);
    		
        oFirstTableCell.addElement(oFirstTable);
        
        oMainTable.addCell(oFirstTableCell);



        PdfPCell oSecondTableCell = new PdfPCell();
        oSecondTableCell.setBorder(Rectangle.NO_BORDER);
        PdfPTable oSecondTable = new PdfPTable(2);
        oSecondTable.setWidthPercentage(85f);
        oSecondTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_DELTYPE,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        oCell = new PdfPCell(new Phrase(enquiryDto.getDeliveryType(), oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);

        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_WARRANTYDATECOMM,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        sDateOfComm=enquiryDto.getWarrantyCommissioningDate()+" Months";
        oCell = new PdfPCell(new Phrase(sDateOfComm, oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setBackgroundColor(new Color(242,242,255));
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);

        oCell = new PdfPCell(new Phrase(QuotationConstants.QUOTATION_PAYMENTTERMS,oTableSmallText));
        oCell.setBorder(0);
        oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        sPaymentTerms=enquiryDto.getPaymentTerms()+QuotationConstants.QUOTATION_BEFOREDISP;
        oCell = new PdfPCell(new Phrase(sPaymentTerms, oTableSmallText));
        oCell.setBorder(1);
        oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setBackgroundColor(new Color(242,242,255));
		oCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        oSecondTable.addCell(oCell);
        
        oSecondTableCell.addElement(oSecondTable);
        
        oMainTable.addCell(oSecondTableCell);
        
        oParagraph.add(oMainTable);

	}

return oParagraph;
}
/**
 * Inner class to add header and footer on every pdf page implementing PdfPageEventHelper
 * @param conn Connection object to connect to database
 * @param EnquiryDto oEnquiryDto object having enquiry details
 * @return Returns the boolean
 * @throws Exception If any error occurs during the process
 * @author 900010540
 * Created on: 19 March 2019
 */

class HeaderFooterPageEvent extends PdfPageEventHelper {
    private Image oBmp1 =null;
    private Image oBmp2 =null;
   
   
   @Override
   /**
    * overridden super class method to add header and footer on every pdf page
    * @param oWriter PdfWriter object to add content to pdf
    * @param oDocument Document object of pdf writer  
    * @author 610092227 (Kalyani U)
    * Created on: 22 June 2019
    */

   public void onEndPage(PdfWriter oWriter, Document oDocument) {
       addHeaderFooter(oWriter,oDocument);
     
   }
   
   
   /**
    * Method called from overriden method of helper class to include logic of adding header and footer on every pdf page
    * @param oWriter PdfWriter object to add content to pdf
    * @param oDocument Document object of pdf writer  
    * @author 610092227 (Kalyani U)
    * Created on: 22 June 2019
    */
   private void addHeaderFooter(PdfWriter oWriter,  Document oDocument){
    
   	PdfContentByte oPdfContentByte1 = null;
   	PdfTemplate oPdfTemplate1 = null;
   	Phrase oPhrase1 = null;
   	PdfContentByte oPdfContentByte2 = null;
   	PdfTemplate oPdfTemplate2 = null;
   	Phrase oPhrase2 = null;
   	
       try {
                     
           String sImagePath1=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
   				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_MELOGOIMG;
   		
   		String sImagePath2=sFullPath.replace(QuotationConstants.QUOTATION_SLASH, File.separator) + File.separator
   				+QuotationConstants.QUOTATION_STRING_HTML + File.separator + QuotationConstants.QUOTATION_STRING_IMAGES + File.separator + QuotationConstants.QUOTATION_REGALLOGOIMG;

           
           oBmp1 = Image.getInstance(sImagePath1);
			oBmp1.scalePercent(QuotationConstants.QUOTATION_LITERAL_20);
			oBmp1.setAbsolutePosition(QuotationConstants.QUOTATION_LITERAL_TEN,QuotationConstants.QUOTATION_LITERAL_780);

			oPdfContentByte1 = oWriter.getDirectContent();
			oPdfContentByte1.addImage(oBmp1);
			 oBmp2 = Image.getInstance(sImagePath2);
			 oBmp2.scalePercent(QuotationConstants.QUOTATION_LITERAL_8);
			 oBmp2.setAbsolutePosition(QuotationConstants.QUOTATION_LITERAL_485, QuotationConstants.QUOTATION_LITERAL_20);
			 oPdfContentByte2 = oWriter.getDirectContent();
			 oPdfContentByte2.addImage(oBmp2);           
			
       } catch(DocumentException de) {
           throw new ExceptionConverter(de);
       } catch (IOException e) {
           throw new ExceptionConverter(e);
       }
   }

 

   
}




	
}



