/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: MasterDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: This Dao Inteface is used to declare methods which will update the changes in Masters and also used to manage Master Data.
 *       This interface is implemented by --> MasterDaoImpl
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 1, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.MasterDto;
import in.com.rbc.quotation.admin.dto.MastersDataDto;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

public interface MasterDao {
	/**
	 * gets the LocationList
	 * @param conn Connection object to connect to database
	 * @return Returns Arraylist object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getLocationList(Connection conn) throws Exception;
	
	/**
	 * gets the CustomerList
	 * @param conn Connection object to connect to database
	 * @return Returns Arraylist object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getCustomerList(Connection conn,String sLocation) throws Exception;
	
	/**
	 * gets the SalemangerLis
	 * @param conn Connection object to connect to database
	 * @return Returns Arraylist object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getSalemangerList(Connection conn,String sRegion) throws Exception;
	
	/**
	 * gets the Region List 
	 * @param conn Connection object to connect to database
	 * @return Returns Arraylist object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getRegionList(Connection conn) throws Exception;

	/**
	 * gets the Status Master List
	 * @param conn Connection object to connect to database
	 * @return Returns Arraylist object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getStatusList(Connection conn) throws Exception;
    /**
	 * gets the Master List
	 * @param conn Connection object to connect to database
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getMastersList(Connection conn) throws Exception;
    /**
	 * gets the Option List of Engineering Data for Selected Attributes
	 * @param conn Connection object to connect to database
	 * @return Returns ArraList object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
    public ArrayList getOptionsList(Connection conn, 
                                    int masterTypeId, 
                                    boolean canIncludeInActive) throws Exception;
    /**
	 * gets the Search Results List for Masters 
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public Hashtable getMasterListResults(Connection connection, MasterDto masterDto, ListModel listModel)throws Exception;
	/**
	 * gets the Details of Master
	 * @param conn Connection object to connect to database
	 * @param masterDto MasterDto object having master details
	 * return Returns  MasterDto object having master details
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public MasterDto getMasterDetails(Connection connection, MasterDto masterDto)throws Exception;
	/**
	 * gets the Delete the Selected Master
	 * @param conn Connection object to connect to database
	 * @param masterDto MasterDto object having master details
	 * @return Returns String object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public String deleteMaster(Connection connection, MasterDto masterDto)throws Exception;
	/**
	 * Inserts or Updates Master Details
	 * @param conn Connection object to connect to database
	 * @param masterDto MasterDto object having enquiry details
	 * @return Returns String object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public String addOrUpdateMaster(Connection connection, MasterDto masterDto,UserDto userDto)throws Exception;
	/**
	 * gets the Master List for Print or Export
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @param masterDto MasterDto object having master details
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public ArrayList getMasterList(Connection connection, MasterDto masterDto, String query)throws Exception;
	   /**
	 * gets the Search Results List for Masters Data
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public Hashtable getMastersDataList(Connection connection, MastersDataDto mastersDataDto, ListModel listModel)throws Exception;
	   /**
	 * gets the Search Results List for Masters 
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public MastersDataDto getMastersDataDetails(Connection connection, MastersDataDto mastersDataDto)throws Exception;
	   /**
	 * gets the Search Results List for Masters 
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public String addOrUpdateMasterData(Connection connection, MastersDataDto mastersDataDto, UserDto userDto)throws Exception;
	   /**
	 * gets the Search Results List for Masters 
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public String deleteMasterData(Connection connection, MastersDataDto mastersDataDto)throws Exception;
	   /**
	 * gets the Search Results List for Master's Data for Print or Export
	 * @param conn Connection object to connect to database
	 * @param masterTypeId selected MasterId 
	 * @param canIncludeInActive Boolean object that defines whethere the results can include Inactive data or not
	 * @return Returns HashTable object
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: October 13, 2008
	 */
	public ArrayList getMasterDataList(Connection connection, MastersDataDto mastersDataDto, String query)throws Exception;
	public boolean updateMasterDataStatusValue(Connection conn, MastersDataDto mastersDataDto, UserDto userDto)throws Exception;
	
	/**
	 * Retrieves the list of four Years
	 * @param conn Connection object to connect to database
	 * @return Returns the list of requests
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: October 21, 2008
	 */
	public ArrayList getYears(Connection conn) throws Exception;
	/**
	 * gets the Details of Master
	 * @param conn Connection object to connect to database
	 * @param masterDto MasterDto object having master details
	 * return Returns  MasterDto object having master details
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public Hashtable getMasterListResults(Connection connection, MasterDto masterDto, ListModel listModel, String sQuery)throws Exception;
	
	/**
	 * gets the RegionID
	 * @param conn Connection object to connect to database
	 * @param String  String object having salesmanager name
	 * return RegionID
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public int getRegionIDBySalesManager(Connection conn,String sSalesManager)throws Exception;
	
	public int getRegionIDBySalesManagerId(Connection conn,String sSalesManager)throws Exception;
	
	/**
	 * gets the LocationID
	 * @param conn Connection object to connect to database
	 * @param int  having customer id
	 * return RegionID
	 * @throws Exception If any error occurs during the process
	 * @author 100006718
	 * Created on: Sep 30, 2008
	 */
	public int getLocationID(Connection conn,int iCustomerID)throws Exception;

	/**
	 * gets the EMPLOYEESLIST
	 * @param conn Connection object to connect to database
	 * @param int  having customer id
	 * return EmployeeId,EmployeeName
	 * @throws Exception If any error occurs during the process
	 * @author 
	 * Created on: Jan 11 2018
	 */
/*	public ArrayList getEmployeesList(Connection conn)throws Exception;
*/
	
    
}
