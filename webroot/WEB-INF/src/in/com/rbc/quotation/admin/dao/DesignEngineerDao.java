/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: DesignEngineerDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 17, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.DesignEngineerDto;
import in.com.rbc.quotation.common.utility.ListModel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public interface DesignEngineerDao {
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public Hashtable getDesignEngineerSearchResult(Connection conn, DesignEngineerDto oDesignEngineerDto, ListModel oListModel) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public boolean createDesignEngineer(Connection conn, DesignEngineerDto oDesignEngineerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public DesignEngineerDto getDesignEngineerInfo(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public boolean saveDesignEngineerVlaue(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public boolean deleteDesignEngineer(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @param sWhereQuery
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ArrayList exportDesignEngineerSearchResult(Connection conn , DesignEngineerDto oDesignEngineerDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception ;
	
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Feb 10, 2009
	 */
	public int isDEAlreadyExists(Connection conn , DesignEngineerDto oDesignEngineerDto) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oDesignEngineerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Feb 10, 2009
	 */
	public boolean updateExistDE(Connection conn , DesignEngineerDto oDesignEngineerDto) throws Exception ;

}
