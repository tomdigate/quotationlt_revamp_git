/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: TechnicalDataAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Nov 19, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.admin.dao.TechnicalDataDao;
import in.com.rbc.quotation.admin.dto.TechnicalDataDto;
import in.com.rbc.quotation.admin.form.TechnicalDataForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.dto.UserDto;

/**
 * <h3>Class Name:-TechnicalDataAction</h3> 
 * <br> This class upload bulk data 
 * <br>	-->viewbulkdata , 
 * <br>	--> and 
 * <br>This Class is Extends from BaseAction Class .
 * @author 900008798 (Abhilash Moola)
 *
 */
public class TechnicalDataAction extends BaseAction {
	
	public ActionForward viewbulkdata(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "viewbulkdata";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		TechnicalDataForm oTechnicalDataForm = (TechnicalDataForm) actionForm;
		if(oTechnicalDataForm ==  null) {
			oTechnicalDataForm = new TechnicalDataForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		Hashtable htSearchResultsList =null; //Object of Hashtable to store different values
		ListModel oListModel=null;
		TechnicalDataDto oTechnicalDataDto = null;
		DaoFactory oDaoFactory=null;
		TechnicalDataDao oTechnicalDataDao = null;
		HttpSession session= request.getSession();
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "technicalDataForm", 
			 "technicalDataForm.invoke", 
			 "viewbulkdata");
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oTechnicalDataDao = oDaoFactory.getTechnicalDataDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oTechnicalDataDto = new TechnicalDataDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oTechnicalDataDto , oTechnicalDataForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList =oTechnicalDataDao.getTechnicalSearchResult(conn,oTechnicalDataDto,oListModel);
			
			
			if(htSearchResultsList != null ) {
				oTechnicalDataDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_TDDTO,oTechnicalDataDto); 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		        request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
		        session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getLocationSearchResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
			}else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
			}
		}catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewbulkdata  Result.", ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
		
	}
	
	/**
	 * 
	 * <h3>Method Name:-addNewTechnicalData</h3> 
	 * <br>Method Used for to open the add new data form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm 	ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 19, 2020
	 */
	public ActionForward addNewTechnicalData(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
    	// Method name Set for log file usage ;
    	String sMethodname = "addNewTechnicalData";
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
    	TechnicalDataForm oTechnicalDataForm = (TechnicalDataForm) actionForm;
		if(oTechnicalDataForm ==  null) {
			oTechnicalDataForm = new TechnicalDataForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    DaoFactory oDaoFactory=null;  //DaoFactory oDaoFactory
	    TechnicalDataDao oTechnicalDataDao = null;
	    TechnicalDataDto oTechnicalDataDto = null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		   
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "addNewTechnicalData Result.", ExceptionUtility.getStackTraceAsString(e));
	    	/* Setting error details to request to display the same in Exception page */
	    		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        /* Forwarding request to Error page */
	    		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	
	/**
	 * 
	 * <h3>Method Name:-updateTechnicalData</h3> 
	 * <br>Method Used for to open the add new data form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm 	ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 19, 2020
	 */
	public ActionForward updateTechnicalData(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
    	// Method name Set for log file usage ;
    	String sMethodname = "updateTechnicalData";
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
    	TechnicalDataForm oTechnicalDataForm = (TechnicalDataForm) actionForm;
		if(oTechnicalDataForm ==  null) {
			oTechnicalDataForm = new TechnicalDataForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    DaoFactory oDaoFactory=null;  //DaoFactory oDaoFactory
	    TechnicalDataDao oTechnicalDataDao = null;
	    TechnicalDataDto oTechnicalDataDto = null;
	    String operation = null;
	    boolean isUpdated = false;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		    oTechnicalDataDao = oDaoFactory.getTechnicalDataDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oTechnicalDataDto = new TechnicalDataDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oTechnicalDataDto , oTechnicalDataForm);
			
			operation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
			if(operation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
				isUpdated = oTechnicalDataDao.updateTechnicalData(conn, oTechnicalDataDto);
				if(isUpdated)
             		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_TECHNICALDATA+" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);
			}else {
				isUpdated = oTechnicalDataDao.addNewTechnicalData(conn,oTechnicalDataDto);
				if(isUpdated)
             	   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oTechnicalDataDto.getMANUFACTURING_LOCATION() +"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);
			}
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_TECHNICALDATA_URL); 
		    
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "updateTechnicalData ..", ExceptionUtility.getStackTraceAsString(e));
	    	/* Setting error details to request to display the same in Exception page */
	    		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        /* Forwarding request to Error page */
	    		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	
	
	/**
	 * <h3>Method Name:-editTechnicalData</h3> 
	 * <br>Method Used for to open the Update exists Location form Page
	 * <br>while opening this page its load the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 23, 2020
	 */
	public ActionForward editTechnicalData(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response) throws Exception {
		/*
       * Setting user's information in request, using the header 
       * information received through request
       */
  		setRolesToRequest(request);
  	
  	// Method name Set for log file usage ;
  		String sMethodname = "editTechnicalData";
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
  		
  	// Get Form objects from the actionForm
  		TechnicalDataForm oTechnicalDataForm =(TechnicalDataForm)actionForm;
	    	if(oTechnicalDataForm ==  null) {
	    		oTechnicalDataForm = new TechnicalDataForm();
	    	}
	    	Connection conn = null;  // Connection object to store the database connection
	    	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    	UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
	    	DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
	    	TechnicalDataDao oTechnicalDataDao = null;
		    TechnicalDataDto oTechnicalDataDto = null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
	    	/* Instantiating the DBUtility object */
  			oDBUtility = new DBUtility();
  			/* Creating an instance of of DaoFactory  */
  			oDaoFactory = new DaoFactory();
  			oTechnicalDataDao = oDaoFactory.getTechnicalDataDao();
  			/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            oTechnicalDataDto = new TechnicalDataDto();
            oDBUtility = new DBUtility();
             /* Copying the Form Object into DTO Object via Propertyutils*/
             PropertyUtils.copyProperties( oTechnicalDataDto,oTechnicalDataForm);
             oTechnicalDataDto.setSERIAL_ID(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_TECHNICADATAID));
             oTechnicalDataDto = oTechnicalDataDao.getTechnicalDataInfo(conn,oTechnicalDataDto);
	    	 /*
	    	  * setLocationList to dispaly the drop down values in location field with appropriate value  
	    	  * setOperation is used to set the operation values as Update for diff in JSP files to dispaly diff button and header label
	    	  */
             
             oTechnicalDataDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
		        
	    	 /* Copying the Form Object into DTO Object via Propertyutils*/
	         PropertyUtils.copyProperties( oTechnicalDataForm ,oTechnicalDataDto);
		         
	     }catch(Exception e) {
		      /*
		       * Logging any exception that raised during this activity in log files using Log4j
		       */
		  		LoggerUtility.log("DEBUG", this.getClass().getName(), "editTechnicalData Result.", ExceptionUtility.getStackTraceAsString(e));
		      
		      /* Setting error details to request to display the same in Exception page */
		      	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		      /* Forwarding request to Error page */
		      	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	      }finally {
	  	       	 /* Releasing/closing the connection object */
		         oDBUtility.releaseResources(conn);  	      
	      }
	       LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}

	/**
	 * <h3>Method Name:-deleteTechnicalData</h3> 
	 * <br>Method Used for to Delete th exists data
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 23, 2020
	 */
	public ActionForward deleteTechnicalData(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
	  	// Method name Set for log file usage ;
  		String sMethodname = "deleteTechnicalData";
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
	   /*
        * Setting user's information in request, using the header 
        * information received through request
        */
	  	setRolesToRequest(request);
	  	 // Get Form objects from the actionForm
  		TechnicalDataForm oTechnicalDataForm =(TechnicalDataForm)actionForm;
	    	if(oTechnicalDataForm ==  null) {
	    		oTechnicalDataForm = new TechnicalDataForm();
	    	}
	  	 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     TechnicalDataDto oTechnicalDataDto = null;
	     boolean isdeleted = false ;
	     DaoFactory oDaoFactory =null;
	     TechnicalDataDao oTechnicalDataDao = null;
	     
	   	 try {
	
     		/* Instantiating the DBUtility object */
  			oDBUtility = new DBUtility();
  			/* Creating an instance of of DaoFactory  */
  			oDaoFactory = new DaoFactory();
  			oTechnicalDataDao = oDaoFactory.getTechnicalDataDao();
  			/* Retrieving the database connection using DBUtility.getDBConnection method */
            oTechnicalDataDto = new TechnicalDataDto();
            
  			
  			if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	resetToken(request);// Reset the Token Request ...
	  			/* Retrieving the database connection using DBUtility.getDBConnection method */
	  			conn = oDBUtility.getDBConnection();
	  			/* Copying the Form Object into DTO Object via Propertyutils*/
	  			PropertyUtils.copyProperties(oTechnicalDataDto , oTechnicalDataForm);
	  			oDBUtility.setAutoCommitFalse(conn);
	  			isdeleted =    oTechnicalDataDao.deleteTechnicalDataValue(conn,oTechnicalDataDto);
	  			if(isdeleted) {
	  				oDBUtility.commit(conn);
		            LoggerUtility.log("INFOR", this.getClass().getName(), "Deleted  Location --","Successfully" );
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oTechnicalDataDto.getMANUFACTURING_LOCATION() +"\" "+QuotationConstants.QUOTATION_REC_DELSUCC);
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_TECHNICALDATA_URL); 
	  			}else {
	    			oDBUtility.rollback(conn);
	    		}
	    		oDBUtility.setAutoCommitTrue(conn);
  			}else {
    			LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_MSG_TOKLOC );
 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
    		}	
	   	 }catch (SQLException se) {
				/*
				 * if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
				 */
				LoggerUtility.log("DEBUG", this.getClass().getName(), "Technical Data delete Result Error Code .", String.valueOf(se.getErrorCode()));
				/*
				 * if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
				 */
				if ( se.getErrorCode() == 2292 ) {
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oTechnicalDataDto.getMANUFACTURING_LOCATION() +"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_TECHNICALDATA_URL); 
	                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
				}
     	}catch(Exception e) {
     		/*
           * Logging any exception that raised during this activity in log files using Log4j
           */
      		LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete result ", ExceptionUtility.getStackTraceAsString(e));
          
          /* Setting error details to request to display the same in Exception page */
          	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
          /* Forwarding request to Error page */
          	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
     	 }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	      
     	 }
     	 LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
     	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * 
	 * <h3>Method Name:-updateTechnicalData</h3> 
	 * <br>Method Used for to open the add new data form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm 	ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 19, 2020
	 */
	public ActionForward bulkUploadTechnicalData(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
    	// Method name Set for log file usage ;
    	String sMethodname = "bulkUploadTechnicalData";
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
    	TechnicalDataForm oTechnicalDataForm = (TechnicalDataForm) actionForm;
		if(oTechnicalDataForm ==  null) {
			oTechnicalDataForm = new TechnicalDataForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    DaoFactory oDaoFactory=null;  //DaoFactory oDaoFactory
	    InputStream inputStream = null;
	    POIFSFileSystem fileSystem = null;
	    Workbook workbook = null;
	    TechnicalDataDto otempTechnicalDataDto = null;
	    List<TechnicalDataDto> oListTechnicalDataDto = new ArrayList<TechnicalDataDto>();
	    TechnicalDataDao oTechnicalDataDao = null;
	    boolean isValid = true;
	    boolean is_Uploaded = false;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		    oTechnicalDataDao = oDaoFactory.getTechnicalDataDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
		    
		    FormFile ffSiteLoadFile = oTechnicalDataForm.getTechnicalDataFile();
		    
		    inputStream = ffSiteLoadFile.getInputStream();
		    if(ffSiteLoadFile.toString().endsWith("xls")) 
		    {
		    fileSystem = new POIFSFileSystem(inputStream);
		    isValid = verifyExcelData(fileSystem);
		    
		    if(!isValid) {
			    if (fileSystem != null) {
			    	/* Retrieving the workbook object */
					HSSFWorkbook workBook = new HSSFWorkbook(fileSystem);
					/* Retrieving the work sheet object */
					HSSFSheet sheet = workBook.getSheetAt(0);
					/* Retrieving the rows collection */
					int rowcount = sheet.getLastRowNum();
					Iterator<Row> rowIterator = sheet.rowIterator();
					String cellValue = "";
					
		            while (rowIterator.hasNext()) 
		            {
		                Row row = rowIterator.next();
		                	 if(row.getRowNum()!=0) {
		 	                	
		 	                	//For each row, iterate through all the columns
		 		                Iterator<Cell> cellIterator = row.cellIterator();
		 		                Cell cell = null;
		 		                otempTechnicalDataDto = new TechnicalDataDto();
		 		                while (cellIterator.hasNext()) 
		 		                {
		 		                	cell = cellIterator.next();
		 		                	cellValue = cell.toString();
		 		                	int cell_value = cell.getColumnIndex();
		 		                    switch(cell_value) {
		 		                    case 0:
		 		                    otempTechnicalDataDto.setMANUFACTURING_LOCATION(cellValue);
		 		                    break;
		 		                    case 1:
		 			                    otempTechnicalDataDto.setPRODUCT_GROUP(cellValue);	
		 			                    break;
		 		                    case 2:
		 			                    otempTechnicalDataDto.setPRODUCT_LINE(cellValue);
		 			                    break;
		 		                    case 3:
		 			                    otempTechnicalDataDto.setSTANDARD(cellValue);	
		 			                    break;
		 		                    case 4:
		 			                	 otempTechnicalDataDto.setPOWER_RATING_KW(cellValue);
		 			                	 break;
		 		                    case 5:
		 			                	 otempTechnicalDataDto.setFRAME_TYPE(cellValue);
		 			                	 break;
		 		                    case 6:
		 			                	 otempTechnicalDataDto.setNUMBER_OF_POLES(cellValue);
		 			                	 break;
		 		                    case 7:
		 			                	 otempTechnicalDataDto.setMOUNTING_TYPE(cellValue);
		 			                	 break;
		 		                    case 8:
		 			                	 otempTechnicalDataDto.setTB_POSITION(cellValue);
		 			                	 break;
		 		                    case 9:
		 			                	 otempTechnicalDataDto.setMODEL_NO(cellValue);
		 			                	 break;
		 		                    case 10:
		 			                	 otempTechnicalDataDto.setVOLTAGE_V(cellValue);
		 			                	 break;
		 		                    case 11:
		 			                	 otempTechnicalDataDto.setPOWER_RATING_HP(cellValue);
		 			                	 break;
		 		                    case 12:
		 			                	 otempTechnicalDataDto.setFREQUENCY_HZ(cellValue);
		 			                	 break;
		 		                    case 13:
		 			                	 otempTechnicalDataDto.setCURRENT_A(cellValue);
		 			                	 break;
		 		                    case 14:
		 			                	 otempTechnicalDataDto.setSPEED_MAX(cellValue);
		 			                	 break;
		 		                    case 15:
		 			                	 otempTechnicalDataDto.setPOWER_FACTOR(cellValue);	
		 			                	 break;
		 		                    case 16:
		 			                	 otempTechnicalDataDto.setEFFICIENCY(cellValue);
		 			                	 break;
		 		                    case 17:
		 			                	 otempTechnicalDataDto.setELECTRICAL_TYPE(cellValue);
		 			                	 break;
		 		                    case 18:
		 			                	 otempTechnicalDataDto.setIP_CODE(cellValue);
		 			                	 break;
		 		                    case 19:
		 			                	 otempTechnicalDataDto.setINSULATION_CLASS(cellValue);	
		 			                	 break;
		 		                    case 20:
		 			                	 otempTechnicalDataDto.setSERVICE_FACTOR(cellValue);
		 			                	 break;
		 		                    case 21:
		 			                	 otempTechnicalDataDto.setDUTY(cellValue);
		 			                	 break;
		 		                    case 22:
		 			                	 otempTechnicalDataDto.setNUMBER_OF_PHASES(cellValue);
		 			                	 break;
		 		                    case 23:
		 			                	 otempTechnicalDataDto.setMAX_AMBIENT_C(cellValue);	
		 			                	 break;
		 		                    case 24:
		 			                	 otempTechnicalDataDto.setDE_BEARING_SIZE(cellValue);	
		 			                	 break;
		 		                    case 25:
		 			                	 otempTechnicalDataDto.setDE_BEARING_TYPE(cellValue);	
		 			                	 break;
		 		                    case 26:
		 			                	 otempTechnicalDataDto.setODE_BEARING_SIZE(cellValue);	
		 			                	 break;
		 		                    case 27:
		 			                	 otempTechnicalDataDto.setODE_BEARING_TYPE(cellValue);	
		 			                	 break;
		 		                    case 28:
		 			                	 otempTechnicalDataDto.setENCLOSURE_TYPE(cellValue);	
		 			                	 break;
		 		                    case 29:
		 			                	 otempTechnicalDataDto.setMOTOR_ORIENTATION(cellValue);
		 			                	 break;
		 		                    case 30:
		 			                	 otempTechnicalDataDto.setFRAME_MATERIAL(cellValue);
		 			                	 break;
		 		                    case 31:
		 			                	 otempTechnicalDataDto.setFRAME_LENGTH(cellValue);
		 			                	 break;
		 		                    case 32:
		 			                	 otempTechnicalDataDto.setROTATION(cellValue);	
		 			                	 break;
		 		                    case 33:
		 			                	 otempTechnicalDataDto.setNUMBER_OF_SPEEDS(cellValue);
		 			                	 break;
		 		                    case 34:
		 			                	 otempTechnicalDataDto.setSHAFT_TYPE(cellValue);	
		 			                	 break;
		 		                    case 35:
		 			                	 otempTechnicalDataDto.setSHAFT_DIAMETER_MM(cellValue);	
		 			                	 break;
		 		                    case 36:
		 			                	 otempTechnicalDataDto.setSHAFT_EXTENSION_MM(cellValue);
		 			                	 break;
		 		                    case 37:
		 			                	 otempTechnicalDataDto.setOUTLINE_DWG_NO(cellValue);
		 			                	 break;
		 		                    case 38:
		 			                	 otempTechnicalDataDto.setCONNECTION_DRAWING_NO(cellValue);	
		 			                	 break;
		 		                    case 39:
		 			                	 otempTechnicalDataDto.setOVERALL_LENGTH_MM(cellValue);	
		 			                	 break;
		 		                    case 40:
		 			                	 otempTechnicalDataDto.setSTARTING_TYPE(cellValue);	
		 			                	 break;
		 		                    case 41:
		 			                	 otempTechnicalDataDto.setTHRU_BOLTS_EXTENSION(cellValue);	
		 			                	 break;
		 		                    case 42:
		 			                	 otempTechnicalDataDto.setTYPE_OF_OVERLOAD_PROTECTION(cellValue);	
		 			                	 break;
		 		                    case 43:
		 			                	 otempTechnicalDataDto.setCE(cellValue);
		 			                	 break;
		 		                    case 44:
		 			                	 otempTechnicalDataDto.setCSA(cellValue);
		 			                	 break;
		 		                    case 45:
		 			                	 otempTechnicalDataDto.setUL(cellValue);
		 			                	 break;
		 		                    
		 		                    }
		 		                	
		 		                }
		 		                oListTechnicalDataDto.add(otempTechnicalDataDto);
		 	                }
		                }
		            is_Uploaded = oTechnicalDataDao.uploadBulkTechnicalData(conn, oListTechnicalDataDto); 
			    }  
		      }
		    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_TECHNICALDATA+" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);
		    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_TECHNICALDATA_URL); 
		    
		    }
		   
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "bulkUploadTechnicalData ..", ExceptionUtility.getStackTraceAsString(e));
	    	/* Setting error details to request to display the same in Exception page */
	    		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        /* Forwarding request to Error page */
	    		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	
	public boolean verifyExcelData(POIFSFileSystem fileSystem) {
		
		TechnicalDataDto otempTechnicalDataDto = null;
	    List<TechnicalDataDto> oListTechnicalDataDto = new ArrayList<TechnicalDataDto>();
	    boolean flag = false;
	    boolean isEmpty_flag = false;
		if (fileSystem != null) {
			
			try {
				/* Retrieving the workbook object */
				HSSFWorkbook workBook = new HSSFWorkbook(fileSystem);
				/* Retrieving the work sheet object */
				HSSFSheet sheet = workBook.getSheetAt(0);
				/* Retrieving the rows collection */
				Iterator<Row> rowIterator = sheet.rowIterator();
				String cellValue = "";
	            while (rowIterator.hasNext()) 
	            {
	                Row row = rowIterator.next();
	                
	                if(row.getRowNum()==0) {
	                	 Iterator<Cell> cellIterator = row.cellIterator();
			                Cell cell = null;
			                while (cellIterator.hasNext()) 
			                {
			                	cell = cellIterator.next();
			                	cellValue = cell.toString();
			                	int cell_value = cell.getColumnIndex();
			                    switch(cell_value) {
			                    case 0:
			                    	if(!cellValue.equalsIgnoreCase("MANUFACTURING LOCATION"))
			                    	flag = true;
			                    break;
			                    case 1:
			                    	if(!cellValue.equalsIgnoreCase("PRODUCT GROUP"))
			                    	flag = true;
				                    break;
			                    case 2:
			                    	if(!cellValue.equalsIgnoreCase("PRODUCT LINE"))
				                    break;
			                    case 3:
			                    	if(!cellValue.equalsIgnoreCase("STANDARD"))
			                    	flag = true;
				                    break;
			                    case 4:
			                    	if(!cellValue.equalsIgnoreCase("POWER RATING KW"))
			                    	flag = true;
				                	 break;
			                    case 5:
			                    	if(!cellValue.equalsIgnoreCase("FRAME TYPE"))
			                    	flag = true;
				                	 break;
			                    case 6:
			                    	if(!cellValue.equalsIgnoreCase("NUMBER OF POLES"))
			                    	flag = true;
				                	 break;
			                    case 7:
			                    	if(!cellValue.equalsIgnoreCase("MOUNTING TYPE"))
			                    	flag = true;
				                	 break;
			                    case 8:
			                    	if(!cellValue.equalsIgnoreCase("TB POSITION"))
			                    	flag = true;
				                	 break;
			                    case 9:
			                    	if(!cellValue.equalsIgnoreCase("MODEL NO"))
			                    	flag = true;
				                	 break;
			                    case 10:
			                    	if(!cellValue.equalsIgnoreCase("VOLTAGE V"))
			                    	flag = true;
				                	 break;
			                    case 11:
			                    	if(!cellValue.equalsIgnoreCase("POWER RATING HP"))
			                    	flag = true;
				                	 break;
			                    case 12:
			                    	if(!cellValue.equalsIgnoreCase("FREQUENCY HZ"))
			                    	flag = true;
				                	 break;
			                    case 13:
			                    	if(!cellValue.equalsIgnoreCase("CURRENT A"))
			                    	flag = true;
				                	 break;
			                    case 14:
			                    	if(!cellValue.equalsIgnoreCase("SPEED MAX"))
			                    	flag = true;
				                	 break;
			                    case 15:
			                    	if(!cellValue.equalsIgnoreCase("POWER FACTOR"))
			                    	flag = true;
				                	 break;
			                    case 16:
			                    	if(!cellValue.equalsIgnoreCase("EFFICIENCY"))
			                    	flag = true;
				                	 break;
			                    case 17:
			                    	if(!cellValue.equalsIgnoreCase("ELECTRICAL TYPE"))
			                    	flag = true;
				                	 break;
			                    case 18:
			                    	if(!cellValue.equalsIgnoreCase("IP CODE"))
			                    	flag = true;
				                	 break;
			                    case 19:
			                    	if(!cellValue.equalsIgnoreCase("INSULATION CLASS"))
			                    	flag = true;
				                	 break;
			                    case 20:
			                    	if(!cellValue.equalsIgnoreCase("SERVICE FACTOR"))
			                    	flag = true;
				                	 break;
			                    case 21:
			                    	if(!cellValue.equalsIgnoreCase("DUTY"))
			                    	flag = true;
				                	 break;
			                    case 22:
			                    	if(!cellValue.equalsIgnoreCase("NUMBER OF PHASES"))
			                    	flag = true;
				                	 break;
			                    case 23:
			                    	if(!cellValue.equalsIgnoreCase("MAX AMBIENT C"))
			                    	flag = true;
				                	 break;
			                    case 24:
			                    	if(!cellValue.equalsIgnoreCase("DE BEARING SIZE"))
			                    	flag = true;
				                	 break;
			                    case 25:
			                    	if(!cellValue.equalsIgnoreCase("DE BEARING TYPE"))
			                    	flag = true;
				                	 break;
			                    case 26:
			                    	if(!cellValue.equalsIgnoreCase("ODE BEARING SIZE"))
			                    	flag = true;
				                	 break;
			                    case 27:
			                    	if(!cellValue.equalsIgnoreCase("ODE BEARING TYPE"))
			                    	flag = true;
				                	 break;
			                    case 28:
			                    	if(!cellValue.equalsIgnoreCase("ENCLOSURE TYPE"))
			                    	flag = true;
				                	 break;
			                    case 29:
			                    	if(!cellValue.equalsIgnoreCase("MOTOR ORIENTATION"))
			                    	flag = true;
				                	 break;
			                    case 30:
			                    	if(!cellValue.equalsIgnoreCase("FRAME MATERIAL"))
			                    	flag = true;
				                	 break;
			                    case 31:
			                    	if(!cellValue.equalsIgnoreCase("FRAME LENGTH"))
			                    	flag = true;
				                	 break;
			                    case 32:
			                    	if(!cellValue.equalsIgnoreCase("MANUFACTURING LOCATION"))
			                    	flag = true;
				                	 break;
			                    case 33:
			                    	if(!cellValue.equalsIgnoreCase("NUMBER OF SPEEDS"))
			                    	flag = true;
				                	 break;
			                    case 34:
			                    	if(!cellValue.equalsIgnoreCase("SHAFT TYPE"))
			                    	flag = true;
				                	 break;
			                    case 35:
			                    	if(!cellValue.equalsIgnoreCase("SHAFT DIAMETER MM"))
			                    	flag = true;
				                	 break;
			                    case 36:
			                    	if(!cellValue.equalsIgnoreCase("SHAFT EXTENSION MM"))
			                    	flag = true;
				                	 break;
			                    case 37:
			                    	if(!cellValue.equalsIgnoreCase("OUTLINE DWG NO"))
			                    	flag = true;
				                	 break;
			                    case 38:
			                    	if(!cellValue.equalsIgnoreCase("CONNECTION DRAWING_NO"))
			                    	flag = true;
				                	 break;
			                    case 39:
			                    	if(!cellValue.equalsIgnoreCase("OVERALL LENGTH MM"))
			                    	flag = true;
				                	 break;
			                    case 40:
			                    	if(!cellValue.equalsIgnoreCase("STARTING TYPE"))
			                    	flag = true;
				                	 break;
			                    case 41:
			                    	if(!cellValue.equalsIgnoreCase("THRU BOLTS EXTENSION"))
			                    	flag = true;
				                	 break;
			                    case 42:
			                    	if(!cellValue.equalsIgnoreCase("TYPE OF OVERLOAD PROTECTION"))
			                    	flag = true;
				                	 break;
			                    case 43:
			                    	if(!cellValue.equalsIgnoreCase("CE"))
			                    	flag = true;
				                	 break;
			                    case 44:
			                    	if(!cellValue.equalsIgnoreCase("CSA"))
			                    	flag = true;
				                	 break;
			                    case 45:
			                    	if(!cellValue.equalsIgnoreCase("UL"))
			                    	flag = true;
				                	 break;
			                    }
			                }
	                }
	                
	                if(flag) {
	                
	                if(row.getRowNum()>=1 && row.getRowNum()<=sheet.getLastRowNum()) {
	                	
	                	//For each row, iterate through all the columns
		                Iterator<Cell> cellIterator = row.cellIterator();
		                Cell cell = null;
		                otempTechnicalDataDto = new TechnicalDataDto();
		                while (cellIterator.hasNext()) 
		                {
		                	cell = cellIterator.next();
		                	cellValue = cell.toString();
		                	int cell_value = cell.getColumnIndex();
		                    switch(cell_value) {
		                    case 0:
		                    if(cellValue.isEmpty())
		                    isEmpty_flag = true;
		                    break;
		                    case 1:
		                    	if(cellValue.isEmpty())
				                isEmpty_flag = true;	
			                    break;
		                    case 2:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                    break;
		                    case 3:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;	
			                    break;
		                    case 4:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 5:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 6:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 7:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 8:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 9:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    case 10:
		                    	if(cellValue.isEmpty())
					                isEmpty_flag = true;
			                	 break;
		                    }
		                	
		                }
	                } 
	                }
	            }
			}catch(Exception e) {
				
			}
            
	    }
		
		return isEmpty_flag;
	}

}
