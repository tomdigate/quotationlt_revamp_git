/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: AdminQueries.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc:  Interface that contains all the SQL queries related to admin functionalities.
 * ******************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.PropertyUtility;

public interface AdminQueries {
    /**
     * String variable to store the QUOTATION & SHARED schema name.
     * It is retrieved from quotation.properties file
     */
    public final String SCHEMA_QUOTATION = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DB_SCHEMA_QUOTATION);
    public final String SCHEMA_SHARED = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DB_SCHEMA_SHARED);
    
    public final String FETCH_STATUSLIST = "SELECT QST_STATUSID, QST_STATUSDESC " +
                                            " FROM " + SCHEMA_QUOTATION + ".RFQ_STATUS " +
                                            " ORDER BY QST_STATUSID ";
    
    public final String FETCH_MASTERSLIST = "SELECT QEM_ENGMID, QEM_DESC " +
                                            " FROM " + SCHEMA_QUOTATION + ".RFQ_ENG_MASTER " +
                                            " ORDER BY QEM_ENGMID ";
    
    public final String FETCH_OPTIONSLIST_ONLYACTIVE = "SELECT QED_ENGDID, QED_DESC, QED_ISACTIVE " +
                                                        " FROM " + SCHEMA_QUOTATION + ".RFQ_ENG_DETAILS " +
                                                        " WHERE QED_ENGMID = ? AND QED_ISACTIVE = 'A' " +
                                                        " ORDER BY QED_DESC";
    
    public final String FETCH_OPTIONSLIST_ALL = "SELECT QED_ENGDID, QED_DESC, QED_ISACTIVE " +
                                                " FROM " + SCHEMA_QUOTATION + ".RFQ_ENG_DETAILS " +
                                                " WHERE QED_ENGMID = ? " +
                                                " ORDER BY QED_DESC";
   
/*
 * Queries for Customer admin paegs .... START ......................
 */
    
    /**
     * Query for count the number of records in customer Table depond upon the search critirea
     */
    public final String COUNT_CUSTOMER_SEARCHRESULT = "SELECT COUNT(*) FROM " 
    													+ SCHEMA_QUOTATION+".RFQ_CUSTOMER CUS, "+SCHEMA_QUOTATION+".RFQ_LOCATION LOC " 
    													+ " WHERE CUS.QCU_LOCATIONID = LOC.QLO_LOCATIONID AND QCU_ISDELETED='N' ";
    
    /**
     * Query for retrive the records in customer Table depond upon the search critirea
     */
    public final String FETCH_CUSTOMER_SEARCHRESULT = "SELECT CUS.QCU_NAME, CUS.QCU_CUSTOMERID, CUS.QCU_CONTACTPERSON, CUS.QCU_EMAIL, CUS.QCU_PHONE, "
    								+ " LOC.QLO_LOCATIONNAME, CUS.QCU_MOBILE, CUS.QCU_FAX, CUS.QCU_ADDRESS,  CUS.QCU_SALESENGINEERID,"
    								+ " CUS.QCU_CUSTOMERTYPE, CUS.QCU_INDUSTRY, CUS.QCU_SAPCODE, CUS.QCU_ORACLECODE, CUS.QCU_GSTNUMBER, " 
    								+ " CUS.QCU_STATE, CUS.QCU_COUNTRY, CUS.QCU_CUSTOMER_RSM, CUS.QCU_CUSTOMERINDUSTRY, CUS.QCU_SALESGROUP, "
    								+ " CUS.QCU_CUSTOMER_CLASS FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER CUS, " + SCHEMA_QUOTATION + ".RFQ_LOCATION LOC "
    								+ " WHERE CUS.QCU_LOCATIONID = LOC.QLO_LOCATIONID AND QCU_ISDELETED='N' ";
    
    /**
     * Query for retrive the all location name from location Table for drop down 
     */
    public final String FETCH_LOCATION_LIST= " SELECT QLO_LOCATIONID,QLO_LOCATIONNAME FROM " +
    					SCHEMA_QUOTATION+".RFQ_LOCATION WHERE QLO_ISACTIVE='A'";
    
    /**
     * Query for retrive the all location name from location Table for drop down 
     */
    public final String FETCH_CUSTOMER_LIST = "SELECT QCU_CUSTOMERID, QCU_NAME FROM "  +
    					SCHEMA_QUOTATION+".RFQ_CUSTOMER WHERE QCU_LOCATIONID = ? AND QCU_ISDELETED='N' ";
    
	/**
	 * Getting the Customer ID from the customer sequence 
	 */
   	public final String FETCH_CUSTOEMRID = "SELECT "+SCHEMA_QUOTATION+".RFQ_CUSTOMERID_SEQ.NEXTVAL CUSTOMERID FROM DUAL" ;
   	
   	public final String FETCH_CUSTOEMRID_MAXNEXTVAL = "SELECT MAX(QCU_CUSTOMERID) AS QCU_CUSTOMERID FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER " ;
   	
	/**
	 *  Query to Insert the data inot Customer Table.
	 */
   	public final String INSERT_CUSTOMER_VALUE = " INSERT INTO " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER "
    			+ " (QCU_CUSTOMERID,QCU_NAME,QCU_LOCATIONID,QCU_CONTACTPERSON,QCU_EMAIL,QCU_PHONE,QCU_MOBILE,QCU_FAX,QCU_ADDRESS,QCU_CREATEDBY,QCU_CREATEDDATE,"
    			+ " QCU_LASTMODIFIEDBY,QCU_LASTMODIFIEDDATE,QCU_SALESENGINEERID,QCU_CUSTOMERTYPE,QCU_INDUSTRY,QCU_SAPCODE,QCU_ORACLECODE,QCU_GSTNUMBER,"
    			+ " QCU_STATE,QCU_COUNTRY) " 
    			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?, SYSDATE, ?, ?, ?, ?, ?, ?, ?, ?) ";
    	
   	/**
   	 * Query for getting the Customer Info by passing of Customer ID ..
   	 */
   	public final String FETCH_CUSTOMER_VALUE = "SELECT QCU_CUSTOMERID, QCU_NAME, QCU_LOCATIONID, QCU_CONTACTPERSON, QCU_EMAIL, QCU_PHONE, QCU_MOBILE, QCU_FAX, "
   										+ " QCU_ADDRESS, QCU_SALESENGINEERID, QCU_CUSTOMERTYPE, QCU_INDUSTRY, QCU_SAPCODE, QCU_ORACLECODE, QCU_GSTNUMBER, "
   										+ " QCU_STATE, QCU_COUNTRY, QCU_CUSTOMER_RSM, QCU_CUSTOMERINDUSTRY, QCU_SALESGROUP, QCU_CUSTOMER_CLASS "
   										+ " FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER  WHERE QCU_CUSTOMERID = ?  ";
    	
    /**
     * update customer Table values
     */
    public final String UPDATE_CUSTOMER_VALUE = "UPDATE "+SCHEMA_QUOTATION+".RFQ_CUSTOMER SET " 
    			+ " QCU_NAME = ?, QCU_LOCATIONID = ?, QCU_CONTACTPERSON = ?, QCU_EMAIL = ?, QCU_PHONE = ?, QCU_MOBILE = ?, QCU_FAX = ?, QCU_ADDRESS = ?, "
    			+ " QCU_LASTMODIFIEDBY = ?, QCU_LASTMODIFIEDDATE = SYSDATE, QCU_SALESENGINEERID = ?, QCU_CUSTOMERTYPE = ?, QCU_INDUSTRY = ?, "
    			+ " QCU_SAPCODE = ?, QCU_ORACLECODE = ?, QCU_GSTNUMBER = ?, QCU_STATE = ?, QCU_COUNTRY = ? "
    			+ " WHERE QCU_CUSTOMERID = ?  ";
    
    
    public final String CHECK_CUSTOMER_ISACTIVE = "SELECT COUNT (1) " + 
                                                    " FROM ( SELECT em.qem_customerid customerid " + 
                                                            "  FROM " + SCHEMA_QUOTATION + ".rfq_enquiry_master em, " +
                                                            "       " + SCHEMA_QUOTATION + ".rfq_customer cu " + 
                                                            " WHERE em.qem_customerid = cu.qcu_customerid " + 
                                                            " UNION " + 
                                                            " SELECT em.qem_customerid customerid " + 
                                                            "   FROM " + SCHEMA_QUOTATION + ".rfq_enquiry_master em, " + 
                                                            "        " + SCHEMA_QUOTATION + ".rfq_customer cu, " + 
                                                            "        " + SCHEMA_QUOTATION + ".rfq_workflow wf " + 
                                                            "  WHERE em.qem_customerid = cu.qcu_customerid " + 
                                                            "    AND em.qem_enquiryid = wf.qwf_enquiryid " + 
                                                            "    AND wf.qwf_workflowstatus = '" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "' " + 
                                                            "    AND qwf_oldstatusid < " + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED +  
                                                            "    AND qwf_newstatusid < " + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED + 
                                                    " ) WHERE customerid = ?";
    
    /**
     * Delete Cusotmer Value in customer table
     */
    public final String DELETE_CUSTOMER_VALUE ="DELETE FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER  WHERE  QCU_CUSTOMERID = ? ";

    /**
     * 
     */
    public final String FETCH_CUSTOMER_NAME = "SELECT DISTINCT QCU_NAME, QCU_CUSTOMERID, QCU_CUSTOMERINDUSTRY FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMERS_V " ;
    
    public final String FETCH_LOCATION_NAME = "SELECT DISTINCT QCU_LOCATIONNAME, QCU_LOCATIONID FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMERS_V " ;
    
    public final String FETCH_LOCATION_BY_SEARCHSTR = "SELECT DISTINCT QLO_LOCATIONNAME, QLO_LOCATIONID FROM "+SCHEMA_QUOTATION+".RFQ_LOCATION " ;
    
    public final String FETCH_CUSTOMER_INDUSTRY_NAME = "SELECT DISTINCT INDUSTRY FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER ";
    
    public final String FETCH_EXISTS_CUSTOMER = "SELECT COUNT(*) as COUNT FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER WHERE QCU_NAME = ? AND QCU_LOCATIONID = ? AND QCU_CONTACTPERSON = ?";

    
    public final String UPDATE_EXISTS_CUSTOMER ="UPDATE "+SCHEMA_QUOTATION+".RFQ_CUSTOMER SET " +
						" QCU_ISDELETED = 'N' , QCU_LASTMODIFIEDBY = ?  WHERE QCU_NAME = ? AND QCU_LOCATIONID = ? AND QCU_CONTACTPERSON = ?";
    					
    
    /*
     * Queries for Location Admin pages.
     */
    
    /**
     * FETCH_REGION_LIST Query for retrive the region list .....
     */
    public final String FETCH_REGION_LIST = "SELECT QRE_REGIONID,QRE_REGIONNAME FROM "+SCHEMA_QUOTATION+".RFQ_REGION ";
    
    /**
     * COUNT_LOCATION_SEARCHRESULT count the number of records in table depond on search criteria 
     */
    public final String COUNT_LOCATION_SEARCHRESULT = "SELECT COUNT(*) FROM "+SCHEMA_QUOTATION+".RFQ_LOCATION LOC ,"+SCHEMA_QUOTATION+".RFQ_REGION REG WHERE LOC.QLO_REGIONID = REG.QRE_REGIONID ";
    
    /**
     * FETCH_LOCATION_SEARCHLIST search the all records in table depond on search criteria 
     */
    public final String FETCH_LOCATION_SEARCHLIST = " SELECT LOC.QLO_LOCATIONID ,LOC.QLO_LOCATIONNAME ,REG.QRE_REGIONNAME,LOC.QLO_ISACTIVE " +
							    					" FROM " +
						    						SCHEMA_QUOTATION+".RFQ_LOCATION LOC ,"+SCHEMA_QUOTATION+".RFQ_REGION REG " +
							    					" WHERE " +
							    					" LOC.QLO_REGIONID = REG.QRE_REGIONID";
    
    /**
     * FETCH_LOCATION_INFO getting the all location info
     */
    public final String FETCH_LOCATION_INFO =" SELECT QLO_LOCATIONID , QLO_LOCATIONNAME , QLO_REGIONID ,QLO_ISACTIVE " +
					    					" FROM " +
					    					SCHEMA_QUOTATION+".RFQ_LOCATION " +
					    					" WHERE " +
					    					" QLO_LOCATIONID = ? ";
    
    /**
     * CREATE_LOCATIONID to create the new location ID records in table ..
     */
    public final String CREATE_LOCATIONID ="SELECT "+SCHEMA_QUOTATION+".RFQ_LOCATIONID_SEQ.NEXTVAL LOCATION_ID FROM DUAL" ;
    
    /**
     * INSERT_LOCATION_VALUES to insert the new records of location values 
     */
    public final String INSERT_LOCATION_VALUES =" INSERT INTO "+SCHEMA_QUOTATION+".RFQ_LOCATION " +
												" ( QLO_LOCATIONID , QLO_LOCATIONNAME , QLO_REGIONID ,QLO_ISACTIVE ,QLO_CREATEDBY,QLO_CREATEDDATE,QLO_LASTMODIFIEDBY,QLO_LASTMODIFIEDDATE) " +
												" VALUES " +
												" ( ? , ? , ? , ? , ? , SYSDATE , ? , SYSDATE)";
    
    /**
     * UPDATE_LOCATION_VALUE query to update the existing location value depond the Location id
     */
    public final String UPDATE_LOCATION_VALUE = " UPDATE "+SCHEMA_QUOTATION+".RFQ_LOCATION " +
						    					" SET " +
						    					" QLO_LOCATIONNAME = ?, QLO_REGIONID = ? , QLO_ISACTIVE = ? ,QLO_LASTMODIFIEDBY = ? , QLO_LASTMODIFIEDDATE = SYSDATE" +
						    					" WHERE QLO_LOCATIONID = ? ";
    
    /**
     * DELETE_LOCATION_VALUE query to delete the particular location from the table 
     */
    public final String DELETE_LOCATION_VALUE = "DELETE "+SCHEMA_QUOTATION+".RFQ_LOCATION WHERE QLO_LOCATIONID = ?  ";
    
    /**
     * UPDATE_LOCATION_STATUS query to update the location status only ...
     */
    public final String UPDATE_LOCATION_STATUS =" UPDATE "+SCHEMA_QUOTATION+".RFQ_LOCATION " +
						    					" SET " +
						    					" QLO_ISACTIVE = ? ,QLO_LASTMODIFIEDBY = ? , QLO_LASTMODIFIEDDATE = SYSDATE " +
						    					" WHERE QLO_LOCATIONID = ?" ;
    
    /*
     * Queries for Sales Manager admin Screen ...
     */
    
    /**
     * COUNT_SbgtaALESMANAGER_SEARCHRESULT query return the number of records in sales manager table  
     */
    public final String COUNT_SALESMANAGER_SEARCHRESULT = "SELECT COUNT(*) FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER SAL ,"+SCHEMA_QUOTATION+".RFQ_REGION REG WHERE SAL.QSM_REGIONID = REG.QRE_REGIONID ";
    
    /**
     * FETCH_SALESMANAGER_SEARCHRESULT qutuery return the sale manager table values deponds on search Criteria   
     */
    public final String FETCH_SALESMANAGER_SEARCHRESULT = " SELECT SAL.QSM_SALESMGRID ,SAL.QSM_SALESMGR ,REG.QRE_REGIONNAME,SAL.QSM_PRIMARYRSM,SAL.QSM_ADDITIONALRSM, SAL.QSM_PHONENO," +
								    						SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_SALESMGR ,'FULLNAME') AS SALESMANAGER_NAME,"+
								    						SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_PRIMARYRSM ,'FULLNAME') AS PRIMARYRSM_NAME, "+
								    						SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_ADDITIONALRSM ,'FULLNAME') AS ADDITIONALRSM_NAME, " +
								    						SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_SALESMGR ,'EMAIL') AS SALESMANAGER_EMAIL " +
								    						" FROM " +
															SCHEMA_QUOTATION+".RFQ_SALESMANAGER SAL, "+SCHEMA_QUOTATION+".RFQ_REGION REG " +
															" WHERE " +
															" SAL.QSM_REGIONID = REG.QRE_REGIONID ";
    
    /**
     * 
     */
    public final String CREATE_SALESMANAGER_ID ="SELECT "+SCHEMA_QUOTATION+".RFQ_SALESMGRID_SEQ.NEXTVAL SALESMANAGER_ID FROM DUAL" ;
    
    /**
     * 
     */
    public final String CREATE_SALESMANAGER_VALUES =" INSERT INTO "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER " +
						    						" (QSM_SALESMGRID, QSM_SALESMGR, QSM_REGIONID, QSM_PRIMARYRSM, QSM_ADDITIONALRSM, QSM_CREATEDBY, QSM_CREATEDDATE, QSM_LASTMODIFIEDBY, QSM_LASTMODIFIEDDATE, QSM_PHONENO )" +
						    						" VALUES " +
						    						" (?, ?, ?, ?, ?, ?, SYSDATE, ?, SYSDATE,? )";
    
    /**
     * 
     */
    public final String FETCH_SALESMANGER_INFO = " SELECT QSM_SALESMGRID, QSM_REGIONID, QSM_SALESMGR, QSM_PRIMARYRSM, QSM_ADDITIONALRSM, QSM_PHONENO, " +
												    SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QSM_SALESMGR ,'FULLNAME') AS SALESMANAGER_NAME,"+
													SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QSM_PRIMARYRSM ,'FULLNAME') AS PRIMARYRSM_NAME, "+
													SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QSM_ADDITIONALRSM ,'FULLNAME') AS ADDITIONALRSM_NAME,  " +
													SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QSM_SALESMGR ,'EMAIL') AS SALESMANAGER_EMAIL  " +
							    					" FROM " +
							    						SCHEMA_QUOTATION+".RFQ_SALESMANAGER " +
							    					" WHERE " +
							    						" QSM_SALESMGRID = ?";
    /**
     * 
     */
    public final String UPDATE_SALESMANAGER_VALUES = "UPDATE "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER SET " +
						    						" QSM_SALESMGR=?, QSM_REGIONID=?, QSM_PRIMARYRSM=?, QSM_ADDITIONALRSM=?, QSM_LASTMODIFIEDBY=?, QSM_LASTMODIFIEDDATE= SYSDATE, QSM_PHONENO=? 	" +
						    						" WHERE QSM_SALESMGRID = ?";
    
    
    
    
/* 
 * Comment By EGR on 04-12-2018
 * TODO
 *   
*/    
    public final String DOES_SALESMANAGER_EXIST = "SELECT QSM_ISDELETED FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER WHERE QSM_SALESMGR = ?  ";

    
    public final String UPDATE_EXISTS_SM = "UPDATE "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER SET QSM_ISDELETED = 'N' WHERE QSM_SALESMGR = ? AND QSM_REGIONID = ?";
    
    
    /**
     * 
     */
    public final String UPDATE_SALESMANAGER_DELETE_COLUMN="UPDATE "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER SET " +
    													   " QSM_ISDELETED = 'Y' WHERE QSM_SALESMGR = ? and QSM_REGIONID=?";
    
    
    public final String DELETE_SALESMANAGER_DELETE_COLUMN="DELETE FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER " +
			   "  WHERE QSM_SALESMGR = ? and QSM_REGIONID=?";

    
    /**
     * 
     */
    public final String CHECK_SALESMANGER_VALUE=" SELECT COUNT(*) FROM" +
    											" ( " +
	    											" SELECT QEM_CREATEDBY SM_SSO " +
	    												" FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_MASTER " +
	    												" WHERE QEM_STATUSID NOT IN (" + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED +")" +
	    											" UNION " +
	    											" SELECT QWF_ASSIGNTO SM_SSO" +
	    												" FROM "+SCHEMA_QUOTATION+".RFQ_WORKFLOW WF " +
	    												" WHERE WF.QWF_WORKFLOWSTATUS='" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'AND (QWF_OLDSTATUSID IN (" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +")) AND (QWF_NEWSTATUSID IN ("+ QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +"))" +
	    										" ) " +
	    											"WHERE SM_SSO = ? " ;
    
/*
 * Queries for Replace Managers Screen.
 */
    
    /**
     * FETCH_MANAGER_VALUE , query 
     */

    public final String FETCH_MANAGER_VALUE =  "SELECT RM.RLE_MGRTYPE, RM.RLE_MGRSSO, RE.EMP_NAMEFULL " + 
                                               "  FROM "+SCHEMA_QUOTATION+".RFQ_MANAGER RM, " + 
                                               "       "+SCHEMA_QUOTATION+".RFQ_EMPLOYEES_V RE " + 
                                               " WHERE RM.RLE_MGRSSO = RE.EMP_SSO";
    
    /**
     * INSERT_MANAGER_VALUE insert new CE or DM values if the table is not exists value ..
     */
    public final String INSERT_MANAGER_VALUE = "INSERT INTO "+SCHEMA_QUOTATION+".RFQ_MANAGER (RLE_MGRSSO,RLE_MGRTYPE) VALUES(?,?) ";
    
    /**
     * UPDATE_MANAGER_VALUE , update the Manager table with respective value of column .
     */
    public final String UPDATE_MANAGER_VALUE="UPDATE "+SCHEMA_QUOTATION+".RFQ_MANAGER SET RLE_MGRSSO =? WHERE RLE_MGRTYPE=?";
    
    /**
     * 
     */
    public final String UPDATE_MANAGERVALUE_WORKFLOW_TABLE = " UPDATE " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW " +
    															" SET QWF_ASSIGNTO = ? " +
    															" WHERE QWF_WORKFLOWSTATUS = '" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'" +
    															" AND QWF_ASSIGNTO = ? AND QWF_OLDSTATUSID IN";
   
    /**
     * 
     */
    public final String FETCH_RSM_LIST =" SELECT QSM_PRIMARYRSM AS QSM_RSM, " +
    									SCHEMA_QUOTATION +".RFQ_GETEMPLOYEEEINFO_FUN(QSM_PRIMARYRSM, 'FULLNAME') AS QSM_RSMNAME " +
    									" FROM " + SCHEMA_QUOTATION + ".RFQ_SALESMANAGER " +
    									" WHERE  QSM_PRIMARYRSM IS NOT NULL AND QSM_PRIMARYRSM!=" + SCHEMA_QUOTATION + ".RFQ_GETEMPLOYEEEINFO_FUN(QSM_PRIMARYRSM, 'FULLNAME') " +
    									" UNION " +
    									" SELECT QSM_ADDITIONALRSM  AS QSM_RSM, " +
    									SCHEMA_QUOTATION +".RFQ_GETEMPLOYEEEINFO_FUN(QSM_ADDITIONALRSM, 'FULLNAME') AS QSM_RSMNAME " +
    									" FROM " + SCHEMA_QUOTATION + ".RFQ_SALESMANAGER " +
    									" WHERE QSM_ADDITIONALRSM IS NOT NULL AND QSM_ADDITIONALRSM!=" + SCHEMA_QUOTATION + ".RFQ_GETEMPLOYEEEINFO_FUN(QSM_ADDITIONALRSM, 'FULLNAME')";
    
    
    /**
     * 
     */
    public final String FETCH_SALESMANAGER_LIST=" SELECT DISTINCT QSM_SALESMGRID, QSM_SALESMGR," +
    											  SCHEMA_QUOTATION +". RFQ_GETEMPLOYEEEINFO_FUN(QSM_SALESMGR, 'FULLNAME') AS QSM_RSMNAME" +
    											" FROM " + SCHEMA_QUOTATION + ".RFQ_SALESMANAGER WHERE QSM_ISDELETED ='N'";
   
    /**
     * 
     */
    public final String UPDATE_PRIMARYRSM_VALUE ="UPDATE " + SCHEMA_QUOTATION + ".RFQ_SALESMANAGER " +
    											" SET    QSM_PRIMARYRSM  = ? " +
    											" WHERE  QSM_PRIMARYRSM  = ?";
    
    /**
     * 
     */
    public final String UPDATE_ADDITIONALRSM_VALUE ="UPDATE " + SCHEMA_QUOTATION + ".RFQ_SALESMANAGER " +
												" SET    QSM_ADDITIONALRSM  = ? " +
												" WHERE  QSM_ADDITIONALRSM = ?";
    
    /**
     * 
     */
    public final String UPDATE_SALESMANGER_VALUE=" UPDATE "+ SCHEMA_QUOTATION +".RFQ_ENQUIRY_MASTER  SET QEM_CREATEDBY = ?,QEM_ENQUIRYNO=REPLACE(QEM_ENQUIRYNO,?,?) WHERE QEM_STATUSID NOT IN ( " + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED +") AND ";
    
    
    
    /*
     * Queries for Desin Engineer Admin screen ...........
     */
    
    /**
     * 
     */
    public final String FETCH_DE_LIST=" SELECT DISTINCT QDE_ENGINEERID, QDE_ENGINEERSSO, " +
    					SCHEMA_QUOTATION +". RFQ_GETEMPLOYEEEINFO_FUN(QDE_ENGINEERSSO, 'FULLNAME') AS QDE_ENGINEERNAME" +
    					" FROM " + SCHEMA_QUOTATION + ".RFQ_DESIGNENGINEER WHERE QDE_ISDELETED ='N'";
     
    
    public final String COUNT_DESING_ENGINEER = " SELECT COUNT(*) FROM "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEER WHERE QDE_ISDELETED='N'";
    
    public final String FETCH_DESIGN_ENGINEER = " SELECT QDE_ENGINEERID, QDE_ENGINEERSSO, " +
    											SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QDE_ENGINEERSSO ,'FULLNAME') AS DESIGNENGINEER_NAME" +
    											" FROM "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEER WHERE QDE_ISDELETED='N'";
    
    public final String CREATE_DESIGNENGINEER_ID ="SELECT "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEERID_SEQ.NEXTVAL DESIGNENGINEER_ID FROM DUAL";
    
    public final String INSERT_DESIGNENGINEER_VALUE="INSERT INTO "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEER (" +
    												" QDE_ENGINEERID,QDE_ENGINEERSSO,QDE_CREATEDBY,QDE_CREATEDDATE,QDE_LASTMODIFIEDBY,QDE_LASTMODIFIEDDATE,QDE_ISDELETED)" +
    												"VALUES (? , ? , ? , SYSDATE , ? , SYSDATE,'N') ";
    
    public final String FETCH_DESIGNENGINEER_INFO=" SELECT QDE_ENGINEERID, QDE_ENGINEERSSO, " +
    												SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QDE_ENGINEERSSO ,'FULLNAME') AS DESIGNENGINEER_NAME" +
    											  " FROM "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEER " +
    											  " WHERE QDE_ENGINEERID = ?";
    
    public final String UPDATE_DESIGNENGINEER_VALUE =" UPDATE "+ SCHEMA_QUOTATION +".RFQ_DESIGNENGINEER SET " +
    												 " QDE_ENGINEERSSO = ? , QDE_LASTMODIFIEDBY = ? , QDE_LASTMODIFIEDDATE =SYSDATE " +
    												 " WHERE QDE_ENGINEERID = ?";
    
    
    public final String CHECK_DESIGNENGINEER_VALUE=" SELECT COUNT(*) FROM" +
													" ( " +
													" SELECT QEM_CREATEDBY SM_SSO " +
														" FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_MASTER " +
														" WHERE QEM_STATUSID NOT IN (" + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED +")" +
													" UNION " +
													" SELECT QWF_ASSIGNTO SM_SSO" +
														" FROM "+SCHEMA_QUOTATION+".RFQ_WORKFLOW WF " +
														" WHERE WF.QWF_WORKFLOWSTATUS='" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'AND (QWF_OLDSTATUSID IN (" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +")) AND (QWF_NEWSTATUSID IN ("+ QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +"))" +
														" ) " +
													"WHERE SM_SSO = ? " ;
    
    

    public final String UPDATE_DESIGNENGINEER_DELETE_COLUMN="DELETE FROM  "+SCHEMA_QUOTATION+".RFQ_DESIGNENGINEER " +
				" WHERE QDE_ENGINEERSSO = ?";

    
    
    public final String FETCH_EXISTS_DE = "SELECT COUNT(*) AS ENGCOUNT FROM "+SCHEMA_QUOTATION+".RFQ_DESIGNENGINEER WHERE QDE_ENGINEERSSO = ?";
    
    public final String UPDATE_DESIGNENGINEER_EXISTS="UPDATE "+SCHEMA_QUOTATION+".RFQ_DESIGNENGINEER SET " +
														" QDE_ISDELETED = 'N' , QDE_LASTMODIFIEDBY = ?  WHERE QDE_ENGINEERSSO = ?";
    
    /*
     * Queries for MASTER data admin pages ...
     */
    
    public final String FETCH_MASTERLIST = "SELECT a.QEM_ENGMID ,a.QEM_DESC from "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER  a";
 
    public final String INSERT_MASTER = "INSERT INTO "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER  (QEM_ENGMID,QEM_DESC,QEM_CREATEDBY,QEM_CREATEDDATE,QEM_LASTMODIFIEDBY,QEM_LASTMODIFIEDDATE,QEM_ATTRIBUTE) VALUES (?,?,?,SYSDATE,?,SYSDATE,?)";
    
    
    public final String UPDATE_MASTER = "UPDATE "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER   SET QEM_DESC = ?,QEM_LASTMODIFIEDBY=?,QEM_LASTMODIFIEDDATE=SYSDATE, QEM_ATTRIBUTE=?  WHERE QEM_ENGMID =? ";
    public final String DELETE_MASTER= "DELETE FROM "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER  WHERE QEM_ENGMID =?";

    public final String FETCH_MASTER = "SELECT a.QEM_ENGMID,a.QEM_DESC FROM "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER  a WHERE UPPER(a.QEM_DESC) = UPPER(?)" ;
   
    public final String FETCH_MAX_MASTERID="SELECT CASE WHEN MAX(QEM_ENGMID) IS NULL THEN 1 ELSE MAX(QEM_ENGMID+1) END AS MAXENGID  FROM "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER ";
    
    
    public final String FETCH_MASTERSDATALIST = "SELECT QEM_ATTRIBUTE,QEM_KEY,QEM_VALUE,QEM_SORTORDER,QEM_ISACTIVE FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST WHERE QEM_ATTRIBUTE=?";
    
    
    public final String FETCH_MAXQEM_KEY ="SELECT MAX(CAST(COALESCE(QEM_KEY,'0') AS INT)+1) MAXKEY FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST WHERE QEM_ATTRIBUTE=?";
    
    
    public final String INSERT_MASTERDATA = "Insert into "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST  (QEM_ATTRIBUTE,QEM_KEY,QEM_VALUE,QEM_SORTORDER,QEM_ISACTIVE,QEM_CREATEDBY,QEM_CREATEDDATE,QEM_LASTMODIFIEDBY,QEM_LASTMODIFIEDDATE) "
    	+ "VALUES (?,?,?,"+SCHEMA_QUOTATION+".RFQ_ENGDETAILID_SEQ.NEXTVAL,'A',?,SYSDATE,?,SYSDATE)";
   
    
    public final String UPDATE_MASTERDATA = "UPDATE "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST  SET QEM_VALUE=?,QEM_LASTMODIFIEDBY=?,QEM_LASTMODIFIEDDATE=SYSDATE  WHERE QEM_ATTRIBUTE =? and QEM_SORTORDER=?";
    
    public final String UPDATE_MASTERDATA_TWO = "UPDATE "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST  SET QEM_KEY=?,QEM_VALUE=?,QEM_LASTMODIFIEDBY=?,QEM_LASTMODIFIEDDATE=SYSDATE  WHERE QEM_ATTRIBUTE =? and QEM_SORTORDER=?";

   
    public final String DELETE_MASTERDATA= "DELETE FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST  WHERE QEM_VALUE=? and QEM_ATTRIBUTE =? and QEM_SORTORDER=?";
    
   
    public final String FETCH_MASTERDATA = "SELECT a.QEM_KEY,a.QEM_VALUE FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST  a WHERE UPPER(TRIM(a.QEM_VALUE)) = UPPER(TRIM(?)) AND a.QEM_ATTRIBUTE = ? " ;
    
    public final String UPDATE_MASTERSDATA_STATUS = " UPDATE "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_PICKLIST " +
	" SET " +
	" QEM_ISACTIVE = ? ,QEM_LASTMODIFIEDBY = ? , QEM_LASTMODIFIEDDATE = SYSDATE " +
	" WHERE QEM_ATTRIBUTE =? and QEM_SORTORDER=?" ;

    
    
    public final String FETCH_LOCATIONS_BYCUSTOMERNAME = "SELECT LOC.QLO_LOCATIONID, LOC.QLO_LOCATIONNAME, CUS.QCU_CUSTOMERID, " +
                                                         "       CUS.QCU_CONTACTPERSON, CUS.QCU_NAME " +
                                                         "  FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER CUS, " +
                                                         "       " + SCHEMA_QUOTATION + ".RFQ_LOCATION LOC " +
								                         " WHERE (CUS.QCU_LOCATIONID = LOC.QLO_LOCATIONID " +
                                                         " 	 OR CUS.QCU_LOCATIONID2 = LOC.QLO_LOCATIONID " +
                                                         " 	 OR CUS.QCU_LOCATIONID3 = LOC.QLO_LOCATIONID " +
                                                         " 	 OR CUS.QCU_LOCATIONID4 = LOC.QLO_LOCATIONID " +
                                                         " 	 OR CUS.QCU_LOCATIONID5 = LOC.QLO_LOCATIONID ) " +
                                                         "   AND LOC.QLO_ISACTIVE='A' " +
                                                         "   AND QCU_ISDELETED='N' ";    
    
    public final String FETCH_CONTACTS_BYCUSTOMERID = "SELECT QCU_CONTACTPERSON, QCU_CONTACTPERSON2, QCU_CONTACTPERSON3, QCU_CONTACTPERSON4, QCU_CONTACTPERSON5 FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMER WHERE ";
    
    public final String FETCH_CUSTOMER_BY_CUSTOMERNAME = " SELECT QCU_CUSTOMERID, QCU_NAME, QCU_CUSTOMERTYPE  FROM " + SCHEMA_QUOTATION + ".RFQ_CUSTOMERS_V WHERE ";
    
    public final String FETCH_SALESMANAGERNAME_LIST="SELECT SALESMGR_ID,SALESMGR_NAME  FROM  " +
    												SCHEMA_QUOTATION+".RFQ_SALESMANAGER_V WHERE UPPER(ISDELETED)='N' AND REGIONID=?";
    
    public final String FETCH_REGION_BYSALESMANAGER="SELECT REGIONID FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER_V"+
    												" WHERE SALESMGR_NAME =?";
    
    public final String FETCH_REGION_BYSALESMANAGERID ="SELECT QSM_REGIONID FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER"+
    												" WHERE QSM_SALESMGR =?";
    
    /*
     * Query retrieves the Years
     */    
    public final String FECTH_YEARS=" SELECT ROWNUM+2007 AS VAL, ROWNUM+2007 AS KEY  FROM  (  SELECT 1 "+
    "FROM DUAL  GROUP BY CUBE (2, 2, 2, 2, 2, 2,2,2)  )  " +
    "WHERE ROWNUM <= (TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY')) - 2007)";
    
    public final String FETCH_LOCATION_ID="SELECT QCU_LOCATIONID FROM  "+SCHEMA_QUOTATION+".RFQ_CUSTOMER  WHERE QCU_CUSTOMERID= ?";
    
    
  	 /**
     * Query to Update Columns In RFQ_DELIVERY_DETAILS Table
     * This query is used in the following method
     *QuotationDaoImpl.updateDeliveryDetails()
     * Added by Gangadhar - Feb 19 , 2019
     */		

    public final String INSERT_DELIVERY_DETAILS = " INSERT INTO "+ SCHEMA_QUOTATION + ".RFQ_DELIVERY_DETAILS" +
		" (QDD_RATINGID, QDD_DELIVERYLOCATION, QDD_DELIVERYADDRESS, QDD_NOOFMOTORS) " +
			" VALUES (?,?,?,?)";
    
  	 /**
     * Query to Fetch Columns In RFQ_DELIVERY_DETAILS Table
     * This query is used in the following method
     *QuotationDaoImpl.getDeliveryDetailsList()
     * Added by Gangadhar - Feb 19 , 2019
     */		

    public final String FETCH_DELIVERY_DETAILS = "SELECT * FROM " +
				""+ SCHEMA_QUOTATION + ".RFQ_DELIVERY_DETAILS WHERE QDD_RATINGID = ?";
        
        
    /**
     * Query to Fetch Count From RFQ_COMMERCIALENGINEER Table
     * This query is used in the following method
     *CommercialEngineerDaoImpl.getCommercialEngineerSearchResult()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String COUNT_COMMERCIAL_ENGINEER = " SELECT COUNT(*) FROM "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEER WHERE QDE_ISDELETED='N'";
  
    /**
     * Query to Fetch Columns In RFQ_COMMERCIALENGINEER Table
     * This query is used in the following method
     *CommercialEngineerDaoImpl.getCommercialEngineerSearchResult()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String FETCH_COMMERCIAL_ENGINEER = " SELECT QDE_ENGINEERID, QDE_ENGINEERSSO, " +
    											SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QDE_ENGINEERSSO ,'FULLNAME') AS DESIGNENGINEER_NAME" +
    											" FROM "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEER WHERE QDE_ISDELETED='N'";
    
   
    /**
     * Query to Fetch Sequence  Id RFQ_COMMERCIALENGINEERID_SEQ Sequence
     * This query is used in the following method
     *CommercialEngineerDaoImpl.createCommercialEngineer()
     * Added by Gangadhar - Feb 19 , 2019
     */
  
    public final String CREATE_COMMERCIALENGINEER_ID ="SELECT "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEERID_SEQ.NEXTVAL DESIGNENGINEER_ID FROM DUAL";
    
   
    /**
     * Query to Insert Data In RFQ_COMMERCIALENGINEER Table
     * This query is used in the following method
     *CommercialEngineerDaoImpl.createCommercialEngineer()
     * Added by Gangadhar - Feb 19 , 2019
     */
  
    public final String INSERT_COMMERCIALENGINEER_VALUE="INSERT INTO "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEER (" +
    												" QDE_ENGINEERID,QDE_ENGINEERSSO,QDE_CREATEDBY,QDE_CREATEDDATE,QDE_LASTMODIFIEDBY,QDE_LASTMODIFIEDDATE,QDE_ISDELETED)" +
    												"VALUES (? , ? , ? , SYSDATE , ? , SYSDATE,'N') ";
    
    /**
     * Query to Insert Data In RFQ_COMMERCIALENGINEER Table
     * This query is used in the following method
     *CommercialEngineerDaoImpl.getCommercialEngineerInfo()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String FETCH_COMMERCIALENGINEER_INFO=" SELECT QDE_ENGINEERID, QDE_ENGINEERSSO, " +
    												SCHEMA_QUOTATION+".RFQ_GETEMPLOYEEEINFO_FUN(QDE_ENGINEERSSO ,'FULLNAME') AS DESIGNENGINEER_NAME" +
    											  " FROM "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEER " +
    											  " WHERE QDE_ENGINEERID = ?";
    
   
    /**
     * Query to Update Data In RFQ_COMMERCIALENGINEER Table
     * This query is used in the following method
     *CommercialEngineerDaoImpl.saveCommercialEngineerVlaue()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String UPDATE_COMMERCIALENGINEER_VALUE =" UPDATE "+ SCHEMA_QUOTATION +".RFQ_COMMERCIALENGINEER SET " +
    												 " QDE_ENGINEERSSO = ? , QDE_LASTMODIFIEDBY = ? , QDE_LASTMODIFIEDDATE =SYSDATE " +
    												 " WHERE QDE_ENGINEERID = ?";
    
    
    
    /**
     * Query to Check This Commercial Engineer have any assignments RFQ_WORKFLOWSTATUS and RFQ_ENQUIRY_MASTER
     * This query is used in the following method
     *CommercialEngineerDaoImpl.deleteCommercialEngineer()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String CHECK_COMMERCIALENGINEER_VALUE=" SELECT COUNT(*) FROM" +
													" ( " +
													" SELECT QEM_CREATEDBY SM_SSO " +
														" FROM "+SCHEMA_QUOTATION+".RFQ_ENQUIRY_MASTER " +
														" WHERE QEM_STATUSID NOT IN (" + QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED +")" +
													" UNION " +
													" SELECT QWF_ASSIGNTO SM_SSO" +
														" FROM "+SCHEMA_QUOTATION+".RFQ_WORKFLOW WF " +
														" WHERE WF.QWF_WORKFLOWSTATUS='" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'AND (QWF_OLDSTATUSID IN (" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +")) AND (QWF_NEWSTATUSID IN ("+ QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +"))" +
														" ) " +
													"WHERE SM_SSO = ? " ;
    
    

    /**
     * Query to Delete Rows From RFQ_COMMERCIALENGINEER
     * This query is used in the following method
     *CommercialEngineerDaoImpl.deleteCommercialEngineer()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String UPDATE_COMMERCIALENGINEER_DELETE_COLUMN="DELETE FROM  "+SCHEMA_QUOTATION+".RFQ_COMMERCIALENGINEER " +
				" WHERE QDE_ENGINEERSSO = ?";

    
    
    /**
     * Query to get the  count From RFQ_COMMERCIALENGINEER
     * This query is used in the following method
     *CommercialEngineerDaoImpl.isCEAlreadyExists()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String FETCH_EXISTS_CE = "SELECT COUNT(*) AS ENGCOUNT FROM "+SCHEMA_QUOTATION+".RFQ_COMMERCIALENGINEER WHERE QDE_ENGINEERSSO = ?";
    
    
    /**
     * Query to update  the columns In RFQ_COMMERCIALENGINEER
     * This query is used in the following method
     *CommercialEngineerDaoImpl.updateExistCE()
     * Added by Gangadhar - Feb 19 , 2019
     */
    public final String UPDATE_COMMERCIALENGINEER_EXISTS="UPDATE "+SCHEMA_QUOTATION+".RFQ_COMMERCIALENGINEER SET " +
														" QDE_ISDELETED = 'N' , QDE_LASTMODIFIEDBY = ?  WHERE QDE_ENGINEERSSO = ?";
    
    /**
     * Query to get the  attribute value from Eng Master table to add options
     * This query is used in the following method
     *MasterDaoImpl.getQemAttributeValue()
     * Added by Kalyani - May 6, 2019
     */
    public final String FETCH_MASTER_ATTRIBUTE = "SELECT QEM_ATTRIBUTE FROM "+SCHEMA_QUOTATION+".RFQ_ENG_MASTER WHERE QEM_ENGMID = ?";
    
    /**
     * FETCH_TECHNICALDATA_LIST Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TECHNICALDATA_LIST = "SELECT * FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * COUNT_TECHNICALDATA_SEARCHRESULT count the number of records in table depend on search criteria 
     */
    public final String COUNT_TECHNICALDATA_SEARCHRESULT = "SELECT COUNT(*) FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA WHERE 1=1 ";
    
    /**
     * Technical data serial id to create the new technical data ID records in table ..
     */
    public final String CREATE_TECHNICALdATA_ID ="SELECT NVL(MAX(SERIAL_ID), 0)+1 AS MAX_VAL FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA" ;

    /**
     * INSERT_TECHNICAL_DATA to insert the new records of technical data values 
     */
    public final String INSERT_TECHNICAL_DATA ="INSERT INTO "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ( SERIAL_ID , MANUFACTURING_LOCATION ,PRODUCT_GROUP ,PRODUCT_LINE ,STANDARD ,"
    		+ "POWER_RATING_KW ,FRAME_TYPE, NUMBER_OF_POLES ,MOUNTING_TYPE ,TB_POSITION ,MODEL_NO ,VOLTAGE_V ,POWER_RATING_HP ,FREQUENCY_HZ ,CURRENT_A ,SPEED_MAX ,"
    		+ "POWER_FACTOR ,EFFICIENCY ,ELECTRICAL_TYPE ,IP_CODE ,INSULATION_CLASS ,SERVICE_FACTOR ,DUTY ,NUMBER_OF_PHASES ,MAX_AMBIENT_C ,DE_BEARING_SIZE ,"
    		+ "DE_BEARING_TYPE ,ODE_BEARING_SIZE ,ODE_BEARING_TYPE ,ENCLOSURE_TYPE ,MOTOR_ORIENTATION ,FRAME_MATERIAL ,FRAME_LENGTH ,ROTATION ,NUMBER_OF_SPEEDS ,"
    		+ "SHAFT_TYPE ,SHAFT_DIAMETER_MM ,SHAFT_EXTENSION_MM ,OUTLINE_DWG_NO ,CONNECTION_DRAWING_NO ,OVERALL_LENGTH_MM ,STARTING_TYPE ,THRU_BOLTS_EXTENSION ,"
    		+ "TYPE_OF_OVERLOAD_PROTECTION ,CE ,CSA ,UL, FRAME_SUFFIX ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"
    		+ ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    /**
     * INSERT_TECHNICAL_DATA to insert the new records of technical data values 
     */
    public final String UPDATE_TECHNICAL_DATA = " UPDATE "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA SET MANUFACTURING_LOCATION= ? ,PRODUCT_GROUP= ? ,PRODUCT_LINE= ? ,STANDARD= ? ," 
    +"POWER_RATING_KW= ? ,FRAME_TYPE= ? ,NUMBER_OF_POLES= ? ,MOUNTING_TYPE= ? ,TB_POSITION= ? ,MODEL_NO= ? ,VOLTAGE_V= ? ,POWER_RATING_HP= ? ,FREQUENCY_HZ= ?"
    + " ,CURRENT_A= ? ,SPEED_MAX= ? ,POWER_FACTOR= ? ,EFFICIENCY= ? ,ELECTRICAL_TYPE= ? ,IP_CODE= ? ,INSULATION_CLASS= ? ,SERVICE_FACTOR= ? ,DUTY= ? ,NUMBER_OF_PHASES= ? ,MAX_AMBIENT_C= ?"
    + " ,DE_BEARING_SIZE= ? ,DE_BEARING_TYPE= ? ,ODE_BEARING_SIZE= ? ,ODE_BEARING_TYPE= ? ,ENCLOSURE_TYPE= ? ,MOTOR_ORIENTATION= ?"
    + " ,FRAME_MATERIAL= ? ,FRAME_LENGTH= ? ,ROTATION= ? ,NUMBER_OF_SPEEDS= ? ,SHAFT_TYPE= ? ,SHAFT_DIAMETER_MM= ? ,SHAFT_EXTENSION_MM= ? ,OUTLINE_DWG_NO= ?"
    + " ,CONNECTION_DRAWING_NO= ? ,OVERALL_LENGTH_MM= ? ,STARTING_TYPE= ? ,THRU_BOLTS_EXTENSION= ? ,TYPE_OF_OVERLOAD_PROTECTION= ? ,CE= ? ,CSA= ? ,UL= ? ,FRAME_SUFFIX= ? WHERE SERIAL_ID = ?";
    
    /**
     * DELETE_LOCATION_VALUE query to delete the particular data from the table 
     */
    public final String DELETE_TECHNICALDATA_VALUE = "DELETE "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA WHERE SERIAL_ID = ?  ";
    
    /**
     * Query for retrieve the all Sales Manager's List from RFQ_SALESMANAGER Table 
     */
    public final String FETCH_SALESMANGER_LIST = " SELECT QSM_SALESMGR, RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_SALESMGR ,'FULLNAME') AS SALESMANAGER_NAME "
    		+ "FROM "+ SCHEMA_QUOTATION+".RFQ_SALESMANAGER SAL ORDER BY SALESMANAGER_NAME";
    
    /**
     * Query for retrieve the specific Sales Manager name by Sales Manager Id from RFQ_SALESMANAGER Table 
     */
    public final String FETCH_SALESMANGER_BY_ID = " SELECT RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_SALESMGR ,'FULLNAME') AS SALESMANAGER_NAME "
    		+ "FROM "+ SCHEMA_QUOTATION+".RFQ_SALESMANAGER SAL WHERE SAL.QSM_SALESMGRID = ?";
    
    /**
     * FETCH_CUSTOMERDISCOUNT_LIST Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_CUSTOMERDISCOUNT_LIST = "SELECT CUST.QCU_NAME, CD.SERIAL_ID, CD.MANUFACTURING_LOCATION, CD.PRODUCT_GROUP, CD.PRODUCT_LINE, "
    													+ " CD.STANDARD, CD.POWER_RATING_KW, CD.FRAME_TYPE, CD.NUMBER_OF_POLES, CD.MOUNTING_TYPE, "
    													+ " CD.TB_POSITION, CD.SAPCODE, CD.ORACLECODE, CD.GSTNUMBER, CD.SALESENGINEERID, "
    													+ " CD.CUSTOMER_DISCOUNT, CD.SM_DISCOUNT, CD.RSM_DISCOUNT, CD.NSM_DISCOUNT FROM " 
    													+ SCHEMA_QUOTATION + ".CUSTOMER_DISCOUNT CD, "
    													+ SCHEMA_QUOTATION + ".RFQ_CUSTOMER CUST  WHERE CD.CUSTOMERID = CUST.QCU_CUSTOMERID ";
    
    /**
     * COUNT_CUSTOMERDISCOUNT_SEARCHRESULT count the number of records in table depend on search criteria 
     */
    public final String COUNT_CUSTOMERDISCOUNT_SEARCHRESULT = "SELECT COUNT(*) FROM "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT CD WHERE 1=1 ";
    
    /**
     * FETCH_LISTPRISECOUNT_LIST Query for retrieve the TECHINCAL DATA list .....
     */ 
    public final String FETCH_LISTPRISECOUNT_LIST = "SELECT * FROM "+SCHEMA_QUOTATION+".LIST_PRISE LP WHERE 1=1 ";
    
    /**
     * COUNT_CUSTOMERDISCOUNT_SEARCHRESULT count the number of records in table depend on search criteria 
     */
    public final String COUNT_LISTPRISE_SEARCHRESULT = "SELECT COUNT(*) FROM "+SCHEMA_QUOTATION+".LIST_PRISE LP WHERE 1=1 ";
    
    /**
     * FETCH_TD_MANUFACTURING Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_MANUFACTURING = "SELECT DISTINCT MANUFACTURING_LOCATION,MANUFACTURING_LOCATION FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * FETCH_TD_PRODUCT_GROUP Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_PRODUCT_GROUP = "SELECT DISTINCT PRODUCT_GROUP,PRODUCT_GROUP FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * FETCH_TD_PRODUCT_LINE Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_PRODUCT_LINE = "SELECT DISTINCT PRODUCT_LINE,PRODUCT_LINE FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    
    /**
     * FETCH_TD_STANDARD Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_STANDARD = "SELECT DISTINCT STANDARD,STANDARD FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * FETCH_TD_POWER_RATING_KW Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_POWER_RATING_KW = "SELECT DISTINCT POWER_RATING_KW,POWER_RATING_KW FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * FETCH_TD_FRAME_TYPE Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_FRAME_TYPE = "SELECT DISTINCT FRAME_TYPE,FRAME_TYPE FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    
    /**
     * FETCH_TD_NUMBER_OF_POLES Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_NUMBER_OF_POLES = "SELECT DISTINCT NUMBER_OF_POLES,NUMBER_OF_POLES FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    
    /**
     * FETCH_TD_MOUNTING_TYPE Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_MOUNTING_TYPE = "SELECT DISTINCT MOUNTING_TYPE,MOUNTING_TYPE FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    
    /**
     * FETCH_TD_TB_POSITION Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_TD_TB_POSITION = "SELECT DISTINCT TB_POSITION,TB_POSITION FROM "+SCHEMA_QUOTATION+".RFQ_TECHNICAL_DATA ";
    
    /**
     * LIST_PRISE  serial id to create the new technical data ID records in table ..
     */
    public final String CREATE_LIST_PRISE_ID ="SELECT NVL(MAX(SERIAL_ID), 0)+1 AS MAX_VAL FROM "+SCHEMA_QUOTATION+".LIST_PRISE" ;
    
    /**
     * INSERT_LIST_PRISE_DATA to insert the new records of technical data values 
     */
    public final String INSERT_LIST_PRISE ="INSERT INTO "+SCHEMA_QUOTATION+".LIST_PRISE (SERIAL_ID, MANUFACTURING_LOCATION, PRODUCT_GROUP, PRODUCT_LINE, STANDARD, POWER_RATING_KW, FRAME_TYPE, FRAME_SUFFIX, " + 
    		" NUMBER_OF_POLES, MOUNTING_TYPE, TB_POSITION, LIST_PRISE, MATERIAL_COST, LOH) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    /**
     * UPDATE_LIST_PRISE_DATA to insert the new records of technical data values 
     */
    public final String UPDATE_LIST_PRISE ="UPDATE "+SCHEMA_QUOTATION+".LIST_PRISE SET MANUFACTURING_LOCATION=?,PRODUCT_GROUP=?,PRODUCT_LINE=?,STANDARD=?,"
    		+ "POWER_RATING_KW=?,FRAME_TYPE=?,FRAME_SUFFIX=?,NUMBER_OF_POLES=?,MOUNTING_TYPE=?,TB_POSITION=?,LIST_PRISE=?,MATERIAL_COST=?,LOH=? WHERE SERIAL_ID=?";
    
    /**
     * FETCH_TECHNICALDATA_LIST Query for retrieve the TECHINCAL DATA list .....
     */
    public final String FETCH_LIST_PRISE = "SELECT * FROM "+SCHEMA_QUOTATION+".LIST_PRISE WHERE SERIAL_ID=?";
    
    public final String FETCH_LIST_PRISE_FOR_ENQUIRY = "SELECT * FROM "+SCHEMA_QUOTATION+".LIST_PRISE WHERE PRODUCT_LINE=? AND POWER_RATING_KW=? AND UPPER(FRAME_TYPE)=UPPER(?) AND UPPER(FRAME_SUFFIX)=UPPER(?) AND NUMBER_OF_POLES=? AND UPPER(MOUNTING_TYPE)=UPPER(?) AND UPPER(TB_POSITION)=UPPER(?) ";
    
    public final String FETCH_LIST_PRISE_WITH_PRICEVALUE = "SELECT * FROM " + SCHEMA_QUOTATION + ".LIST_PRISE WHERE UPPER(MANUFACTURING_LOCATION)=UPPER(?) AND UPPER(PRODUCT_GROUP)=UPPER(?) AND PRODUCT_LINE=? AND POWER_RATING_KW=? AND UPPER(FRAME_TYPE)=UPPER(?) AND UPPER(FRAME_SUFFIX)=UPPER(?) AND NUMBER_OF_POLES=? AND UPPER(MOUNTING_TYPE)=UPPER(?) AND UPPER(TB_POSITION)=UPPER(?) AND LIST_PRISE=?";
    /**
     * DELETE_LISTPRICE query to delete the particular data from the table 
     */
    public final String DELETE_LISTPRICE = "DELETE "+SCHEMA_QUOTATION+".LIST_PRISE WHERE SERIAL_ID = ?  ";
    
    /**
     * Query for retrive the all location name from location Table for drop down 
     */
    public final String FETCH_CUSTOMERNMAE_LIST= " SELECT QSM_SALESMGR,RFQ_GETEMPLOYEEEINFO_FUN(SAL.QSM_SALESMGR ,'FULLNAME') AS SALESMANAGER_NAME "
    		+ "FROM "+ SCHEMA_QUOTATION+".RFQ_SALESMANAGER SAL ORDER BY SALESMANAGER_NAME";
    
    /**
     * Query for retrive the all customer name from RFQ_CUSTOMER Table for drop down 
     */
    public final String FETCH_CUSTOMERNAME_LIST= "SELECT QCU_CUSTOMERID,QCU_NAME FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER ORDER BY QCU_NAME";
    
    /**
     * Query for retrive the all SAPCODE LIST from CUSTOMER_DISCOUNT Table for drop down 
     */
    public final String FETCH_SAPCODE_LIST= "SELECT DISTINCT QCU_SAPCODE,QCU_SAPCODE FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER";
    
    /**
     * Query for retrive the all ORACLECODE LIST from CUSTOMER_DISCOUNT Table for drop down 
     */
    public final String FETCH_ORACLECODE_LIST= "SELECT DISTINCT QCU_ORACLECODE,QCU_ORACLECODE FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER";
    
    /**
     * Query for retrive the all ORACLECODE LIST from CUSTOMER_DISCOUNT Table for drop down 
     */
    public final String FETCH_GSTNUMBER_LIST= "SELECT DISTINCT QCU_GSTNUMBER,QCU_GSTNUMBER FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER";
    
    /**
     * LIST_PRISE  serial id to create the new customer discount ID records in table ..
     */
    public final String CREATE_CUSTOMERDISCOUNT_ID ="SELECT NVL(MAX(SERIAL_ID), 0)+1 AS MAX_VAL FROM "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT" ;
    
    /**
     * INSERT_LIST_PRISE_DATA to insert the new records of technical data values 
     */
    public final String INSERT_CUSTOMERDISCOUNT ="INSERT INTO " + SCHEMA_QUOTATION + ".CUSTOMER_DISCOUNT (SERIAL_ID, MANUFACTURING_LOCATION, PRODUCT_GROUP, PRODUCT_LINE, "
    		+ " STANDARD, POWER_RATING_KW, FRAME_TYPE, NUMBER_OF_POLES, MOUNTING_TYPE, CUSTOMERID, SAPCODE, ORACLECODE, GSTNUMBER, SALESENGINEERID, "
    		+ " CUSTOMER_DISCOUNT, SM_DISCOUNT, RSM_DISCOUNT, NSM_DISCOUNT ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    /**
     * FETCH_CUSTOMERDISCOUNT Query for retrieve the CUSTOMER DISCOUNT list .....
     */
    public final String FETCH_CUSTOMERDISCOUNT = "SELECT * FROM "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT WHERE SERIAL_ID=?";
    
    public final String FETCH_CUSTOMERDISCOUNT_FOR_ENQUIRY = "SELECT * FROM "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT WHERE PRODUCT_LINE=? AND FRAME_TYPE=? AND NUMBER_OF_POLES=? AND CUSTOMERID=? ";
    
    /**
     * INSERT_LIST_PRISE_DATA to insert the new records of technical data values 
     */
    public final String UPDATE_CUSTOMERDISCOUNT ="UPDATE "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT SET MANUFACTURING_LOCATION=?, PRODUCT_GROUP=?, PRODUCT_LINE=?, STANDARD=?, "
    		+ " POWER_RATING_KW=?, FRAME_TYPE=?, NUMBER_OF_POLES=?, MOUNTING_TYPE=?, TB_POSITION=?, CUSTOMERID=?, SAPCODE=?, ORACLECODE=?, GSTNUMBER=?, SALESENGINEERID=?, "
    		+ " CUSTOMER_DISCOUNT=?, SM_DISCOUNT=?, RSM_DISCOUNT=?, NSM_DISCOUNT=? WHERE SERIAL_ID=? ";
    
    /**
     * FETCH_CUSTOMERDISCOUNT Query for retrieve the CUSTOMER DISCOUNT list .....
     */
    public final String DELETE_CUSTOMERDISCOUNT = "DELETE "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT WHERE SERIAL_ID=?";
    
    public final String FETCH_CUSTDISCOUNT_LIST = "SELECT * FROM "+SCHEMA_QUOTATION+".CUSTOMER_DISCOUNT CD, "+SCHEMA_QUOTATION+".RFQ_CUSTOMER CUST WHERE CD.CUSTOMERID = CUST.QCU_CUSTOMERID ";
    
    /**
     * FETCH_APPROVAL_LIMIT Query to retrieve Approval Limit values for RSM_DISCOUNT, NSM_DISCOUNT .....
     */
    public final String FETCH_APPROVAL_LIMIT = " SELECT * FROM " + SCHEMA_QUOTATION + ".CUSTOMER_DISCOUNT WHERE PRODUCT_LINE=? AND FRAME_TYPE=? AND CUSTOMERID=? ";

    public final String FETCH_LT_MTOADDONID_NEXTVAL = "SELECT " + SCHEMA_QUOTATION  + ".RFQ_LT_MTOADDONID_SEQ.NEXTVAL MTOADDON_ID FROM DUAL";
    
    public final String INSERT_LT_MTOADDON_DETAILS = " INSERT INTO " + SCHEMA_QUOTATION + ".RFQ_LT_MTOADDON_DETAILS " + 
    							" (QRD_LT_MTOADDONID, QRD_LT_MTOENQUIRYID, QRD_LT_MTORATINGNO, QRD_LT_MTOADDONTYPE, QRD_LT_MTOADDONQTY, " +
    							" QRD_LT_MTOADDONVALUE, QRD_LT_MTOADDONCALCTYPE, QRD_LT_MTOISDESIGNREF, QRD_LT_MTOADDONPAGETYPE, " +
    							" QRD_LT_CREATEDBY, QRD_LT_CREATEDON ) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE) ";
    
    public final String FETCH_LT_MTORATING_NEXTVAL = "SELECT " + SCHEMA_QUOTATION  + ".RFQ_LT_MTORATINGID_SEQ.NEXTVAL MTORATING_ID FROM DUAL";
    
    public final String UPDATE_LT_MTORATING_DETAILS = " UPDATE " + SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS SET QRD_LT_MTOQUANTITY = ?, QRD_LT_MTOPRODLINEID = ?," +
    		" QRD_LT_MTOKW = ?, QRD_LT_MTOPOLE = ?, QRD_LT_MTOFRAME = ?, QRD_LT_MTOFRAME_SUFFIX = ?, QRD_LT_MTOMOUNTINGID = ?, QRD_LT_MTOTBPOSITIONID = ?, " +
    		" QRD_LT_MTOTAG_NUMBER = ?, QRD_LT_MTOMFGLOCATION = ?, QRD_LT_MTOSTANDARDADDONPERCENT = ?, QRD_LT_MTOSTANDARDCASHEXTRA = ? " +
    		" WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
    
    public final String INSERT_LT_MTORATING_DETAILS = " INSERT INTO " + SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS " + 
    		" (QRD_LT_MTORATINGID, QRD_LT_MTOENQUIRYID, QRD_LT_MTORATINGNO, QRD_LT_MTOQUANTITY, QRD_LT_MTOPRODLINEID, " +
    		" QRD_LT_MTOKW, QRD_LT_MTOPOLE, QRD_LT_MTOFRAME, QRD_LT_MTOFRAME_SUFFIX, QRD_LT_MTOMOUNTINGID, QRD_LT_MTOTBPOSITIONID, " +
    		" QRD_LT_MTOTAG_NUMBER, QRD_LT_MTOMFGLOCATION, QRD_LT_MTOSTANDARDADDONPERCENT, QRD_LT_MTOSTANDARDCASHEXTRA ) VALUES " +
    		" (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    
    public final String UPDATE_LT_MTO_HIGHLIGHTFLAG = " UPDATE " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_MTOHIGHLIGHT = ? WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? ";
    
    
}
