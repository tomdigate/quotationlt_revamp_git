/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: UpdateManagersDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 13, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.UpdateManagersDto;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public interface UpdateManagersDao {

	public UpdateManagersDto getManagerValues(Connection conn, 
			   								  UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public boolean updateChiefExecutiveValues(Connection conn, 
			   						   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public boolean updateDesignManagerValue(Connection conn, 
			   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public boolean updateRegionalSalesManagerValue(Connection conn, 
			   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public boolean updateSalesManagerValue(Connection conn, 
			   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public boolean replaceManagersWorkflow(Connection conn, 
			   							   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	public ArrayList getSalesManagerList(Connection conn)throws Exception;
	
	public ArrayList getReginolSalesManagerList(Connection conn)throws Exception;

	public ArrayList getDesignEngineerList(Connection conn)throws Exception;
	
	
	/**
	 * To update Commercial Manager  in Admin Replace Managers page
	 * @param conn Connection object to connect to database	
	 * @param oUpdateManagersDto UpdateManagersDto object which has Commercial Manager data
	 * @return Returns boolean true if update success else false
	 * @throws Exception If any error occurs during the process
	 * @author 610092227 (Kalyani Uppalapati)
	 * Created on: 28 June 2019
	 */
	public boolean updateCommercialManagerValue(Connection conn, 
			   UpdateManagersDto oUpdateManagersDto)throws Exception;
	
	
}
