/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CommercialEngineerDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: DAO Implementation for the corresponding Interface
 * *****************************************************************************
 * Author: 900010540 (Gangadhar)
 * Date: March 1,2019
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.CommercialEngineerDto;
import in.com.rbc.quotation.admin.dto.DesignEngineerDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

public class CommercialEngineerDaoImpl implements CommercialEngineerDao , AdminQueries {
	
	/**
	 * To fetch CE search results
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object
	 * @param oListModel ListModel Object
	 * @return Hashtable object
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public Hashtable getCommercialEngineerSearchResult(Connection conn, CommercialEngineerDto oCommercialEngineerDto, ListModel oListModel) throws Exception {
	   /*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getCommercialEngineerSearchResult";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
			
 		/* PreparedStatement object to handle database operations */
         PreparedStatement pstmt = null ;
         
         /* ResultSet object to store the rows retrieved from database */
         ResultSet rs = null ;      
         
         /* Object of DBUtility class to handle database operations */
         DBUtility oDBUtility = new DBUtility();
        
         /*
          * Hashtable that stores the following values, and that is used to display results in search page
          * --> ArrayList containing the rows retrieved from database
          * --> ListModel object that hold the values used to retrieve values and display the same
          * --> String array containing the list of column names to be displayed as column headings
          */
         Hashtable htSearchResults = null;
         
         ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
         
         
         int iCursorPosition = QuotationConstants.QUOTATION_LITERAL_ZERO; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
         
         String sWhereString = null; // Stores the Where clause condition
         String sSqlQueryString = null; // Stores the complete SQL query to be executed
         
         try {
        	 sWhereString = " AND QDE_ENGINEERSSO LIKE '"+oCommercialEngineerDto.getDesignEngineerSSO()+"%'";
         	 sSqlQueryString = COUNT_COMMERCIAL_ENGINEER+sWhereString ;
         	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Desing Engineer Count Query",sSqlQueryString);
         	pstmt = conn.prepareCall(sSqlQueryString);
         	
         	/* Execute the Query for counting the number records*/
         	rs=pstmt.executeQuery();
         	
         	if(rs.next()) {
         		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
         	}else {
         		oListModel.setTotalRecordCount(QuotationConstants.QUOTATION_LITERAL_ZERO);// if the Query return no records , it's set the Total recordcount  "0" as default
         	}
         	oDBUtility.releaseResources(rs, pstmt);
         	DesignEngineerDto oTempDesignEngineerDto ;// Create the Temporary Location Object for storing the table column values 
         	 
         	 if(oListModel.getTotalRecordCount()>QuotationConstants.QUOTATION_LITERAL_ZERO) {
         		 htSearchResults = new Hashtable();
         		 
         		 if(oListModel.getCurrentPageInt()<QuotationConstants.QUOTATION_LITERAL_ZERO)
         			 throw new Exception("Current Page is negative in search method...");
         	
         		 iCursorPosition = (oListModel.getCurrentPageInt()-QuotationConstants.QUOTATION_LITERAL_ONE) * oListModel.getRecordsPerPageInt();
         		 
         		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
	 			 /*
	 			  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
	 			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
	 			  * and the data's are ordered by Location DBFiled as by default
	 			  * 
	 			  */
              		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
  						                    .append(FETCH_COMMERCIAL_ENGINEER)
  						                    .append(sWhereString)
  						                    .append(" ORDER BY ")
  						                    .append(ListDBColumnUtil.getDesignEngineerSearchResultDBField(oListModel.getSortBy()))
  						                    .append(" " + oListModel.getSortOrderDesc())
  						                    .append(" ) Q WHERE rownum <= (")
  						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
  						                    .append(" WHERE rnum > " + iCursorPosition + "")
  						                    .toString();
              		
              		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Commercial Engineer Retrive Query",sSqlQueryString );
              		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
              		
              		rs= pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
              		if (rs.next()) {
                          do {
                         	 oTempDesignEngineerDto = new DesignEngineerDto();
                         	oTempDesignEngineerDto.setDesignEngineerId(rs.getInt("QDE_ENGINEERID"));
                         	oTempDesignEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
                         	oTempDesignEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
                         	 alSearchResultsList.add(oTempDesignEngineerDto);
                         } while (rs.next());
                      }
                }
         		  htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                  htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                  htSearchResults.put("locationQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
              }
         	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
         }finally {
         	 /* Releasing ResultSet & PreparedStatement objects */
             oDBUtility.releaseResources(rs, pstmt);
         }
         return htSearchResults ;
	}
	
	/**
	 * To create CommercialEngineer 
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on create success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean createCommercialEngineer(Connection conn, CommercialEngineerDto oCommercialEngineerDto)throws Exception {
		   /*
	    	* Setting the Method Name as generic for logger Utility purpose . 
	        */
			String sMethodName ="createCommercialEngineer"; 
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
			
			/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        PreparedStatement pstmt1 = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;      
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        
	        /*getting the newlly Customer ID from the Customer sequence */
	        int iGetDesingengineerID = QuotationConstants.QUOTATION_LITERAL_ZERO;
	        boolean isCreated = QuotationConstants.QUOTATION_FALSE ;
	        int iCreatedCount=QuotationConstants.QUOTATION_LITERAL_ZERO;
	        try {
	        	
		        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Create Commercial Engineer ID query :="+ CREATE_COMMERCIALENGINEER_ID);
		        	pstmt1 = conn.prepareStatement(CREATE_COMMERCIALENGINEER_ID);
		        	rs = pstmt1.executeQuery();// Execute the creating the Customer Sequence Query,
		        	
		        	if(rs.next()) {
		        		iGetDesingengineerID = rs.getInt("DESIGNENGINEER_ID");
		        	}
		        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Insert Commercial Engineer Vlues query :="+ INSERT_COMMERCIALENGINEER_VALUE);
		        	
		        	pstmt = conn.prepareStatement(INSERT_COMMERCIALENGINEER_VALUE);
		        	
		        	pstmt.setInt(1, iGetDesingengineerID);
		        	pstmt.setString(2,oCommercialEngineerDto.getDesignEngineerSSO());
		        	pstmt.setString(3,oCommercialEngineerDto.getCreatedById());
		        	pstmt.setString(4,oCommercialEngineerDto.getLastUpdatedById());
		        	iCreatedCount= pstmt.executeUpdate();
		            if(iCreatedCount > QuotationConstants.QUOTATION_LITERAL_ZERO ) {
		            	isCreated = true;
		            }
	        	
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"is Commercial engineer created isCreated :="+ isCreated);
	            
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources( rs,pstmt);
	            oDBUtility.releaseResources( pstmt1);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
	        return isCreated;
			
		}

	
	/**
	 *  To fetch CommercialEngineer Information
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return CommercialEngineerDto object with CE data
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public CommercialEngineerDto getCommercialEngineerInfo(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "getCommercialEngineerInfo";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        try {
        	pstmt = conn.prepareStatement(FETCH_COMMERCIALENGINEER_INFO);
        	pstmt.setInt(1,oCommercialEngineerDto.getDesignEngineerId());
        	rs = pstmt.executeQuery();
			
			while(rs.next()) {
				oCommercialEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
				oCommercialEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return oCommercialEngineerDto;
	}
	
	/**
	 * To update the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on save success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean saveCommercialEngineerVlaue(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception  {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "saveCommercialEngineerVlaue";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE ;
		int iUdateRowCount =QuotationConstants.QUOTATION_LITERAL_ZERO;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Commercial Engineer ID :="+oCommercialEngineerDto.getDesignEngineerId());
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Commercial Engineer Update Query  :="+UPDATE_COMMERCIALENGINEER_VALUE);
        	pstmt = conn.prepareStatement(UPDATE_COMMERCIALENGINEER_VALUE);
        	pstmt.setString(1, oCommercialEngineerDto.getDesignEngineerSSO());
        	pstmt.setString(2, oCommercialEngineerDto.getLastUpdatedById());
        	pstmt.setInt(3, oCommercialEngineerDto.getDesignEngineerId());
        	iUdateRowCount = pstmt.executeUpdate();
        	if(iUdateRowCount > QuotationConstants.QUOTATION_LITERAL_ZERO) {
        		isUpdated = QuotationConstants.QUOTATION_TRUE;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	/** 
	 * To check whether given CE already exists
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 	 
	 * @return int value 1 if exists else 0
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */

	public int isCEAlreadyExists(Connection conn , CommercialEngineerDto oCommercialEngineerDto) throws Exception  {
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "isCEAlreadyExists";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*getting the newly Customer ID from the Customer sequence */
        int iExists = QuotationConstants.QUOTATION_LITERAL_ZERO;
        
        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH_EXISTS_CE :="+FETCH_EXISTS_CE);
        	pstmt = conn.prepareStatement(FETCH_EXISTS_CE);
        	pstmt.setString(1,oCommercialEngineerDto.getDesignEngineerSSO());
        	rs = pstmt.executeQuery();
			
			while(rs.next()) {
				if(rs.getInt("engcount")>QuotationConstants.QUOTATION_LITERAL_ZERO)
				{
					iExists = QuotationConstants.QUOTATION_LITERAL_ONE;
				}
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		
		return iExists;
	}
	/**
	 *  To update the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 	 
	 * @return boolean true on update success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */

	public boolean updateExistCE(Connection conn , CommercialEngineerDto oCommercialEngineerDto) throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateExistCE";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE ;
		int iUdateRowCount=QuotationConstants.QUOTATION_LITERAL_ZERO;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Commercial Engineer Update Query  :="+UPDATE_COMMERCIALENGINEER_EXISTS  +"Exists DE SSO :="+oCommercialEngineerDto.getDesignEngineerSSO());
        	pstmt = conn.prepareStatement(UPDATE_COMMERCIALENGINEER_EXISTS);
        	pstmt.setString(2, oCommercialEngineerDto.getDesignEngineerSSO());
        	pstmt.setString(1, oCommercialEngineerDto.getLastUpdatedById());
           iUdateRowCount = pstmt.executeUpdate();
        	if(iUdateRowCount > QuotationConstants.QUOTATION_LITERAL_ZERO) {
        		
        		isUpdated = QuotationConstants.QUOTATION_TRUE;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	/**
	 * To delete the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on delete success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean deleteCommercialEngineer(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception{
     	/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName ="deleteCommercialEngineer"; 
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ; 
        
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isDeleted = QuotationConstants.QUOTATION_FALSE;
		int iCountRow = QuotationConstants.QUOTATION_LITERAL_MINUSONE ;
		int iUpdateRow =QuotationConstants.QUOTATION_LITERAL_ZERO;
		 try {
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Delete Commercial Engineer Query 	 ::=Enginner ID :="+oCommercialEngineerDto.getDesignEngineerId());
			 pstmt = conn.prepareStatement(CHECK_COMMERCIALENGINEER_VALUE);
	        	
			 pstmt.setString(1, oCommercialEngineerDto.getDesignEngineerSSO());
			 rs = pstmt.executeQuery();
	        	
        	   if(rs.next()) {
		        	iCountRow = rs.getInt(1);
		        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Query the number of records avaialble iCountRow in Commercial Engineer Value:="+iCountRow);
		        }		        
			        if(iCountRow ==QuotationConstants.QUOTATION_LITERAL_ZERO) {// if the count ==0 then the cursor will go inside of if loop ..
			        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query for isDelete Column in Slaes Manager table :="+UPDATE_COMMERCIALENGINEER_DELETE_COLUMN);
			        	pstmt1 = conn.prepareStatement(UPDATE_COMMERCIALENGINEER_DELETE_COLUMN);
			        	pstmt1.setString(1,oCommercialEngineerDto.getDesignEngineerSSO());
			        	iUpdateRow = pstmt1.executeUpdate();
			        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Update Query isUpdateRow :="+iUpdateRow);
			        	if(iUpdateRow > QuotationConstants.QUOTATION_LITERAL_ZERO) {
			        		isDeleted = QuotationConstants.QUOTATION_TRUE ;
			        	}
			        }
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Afetr deletion Commercial engineer value : result isDeleted :="+isDeleted );	
	        }finally {
	        	/* Releasing PreparedStatement objects */
	        	oDBUtility.releaseResources(pstmt1);
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}
	
	/** 
	 * To export the CommercialEngineer search results data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @param sWhereQuery where checks string to append the query
	 * @param sSortFilter SortFilter String
	 * @param sSortType SortType String
	 * @return ArrayList object with CE search results data
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public ArrayList exportCommercialEngineerSearchResult(Connection conn , CommercialEngineerDto oCommercialEngineerDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception{
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName ="exportCommercialEngineerSearchResult"; 
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
            
           CommercialEngineerDto oTempCommercialEngineerDto=null ;// Create the Temporary Location Object for storing the table column values 
            try {

            	sSqlQueryString = FETCH_COMMERCIAL_ENGINEER;
            	
            	if (sWhereQuery != null){
            		sSqlQueryString = sSqlQueryString + " "+sWhereQuery;
            	}
            	
            	if (sSortFilter != null && sSortFilter.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
            		sSqlQueryString = sSqlQueryString + " ORDER BY "+ sSortFilter;
            		if (sSortType != null && sSortType.trim().length() >QuotationConstants.QUOTATION_LITERAL_ZERO)
            			sSqlQueryString = sSqlQueryString + " " +sSortType;
            	}
            	
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Commercial Engineer Query ",sSqlQueryString);
            	pstmt = conn.prepareCall(sSqlQueryString);
            	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
            	
            	
         		if (rs.next()) {
                     do {
                    	 oTempCommercialEngineerDto = new CommercialEngineerDto();
                    	 oTempCommercialEngineerDto.setDesignEngineerId(rs.getInt("QDE_ENGINEERID"));
                    	 oTempCommercialEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
                    	 oTempCommercialEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
                    	 alSearchResultsList.add(oTempCommercialEngineerDto);
                    } while (rs.next());
                 }
            }finally {
            	/* Releasing PreparedStatement objects */
                oDBUtility.releaseResources( rs,pstmt);
            }
		
		return alSearchResultsList;
	}


}
