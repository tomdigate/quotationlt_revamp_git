package in.com.rbc.quotation.admin.dto;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.collections.map.HashedMap;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewMTOAddOnDto;

public class AddOnDetailsDto {

	private String invoke;
	private int addOnId;
	private String addOnProdGroup;
	private String addOnProdLine;
	private String addOnSpecialFeature;
	private String addOnSpecialFeatureValue;
	private String addOnFrameSize;
	private int addOnValue;
	private String addOnCalculationType;
	private String createdById;
	private String lastUpdatedById;
	private String createdDate;
	private String lastUpdatedDate;
	private ArrayList searchResultsList = null;
	private String operation;
	private HashMap<String, String> addOnSpecialFeatureMap = new HashMap<String, String>();
	private double totalAddOnPercentValue = 0d;
	private double totalAddOnCashExtraValue = 0d;
	
	private String isReferDesign;
	private boolean fetchAddOnList;
	private String flg_Addon_Leads_MTO;
	private String flg_Addon_TBoxSize_MTO;
	private String flg_Addon_BearingDE_MTO;
	private String flg_Addon_CableEntry_MTO;
	
	private String addOnKW;
	private String addOnPole;
	private String addOnFrameSuffix;
	private String addOnMounting;
	private String addOnTBPos;
	private String addOnMfgLoc;

	private ArrayList<KeyValueVo> newMTOTechDetailsList = new ArrayList<KeyValueVo>();
	private ArrayList<NewMTOAddOnDto> newMTOAddOnsList = new ArrayList<NewMTOAddOnDto>();
	
	private String mtoHighlightFlag;
	
	public String getInvoke() {
		return CommonUtility.replaceNull(invoke);
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public int getAddOnId() {
		return addOnId;
	}
	public void setAddOnId(int addOnId) {
		this.addOnId = addOnId;
	}
	public String getAddOnProdGroup() {
		return CommonUtility.replaceNull(addOnProdGroup);
	}
	public void setAddOnProdGroup(String addOnProdGroup) {
		this.addOnProdGroup = addOnProdGroup;
	}
	public String getAddOnProdLine() {
		return CommonUtility.replaceNull(addOnProdLine);
	}
	public void setAddOnProdLine(String addOnProdLine) {
		this.addOnProdLine = addOnProdLine;
	}
	public String getAddOnSpecialFeature() {
		return CommonUtility.replaceNull(addOnSpecialFeature);
	}
	public void setAddOnSpecialFeature(String addOnSpecialFeature) {
		this.addOnSpecialFeature = addOnSpecialFeature;
	}
	public String getAddOnSpecialFeatureValue() {
		return CommonUtility.replaceNull(addOnSpecialFeatureValue);
	}
	public void setAddOnSpecialFeatureValue(String addOnSpecialFeatureValue) {
		this.addOnSpecialFeatureValue = addOnSpecialFeatureValue;
	}
	public String getAddOnFrameSize() {
		return CommonUtility.replaceNull(addOnFrameSize);
	}
	public void setAddOnFrameSize(String addOnFrameSize) {
		this.addOnFrameSize = addOnFrameSize;
	}
	public int getAddOnValue() {
		return addOnValue;
	}
	public void setAddOnValue(int addOnValue) {
		this.addOnValue = addOnValue;
	}
	public String getAddOnCalculationType() {
		return CommonUtility.replaceNull(addOnCalculationType);
	}
	public void setAddOnCalculationType(String addOnCalculationType) {
		this.addOnCalculationType = addOnCalculationType;
	}
	public String getCreatedById() {
		return CommonUtility.replaceNull(createdById);
	}
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}
	public String getLastUpdatedById() {
		return CommonUtility.replaceNull(lastUpdatedById);
	}
	public void setLastUpdatedById(String lastUpdatedById) {
		this.lastUpdatedById = lastUpdatedById;
	}
	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastUpdatedDate() {
		return CommonUtility.replaceNull(lastUpdatedDate);
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public ArrayList getSearchResultsList() {
		return searchResultsList;
	}
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}
	public String getOperation() {
		return CommonUtility.replaceNull(operation);
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public HashMap<String, String> getAddOnSpecialFeatureMap() {
		return addOnSpecialFeatureMap;
	}
	public void setAddOnSpecialFeatureMap(HashMap<String, String> addOnSpecialFeatureMap) {
		this.addOnSpecialFeatureMap = addOnSpecialFeatureMap;
	}
	
	/* totalAddOnPercent and totalAddOnCashExtra */
	public double getTotalAddOnPercentValue() {
		return totalAddOnPercentValue;
	}
	public void setTotalAddOnPercentValue(double totalAddOnPercentValue) {
		this.totalAddOnPercentValue = totalAddOnPercentValue;
	}
	public double getTotalAddOnCashExtraValue() {
		return totalAddOnCashExtraValue;
	}
	public void setTotalAddOnCashExtraValue(double totalAddOnCashExtraValue) {
		this.totalAddOnCashExtraValue = totalAddOnCashExtraValue;
	}
	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}
	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}
	public boolean isFetchAddOnList() {
		return fetchAddOnList;
	}
	public void setFetchAddOnList(boolean fetchAddOnList) {
		this.fetchAddOnList = fetchAddOnList;
	}
	public String getFlg_Addon_Leads_MTO() {
		return CommonUtility.replaceNull(flg_Addon_Leads_MTO);
	}
	public void setFlg_Addon_Leads_MTO(String flg_Addon_Leads_MTO) {
		this.flg_Addon_Leads_MTO = flg_Addon_Leads_MTO;
	}
	public String getFlg_Addon_TBoxSize_MTO() {
		return CommonUtility.replaceNull(flg_Addon_TBoxSize_MTO);
	}
	public void setFlg_Addon_TBoxSize_MTO(String flg_Addon_TBoxSize_MTO) {
		this.flg_Addon_TBoxSize_MTO = flg_Addon_TBoxSize_MTO;
	}
	public String getFlg_Addon_BearingDE_MTO() {
		return CommonUtility.replaceNull(flg_Addon_BearingDE_MTO);
	}
	public void setFlg_Addon_BearingDE_MTO(String flg_Addon_BearingDE_MTO) {
		this.flg_Addon_BearingDE_MTO = flg_Addon_BearingDE_MTO;
	}
	public String getFlg_Addon_CableEntry_MTO() {
		return CommonUtility.replaceNull(flg_Addon_CableEntry_MTO);
	}
	public void setFlg_Addon_CableEntry_MTO(String flg_Addon_CableEntry_MTO) {
		this.flg_Addon_CableEntry_MTO = flg_Addon_CableEntry_MTO;
	}
	
	/* Added New Fields as per Latest Rules */
	
	public String getAddOnKW() {
		return CommonUtility.replaceNull(addOnKW);
	}
	public void setAddOnKW(String addOnKW) {
		this.addOnKW = addOnKW;
	}
	public String getAddOnPole() {
		return CommonUtility.replaceNull(addOnPole);
	}
	public void setAddOnPole(String addOnPole) {
		this.addOnPole = addOnPole;
	}
	public String getAddOnFrameSuffix() {
		return CommonUtility.replaceNull(addOnFrameSuffix);
	}
	public void setAddOnFrameSuffix(String addOnFrameSuffix) {
		this.addOnFrameSuffix = addOnFrameSuffix;
	}
	public String getAddOnMounting() {
		return CommonUtility.replaceNull(addOnMounting);
	}
	public void setAddOnMounting(String addOnMounting) {
		this.addOnMounting = addOnMounting;
	}
	public String getAddOnTBPos() {
		return CommonUtility.replaceNull(addOnTBPos);
	}
	public void setAddOnTBPos(String addOnTBPos) {
		this.addOnTBPos = addOnTBPos;
	}
	public String getAddOnMfgLoc() {
		return CommonUtility.replaceNull(addOnMfgLoc);
	}
	public void setAddOnMfgLoc(String addOnMfgLoc) {
		this.addOnMfgLoc = addOnMfgLoc;
	}
	public ArrayList<KeyValueVo> getNewMTOTechDetailsList() {
		return newMTOTechDetailsList;
	}
	public void setNewMTOTechDetailsList(ArrayList<KeyValueVo> newMTOTechDetailsList) {
		this.newMTOTechDetailsList = newMTOTechDetailsList;
	}
	public ArrayList<NewMTOAddOnDto> getNewMTOAddOnsList() {
		return newMTOAddOnsList;
	}
	public void setNewMTOAddOnsList(ArrayList<NewMTOAddOnDto> newMTOAddOnsList) {
		this.newMTOAddOnsList = newMTOAddOnsList;
	}
	public String getMtoHighlightFlag() {
		return CommonUtility.replaceNull(mtoHighlightFlag);
	}
	public void setMtoHighlightFlag(String mtoHighlightFlag) {
		this.mtoHighlightFlag = mtoHighlightFlag;
	}
	
	
	
}
