package in.com.rbc.quotation.admin.dto;

public class CustomerDiscountBulkLoadDto {

	String invoke = null;
	private String isCustomerDiscountExists = null;
	private long serialId = 0;
	private String mfgLocation;
	private String productGroup;
	private String productLine;
	private String standard;
	private String powerRatingKW;
	private String frameType;
	private int noOfPoles;
	private String mountType;
	private String tbPosition;
	private String customerId;
	private String sapCode;
	private String oracleCode;
	private String gstNumber;
	private String salesEngineerId;
	private String customerDiscount;
	private String smDiscount;
	private String rsmDiscount;
	private String nsmDiscount;
	
	
	public String getInvoke() {
		return invoke;
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getIsCustomerDiscountExists() {
		return isCustomerDiscountExists;
	}
	public void setIsCustomerDiscountExists(String isCustomerDiscountExists) {
		this.isCustomerDiscountExists = isCustomerDiscountExists;
	}
	public long getSerialId() {
		return serialId;
	}
	public void setSerialId(long serialId) {
		this.serialId = serialId;
	}
	public String getMfgLocation() {
		return mfgLocation;
	}
	public void setMfgLocation(String mfgLocation) {
		this.mfgLocation = mfgLocation;
	}
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getPowerRatingKW() {
		return powerRatingKW;
	}
	public void setPowerRatingKW(String powerRatingKW) {
		this.powerRatingKW = powerRatingKW;
	}
	public String getFrameType() {
		return frameType;
	}
	public void setFrameType(String frameType) {
		this.frameType = frameType;
	}
	public int getNoOfPoles() {
		return noOfPoles;
	}
	public void setNoOfPoles(int noOfPoles) {
		this.noOfPoles = noOfPoles;
	}
	public String getMountType() {
		return mountType;
	}
	public void setMountType(String mountType) {
		this.mountType = mountType;
	}
	public String getTbPosition() {
		return tbPosition;
	}
	public void setTbPosition(String tbPosition) {
		this.tbPosition = tbPosition;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSapCode() {
		return sapCode;
	}
	public void setSapCode(String sapCode) {
		this.sapCode = sapCode;
	}
	public String getOracleCode() {
		return oracleCode;
	}
	public void setOracleCode(String oracleCode) {
		this.oracleCode = oracleCode;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getSalesEngineerId() {
		return salesEngineerId;
	}
	public void setSalesEngineerId(String salesEngineerId) {
		this.salesEngineerId = salesEngineerId;
	}
	public String getCustomerDiscount() {
		return customerDiscount;
	}
	public void setCustomerDiscount(String customerDiscount) {
		this.customerDiscount = customerDiscount;
	}
	public String getSmDiscount() {
		return smDiscount;
	}
	public void setSmDiscount(String smDiscount) {
		this.smDiscount = smDiscount;
	}
	public String getRsmDiscount() {
		return rsmDiscount;
	}
	public void setRsmDiscount(String rsmDiscount) {
		this.rsmDiscount = rsmDiscount;
	}
	public String getNsmDiscount() {
		return nsmDiscount;
	}
	public void setNsmDiscount(String nsmDiscount) {
		this.nsmDiscount = nsmDiscount;
	}
	
	
	
}
