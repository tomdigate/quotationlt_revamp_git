/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: LocationAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import in.com.rbc.quotation.admin.dao.LocationDao;
import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.admin.form.LocationForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * <h3>Class Name:-LocationAction</h3> 
 * <br> This class contains the all Business functionality  
 * <br>	-->viewlocationsearch , 
 * <br>	-->Add new Location ,
 * <br>	-->Upate   Location, 
 * <br>	-->Delete  Location, 
 * <br>	-->export  Location into Excel , and 
 * <br>This Class is Extends from BaseAction Class .
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class LocationAction extends BaseAction {
	
	
	/**
	 * <h3>Method Name:-viewlocationsearch</h3> 
	 * <br>Method is Displaying the Location Search result, by default it'll dispaly the all Values from the table. 
	 * <br>By Calling the ListModel class the columns are sorting  
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_VIEW page
	 * @throws Exception 
	 * 				if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward viewlocationsearch(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "viewlocationsearch";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		// Get Form objects from the actionForm
		LocationForm oLocationForm =(LocationForm)actionForm;
		if(oLocationForm ==  null) {
			oLocationForm = new LocationForm();
		}
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		Hashtable htSearchResultsList =null; //Object of Hashtable to store different values
		ListModel oListModel=null;
		DaoFactory oDaoFactory=null;
		LocationDao oLocationDao=null;
		LocationDto oLocationDto=null;
		HttpSession session= request.getSession();
		try {
			saveToken(request);// To avoid entering the duplicate value into database. 
			/*
			* Object of Hashtable to store different values 
			*/
			htSearchResultsList = new Hashtable();
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "locationForm", 
			 "locationForm.invoke", 
			 "viewlocationsearch");
				
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oLocationDao = oDaoFactory.getLocationDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oLocationDto = new LocationDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oLocationDto , oLocationForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList =oLocationDao.getLocationSearchResult(conn,oLocationDto,oListModel);
			
			if(htSearchResultsList != null ) {
				oLocationDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_LOCDTO,oLocationDto);                 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getLocationSearchResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
			}else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
				
				// remove the values that are set in the session
				session.removeAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
			}
			oLocationForm.setRegionList(oLocationDao.getRegionList(conn));
		}catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewLocation Search Result.", ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	}
	
	/**
	 * 
	 * <h3>Method Name:-addNewLocationPage</h3> 
	 * <br>Method Used for to open the add new Location form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm 	ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward addNewLocationPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
      	// Method name Set for log file usage ;
      	String sMethodname = "addNewLocationPage";
      	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
      	setRolesToRequest(request);
      	// Get Form objects from the actionForm
	  	LocationForm oLocationForm =(LocationForm)actionForm;
	  	if(oLocationForm ==  null) {
	  		oLocationForm = new LocationForm();
	  	}
      	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    DaoFactory oDaoFactory=null;  //DaoFactory oDaoFactory
	    LocationDao oLocationDao=null;
	    LocationDto oLocationDto=null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		    oLocationDao = oDaoFactory.getLocationDao();
		    /* Retrieving the database connection using DBUtility.getDBConnection method */
	    	conn = oDBUtility.getDBConnection();
	    	oLocationDto= new LocationDto();
	    	/*
	  	    * setLocationList to dispaly the drop down values in location field   
	  	    */
	    	oLocationForm.setRegionList(oLocationDao.getRegionList(conn));
	    	
	    	oLocationForm.setStatus(QuotationConstants.QUOTATION_STRING_A);
	    	oLocationForm.setLocationName("");
	    	oLocationForm.setRegionId(0);
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "addNewLocation Result.", ExceptionUtility.getStackTraceAsString(e));
	    	/* Setting error details to request to display the same in Exception page */
	    		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        /* Forwarding request to Error page */
	    		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	
	/**
	 * <h3>Method Name:-editLocationPage</h3> 
	 * <br>Method Used for to open the Update exists Location form Page
	 * <br>while opening this page its load the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward editLocationPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response) throws Exception {
		/*
       * Setting user's information in request, using the header 
       * information received through request
       */
  		setRolesToRequest(request);
  	
  	// Method name Set for log file usage ;
  		String sMethodname = "editLocationPage";
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
  		
  	// Get Form objects from the actionForm
  		LocationForm oLocationForm =(LocationForm)actionForm;
	    	if(oLocationForm ==  null) {
	    		oLocationForm = new LocationForm();
	    	}
	    	Connection conn = null;  // Connection object to store the database connection
	    	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    	UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
	    	DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
	    	LocationDao oLocationDao=null;
	    	LocationDto oLocationDto=null;
	    	UserAction oUserAction=null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
	    	/* Instantiating the DBUtility object */
  			oDBUtility = new DBUtility();
  			/* Creating an instance of of DaoFactory  */
  			oDaoFactory = new DaoFactory();
  			oLocationDao = oDaoFactory.getLocationDao();
  			/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            oLocationDto = new LocationDto();
            oDBUtility = new DBUtility();
            oUserAction = new UserAction();
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
             if (oUserDto == null){
          	   oUserDto = new UserDto();
             }
             /* Copying the Form Object into DTO Object via Propertyutils*/
             PropertyUtils.copyProperties( oLocationDto,oLocationForm);
             oLocationDto.setLocationId(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_LOCATIONID));
             oLocationDto = oLocationDao.getLocationInfo(conn,oLocationDto);
	    	 /*
	    	  * setLocationList to dispaly the drop down values in location field with appropriate value  
	    	  * setOperation is used to set the operation values as Update for diff in JSP files to dispaly diff button and header label
	    	  */
             oLocationDto.setRegionList(oLocationDao.getRegionList(conn));
             oLocationDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
		        
	    	 /* Copying the Form Object into DTO Object via Propertyutils*/
	         PropertyUtils.copyProperties( oLocationForm ,oLocationDto);
		         
	     }catch(Exception e) {
		      /*
		       * Logging any exception that raised during this activity in log files using Log4j
		       */
		  		LoggerUtility.log("DEBUG", this.getClass().getName(), "editLocationPage Result.", ExceptionUtility.getStackTraceAsString(e));
		      
		      /* Setting error details to request to display the same in Exception page */
		      	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		      /* Forwarding request to Error page */
		      	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	      }finally {
	  	       	 /* Releasing/closing the connection object */
		         oDBUtility.releaseResources(conn);  	      
	      }
	       LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-updateLocation</h3> 
	 * <br>Method Used for to open the Update exists Location form Page
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward updateLocation(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception  {
     	// Method name Set for log file usage ;
      	String sMethodname = "updateLocation";
      	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	    */
      	setRolesToRequest(request);
      	// Get Form objects from the actionForm
      	 LocationForm oLocationForm =(LocationForm)actionForm;
      	 if(oLocationForm ==  null) {
      		 oLocationForm = new LocationForm();
      	 }
      	 Connection conn = null;  // Connection object to store the database connection
       	 DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       	 UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
       	 boolean isSuccess = false ;
       	 String operation = null;
       	DaoFactory oDaoFactory =null;
       	LocationDao oLocationDao=null;
       	LocationDto oLocationDto=null;
       	UserAction oUserAction=null;
       	 try {
  	       		/* Instantiating the DBUtility object */
        		oDBUtility = new DBUtility();
        		/* Creating an instance of of DaoFactory  */
             	oDaoFactory = new DaoFactory();
             	oLocationDao  = oDaoFactory.getLocationDao();
             	/* Retrieving the database connection using DBUtility.getDBConnection method */
 	            conn = oDBUtility.getDBConnection();
 	            oLocationDto= new LocationDto();
 	          
 	           if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	    resetToken(request);// Reset the Token Request ...
	 	            /* Copying the Form Object into DTO Object via Propertyutils*/
	 	            PropertyUtils.copyProperties(oLocationDto , oLocationForm);
	            	oUserAction = new UserAction();
	            	oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
	            	if (oUserDto == null){
	            	   oUserDto = new UserDto();
	            	}
	            	operation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
	            	oDBUtility.setAutoCommitFalse(conn);// Set Auto Commit False..
	            	
	            	if(operation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
		              	 oLocationDto.setLastUpdatedById(oUserDto.getUserId());
		              	 isSuccess = oLocationDao.saveLocationValue(conn,oLocationDto);
		              	 if(isSuccess)
		              		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_LOCATION+" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);
	            	}else{
	            		oLocationDto.setCreatedById(oUserDto.getUserId());
	            		oLocationDto.setLastUpdatedById(oUserDto.getUserId());
	            		isSuccess = oLocationDao.createLocation(conn,oLocationDto);
	            		if(isSuccess)
	                	   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oLocationDto.getLocationName()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);
	               }
	            	
	               /*
	                * Depond upon the Success value , the page is re-directed with message 
	                */
		 	          if(isSuccess) {
		 	        	  oDBUtility.commit(conn);
		 	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWLOCTION_URL); 
		 	          }else {
		 	        	 oDBUtility.rollback(conn);
		 	        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 	          }
		 	         oDBUtility.setAutoCommitTrue(conn);
 	            }else {
	            	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_MSG_TOKLOC);
	 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	         	}
       	 }catch (SQLException se) {
			/*
			 * if the SQL Exception is Unique Constrain error then customer won't be delete and it'll return the SQL Exception .
			 */
			 LoggerUtility.log("DEBUG", this.getClass().getName(), "Location Adding duplicate values Error Code .", String.valueOf(se.getErrorCode()));
			/*
			 * if the Error is 00001 , then the forward to Error Failure page and dispaly the message as Location details already exists
			 */
			if ( se.getErrorCode() == 00001 ) {
				LoggerUtility.log("DEBUG", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_LOC_EXISTS+" "+String.valueOf(se.getErrorCode()));
				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_LOC_EXISTS);
            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			}
        }catch(Exception e) {
       		 /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
    		LoggerUtility.log("DEBUG", this.getClass().getName(), "location update Result.", ExceptionUtility.getStackTraceAsString(e));
        
    		/* Setting error details to request to display the same in Exception page */
        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
        	/* Forwarding request to Error page */
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
       	 }finally {
  	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	      
       	 }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * <h3>Method Name:-deleteLocation</h3> 
	 * <br>Method Used for to Delete th exists Location
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward deleteLocation(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
	  	// Method name Set for log file usage ;
  		String sMethodname = "deleteLocation";
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
	   /*
        * Setting user's information in request, using the header 
        * information received through request
        */
	  	setRolesToRequest(request);
	  	 // Get Form objects from the actionForm
	  	 LocationForm oLocationForm =(LocationForm)actionForm;
	  	 if(oLocationForm ==  null) {
	  		 oLocationForm = new LocationForm();
	  	 }
	  	 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     LocationDto oLocationDto = null;
	     boolean isdeleted = false ;
	     DaoFactory oDaoFactory =null;
	     LocationDao oLocationDao=null;
	   	 try {
	
     		/* Instantiating the DBUtility object */
  			oDBUtility = new DBUtility();
  			/* Creating an instance of of DaoFactory  */
  			oDaoFactory = new DaoFactory();
  			oLocationDao = oDaoFactory.getLocationDao();
  			oLocationDto = new LocationDto();
  			if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	resetToken(request);// Reset the Token Request ...
	  			/* Retrieving the database connection using DBUtility.getDBConnection method */
	  			conn = oDBUtility.getDBConnection();
	  			/* Copying the Form Object into DTO Object via Propertyutils*/
	  			PropertyUtils.copyProperties(oLocationDto , oLocationForm);
	  			oDBUtility.setAutoCommitFalse(conn);
	  			isdeleted =    oLocationDao.deleteLocationValue(conn,oLocationDto);
	  			if(isdeleted) {
	  				oDBUtility.commit(conn);
		            LoggerUtility.log("INFOR", this.getClass().getName(), "Deleted  Location --","Successfully" );
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oLocationDto.getLocationName()+"\" "+QuotationConstants.QUOTATION_REC_DELSUCC);
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWLOCTION_URL); 
	  			}else {
	    			oDBUtility.rollback(conn);
	    		}
	    		oDBUtility.setAutoCommitTrue(conn);
  			}else {
    			LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_MSG_TOKLOC );
 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
    		}	
	   	 }catch (SQLException se) {
				/*
				 * if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
				 */
				LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete Result Error Code .", String.valueOf(se.getErrorCode()));
				/*
				 * if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
				 */
				if ( se.getErrorCode() == 2292 ) {
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oLocationDto.getLocationName()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWLOCTION_URL); 
	                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
				}
     	}catch(Exception e) {
     		/*
           * Logging any exception that raised during this activity in log files using Log4j
           */
      		LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete result ", ExceptionUtility.getStackTraceAsString(e));
          
          /* Setting error details to request to display the same in Exception page */
          	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
          /* Forwarding request to Error page */
          	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
     	 }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	      
     	 }
     	 LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
     	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
					
	/**
	 * 
	 * <h3>Class Name :- exportLocationSearch </h3>
	 * <br> get the Values from arraylist Object and fetch into Excel Sheet .
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward exportLocationSearch(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		// Method name Set for log file usage ;
  		String sMethodname = "exportCustomerSearch";
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
  		/*
  		 * Setting user's information in request, using the header 
  		 * information received through request
  		 */
  		setRolesToRequest(request);
  	    // Get Form objects from the actionForm
  		LocationForm oLocationForm =(LocationForm)actionForm;
  		if(oLocationForm ==  null) {
  			oLocationForm = new LocationForm();
  		}
	    	 Connection conn = null;  // Connection object to store the database connection
		     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		     LocationDto oLocationDto = null;
		     HttpSession session = request.getSession();
		     String sOperation = null; 
		     String sWhereQuery = null;
		     String sSortFilter = null;
		     String sSortOrder = null;
		     DaoFactory oDaoFactory=null;
		     LocationDao oLocationDao=null;
		     ArrayList alSearchResultsList=new ArrayList();
		     //session.setAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY));
	     try {
	    	     /* Instantiating the DBUtility object */
	 			oDBUtility = new DBUtility();
	 		    /* Creating an instance of of DaoFactory  */
	 			oDaoFactory = new DaoFactory();
	 			oLocationDao = oDaoFactory.getLocationDao();
	 			/* Retrieving the database connection using DBUtility.getDBConnection method */
	 			conn = oDBUtility.getDBConnection();
	 			oLocationDto = new LocationDto();
	 			
	 			sWhereQuery = (String)session.getAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
	 			sOperation = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_FUNCTIONTYPES);
	 			
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
	 				sSortFilter = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
	 			
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
	 				sSortOrder = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
	 			
		         /* Copying the Form Object into DTO Object via Propertyutils*/
		           PropertyUtils.copyProperties(oLocationDto,oLocationForm);       
	           
		         /* Retrieving the search results using oHelpDto.getHelpList method */
		          alSearchResultsList = oLocationDao.exportLocationSearchResult(conn,oLocationDto,sWhereQuery, sSortFilter, sSortOrder);  
	           
		           request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, alSearchResultsList); 
	         
	     }catch(Exception e) {
  	 	/*
           * Logging any exception that raised during this activity in log files using Log4j
           */
      		LoggerUtility.log("DEBUG", this.getClass().getName(), "viewLocation Search Result.", ExceptionUtility.getStackTraceAsString(e));
         
          /* Setting error details to request to display the same in Exception page */
          	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
          /* Forwarding request to Error page */
          	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	    	
	    	 /* Releasing/closing the connection object */
        		oDBUtility.releaseResources(conn);  	  
	     }
	     LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	     
	     if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
	     else if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
	     else 
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}

	/**
	 * 
	 * <h3>Class Name :- updateLocationStatus </h3>
	 * <br> get the Location status from the table and update into ajax function
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward updateLocationStatus(ActionMapping actionMapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {  

	  	// Method name Set for log file usage ;
  		String sMethodname = "updateLocationStatus";
  		String sClassName = this.getClass().getName();
  		LoggerUtility.log("INFO", sClassName, sMethodname, "START");
		
	   /*
        * Setting user's information in request, using the header 
        * information received through request
        */
  		setRolesToRequest(request);
	  	
        // Object of DBUtility class to handle DB connection objects
		DBUtility oDBUtility = new DBUtility();
		
		// Connection object to store the database connection
		Connection conn = null;
		
		UserDto oUserDto = null;
		
		try {			
    		// setting content type to create xml  
    		response.setContentType("text/xml;charset=utf-8");
    		PrintWriter out = response.getWriter();
    		
    		/* Object of ArrayList to hold the retrieved list of options*/
    		ArrayList arlSubBusinessOptionsList = new ArrayList();
    		ArrayList arlCustomerList = new ArrayList();
    		
    		/* StringBuffer to hold the XML */
    		StringBuffer sbBuiltXML = new StringBuffer();
    		
    		/* String that holds the targetObjects values that we get from the request */
    		String sTargetObjects = null;
    		String sAction = null;
    		int iLocationId = 0;
    		
    		boolean isUpdated = false ;
    		/* Instantiating the DBUtility object */
 			oDBUtility = new DBUtility();
 			/* Creating an instance of of DaoFactory  */
 			DaoFactory oDaoFactory = new DaoFactory();
 			LocationDao oLocationDao = oDaoFactory.getLocationDao();
 			/* Retrieving the database connection using DBUtility.getDBConnection method */
 			conn = oDBUtility.getDBConnection();
 			LocationDto oLocationDto= new LocationDto();
 			
 			UserAction oUserAction = new UserAction();
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
             
            if (oUserDto == null){
          	   oUserDto = new UserDto();
            }
    		
            iLocationId = CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_LOCID );
            sAction = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_ACTION);
    		sTargetObjects = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_ADMINLOOKUP_TARGETOBJECTS);
    		
    		oLocationDto.setLocationId(iLocationId);
    		oLocationDto.setStatus(sAction);
    		oLocationDto.setLastUpdatedById(oUserDto.getUserId());
    		
    		oDBUtility.setAutoCommitFalse(conn);
    		isUpdated = oLocationDao.updateLocationStatusValue(conn, oLocationDto);
    		LoggerUtility.log("INFO", sClassName, sMethodname, "Location Status Updated -- " + isUpdated);
    		if ( isUpdated ) {
    			oDBUtility.commit(conn);
    		} else {
    			oDBUtility.rollback(conn);
    		}
    		oDBUtility.setAutoCommitTrue(conn);
    		
			//Setting the status code 200 
            response.setStatus(200, "Get Options (XML) Successful");
            
            //Creating xml
            sbBuiltXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); 
            sbBuiltXML.append("<result>");
    		sbBuiltXML.append("<params>");
    		sbBuiltXML.append("<objType>statusupdate</objType>");
    		sbBuiltXML.append("<target>" + sTargetObjects + "</target>");
    		sbBuiltXML.append("<updated>" + isUpdated + "</updated>");
    		sbBuiltXML.append("<action>" + sAction + "</action>");
    		sbBuiltXML.append("</params>");			             
            sbBuiltXML.append("</result>");		 
            
            LoggerUtility.log("INFO", sClassName, sMethodname, "sbBuiltXML --> " + sbBuiltXML);
            out.print(sbBuiltXML);
		}catch(Exception e) {
			oDBUtility.rollback(conn);
			/*
	           * Logging any exception that raised during this activity in log files using Log4j
	           */
	      		LoggerUtility.log("DEBUG", this.getClass().getName(), "updateLocation status Result.", ExceptionUtility.getStackTraceAsString(e));
	         
	          /* Setting error details to request to display the same in Exception page */
	          	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	          /* Forwarding request to Error page */
	          	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
            
            /*
             * Releasing the resources
             */
			if (oDBUtility != null && conn != null){
				oDBUtility.releaseResources(conn);
			}
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodname, "END");
		return null;
	}

}
