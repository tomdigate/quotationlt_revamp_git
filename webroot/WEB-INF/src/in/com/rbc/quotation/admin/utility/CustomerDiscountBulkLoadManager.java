package in.com.rbc.quotation.admin.utility;

import java.io.InputStream;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dao.CustomerDiscountDao;
import in.com.rbc.quotation.admin.dto.CustomerDiscountBulkLoadDto;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.factory.DaoFactory;

public class CustomerDiscountBulkLoadManager {

	/**
	 * This method retrieves the information from the excel sheet.
	 * @param request HttpServletRequest object to handle request operations
	 * @param xlsPath String variable holding the excel file name & path.
	 * @return ArrayList holding the ListPriceBulkLoadDto objects.
	 * @throws Exception Throws exception to the calling method, in case of any error.
	 */
	public ArrayList retrieveFromExcel(HttpServletRequest request, InputStream ipStrCustDiscountLoadFile) throws Exception {
		String sMethodName = "retrieveFromExcel";
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "START : ");
	    
	    InputStream inputStream = null;
	    POIFSFileSystem fileSystem = null;
	    CustomerDiscountBulkLoadDto oCustomerDiscountBulkLoadDto = null;
	    ArrayList arlCustDiscountsList = null;
	    String cellVal = "";
	    
	    try {
	    	//getting the uploaded file
	        inputStream = ipStrCustDiscountLoadFile;
	        
			if (inputStream != null) {
				/* Retrieving the FileSystem object */
                fileSystem = new POIFSFileSystem(inputStream);
                
				if (fileSystem != null) {
					/* Retrieving the workbook object */
					HSSFWorkbook workBook = new HSSFWorkbook(fileSystem);
					/* Retrieving the work sheet object */
					HSSFSheet sheet = workBook.getSheetAt(0);
					/* Retrieving the rows collection */
					Iterator rows = sheet.rowIterator();
					/* Initializing the employees ArrayList object */
					arlCustDiscountsList = new ArrayList();
					
					/* 
	                 * Starting of loop to parse through the collection of rows
	                 * and retrieve all the CustomerDiscount information from the sheet.
	                 */
					while (rows.hasNext()) {
						/* Retrieving the Excel row object holding a CustomerDiscount Record */
	                    HSSFRow row = (HSSFRow)rows.next();
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "Row No.: " + row.getRowNum());
	                    
	                    /*
	                     * Checking if the file specified for bulk load is valid or not.
	                     * If valid, skipping the first row, since it contains the column headings.
	                     */
	                    if (row.getRowNum() < 1) {
	                    	/* Skipping the Initial row, containing the column related information */
	                        continue;
	                    }
	                    if (row.getRowNum() == 1) {
	                    	Iterator cells = row.cellIterator();
	                        int cellCount = 0;
	                        while (cells.hasNext()) {
	                            /* Retrieving the cell object */
	                            HSSFCell cell = (HSSFCell)cells.next();
	                            cellCount++;
	                        }
	                        LoggerUtility.log("INFO", sClassName, sMethodName, "Cell Count = " + cellCount);
	                        
	                        if (cellCount != Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_CUSTOMERDISCOUNT_BULKUPLOAD_COLUMNCOUNT))) {
	                        	/*
	                             * Case when the number of columns specified in the sheet are not equal to the count required 
	                             * i.e., when not all information is specified.
	                             * Setting error message in request, which has to be displayed in the confirmation/report page.
	                             */
	                            request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, "Please specify valid excel sheet to Bulk Load the Customer Discounts");
	                            LoggerUtility.log("INFO", sClassName, sMethodName, "Please specify valid excel sheet to Bulk Load the Customer Discounts");
	                            
	                            arlCustDiscountsList = null;
	                            break;
	                        }
	                        /* Skipping the first row, containing the column headings */
	                        continue;
	                    }
	                    
	                    /*
	                     * Retrieving the no of records defined in properties file for Bulk Upload
	                     * Break the loop : if record count is more than the count defined in the properties file.
	                     * This is done to handle the performance of the functionality.
	                     */
	                    if ( row.getRowNum() > (Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT))+2 )) {
	                    	/* Setting error details to request to display the same in Exception page */
							request.setAttribute(QuotationConstants.APP_ERRORMESSAGE,
								"The maximum number of records to be uploaded can be "
								+ PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT)
								+ " only. Hence, records more than "
								+ PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT)
								+ " were ignored during the process."
								+ " Please specify rest of the Customer Discount Details in a separate input file, and reload the records.");
	                        break;
	                    }
	                    
	                    if(!CommonUtility.checkIfRowHasEmptyValues(row, QuotationConstants.QUOTATION_CUSTOMERDISCOUNT_VALIDCOLUMNCOUNT)) {
	                    	/* Retrieving the collection of cells */
	                    	Iterator cells = row.cellIterator();
	                    	/* Inititalizing the CustomerDiscountBulkLoadDto object */
	                    	oCustomerDiscountBulkLoadDto = new CustomerDiscountBulkLoadDto();
	                    	
	                    	/* Looping through the cells collection to retrieve the information, depending on the datatype of the cell contents */
	                    	int cellCount = -1;
	                        while (cells.hasNext() && cellCount < Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_CUSTOMERDISCOUNT_BULKUPLOAD_COLUMNCOUNT))) {
	                            cellCount++;
	                            
	                            /* Retrieving the cell object */
	                            HSSFCell cell = (HSSFCell)cells.next();
	                            
	                            LoggerUtility.log("INFO", sClassName, sMethodName, "cellCount = " + cellCount + " -- cell.getCellNum() = " + cell.getCellNum() + " -- cell.getCellType() = " + cell.getCellType());
	                            
	                            switch (cell.getCellType()) {
	                                /*  When cell content is numeric, it can either be a number value or a date value. 
	                                 * Currently No Date Fields are present in Customer Discount Upload.
	                                 * Hence checking the format of the cell for numeric value and retrieving accordingly. */
	                                case HSSFCell.CELL_TYPE_NUMERIC: {                                    
	                                    LoggerUtility.log("INFO", sClassName, sMethodName, "row number  = " + cell.getCellNum());
	                                    /*if (cell.getCellNum() == 10 ) {
	                                        //Cell Type Date.
	                                        cellVal = cell.getDateCellValue().toString();
	                                        String date = new SimpleDateFormat("MM/dd/yyyy").
	                                        format(new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(cellVal.toString()));
	                                        cellVal = date;
	                                        LoggerUtility.log("INFO", sClassName, sMethodName, "Date value - " + cellVal);                                        
	                                    } else { */
	                                    
	                                    //cell type numeric.                                    	
	                                    cellVal = CommonUtility.round(cell.getNumericCellValue(), 0);
	                                    LoggerUtility.log("INFO", sClassName, sMethodName, "Number value - " + cellVal);
	                                    break;
	                                }
	                                case HSSFCell.CELL_TYPE_STRING : {
	                                    // Cell type string.
	                                    String richTextString = cell.getStringCellValue();
	                                    cellVal = richTextString;
	                                    break;
	                                }
	                                case HSSFCell.CELL_TYPE_FORMULA: {
	                                	// Cell type with Decimal value.
	                                    double cellValDoub = cell.getNumericCellValue();
	                                    DecimalFormat df = new DecimalFormat("##");
	                                    cellVal = df.format(cellValDoub).toString();
	                                    break;
	                                }
	                                default : {
	                                    // types other than String and Numeric.
	                                    cellVal = "NA";
	                                    break;
	                                }
	                            }
	                            LoggerUtility.log("INFO", sClassName, sMethodName, "--> Cell No.: " + cell.getCellNum()
	                                                                            + " ::: Cell type: " + cell.getCellType()
	                                                                            + " ::: Cell value: " + cellVal);
	                            
	                            /*
	                             * Check the cell number and store the cell value in appropriate property of the 
	                             * CustomerDiscountBulkLoadDto object, which in-turn, will be stored in an ArrayList.
	                             */
	                            switch (cell.getCellNum()) {
	                            	case 0: {
	                            		oCustomerDiscountBulkLoadDto.setMfgLocation(cellVal);		break;
	                            	}
	                            	case 1: {
	                            		oCustomerDiscountBulkLoadDto.setProductGroup(cellVal);		break;
	                            	}
	                            	case 2: {
	                            		oCustomerDiscountBulkLoadDto.setProductLine(cellVal);		break;
	                            	}
	                            	case 3: {
	                            		if("Yes".equals(String.valueOf(cellVal))){
	                            			oCustomerDiscountBulkLoadDto.setStandard("Y");
										} else {
											oCustomerDiscountBulkLoadDto.setStandard("N");
										}
	                            		break;
	                            	}
	                            	case 4: {
	                            		oCustomerDiscountBulkLoadDto.setPowerRatingKW(cellVal);		break;
	                            	}
	                            	case 5: {
	                            		oCustomerDiscountBulkLoadDto.setFrameType(cellVal);			break;
	                            	}
	                            	case 6: {
	                            		oCustomerDiscountBulkLoadDto.setNoOfPoles(Integer.parseInt(cellVal));	break;
	                            	}
	                            	case 7: {
	                            		oCustomerDiscountBulkLoadDto.setMountType(cellVal);			break;
	                            	}
	                            	case 8: {
	                            		oCustomerDiscountBulkLoadDto.setTbPosition(cellVal);		break;
	                            	}
	                            	case 9: {
	                            		oCustomerDiscountBulkLoadDto.setCustomerId(cellVal.trim());	break;
	                            	}
	                            	case 10: {
	                            		oCustomerDiscountBulkLoadDto.setCustomerDiscount(cellVal);	break;
	                            	}
	                            	case 11: {
	                            		oCustomerDiscountBulkLoadDto.setSmDiscount(cellVal);		break;
	                            	}
	                            	case 12: {
	                            		oCustomerDiscountBulkLoadDto.setRsmDiscount(cellVal);		break;
	                            	}
	                            	case 13: {
	                            		oCustomerDiscountBulkLoadDto.setNsmDiscount(cellVal);		break;
	                            	}
	                            }
	                        }
	                        /* Adding oCustomerDiscountBulkLoadDto to the ArrayList */
                            arlCustDiscountsList.add(oCustomerDiscountBulkLoadDto);
	                    }
					}
				}
			}
			
	    } finally {
	    	if(inputStream != null){
	    		inputStream.close();
	    	}
	        fileSystem = null;
	    }
	    
	    LoggerUtility.log("INFO", sClassName, sMethodName, "END : ");
	    return arlCustDiscountsList;
	}
	
	/**
	 * This method loops through the Customer Discounts List, retrieved from the excel sheet
	 * Validates the information, inserts valid Customer Discounts information in
	 * CUSTOMER_DISCOUNT table and generates report based on the activity.
	 * 
	 * @param conn Connection object to perform database objects
	 * @param arlCustomerDiscountsList ArrayList holding the CustomerDiscountBulkLoadDto objects of all Discounts listed in the excel sheet.
	 * @return Hashtable object which contains different list for inserted records count, invalid records count, existing records count, etc
	 */
	public Hashtable processCustDiscountBulkLoadList(Connection conn, ArrayList arlCustomerDiscountsList, String loggedInUserId) throws Exception {
		String sMethodName = "processCustDiscountBulkLoadList";
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "-START");
	    
	    String sBlankSpaceChar =  "";
	    if (sBlankSpaceChar == null)
	    	sBlankSpaceChar = "";
	    
	    // Hashtable object : To store the List Price Bulk Load report/results.
	    Hashtable htBulkLoadReport = null;
	    // ArrayList to store the list of records that are valid and inserted.
	    ArrayList arlInsertedRecordsList = null;
	    // ArrayList to store the list of records that could not be processed.
	    ArrayList arlExistingRecordsList = null;
	    // ArrayList to store the list of records that could not be processed, since their format is invalid or any other reason.
	    ArrayList arlInvalidRecordsList = null;
	    
	    // String to store the result message to be displayed in bulk load activity report/confirmation page.
	    String sResultString = null;
	    // KeyValueVo object to store ListPrice information in the report, which is used during the process
	    KeyValueVo oKeyValueVo = null;
	    // Boolean FLag to Indicate if List Price can be inserted to DB or not.
	    boolean canProceed = false;
	    
	    CustomerDiscountBulkLoadDto oCustomerDiscountBulkLoadDto = new CustomerDiscountBulkLoadDto();
	    
	    try {
	    	DaoFactory oDaoFactory = new DaoFactory();
	    	CustomerDiscountDao oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
	    	CustomerDao oCustomerDao = oDaoFactory.getCustomerDao();
	    	
	    	if (arlCustomerDiscountsList.size() > 0) {
	    		/* Initializing the objects */
				sResultString = "";
				arlInsertedRecordsList = new ArrayList();
				arlExistingRecordsList = new ArrayList();
				arlInvalidRecordsList = new ArrayList();
				
				// Looping through the Price ArrayList to validate and update database accordingly.
				Iterator it = arlCustomerDiscountsList.iterator();
				
				int i=0;
	            int recordCount = 0;
	            String sDiscountDetails = "";
	            
	            while(it.hasNext()) {
	            	oCustomerDiscountBulkLoadDto = (CustomerDiscountBulkLoadDto)it.next();
	            	
	            	// If Customer Discount value's are Null/Empty/NaN - Skip the record, add to Invalid Records List.
	            	String sCustomerDiscountValue = oCustomerDiscountBulkLoadDto.getCustomerDiscount();
	            	String sSMDiscountValue = oCustomerDiscountBulkLoadDto.getSmDiscount();
	            	String sRSMDiscountValue = oCustomerDiscountBulkLoadDto.getRsmDiscount();
	            	String sNSMDiscountValue = oCustomerDiscountBulkLoadDto.getNsmDiscount();
	            	if(StringUtils.isBlank(sCustomerDiscountValue) || CommonUtility.isNaN(sCustomerDiscountValue)
	            			|| StringUtils.isBlank(sSMDiscountValue) || CommonUtility.isNaN(sSMDiscountValue) 
	            			|| StringUtils.isBlank(sRSMDiscountValue) || CommonUtility.isNaN(sRSMDiscountValue)
	            			|| StringUtils.isBlank(sNSMDiscountValue) || CommonUtility.isNaN(sNSMDiscountValue) ) {
	            		recordCount++;
	            		i++;
	            		// Updating the Invalid Records list.
	            		sDiscountDetails = "Cust. Discount = " + sCustomerDiscountValue + " : SM Discount = " + sSMDiscountValue + " : RSM Discount = " + sRSMDiscountValue + " : NSM Discount = " + sNSMDiscountValue;
	            		arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, sDiscountDetails , "Line No." + (i+2), "This Record contains Invalid List Price value.");
	            		LoggerUtility.log("INFO", sClassName, sMethodName, "Line No." + (i+2) + " - This Record contains Invalid List Price value.");
	            		continue;
	            	}
	            	
	            	// If Required Parameters for LIST_PRICE Table are missing - Skip the record, add to Invalid Records List.
	            	if(StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getMfgLocation()) 
	            			|| StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getProductGroup())
	            			|| StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getProductLine()) 
	            			|| StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getStandard()) 
	            			|| StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getPowerRatingKW())
	            			|| StringUtils.isBlank(oCustomerDiscountBulkLoadDto.getFrameType()) 
	            			|| oCustomerDiscountBulkLoadDto.getNoOfPoles() == 0 ) {
	            		recordCount++;
	            		i++;
	            		// Updating the Invalid Records list.
	            		sDiscountDetails = "Mfg Location = " + oCustomerDiscountBulkLoadDto.getMfgLocation() + ", Prod Group = " + oCustomerDiscountBulkLoadDto.getProductGroup()
	            							+ ", Prod Line = " + oCustomerDiscountBulkLoadDto.getProductLine() + ", Standard = " + oCustomerDiscountBulkLoadDto.getStandard()
	            							+ ", Power Rating = " + oCustomerDiscountBulkLoadDto.getPowerRatingKW() + ", Frame Type = " + oCustomerDiscountBulkLoadDto.getFrameType()
	            							+ ", No. of Poles = " + oCustomerDiscountBulkLoadDto.getNoOfPoles();
	            		arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, sDiscountDetails, "Line No." + (i+2), "Required Parameters for List Price are missing.");
	            		LoggerUtility.log("INFO", sClassName, sMethodName, "Line No." + (i+2) + " - Required Parameters for List Price are missing.");
	            		continue;
	            	}
	            	
	            	// If Customer Discount already present (With matching parameters) - Skip the record, add to Existing Records List.
	            	String isCDExists = "No";
	            	//String sCustomerIdVal = oCustomerDao.fetchCustomerIdByName(conn, oCustomerDiscountBulkLoadDto.getCustomerId());
	            	//oKeyValueVo = oCustomerDiscountDao.isCustomerDiscountExists(conn, oCustomerDiscountBulkLoadDto, sCustomerIdVal);
	            	if (oKeyValueVo != null) {
						recordCount++;
	                	i++;
	                	isCDExists="Yes";
	                	oCustomerDiscountBulkLoadDto.setIsCustomerDiscountExists("Y");
	                	canProceed = false;
	                	// Updating the Existing Records list.
	                	arlExistingRecordsList = updateReportList(arlExistingRecordsList, oKeyValueVo.getValue(), "Line No." + (i+2), "Customer Discount : " + oKeyValueVo.getValue() + " with given Product Line and Frame paramters already exists!");
					} else {
						LoggerUtility.log("INFO", sClassName, sMethodName, " if oKeyValueVo == null. List Price record is not in DB... Can be inserted.");
	                    // Case when List Price record does not exist.
						oCustomerDiscountBulkLoadDto.setIsCustomerDiscountExists("N");
	                    canProceed = true;
					}
	            	
	            	// If all the conditions are satisfied : Insert the List Price.
					if (canProceed) {
						String isRecordInserted = null;     
	                    boolean oCustomerDiscountCreated = false;
	                    
	                    if("N".equals(oCustomerDiscountBulkLoadDto.getIsCustomerDiscountExists())){
	                    	CustomerDiscountDto oCustomerDiscountDto = new CustomerDiscountDto();
	                    	oCustomerDiscountDto.setManufacturing_location(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QEM_LT_MFG_LOCATION, oCustomerDiscountBulkLoadDto.getMfgLocation()));
	                    	oCustomerDiscountDto.setProduct_group(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_PRODGROUP, oCustomerDiscountBulkLoadDto.getProductGroup()));
	                    	oCustomerDiscountDto.setProduct_line(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, oCustomerDiscountBulkLoadDto.getProductLine()));
	                    	if("YES".equalsIgnoreCase(oCustomerDiscountBulkLoadDto.getStandard())) {
	                    		oCustomerDiscountDto.setStandard("Y");
	                    	} else if("NO".equalsIgnoreCase(oCustomerDiscountBulkLoadDto.getStandard())) {
	                    		oCustomerDiscountDto.setStandard("N");
	                    	}
	                    	oCustomerDiscountDto.setPower_rating_kw(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_KW, oCustomerDiscountBulkLoadDto.getPowerRatingKW()));
	                    	oCustomerDiscountDto.setFrame_type(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, oCustomerDiscountBulkLoadDto.getFrameType()));
	                    	oCustomerDiscountDto.setNumber_of_poles(Integer.parseInt(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, String.valueOf(oCustomerDiscountBulkLoadDto.getNoOfPoles()))));
	                    	oCustomerDiscountDto.setMounting_type(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, oCustomerDiscountBulkLoadDto.getMountType()));
	                    	//oCustomerDiscountDto.setTb_position(oCustomerDiscountDao.getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, oCustomerDiscountBulkLoadDto.getTbPosition()));
	                    	oCustomerDiscountDto.setCustomer_name(oCustomerDao.fetchCustomerIdByName(conn, oCustomerDiscountBulkLoadDto.getCustomerId()));
	                    	oCustomerDiscountDto.setSapcode(oCustomerDiscountBulkLoadDto.getSapCode());
	                    	oCustomerDiscountDto.setOraclecode(oCustomerDiscountBulkLoadDto.getOracleCode());
	                    	oCustomerDiscountDto.setGstnumber(oCustomerDiscountBulkLoadDto.getGstNumber());
	                    	oCustomerDiscountDto.setSalesengineer(oCustomerDiscountBulkLoadDto.getSalesEngineerId());
	                    	oCustomerDiscountDto.setCustomer_discount(oCustomerDiscountBulkLoadDto.getCustomerDiscount());
	                    	oCustomerDiscountDto.setSm_discount(oCustomerDiscountBulkLoadDto.getSmDiscount());
	                    	oCustomerDiscountDto.setRsm_discount(oCustomerDiscountBulkLoadDto.getRsmDiscount());
	                    	oCustomerDiscountDto.setNsm_discount(oCustomerDiscountBulkLoadDto.getNsmDiscount());
	                    	
	                    	oCustomerDiscountCreated = oCustomerDiscountDao.addCustomerDiscount(conn, oCustomerDiscountDto);
	                    	if (oCustomerDiscountCreated == true) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Record inserted - " + isRecordInserted);
                                /* Updating the Inserted Records list */
                                arlInsertedRecordsList = updateReportList(arlInsertedRecordsList, oCustomerDiscountDto.getCustomer_discount(), "Line No." + (i+3), oCustomerDiscountDto.getCustomer_discount());
                                recordCount++;
                                i++;
							} else {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Record could not be inserted - " + isRecordInserted);
								/* Updating the Invalid Records list */
								arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, oCustomerDiscountDto.getCustomer_discount(), "Line No." + (i+3), " CustomerDiscount could not be created. Error occurred during the process.");
								recordCount++;
                                i++;
							}
	                    }
					} else {
						LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Customer Discount record not processed - - -");
					}
	            }
	            
	            //System.out.println("Bulk Load Process : Value of  i : " + i);
				//System.out.println("Bulk Load Process : arlCustomerDiscountsList size : " + arlCustomerDiscountsList.size());
				if (i == arlCustomerDiscountsList.size()) {
					sResultString = "Success";
                }
				
				/* Initializing the Hashtable and setting report information */
	            htBulkLoadReport = new Hashtable();
	            htBulkLoadReport.put("RESULT", sResultString);
	            htBulkLoadReport.put("RECORDCOUNT", recordCount);
	            htBulkLoadReport.put("INSERTEDRECORDS", arlInsertedRecordsList);
	            htBulkLoadReport.put("EXISTINGRECORDS", arlExistingRecordsList);
	            htBulkLoadReport.put("INVALIDRECORDS", arlInvalidRecordsList);
	    	}
	    	
	    } finally {
			// releasing the ArrayList objects
			arlInsertedRecordsList = null;
			arlExistingRecordsList = null;
			arlInvalidRecordsList = null;
			oKeyValueVo = null;
		}
	    
	    LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	    return htBulkLoadReport;
	}
	
	/**
	 * Method to set values in Key-Value format, in an object and update it in the specified ArrayList
	 * 
	 * @param arlReportList ArrayList that has to be updated
	 * @param sKey Key value to set
	 * @param sValue1 Value1 to set
	 * @param sValue2 Value2 to set
	 * @return Returns the updated ArrayList
	 */
	private ArrayList updateReportList(ArrayList arlReportList, String sKey, String sValue1, String sValue2) {
		KeyValueVo oKeyValueVo = new KeyValueVo();
		oKeyValueVo.setKey(sKey);
		oKeyValueVo.setValue(sValue1);
		oKeyValueVo.setValue2(sValue2);
		arlReportList.add(oKeyValueVo);
		LoggerUtility.log("INFO", this.getClass().getName(), "updateReportList",
				"CustomerDiscountBulkLoadManager.updateReportList(): Key = " + sKey + " ::: Value 1 = " + sValue1 + " ::: Value 2 = " + sValue2);
		return arlReportList;
	}
}
