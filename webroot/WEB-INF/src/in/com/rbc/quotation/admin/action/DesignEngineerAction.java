/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: DesignEngineerAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 17, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import in.com.rbc.quotation.admin.dao.DesignEngineerDao;
import in.com.rbc.quotation.admin.dto.DesignEngineerDto;
import in.com.rbc.quotation.admin.form.DesignEngineerForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class DesignEngineerAction extends BaseAction{
	
	/**
	 * 
	 * <h3>Method Name:-viewDesignEngineer</h3> 
	 * <br>Method is Displaying the Design Engineer Search result, by default it'll dispaly the all Values from the table. 
	 * <br>By Calling the ListModel class the columns are sorting  
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_VIEW page
	 * @throws Exception 
	 * 				if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @throws SQLException
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward viewDesignEngineer(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception,SQLException {
		//Method name Set for log file usage ;
		String sMethodname = QuotationConstants.QUOTATION_VIEWDE;
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
		if(oDesignEngineerForm ==  null) {
			oDesignEngineerForm = new DesignEngineerForm();
		}
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		HttpSession session= request.getSession();
		Hashtable htSearchResultsList =null;
		ListModel oListModel =null;
		DaoFactory oDaoFactory = null;
		DesignEngineerDao oDesignEngineerDao=null;
		DesignEngineerDto oDesignEngineerDto=null;
		try {
			saveToken(request);// To avoid entering the duplicate value into database. 
			/*
			* Object of Hashtable to store different values 
			*/
			htSearchResultsList = new Hashtable();
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "designEngineerForm", 
			 "designEngineerForm.invoke", 
			 "viewDesignEngineer");
				
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oDesignEngineerDao = oDaoFactory.getDesignEngineerDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oDesignEngineerDto = new DesignEngineerDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oDesignEngineerDto , oDesignEngineerForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList =oDesignEngineerDao.getDesignEngineerSearchResult(conn,oDesignEngineerDto,oListModel);
			if(htSearchResultsList != null ) {
				oDesignEngineerDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_DEDTO,oDesignEngineerDto);                 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getDesignEngineerSearchResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
			}else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
				
				// remove the values that are set in the session
				session.removeAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
			}
		}catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewLocation Search Result.", ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	}
	
	/**
	 * 
	 * <h3>Method Name:-addNewDesignEngineerPage</h3> 
	 * <br>Method Used for to open the add new Design Engineer  form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward addNewDesignEngineerPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = QuotationConstants.QUOTATION_ADDNEWDE;
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
    	//Get Form objects from the actionForm
		DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
		if(oDesignEngineerForm ==  null) {
			oDesignEngineerForm = new DesignEngineerForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
	    DesignEngineerDto oDesignEngineerDto =null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database.
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		    /* Retrieving the database connection using DBUtility.getDBConnection method */
	    	conn = oDBUtility.getDBConnection();
	    	oDesignEngineerDto = new DesignEngineerDto();
	    	oDesignEngineerForm.setDesignEngineerName("");
	    	oDesignEngineerForm.setDesignEngineerSSO("");
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "addNewLocation Result.", ExceptionUtility.getStackTraceAsString(e));
	    		/* Setting error details to request to display the same in Exception page */
	    		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	    		
	    		/* Forwarding request to Error page */
	    		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-editDesignEngineerPage</h3> 
	 * <br>Method Used for to open the Update exists DesignEngineerPage form Page
	 * <br>while opening this page its load the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward editDesignEngineerPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response) throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = QuotationConstants.QUOTATION_EDITDE;
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
		if(oDesignEngineerForm ==  null) {
			oDesignEngineerForm = new DesignEngineerForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
    	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
    	UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
    	DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
    	DesignEngineerDao oDesignEngineerDao  =null;
    	DesignEngineerDto oDesignEngineerDto =null;
    	UserAction oUserAction =null;
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database. 
	    	/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oDesignEngineerDao  = oDaoFactory.getDesignEngineerDao();
	     	/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            oDesignEngineerDto = new DesignEngineerDto();
            oDBUtility = new DBUtility();
            oUserAction = new UserAction();
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
             if (oUserDto == null){
          	   oUserDto = new UserDto();
             }
             /* Copying the Form Object into DTO Object via Propertyutils*/
             PropertyUtils.copyProperties( oDesignEngineerDto,oDesignEngineerForm);
	    	 
             oDesignEngineerDto.setDesignEngineerId(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_DEID));
             oDesignEngineerDto = oDesignEngineerDao.getDesignEngineerInfo(conn,oDesignEngineerDto);
	    	 /*
	    	  * setOperation is used to set the operation values as Update for diff in JSP files to dispaly diff button and header label
	    	  */
             oDesignEngineerDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
	    	 /* Copying the Form Object into DTO Object via Propertyutils*/
	         PropertyUtils.copyProperties( oDesignEngineerForm ,oDesignEngineerDto);
	     }catch(Exception e) {
		      /*
	           * Logging any exception that raised during this activity in log files using Log4j
	           */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "editLocationPage Result.", ExceptionUtility.getStackTraceAsString(e));
		        /* Setting error details to request to display the same in Exception page */
		        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		        	
		        /* Forwarding request to Error page */
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	       	 }finally {
	  	       	 /* Releasing/closing the connection object */
		             	oDBUtility.releaseResources(conn);  	      
	       	 }
	       LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-updateDesinEngineerValue</h3> 
	 * <br>Method Used for to open the Update exists Design Engineer form Page
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward updateDesinEngineerValue(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception  {
		//Method name Set for log file usage ;
    	String sMethodname = QuotationConstants.QUOTATION_UPDATEDE;
    	 LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
	    // Get Form objects from the actionForm
    	DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
 		if(oDesignEngineerForm ==  null) {
 			oDesignEngineerForm = new DesignEngineerForm();
 		}
		 Connection conn = null;  // Connection object to store the database connection
	   	 DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	   	 UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
       	 boolean isSuccess = false ;
       	String sOperation = null;
       	UserAction oUserAction = null;
       	DesignEngineerDto oDesignEngineerDto = null;
       	DaoFactory oDaoFactory =  null;
       	DesignEngineerDao oDesignEngineerDao  = null;
       	String sExistingDesignEngineerSSO=null;
       	int iExists=0;
       	
		boolean isUpdated =false;
       	 try {
  	       		/* Instantiating the DBUtility object */
        		oDBUtility = new DBUtility();
        		/* Creating an instance of of DaoFactory  */
             	oDaoFactory = new DaoFactory();
             	oDesignEngineerDao  = oDaoFactory.getDesignEngineerDao();
 	            oDesignEngineerDto = new DesignEngineerDto();
	 	            
 	           if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	   	resetToken(request);// Reset the Token Request ...
	         	   	/* Retrieving the database connection using DBUtility.getDBConnection method */
 	 	            conn = oDBUtility.getDBConnection();
 	 	            /* Copying the Form Object into DTO Object via Propertyutils*/
 		            PropertyUtils.copyProperties(oDesignEngineerDto , oDesignEngineerForm);
 		    	     
 		           iExists=0;
 		            oUserAction = new UserAction();
 	                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
 	                 if (oUserDto == null){
 	              	   oUserDto = new UserDto();
 	                 }
 	                sOperation =oDesignEngineerForm.getOperation();
 	                 oDBUtility.setAutoCommitFalse(conn);
 	                 if(sOperation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE))
 	                 {
 	                	 oDesignEngineerDto.setLastUpdatedById(oUserDto.getUserId());
 	                	 sExistingDesignEngineerSSO=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_EXTDESSO);
 	                	 if(sExistingDesignEngineerSSO.equalsIgnoreCase(oDesignEngineerDto.getDesignEngineerSSO()))
 	                	 {
 	 	                	 isSuccess = oDesignEngineerDao.saveDesignEngineerVlaue(conn,oDesignEngineerDto);
 	 	                	 if(isSuccess)
 	 	                		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_STRING_SPACE+QuotationConstants.QUOTATION_DE+QuotationConstants.QUOTATION_STRING_SPACE+QuotationConstants.QUOTATION_REC_UPDATESUCC);
 	 	                
 	 	 	       	          if(isSuccess) {
 	 	 	       	        	  oDBUtility.commit(conn);
 	 	 	       	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
 	 	 	       	          }else {
 	 	 	       	        	  oDBUtility.rollback(conn);
 	 	 	       	        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
 	 	 	       	          }
 
 	                	 }
 	                	 else
 	                	 {
 	 	                	iExists =  oDesignEngineerDao.isDEAlreadyExists(conn,oDesignEngineerDto);
 		                    if(iExists==QuotationConstants.QUOTATION_LITERAL_ONE)
 		                    {
 			 					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_DE_EXISTS);
 			 	            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);

 		                    }
 		                    else
 		                    {
 		                    	
 	 	 	                	 isSuccess = oDesignEngineerDao.saveDesignEngineerVlaue(conn,oDesignEngineerDto);
 	 	 	                	 if(isSuccess)
 	 	 	                		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_STRING_SPACE+QuotationConstants.QUOTATION_DE+QuotationConstants.QUOTATION_STRING_SPACE+QuotationConstants.QUOTATION_REC_UPDATESUCC);
 	 	 	                
 	 	 	 	       	          if(isSuccess) {
 	 	 	 	       	        	  oDBUtility.commit(conn);
 	 	 	 	       	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
 	 	 	 	       	          }else {
 	 	 	 	       	        	  oDBUtility.rollback(conn);
 	 	 	 	       	        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
 	 	 	 	       	          }

 		                    }
 	                	 }

 	                 }else {
 	                	 //Insert Design Engineer
 	                	 oDesignEngineerDto.setCreatedById(oUserDto.getUserId());
 	                	 oDesignEngineerDto.setLastUpdatedById(oUserDto.getUserId());
 	                	iExists =  oDesignEngineerDao.isDEAlreadyExists(conn,oDesignEngineerDto);
 		                    if(iExists==QuotationConstants.QUOTATION_LITERAL_ONE)
 		                    {
 			 					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_DE_EXISTS);
 			 	            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);

 		                    }
 		                    else
 		                    {
 		                    	isSuccess = oDesignEngineerDao.createDesignEngineer(conn,oDesignEngineerDto);	
 	 		                     if(isSuccess)
 	 		                    	 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oDesignEngineerDto.getDesignEngineerName()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);

 	 		 	       	          if(isSuccess) {
 	 		 	       	        	  oDBUtility.commit(conn);
 	 		 	       	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
 	 		 	       	          }else {
 	 		 	       	        	  oDBUtility.rollback(conn);
 	 		 	       	        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
 	 		 	       	          }

 		                    }
		                     

 	                 }
 	                 /*
 	                  * Depond upon the Success value , the page is re-directed with message 
 	                  */
 	       	       oDBUtility.setAutoCommitTrue(conn);
 	           }else {
		 		   	LoggerUtility.log("INFO", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_MSG_TOKDE);
	 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	 	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 		   
		 	   }
	       	 }catch (SQLException se) {

	 				/*
	 				 * if the SQL Exception is Unique Constrain error then customer won't be delete and it'll return the SQL Exception .
	 				 */
	 				 LoggerUtility.log("DEBUG", this.getClass().getName(),sMethodname, "Design Engineer Adding duplicate values."+String.valueOf(se.getErrorCode()));
	 				/*
	 				 * if the Error is 00001 , then the forward to Error Failure page and dispaly the message as Location details already exists
	 				 */
	 				if ( se.getErrorCode() == 00001 ) {
	 					try {
	 						iExists=QuotationConstants.QUOTATION_LITERAL_ZERO;
	 						isUpdated =false;
	 					/* Instantiating the DBUtility object */
	 						oDBUtility = new DBUtility();
	 	        		
	 					/* Creating an instance of of DaoFactory  */
	 	             	oDaoFactory = new DaoFactory();
	 	             	
	 	             	oDesignEngineerDao  = oDaoFactory.getDesignEngineerDao();
	 	 	            oDesignEngineerDto = new DesignEngineerDto();
	 	 	            
	 	 	            if (conn != null)
	 	 	               oDBUtility.releaseResources(conn);
	 	 	            
	 	 	            /* Retrieving the database connection using DBUtility.getDBConnection method */
	 	 	            	conn = oDBUtility.getDBConnection();
	 	 	            
	 	 	            /* Copying the Form Object into DTO Object via Propertyutils*/
	 	 	            	PropertyUtils.copyProperties(oDesignEngineerDto , oDesignEngineerForm);
	 	 	            
	 	 	            iExists =  oDesignEngineerDao.isDEAlreadyExists(conn,oDesignEngineerDto);
	 	 	            if(iExists == QuotationConstants.QUOTATION_LITERAL_ONE) {
	 	 	            	oUserAction = new UserAction();
	 	 	                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
	 	 	                 if (oUserDto == null){
	 	 	              	   oUserDto = new UserDto();
	 	 	                 }
	 	 	            	oDesignEngineerDto.setLastUpdatedById(oUserDto.getUserId());
	 	 	            	isUpdated = oDesignEngineerDao.updateExistDE(conn,oDesignEngineerDto);
	 	 	            	oDBUtility.setAutoCommitFalse(conn);
	 	 	            	if(isUpdated) {
		 	       	        	  oDBUtility.commit(conn);
		 	       	        	  oDBUtility.setAutoCommitTrue(conn);
		 	       	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oDesignEngineerDto.getDesignEngineerName()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);
		 	       	        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
		 	       	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
		 	       	          }else {
		 	       	        	  oDBUtility.rollback(conn);
		 	       	        	  oDBUtility.setAutoCommitTrue(conn);
		 	       	        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 	       	          }
	 	 	            }else {
	 	 	            	LoggerUtility.log("DEBUG", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_DE_EXISTS+" = "+ String.valueOf(se.getErrorCode()));
		 					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_DE_EXISTS);
		 	            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	 	 	            }
	 	 	            
	 					}catch(Exception e1) {
	 						 /*
	 			             * Logging any exception that raised during this activity in log files using Log4j
	 			             */
	 			        	LoggerUtility.log("DEBUG", this.getClass().getName(), "DE update Result.", ExceptionUtility.getStackTraceAsString(e1));
	 			            /* Setting error details to request to display the same in Exception page */
	 			            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e1));
	 			           
	 			            /* Forwarding request to Error page */
	 			            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	 					}
	 				}
		    }catch(Exception e) {
	       		 /*
	             * Logging any exception that raised during this activity in log files using Log4j
	             */
	        	LoggerUtility.log("DEBUG", this.getClass().getName(), "DE add or update Result.", ExceptionUtility.getStackTraceAsString(e));
	            /* Setting error details to request to display the same in Exception page */
	            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	            
	            /* Forwarding request to Error page */
	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	       	 }finally {
	  	       	 /* Releasing/closing the connection object */
		         oDBUtility.releaseResources(conn);  	      
	       	 }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * <h3>Method Name:-deleteDesignEngineerValues</h3> 
	 * <br>Method Used for to Delete th exists Design Engineer
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward deleteDesignEngineerValues(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
  		String sMethodname = QuotationConstants.QUOTATION_DELDEVAL;
  		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
  		setRolesToRequest(request);
	  	// Get Form objects from the actionForm
  		DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
 		if(oDesignEngineerForm ==  null) {
 			oDesignEngineerForm = new DesignEngineerForm();
 		}
	  	 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     DesignEngineerDto oDesignEngineerDto = null;
	     boolean isdeleted = false ;
	     DaoFactory oDaoFactory =null;
	     DesignEngineerDao oDesignEngineerDao  =null;
	   	 try {
	   		/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oDesignEngineerDao  = oDaoFactory.getDesignEngineerDao();
	     	oDesignEngineerDto = new DesignEngineerDto();
			if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
				resetToken(request);// Reset the Token Request ...
				/* Retrieving the database connection using DBUtility.getDBConnection method */
				conn = oDBUtility.getDBConnection();
	            /* Copying the Form Object into DTO Object via Propertyutils*/
				PropertyUtils.copyProperties(oDesignEngineerDto , oDesignEngineerForm);
				oDBUtility.setAutoCommitFalse(conn);
				isdeleted =    oDesignEngineerDao.deleteDesignEngineer(conn,oDesignEngineerDto);
				if(isdeleted) {
					oDBUtility.commit(conn);
		            LoggerUtility.log("INFOR", this.getClass().getName(), "Deleted  Design Engineer --","Successfully" );
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oDesignEngineerDto.getDesignEngineerName()+"\" "+QuotationConstants.QUOTATION_REC_DELSUCC);
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
				}else {
					
					oDBUtility.rollback(conn);
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, "");
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oDesignEngineerDto.getDesignEngineerName()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
			
				}
				oDBUtility.setAutoCommitTrue(conn);
			}else {
				LoggerUtility.log("INFO", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_MSG_TOKDE_DELETION);
 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
 	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			}
	   	 }catch (SQLException se) {
				/*
				 * if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
				 */
				LoggerUtility.log("DEBUG", this.getClass().getName(), "Design Engineer delete Result Error Code .", String.valueOf(se.getErrorCode()));
				/*
				 * if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
				 */
				if ( se.getErrorCode() == 2292 ) {
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oDesignEngineerDto.getDesignEngineerName()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWDE_URL); 
	                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
				}
	   	 }catch(Exception e) {
	   		/*
	         * Logging any exception that raised during this activity in log files using Log4j
	         */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete result ", ExceptionUtility.getStackTraceAsString(e));
	        
	        /* Setting error details to request to display the same in Exception page */
	        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        	
	        /* Forwarding request to Error page */
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	   	 }finally {
	       	 /* Releasing/closing the connection object */
	             	oDBUtility.releaseResources(conn);  	      
	   	 }
	   	 LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	   	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * <h3>Class Name :- exportDesignEngineerSearchResult </h3>
	 * <br> get the Values from arraylist Object and fetch into Excel Sheet .
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward exportDesignEngineerSearchResult(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
		String sMethodname =QuotationConstants.QUOTATION_EXPDEVAL;
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		DesignEngineerForm oDesignEngineerForm =(DesignEngineerForm)actionForm;
 		if(oDesignEngineerForm ==  null) {
 			oDesignEngineerForm = new DesignEngineerForm();
 		}
		 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     DesignEngineerDto oDesignEngineerDto = null;
	     HttpSession session = request.getSession();
	     String sOperation = null; 
	     String sWhereQuery = null;
	     String sSortFilter = null;
	     String sSortOrder = null;
	     DaoFactory oDaoFactory =null;
	     DesignEngineerDao oDesignEngineerDao=null;
	     ArrayList arlSearchResultsList =new ArrayList();
	     try {
	    	 	/* Instantiating the DBUtility object */
	 			oDBUtility = new DBUtility();
	 			/* Creating an instance of of DaoFactory  */
	 			oDaoFactory = new DaoFactory();
	 			oDesignEngineerDao = oDaoFactory.getDesignEngineerDao();
	 			/* Retrieving the database connection using DBUtility.getDBConnection method */
	 			conn = oDBUtility.getDBConnection();
	 			 oDesignEngineerDto = new DesignEngineerDto();
	 			sWhereQuery = (String)session.getAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
	 			sOperation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FUNCTIONTYPES);
	 			
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
	 				sSortFilter = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
	 			
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
	 				sSortOrder = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
	 			
	 			/* Copying the Form Object into DTO Object via Propertyutils*/
	           PropertyUtils.copyProperties(oDesignEngineerDto,oDesignEngineerForm);       
	           /* Retrieving the search results using oHelpDto.getHelpList method */
	           arlSearchResultsList = oDesignEngineerDao.exportDesignEngineerSearchResult(conn,oDesignEngineerDto,sWhereQuery, sSortFilter, sSortOrder);  
	           request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, arlSearchResultsList); 
	     }catch(Exception e) {
			 	/*
		         * Logging any exception that raised during this activity in log files using Log4j
		         */
	    		LoggerUtility.log("DEBUG", this.getClass().getName(), "viewLocation Search Result.", ExceptionUtility.getStackTraceAsString(e));
	    		/* Setting error details to request to display the same in Exception page */
	        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        	
	        	/* Forwarding request to Error page */
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	    	 	/* Releasing/closing the connection object */
      			oDBUtility.releaseResources(conn);  	  
	     }
	     LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	     if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
	     else if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
	     else 
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}
}
