package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import in.com.rbc.quotation.admin.dto.CustomerDiscountBulkLoadDto;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.admin.dto.ListPriceBulkLoadDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public interface CustomerDiscountDao {
	
	/**
	 * Method to Get the Customer Discount Search Result - Customer Discount Admin
	 * @param conn
	 * @param oCustomerDiscountDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 02, 2020
	 */
	public Hashtable getCustomerDiscountSearchResult(Connection conn, CustomerDiscountDto oCustomerDiscountDto, ListModel oListModel) throws Exception ;
	
	/**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception If any error occurs during the process
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 3, 2020
	 */
	public CustomerDiscountDto loadLookUpValues(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception ;
 
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getCustomerNamesList(Connection conn)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getSapcodelist(Connection conn)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getOraclecodelist(Connection conn)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getGstnumberlist(Connection conn)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getSalesManagersList(Connection conn)throws Exception;
	
	/**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception If any error occurs during the process
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 3, 2020
	 */
	public boolean addCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception;
 
	/**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception If any error occurs during the process
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 3, 2020
	 */
	public boolean updateCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception ;
 
	/**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception If any error occurs during the process
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 3, 2020
	 */
	public CustomerDiscountDto getCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception;
 
	/**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception If any error occurs during the process
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 3, 2020
	 */
	public boolean deleteCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception;

	/**
	 * Method to Fetch Customer Discount - Place Enquiry Screen
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return Returns the CustomerDiscountDto object
	 * @throws Exception
	 */
	public double fetchCustomerDiscount(Connection conn, CustomerDiscountDto  oCustomerDiscountDto) throws Exception;
	
	/**
	 * Method to Fetch RSM Discount Approval Limit
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return KeyValueVo Returns the RSM Approval Limit as KeyValueVo
	 * @throws Exception
	 */
	public KeyValueVo fetchRSMDiscountApprovalLimit(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception;
	
	/**
	 * Method to Fetch NSM Discount Approval Limit
	 * @param conn Connection object to connect to database
	 * @param oCustomerDiscountDto CustomerDiscountDto object having enquiry details
	 * @return KeyValueVo Returns the RSM Approval Limit as KeyValueVo
	 * @throws Exception
	 */
	public KeyValueVo fetchNSMDiscountApprovalLimit(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception;
	
	/**
	  * This Method is used to check if Customer Discount value already Exists in DB or not (For specific Customer Discount Params).
	  * @param conn Connection object to connect to database
	  * @param oCustomerDiscountBulkLoadDto CustomerDiscountBulkLoadDto that contains the CustomerDiscount Value and related parameters.
	  * @return Return KeyValueVo - To Indicate if ListPrice with matching parameters is already present or not.
	  * @throws Exception If any error occurs during the process
	  */
	 public KeyValueVo isCustomerDiscountExists(Connection conn, CustomerDiscountBulkLoadDto oCustomerDiscountBulkLoadDto, String sCustomerIdVal) throws Exception;

	 public String getOptionsbyAttributeKey(Connection conn, String sLookupField, String sLookupKey) throws Exception;
	 
	 public String getOptionsbyAttributeValue(Connection conn, String sLookupField, String sLookupValue) throws Exception;
	 
	 public ArrayList exportCustDiscountSearchResult(Connection conn, CustomerDiscountDto oCustomerDiscountDto, String sWhereQuery, String sSortFilter, String sSortOrder) throws Exception;
	 
}
