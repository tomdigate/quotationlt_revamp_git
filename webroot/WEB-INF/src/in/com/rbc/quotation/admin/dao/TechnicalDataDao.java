/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: TechnicalDataDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Nov 20, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.util.Hashtable;
import java.util.List;

import in.com.rbc.quotation.admin.dto.TechnicalDataDto;
import in.com.rbc.quotation.common.utility.ListModel;

/**
 * @author 900008798 (Abhilash Moola)
 *
 */
public interface TechnicalDataDao {
	
	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public Hashtable getTechnicalSearchResult(Connection conn, TechnicalDataDto oTechnicalDataDto, ListModel oListModel) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public boolean addNewTechnicalData(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public TechnicalDataDto getTechnicalDataInfo(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public boolean updateTechnicalData(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception;
	

	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public boolean deleteTechnicalDataValue(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @param oTechnicalDataDto
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Nov 20, 2020
	 */
	public boolean uploadBulkTechnicalData(Connection conn, List<TechnicalDataDto> oListTechnicalDataDto) throws Exception;

}
