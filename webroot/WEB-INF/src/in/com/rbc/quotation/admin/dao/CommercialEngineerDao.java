/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CommercialEngineerDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: CommercialEngineer DAO Interface
 * *****************************************************************************
 * Author: 900010540 (Gangadhar)
 * Date: Mar 1, 2019
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.CommercialEngineerDto;
import in.com.rbc.quotation.common.utility.ListModel;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;


public interface CommercialEngineerDao {
	/**
	 * To fetch CE search results
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object
	 * @param oListModel ListModel Object
	 * @return Hashtable object
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public Hashtable getCommercialEngineerSearchResult(Connection conn, CommercialEngineerDto oCommercialEngineerDto, ListModel oListModel) throws Exception ;
	
	/**
	 * To create CommercialEngineer 
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on create success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean createCommercialEngineer(Connection conn, CommercialEngineerDto oCommercialEngineerDto)throws Exception ;
	
	/**
	 *  To fetch CommercialEngineer Information
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return CommercialEngineerDto object with CE data
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public CommercialEngineerDto getCommercialEngineerInfo(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception ;
	
	/**
	 * To update the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on save success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean saveCommercialEngineerVlaue(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception ;
	
	/**
	 * To delete the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @return boolean true on delete success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean deleteCommercialEngineer(Connection conn , CommercialEngineerDto oCommercialEngineerDto)throws Exception;
	
	/** 
	 * To export the CommercialEngineer search results data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 
	 * @param sWhereQuery where checks string to append the query
	 * @param sSortFilter SortFilter String
	 * @param sSortType SortType String
	 * @return ArrayList object with CE search results data
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public ArrayList exportCommercialEngineerSearchResult(Connection conn , CommercialEngineerDto oCommercialEngineerDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception ;
	
	
	/** 
	 * To check whether given CE already exists
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 	 
	 * @return int value 1 if exists else 0
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public int isCEAlreadyExists(Connection conn , CommercialEngineerDto oCommercialEngineerDto) throws Exception ;
	
	/**
	 *  To update the CommercialEngineer data
	 * @param conn Connection object to connect to database
	 * @param oCommercialEngineerDto CommercialEngineerDto Object	 	 
	 * @return boolean true on update success else false
	 * @throws Exception If any error occurs during the process
	 * @author 900010540 (Gangadhar)
	 * Created on: March 1,2019
	 */
	public boolean updateExistCE(Connection conn , CommercialEngineerDto oCommercialEngineerDto) throws Exception ;


}
