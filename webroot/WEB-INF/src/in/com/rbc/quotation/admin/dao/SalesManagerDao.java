/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: SalesManagerDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 6, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.SalesManagerDto;
import in.com.rbc.quotation.common.utility.ListModel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public interface SalesManagerDao {
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public Hashtable getSalesManagerSearchResult(Connection conn, SalesManagerDto oSalesManagerDto, ListModel oListModel) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public boolean createSalesManager(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public SalesManagerDto getSalesManagerInfo(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public boolean saveSalesManagerValue(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public boolean deleteSalesManagerValue(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @param sWhereQuery
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public ArrayList exportSalesManagerSearchResult(Connection conn, SalesManagerDto oSalesManagerDto,String sWhereQuery, String sSortOrder, String sSortType)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oSalesManagerDto
	 * @param sWhereQuery
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public int doesSalesManagerExist(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception ;
	
	public boolean updateExistSM(Connection conn ,SalesManagerDto oSalesManagerDto) throws Exception ;

}
