/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: SalesManagerAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 6, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */

import in.com.rbc.quotation.admin.dao.LocationDao;
import in.com.rbc.quotation.admin.dao.SalesManagerDao;
import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.admin.dto.SalesManagerDto;
import in.com.rbc.quotation.admin.form.LocationForm;
import in.com.rbc.quotation.admin.form.SalesManagerForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * <h3>Class Name:-SalesManagerAction</h3> 
 * <br> This class contains the all Business functionality  
 * <br>	-->viewsalesmanagerSearchResult , 
 * <br>	-->Add new Sales Manager ,
 * <br>	-->Upate   Sales Manager, 
 * <br>	-->Delete  Sales Manager, 
 * <br>	-->export  Sales Manager into Excel , and 
 * <br>This Class is Extends from BaseAction Class .
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class SalesManagerAction extends BaseAction {

	/**
	 * <h3>Method Name:-viewsalesmanagerSearchResult</h3> 
	 * <br>Method is Displaying the Sales Manager Search result, by default it'll dispaly the all Values from the table. 
	 * <br>By Calling the ListModel class the columns are sorting  
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_VIEW page
	 * @throws Exception 
	 * 				if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward viewsalesmanagerSearchResult(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request, HttpServletResponse response)throws Exception {
		// Method name Set for log file usage ;
		String sMethodname = "viewsalesmanagerSearchResult";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;
		if(oSalesManagerForm ==  null) {
			oSalesManagerForm = new SalesManagerForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		Hashtable htSearchResultsList =null; //Object of Hashtable to store different values
		ListModel oListModel=null;
		DaoFactory oDaoFactory=null;
		SalesManagerDao oSalesManagerDao=null;
		LocationDao oLocationDao =null;
		SalesManagerDto oSalesManagerDto=null;
		HttpSession session= request.getSession();
		try {
			saveToken(request);// To avoid entering the duplicate value into database. 
			
			htSearchResultsList = new Hashtable();
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "salesManagerForm", 
			 "salesManagerForm.invoke", 
			 "viewsalesmanagerSearchResult");
				
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oSalesManagerDao = oDaoFactory.getSalesManagerDao();
			
			oLocationDao = oDaoFactory.getLocationDao();// to get the region List for drop down list
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oSalesManagerDto = new SalesManagerDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oSalesManagerDto , oSalesManagerForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList =oSalesManagerDao.getSalesManagerSearchResult(conn,oSalesManagerDto,oListModel);
			
			if(htSearchResultsList != null ) {
				oSalesManagerDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_SALESMGRDTO,oSalesManagerDto);                 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getSalesManagerSearchResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
				
				
			}else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(QuotationConstants.QUOTATION_LITERAL_ZERO));
				
				//remove the values that are set in the session
				session.removeAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
			}
			oSalesManagerForm.setRegionList(oLocationDao.getRegionList(conn));
		}catch(Exception e) {
		
		/*
		* Logging any exception that raised during this activity in log files using Log4j
		*/
		LoggerUtility.log("DEBUG", this.getClass().getName(), "viewSalesManager Search Result.", ExceptionUtility.getStackTraceAsString(e));
		
		/* Setting error details to request to display the same in Exception page */
		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		/* Forwarding request to Error page */
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	}

	/**
	 * 
	 * <h3>Method Name:-addNewSalesManagerPage</h3> 
	 * <br>Method Used for to open the add new Sales Manager form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward addNewSalesManagerPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		// Method name Set for log file usage ;
    	String sMethodname = "addNewLocationPage";
    	DaoFactory oDaoFactory =null;
    	SalesManagerDao oSalesManagerDao=null;
    	LocationDao oLocationDao=null;
    	SalesManagerDto oSalesManagerDto=null;
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
    	SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;
		if(oSalesManagerForm ==  null) {
			oSalesManagerForm = new SalesManagerForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    try {
	    	saveToken(request);// To avoid entering the duplicate value into database.    		 
		    /* Instantiating the DBUtility object */
		    oDBUtility = new DBUtility();
		    /* Creating an instance of of DaoFactory  */
		    oDaoFactory = new DaoFactory();
		    oSalesManagerDao = oDaoFactory.getSalesManagerDao(); 
		    oLocationDao = oDaoFactory.getLocationDao();
		    /* Retrieving the database connection using DBUtility.getDBConnection method */
		    conn = oDBUtility.getDBConnection();
		    oSalesManagerDto= new SalesManagerDto();
		 /*
		  * setLocationList to display the drop down values in location field with appropriate value  
		  */
	    	oSalesManagerForm.setRegionList(oLocationDao.getRegionList(conn));
	    	oSalesManagerForm.setRegionId(QuotationConstants.QUOTATION_LITERAL_ZERO);
	    	oSalesManagerForm.setAdditionalRSM("");
	    	oSalesManagerForm.setAdditionalRSMSSO("");
	    	oSalesManagerForm.setSalesManager("");
	    	oSalesManagerForm.setSalesManagerSSO("");
	    	oSalesManagerForm.setPrimaryRSM("");
	    	oSalesManagerForm.setPrimaryRSMSSO("");
	    }catch(Exception e){
	    	/*
	    	 * Logging any exception that raised during this activity in log files using Log4j
	    	 */
	    	LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname, ExceptionUtility.getStackTraceAsString(e));
	    	/* Setting error details to request to display the same in Exception page */
	    	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	    	
	        /* Forwarding request to Error page */
	    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     } finally {
	       	 /* Releasing/closing the connection object */
	         oDBUtility.releaseResources(conn);  	       		 
	     }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-editSalesManagerPage</h3> 
	 * <br>Method Used for to open the Update exists SalesManagerPage form Page
	 * <br>while opening this page its load the all form values .
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward editSalesManagerPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response) throws Exception {
		//Method name Set for log file usage ;
		String sClassName = this.getClass().getName();
		String sMethodname = "editSalesManagerPage";
		DaoFactory oDaoFactory =null;
		LocationDao oLocationDao=null;
		SalesManagerDao oSalesManagerDao=null;
		SalesManagerDto oSalesManagerDto=null;
		UserAction oUserAction=null;
	  	Connection conn = null;  // Connection object to store the database connection
	  	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	  	UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
	  	int oSalesManagerId=0;
		LoggerUtility.log("INFO",sClassName,sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
			setRolesToRequest(request);
	    // Get Form objects from the actionForm
		SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;
	  	if(oSalesManagerForm ==  null) {
	  		oSalesManagerForm = new SalesManagerForm();
	  	}
		try {
			saveToken(request);// To avoid entering the duplicate value into database. 
    		/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oLocationDao = oDaoFactory.getLocationDao();
			oSalesManagerDao = oDaoFactory.getSalesManagerDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oSalesManagerDto = new SalesManagerDto();
			oUserAction = new UserAction();
			oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
       
			if (oUserDto == null){
        	   oUserDto = new UserDto();
			}
			oSalesManagerId= Integer.parseInt(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_SMID));
			oSalesManagerDto.setSalesMgrId(oSalesManagerId);
			LoggerUtility.log("INFO",sClassName,sMethodname,"Before Calling od SalesManagerInfo Impl mehtod :== Sales Maneger ID :=="+oSalesManagerId);
			oSalesManagerDto = oSalesManagerDao.getSalesManagerInfo(conn,oSalesManagerDto);
			LoggerUtility.log("INFO",sClassName,sMethodname,"After Calling od SalesManagerInfo Impl mehtod");
		  	 /*
		  	  * setLocationList to dispaly the drop down values in location field with appropriate value  
		  	  * setOperation is used to set the operation values as Update for diff in JSP files to dispaly diff button and header label
		  	  */
			oSalesManagerDto.setRegionList(oLocationDao.getRegionList(conn));
			oSalesManagerDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties( oSalesManagerForm ,oSalesManagerDto);
			LoggerUtility.log("INFO",sClassName,sMethodname,"Sales manager name "+oSalesManagerForm.getSalesManager());
		         
	     	}catch(Exception e) {
		      /*
		       * Logging any exception that raised during this activity in log files using Log4j
		       */
		  		LoggerUtility.log("DEBUG", sClassName, sMethodname, ExceptionUtility.getStackTraceAsString(e));
		      
		      /* Setting error details to request to display the same in Exception page */
		      	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		      /* Forwarding request to Error page */
		      	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
 		 
	       }finally {
	  	       	 /* Releasing/closing the connection object */
		             	oDBUtility.releaseResources(conn);  	      
	       }
	       LoggerUtility.log("INFO",sClassName,sMethodname,"END");
	       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-updateSalesManager</h3> 
	 * <br>Method Used for to open the Update exists SalesManager form Page
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward updateSalesManager(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception  {
		// Method name Set for log file usage ;
		String sMethodname = "updateSalesManager";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
	   /*
	    * Setting user's information in request, using the header 
	    * information received through request
	    */
	    setRolesToRequest(request);
	    // Get Form objects from the actionForm
    	SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;

    	if(oSalesManagerForm ==  null) {
    		oSalesManagerForm = new SalesManagerForm();
    	 }
    	 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
	     DaoFactory oDaoFactory =null;
	     SalesManagerDao oSalesManagerDao=null;
	     SalesManagerDto oSalesManagerDto=null;
	     UserAction oUserAction=null;
	     boolean isSuccess = false ;
	     String sOperation = null;
	     String sExistingSmSSOId=null;
	     int iExist=0;
	     try {
		    	/* Instantiating the DBUtility object */
		        oDBUtility = new DBUtility();
		        /* Creating an instance of of DaoFactory  */
		        oDaoFactory = new DaoFactory();
		        oSalesManagerDao  = oDaoFactory.getSalesManagerDao();
		        /* Retrieving the database connection using DBUtility.getDBConnection method */
		 	   
		 	    oSalesManagerDto = new SalesManagerDto();
		 	    
		 	   if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	   resetToken(request);// Reset the Token Request ...
	         	   conn = oDBUtility.getDBConnection();
			 	   /* Copying the Form Object into DTO Object via Propertyutils*/
	         	   PropertyUtils.copyProperties(oSalesManagerDto , oSalesManagerForm);

			       oUserAction = new UserAction();
			       oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
			       if (oUserDto == null){
			    	   oUserDto = new UserDto();
			       }
			       sOperation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
			       sExistingSmSSOId=CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OLDSMSSO);
			       
			       if(sOperation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
			    	   oSalesManagerDto.setLastUpdatedById(oUserDto.getUserId());
			    	   if(oSalesManagerDto.getSalesManagerSSO().equalsIgnoreCase(sExistingSmSSOId))
			    	   {
							oDBUtility.setAutoCommitFalse(conn);
							isSuccess = oSalesManagerDao.saveSalesManagerValue(conn,oSalesManagerDto);
			          		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_UPDATION_SUCCESS);
			            	 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);

							LoggerUtility.log("INFO", sClassName, sMethodname, "Sales Mangeer Upadte result  Inside Else loop -- " + isSuccess);
  
							 if(isSuccess) {
				            	   oDBUtility.commit(conn);
				            	  // request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "SalesManger "+QuotationConstants.QUOTATION_REC_UPDATESUCC);
				               	}else {
				               		oDBUtility.rollback(conn);
				               	}
				               oDBUtility.setAutoCommitTrue(conn);

			    	   }
			    	   else
			    	   {
			    		   
			    		   iExist = oSalesManagerDao.doesSalesManagerExist(conn, oSalesManagerDto);  
			    		   if(iExist == QuotationConstants.QUOTATION_LITERAL_MINUSONE){
		          		   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,QuotationConstants.QUOTATION_UPDATION_FAILED);
		          		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, oSalesManagerDto.getSalesManager()+QuotationConstants.QUOTATION_UPDATE_FAILED);
		          	       request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL); 
		          	     
			    		   }
			    		   else
			    		   {
								oDBUtility.setAutoCommitFalse(conn);
								isSuccess = oSalesManagerDao.saveSalesManagerValue(conn,oSalesManagerDto);
				          		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_UPDATION_SUCCESS);
				            	 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);

								LoggerUtility.log("INFO", sClassName, sMethodname, "Sales Mangeer Upadte result  Inside Else loop -- " + isSuccess);

								 if(isSuccess) {
					            	   oDBUtility.commit(conn);

					               	}else {
					               		oDBUtility.rollback(conn);
					               	}
					               oDBUtility.setAutoCommitTrue(conn);

			    		   }
			    	   }
					   	
				          if(isSuccess) {

				        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);
				          }else {
			          		   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,QuotationConstants.QUOTATION_UPDATION_FAILED);
			          		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, oSalesManagerDto.getSalesManager()+QuotationConstants.QUOTATION_UPDATE_FAILED);
			          	       request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_VIEWSM_URL); 

				        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				          }

		               
					   
			       }else {
			    	   
			    	   
			    	   //Sales Managers are adding
			    	   LoggerUtility.log("INFO", sClassName, sMethodname, "sales manager creation Else Loop ");
		          	   oSalesManagerDto.setCreatedById(oUserDto.getUserId());
		          	   oSalesManagerDto.setLastUpdatedById(oUserDto.getUserId());
		          
		          	   
		          	   //check if the sales manager exists or not
		          	    iExist = oSalesManagerDao.doesSalesManagerExist(conn, oSalesManagerDto);	
		          	   if (iExist == QuotationConstants.QUOTATION_LITERAL_MINUSONE){

		          		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_INSERTION_FAILED);
		          		 request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, oSalesManagerDto.getSalesManager()+" "+QuotationConstants.QUOTATION_SMALREDAY_EXISTS);
		          	     request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);

		          		   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		          	   }else {

		          		   oDBUtility.setAutoCommitFalse(conn);
			               isSuccess = oSalesManagerDao.createSalesManager(conn,oSalesManagerDto);
			          		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_INSERTION_SUCCESS);
			            	 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);


			               LoggerUtility.log("INFO", sClassName, sMethodname, "Sales Mangeer Creation result -- " + isSuccess);
		          	   }
		          	   
		               if(isSuccess) {
		            	   oDBUtility.commit(conn);
		            	   
		               	}else {
		               		oDBUtility.rollback(conn);
		               	}
		               oDBUtility.setAutoCommitTrue(conn);
				          if(isSuccess) {

				        	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);
				          }else {
			          		   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_INSERTION_FAILED);
			          		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, oSalesManagerDto.getSalesManager()+QuotationConstants.QUOTATION_INSERT_FAILED);
			          	       request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);

				        	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				          }

			       }//Else Loop
				      /*
			          * Depond upon the Success value , the page is re-directed with message 
			          */
		 	   }else {
		 		   	LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname,"Message for Token handling in Updation of Sales Manger ");
		 		   request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_INVALID_ACTION);
		 		   	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	          	       request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);

		 		   	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 		   
		 	   }
	     	}catch (SQLException se) {
 				/*
 				 * if the SQL Exception is Unique Constrain error then customer won't be delete and it'll return the SQL Exception .
 				 */
 				 LoggerUtility.log("DEBUG", this.getClass().getName(),sMethodname, "Sales Manager added duplicate value Error Code = "+String.valueOf(se.getErrorCode()));
 				/*
 				 * if the Error is 00001 , then the forward to Error Failure page and dispaly the message as Location details already exists
 				 */
 				if ( se.getErrorCode() == 00001 ) {
 					LoggerUtility.log("DEBUG", this.getClass().getName(), QuotationConstants.QUOTATION_SM_ALREADY_EXISTS, String.valueOf(se.getErrorCode()));
 					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_SM_ALREADY_EXISTS);
 	            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
 				}
	        }catch(Exception e) {
	       		 /*
	             * Logging any exception that raised during this activity in log files using Log4j
	             */
	        		LoggerUtility.log("DEBUG", this.getClass().getName(), "sales manager update Result.", ExceptionUtility.getStackTraceAsString(e));
	            /* Setting error details to request to display the same in Exception page */
	            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	            /* Forwarding request to Error page */
	            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	       	 }finally {
	  	       	 /* Releasing/closing the connection object */
		             	oDBUtility.releaseResources(conn);  	      
	       	 }
	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * <h3>Method Name:-deleteSalesManager</h3> 
	 * <br>Method Used for to Delete th exists SalesManager
	 * @param  actionForm   ActionForm object to handle the form elements
	 * @param  request  	HttpServletRequest object to handle request operations
	 * @param  response 	HttpServletResponse object to handle response operations
	 * @return returns    	ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward deleteSalesManager(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
	  	String sMethodname = "deleteSalesManager";
	  	String sClassName=this.getClass().getName();
  		LoggerUtility.log("INFO", sClassName,sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
		setRolesToRequest(request);
	  	 // Get Form objects from the actionForm
	  	 SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;
	  	 if(oSalesManagerForm ==  null) {
	  		oSalesManagerForm = new SalesManagerForm();
	  	 }
	  	 Connection conn = null;  // Connection object to store the database connection
	     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	     SalesManagerDto oSalesManagerDto = null;
	     boolean isdeleted = false ;
	     DaoFactory oDaoFactory =null;
	     SalesManagerDao oSalesManagerDao =null;
	     String sRegionId="";

	   	 try {
	
	   		/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oSalesManagerDao = oDaoFactory.getSalesManagerDao();
			oSalesManagerDto = new SalesManagerDto();
			if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
		        resetToken(request);// Reset the Token Request ...
				/* Retrieving the database connection using DBUtility.getDBConnection method */
				conn = oDBUtility.getDBConnection();
				/* Copying the Form Object into DTO Object via Propertyutils*/
				PropertyUtils.copyProperties(oSalesManagerDto , oSalesManagerForm);
				LoggerUtility.log("INFO", sClassName, sMethodname, "Sales Mangeer SSO -- " + oSalesManagerDto.getSalesManagerSSO());
				oDBUtility.setAutoCommitFalse(conn);//Set Auto Commit as False
				sRegionId=request.getParameter(QuotationConstants.QUOTATION_REGIONID).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:request.getParameter(QuotationConstants.QUOTATION_REGIONID);
				
				oSalesManagerDto.setRegionId(Integer.parseInt(sRegionId));
				isdeleted =    oSalesManagerDao.deleteSalesManagerValue(conn,oSalesManagerDto);
				LoggerUtility.log("INFO", sClassName, sMethodname, "Sales Mangeer Deleted result -- " + isdeleted);
				if(isdeleted) {
					oDBUtility.commit(conn);
	            	LoggerUtility.log("INFOR", this.getClass().getName(), "Deleted  Sales Manager --","Successfully" );
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,QuotationConstants.QUOTATION_DELETED_SUCCESS);
	            	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_DELSUCC);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_VIEWSM_URL); 
				}else {
					oDBUtility.rollback(conn);
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,QuotationConstants.QUOTATION_DELETED_FAILED);
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);
	                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
				}
				oDBUtility.setAutoCommitTrue(conn);
			}else {
				LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "Message for Token handling in Deletion of Sales Manger" );
 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			}
	   	 }catch (SQLException se) {
				/*
				 * if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
				 */
				LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete Result Error Code .", String.valueOf(se.getErrorCode()));
				/*
				 * if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
				 */
				if ( se.getErrorCode() == 2292 ) {
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oSalesManagerDto.getSalesManager()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
	                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWSM_URL);
	                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
				}
	   	 }catch(Exception e) {
	   		/*
	         * Logging any exception that raised during this activity in log files using Log4j
	         */
    		LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete result ", ExceptionUtility.getStackTraceAsString(e));
        
    		/* Setting error details to request to display the same in Exception page */
        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
        	/* Forwarding request to Error page */
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
   	 }finally {
	       	 /* Releasing/closing the connection object */
	         oDBUtility.releaseResources(conn);  	      
   	 }
   	 LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
   	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * 
	 * <h3>Class Name :- exportSalesManagerSearch </h3>
	 * <br> get the Values from arraylist Object and fetch into Excel Sheet .
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	public ActionForward exportSalesManagerSearch(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = "exportSalesManagerSearch";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName,sMethodname,"START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		SalesManagerForm oSalesManagerForm =(SalesManagerForm)actionForm;
		if(oSalesManagerForm ==  null) {
			oSalesManagerForm = new SalesManagerForm();
		}
	    	 Connection conn = null;  // Connection object to store the database connection
		     DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		     SalesManagerDto oSalesManagerDto = null;
		     HttpSession session = request.getSession();
		     String sOperation = null; 
		     String sWhereQuery = null;
		     String sSortFilter = null;
		     String sSortOrder = null;
		     DaoFactory oDaoFactory =null;
		     SalesManagerDao oSalesManagerDao =null;
		     ArrayList alSearchResultsList=null;

	     try {
	    	 	/* Instantiating the DBUtility object */
	 			oDBUtility = new DBUtility();
	 			/* Creating an instance of of DaoFactory  */
	 			oDaoFactory = new DaoFactory();
	 			oSalesManagerDao = oDaoFactory.getSalesManagerDao();
	 			/* Retrieving the database connection using DBUtility.getDBConnection method */
	 			conn = oDBUtility.getDBConnection();
	 			oSalesManagerDto = new SalesManagerDto();
	 			
	 			sWhereQuery = (String)session.getAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
	 				sSortFilter = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
	 			
	 			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
	 				sSortOrder = (String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
	 			
	 			sOperation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FUNCTIONTYPES);
	 			
	 			/* Copying the Form Object into DTO Object via Propertyutils*/
	           PropertyUtils.copyProperties(oSalesManagerDto,oSalesManagerForm);       
	           
	           /* Retrieving the search results using oHelpDto.getHelpList method */
	           alSearchResultsList=new ArrayList();
	           alSearchResultsList = oSalesManagerDao.exportSalesManagerSearchResult(conn,oSalesManagerDto,sWhereQuery, sSortFilter, sSortOrder);  
	           
	           request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, alSearchResultsList); 
	         
	     }catch(Exception e) {
		 	/*
	         * Logging any exception that raised during this activity in log files using Log4j
	         */
    		LoggerUtility.log("DEBUG", this.getClass().getName(), "viewLocation Search Result.", ExceptionUtility.getStackTraceAsString(e));
       
    		/* Setting error details to request to display the same in Exception page */
        	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

        	/* Forwarding request to Error page */
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	    	
	    	 /* Releasing/closing the connection object */
      		 oDBUtility.releaseResources(conn);  	  
	     }
	     LoggerUtility.log("INFO", sClassName,sMethodname,"END");
	     
	     if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
	     else if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
	     else 
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}

	
}

