/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: UpdateManagersAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 13, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import in.com.rbc.quotation.admin.dao.UpdateManagersDao;
import in.com.rbc.quotation.admin.dto.UpdateManagersDto;
import in.com.rbc.quotation.admin.form.UpdateManagersForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class UpdateManagersAction extends BaseAction {
	
	public ActionForward updateManagers(ActionMapping actionMapping , ActionForm actionForm , HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = QuotationConstants.QUOTATION_UPDATEMGRS;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName,sMethodname,"START");
		
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	    */
			setRolesToRequest(request);
	    // Get Form objects from the actionForm
		UpdateManagersForm oUpdateManagersForm =(UpdateManagersForm)actionForm;
		if(oUpdateManagersForm ==  null) {
			oUpdateManagersForm = new UpdateManagersForm();
		}
		Connection conn = null;  // Connection object to store the database connection
	  	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	  	DaoFactory oDaoFactory =null; /* Creating an instance of of DaoFactory  */
	  	UpdateManagersDao oUpdateManagersDao =null;
	  	UpdateManagersDto oUpdateManagersDto =null;
		try {
			saveToken(request);// To avoid entering the duplicate value into database. 
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			
			oDaoFactory = new DaoFactory();
			oUpdateManagersDao = oDaoFactory.getUpdateManagersDao();
			conn = oDBUtility.getDBConnection();
			oUpdateManagersDto =  new UpdateManagersDto();
			/* Copying the FORM Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oUpdateManagersDto , oUpdateManagersForm);
			oUpdateManagersDto = oUpdateManagersDao.getManagerValues(conn,oUpdateManagersDto);
			/* Copying the DTO Object into FORM Object via Propertyutils*/
			PropertyUtils.copyProperties(oUpdateManagersForm , oUpdateManagersDto);
			oUpdateManagersForm.setManagertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
			oUpdateManagersForm.setRegionalSalesManagerList(oUpdateManagersDao.getReginolSalesManagerList(conn));
			oUpdateManagersForm.setSalesManagerList(oUpdateManagersDao.getSalesManagerList(conn));
			oUpdateManagersForm.setDesignEngineerList(oUpdateManagersDao.getDesignEngineerList(conn));
			
			
			
		}catch(Exception e) {
			 /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
        		LoggerUtility.log("DEBUG", this.getClass().getName(),sMethodname, "Replace managers Result."+ ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            	
            /* Forwarding request to Error page */
            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
			/* Releasing/closing the connection object */
         	oDBUtility.releaseResources(conn);  
		}
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	}
	
	public ActionForward saveValues(ActionMapping actionMapping , ActionForm actionForm , HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		//Method name Set for log file usage ;
		String sMethodname = QuotationConstants.QUOTATION_SAVE_VALUES;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName,sMethodname,"START");
		
		UpdateManagersForm oUpdateManagersForm =(UpdateManagersForm)actionForm;
		if(oUpdateManagersForm ==  null) {
			oUpdateManagersForm = new UpdateManagersForm();
		}
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		
		boolean isUpdated =false ;
		boolean isWorkFlowUpdate =false ;
		String sTypeofManager="";
		DaoFactory oDaoFactory =null;
		UpdateManagersDao oUpdateManagersDao =null;
		UpdateManagersDto oUpdateManagersDto =null;
		String sOperation =null;
		setRolesToRequest(request);
		try {
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oUpdateManagersDao = oDaoFactory.getUpdateManagersDao();
			oUpdateManagersDto =  new UpdateManagersDto();
			
			if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	resetToken(request);// Reset the Token Request ...
	         	conn = oDBUtility.getDBConnection();
	         	sOperation = oUpdateManagersForm.getOperation();
	         	LoggerUtility.log("INFO", sClassName,sMethodname,"Operation going to happend :="+sOperation);
				/* Copying the Form Object into DTO Object via Propertyutils*/
				PropertyUtils.copyProperties(oUpdateManagersDto , oUpdateManagersForm);
				oDBUtility.setAutoCommitFalse(conn);
				LoggerUtility.log("INFO", sClassName, sMethodname,"Type of Manager Updation "+oUpdateManagersDto.getOperation());
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE)) {
					isUpdated = oUpdateManagersDao.updateChiefExecutiveValues(conn, oUpdateManagersDto);
					sTypeofManager= QuotationConstants.QUOTATION_CE;
				}
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM)) {
					isUpdated = oUpdateManagersDao.updateDesignManagerValue(conn, oUpdateManagersDto);
					sTypeofManager=QuotationConstants.QUOTATION_DM;
				}
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM)) {
					isUpdated = oUpdateManagersDao.updateRegionalSalesManagerValue(conn, oUpdateManagersDto);
					sTypeofManager= QuotationConstants.QUOTATION_RSM;
				}
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM)) {
					isUpdated = oUpdateManagersDao.updateSalesManagerValue(conn, oUpdateManagersDto);
					sTypeofManager= QuotationConstants.QUOTATION_SM;
				}
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DE)) {
					isUpdated = QuotationConstants.QUOTATION_TRUE;
					sTypeofManager= QuotationConstants.QUOTATION_DE;
				}
				if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM)) {
					isUpdated = oUpdateManagersDao.updateCommercialManagerValue(conn, oUpdateManagersDto);
					sTypeofManager=QuotationConstants.QUOTATION_CM;
				}
				
				LoggerUtility.log("INFO", sClassName,sMethodname, "Updation result in Action class := " + isUpdated);
				
				
				if(isUpdated) {
					isWorkFlowUpdate = oUpdateManagersDao.replaceManagersWorkflow(conn, oUpdateManagersDto);
					LoggerUtility.log("INFO", sClassName,sMethodname, "Updation result in Action class - workflow := " + isWorkFlowUpdate);
					request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\" "+sTypeofManager+"\" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);
				} else {
                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_REQ_PROC);
                }
				if(isUpdated) {
					oDBUtility.commit(conn);
				}else {
					oDBUtility.rollback(conn);
				}
				oDBUtility.setAutoCommitTrue(conn);
				/*
		        * Depond upon the Success value , the page is re-directed with message 
		        */
		        if (isUpdated) {
		            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_REPMGR_URL);
		        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_MSG, QuotationConstants.QUOTATION_RET_REPMGRS);
		        } else {

                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_REPMGR_URL);
                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_MSG, QuotationConstants.QUOTATION_RET_REPMGRS);
		        }
			 } else {
				 LoggerUtility.log("INFO", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_REPMGR_EXIST);
	 			 request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	 	         return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			 }
		} catch(Exception e) {
			 /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
        		LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname,"Replace managers Result."+ ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            	
            /* Forwarding request to Error page */
            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
         	oDBUtility.releaseResources(conn);  
		}
		
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}

}
