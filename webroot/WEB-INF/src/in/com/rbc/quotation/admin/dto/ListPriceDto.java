package in.com.rbc.quotation.admin.dto;

import java.util.ArrayList;

import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class ListPriceDto {
	
	private String invoke;
	private int serial_id;
	private String manufacturing_location;
	private String product_group;
	private String product_line;
	private String standard;
	private String power_rating_kw;
	private String frame_type;
	private String frame_suffix;
	private int number_of_poles;
	private String mounting_type;
	private String tb_position;
	private String list_price;
	private String material_cost;
	private String loh;
	private String isDesignMCSpecified;
	private ArrayList searchResultsList = null;
	private String operation;
	private ArrayList maufacturingList = new ArrayList();
	private ArrayList productGroupList = new ArrayList();
	private ArrayList productLineList = new ArrayList();
	private ArrayList standardList = new ArrayList();
	private ArrayList powerratingkwList = new ArrayList();
	private ArrayList frametypelist = new ArrayList();
	private ArrayList numberofpoleslist = new ArrayList();
	private ArrayList mountingyypelist = new ArrayList();
	private ArrayList tbpositionlist = new ArrayList();
	private ArrayList framesuffixlist = new ArrayList();
	
	FormFile listPriceFile = null;
	
	private Double listPriceValue = null;
	
	public String getInvoke() {
		return CommonUtility.replaceNull(invoke);
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public int getSerial_id() {
		return serial_id;
	}
	public void setSerial_id(int serial_id) {
		this.serial_id = serial_id;
	}
	public String getManufacturing_location() {
		return CommonUtility.replaceNull(manufacturing_location);
	}
	public void setManufacturing_location(String manufacturing_location) {
		this.manufacturing_location = manufacturing_location;
	}
	public String getProduct_group() {
		return CommonUtility.replaceNull(product_group);
	}
	public void setProduct_group(String product_group) {
		this.product_group = product_group;
	}
	public String getProduct_line() {
		return CommonUtility.replaceNull(product_line);
	}
	public void setProduct_line(String product_line) {
		this.product_line = product_line;
	}
	public String getStandard() {
		return CommonUtility.replaceNull(standard);
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getPower_rating_kw() {
		return CommonUtility.replaceNull(power_rating_kw);
	}
	public void setPower_rating_kw(String power_rating_kw) {
		this.power_rating_kw = power_rating_kw;
	}
	public String getFrame_type() {
		return CommonUtility.replaceNull(frame_type);
	}
	public void setFrame_type(String frame_type) {
		this.frame_type = frame_type;
	}
	public String getFrame_suffix() {
		return CommonUtility.replaceNull(frame_suffix);
	}
	public void setFrame_suffix(String frame_suffix) {
		this.frame_suffix = frame_suffix;
	}
	public int getNumber_of_poles() {
		return number_of_poles;
	}
	public void setNumber_of_poles(int number_of_poles) {
		this.number_of_poles = number_of_poles;
	}
	public String getMounting_type() {
		return CommonUtility.replaceNull(mounting_type);
	}
	public void setMounting_type(String mounting_type) {
		this.mounting_type = mounting_type;
	}
	public String getTb_position() {
		return CommonUtility.replaceNull(tb_position);
	}
	public void setTb_position(String tb_position) {
		this.tb_position = tb_position;
	}
	public String getList_price() {
		return CommonUtility.replaceNull(list_price);
	}
	public void setList_price(String list_price) {
		this.list_price = list_price;
	}
	public String getMaterial_cost() {
		return CommonUtility.replaceNull(material_cost);
	}
	public void setMaterial_cost(String material_cost) {
		this.material_cost = material_cost;
	}
	public String getLoh() {
		return CommonUtility.replaceNull(loh);
	}
	public void setLoh(String loh) {
		this.loh = loh;
	}
	public ArrayList getSearchResultsList() {
		return searchResultsList;
	}
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public ArrayList getMaufacturingList() {
		return maufacturingList;
	}
	public void setMaufacturingList(ArrayList maufacturingList) {
		this.maufacturingList = maufacturingList;
	}
	public ArrayList getProductGroupList() {
		return productGroupList;
	}
	public void setProductGroupList(ArrayList productGroupList) {
		this.productGroupList = productGroupList;
	}
	public ArrayList getProductLineList() {
		return productLineList;
	}
	public void setProductLineList(ArrayList productLineList) {
		this.productLineList = productLineList;
	}
	public ArrayList getStandardList() {
		return standardList;
	}
	public void setStandardList(ArrayList standardList) {
		this.standardList = standardList;
	}
	public ArrayList getPowerratingkwList() {
		return powerratingkwList;
	}
	public void setPowerratingkwList(ArrayList powerratingkwList) {
		this.powerratingkwList = powerratingkwList;
	}
	public ArrayList getFrametypelist() {
		return frametypelist;
	}
	public void setFrametypelist(ArrayList frametypelist) {
		this.frametypelist = frametypelist;
	}
	public ArrayList getNumberofpoleslist() {
		return numberofpoleslist;
	}
	public void setNumberofpoleslist(ArrayList numberofpoleslist) {
		this.numberofpoleslist = numberofpoleslist;
	}
	public ArrayList getMountingyypelist() {
		return mountingyypelist;
	}
	public void setMountingyypelist(ArrayList mountingyypelist) {
		this.mountingyypelist = mountingyypelist;
	}
	public ArrayList getTbpositionlist() {
		return tbpositionlist;
	}
	public void setTbpositionlist(ArrayList tbpositionlist) {
		this.tbpositionlist = tbpositionlist;
	}
	public FormFile getListPriceFile() {
		return listPriceFile;
	}
	public void setListPriceFile(FormFile listPriceFile) {
		this.listPriceFile = listPriceFile;
	}
	public ArrayList getFramesuffixlist() {
		return framesuffixlist;
	}
	public void setFramesuffixlist(ArrayList framesuffixlist) {
		this.framesuffixlist = framesuffixlist;
	}
	public String getIsDesignMCSpecified() {
		return CommonUtility.replaceNull(isDesignMCSpecified);
	}
	public void setIsDesignMCSpecified(String isDesignMCSpecified) {
		this.isDesignMCSpecified = isDesignMCSpecified;
	}
	public Double getListPriceValue() {
		return listPriceValue;
	}
	public void setListPriceValue(Double listPriceValue) {
		this.listPriceValue = listPriceValue;
	}
	
}
