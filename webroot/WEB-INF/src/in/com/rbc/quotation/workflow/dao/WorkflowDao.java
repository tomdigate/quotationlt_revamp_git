/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: WorkflowDao.java
 * Package: in.com.rbc.quotation.workflow.dao
 * Desc: Interface that declares all the methods related to Workflow activities.
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.workflow.dao;

import in.com.rbc.quotation.workflow.dto.WorkflowDto;

import java.sql.Connection;
import java.util.ArrayList;

public interface WorkflowDao {
    /**
     * Method to retrieve the list of recent enquires for the logged-in user.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param userSSOId SSO Id of the logged-in user whose information has to be
     *                  retrieve from database.
     * @return Returns an ArrayList containing objects of WorkflowDto class, 
     *                  which holds the enquiry details created by the logged-in user.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 3, 2008
     */
    public ArrayList getMyRecentEnquires(Connection conn,
                                         String userSSOId) throws Exception;
    
    /**
     * Method to retrieve the list of enquires that are awaiting for approval 
     * by the logged-in user.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param userSSOId SSO Id of the logged-in user whose information has to be
     *                  retrieve from database.
     * @return Returns an ArrayList containing objects of WorkflowDto class, 
     *                  which holds the enquiry details created by the logged-in user.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 3, 2008
     */
    public ArrayList getMyAwaitingApprovalEnquires(Connection conn,
                                                   String userSSOId) throws Exception;
    
    /**
     * Method to retrieve the list of recent enquires that were created by the
     * logged-in user and released to customer.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param userSSOId SSO Id of the logged-in user whose information has to be
     *                  retrieve from database.
     * @return Returns an ArrayList containing objects of WorkflowDto class, 
     *                  which holds the enquiry details created by the logged-in user.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 3, 2008
     */
    public ArrayList getMyRecentReleasedEnquires(Connection conn,
                                                 String userSSOId) throws Exception;
    
    /**
     * Method to retrieve the Workflow Rule i.e., new status and next level code
     * to whom the enquiry has to be assigned to.  This information is retrieve 
     * based on the action code and current/old status of the enquiry that are 
     * passed to the method.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the details of current status 
     *                     and action code.
     * @return WorkflowDto object along with the new status and next level code.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public WorkflowDto getWorkflowRule(Connection conn,
                                       WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * Method to retrieve the next level assigned to user information based on 
     * the next level code and enquiry id.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the enquiry id and next level code.
     * @return WorkflowDto object along with the assigned to user information.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public WorkflowDto getNextLevelAssignTo(Connection conn,
                                            WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * Method to insert a workflow record in Workflow table.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operation.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public boolean insertWorkflowDetail(Connection conn,
                                        WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * Method to update and existing workflow record in Workflow table with the 
     * new status.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operation.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public boolean updateWorkflowDetail(Connection conn,
                                        WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * Method to update the main Enquiry master table with the current status.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operation.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public boolean updateEnquiryMasterWithStatus(Connection conn,
                                                 WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * This method is used for retrieving the list of approvers comments.
     * Approvers list that is displayed in CAP, II and FI areas of view request
     * will use this method.
     * 
     * @param conn
     *            Connection object to connect to database.
     * @param oWorkflowDto
     *            WorkflowDto object which has workflow input values.
     * @return alMembersList ArrayList collection of WorkFlowDto objects
     * @throws Exception
     *             If any error occurs during the process   
     * Created on: Oct 13, 2008
     */
    public ArrayList getApproversCommentsList(Connection conn, WorkflowDto oWorkflowDto) throws Exception;
    
    /**
     * Method to retrieve the Enquiry details that have to be displayed in the
     * e-mails that will be sent when different actions are performed.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the details of current status 
     *                     and action code.
     * @return WorkflowDto object along with the new status and next level code.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public WorkflowDto getEnquiryDetails(Connection conn,
                                         WorkflowDto oWorkflowDto) throws Exception;

	public boolean updateRatingIsValidated(Connection conn,
			WorkflowDto oWorkflowDto) throws Exception;
	
    /**
     * Method to delete the existing workflow record in Workflow table Based on WorkFlow Status
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operation.
     * @throws Exception In case when an error occurs during the process.
     * @author 900010540 (E.Gangadhara Rao.)
     * <br> Created on: Feb 4,2019
     */

	public boolean deletePrevStatusBasedonEnquiry(Connection conn,WorkflowDto oWorkflowDto) throws Exception;
}
