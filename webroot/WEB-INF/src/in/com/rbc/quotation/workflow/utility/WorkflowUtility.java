/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: WorkflowUtility.java
 * Package: in.com.rbc.quotation.workflow.utility
 * Desc: Utility class that contains all the methods related to Workflow engine
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 7, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.workflow.utility;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.SendMailUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dao.WorkflowDao;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;

public class WorkflowUtility {
    /**
     * Method to update workflow details in database depending on the action 
     * performed.
     * 
     * @param conn Connection object to retrieve information from database.
     * @param request HttpServletRequest object for performing certain operations.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operations.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    public int executeWorkflow(Connection conn, 
                                  HttpServletRequest request,
                                  WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "executeWorkflow";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "**************************** START - EXECUTE WORKFLOW ****************************");
        
        boolean canProceed = true;
        int iReturnInt = -1;
        
        /* Setting the application URL in WorkflowDto object */
        oWorkflowDto.setAppUrl("http://" + request.getHeader("host") + request.getContextPath());
        
        /* Checking if all the necessary inputs are provided or not. */
        canProceed = validateInputs(conn, oWorkflowDto);
        
        /* Proceeding only if all the necessary inputs are provided */
        if ( !canProceed ) {
            LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Invalid Parameters or Missed Parameter(s)");
            throw new Exception("Invalid Parameters or Missed Parameter(s)");
        } else {
            DaoFactory oDaoFactory = new DaoFactory();
            WorkflowDao oWorkflowDao = oDaoFactory.getWorkflowDao();
            
            oWorkflowDto = retrieveWorkflowDetails(conn, oWorkflowDao, oWorkflowDto);
            
            if ( oWorkflowDto==null ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Workflow Rule could not be retrieved due to invalid parameters");
                throw new Exception("Workflow Rule could not be retrieved due to invalid parameters");
                
            } else {
                if ( oWorkflowDto.getOldStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED
                        && oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION) ) {
                    
                    canProceed = initiateRevisionWorkflow(conn, oWorkflowDao, oWorkflowDto);
                } 
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                        && oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM) ) {
                    
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                        && oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM) ) {
                    
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                		&& oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                		&& oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOQA) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                		&& oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSCM) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                		&& oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMFG) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                        && oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE) ) {
                    
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                } 
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM
                        	&&	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCOMMERCIALMGR))
                        	&& 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_NA) ) {
                  
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED
                        &&	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST))
                        &&	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_LOST) ) {
                  
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED
                        &&	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_WON))
                        &&	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_WON) ) {
                  
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED
                        && 	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT))
                        && 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED) ) {
                  
                    canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                        && 	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT))
                        && 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT
                        && 	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST))
                        && 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_LOST) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_REGRET_PENDINGSM
                        && 	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT))
                        && 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else if ( (oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_REGRET_PENDINGSM
                        && 	oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST))
                        && 	oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_LOST) ) {
                	
                	canProceed = initiateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                else {
                	
                    canProceed = updateWorkflow(conn, oWorkflowDao, oWorkflowDto);
                }
                
				if (canProceed) {
					/* Retrieving Enquiry details to be sent in email */
					oWorkflowDto = oWorkflowDao.getEnquiryDetails(conn, oWorkflowDto);

					/* Send email */
					SendMailUtility oSendMailUtility = new SendMailUtility();

					boolean isMailSent = oSendMailUtility.sendWorkflowEmail(oWorkflowDto);
					if (isMailSent) {
						iReturnInt = 1;
					} else {
						iReturnInt = 0;
					}
				}
				
            }
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "**************************** END - EXECUTE WORKFLOW ****************************");
        return iReturnInt;
    }
    
    /**
     * Method to validate the inputs provided to perform the workflow updating.
     * 
     * @param conn Connection object for validating its existence.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @returnn Either true or false depending on the database operations.
     * @throws Exception In case when the validation fails.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    private boolean validateInputs(Connection conn, 
                                   WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "validateInputs";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean canProceed = true;
        
        /* 
         * Checking if the connection object is available or not.
         * Throwing a custom exception if Connection object is NULL.
         */
        if ( conn == null ) {
            canProceed = false;
            LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Connection is NULL");
            throw new Exception("Database Connection is NULL");
        }
        
        /* 
         * Checking if the application URL is available or not.
         * Throwing a custom exception if application URL is NULL.
         */
        if ( oWorkflowDto.getAppUrl() == null ) {
            canProceed = false;
            LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Application URL is NULL");
            throw new Exception("Application URL is NULL");
        }
        
        /*
         * Checking if all the necessary inputs are provided or not.
         */
        if ( oWorkflowDto.getEnquiryId()<=0 
                || oWorkflowDto.getLoginUserId()==null || oWorkflowDto.getLoginUserId().trim().length()<0
                || oWorkflowDto.getActionCode()==null || oWorkflowDto.getActionCode().trim().length()<0
                || oWorkflowDto.getOldStatusId()<=0 ) {
            canProceed = false;
            LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Invalid Parameters or Missed Parameter(s)");
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Logged-in User = " + oWorkflowDto.getLoginUserId());
            throw new Exception("Invalid Parameters or Missed Parameter(s)");
        } else {
            if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION)
                    && oWorkflowDto.getBaseEnquiryId()<=0 ) {
                canProceed = false;
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Invalid Parameters or Missed Parameter(s)");
                LoggerUtility.log("INFO", sClassName, sMethodName, "Base Enquiry Id = " + oWorkflowDto.getBaseEnquiryId());
                throw new Exception("Invalid Parameters or Missed Parameter(s)");
            }
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "Inputs for Workflow execution: ");
        LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
        LoggerUtility.log("INFO", sClassName, sMethodName, "Base Enquiry Id = " + oWorkflowDto.getBaseEnquiryId());
        LoggerUtility.log("INFO", sClassName, sMethodName, "Logged-in User Id = " + oWorkflowDto.getLoginUserId());
        LoggerUtility.log("INFO", sClassName, sMethodName, "Action Code = " + oWorkflowDto.getActionCode());
        LoggerUtility.log("INFO", sClassName, sMethodName, "Old Status Id = " + oWorkflowDto.getOldStatusId());
        LoggerUtility.log("INFO", sClassName, sMethodName, "Assigned To = " + oWorkflowDto.getAssignedTo());
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return canProceed;
    }
    
    /**
     * Method to retrieve necessary details for updating the workflow information
     * in database
     * 
     * @param conn Connection object to retrieve information from database.
     * @param oWorkflowDao WorkflowDao object to perform database operations.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return WorkflowDto object with all the necessary details retrieved from database.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    private WorkflowDto retrieveWorkflowDetails(Connection conn, 
                                                WorkflowDao oWorkflowDao,
                                                WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "retrieveWorkflowDetails";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean canProceed = true;
        WorkflowDto oOutputWorkflowDto = null;
        
        /* Retrieving the new status and next level code. */
        oWorkflowDto = oWorkflowDao.getWorkflowRule(conn, oWorkflowDto);
        
        if ( oWorkflowDto == null ) {
        	canProceed = false;
            LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Workflow Rule could not be retrieved due to invalid parameters");
            throw new Exception("Workflow Rule could not be retrieved due to invalid parameters");
        } else {
        	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Rule - Retrieved");
        }
        
        
        /*
         * Retrieving the Design Manager's details to assign the request i.e., 
         * Submitting to Design Office.
         */
        if ( oWorkflowDto.getAssignedTo().trim().length() > 0 ) {
            LoggerUtility.log("INFO", sClassName, sMethodName, "Assign To - Specified by the user");
            
            /* Instantiating the UserAction object */
            UserAction oUserAction = new UserAction();
            
            /* Retrieving the logged-in user information */
            UserDto oUserDto = oUserAction.getUserInfoForId(conn, oWorkflowDto.getAssignedTo());
            
            if ( oUserDto == null ) {
                canProceed = false;
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Assign To user details could not be retrieved");
                throw new Exception("Assign To user details could not be retrieved");
            } else {
                oWorkflowDto.setAssignedToName(oUserDto.getFullName());
                oWorkflowDto.setAssignedToEmail(oUserDto.getEmailId());
                LoggerUtility.log("INFO", sClassName, sMethodName, "Assign To user details - Retrieved");
            }
        } else {
            LoggerUtility.log("INFO", sClassName, sMethodName, "Assign To - According to Workflow");
            
            /* Fetching Next Level Assign Code */
            oWorkflowDto = oWorkflowDao.getNextLevelAssignTo(conn, oWorkflowDto);

            if ( oWorkflowDto == null ) {
                canProceed = false;
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Next Level Assign To user details could not be retrieved due to invalid parameters");
                throw new Exception("Next Level Assign To user details could not be retrieved due to invalid parameters");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Next Level Assign To user details - Retrieved");
            } 
        }
        
        if ( canProceed ) {
            oOutputWorkflowDto = new WorkflowDto();
            PropertyUtils.copyProperties(oOutputWorkflowDto, oWorkflowDto);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oOutputWorkflowDto;
    }
    
    /**
     * Method to perform database update operations for initiating workflow 
     * process when an enquiry is created and submitted to Design Office.
     * 
     * @param conn Connection object to perform database operations.
     * @param oWorkflowDao WorkflowDao object to perform database operations.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operations.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    private boolean initiateWorkflow(Connection conn,
                                     WorkflowDao oWorkflowDao,
                                     WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "initiateWorkflow";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean canProceed = true;
        
        if ( canProceed ) {
            WorkflowDto oTempWorkflowDto = new WorkflowDto();

            PropertyUtils.copyProperties(oTempWorkflowDto, oWorkflowDto);
            if(oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM)
            		|| oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM) ) {
            	
            	oTempWorkflowDto.setAssignedTo(oWorkflowDto.getAssignedTo());
            } else {
            	
            	oTempWorkflowDto.setAssignedTo(oWorkflowDto.getLoginUserId());
            }
            oTempWorkflowDto.setActionCode(oWorkflowDto.getActionCode());
            
            /* Inserting new status in Workflow table */
            canProceed = oWorkflowDao.insertWorkflowDetail(conn, oTempWorkflowDto);
            if ( !canProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - New Workflow record could not be inserted");
                throw new Exception("New Workflow record could not be inserted");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "New Workflow record - Inserted");
            }
        }
        
        if(!oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_LOST)
        		&& !oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_WON)
        		&& !oWorkflowDto.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED) ) {
		        
        	if ( canProceed ) {
		            WorkflowDto oTempWorkflowDto = new WorkflowDto();
		            PropertyUtils.copyProperties(oTempWorkflowDto, oWorkflowDto);
		            
		            oTempWorkflowDto.setAssignedTo(oWorkflowDto.getAssignedTo());
		            oTempWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
		            oTempWorkflowDto.setOldStatusId(oWorkflowDto.getNewStatusId());
		            oTempWorkflowDto.setNewStatusId(oWorkflowDto.getNewStatusId());
		            oTempWorkflowDto.setRemarks(null);
		            
		            /* Inserting new status in Workflow table */
		            canProceed = oWorkflowDao.insertWorkflowDetail(conn, oTempWorkflowDto);
		            if ( !canProceed ) {
		                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - New Workflow record could not be inserted");
		                throw new Exception("New Workflow record could not be inserted");
		            } else {
		                LoggerUtility.log("INFO", sClassName, sMethodName, "New Workflow record - Inserted");
		            }
        	}
        }
        
        if ( canProceed ) {
        	/* Update status in Enquiry master table */
        	canProceed = oWorkflowDao.updateEnquiryMasterWithStatus(conn, oWorkflowDto);
        	if ( !canProceed ) {
        		LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Enquiry Master could not be updated with new status");
        		throw new Exception("Enquiry Master could not be updated with new status");
        	} else {
        		LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Master - Udpated with new status - " + oWorkflowDto.getStatus());
        	}
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return canProceed;
    }
    
    /**
     * Method to perform database update operations when any action is performed
     * in the application which may include any type of action like submitting 
     * the enquiry, returning the enquiry, etc.
     * 
     * @param conn Connection object to perform database operations.
     * @param oWorkflowDao WorkflowDao object to perform database operations.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operations.
     * @throws Exception In case when an error occurs during the process.
     * @author 100002865 (Shanthi Chinta)
     * <br> Created on: Oct 8, 2008
     */
    private boolean updateWorkflow(Connection conn,
                                   WorkflowDao oWorkflowDao,
                                   WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "updateWorkflow";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean canProceed = true;
        
        if ( canProceed ) {
            
        	/*  Updating old status in Workflow table  */
            canProceed = oWorkflowDao.updateWorkflowDetail(conn, oWorkflowDto);
            
            if ( !canProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Previous Workflow record could not be updated");
                throw new Exception("Previous Workflow record could not be updated");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Previous Workflow record - Updated");
            }
        }
        
        if ( canProceed 
               && ( !oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RELEASETOCUSTOMER)
            		   &&  !oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_WON) )  ) {
        	
            WorkflowDto oTempWorkflowDto = new WorkflowDto();
            PropertyUtils.copyProperties(oTempWorkflowDto, oWorkflowDto);
            
            oTempWorkflowDto.setAssignedTo(oWorkflowDto.getAssignedTo());
            oTempWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
            oTempWorkflowDto.setOldStatusId(oWorkflowDto.getNewStatusId());
            oTempWorkflowDto.setNewStatusId(oWorkflowDto.getNewStatusId());
            oTempWorkflowDto.setRemarks(null);
            
            /*
             * Inserting new status in Workflow table
             */
            canProceed = oWorkflowDao.insertWorkflowDetail(conn, oTempWorkflowDto);
            if ( !canProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - New Workflow record could not be inserted");
                throw new Exception("New Workflow record could not be inserted");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "New Workflow record - Inserted");
            }
        }
        
        if ( canProceed ) {
            /*
             * Update status in Enquiry master table
             */
            canProceed = oWorkflowDao.updateEnquiryMasterWithStatus(conn, oWorkflowDto);
            if ( !canProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Enquiry Master could not be udpated with new status");
                throw new Exception("Enquiry Master could not be udpated with new status");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Master - Udpated with new status - " + oWorkflowDto.getStatus());
            }
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
        return canProceed;
    }
    
    
    private boolean initiateRevisionWorkflow(Connection conn, WorkflowDao oWorkflowDao, WorkflowDto oWorkflowDto) throws Exception {
        
        String sMethodName = "initiateRevisionWorkflow";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean canProceed = true;
        
        if ( canProceed ) {
            WorkflowDto oTempWorkflowDto = new WorkflowDto();
            oTempWorkflowDto.setLoginUserId(oWorkflowDto.getLoginUserId());
            oTempWorkflowDto.setLoginUserName(oWorkflowDto.getLoginUserName());
            oTempWorkflowDto.setLoginUserEmail(oWorkflowDto.getLoginUserEmail());
            
            // Inserting Superseded status in Workflow table.
            oTempWorkflowDto.setEnquiryId(oWorkflowDto.getBaseEnquiryId());
            oTempWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION);
            oTempWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);
            oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED);
            oTempWorkflowDto.setAssignedTo(oWorkflowDto.getLoginUserId());
            
            canProceed = oWorkflowDao.insertWorkflowDetail(conn, oTempWorkflowDto);
            if ( !canProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - New Workflow record could not be inserted");
                throw new Exception("New Workflow record could not be inserted");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "New Workflow record - Inserted");
            }
            
            // Inserting Pending Workflow Record - For New Revision Enquiry - Based on Remarks.
            if ( canProceed ) {
            	oTempWorkflowDto.setEnquiryId(oWorkflowDto.getEnquiryId());
            	oTempWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
            	if( QuotationConstants.QUOTATION_WORKFLOW_REVISION_TECHNICAL.equalsIgnoreCase(oWorkflowDto.getRevisionRemarks()) ) {
            		oTempWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
    	            oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            	}
            	else if( QuotationConstants.QUOTATION_WORKFLOW_REVISION_COMMERCIAL.equalsIgnoreCase(oWorkflowDto.getRevisionRemarks()) ) {
            		oTempWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
    	            oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
            	}
	            oTempWorkflowDto.setRemarks(oTempWorkflowDto.getRevisionRemarks());
	            
	            canProceed = oWorkflowDao.insertWorkflowDetail(conn, oTempWorkflowDto);
	            if ( !canProceed ) {
	                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - New Workflow record could not be inserted");
	                throw new Exception("New Workflow record could not be inserted");
	            } else {
	                LoggerUtility.log("INFO", sClassName, sMethodName, "New Workflow record - Inserted");
	            }
	            
	            /* Update status of New Revision Enquiry in Enquiry master table - Based on Remarks. */
	            if ( canProceed ) {
	            	oTempWorkflowDto.setEnquiryId(oWorkflowDto.getEnquiryId());
	            	if( QuotationConstants.QUOTATION_WORKFLOW_REVISION_TECHNICAL.equalsIgnoreCase(oWorkflowDto.getRevisionRemarks()) ) {
	    	            oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
	            	}
	            	else if( QuotationConstants.QUOTATION_WORKFLOW_REVISION_COMMERCIAL.equalsIgnoreCase(oWorkflowDto.getRevisionRemarks()) ) {
	    	            oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
	            	}
		            oTempWorkflowDto.setRevisionRemarks(oTempWorkflowDto.getRevisionRemarks());
		            
	            	canProceed = oWorkflowDao.updateEnquiryMasterWithStatus(conn, oTempWorkflowDto);
	            	if ( !canProceed ) {
		                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Enquiry Master could not be updated with new status");
		                throw new Exception("Enquiry Master could not be updated with new status");
		            } else {
		                LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Master - Udpated with new status - " + oTempWorkflowDto.getStatus());
		            }
	            	
	                /* Update status of Base Enquiry in Enquiry master table */
	            	oTempWorkflowDto.setEnquiryId(oWorkflowDto.getBaseEnquiryId());
	            	oTempWorkflowDto.setNewStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED);
	                oTempWorkflowDto.setRevisionRemarks(oWorkflowDto.getRevisionRemarks());
	                
	                canProceed = oWorkflowDao.updateEnquiryMasterWithStatus(conn, oTempWorkflowDto);
	                if ( !canProceed ) {
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Enquiry Master could not be updated with new status");
	                    throw new Exception("Enquiry Master could not be updated with new status");
	                } else {
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Master - Udpated with new status - " + oTempWorkflowDto.getStatus());
	                }
	            }
            }
            
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return canProceed;
    }
    
    /**
     * This method is used to Fetch Previous Record Columns MCUNIT Value From RFQ_RATING_DETAILS
     * 
     * @param Connection	It Will Hold Connection Object To Get Connection From DataBase    
     * @param WorkflowDao	WorkflowDao Containts Methods Where we Perform DB Operations
     * @param WorkflowDto	WorkflowDto get the dto Values Copied From Form Object        
     * @return 				returns boolean Either True or false
     * @throws Exception	if any error occurs while performing database operations, etc
     * @author 900010540 	(Gangadhara Rao) on 3O January 2019
     */

	public boolean updateRatingIsValidated(Connection conn,WorkflowDao oWorkFlowDao, WorkflowDto oWorkflowDto) throws Exception {
		
        String sMethodName = "updateRatingIsValidated";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean isCanProceed = true;
        
        if ( isCanProceed ) {
            /*
             * Updating old status in Workflow table
             */
        	isCanProceed = oWorkFlowDao.updateRatingIsValidated(conn, oWorkflowDto);
            if ( !isCanProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION -Is Validated Column In RFQ_RATING_DETAILS   could not be updated");

					throw new Exception("Is Validated Column could not be updated");
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Is Validated Column In RFQ_RATING_DETAILS Table is - Updated");
            }
        }

        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isCanProceed;
	}
	
    /**
     * Method to perform database delete operations when any action is performed
     * in the application which may include any type of action like submitting 
     * the enquiry, returning the enquiry, etc
     * @param conn Connection object to perform database operations.
     * @param oWorkflowDto WorkflowDto object holding the necessary inputs.
     * @return Either true or false depending on the database operations.
     * @author 900010540 (E.Gangadhar)
     * <br> Created on: 04 Febraury 2019
     */
	public boolean deletePrevStatusBasedonEnquiry(Connection conn,WorkflowDto oWorkflowDto) throws Exception {
		
        String sMethodName = "deletePrevStatusBasedonEnquiry";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean isCanProceed = true;
        DaoFactory oDaoFactory = new DaoFactory();
        WorkflowDao oWorkflowDao = oDaoFactory.getWorkflowDao();

        
        if ( isCanProceed ) {
            /*
             * Delete old status in Workflow table
             */
        	isCanProceed = oWorkflowDao.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
            if ( !isCanProceed ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "EXCEPTION - Unable to delete a Row From RFQ_WORKFLOW Based On QWF_WORKFLOWSTATUS "+oWorkflowDto.getActionCode()+" Does not Exists, deletion failed");

					
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "A row from RFQ_WORKFLOW based on QWF_WORKFLOWSTATUS "+oWorkflowDto.getActionCode()+"is deleted Sucessfully");
            }
        }

        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isCanProceed;

	}

}
