/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: WorkflowDaoImpl.java
 * Package: in.com.rbc.quotation.workflow.dao
 * Desc: DAO Implementation class that implemented all the methods related to
 *       Workflow activities.
 *       This class implements the interfaces, WorkflowDao & WorkflowQueries
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.workflow.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

public class WorkflowDaoImpl implements WorkflowDao, WorkflowQueries {
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getMyRecentEnquires()
     */
    public ArrayList getMyRecentEnquires(Connection conn,
                                         String userSSOId) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getMyRecentEnquires";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlEnquiresList = null;
        String sAssingTo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "My Recent Enquires - Query ::: " + WorkflowQueries.FETCH_MYRECENTENQUIRES);
            LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO Id = " + userSSOId);
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_MYRECENTENQUIRES);
           
            pstmt.setString(1, userSSOId);
            pstmt.setString(2, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
            
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                arlEnquiresList = new ArrayList();
                do {
                    WorkflowDto oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(rs.getLong("QEM_ENQUIRYID"));
                    oWorkflowDto.setEnquiryNo(rs.getString("QEM_ENQUIRYNO"));
                    oWorkflowDto.setStatusId(rs.getInt("QEM_STATUSID"));
                    oWorkflowDto.setStatus(rs.getString("QEM_STATUSDESC"));
                    oWorkflowDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
                    oWorkflowDto.setCreatedByName(rs.getString("QEM_CREATEDBYNAME"));
                    oWorkflowDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
                    oWorkflowDto.setProjName(rs.getString("QEM_PROJECTNAME"));
                    if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
                    	oWorkflowDto.setIsNewEnquiry("N");
                    } else {
                    	oWorkflowDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
                    }
                    
                    sAssingTo=rs.getString("QEM_ASSIGNTO")==""?"--":rs.getString("QEM_ASSIGNTO");
                    oWorkflowDto.setAssignedTo(sAssingTo);
                    arlEnquiresList.add(oWorkflowDto);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlEnquiresList;
    }

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getMyAwaitingApprovalEnquires(java.sql.Connection, java.lang.String)
     */
    public ArrayList getMyAwaitingApprovalEnquires(Connection conn, 
                                                   String userSSOId) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getMyAwaitingApprovalEnquires";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlEnquiresList = null;
        String sAssingTo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "My Awaiting Approval Enquiries - Query ::: " + WorkflowQueries.FETCH_MYAWAITINGAPPROVALENQUIRES);
            LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO Id = " + userSSOId);
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_MYAWAITINGAPPROVALENQUIRES);
            pstmt.setString(1, userSSOId);
            pstmt.setString(2, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
            WorkflowDto oWorkflowDto =null;
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                arlEnquiresList = new ArrayList();
                do {
                   oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(rs.getLong("QEM_ENQUIRYID"));
                    oWorkflowDto.setEnquiryNo(rs.getString("QEM_ENQUIRYNO"));
                    oWorkflowDto.setStatusId(rs.getInt("QEM_STATUSID"));
                    oWorkflowDto.setStatus(rs.getString("QEM_STATUSDESC"));
                    oWorkflowDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
                    oWorkflowDto.setCreatedByName(rs.getString("QEM_CREATEDBYNAME"));
                    oWorkflowDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
                    oWorkflowDto.setProjName(rs.getString("QEM_PROJECTNAME"));
                    sAssingTo=rs.getString("QEM_ASSIGNTO")==""?"--":rs.getString("QEM_ASSIGNTO");
                    oWorkflowDto.setAssignedTo(sAssingTo);
                    if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
                    	oWorkflowDto.setIsNewEnquiry("N");
                    } else {
                    	oWorkflowDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
                    }
                    
                    arlEnquiresList.add(oWorkflowDto);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlEnquiresList;
    }

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getMyRecentReleasedEnquires(java.sql.Connection, java.lang.String)
     */
    public ArrayList getMyRecentReleasedEnquires(Connection conn, 
                                                 String userSSOId) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getMyRecentReleasedEnquires";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlEnquiresList = null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "My Recent Released Enquires - Query ::: " + WorkflowQueries.FETCH_MYRECENTRELEASEDENQUIRES);
            LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO Id = " + userSSOId);
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_MYRECENTRELEASEDENQUIRES);
            pstmt.setString(1, userSSOId);
            pstmt.setString(2, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                arlEnquiresList = new ArrayList();
                do {
                    WorkflowDto oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(rs.getLong("QEM_ENQUIRYID"));
                    oWorkflowDto.setEnquiryNo(rs.getString("QEM_ENQUIRYNO"));
                    oWorkflowDto.setStatusId(rs.getInt("QEM_STATUSID"));
                    oWorkflowDto.setStatus(rs.getString("QEM_STATUSDESC"));
                    oWorkflowDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
                    oWorkflowDto.setCreatedByName(rs.getString("QEM_CREATEDBYNAME"));
                    oWorkflowDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
                    oWorkflowDto.setProjName(rs.getString("QEM_PROJECTNAME"));
                    if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
                    	oWorkflowDto.setIsNewEnquiry("N");
                    } else {
                    	oWorkflowDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
                    }
                    
                    arlEnquiresList.add(oWorkflowDto);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlEnquiresList;
    }

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getWorkflowRule(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public WorkflowDto getWorkflowRule(Connection conn, 
                                       WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getWorkflowRule";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        WorkflowDto oOutputWorkflowDto = null;
        
        try {
            /*
             * Querying the database to retrieve the workflow rule i.e., the 
             * new status that has to be set to the enquiry and next level code 
             * identifying to whom the enquiry has to be assigned.
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Rule - Query ::: " + WorkflowQueries.FETCH_WORKFLOWRULE);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Action Code = " + oWorkflowDto.getActionCode());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Old Status Id = " + oWorkflowDto.getOldStatusId());
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_WORKFLOWRULE);
            
            //Fetch WorkFlow
            pstmt.setString(1, oWorkflowDto.getActionCode());
            pstmt.setInt(2, oWorkflowDto.getOldStatusId());
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                oOutputWorkflowDto = new WorkflowDto();
                
                oWorkflowDto.setActionCode(rs.getString("QRL_ACTIONCODE"));
                oWorkflowDto.setActionDesc(rs.getString("QRL_ACTIONCODEDESC"));
                
                if(oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM)
                		&&	oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE ) 
                { 	
                	//TODO Bi-pass the Submission Flow From DM TO RSM
                	oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM);
                	oWorkflowDto.setOldStatus(QuotationConstants.QUOTATION_PENDING_DESIGNOFFICE);
                }
                else if(oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODM)
                		&&  oWorkflowDto.getOldStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE) 
                { 	
                	//TODO Bi-pass the Return Flow RSM TO DM
                	oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
                	oWorkflowDto.setOldStatus(QuotationConstants.QUOTATION_PENDING_RSM);
                }
                else
                {
                	//TODO Bipass the Flow From DM TO CE
                 	oWorkflowDto.setOldStatusId(rs.getInt("QRL_CURRSTATUSID"));
                	oWorkflowDto.setOldStatus(rs.getString("QRL_CURRSTATUSDESC"));
                }
                
                oWorkflowDto.setNewStatusId(rs.getInt("QRL_NEWSTATUSID"));
                oWorkflowDto.setNewStatus(rs.getString("QRL_NEWSTATUSDESC"));
                oWorkflowDto.setNextLevelCode(rs.getString("QRL_NEXTLEVELCODE"));
                oWorkflowDto.setNextLevelDesc(rs.getString("QRL_NEXTLEVELDESC"));
                oWorkflowDto.setEmailFlag(rs.getString("QRL_EMAILFLAG"));
                
                
                PropertyUtils.copyProperties(oOutputWorkflowDto, oWorkflowDto);
            }
            
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oOutputWorkflowDto;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getNextLevelAssignTo(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public WorkflowDto getNextLevelAssignTo(Connection conn, 
                                            WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getWorkflowAssignTo";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        WorkflowDto oOutputWorkflowDto = null;
        
        try {
            /*
             * Querying the database to retrieve the next level user details to
             * whom the enquiry has to be assigned.
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow - Next Level Assign To - Query ::: " + WorkflowQueries.FETCH_NEXTLEVELASSIGNTO);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_NEXTLEVELASSIGNTO);
            pstmt.setLong(1, oWorkflowDto.getEnquiryId());
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                oOutputWorkflowDto = new WorkflowDto();
                
                /*
                 * Case when next level assigned to user is Sales Manager
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_SALESMANAGER"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_SALESMANAGERNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_SALESMANAGEREMAIL"));
                }
                /*
                 * Case when next level assigned to user is Commercial Managers
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_COMMERCIALMANAGER"));
                      oWorkflowDto.setAssignedToName(rs.getString("AST_COMMERCIALMANAGERNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_COMMERCIALMANAGEREMAIL"));
                }

                /*
                 * Case when next level assigned to user is Design Manager
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_DESIGNMANAGER"));
                    
                    oWorkflowDto.setAssignedToName(rs.getString("AST_DESIGNMANAGERNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_DESIGNMANAGEREMAIL"));
                }
                /*
                 * Case when next level assigned to user is Design Engineer
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DE) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_DESIGNENGINEER"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_DESIGNENGINEERNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_DESIGNENGINEEREMAIL"));
                }
                /*
                 * Case when next level assigned to user is Chief Executive
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_CHIEFEXECUTIVE"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_CHIEFEXECUTIVENAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_CHIEFEXECUTIVEEMAIL"));
                }
                
                
                /*
                 * Case when next level assigned to user is Regional Sales Manager
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_PRIMARYRSM"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_PRIMARYRSMNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_PRIMARYRSMEMAIL"));
                    oWorkflowDto.setCcEmailTo(rs.getString("AST_ADDITIONALRSMEMAIL"));
                }
                /*
                 * Case when next level assigned to user is Customer
                 */
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_END) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_CUSTOMERNAME"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_CONTACTPERSON"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_CONTACTPERSONEMAIL"));
                }
                
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_NSM) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_NSM"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_NSMNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_NSMEMAIL"));
                }
                
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MH) ) {
                    oWorkflowDto.setAssignedTo(rs.getString("AST_MH"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_MHNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_MHEMAIL"));
                }
                
                if ( oWorkflowDto.getNextLevelCode()
                        .equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_BH) ) {
                	oWorkflowDto.setAssignedTo(rs.getString("AST_BH"));
                    oWorkflowDto.setAssignedToName(rs.getString("AST_BHNAME"));
                    oWorkflowDto.setAssignedToEmail(rs.getString("AST_BHEMAIL"));
                }
                
                PropertyUtils.copyProperties(oOutputWorkflowDto, oWorkflowDto);
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oOutputWorkflowDto;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#insertWorkflowDetail(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public boolean insertWorkflowDetail(Connection conn, 
                                        WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "insertWorkflowDetail";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null, pstmt1 = null;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        boolean hasPendingRecord = false;
        boolean isDatabaseUpdated = false;
        int iRowCount = 0;
        
        try {
            /* Workflow details. */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to insert Workflow details --> " + WorkflowQueries.INSERT_WORKFLOWDETAIL);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Assigned To = " + oWorkflowDto.getAssignedTo());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Action Code = " + oWorkflowDto.getActionCode());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Old Status Id = " + oWorkflowDto.getOldStatusId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
            
            if(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING.equals(oWorkflowDto.getActionCode())) {
            	// Fetch - If any "P"(Pending) Record - Based on EnquiryId.
            	// If Present - Update Existing Record.
            	// Else Insert New Record.
            	String sCheckQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW WHERE QWF_ENQUIRYID = ? AND QWF_WORKFLOWSTATUS = 'P' ";
            	pstmt1 = conn.prepareStatement(sCheckQuery);
            	pstmt1.setLong(1, oWorkflowDto.getEnquiryId());
            	rs1 = pstmt1.executeQuery();
            	if(rs1.next()) {
            		pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_PENDING_WORKFLOWDETAIL);
            		pstmt.setString(1, oWorkflowDto.getAssignedTo());
                    pstmt.setString(2, oWorkflowDto.getActionCode());
                    pstmt.setInt(3, oWorkflowDto.getOldStatusId());
                    pstmt.setInt(4, oWorkflowDto.getNewStatusId());
                    pstmt.setLong(5, oWorkflowDto.getEnquiryId());
                    
                    iRowCount = pstmt.executeUpdate();
                    hasPendingRecord = true;
            	} 
            } 
            oDBUtility.releaseResources(pstmt);
            
            if(!hasPendingRecord) {
            	// For All cases where ActionCode != "P".
            	pstmt = conn.prepareStatement(WorkflowQueries.INSERT_WORKFLOWDETAIL);
                pstmt.setLong(1, oWorkflowDto.getEnquiryId());
                pstmt.setString (2, oWorkflowDto.getAssignedTo());
                pstmt.setString (3, oWorkflowDto.getActionCode());
                pstmt.setInt(4, oWorkflowDto.getOldStatusId());
                pstmt.setInt(5, oWorkflowDto.getNewStatusId());
                pstmt.setString (6, oWorkflowDto.getRemarks());
                
                iRowCount = pstmt.executeUpdate();
            }
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "Inserted Row Count = " + iRowCount);
            if ( iRowCount > 0 )
                isDatabaseUpdated = true;
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
            oDBUtility.releaseResources(rs1, pstmt1);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDatabaseUpdated;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#updateWorkflowDetail(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public boolean updateWorkflowDetail(Connection conn, 
                                        WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "updateWorkflowDetail";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean isDatabaseUpdated = false;
        int iRowCount=0;
        
        try {
            /*
             * Updating the workflow details in the database with the new status
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to update Workflow details --> " + WorkflowQueries.UPDATE_WORKFLOWDETAIL);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Action Code = " + oWorkflowDto.getActionCode());
            LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Remarks = " + oWorkflowDto.getRemarks());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Login User Id = " + oWorkflowDto.getLoginUserId());
            LoggerUtility.log("INFO", sClassName, sMethodName, "Old Status Id = " + oWorkflowDto.getOldStatusId());
            
            pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_WORKFLOWDETAIL);
            
            pstmt.setString (1, oWorkflowDto.getActionCode());
            pstmt.setInt(2, oWorkflowDto.getNewStatusId());
            pstmt.setString (3, oWorkflowDto.getRemarks());
            pstmt.setLong(4, oWorkflowDto.getEnquiryId());
            pstmt.setString (5, oWorkflowDto.getLoginUserId());
            pstmt.setInt(6, oWorkflowDto.getOldStatusId());
            
            iRowCount = pstmt.executeUpdate();
            LoggerUtility.log("INFO", sClassName, sMethodName, "Updated Row Count = " + iRowCount);
            if ( iRowCount > 0 )
                isDatabaseUpdated = true;
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDatabaseUpdated;
    }

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#updateEnquiryMasterWithStatus(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public boolean updateEnquiryMasterWithStatus(Connection conn,WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "updateEnquiryMasterWithStatus";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean isDatabaseUpdated = false;
        int iRowCount =0;
        
        try {
            /*
             * Updating the Enquiry master table with the current/new status
             */
            if ( oWorkflowDto.getNewStatusId()==QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Query to update Enquiry master --> " + WorkflowQueries.UPDATE_ENQUIRYMASTERWITHSTATUS_REVISION);
                LoggerUtility.log("INFO", sClassName, sMethodName, "Base Enquiry Id = " + oWorkflowDto.getBaseEnquiryId());
                LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
                
                pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_ENQUIRYMASTERWITHSTATUS_REVISION);
                pstmt.setInt(1, oWorkflowDto.getNewStatusId());
                pstmt.setString(2, oWorkflowDto.getRevisionRemarks());
                pstmt.setLong(3, oWorkflowDto.getEnquiryId());
                
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, "Query to update Enquiry master --> " + WorkflowQueries.UPDATE_ENQUIRYMASTERWITHSTATUS);

                if(oWorkflowDto.getPageChecking().equalsIgnoreCase("dm")) {
	                pstmt = conn.prepareStatement(WorkflowQueries.DM_UPDATE_ENQUIRYMASTERWITHSTATUS);
	                pstmt.setInt(1,oWorkflowDto.getNewStatusId());
	                pstmt.setString(2,oWorkflowDto.getDmSsoid());
	                pstmt.setString(3,oWorkflowDto.getDmName());
	                pstmt.setString(4,oWorkflowDto.getDmNote());
	                pstmt.setString(5,oWorkflowDto.getDmToSmReturn());
	                pstmt.setLong(6,oWorkflowDto.getEnquiryId());
	                
	                LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
	                LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else  if(oWorkflowDto.getPageChecking().equalsIgnoreCase("de")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.DE_UPDATE_ENQUIRYMASTERWITHSTATUS);
					pstmt.setInt(1, oWorkflowDto.getNewStatusId());
					pstmt.setString(2, oWorkflowDto.getDmSsoid());
					pstmt.setString(3, oWorkflowDto.getDmName());
					pstmt.setString(4, oWorkflowDto.getDmNote());
					pstmt.setString(5, oWorkflowDto.getDeToSmReturn());
					pstmt.setLong(6, oWorkflowDto.getEnquiryId());
					
					LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("CE")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.CE_UPDATE_ENQUIRYMASTERWITHSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getCeSsoid());
            	   	pstmt.setString(3, oWorkflowDto.getCeName());
            	   	pstmt.setString(4, oWorkflowDto.getCeNote());
            	   	pstmt.setLong(5, oWorkflowDto.getEnquiryId());
            	   	
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("RSM")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.RSM_UPDATE_ENQUIRYMASTERWITHSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getRsmSsoid());
            	   	pstmt.setString(3, oWorkflowDto.getRsmName());
            	   	pstmt.setString(4, oWorkflowDto.getRsmNote());
            	   	pstmt.setLong(5, oWorkflowDto.getEnquiryId());
            	   
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("QUOTATION_CE")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.QUOTATION_CE_UPDATE_ENQUIRYMASTERWITHSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getRemarks());
            	   	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
            	   
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("QUOTATION_DM")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.QUOTATION_DM_UPDATE_ENQUIRYMASTERWITHSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getRemarks());
            	   	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
            	   	
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("QUOTATION_RSM")) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.QUOTATION_RSM_UPDATE_ENQUIRYMASTERWITHSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getRemarks());
            	   	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
            	   	
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase(QuotationConstants.QUOATION_PAGECHECKING_CMWON)) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONCMRETURN_ENQUIRYMASTERSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getCmToSmReturn());
            	   	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
            	   
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
               }
               else if(oWorkflowDto.getPageChecking().equalsIgnoreCase(QuotationConstants.QUOTATION_CEWON)) {
            	   	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONCERETURN_ENQUIRYMASTERSTATUS);
            	   	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
            	   	pstmt.setString(2, oWorkflowDto.getCommengToSmReturn());
            	   	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
            	   	
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            	   	LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                }
                else if(oWorkflowDto.getPageChecking().equalsIgnoreCase("cm")) {
                	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATION_ENQUIRYMASTERNOTESTATUS);
                	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
                	pstmt.setString(2, oWorkflowDto.getCmNote());
                	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
                	
                	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
                    LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                }
                else if(oWorkflowDto.getPageChecking().equalsIgnoreCase(QuotationConstants.QUOTATION_DMWON)) {
                	if(oWorkflowDto.getsReturnComment() != null 
                			&& oWorkflowDto.getsReturnComment().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM)) {
	                	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONDMRETURN_ENQUIRYMASTERSTATUS);
	                	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
	                	pstmt.setString(2, oWorkflowDto.getDmToCmReturn());
	                	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
	                	
	                	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                	}
                	else {
	                	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONDMRETURNTOSM);
	                	pstmt.setInt(1, oWorkflowDto.getNewStatusId());
	                	pstmt.setString(2, oWorkflowDto.getDmToCmReturn());
	                	pstmt.setLong(3, oWorkflowDto.getEnquiryId());
	                	
	                	LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                	}
                }
                else if(oWorkflowDto.getPageChecking().equalsIgnoreCase(QuotationConstants.QUOTATION_DEWON))  {
                	if(oWorkflowDto.getsReturnComment() != null 
                			&& oWorkflowDto.getsReturnComment().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM)) {
                		pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONDERETURN_ENQUIRYMASTERSTATUS);
                		pstmt.setInt(1, oWorkflowDto.getNewStatusId());
                		pstmt.setString(2, oWorkflowDto.getDeToCmReturn());
                		pstmt.setLong(3, oWorkflowDto.getEnquiryId());
                		
                		LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
                        LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                	}
                	else {
                		pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_QUOTATIONDERETURNTOSM);
                		pstmt.setInt(1, oWorkflowDto.getNewStatusId());
                		pstmt.setString(2, oWorkflowDto.getDeToCmReturn());
                		pstmt.setLong(3, oWorkflowDto.getEnquiryId());
                		
                		LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
                        LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                	}
                }
                else {
                	pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_ENQUIRYMASTERWITHSTATUS);
                    pstmt.setInt(1, oWorkflowDto.getNewStatusId());
                    pstmt.setLong(2, oWorkflowDto.getEnquiryId());
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
                    LoggerUtility.log("INFO", sClassName, sMethodName, "New Status Id = " + oWorkflowDto.getNewStatusId());
                }
                
            }
            
            iRowCount = pstmt.executeUpdate();
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "Updated Row Count = " + iRowCount);
            
            if ( iRowCount > 0 )
                isDatabaseUpdated = true;
            
        } 
        catch(Exception e) {
            LoggerUtility.log("DEBUG", "WorkflowDaoImpl", "updateEnquiryMasterWithStatus", ExceptionUtility.getStackTraceAsString(e));
        }
        finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDatabaseUpdated;
    }
    
    public ArrayList getApproversCommentsList(Connection conn, WorkflowDto oWorkflowDto) throws Exception {

        // PreparedStatement object to handle database operations.
        PreparedStatement pstmt = null;

        // ResultSet object to store the rows retrieved from database.
        ResultSet rs = null;

        // Object of DBUtility class to handle database resource operations.
        DBUtility oDBUtility = new DBUtility();

        ArrayList alMembersList = null;
       
        try {
            pstmt = conn
                    .prepareStatement(WorkflowQueries.GET_APPROVERSINFO_BY_ENQUIRYID);
            pstmt.setLong(1, oWorkflowDto.getEnquiryId());

            // Executing the prepared Statement and assigning the results to
            // result set.
            rs = pstmt.executeQuery();

            if ( rs.next() ) {
                alMembersList = new ArrayList();
                do {
                    WorkflowDto oTempWorkflowDto = new WorkflowDto();
                    oTempWorkflowDto.setAssignedToName(rs.getString("EMP_NAMEFULL"));
                    oTempWorkflowDto.setLastModifiedDate(rs
                            .getString("QWF_LASTMODIFIEDDATE"));
                    oTempWorkflowDto.setRemarks(rs.getString("QWF_REMARKS"));                   
                    alMembersList.add(oTempWorkflowDto);
                } while ( rs.next() );
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return alMembersList;
    }

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.workflow.dao.WorkflowDao#getEnquiryDetails(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
    public WorkflowDto getEnquiryDetails(Connection conn, WorkflowDto oWorkflowDto) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getEnquiryDetails";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        WorkflowDto oOutputWorkflowDto = null;
        EnquiryDto oEnquiryDto = null;
        
        try {
            /*
             * Querying the database to retrieve the workflow rule i.e., the 
             * new status that has to be set to the enquiry and next level code 
             * identifying to whom the enquiry has to be assigned.
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Fetching Enquiry Details - Query ::: " + WorkflowQueries.FETCH_ENQUIRYDETAILS);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            pstmt = conn.prepareStatement(WorkflowQueries.FETCH_ENQUIRYDETAILS);
            pstmt.setLong(1, oWorkflowDto.getEnquiryId());
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                oOutputWorkflowDto = new WorkflowDto();
                oEnquiryDto = new EnquiryDto();
                
                oEnquiryDto.setEnquiryId(rs.getInt("QEM_ENQUIRYID"));
                oEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));
                oEnquiryDto.setCustomerId(rs.getInt("QEM_CUSTOMERID"));
                oEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
                oEnquiryDto.setLocationId(rs.getInt("QEM_LOCATIONID"));
                oEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME"));
                oEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPEDESC"));
                oEnquiryDto.setConsultantName(rs.getString("QEM_CONSULTANTNAME"));
                oEnquiryDto.setCrossReference(rs.getString("QEM_CROSSREFERENCE"));
                oEnquiryDto.setDescription(rs.getString("QEM_DESC"));
                oEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));
                oEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC"));
                oEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY"));
                oEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME"));
                oEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
                oEnquiryDto.setEndUser(rs.getString("QEM_ENDUSER"));
                oWorkflowDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
                oWorkflowDto.setDesignReference(rs.getString("QEM_ENQUIRYREFERENCENO"));
                
                oWorkflowDto.setEnquiryDto(oEnquiryDto);
                
                PropertyUtils.copyProperties(oOutputWorkflowDto, oWorkflowDto);
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oOutputWorkflowDto;
    }

	@Override
	public boolean updateRatingIsValidated(Connection conn,
			WorkflowDto oWorkflowDto)throws Exception {
        String sClassName = this.getClass().getName();
        String sMethodName = "updateRatingIsValidated";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean isDatabaseUpdated = false;
        int iRowCount =0;
        
        try {
            /*
             * Updating the workflow details in the database with the new status
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId());
            pstmt = conn.prepareStatement(WorkflowQueries.UPDATE_IS_VALIDATED_RFQ_RATING_DETAILS);
            pstmt.setString (1, "N");
            pstmt.setLong(2, oWorkflowDto.getEnquiryId());
            
            iRowCount = pstmt.executeUpdate();
            LoggerUtility.log("INFO", sClassName, sMethodName, "Updated Row Count = " + iRowCount);
            if ( iRowCount > 0 )
                isDatabaseUpdated = true;
        } finally {
        	
            oDBUtility.releaseResources(pstmt);
        }
        
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDatabaseUpdated;

	}
    /* (non-Javadoc)
     * @see boolean#deletePrevStatusBasedonEnquiry(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
     */
	@Override
	public boolean deletePrevStatusBasedonEnquiry(Connection conn,
			WorkflowDto oWorkflowDto) throws Exception {
		// TODO Auto-generated method stub
        String sClassName = this.getClass().getName();
        String sMethodName = "deletePrevStatusBasedonEnquiry";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean isDataDeleted = false;
        int iRowCount =0;
        
        try {
            /*
             * Updating the workflow details in the database with the new status
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Enquiry Id = " + oWorkflowDto.getEnquiryId()+" Action Code = "+oWorkflowDto.getActionCode());
            pstmt = conn.prepareStatement(WorkflowQueries.DELETE_WORKFLOW_BASEDPREVSTATUS);
            pstmt.setLong(1, oWorkflowDto.getEnquiryId());
            pstmt.setString (2, oWorkflowDto.getActionCode());
            
            iRowCount = pstmt.executeUpdate();
            LoggerUtility.log("INFO", sClassName, sMethodName, "Deleted Row Count = " + iRowCount);
            if ( iRowCount > 0 )
            	isDataDeleted = true;
        } finally {
        	
            oDBUtility.releaseResources(pstmt);
        }
        
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDataDeleted;

	}
}
