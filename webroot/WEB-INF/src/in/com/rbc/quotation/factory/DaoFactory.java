/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: DaoFactory.java
 * Package: in.com.rbc.quotation.factory
 * Desc: This class holds the methods which return objects of all the DAO's 
 *       defined in the application
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.factory;

import in.com.rbc.quotation.admin.dao.AddOnDetailsDao;
import in.com.rbc.quotation.admin.dao.AddOnDetailsDaoImpl;
import in.com.rbc.quotation.admin.dao.CommercialEngineerDao;
import in.com.rbc.quotation.admin.dao.CommercialEngineerDaoImpl;
import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dao.CustomerDaoImpl;
import in.com.rbc.quotation.admin.dao.CustomerDiscountDao;
import in.com.rbc.quotation.admin.dao.CustomerDiscountDaoImpl;
import in.com.rbc.quotation.admin.dao.DesignEngineerDao;
import in.com.rbc.quotation.admin.dao.DesignEngineerDaoImpl;
import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dao.ListPriseDaoImpl;
import in.com.rbc.quotation.admin.dao.LocationDao;
import in.com.rbc.quotation.admin.dao.LocationDaoImpl;
import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.admin.dao.MasterDaoImpl;
import in.com.rbc.quotation.admin.dao.SalesManagerDao;
import in.com.rbc.quotation.admin.dao.SalesManagerDaoImpl;
import in.com.rbc.quotation.admin.dao.TechnicalDataDao;
import in.com.rbc.quotation.admin.dao.TechnicalDataDaoImpl;
import in.com.rbc.quotation.admin.dao.UpdateManagersDao;
import in.com.rbc.quotation.admin.dao.UpdateManagersDaoImpl;
import in.com.rbc.quotation.enquiry.dao.MTOQuotationDao;
import in.com.rbc.quotation.enquiry.dao.MTOQuotationDaoImpl;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dao.QuotationDaoImpl;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDaoImpl;
import in.com.rbc.quotation.reports.dao.ReportsDao;
import in.com.rbc.quotation.reports.dao.ReportsDaoImpl;
import in.com.rbc.quotation.user.dao.UserDao;
import in.com.rbc.quotation.user.dao.UserDaoImpl;
import in.com.rbc.quotation.workflow.dao.WorkflowDao;
import in.com.rbc.quotation.workflow.dao.WorkflowDaoImpl;


public class DaoFactory {
    /**
     * This method creates an instance of UserDaoImpl class, upcasts to 
     * UserDao (interface implemented by UserDaoImpl) and returns the same
     * @return Returns the object of UserDao
     */
    public UserDao getUserDao() {
        return new UserDaoImpl();
    }
    
    /**
     * This method creates an instance of WorkflowDaoImpl class, upcasts to 
     * WorkflowDao (interface implemented by WorkflowDaoImpl) and returns the same
     * @return Returns the object of WorkflowDao
      */
    public WorkflowDao getWorkflowDao() {
        return new WorkflowDaoImpl();
    }
   
    /**
     * This method creates an instance of QuotationDaoImpl class, upcasts to 
     * QuotationDao (interface implemented by QuotationDaoImpl) and returns the same
     * @return Returns the object of QuotationDao
     */
    public QuotationDao getQuotationDao() {
        return new QuotationDaoImpl();
    }
    
    /**
     * This method creates an instance of CustomerDaoImpl class, upcasts to 
     * CustomerDao (interface implemented by CustomerDaoImpl) and returns the same
     * @return Returns the object of CustomerDao
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Sep 30, 2008
     */
    public CustomerDao getCustomerDao() {
    	return new CustomerDaoImpl();
    }
    
    /**
     * This method creates an instance of LocationDaoImpl class, upcasts to 
     * LocationDao (interface implemented by LocationDaoImpl) and returns the same
     * @return Returns the object of LocationDao
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 3, 2008
     */
    public LocationDao getLocationDao() {
    	return new LocationDaoImpl();
    }
    
    /**
     * This method creates an instance of SalesManagerDaoImpl class, upcasts to 
     * SalesManagerDao (interface implemented by SalesManagerDaoImpl) and returns the same
     * @return Returns the object of SalesManagerDao
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 7, 2008
     */
    public SalesManagerDao getSalesManagerDao() {
    	return new SalesManagerDaoImpl();
    }
    
    /**
     * This method creates an instance of DesignEngineerDaoImpl class, upcasts to 
     * DesignEngineerDao (interface implemented by DesignEngineerDaoImpl) and returns the same
     * @return Returns the object of DesignEngineerDao
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 17, 2008
     */
    public DesignEngineerDao getDesignEngineerDao() {
    	return new DesignEngineerDaoImpl();
    }
    
    /**
     * This method creates an instance of UpdateManagersDaoImpl class, upcasts to 
     * UpdateManagersDao (interface implemented by UpdateManagersDaoImpl) and returns the same
     * @return  Returns the object of UpdateManagersDao
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Oct 13, 2008
     */
    public UpdateManagersDao getUpdateManagersDao() {
    	return new UpdateManagersDaoImpl();
    }

	/**
     * This method creates an instance of MasterDaoImpl class, upcasts to 
     * MasterDao (interface implemented by MasterDaoImpl) and returns the same
     * @return Returns the object of MasterDao
     * @author 100006126
     * Created on: Otc 1, 2008
     */
	public MasterDao getMasterDao() {
	    return new MasterDaoImpl();
	}

	public SearchEnquiryDao getSearchEnquiryDao() {
		 return new SearchEnquiryDaoImpl();
	}
	
	public ReportsDao getReportsDao() {
		 return new ReportsDaoImpl();
	}
   
    /**
     * This method creates an instance of CommercialEngineerDaoImpl class, upcasts to 
     * DesignEngineerDao (interface implemented by CommercialEngineerDaoImpl) and returns the same
     * @return Returns the object of CommercialEngineerDao
     * @author 900010540 (Gangadhar)
     * Created on: March 1,2019
     */
    public CommercialEngineerDao getCommercialEngineerDao() {
    	return new CommercialEngineerDaoImpl();
    }
    
    /**
     * This method creates an instance of TechnicalDataDaoImpl class, upcasts to 
     * TechnicalDataDao (interface implemented by TechnicalDataDaoImpl) and returns the same
     * @return Returns the object of TechnicalDataDao
     * @author 900008798 (Abhilash Moola)
     * Created on: Nov 20, 2020
     */
    public TechnicalDataDao getTechnicalDataDao() {
    	return new TechnicalDataDaoImpl();
    }
    
    /**
     * This method creates an instance of CustomerDiscountDaoImpl class, upcasts to 
     * CustomerDiscountDao (interface implemented by CustomerDiscountDaoImpl) and returns the same
     * @return Returns the object of TechnicalDataDao
     * @author 900008798 (Abhilash Moola)
     * Created on: Dec 02, 2020
     */
    public CustomerDiscountDao getCustomerDiscountDao() {
    	return new CustomerDiscountDaoImpl();
    }
    
    /**
     * This method creates an instance of ListPriseDaoImpl class, upcasts to 
     * ListPriseDao (interface implemented by ListPriseDaoImpl) and returns the same
     * @return Returns the object of ListPriseDao
     * @author 900008798 (Abhilash Moola)
     * Created on: Dec 02, 2020
     */
    public ListPriseDao getListPriseDao() {
    	return new ListPriseDaoImpl();
    }

    /**
     * This method creates an instance of AddOnDetailsDaoImpl class, upcasts to 
     * AddOnDetailsDao (interface implemented by AddOnDetailsDaoImpl) and returns the same
     * @return Returns the object of AddOnDetailsDao
     */
    public AddOnDetailsDao getAddOnDetailsDao() {
    	return new AddOnDetailsDaoImpl();
    }
    
    /**
     * This method creates an instance of MTOQuotationDaoImpl class, upcasts to 
     * MTOQuotationDao (interface implemented by MTOQuotationDaoImpl) and returns the same
     * @return		Returns the object of MTOQuotationDao
     */
    public MTOQuotationDao getMTOQuotationDao() {
    	return new MTOQuotationDaoImpl();
    }
    
}
