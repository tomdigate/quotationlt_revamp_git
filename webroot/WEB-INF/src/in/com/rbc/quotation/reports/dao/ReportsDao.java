package in.com.rbc.quotation.reports.dao;

import in.com.rbc.quotation.reports.dto.ReportDto;

import java.sql.Connection;
import java.util.ArrayList;

/**
 * This interface contains all the database method used for
 * generating graphs.
 */
public interface ReportsDao {
	
    /**
     * Retrieves the list of ReportsDataSetDto objects
     * @param conn Connection object to connect to database
     * @param oReportDto ReportDto object which has Report Input information
     * @return Returns the list of ReportsDataSetDto objects
     * @throws Exception If any error occurs during the process
     * @author 100003810 (Subhakar Edeti)
     * Created on: October 27, 2008
     */
    public ArrayList getDataSetForGraph(Connection conn, ReportDto oReportDto ) throws Exception;
    
	/**
	 * Retrieves the RegionID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns RegionID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getRegionID(Connection conn,String sRegiondesc) throws Exception;
	
	/**
	 * Retrieves the SaleManagerID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns SaleManagerID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getSaleManagerID(Connection conn,String sSalesMangerdesc) throws Exception;
	
	/**
	 * Retrieves the CustomerID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns CustomerID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getCustomerID(Connection conn,String sCustomerdesc) throws Exception;
	
	/**
	 * Retrieves the LocationID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns LocationID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getLocationID(Connection conn,String sLocationdesc) throws Exception;
}
