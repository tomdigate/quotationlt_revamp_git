/**
 * ******************************************************************************************
 * Project Name: Quality Tools
 * Document Name: ReportDatasetDto
 * Package: in.com.rbc.quality.fracas.reports.dto
 * Desc: DTO class that holds getters and setters of each property for dataset.
 * ******************************************************************************************
 * Author: 100003810 (Subhakar Edeti)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.reports.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

/**
 * This Class is used for to hold the dataset for
 * generating detail graph
 */
public class ReportDatasetDto {
	/**
	 * Stroies the section  
	 */	
	private String section = null;
	/**
	 * Stroies the sectionId  
	 */	
    private int sectionId = 0;
    /**
     * Stroies the openCount  
     */	
	private int openCount = 0;
	/**
     * Stroies the legend  
     */	
	private String legend=null;	
	
	 /**
     * @return Returns the openCount.
     */
	public int getOpenCount() {
		return openCount;
	}
	/**
     * @param openCount The openCount to set.
     */
	public void setOpenCount(int openCount) {
		this.openCount = openCount;
	}
	 /**
     * @return Returns the section.
     */
	public String getSection() {
		return CommonUtility.replaceNull(section);
	}
	/**
     * @param section The section to set.
     */
	public void setSection(String section) {
		this.section = section;
	}
    
    /**
     * @return Returns the sectionId.
     */
    public int getSectionId() {
    
        return sectionId;
    }
    
    /**
     * @param sectionId The sectionId to set.
     */
    public void setSectionId(int sectionId) {
    
        this.sectionId = sectionId;
    }
    /**
     * @return Returns the legend.
     */
	public String getLegend() {
		return CommonUtility.replaceNull(legend);
	}
	/**
     * @param legend The legend to set.
     */
	public void setLegend(String legend) {
		this.legend = legend;
	}
	
}
