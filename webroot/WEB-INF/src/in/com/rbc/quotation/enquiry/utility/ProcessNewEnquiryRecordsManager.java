package in.com.rbc.quotation.enquiry.utility;

import java.sql.Connection;
import java.util.ArrayList;

import in.com.rbc.common.utility.ExceptionUtility;
import in.com.rbc.common.utility.MailUtility;
import in.com.rbc.quotation.common.constants.MailConstants;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.dao.UserDao;
import in.com.rbc.quotation.user.dto.UserDto;

public class ProcessNewEnquiryRecordsManager {

	public ArrayList<NewEnquiryDto> fetchNoMailSentEnquiriesList() throws Exception {
		String sMethodName = "fetchNoMailSentEnquiriesList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        ArrayList<NewEnquiryDto> alNoMailSentEnquiriesList = new ArrayList<>();
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        
        try {
        	/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
        	/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            alNoMailSentEnquiriesList = oQuotationDao.fetchNoMailSentEnquiries(conn);
            
        } catch(Exception e) {
        	//e.printStackTrace();
	    	//System.out.println("ProcessNewEnquiryRecordsManager.fetchNoMailSentEnquiriesList :: Exception Occurred  :: " + ExceptionUtility.getStackTraceAsString(e));
	    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "ERRORED - while Fetching Enquiries in DB : \n" + ExceptionUtility.getStackTraceAsString(e));
	    	throw e;
        } finally {
        	oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return alNoMailSentEnquiriesList;
	}
	
	public ArrayList<NewEnquiryDto> fetchMailSentCurrentAssigneeEnquiriesList() throws Exception {
		String sMethodName = "fetchMailSentCurrentAssigneeEnquiriesList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        ArrayList<NewEnquiryDto> alMailSent_CurrentAssignee_EnquiriesList = new ArrayList<>();
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        
        try {
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            alMailSent_CurrentAssignee_EnquiriesList = oQuotationDao.fetchMailSentCurrentAssigneeEnquiries(conn);
            
        } catch(Exception e) {
        	//e.printStackTrace();
	    	//System.out.println("ProcessNewEnquiryRecordsManager.fetchMailSentCurrentAssigneeEnquiriesList :: Exception Occurred  :: " + ExceptionUtility.getStackTraceAsString(e));
	    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "ERRORED - while Fetching Enquiries in DB : \n" + ExceptionUtility.getStackTraceAsString(e));
	    	throw e;
        } finally {
        	oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return alMailSent_CurrentAssignee_EnquiriesList;
	}
	
	public ArrayList<NewEnquiryDto> fetchMailNotSentCurrentAssigneeEnquiriesList() throws Exception {
		String sMethodName = "fetchMailNotSentCurrentAssigneeEnquiriesList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        ArrayList<NewEnquiryDto> alMailNotSent_CurrentAssignee_EnquiriesList = new ArrayList<>();
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        
        try {
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
        	/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            alMailNotSent_CurrentAssignee_EnquiriesList = oQuotationDao.fetchMailNotSentCurrentAssigneeEnquiries(conn);
            
        } catch(Exception e) {
        	//e.printStackTrace();
	    	//System.out.println("ProcessNewEnquiryRecordsManager.fetchMailSentCurrentAssigneeEnquiriesList :: Exception Occurred  :: " + ExceptionUtility.getStackTraceAsString(e));
	    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "ERRORED - while Fetching Enquiries in DB : \n" + ExceptionUtility.getStackTraceAsString(e));
	    	throw e;
        } finally {
        	oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return alMailNotSent_CurrentAssignee_EnquiriesList;
	}
	
	
	public void processNotifierEnquiries(ArrayList<NewEnquiryDto> alProcessNotifierList, String sAppUrl) throws Exception {
		String sMethodName = "processNotifierEnquiries"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        UserDao oUserDao = null;
        
        boolean isMailSent = false;
        String sFromEmailId = "", sToEMailId = "", sEmailSubject = "", sEmailBody = "", sCCEMailId = "";
        String shouldSendMailStoped = null;
		boolean canSendEmails = true;
		
        try {
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Create UserDao */
            oUserDao = oDaoFactory.getUserDao();
            
        	for(NewEnquiryDto oNewEnquiryDto : alProcessNotifierList) {
        		
        		// Get Creater User and Assigned User Details.
        		UserDto oCreatedUserDto = oUserDao.getUserInfo(conn, oNewEnquiryDto.getCreatedBy());
        		UserDto oWorkflowAssignedUserDto = oUserDao.getUserInfo(conn, oNewEnquiryDto.getWorkflowAssignedTo());
        		
        		// Set the Email Attributes.
        		sFromEmailId = oCreatedUserDto.getEmailId();
				sToEMailId = oWorkflowAssignedUserDto.getEmailId();
				sEmailSubject = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, MailConstants.QUOTATION_AWAITINGAPPROVAL_EMAIL_SUBJECT);
				sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, MailConstants.QUOTATION_AWAITINGAPPROVAL_EMAIL_BODY);
				
				// Process Email Subject.
				sEmailSubject = CommonUtility.replaceString(sEmailSubject, "__EnquiryNumber__",    oNewEnquiryDto.getEnquiryNumber());
				
				// Process Email Body. 
				sEmailBody = CommonUtility.replaceString(sEmailBody, "__AssignedToName__",   oWorkflowAssignedUserDto.getFullName());
				sEmailBody = CommonUtility.replaceString1(sEmailBody, "__EnquiryLink__",     sAppUrl);
				sEmailBody = CommonUtility.replaceString(sEmailBody, "__EnquiryNumber__",    oNewEnquiryDto.getEnquiryNumber());
				
				shouldSendMailStoped = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_SENDMAIL_STOP);
		        if (shouldSendMailStoped != null
						&& shouldSendMailStoped.trim().length() != 0
						&& shouldSendMailStoped.equalsIgnoreCase("YES")) {
		        	// Set FALSE.
		        	canSendEmails = false;
		        }
				
        		// Step 1: Send the Mail.
		        if(canSendEmails) {
		        	isMailSent = MailUtility.sendMail(sFromEmailId, sToEMailId, sEmailSubject, sEmailBody, sCCEMailId);
		        }
		        //System.out.println("sFromEmailId," + sFromEmailId + " sToEMailId, " + sToEMailId + " :: isMailSent = " + isMailSent);
        		
        		// Step 2: If isMailSent = true, Update Notifier Mail Details.
        		if(isMailSent) {
        			oQuotationDao.processNotifierMailSentUpdates(conn, oNewEnquiryDto);
        		}
        		
        	}
        } catch(Exception e) {
        	//e.printStackTrace();
	    	//System.out.println("ProcessNewEnquiryRecordsManager.processNotifierEnquiries :: Exception Occurred  :: " + ExceptionUtility.getStackTraceAsString(e));
	    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "ERRORED - while Fetching Enquiries in DB : \n" + ExceptionUtility.getStackTraceAsString(e));
	    	throw e;
        } finally {
        	oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	}
	
}
