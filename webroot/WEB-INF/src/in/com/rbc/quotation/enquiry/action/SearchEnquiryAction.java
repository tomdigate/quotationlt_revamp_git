/**
 * ******************************************************************************************
 * Project Name: Request For Quotation Search
 * Document Name: SearchEnquiryAction.java
 * Package: in.com.rbc.quotation.requst.action
 * Desc:  Action class that contains all the action methods for Search
 * ******************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.action;

import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.admin.dao.MasterDaoImpl;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDao;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto;
import in.com.rbc.quotation.enquiry.form.EnquiryForm;
import in.com.rbc.quotation.enquiry.form.SearchEnquiryForm;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.reports.dao.ReportsDao;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SearchEnquiryAction extends BaseAction {
	
	 /**
     * This method is used to display the search page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward viewSearchEnquiryResults(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "viewSearchEnquiryResults";
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        HttpSession session= request.getSession();
        UserDto oUserDto = null;
    	
        /*Saving Request into a Token*/
        saveToken(request);
       
    	try{
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		SearchEnquiryForm oSearchEnquiryForm = (SearchEnquiryForm)actionForm;
    		if (oSearchEnquiryForm == null){
    			oSearchEnquiryForm = new SearchEnquiryForm();
    		}
    		
    		String dispatch = request.getParameter("dispatch");
    		if (dispatch == null){
    			oSearchEnquiryForm.setSearchType("E");
    			dispatch = oSearchEnquiryForm.getDispatch();
    			
    			if (dispatch == null){
    				dispatch = "";
    			}
    		}
    		if(oSearchEnquiryForm.getSearchType()==null || oSearchEnquiryForm.getSearchType().equalsIgnoreCase("") )
    			oSearchEnquiryForm.setSearchType("E");
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
            */
            setRolesToRequest(request); 
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            UserAction oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            DaoFactory oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information*/
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /*
             * This code will be executed when the request is coming from confirmation pages
             * in these cases we have to display the enquiries that were generated by the logged-in user
             */
            if (dispatch.equalsIgnoreCase("fromview")){
            	oSearchEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            	oSearchEnquiryForm.setCreatedBy(oUserDto.getFullName());    			
    		}
         
             /* Object of Hashtable to store different values that are retrieved 
             * using searchAssetVo.getAttributeSearchResults method
             */
        	Hashtable htSearchResultsList = new Hashtable();
        	
        	/*
             * Object of ListModel class to store the values necessary to display 
             * the results in attribute search results page with pagination
             */
        	ListModel oListModel = new ListModel();
            
            if(dispatch!=null && !dispatch.equalsIgnoreCase(""))
            {
            	request.setAttribute("searchAction","true");
            }
            	
            
            
            /*
                 * Setting values for ListModel that are necessary to retrieve rows from the database
                 * and display them in search results page. It holds information like
                 * --> number of rows to be displayed at once in the page
                 * --> column on which rows have to be sorted
                 * --> order in which rows have to be sorted
                 * --> set of rows to be retrieved depending on the page number selected for viewing
                 * Details of parameters:
                 * 1. Request object
                 * 2. Sort column index
                 * 3. Sort order
                 * 4. Search form
                 * 5. Field that stores the invoke method name
                 * 8. Invoke method name
                 */
                oListModel.setParams(request, 
                                       "1", 
                                       in.com.rbc.quotation.common.utility.ListModel.ASCENDING, 
                                       "SearchEnquiryForm", 
                                       "SearchEnquiryForm.invoke", 
                                       "viewSearchEnquiryResults");
            	
                SearchEnquiryDto oSearchEnquiryDto = new SearchEnquiryDto() ;
                SearchEnquiryDao oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
                
                PropertyUtils.copyProperties(oSearchEnquiryDto , oSearchEnquiryForm);
                	request.setAttribute("userSSO",oUserDto.getUserId());
                if(oSearchEnquiryDao.isSalesManager(conn,oUserDto.getUserId()))
                {
                	request.setAttribute("salesManager","true");
                	
                }
                if(oSearchEnquiryDao.setQuotedFlag(conn,oSearchEnquiryDto.getCreatedBySSO(),oUserDto.getUserId())){
                	request.setAttribute(QuotationConstants.QUOTATION_QUOTEDFLAG,QuotationConstants.QUOTATION_STRING_TRUE);
                }
                
                if (dispatch.equalsIgnoreCase("new search")){
                	  
                	oListModel.setSortBy("1");  
                	oSearchEnquiryForm.setDispatch("");
                	oSearchEnquiryForm.setFrameCheck("");
                	oSearchEnquiryForm.setKwCheck("");
                	oSearchEnquiryForm.setMountingCheck("");
                	oSearchEnquiryForm.setVoltsCheck("");
                	oSearchEnquiryForm.setPoleCheck("");
              	 
                	if (CommonUtility.getStringParameter(request, "customerName").trim().length() > 0){
                		request.getSession().setAttribute("searchCustomerName", CommonUtility.getStringParameter(request, "customerName"));
                	}else{
                		request.getSession().removeAttribute("searchCustomerName");
                	}
                }
                
               if(dispatch.equalsIgnoreCase("fromViewPage")){
            	   oSearchEnquiryDto = (SearchEnquiryDto)session.getAttribute("oSearchEnquiryDto");
               }                	
                
 	           htSearchResultsList = oSearchEnquiryDao.getEnquirySearchResults(conn, 
 	            		oSearchEnquiryDto,oListModel,oUserDto.getUserId());
 	           if (htSearchResultsList != null) {
   	          	
 	        	  oSearchEnquiryDto.setSearchResultsList((ArrayList)htSearchResultsList
	                                  .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
	 

	                request.setAttribute("oSearchEnquiryDto", oSearchEnquiryDto);                        
	                session.setAttribute("oSearchEnquiryDto", oSearchEnquiryDto);
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
	                                          (ArrayList)htSearchResultsList
	                                          .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,
	                                           htSearchResultsList
	                                           .get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
	                                           new Integer(((ArrayList)htSearchResultsList
	                                           .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST))
	                                       .size()));
	                session.setAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY,(String)htSearchResultsList
                            .get(QuotationConstants.QUOTATION_SEARCH_QUERY));
	                session.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                            (ArrayList)htSearchResultsList
                            .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
	        
	            } else {
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
	            }  	       
 	          
 	          loadLookUpValues(conn,oSearchEnquiryForm);
 	          
 	          oSearchEnquiryForm.setStatusList(new MasterDaoImpl().getStatusList(conn)); 	          
 	        
     	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            oDBUtility.releaseResources(conn);
        }
    	
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    }
    
    
    /**
     * This method is used to display the search page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward printOrExportEnquiryResults(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "viewSearchEnquiryResults";
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        // This is to filter the request based on print and export
        String sFunctionType = "";
        UserDto oUserDto = null;
    	
    	try{
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		SearchEnquiryForm oSearchEnquiryForm = (SearchEnquiryForm)actionForm;
    		if (oSearchEnquiryForm == null){
    			oSearchEnquiryForm = new SearchEnquiryForm();
    		}
        	sFunctionType = request.getParameter("functionType");
        	oSearchEnquiryForm.setSearchType(request.getParameter("searchType")==null?"R":request.getParameter("searchType"));
        	
			if (sFunctionType == null || sFunctionType.trim().length() < 1){
				sFunctionType = "None";
			}
			request.setAttribute("sFunctionType",sFunctionType);
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
            */
            setRolesToRequest(request); 
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            UserAction oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            DaoFactory oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information*/
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
         
             /* Object of Hashtable to store different values that are retrieved 
             * using searchAssetVo.getAttributeSearchResults method
             */
        	ArrayList alSearchResultsList = new ArrayList();
        	
        	/*
             * Object of ListModel class to store the values necessary to display 
             * the results in attribute search results page with pagination
             */
        	ListModel oListModel = new ListModel();
            
            /*
                 * Setting values for ListModel that are necessary to retrieve rows from the database
                 * and display them in search results page. It holds information like
                 * --> number of rows to be displayed at once in the page
                 * --> column on which rows have to be sorted
                 * --> order in which rows have to be sorted
                 * --> set of rows to be retrieved depending on the page number selected for viewing
                 * Details of parameters:
                 * 1. Request object
                 * 2. Sort column index
                 * 3. Sort order
                 * 4. Search form
                 * 5. Field that stores the invoke method name
                 * 8. Invoke method name
                 */
                oListModel.setParams(request, 
                                       "1", 
                                       in.com.rbc.quotation.common.utility.ListModel.ASCENDING, 
                                       "SearchEnquiryForm", 
                                       "SearchEnquiryForm.invoke", 
                                       "viewSearchEnquiryResults");
            	HttpSession session=request.getSession();
            	
                SearchEnquiryDto oSearchEnquiryDto = new SearchEnquiryDto() ;
                SearchEnquiryDao oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
                
                PropertyUtils.copyProperties(oSearchEnquiryDto , oSearchEnquiryForm);
	            
 	           /* Retrieving the search results using objAttributeSearchDao.getAttributeSearchResults method */
 	            alSearchResultsList = oSearchEnquiryDao.getEnquirySearchList(conn, 
 	            		oSearchEnquiryDto,oListModel,oUserDto.getUserId(),(String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY));
 	           if (alSearchResultsList.size()>0) {
   	          	
 	        	  oSearchEnquiryDto.setSearchResultsList((ArrayList)alSearchResultsList);
 	        	  
	              request.setAttribute("oSearchEnquiryDto", oSearchEnquiryDto);                        
	              request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)alSearchResultsList);
	              request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(alSearchResultsList.size()));
	        
	            } else {
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
	            } 
     	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            oDBUtility.releaseResources(conn);
        }
    	
        if (sFunctionType.equalsIgnoreCase("export")){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
		}else if (sFunctionType.equalsIgnoreCase("print")){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
		}else{
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, 
                    "A problem occurred while retrieving records" +
                    " Please try again.");
       	 	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}
    }
    /**
     * This method is used to display the search page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward interimSearch(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "interimSearch";
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        HttpSession session= request.getSession();
        UserDto oUserDto = null;
    	ReportsDao oReportsDao = null;
        /*Saving Request into a Token*/
        saveToken(request);
       
    	try{
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		SearchEnquiryForm oSearchEnquiryForm = (SearchEnquiryForm)actionForm;
    		if (oSearchEnquiryForm == null){
    			oSearchEnquiryForm = new SearchEnquiryForm();
    		}
    		
    		String dispatch = request.getParameter("dispatch");
    		
    		String sRegionId = request.getParameter("regionid")==null?"":request.getParameter("regionid")=="0"?"":request.getParameter("regionid"); 
			String sStatusId = request.getParameter("statusid")==null?"":request.getParameter("statusid")=="0"?"":request.getParameter("statusid");
			String sSalesManagerId = request.getParameter("salesmangerid")==null?"":request.getParameter("salesmangerid")=="0"?"":request.getParameter("salesmangerid");
			
			String sCustomerId = request.getParameter("customerid")==null?"":request.getParameter("customerid")=="0"?"":request.getParameter("customerid");
			String sYear = request.getParameter("year")==null?"":request.getParameter("year")=="0"?"":request.getParameter("year");
			String sMonth = request.getParameter("month")==null?"":request.getParameter("month")=="0"?"":request.getParameter("month");
			String sGraphType = request.getParameter("graphtype")==null?"0":request.getParameter("graphtype");
			int iSectionid=CommonUtility.getIntParameter(request, "sectionid");
			int iGraphtype=CommonUtility.getIntParameter(request,"graphtype");			
			String section=CommonUtility.replaceNull(request.getParameter("section"));
			
    		if (dispatch == null){
    			oSearchEnquiryForm.setSearchType("R");
    			dispatch = oSearchEnquiryForm.getDispatch();
    			
    			if (dispatch == null){
    				dispatch = "";
    			}
    		}
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
            */
            setRolesToRequest(request); 
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            UserAction oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            DaoFactory oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information*/
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
           /* Geting Factory Instance of Reports Dao */
            oReportsDao = oDaoFactory.getReportsDao();
            
            
            SearchEnquiryDao oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
            
            
            if(!sSalesManagerId.equalsIgnoreCase(""))
			{
            	sSalesManagerId = oSearchEnquiryDao.getSaleManagerSSObyId(conn,sSalesManagerId) + "";
            }
            /*  if(sGraphType.equalsIgnoreCase(QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH+""))
			{
                sCustomerId = oReportsDao.getCustomerID(conn,sCustomerId)+"";
			}
            
             * This code will be executed when the request is coming from confirmation pages
             * in these cases we have to display the enquiries that were generated by the logged-in user
             */
            if (dispatch.equalsIgnoreCase("fromview")){
            	oSearchEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            	oSearchEnquiryForm.setCreatedBy(oUserDto.getFullName());    			
    		}
         
            if(sGraphType!=null && !sGraphType.equalsIgnoreCase("0"))
            {
            	oSearchEnquiryForm.setCreatedBySSO(sSalesManagerId);
            	oSearchEnquiryForm.setCustomerId(sCustomerId);
            	oSearchEnquiryForm.setStatusId(sStatusId);
            	
            	oSearchEnquiryForm.setRegionId(sRegionId);
            	oSearchEnquiryForm.setYear(sYear);
            	oSearchEnquiryForm.setMonth(sMonth);
            	request.setAttribute("searchAction","true");
            	oSearchEnquiryForm.setSearchType("E");
            }
            
            if(iSectionid > 0){
            	switch(iGraphtype){
            		case QuotationConstants.RBC_QUOTATION_REGION_GRAPH : {
            			oSearchEnquiryForm.setRegionId(""+iSectionid);        
            			break;
            		}
            		case QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH : {
            			oSearchEnquiryForm.setCreatedBySSO(""+iSectionid);        
            			break;
            		}
            		case QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH : {
            			oSearchEnquiryForm.setCustomerId(""+iSectionid);        
            			break;
            		}
            		case QuotationConstants.RBC_QUOTATION_STATUS_GRAPH : {
            			oSearchEnquiryForm.setStatusId(""+iSectionid);        
            			break;
            		}
            		case QuotationConstants.RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH : {
            			oSearchEnquiryForm.setLocationId(""+iSectionid);        
            			break;
            		}
            	}
				
			}
            else if (section.trim().length()>0)
            {
            	switch(iGraphtype){
        		case QuotationConstants.RBC_QUOTATION_REGION_GRAPH : {
        			oSearchEnquiryForm.setRegionId(""+iSectionid);        
        			break;
        		}
        		case QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH : {
        			oSearchEnquiryForm.setCreatedBySSO(""+iSectionid);        
        			break;
        		}
        		case QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH : {
        			oSearchEnquiryForm.setCustomerId(""+iSectionid);        
        			break;
        		}
        		case QuotationConstants.RBC_QUOTATION_STATUS_GRAPH : {
        			oSearchEnquiryForm.setStatusName(""+section);        
        			break;
        		}
        		case QuotationConstants.RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH : {
        			oSearchEnquiryForm.setLocationId(""+iSectionid);        
        			break;
        		}
        	}
            }
            	
            
            
             /* Object of Hashtable to store different values that are retrieved 
             * using searchAssetVo.getAttributeSearchResults method
             */
        	Hashtable htSearchResultsList = new Hashtable();
        	
        	/*
             * Object of ListModel class to store the values necessary to display 
             * the results in attribute search results page with pagination
             */
        	ListModel oListModel = new ListModel();
            
            if(dispatch!=null && !dispatch.equalsIgnoreCase(""))
            {
            	request.setAttribute("searchAction","true");
            }
            	
          
            
            /*
                 * Setting values for ListModel that are necessary to retrieve rows from the database
                 * and display them in search results page. It holds information like
                 * --> number of rows to be displayed at once in the page
                 * --> column on which rows have to be sorted
                 * --> order in which rows have to be sorted
                 * --> set of rows to be retrieved depending on the page number selected for viewing
                 * Details of parameters:
                 * 1. Request object
                 * 2. Sort column index
                 * 3. Sort order
                 * 4. Search form
                 * 5. Field that stores the invoke method name
                 * 8. Invoke method name
                 */
                oListModel.setParams(request, 
                                       "1", 
                                       in.com.rbc.quotation.common.utility.ListModel.ASCENDING, 
                                       "SearchEnquiryForm", 
                                       "SearchEnquiryForm.invoke", 
                                       "viewSearchEnquiryResults");
            	
                SearchEnquiryDto oSearchEnquiryDto = new SearchEnquiryDto() ;
                
                
                PropertyUtils.copyProperties(oSearchEnquiryDto , oSearchEnquiryForm);
                	request.setAttribute("userSSO",oUserDto.getUserId());
                if(oSearchEnquiryDao.isSalesManager(conn,oUserDto.getUserId()))
                {
                	request.setAttribute("salesManager","true");
                	
                }
 	           /* Retrieving the search results using objAttributeSearchDao.getAttributeSearchResults method */
 	            htSearchResultsList = oSearchEnquiryDao.getEnquirySearchResults(conn, 
 	            		oSearchEnquiryDto,oListModel,oUserDto.getUserId());
 	           if (htSearchResultsList != null) {
 	        	  oSearchEnquiryDto.setSearchResultsList((ArrayList)htSearchResultsList
	                                  .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
 	        	  
	                request.setAttribute("oSearchEnquiryDto", oSearchEnquiryDto);                        
	                
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
	                                          (ArrayList)htSearchResultsList
	                                          .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,
	                                           htSearchResultsList
	                                           .get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
	                                           new Integer(((ArrayList)htSearchResultsList
	                                           .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST))
	                                       .size()));
	                session.setAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY,(String)htSearchResultsList
                            .get(QuotationConstants.QUOTATION_SEARCH_QUERY));
	                
	                session.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                            (ArrayList)htSearchResultsList
                            .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
	            } else {
	                request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
	            } 
 	           
 	            
 	          oSearchEnquiryForm.setStatusList(new MasterDaoImpl().getStatusList(conn));
 	         loadLookUpValues(conn,oSearchEnquiryForm); 	          
 	         
     	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            oDBUtility.releaseResources(conn);
        }
    	
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    	
    	
    }
    
    /**
   	* This method is used to load the list values for search by attributes in search enquiry screen
   	* @param conn Connection object to connect to database
   	* @param SearchEnquiryForm oSearchEnquiryForm object having enquiry search data
   	* @return Returns SearchEnquiryForm Object
   	* @author 610092227
   	* Created on: 01 Nov 2019
   	*/
    
 private SearchEnquiryForm loadLookUpValues(Connection conn, SearchEnquiryForm oSearchEnquiryForm) throws Exception{
    	
    	
    	/* Creating an instance of of DaoFactory & retrieving UserDao object */
        DaoFactory oDaoFactory = new DaoFactory();		
		QuotationDao oQuotationDao = oDaoFactory.getQuotationDao();		
		
		oSearchEnquiryForm.setAlEnclosureList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ENCLOSURE)); 
		oSearchEnquiryForm.setAlPoleList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_POLE));		
		oSearchEnquiryForm.setAlScsrList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TYPE)); 
		oSearchEnquiryForm.setMountingList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MOUNTING)); 
		oSearchEnquiryForm.setAlApplicationList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_APPL)); 
		oSearchEnquiryForm.setAlVoltsList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_VOLTS)); 
		oSearchEnquiryForm.setAlIndustryList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_INDUSTRY));
		

		
	
    	return oSearchEnquiryForm;
    }
}
