/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: MTOQuotationDao.java
 * Package: in.com.rbc.quotation.enquiry.dao
 * Desc:  Dao interface that defines methods related to MTO Enquiry functionalities. <br>
 *        This interface is implemented by --> MTOQuotationDaoImpl
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;

public interface MTOQuotationDao {

	/**
	 * Method to Fetch MTO Rating Details Based on Enquiry ID
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NewMTORatingDto> fetchNewMTORatingDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Fetch MTO Rating - With Add-On and Technical Details.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public NewMTORatingDto fetchNewMTOTechAddOnDetails(Connection conn, NewMTORatingDto oMTORatingDto) throws Exception;
	
	/**
	 * Method to Search MTO Ratings Based on Search Criteria.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NewMTORatingDto> searchMTORatings(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Fetch MTO AddOn Map.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public Map<String,String> fetchMTOAddOnMap(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Fetch Re-assign To Users List.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<KeyValueVo> fetchMTOReassignUsersList(Connection conn) throws Exception;

	/**
	 * Method to Save the Re-assign Comments.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @throws Exception
	 */
	public void saveReassignComments(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Save/Update the Design Fields on Return to SM.
	 * @param conn
	 * @param oNewEnquiryDto
	 */
	public String updateDesignFieldsOnReturn(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Verify If all MTO Ratings are Design Complete or Not.
	 * @param conn
	 * @param sEnquiryId
	 * @return
	 * @throws Exception
	 */
	public boolean verifyMTOEnquiryDesignComplete(Connection conn, String sEnquiryId) throws Exception;
	
	/**
	 * Method to Save MTO Ratings List with values from Design page.
	 * @param conn
	 * @param alMTORatingsList
	 * @return
	 * @throws Exception
	 */
	public String saveMTORatingDetails(Connection conn, ArrayList<NewRatingDto> alMTORatingsList) throws Exception;
	
	/**
	 * This Method is used to Fetch the MTO Enquiry and Rating details based on an enquiryId
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param sOperationType
	 * @return NewEnquiryDto
	 * @throws Exception
	 */
	public NewEnquiryDto getMTOEnquiryDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception;
	
	/**
	 * This Method is used to Fetch the Complete RatingsList with MTO Fields, based on an enquiryId.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NewRatingDto> fetchMTORatingDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Save MTO Tech Rating values and AddOn Details from Design page.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param sOperationType
	 * @return NewEnquiryDto
	 * @throws Exception
	 */
	public String saveMTOTechCompleteDetails(Connection conn, ArrayList<NewMTORatingDto> alMTOTechCompleteDetails, String loggedInUserId) throws Exception;
	
	/**
	 * Method to Fetch the Product Lines PickList - FOr which PDF needs to be generated.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getPDFProdLinesList(Connection conn) throws Exception;

	/**
	 * Method to Save the NewTechnicalOfferDto - PDF Content - List for all applicable Ratings.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param alRatingPDFList
	 * @return
	 * @throws Exception
	 */
	public String savePDFRatingsList(Connection conn, NewEnquiryDto oNewEnquiryDto, ArrayList<NewTechnicalOfferDto> alRatingPDFList) throws Exception;
	
}
