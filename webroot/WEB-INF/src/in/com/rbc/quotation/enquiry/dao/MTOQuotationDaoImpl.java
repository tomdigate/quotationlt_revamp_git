package in.com.rbc.quotation.enquiry.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTOAddOnDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;

public class MTOQuotationDaoImpl implements MTOQuotationDao, EnquiryQueries {

	@Override
	public ArrayList<NewMTORatingDto> fetchNewMTORatingDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchNewMTORatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewMTORatingDto> alRatingsList = new ArrayList<NewMTORatingDto>();
    	NewMTORatingDto oTempNewMTORatingDto = null;
    	
    	try {
    		pstmt = conn.prepareStatement(GET_NEWMTORATING_DETAILS);
    		pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewMTORatingDto = new NewMTORatingDto();
    			
    			oTempNewMTORatingDto.setMtoRatingId(rs.getInt("QRD_LT_MTORATINGID"));
    			oTempNewMTORatingDto.setMtoEnquiryId(rs.getInt("QRD_LT_MTOENQUIRYID"));
    			oTempNewMTORatingDto.setMtoRatingNo(rs.getInt("QRD_LT_MTORATINGNO"));
    			oTempNewMTORatingDto.setMtoQuantity(rs.getInt("QRD_LT_MTOQUANTITY"));
    			
    			// MTO Search Fields
    			oTempNewMTORatingDto.setMtoProdLineId(rs.getString("QRD_LT_MTOPRODLINEID"));
    			oTempNewMTORatingDto.setMtoKWId(rs.getString("QRD_LT_MTOKW"));
    			oTempNewMTORatingDto.setMtoPoleId(String.valueOf(rs.getInt("QRD_LT_MTOPOLE")));
    			oTempNewMTORatingDto.setMtoFrameId(rs.getString("QRD_LT_MTOFRAME"));
    			oTempNewMTORatingDto.setMtoFrameSuffixId(rs.getString("QRD_LT_MTOFRAME_SUFFIX"));
    			oTempNewMTORatingDto.setMtoMountingId(rs.getString("QRD_LT_MTOMOUNTINGID"));
    			oTempNewMTORatingDto.setMtoTBPositionId(rs.getString("QRD_LT_MTOTBPOSITIONID"));
    			oTempNewMTORatingDto.setMtoMfgLocation(rs.getString("QRD_LT_MTOMFGLOCATION"));
    			// MTO Commercial Fields
    			oTempNewMTORatingDto.setMtoStandardAddOnPercent(rs.getString("QRD_LT_MTOSTANDARDADDONPERCENT"));
    			oTempNewMTORatingDto.setMtoStandardCashExtra(rs.getString("QRD_LT_MTOSTANDARDCASHEXTRA"));
    			oTempNewMTORatingDto.setMtoSpecialAddOnPercent(rs.getString("QRD_LT_MTOSPECIALADDONPERCENT"));
    			oTempNewMTORatingDto.setMtoSpecialCashExtra(rs.getString("QRD_LT_MTOSPECIALCASHEXTRA"));
    			oTempNewMTORatingDto.setMtoPercentCopper(rs.getString("QRD_LT_MTOPERCENTCOPPER"));
    			oTempNewMTORatingDto.setMtoPercentStamping(rs.getString("QRD_LT_MTOPERCENTSTAMPING"));
    			oTempNewMTORatingDto.setMtoPercentAluminium(rs.getString("QRD_LT_MTOPERCENTALUMINIUM"));
    			oTempNewMTORatingDto.setMtoTotalAddOnPercent(rs.getString("QRD_LT_MTOTOTALADDONPERCENT"));
    			oTempNewMTORatingDto.setMtoTotalCashExtra(rs.getString("QRD_LT_MTOTOTALCASHEXTRA"));
    			oTempNewMTORatingDto.setMtoMaterialCost(rs.getString("QRD_LT_MTOMATERIALCOST"));
    			// Actual Top Fields - Mandatory.
    			oTempNewMTORatingDto.setActualProdLineId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, rs.getString("QRD_LT_MTOPRODLINEID")));
    			oTempNewMTORatingDto.setActualKWId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, rs.getString("QRD_LT_MTOKW")));
    			oTempNewMTORatingDto.setActualPoleId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, String.valueOf(rs.getInt("QRD_LT_MTOPOLE"))));
    			oTempNewMTORatingDto.setActualFrameId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, rs.getString("QRD_LT_MTOFRAME")));
    			oTempNewMTORatingDto.setActualFrameSuffixId(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FRAME_SUFFIX, rs.getString("QRD_LT_MTOFRAME_SUFFIX")));
    			oTempNewMTORatingDto.setActualMountingId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, rs.getString("QRD_LT_MTOMOUNTINGID")));
    			oTempNewMTORatingDto.setActualTBPositionId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, rs.getString("QRD_LT_MTOTBPOSITIONID")));
    			oTempNewMTORatingDto.setActualMfgLocation(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_MFG_LOCATION, rs.getString("QRD_LT_MTOMFGLOCATION")));
    			// MTO Special Features
    			oTempNewMTORatingDto.setMtoSpecialFeatures(rs.getString("QRD_LT_MTO_SPECIALFEATURES"));
    			oTempNewMTORatingDto.setMtoDesignComplete(rs.getString("QRD_LT_MTO_ISDESIGNCOMPLETE"));
    			
    			alRatingsList.add(oTempNewMTORatingDto);
    		}
    		
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return alRatingsList;
	}
	
	@Override
	public NewMTORatingDto fetchNewMTOTechAddOnDetails(Connection conn, NewMTORatingDto oMTORatingDto)throws Exception {
		String sMethodName = "fetchNewMTOTechAddOnDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null,  pstmt2 = null, pstmt3 = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs1 = null, rs2 = null, rs3 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        ArrayList<KeyValueVo> alNewMTOTechDetailsList = new ArrayList<>();
        ArrayList<NewMTOAddOnDto> alNewMTOAddOnsList = new ArrayList<>();
        ArrayList<KeyValueVo> alNewCompleteAddOnsList = new ArrayList<>();
        
        KeyValueVo oKeyValueVo = null;
        NewMTOAddOnDto oNewMTOAddOnDto = null;
        String sKeyValue = "";
        
		try {
			pstmt1 = conn.prepareStatement(GET_NEWMTO_TECHDETAILS_BY_RATINGNO);
    		pstmt1.setInt(1, oMTORatingDto.getMtoEnquiryId());
    		pstmt1.setInt(2, oMTORatingDto.getMtoRatingNo());
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oKeyValueVo = new KeyValueVo();
    			sKeyValue = rs1.getString("QRD_LT_MTOADDONTYPE");
    			oKeyValueVo.setKey(sKeyValue.substring(0, sKeyValue.indexOf("-")));
    			oKeyValueVo.setValue(rs1.getString("QRD_LT_MTOADDONVALUE"));
    			
    			alNewMTOTechDetailsList.add(oKeyValueVo);
    		}
    		oDBUtility.releaseResources(rs1, pstmt1);
    		oMTORatingDto.setNewMTOTechDetailsList(alNewMTOTechDetailsList);
    		
    		pstmt2 = conn.prepareStatement(GET_NEWMTO_ADDONDETAILS_BY_RATINGNO);
    		pstmt2.setInt(1, oMTORatingDto.getMtoEnquiryId());
    		pstmt2.setInt(2, oMTORatingDto.getMtoRatingNo());
    		rs2 = pstmt2.executeQuery();
    		
    		while(rs2.next()) {
    			oNewMTOAddOnDto = new NewMTOAddOnDto();
    			oNewMTOAddOnDto.setMtoEnquiryId(rs2.getInt("QRD_LT_MTOENQUIRYID"));
    			oNewMTOAddOnDto.setMtoRatingNo(rs2.getInt("QRD_LT_MTORATINGNO"));
    			oNewMTOAddOnDto.setMtoAddOnType(rs2.getString("QRD_LT_MTOADDONTYPE"));
    			oNewMTOAddOnDto.setMtoAddOnQty(rs2.getInt("QRD_LT_MTOADDONQTY"));
    			oNewMTOAddOnDto.setMtoAddOnValue(rs2.getString("QRD_LT_MTOADDONVALUE"));
    			oNewMTOAddOnDto.setMtoAddOnCalcType(rs2.getString("QRD_LT_MTOADDONCALCTYPE"));
    			
    			alNewMTOAddOnsList.add(oNewMTOAddOnDto);
    		}
    		oDBUtility.releaseResources(rs2, pstmt2);
    		oMTORatingDto.setNewMTOAddOnsList(alNewMTOAddOnsList);
    		
    		/* 
    		pstmt3 = conn.prepareStatement(GET_COMPLETE_ADDONLIST_BY_PRODLINE);
    		pstmt3.setString(1, getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, oMTORatingDto.getMtoProdLineId()));
    		pstmt3.setString(2, getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, oMTORatingDto.getMtoFrameId()));
    		rs3 = pstmt3.executeQuery();
    		*/
    		
    		pstmt3 = conn.prepareStatement(GET_DESIGN_ADDONLIST);
    		rs3 = pstmt3.executeQuery();
    		
    		while(rs3.next()) {
    			oKeyValueVo = new KeyValueVo();
    			oKeyValueVo.setKey(rs3.getString("RAD_ID"));
    			oKeyValueVo.setValue(rs3.getString("RAD_SPECIAL_FEATURE") + " - " + rs3.getString("RAD_SPECIAL_VALUE"));
    			
    			alNewCompleteAddOnsList.add(oKeyValueVo);
    		}
    		oMTORatingDto.setNewCompleteAddOnsList(alNewCompleteAddOnsList);
    		
		} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
            oDBUtility.releaseResources(rs2, pstmt2);
            oDBUtility.releaseResources(rs3, pstmt3);
    	}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return oMTORatingDto;
	}

	@Override
	public ArrayList<NewMTORatingDto> searchMTORatings(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "searchMTORatings";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewMTORatingDto> alRatingsList = new ArrayList<NewMTORatingDto>();
    	NewMTORatingDto oTempNewMTORatingDto = null;
    	
    	try {
    		String sSearchQuery = buildSearchMTORatingsQuery(oNewEnquiryDto);
    		pstmt = conn.prepareStatement(sSearchQuery);
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewMTORatingDto = new NewMTORatingDto();
    			
    			oTempNewMTORatingDto.setMtoRatingId(rs.getInt("QRD_LT_MTORATINGID"));
    			oTempNewMTORatingDto.setMtoEnquiryId(rs.getInt("QRD_LT_MTOENQUIRYID"));
    			oTempNewMTORatingDto.setMtoRatingNo(rs.getInt("QRD_LT_MTORATINGNO"));
    			oTempNewMTORatingDto.setMtoQuantity(rs.getInt("QRD_LT_MTOQUANTITY"));
    			
    			// MTO Search Fields
    			oTempNewMTORatingDto.setMtoProdLineId(rs.getString("QRD_LT_MTOPRODLINEID"));
    			oTempNewMTORatingDto.setMtoKWId(rs.getString("QRD_LT_MTOKW"));
    			oTempNewMTORatingDto.setMtoPoleId(String.valueOf(rs.getInt("QRD_LT_MTOPOLE")));
    			oTempNewMTORatingDto.setMtoFrameId(rs.getString("QRD_LT_MTOFRAME"));
    			oTempNewMTORatingDto.setMtoFrameSuffixId(rs.getString("QRD_LT_MTOFRAME_SUFFIX"));
    			oTempNewMTORatingDto.setMtoMountingId(rs.getString("QRD_LT_MTOMOUNTINGID"));
    			oTempNewMTORatingDto.setMtoTBPositionId(rs.getString("QRD_LT_MTOTBPOSITIONID"));
    			oTempNewMTORatingDto.setMtoMfgLocation(rs.getString("QRD_LT_MTOMFGLOCATION"));
    			// MTO Commercial Fields
    			oTempNewMTORatingDto.setMtoSpecialAddOnPercent(rs.getString("QRD_LT_MTOSPECIALADDONPERCENT"));
    			oTempNewMTORatingDto.setMtoSpecialCashExtra(rs.getString("QRD_LT_MTOSPECIALCASHEXTRA"));
    			oTempNewMTORatingDto.setMtoPercentCopper(rs.getString("QRD_LT_MTOPERCENTCOPPER"));
    			oTempNewMTORatingDto.setMtoPercentStamping(rs.getString("QRD_LT_MTOPERCENTSTAMPING"));
    			oTempNewMTORatingDto.setMtoPercentAluminium(rs.getString("QRD_LT_MTOPERCENTALUMINIUM"));
    			oTempNewMTORatingDto.setMtoTotalAddOnPercent(rs.getString("QRD_LT_MTOTOTALADDONPERCENT"));
    			oTempNewMTORatingDto.setMtoTotalCashExtra(rs.getString("QRD_LT_MTOTOTALCASHEXTRA"));
    			oTempNewMTORatingDto.setMtoMaterialCost(rs.getString("QRD_LT_MTOMATERIALCOST"));
    			// Actual Top Fields - Mandatory.
    			oTempNewMTORatingDto.setActualProdLineId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, rs.getString("QRD_LT_MTOPRODLINEID")));
    			oTempNewMTORatingDto.setActualKWId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, rs.getString("QRD_LT_MTOKW")));
    			oTempNewMTORatingDto.setActualPoleId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, String.valueOf(rs.getInt("QRD_LT_MTOPOLE"))));
    			oTempNewMTORatingDto.setActualFrameId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, rs.getString("QRD_LT_MTOFRAME")));
    			oTempNewMTORatingDto.setActualFrameSuffixId(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FRAME_SUFFIX, rs.getString("QRD_LT_MTOFRAME_SUFFIX")));
    			oTempNewMTORatingDto.setActualMountingId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, rs.getString("QRD_LT_MTOMOUNTINGID")));
    			oTempNewMTORatingDto.setActualTBPositionId(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, rs.getString("QRD_LT_MTOTBPOSITIONID")));
    			oTempNewMTORatingDto.setActualMfgLocation(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_MFG_LOCATION, rs.getString("QRD_LT_MTOMFGLOCATION")));
    			// MTO Special Features
    			oTempNewMTORatingDto.setMtoSpecialFeatures(rs.getString("QRD_LT_MTO_SPECIALFEATURES"));
    			
    			alRatingsList.add(oTempNewMTORatingDto);
    		}
    		
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return alRatingsList;
	}
	
	private String buildSearchMTORatingsQuery(NewEnquiryDto oNewEnquiryDto) {
		String sMethodName = "buildSearchMTORatingsQuery";

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		StringBuffer sbFieldQuery = new StringBuffer();
		
		if (oNewEnquiryDto != null) {
			sbFieldQuery.append(SEARCH_NEWMTORATINGS);
			
			/* Check EnquiryId */
			sbFieldQuery.append("QRD_LT_MTOENQUIRYID = ");
			sbFieldQuery.append(oNewEnquiryDto.getEnquiryId() + " AND ");
			
			/* Check using the Search Fields */
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchMfgLocation()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchMfgLocation())) {
				sbFieldQuery.append("QRD_LT_MTOMFGLOCATION = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchMfgLocation() + "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchProdLine()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchProdLine())) {
				sbFieldQuery.append("QRD_LT_MTOPRODLINEID = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchProdLine()+ "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchKW()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchKW())) {
				sbFieldQuery.append("QRD_LT_MTOKW = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchKW()+ "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchPole()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchPole())) {
				sbFieldQuery.append("QRD_LT_MTOPOLE = ");
				sbFieldQuery.append(Integer.parseInt(oNewEnquiryDto.getSearchPole()) + " AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchFrame()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchFrame())) {
				sbFieldQuery.append("QRD_LT_MTOFRAME = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchFrame()+ "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchFrameSuffix()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchFrameSuffix())) {
				sbFieldQuery.append("QRD_LT_MTOFRAME_SUFFIX = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchFrameSuffix()+ "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchMounting()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchMounting())) {
				sbFieldQuery.append("QRD_LT_MTOMOUNTINGID = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchMounting()+ "' AND ");
			}
			if (StringUtils.isNotBlank(oNewEnquiryDto.getSearchTBPosition()) && !QuotationConstants.QUOTATION_ZERO.equals(oNewEnquiryDto.getSearchTBPosition())) {
				sbFieldQuery.append("QRD_LT_MTOTBPOSITIONID = ");
				sbFieldQuery.append("'"+oNewEnquiryDto.getSearchTBPosition()+ "' AND ");
			}
			
			// Remove Last AND String in Query.
			if (sbFieldQuery.toString().trim().endsWith("AND"))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf("AND")));
		}
		
		//System.out.println("sbFieldQuery ......" + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sbFieldQuery.toString();
	}
	
	@Override
	public Map<String,String> fetchMTOAddOnMap(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchMTOAddOnMap";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        Map<String,String> hmCompleteAddOnsMap = new HashMap<String,String>();
        
        try {
        	pstmt1 = conn.prepareStatement(GET_DESIGN_ADDONLIST);
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			
    			hmCompleteAddOnsMap.put(rs1.getString("RAD_ID"), rs1.getString("RAD_SPECIAL_FEATURE") + " - " + rs1.getString("RAD_SPECIAL_VALUE"));
    			
    		}
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return hmCompleteAddOnsMap;
	}
	
	@Override
	public List<KeyValueVo> fetchMTOReassignUsersList(Connection conn) throws Exception {
		String sMethodName = "fetchMTOReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        List<KeyValueVo> alReassignUsersList = new ArrayList<>();
        KeyValueVo oKeyValueVo = null;
        
        try {
        	pstmt1 = conn.prepareStatement(FETCH_USER_DETAILS);
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oKeyValueVo = new KeyValueVo();
    			oKeyValueVo.setKey(rs1.getString("QUD_LT_USER_SSO"));
    			oKeyValueVo.setValue(rs1.getString("USER_FULLNAME"));
    			
    			alReassignUsersList.add(oKeyValueVo);
    		}
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return alReassignUsersList;
	}
	
	@Override
	public void saveReassignComments(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "saveReassignComments";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	pstmt1 = conn.prepareStatement(UPDATE_NEWENQUIRY_REASSIGN_COMMENTS);
			pstmt1.setString(1, oNewEnquiryDto.getReassignRemarks());
			pstmt1.setInt(2, oNewEnquiryDto.getEnquiryId());
			
			pstmt1.executeUpdate();
			
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt1);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	@Override
	public String updateDesignFieldsOnReturn(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "updateDesignFieldsOnReturn";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sResult = QuotationConstants.QUOTATION_STRING_Y;
        try {
        	pstmt1 = conn.prepareStatement(UPDATE_MTORATING_RETURNTOSM);
        	pstmt1.setString(1, oNewEnquiryDto.getDesignReferenceNumber());
        	pstmt1.setString(2, oNewEnquiryDto.getCommentsDeviations());
        	pstmt1.setString(3, oNewEnquiryDto.getDesignNote());
        	pstmt1.setString(4, oNewEnquiryDto.getQaNote());
        	pstmt1.setString(5, oNewEnquiryDto.getScmNote());
        	pstmt1.setString(6, oNewEnquiryDto.getDmUpdateStatus());
        	pstmt1.setInt(7, oNewEnquiryDto.getEnquiryId());
        	int iResult = pstmt1.executeUpdate();
        	
        	if(iResult == 0) {
        		sResult = QuotationConstants.QUOTATION_STRING_N;
        	}
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt1);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return sResult;
	}
	
	@Override
	public boolean verifyMTOEnquiryDesignComplete(Connection conn, String sEnquiryId) throws Exception {
		String sMethodName = "verifyMTOEnquiryDesignComplete";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs1 = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean isMTOEnquiryDesignComplete = true;
        String sDesignCompleteVal = "";
        
        try { 
        	pstmt1 = conn.prepareStatement(FETCH_MTOENQUIRY_DESIGNCOMPLETE_DETAILS);
        	pstmt1.setInt(1, Integer.parseInt(sEnquiryId));
        	rs1 = pstmt1.executeQuery();
        	
        	while(rs1.next()) {
        		sDesignCompleteVal = rs1.getString("QRD_LT_MTO_ISDESIGNCOMPLETE");
        		
        		if(StringUtils.isBlank(sDesignCompleteVal) || QuotationConstants.QUOTATION_STRING_NO.equalsIgnoreCase(sDesignCompleteVal.trim()) || QuotationConstants.QUOTATION_ZERO.equalsIgnoreCase(sDesignCompleteVal.trim())) {
        			isMTOEnquiryDesignComplete = false;
        			break;
        		}
        	}
        	
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return isMTOEnquiryDesignComplete;
	}
	
	@Override
	public String saveMTORatingDetails(Connection conn, ArrayList<NewRatingDto> alMTORatingsList) throws Exception {
		String sMethodName = "saveMTORatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		PreparedStatement pstmt3 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 1, iRatingId = 0, iCountVal = 0, iInsertRes = 0, iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>();
		ArrayList<Integer> alInsertResult = new ArrayList<>();
		ArrayList<Integer> alUpdateResult = new ArrayList<>();
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLRatingQuery = null;
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_RATINGID = ? AND QRD_LT_ENQUIRYID = ?";
		pstmt = conn.prepareStatement(sChkQuery);
		
		try {
			for(int iCount = 0; iCount < alMTORatingsList.size(); iCount++) {
				oTempNewRatingDto = (NewRatingDto) alMTORatingsList.get(iCount);
				
				pstmt.setInt(1, oTempNewRatingDto.getRatingId());
				pstmt.setInt(2, oTempNewRatingDto.getEnquiryId());
				rs = pstmt.executeQuery();
				
				if(rs.next()) {		// UPDATE
					iRatingId = oTempNewRatingDto.getRatingId();
					sSQLRatingQuery = buildMTORatingUpdateQuery(oTempNewRatingDto, iRatingId);
					pstmt1 = conn.prepareStatement(sSQLRatingQuery);
					iUpdateRes = pstmt1.executeUpdate();
					alUpdateResult.add(iUpdateRes);
				} else {			// INSERT
					pstmt2 = conn.prepareStatement(FETCH_LT_RATINGID_NEXTVAL);	    
		    		rs1 = pstmt2.executeQuery();
		    		if (rs1.next()) {
		    			iRatingId = Integer.parseInt(rs1.getString("RATING_ID"));
		    		}
		    		oDBUtility.releaseResources(rs1, pstmt2);
		    		
		    		sSQLRatingQuery = buildMTORatingInsertQuery(oTempNewRatingDto, iRatingId);
		    		pstmt3 = conn.prepareStatement(sSQLRatingQuery);
		    		iInsertRes = pstmt3.executeUpdate();
		    		alInsertResult.add(iInsertRes);
				}
				oDBUtility.releaseResources(rs);
			}
			if(alInsertResult.size() > 0 ) {
				for(Integer iInsertVal : alInsertResult) {
					if(iInsertVal == 0) {
						sResult = QuotationConstants.QUOTATION_STRING_N;
						break;
					}
				}
			}
			if(alUpdateResult.size() > 0 ) {
				for(Integer iUpdateVal : alUpdateResult) {
					if(iUpdateVal == 0) {
						sResult = QuotationConstants.QUOTATION_STRING_N;
						break;
					}
				}
			}
			
		} finally {
			oDBUtility.releaseResources(pstmt);
			oDBUtility.releaseResources(pstmt1);
            oDBUtility.releaseResources(pstmt2);
		}
		
		return sResult;
	}
	
	private String buildMTORatingUpdateQuery(NewRatingDto oNewRatingDto, int iRatingId) {
		String sMethodName = "buildMTORatingUpdateQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		StringBuffer sbFieldQuery = new StringBuffer();
		
		if (oNewRatingDto != null) {
			sbFieldQuery.append(UPDATE_MTORATING_DETAILS);
			
			/* QRD_QUANTITY */
			sbFieldQuery.append("QRD_LT_QUANTITY = ");
			sbFieldQuery.append(oNewRatingDto.getQuantity() + ", ");
			
			if (StringUtils.isNotBlank(oNewRatingDto.getModelNumber()) ) {
				sbFieldQuery.append("QRD_LT_MODELNUMBER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getModelNumber() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtApplicationId()) ) {
				sbFieldQuery.append("QRD_LT_APPLICATIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtApplicationId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtEffClass()) ) {
				sbFieldQuery.append("QRD_LT_EFFICIENCYCLASS = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEffClass() + "', ");
			}
			
			/* ********************************************  Basic Fields Section ******************************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtReplacementMotor()) ) {
				sbFieldQuery.append("QRD_LT_REPLACEMENTMOTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEffClass() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtEarlierMotorSerialNo()) ) {
				sbFieldQuery.append("QRD_LT_EARLIER_SUPPLIED_MOTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEarlierMotorSerialNo() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtEnclosure()) ) {
				sbFieldQuery.append("QRD_LT_ENCLOSURE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEnclosure() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtForcedCoolingId()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFCOOLING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtForcedCoolingId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVoltId()) ) {
				sbFieldQuery.append("QRD_LT_VOLTAGEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVoltAddVariation()) ) {
				sbFieldQuery.append("QRD_LT_VOLTADDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltAddVariation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVoltRemoveVariation()) ) {
				sbFieldQuery.append("QRD_LT_VOLTREMOVEVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltRemoveVariation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFreqId()) ) {
				sbFieldQuery.append("QRD_LT_FREQUENCYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFreqAddVariation()) ) {
				sbFieldQuery.append("QRD_LT_FREQADDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqAddVariation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFreqRemoveVariation()) ) {
				sbFieldQuery.append("QRD_LT_FREQREMOVEVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqRemoveVariation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCombVariation()) ) {
				sbFieldQuery.append("QRD_LT_COMBINEDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCombVariation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHazardAreaTypeId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDAREATYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardAreaTypeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHazardAreaId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDAREAID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardAreaId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtGasGroupId()) ) {
				sbFieldQuery.append("QRD_LT_GASGROUPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtGasGroupId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHazardZoneId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDZONEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardZoneId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDustGroupId()) ) {
				sbFieldQuery.append("QRD_LT_DUSTGROUPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDustGroupId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTempClass()) ) {
				sbFieldQuery.append("QRD_LT_TEMPCLASSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTempClass() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempId()) ) {
				sbFieldQuery.append("QRD_LT_AMBTEMPADDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAmbTempId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempRemoveId()) ) {
				sbFieldQuery.append("QRD_LT_AMBTEMPREMOVEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAmbTempRemoveId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtInsulationClassId()) ) {
				sbFieldQuery.append("QRD_LT_INSULATIONCLASSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtInsulationClassId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTempRise()) ) {
				sbFieldQuery.append("QRD_LT_TEMPRISEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTempRise() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAltitude()) ) {
				sbFieldQuery.append("QRD_LT_ALTITUDE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAltitude() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtIPId()) ) {
				sbFieldQuery.append("QRD_LT_IPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtIPId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfStarting()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFSTARTINGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMethodOfStarting() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDirectionOfRotation()) ) {
				sbFieldQuery.append("QRD_LT_ROTATIONDIRECTIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDirectionOfRotation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtStandardRotation()) ) {
				sbFieldQuery.append("QRD_LT_STANDARDROTATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStandardRotation() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtGreaseType()) ) {
				sbFieldQuery.append("QRD_LT_GREASETYPE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtGreaseType() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintingTypeId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintingTypeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTSHADEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintShadeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeValue()) ) {
				sbFieldQuery.append("QRD_LT_PAINTSHADE_VALUE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintShadeValue() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintThicknessId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTTHICKESSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintThicknessId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCableSizeId()) ) {
				sbFieldQuery.append("QRD_LT_CABLESIZEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableSizeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtNoOfRuns()) ) {
				sbFieldQuery.append("QRD_LT_NOOFRUNSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoOfRuns() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtNoOfCores()) ) {
				sbFieldQuery.append("QRD_LT_NOOFCORESID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoOfCores() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCrossSectionAreaId()) ) {
				sbFieldQuery.append("QRD_LT_CROSSSECTIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCrossSectionAreaId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtConductorMaterialId()) ) {
				sbFieldQuery.append("QRD_LT_CONDMATERIALID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtConductorMaterialId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCableDiameter()) ) {
				sbFieldQuery.append("QRD_LT_CABLEDIAMETER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableDiameter() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtNoiseLevel()) ) {
				sbFieldQuery.append("QRD_LT_NOISELEVEL = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoiseLevel() + "', ");
			}
			/* ********************************************  Basic Fields Section ******************************************** */
			
			/* ********************************************  Electrical Fields Section *************************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWindingConfig()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGCONFIGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingConfig() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWindingWire()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGWIREID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingWire() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWindingTreatmentId()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGTREATMENTID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingTreatmentId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDutyId()) ) {
				sbFieldQuery.append("QRD_LT_DUTYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDutyId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCDFId()) ) {
				sbFieldQuery.append("QRD_LT_CDFID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCDFId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtStartsId()) ) {
				sbFieldQuery.append("QRD_LT_STARTSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStartsId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtOverloadingDuty()) ) {
				sbFieldQuery.append("QRD_LT_OVERLOADDUTYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtOverloadingDuty() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtServiceFactor()) ) {
				sbFieldQuery.append("QRD_LT_SERVICEFACTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtServiceFactor() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBroughtOutTerminals()) ) {
				sbFieldQuery.append("QRD_LT_BROUGHTOUTTERMINALS = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBroughtOutTerminals() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLeadId()) ) {
				sbFieldQuery.append("QRD_LT_LEADID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLeadId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtStartingCurrentOnDOLStart()) ) {
				sbFieldQuery.append("QRD_LT_STARTINGCURRENTDOLID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStartingCurrentOnDOLStart() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDApplTypeId()) ) {
				sbFieldQuery.append("QRD_LT_VFDTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDApplTypeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMin()) ) {
				sbFieldQuery.append("QRD_LT_VFDSPEEDMINID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMin() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMax()) ) {
				sbFieldQuery.append("QRD_LT_VFDSPEEDMAXID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMax() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtConstantEfficiencyRange()) ) {
				sbFieldQuery.append("QRD_LT_CONSTEFFRANGEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtConstantEfficiencyRange() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRVId()) ) {
				sbFieldQuery.append("QRD_LT_RV = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRVId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRAId()) ) {
				sbFieldQuery.append("QRD_LT_RA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRAId() + "', ");
			}
			/* ********************************************  Electrical Fields Section *************************************** */
			
			/* ********************************************  Mechanical Fields Section *************************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfCoupling()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFCOUPLINGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMethodOfCoupling() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMainTB()) ) {
				sbFieldQuery.append("QRD_LT_MAINTERMBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMainTB() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTerminalBoxSizeId()) ) {
				sbFieldQuery.append("QRD_LT_TERMINALBOXSIZEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTerminalBoxSizeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAuxTerminalBoxId()) ) {
				sbFieldQuery.append("QRD_LT_AUXTERMINALBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAuxTerminalBoxId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftTypeId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftTypeId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftMaterialId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTMATERIALID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftMaterialId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftGroundingId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTGROUNDING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftGroundingId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingSystemId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGSYSTEMID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingSystemId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingNDEId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGNDEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingNDEId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingNDESize()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGNDESIZE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingNDESize() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingDEId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGDEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingDEId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingDESize()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGDESIZE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingDESize() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCableEntry()) ) {
				sbFieldQuery.append("QRD_LT_CABLEENTRY = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableEntry() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLubrication()) ) {
				sbFieldQuery.append("QRD_LT_LUBRICATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLubrication() + "', ");
			}
			/* ********************************************  Mechanical Fields Section *************************************** */
			
			/* ********************************************  Performance Fields Section ************************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfRatedSpeed()) ) {
				sbFieldQuery.append("QRD_LT_RATEDSPEED = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfRatedSpeed() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfRatedCurr()) ) {
				sbFieldQuery.append("QRD_LT_RATEDCURRENT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfRatedCurr() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfNoLoadCurr()) ) {
				sbFieldQuery.append("QRD_LT_NOLOADCURRENT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfNoLoadCurr() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfLockRotorCurr()) ) {
				sbFieldQuery.append("QRD_LT_LOCKROTORCURRENT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfLockRotorCurr() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfNominalTorque()) ) {
				sbFieldQuery.append("QRD_LT_NOMINALTORQUE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfNominalTorque() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfMaxTorque()) ) {
				sbFieldQuery.append("QRD_LT_MAXIMUMTORQUE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfMaxTorque() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfLockRotorTorque()) ) {
				sbFieldQuery.append("QRD_LT_LOCKROTORTORQUE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfLockRotorTorque() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfSSTHot()) ) {
				sbFieldQuery.append("QRD_LT_SSTHOT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfSSTHot() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfSSTCold()) ) {
				sbFieldQuery.append("QRD_LT_SSTCOLD = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfSSTCold() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfStartTimeRatedVolt()) ) {
				sbFieldQuery.append("QRD_LT_STARTTIME_VOLT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfStartTimeRatedVolt() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfStartTime80RatedVolt()) ) {
				sbFieldQuery.append("QRD_LT_STARTTIME_VOLTAT80 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfStartTime80RatedVolt() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMotorGD2Value()) ) {
				sbFieldQuery.append("QRD_LT_MOTORGD2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMotorGD2Value() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadGD2Value()) ) {
				sbFieldQuery.append("QRD_LT_LOADGD2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadGD2Value() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVibrationId()) ) {
				sbFieldQuery.append("QRD_LT_VIBRATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVibrationId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMotorWeight()) ) {
				sbFieldQuery.append("QRD_LT_TOTALMOTORWEIGHT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMotorWeight() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercent100Above()) ) {
				sbFieldQuery.append("QRD_LT_LOADEFF100ABOVE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadEffPercent100Above() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt100()) ) {
				sbFieldQuery.append("QRD_LT_LOADEFFAT100 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt100() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt75()) ) {
				sbFieldQuery.append("QRD_LT_LOADEFFAT75 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt75() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt50()) ) {
				sbFieldQuery.append("QRD_LT_LOADEFFAT50 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt50() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt100()) ) {
				sbFieldQuery.append("QRD_LT_LOADPFAT100 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt100() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt75()) ) {
				sbFieldQuery.append("QRD_LT_LOADPFAT75 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt75() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt50()) ) {
				sbFieldQuery.append("QRD_LT_LOADPFAT50 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt50() + "', ");
			}
			/* ********************************************  Performance Fields Section ************************************** */
			
			/* ********************************************  Accessories Fields Section ************************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithoutTDId()) ) {
				sbFieldQuery.append("QRD_LT_FLYINGLEADWITHOUTTB = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithoutTDId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithTDId()) ) {
				sbFieldQuery.append("QRD_LT_FLYINGLEADWITHTB = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithTDId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHardware()) ) {
				sbFieldQuery.append("QRD_LT_HARDWARE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHardware() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpaceHeaterId()) ) {
				sbFieldQuery.append("QRD_LT_SPACEHEATER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpaceHeaterId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtThermisterId()) ) {
				sbFieldQuery.append("QRD_LT_THERMISTERID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtThermisterId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRTDId()) ) {
				sbFieldQuery.append("QRD_LT_RTDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRTDId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBTDId()) ) {
				sbFieldQuery.append("QRD_LT_BTDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBTDId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpreaderBoxId()) ) {
				sbFieldQuery.append("QRD_LT_SPREADERBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpreaderBoxId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCableSealingBoxId()) ) {
				sbFieldQuery.append("QRD_LT_CABLESEALINGBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableSealingBoxId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtGlandPlateId()) ) {
				sbFieldQuery.append("QRD_LT_GLANDPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtGlandPlateId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDoubleCompressionGlandId()) ) {
				sbFieldQuery.append("QRD_LT_DOUBLECOMPRESSION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDoubleCompressionGlandId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDirectionArrowPlateId()) ) {
				sbFieldQuery.append("QRD_LT_DIRECTIONARROWPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDirectionArrowPlateId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAddNamePlateId()) ) {
				sbFieldQuery.append("QRD_LT_ADDLNAMEPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAddNamePlateId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVibrationProbe()) ) {
				sbFieldQuery.append("QRD_LT_VIBRPROBEMOUNTPAD = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVibrationProbe() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSPMMountingProvisionId()) ) {
				sbFieldQuery.append("QRD_LT_SPMMOUNTING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSPMMountingProvisionId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTechoMounting()) ) {
				sbFieldQuery.append("QRD_LT_TECHOMOUNTING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTechoMounting() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMetalFanId()) ) {
				sbFieldQuery.append("QRD_LT_METALFAN = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMetalFanId() + "', ");
			}
			/* ********************************************  Accessories Fields Section ************************************** */
			
			/* ********************************************  Certificates/Spares Fields Section ****************************** */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWitnessRoutineId()) ) {
				sbFieldQuery.append("QRD_LT_WITNESSROUTINE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWitnessRoutineId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest1Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST1 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest1Id() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest2Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest2Id() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest3Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST3 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest3Id() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTypeTestId()) ) {
				sbFieldQuery.append("QRD_LT_TYPETEST = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTypeTestId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtULCEId()) ) {
				sbFieldQuery.append("QRD_LT_CERTIFICATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtULCEId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtQAPId()) ) {
				sbFieldQuery.append("QRD_LT_QAPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtQAPId() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares1()) ) {
				sbFieldQuery.append("QRD_LT_SPARES1 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares1() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares2()) ) {
				sbFieldQuery.append("QRD_LT_SPARES2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares2() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares3()) ) {
				sbFieldQuery.append("QRD_LT_SPARES3 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares3() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares4()) ) {
				sbFieldQuery.append("QRD_LT_SPARES4 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares4() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares5()) ) {
				sbFieldQuery.append("QRD_LT_SPARES5 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares5() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDataSheet()) ) {
				sbFieldQuery.append("QRD_LT_DATASHEET = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDataSheet() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMotorGA()) ) {
				sbFieldQuery.append("QRD_LT_MOTORGA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMotorGA() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfCurves()) ) {
				sbFieldQuery.append("QRD_LT_PERFCURVE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfCurves() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTBoxGA()) ) {
				sbFieldQuery.append("QRD_LT_TBOXGA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTBoxGA() + "', ");
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalComments()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_COMMENTS = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalComments() + "', ");
			}
			/* ********************************************  Certificates/Spares Fields Section ****************************** */
			
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

			sbFieldQuery.append(" WHERE QRD_LT_RATINGID = " + oNewRatingDto.getRatingId());
			
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sbFieldQuery.toString();
	}
	
	
	private String buildMTORatingInsertQuery(NewRatingDto oNewRatingDto, int iRatingId) {
		String sMethodName = "buildMTORatingInsertQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		String sInsertQuery = null;
		StringBuffer sbFieldQuery = new StringBuffer();
		StringBuffer sbValuesQuery = new StringBuffer();
		
		if (oNewRatingDto != null) {
			sbFieldQuery.append(INSERT_NEWRATING_DETAILS);
			
			sbFieldQuery.append("(");
			sbValuesQuery.append(" VALUES (");
			
			/* Rating ID */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "RatingId -  " + iRatingId);
			sbFieldQuery.append("QRD_LT_RATINGID, ");
			sbValuesQuery.append(iRatingId + ", ");
			/* Enquiry Id */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Enquiry Id -  " + oNewRatingDto.getEnquiryId());
			sbFieldQuery.append("QRD_LT_ENQUIRYID, ");
			sbValuesQuery.append(oNewRatingDto.getEnquiryId() + ", ");
			/* Rating no. */
			sbFieldQuery.append("QRD_LT_RATINGNO, ");
			sbValuesQuery.append(oNewRatingDto.getRatingNo() + ", ");
			/* Quantity */
			sbFieldQuery.append("QRD_LT_QUANTITY, ");
			sbValuesQuery.append(oNewRatingDto.getQuantity() + ", ");
			
			
			/* ********************************************  Top Fields Section ********************************************** */
			sbFieldQuery.append("QRD_LT_PRODLINEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtProdLineId() + "', ");
			sbFieldQuery.append("QRD_LT_KW, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtKWId() + "', ");
			sbFieldQuery.append("QRD_LT_POLE, ");
			sbValuesQuery.append(Integer.parseInt(oNewRatingDto.getLtPoleId()) + ", ");
			sbFieldQuery.append("QRD_LT_FRAME, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFrameId() + "', ");
			sbFieldQuery.append("QRD_LT_FRAME_SUFFIX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFrameSuffixId() + "', ");
			sbFieldQuery.append("QRD_LT_MOUNTINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMountingId() + "', ");
			sbFieldQuery.append("QRD_LT_TBPOSITIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTBPositionId() + "', ");
			sbFieldQuery.append("QRD_LT_EFFICIENCYCLASS, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtEffClass() + "', ");
			sbFieldQuery.append("QRD_LT_APPLICATIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtApplicationId() + "', ");
			sbFieldQuery.append("QRD_LT_MODELNUMBER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getModelNumber() + "', ");
			/* ********************************************  Top Fields Section ********************************************** */
			
			/* ********************************************  Basic Fields Section ******************************************** */
			sbFieldQuery.append("QRD_LT_REPLACEMENTMOTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtReplacementMotor() + "', ");
			sbFieldQuery.append("QRD_LT_EARLIER_SUPPLIED_MOTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtEarlierMotorSerialNo() + "', ");
			sbFieldQuery.append("QRD_LT_ENCLOSURE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtEnclosure() + "', ");
			sbFieldQuery.append("QRD_LT_METHODOFCOOLING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtForcedCoolingId() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTAGEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltId() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTADDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltAddVariation() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTREMOVEVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltRemoveVariation() + "', ");
			sbFieldQuery.append("QRD_LT_FREQUENCYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqId() + "', ");
			sbFieldQuery.append("QRD_LT_FREQADDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqAddVariation() + "', ");
			sbFieldQuery.append("QRD_LT_FREQREMOVEVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqRemoveVariation() + "', ");
			sbFieldQuery.append("QRD_LT_COMBINEDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCombVariation() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDAREATYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardAreaTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDAREAID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardAreaId() + "', ");
			sbFieldQuery.append("QRD_LT_GASGROUPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtGasGroupId() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDZONEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardZoneId() + "', ");
			sbFieldQuery.append("QRD_LT_DUSTGROUPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDustGroupId() + "', ");
			sbFieldQuery.append("QRD_LT_TEMPCLASSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTempClass() + "', ");
			sbFieldQuery.append("QRD_LT_AMBTEMPADDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAmbTempId() + "', ");
			sbFieldQuery.append("QRD_LT_AMBTEMPREMOVEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAmbTempRemoveId() + "', ");
			sbFieldQuery.append("QRD_LT_INSULATIONCLASSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtInsulationClassId() + "', ");
			sbFieldQuery.append("QRD_LT_TEMPRISEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTempRise() + "', ");
			sbFieldQuery.append("QRD_LT_ALTITUDE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAltitude() + "', ");
			sbFieldQuery.append("QRD_LT_IPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtIPId() + "', ");
			sbFieldQuery.append("QRD_LT_METHODOFSTARTINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMethodOfStarting() + "', ");
			sbFieldQuery.append("QRD_LT_ROTATIONDIRECTIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDirectionOfRotation() + "', ");
			sbFieldQuery.append("QRD_LT_STANDARDROTATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStandardRotation() + "', ");
			sbFieldQuery.append("QRD_LT_GREASETYPE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtGreaseType() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintingTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTSHADEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintShadeId() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTSHADE_VALUE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintShadeValue() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTTHICKESSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintThicknessId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLESIZEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableSizeId() + "', ");
			sbFieldQuery.append("QRD_LT_NOOFRUNSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoOfRuns() + "', ");
			sbFieldQuery.append("QRD_LT_NOOFCORESID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoOfCores() + "', ");
			sbFieldQuery.append("QRD_LT_CROSSSECTIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCrossSectionAreaId() + "', ");
			sbFieldQuery.append("QRD_LT_CONDMATERIALID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtConductorMaterialId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLEDIAMETER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableDiameter() + "', ");
			sbFieldQuery.append("QRD_LT_NOISELEVEL, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoiseLevel() + "', ");
			/* ********************************************  Basic Fields Section ******************************************** */
			
			/* ********************************************  Electrical Fields Section *************************************** */
			sbFieldQuery.append("QRD_LT_WINDINGCONFIGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingConfig() + "', ");
			sbFieldQuery.append("QRD_LT_WINDINGWIREID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingWire() + "', ");
			sbFieldQuery.append("QRD_LT_WINDINGTREATMENTID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingTreatmentId() + "', ");
			sbFieldQuery.append("QRD_LT_DUTYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDutyId() + "', ");
			sbFieldQuery.append("QRD_LT_CDFID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCDFId() + "', ");
			sbFieldQuery.append("QRD_LT_STARTSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStartsId() + "', ");
			sbFieldQuery.append("QRD_LT_OVERLOADDUTYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtOverloadingDuty() + "', ");
			sbFieldQuery.append("QRD_LT_SERVICEFACTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtServiceFactor() + "', ");
			sbFieldQuery.append("QRD_LT_BROUGHTOUTTERMINALS, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBroughtOutTerminals() + "', ");
			sbFieldQuery.append("QRD_LT_LEADID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLeadId() + "', ");
			sbFieldQuery.append("QRD_LT_STARTINGCURRENTDOLID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStartingCurrentOnDOLStart() + "', ");
			sbFieldQuery.append("QRD_LT_VFDTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDApplTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_VFDSPEEDMINID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMin() + "', ");
			sbFieldQuery.append("QRD_LT_VFDSPEEDMAXID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMax() + "', ");
			sbFieldQuery.append("QRD_LT_CONSTEFFRANGEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtConstantEfficiencyRange() + "', ");
			sbFieldQuery.append("QRD_LT_RV, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRVId() + "', ");
			sbFieldQuery.append("QRD_LT_RA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRAId() + "', ");
			/* ********************************************  Electrical Fields Section *************************************** */
			
			/* ********************************************  Mechanical Fields Section *************************************** */
			sbFieldQuery.append("QRD_LT_METHODOFCOUPLINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMethodOfCoupling() + "', ");
			sbFieldQuery.append("QRD_LT_MAINTERMBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMainTB() + "', ");
			sbFieldQuery.append("QRD_LT_TERMINALBOXSIZEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTerminalBoxSizeId() + "', ");
			sbFieldQuery.append("QRD_LT_AUXTERMINALBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAuxTerminalBoxId() + "', ");
			sbFieldQuery.append("QRD_LT_SHAFTTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_SHAFTMATERIALID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftMaterialId() + "', ");
			sbFieldQuery.append("QRD_LT_SHAFTGROUNDING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftGroundingId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGSYSTEMID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingSystemId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGNDEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingNDEId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGNDESIZE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingNDESize() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGDEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingDEId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGDESIZE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingDESize() + "', ");
			sbFieldQuery.append("QRD_LT_CABLEENTRY, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableEntry() + "', ");
			sbFieldQuery.append("QRD_LT_LUBRICATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLubrication() + "', ");
			/* ********************************************  Mechanical Fields Section *************************************** */
			
			/* ********************************************  Performance Fields Section ************************************** */
			sbFieldQuery.append("QRD_LT_RATEDSPEED, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfRatedSpeed() + "', ");
			sbFieldQuery.append("QRD_LT_RATEDCURRENT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfRatedCurr() + "', ");
			sbFieldQuery.append("QRD_LT_NOLOADCURRENT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfNoLoadCurr() + "', ");
			sbFieldQuery.append("QRD_LT_LOCKROTORCURRENT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfLockRotorCurr() + "', ");
			sbFieldQuery.append("QRD_LT_NOMINALTORQUE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfNominalTorque() + "', ");
			sbFieldQuery.append("QRD_LT_MAXIMUMTORQUE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfMaxTorque() + "', ");
			sbFieldQuery.append("QRD_LT_LOCKROTORTORQUE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfLockRotorTorque() + "', ");
			sbFieldQuery.append("QRD_LT_SSTHOT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfSSTHot() + "', ");
			sbFieldQuery.append("QRD_LT_SSTCOLD, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfSSTCold() + "', ");
			sbFieldQuery.append("QRD_LT_STARTTIME_VOLT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfStartTimeRatedVolt() + "', ");
			sbFieldQuery.append("QRD_LT_STARTTIME_VOLTAT80, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfStartTime80RatedVolt() + "', ");
			sbFieldQuery.append("QRD_LT_MOTORGD2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMotorGD2Value() + "', ");
			sbFieldQuery.append("QRD_LT_LOADGD2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadGD2Value() + "', ");
			sbFieldQuery.append("QRD_LT_VIBRATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVibrationId() + "', ");
			sbFieldQuery.append("QRD_LT_TOTALMOTORWEIGHT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMotorWeight() + "', ");
			sbFieldQuery.append("QRD_LT_LOADEFF100ABOVE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadEffPercent100Above() + "', ");
			sbFieldQuery.append("QRD_LT_LOADEFFAT100, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt100() + "', ");
			sbFieldQuery.append("QRD_LT_LOADEFFAT75, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt75() + "', ");
			sbFieldQuery.append("QRD_LT_LOADEFFAT50, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadEffPercentAt50() + "', ");
			sbFieldQuery.append("QRD_LT_LOADPFAT100, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt100() + "', ");
			sbFieldQuery.append("QRD_LT_LOADPFAT75, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt75() + "', ");
			sbFieldQuery.append("QRD_LT_LOADPFAT50, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadPFPercentAt50() + "', ");
			/* ********************************************  Performance Fields Section ************************************** */
			
			/* ********************************************  Accessories Fields Section ************************************** */
			sbFieldQuery.append("QRD_LT_FLYINGLEADWITHOUTTB, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithoutTDId() + "', ");
			sbFieldQuery.append("QRD_LT_FLYINGLEADWITHTB, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithTDId() + "', ");
			sbFieldQuery.append("QRD_LT_HARDWARE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHardware() + "', ");
			sbFieldQuery.append("QRD_LT_SPACEHEATER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpaceHeaterId() + "', ");
			sbFieldQuery.append("QRD_LT_THERMISTERID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtThermisterId() + "', ");
			sbFieldQuery.append("QRD_LT_RTDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRTDId() + "', ");
			sbFieldQuery.append("QRD_LT_BTDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBTDId() + "', ");
			sbFieldQuery.append("QRD_LT_SPREADERBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpreaderBoxId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLESEALINGBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableSealingBoxId() + "', ");
			sbFieldQuery.append("QRD_LT_GLANDPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtGlandPlateId() + "', ");
			sbFieldQuery.append("QRD_LT_DOUBLECOMPRESSION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDoubleCompressionGlandId() + "', ");
			sbFieldQuery.append("QRD_LT_DIRECTIONARROWPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDirectionArrowPlateId() + "', ");
			sbFieldQuery.append("QRD_LT_ADDLNAMEPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAddNamePlateId() + "', ");
			sbFieldQuery.append("QRD_LT_VIBRPROBEMOUNTPAD, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVibrationProbe() + "', ");
			sbFieldQuery.append("QRD_LT_SPMMOUNTING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSPMMountingProvisionId() + "', ");
			sbFieldQuery.append("QRD_LT_TECHOMOUNTING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTechoMounting() + "', ");
			sbFieldQuery.append("QRD_LT_METALFAN, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMetalFanId() + "', ");
			/* ********************************************  Accessories Fields Section ************************************** */
			
			/* ********************************************  Certificates/Spares Fields Section ****************************** */
			sbFieldQuery.append("QRD_LT_WITNESSROUTINE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWitnessRoutineId() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST1, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest1Id() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest2Id() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST3, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest3Id() + "', ");
			sbFieldQuery.append("QRD_LT_TYPETEST, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTypeTestId() + "', ");
			sbFieldQuery.append("QRD_LT_CERTIFICATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtULCEId() + "', ");
			sbFieldQuery.append("QRD_LT_QAPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtQAPId() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES1, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares1() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares2() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES3, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares3() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES4, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares4() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES5, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares5() + "', ");
			sbFieldQuery.append("QRD_LT_DATASHEET, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDataSheet() + "', ");
			sbFieldQuery.append("QRD_LT_MOTORGA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMotorGA() + "', ");
			sbFieldQuery.append("QRD_LT_PERFCURVE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfCurves() + "', ");
			sbFieldQuery.append("QRD_LT_TBOXGA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTBoxGA() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_COMMENTS, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalComments() + "', ");
			
			/* ********************************************  Certificates/Spares Fields Section ****************************** */
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

			if (sbValuesQuery.toString().trim().endsWith(","))
				sbValuesQuery = new StringBuffer(sbValuesQuery.toString().trim().substring(0, sbValuesQuery.toString().trim().lastIndexOf(",")));

			sbFieldQuery.append(")");
			sbValuesQuery.append(")");
			
			sInsertQuery = sbFieldQuery.toString() + sbValuesQuery.toString();
		}
		
		//System.out.println("sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		//System.out.println("sbValuesQuery ...... " + sbValuesQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbValuesQuery ...... " + sbValuesQuery);
		//System.out.println("Final Query -  " + sInsertQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Final Query -  " + sInsertQuery);
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sInsertQuery;
	}

	@Override
	public NewEnquiryDto getMTOEnquiryDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception {
		String sMethodName = "getMTOEnquiryDetailsForView";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_LT_ENQUIRYDETAILS_FROMVIEW);
        	pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	
        	if (rs != null && rs.next()) {
        		oNewEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));   
        		oNewEnquiryDto.setRevisionNumber(rs.getInt("QEM_REVISIONNO"));
        		oNewEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
        		oNewEnquiryDto.setCustomerType(rs.getString("QEM_CUSTOMERTYPE"));
        		oNewEnquiryDto.setCustomerId(rs.getInt("QEM_CUSTOMERID"));
        		oNewEnquiryDto.setLocationId(rs.getInt("QEM_LOCATIONID"));
        		oNewEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME"));
        		oNewEnquiryDto.setConcernedPerson(rs.getString("QEM_CONCERNED_PERSON"));
        		oNewEnquiryDto.setProjectName(rs.getString("QEM_PROJECTNAME"));
        		oNewEnquiryDto.setIs_marathon_approved(rs.getString("QEM_IS_MARATHON_APPRV"));
        		oNewEnquiryDto.setConsultantName(rs.getString("QEM_CONSULTANTNAME"));
        		oNewEnquiryDto.setEndUser(rs.getString("QEM_ENDUSER"));
        		oNewEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY"));
        		oNewEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPE"));
        		oNewEnquiryDto.setWinningChance(rs.getString("QEM_WINCHANCE"));
        		oNewEnquiryDto.setTargeted(rs.getString("QEM_TARGETED"));
        		oNewEnquiryDto.setRemarks(rs.getString("QEM_REMARKS"));
        		
        		if(StringUtils.isNotBlank(rs.getString("QEM_ENQRECEIPTMNTH"))) {
        			oNewEnquiryDto.setReceiptDate(CommonUtility.buildDateString(rs.getString("QEM_ENQRECEIPTMNTH")));
        		}else {
        			oNewEnquiryDto.setReceiptDate("");
        		}
        		if(StringUtils.isNotBlank(rs.getString("QEM_REQOFFERMNTH"))) {
        			oNewEnquiryDto.setSubmissionDate(CommonUtility.buildDateString(rs.getString("QEM_REQOFFERMNTH")));
        		}else {
        			oNewEnquiryDto.setSubmissionDate("");
        		}
        		if(StringUtils.isNotBlank(rs.getString("QEM_ORDCLOSEMNTH"))) {
        			oNewEnquiryDto.setClosingDate(CommonUtility.buildDateString(rs.getString("QEM_ORDCLOSEMNTH")));
        		}else {
        			oNewEnquiryDto.setClosingDate("");
        		}
        		
        		if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
        			oNewEnquiryDto.setIsNewEnquiry("N");
        		} else {
        			oNewEnquiryDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
        		}
        		
        		oNewEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));
        		oNewEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC"));
        		oNewEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME"));
        		oNewEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
        		oNewEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY"));
        		oNewEnquiryDto.setSmNote(rs.getString("QEM_SMNOTE"));
        		oNewEnquiryDto.setSm_name(rs.getString("QEM_SMNAME"));
        		oNewEnquiryDto.setSupervisionDetails(rs.getString("QEM_SUPERVISION_DETAILS"));
        		oNewEnquiryDto.setSuperviseNoOfManDays(rs.getString("QEM_SUPERVISE_MANDAYS"));
        		oNewEnquiryDto.setSuperviseToFroTravel(rs.getString("QEM_SUPERVISE_TRAVELTOFRO"));
        		oNewEnquiryDto.setSuperviseLodging(rs.getString("QEM_SUPERVISE_LODGING"));
        		oNewEnquiryDto.setRsmNote(rs.getString("QEM_RSMNOTE"));
        		oNewEnquiryDto.setNsmNote(rs.getString("QEM_NSMNOTE"));
        		oNewEnquiryDto.setMhNote(rs.getString("QEM_MHNOTE"));
        		oNewEnquiryDto.setDesignNote(rs.getString("QEM_DMNOTE"));
        		oNewEnquiryDto.setDmUpdateStatus(rs.getString("QEM_DMUPDATESTATUS"));
        		oNewEnquiryDto.setIsReferDesign(rs.getString("QEM_IS_REFER_DESIGN"));
        		oNewEnquiryDto.setCommentsDeviations(rs.getString("QEM_DEVIATIONCOMMENTS"));
        		oNewEnquiryDto.setReassignRemarks(rs.getString("QEM_REASSIGN_COMMENTS"));
        		oNewEnquiryDto.setDesignReferenceNumber(rs.getString("QEM_ENQUIRYREFERENCENO"));
        		
        		if(QuotationConstants.QUOTATION_FORWARD_VIEW.equalsIgnoreCase(sOperationType)) {
        			
        			// Fetch RatingsList for VIEW
        			oNewEnquiryDto.setNewRatingsList(fetchMTORatingDetailsForView(conn, oNewEnquiryDto));
        			
        		} else if(QuotationConstants.QUOTATION_EDIT.equalsIgnoreCase(sOperationType)) {
        			
        			// Fetch RatingsList for EDIT
        			oNewEnquiryDto.setNewRatingsList(fetchMTORatingDetails(conn, oNewEnquiryDto));
        		}
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return oNewEnquiryDto;
	}
	
	public ArrayList<NewRatingDto> fetchMTORatingDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchNewRatingDetailsForView";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
    	NewRatingDto oTempNewRatingDto = null;
    	
    	try {
    		int iEnquiryId = oNewEnquiryDto.getEnquiryId();
    		
    		pstmt = conn.prepareStatement(GET_NEWRATING_DETAILS);
    		pstmt.setInt(1, iEnquiryId);
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewRatingDto = new NewRatingDto();
    			
    			oTempNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oTempNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oTempNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oTempNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			oTempNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEDESC"));
    			oTempNewRatingDto.setLtKWId(rs.getString("QRD_LT_KWDESC"));
    			oTempNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLEDESC")));
    			oTempNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAMEDESC"));
    			oTempNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX_DESC"));
    			oTempNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGDESC"));
    			oTempNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONDESC"));
    			oTempNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASSDESC"));
    			oTempNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONDESC"));
    			oTempNewRatingDto.setModelNumber(rs.getString("QRD_LT_MODELNUMBER"));
    			
    			/* ********************************************  Basic Fields Section ******************************************** */
    			oTempNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oTempNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oTempNewRatingDto.setLtEnclosure(rs.getString("QRD_LT_ENCLOSURE"));
    			oTempNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLINGDESC"));
    			oTempNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEDESC"));
    			oTempNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATIONDESC"));
    			oTempNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATIONDESC"));
    			oTempNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYDESC"));
    			oTempNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATIONDESC"));
    			oTempNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATIONDESC"));
    			oTempNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATIONDESC"));
    			oTempNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEDESC"));
    			oTempNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREADESC"));
    			oTempNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPDESC"));
    			oTempNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEDESC"));
    			oTempNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPDESC"));
    			oTempNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSDESC"));
    			oTempNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDDESC"));
    			oTempNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEDESC"));
    			oTempNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSDESC"));
    			oTempNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEDESC"));
    			oTempNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oTempNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPDESC"));
    			oTempNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGDESC"));
    			oTempNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONDESC"));
    			oTempNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROT_DESC"));
    			oTempNewRatingDto.setLtGreaseType(rs.getString("QRD_LT_GREASETYPEDESC"));
    			oTempNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEDESC"));
    			oTempNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEDESC"));
    			oTempNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oTempNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSDESC"));
    			oTempNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEDESC"));
    			oTempNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSDESC"));
    			oTempNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESDESC"));
    			oTempNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONDESC"));
    			oTempNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALDESC"));
    			oTempNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oTempNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVELDESC"));
    			
    			/* ********************************************  Electrical Fields Section *************************************** */
    			oTempNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGDESC"));
    			oTempNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREDESC"));
    			oTempNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTDESC"));
    			oTempNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYDESC"));
    			oTempNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFDESC"));
    			oTempNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSDESC"));
    			oTempNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYDESC"));
    			oTempNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTORDESC"));
    			oTempNewRatingDto.setLtBroughtOutTerminals(rs.getString("QRD_LT_BROUGHTOUTTERMINALSDESC"));
    			oTempNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADDESC"));
    			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLDESC"));
    			oTempNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEDESC"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINDESC"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXDESC"));
    			oTempNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEDESC"));
    			oTempNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oTempNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			
    			/* ********************************************  Mechanical Fields Section *************************************** */
    			oTempNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGDESC"));
    			oTempNewRatingDto.setLtMainTB(rs.getString("QRD_LT_MAINTERMBOXDESC"));
    			oTempNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEDESC"));
    			oTempNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOXDESC"));
    			oTempNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEDESC"));
    			oTempNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALDESC"));
    			oTempNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDINGDESC"));
    			oTempNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMDESC"));
    			oTempNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEDESC"));
    			oTempNewRatingDto.setLtBearingNDESize(rs.getString("QRD_LT_BEARINGNDESIZE"));
    			oTempNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEDESC"));
    			oTempNewRatingDto.setLtBearingDESize(rs.getString("QRD_LT_BEARINGDESIZE"));
    			oTempNewRatingDto.setLtCableEntry(rs.getString("QRD_LT_CABLEENTRYDESC"));
    			oTempNewRatingDto.setLtLubrication(rs.getString("QRD_LT_LUBRICATIONDESC"));
    			
    			/* ********************************************  Performance Fields Section ************************************** */
    			oTempNewRatingDto.setLtPerfRatedSpeed(rs.getString("QRD_LT_RATEDSPEED"));
    			oTempNewRatingDto.setLtPerfRatedCurr(rs.getString("QRD_LT_RATEDCURRENT"));
    			oTempNewRatingDto.setLtPerfNoLoadCurr(rs.getString("QRD_LT_NOLOADCURRENT"));
    			oTempNewRatingDto.setLtPerfLockRotorCurr(rs.getString("QRD_LT_LOCKROTORCURRENT"));
    			oTempNewRatingDto.setLtPerfNominalTorque(rs.getString("QRD_LT_NOMINALTORQUE"));
    			oTempNewRatingDto.setLtPerfMaxTorque(rs.getString("QRD_LT_MAXIMUMTORQUE"));
    			oTempNewRatingDto.setLtPerfLockRotorTorque(rs.getString("QRD_LT_LOCKROTORTORQUE"));
    			oTempNewRatingDto.setLtPerfSSTHot(rs.getString("QRD_LT_SSTHOT"));
    			oTempNewRatingDto.setLtPerfSSTCold(rs.getString("QRD_LT_SSTCOLD"));
    			oTempNewRatingDto.setLtPerfStartTimeRatedVolt(rs.getString("QRD_LT_STARTTIME_VOLT"));
    			oTempNewRatingDto.setLtPerfStartTime80RatedVolt(rs.getString("QRD_LT_STARTTIME_VOLTAT80"));
    			oTempNewRatingDto.setLtMotorGD2Value(rs.getString("QRD_LT_MOTORGD2"));
    			oTempNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oTempNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATIONDESC"));
    			oTempNewRatingDto.setLtMotorWeight(rs.getString("QRD_LT_TOTALMOTORWEIGHT"));
    			oTempNewRatingDto.setLtLoadEffPercent100Above(rs.getString("QRD_LT_LOADEFF100ABOVE"));
    			oTempNewRatingDto.setLtLoadEffPercentAt100(rs.getString("QRD_LT_LOADEFFAT100"));
    			oTempNewRatingDto.setLtLoadEffPercentAt75(rs.getString("QRD_LT_LOADEFFAT75"));
    			oTempNewRatingDto.setLtLoadEffPercentAt50(rs.getString("QRD_LT_LOADEFFAT50"));
    			oTempNewRatingDto.setLtLoadPFPercentAt100(rs.getString("QRD_LT_LOADPFAT100"));
    			oTempNewRatingDto.setLtLoadPFPercentAt75(rs.getString("QRD_LT_LOADPFAT75"));
    			oTempNewRatingDto.setLtLoadPFPercentAt50(rs.getString("QRD_LT_LOADPFAT50"));
    			
    			/* ********************************************  Accessories Fields Section ************************************** */
    			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTBDESC"));
    			oTempNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTBDESC"));
    			oTempNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWAREDESC"));
    			oTempNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATERDESC"));
    			oTempNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERDESC"));
    			oTempNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDDESC"));
    			oTempNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDDESC"));
    			oTempNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOXDESC"));
    			oTempNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOXDESC"));
    			oTempNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATEDESC"));
    			oTempNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSIONDESC"));
    			oTempNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATEDESC"));
    			oTempNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATEDESC"));
    			oTempNewRatingDto.setLtVibrationProbe(rs.getString("QRD_LT_VIBRPROBEMOUNTPAD"));
    			oTempNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTINGDESC"));
    			oTempNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTINGDESC"));
    			oTempNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFANDESC"));
    			
    			/* ********************************************  Certificate/Spares Fields Section ******************************* */
    			oTempNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINEDESC"));
    			oTempNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1DESC"));
    			oTempNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2DESC"));
    			oTempNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3DESC"));
    			oTempNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETESTDESC"));
    			oTempNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATIONDESC"));
    			oTempNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPDESC"));
    			oTempNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1_DESC"));
    			oTempNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2_DESC"));
    			oTempNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3_DESC"));
    			oTempNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4_DESC"));
    			oTempNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5_DESC"));
    			oTempNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEETDESC"));
    			oTempNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oTempNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVEDESC"));
    			oTempNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oTempNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			alRatingsList.add(oTempNewRatingDto);
    		}
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return alRatingsList;
	}
	
	public ArrayList<NewRatingDto> fetchMTORatingDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchMTORatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
    	NewRatingDto oTempNewRatingDto = null;
    	
    	try {
    		int iEnquiryId = oNewEnquiryDto.getEnquiryId();
    		
    		pstmt = conn.prepareStatement(GET_NEWRATING_DETAILS);
    		pstmt.setInt(1, iEnquiryId);
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewRatingDto = new NewRatingDto();
    			
    			oTempNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oTempNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oTempNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oTempNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			oTempNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEID"));
    			oTempNewRatingDto.setLtKWId(rs.getString("QRD_LT_KW"));
    			oTempNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLE")));
    			oTempNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAME"));
    			oTempNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oTempNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGID"));
    			oTempNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONID"));
    			oTempNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASS"));
    			oTempNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONID"));
    			oTempNewRatingDto.setModelNumber(rs.getString("QRD_LT_MODELNUMBER"));
    			
    			/* ********************************************  Basic Fields Section ******************************************** */
    			oTempNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oTempNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oTempNewRatingDto.setLtEnclosure(rs.getString("QRD_LT_ENCLOSURE"));
    			oTempNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLING"));
    			oTempNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEID"));
    			oTempNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATION"));
    			oTempNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATION"));
    			oTempNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYID"));
    			oTempNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATION"));
    			oTempNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATION"));
    			oTempNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATION"));
    			oTempNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEID"));
    			oTempNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREAID"));
    			oTempNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPID"));
    			oTempNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEID"));
    			oTempNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPID"));
    			oTempNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSID"));
    			oTempNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDID"));
    			oTempNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEID"));
    			oTempNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSID"));
    			oTempNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEID"));
    			oTempNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oTempNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPID"));
    			oTempNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGID"));
    			oTempNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONID"));
    			oTempNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROTATION"));
    			oTempNewRatingDto.setLtGreaseType(rs.getString("QRD_LT_GREASETYPE"));
    			oTempNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEID"));
    			oTempNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEID"));
    			oTempNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oTempNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSID"));
    			oTempNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEID"));
    			oTempNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSID"));
    			oTempNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESID"));
    			oTempNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONID"));
    			oTempNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALID"));
    			oTempNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oTempNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVEL"));
    			
    			/* ********************************************  Electrical Fields Section *************************************** */
    			oTempNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGID"));
    			oTempNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREID"));
    			oTempNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTID"));
    			oTempNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYID"));
    			oTempNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFID"));
    			oTempNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSID"));
    			oTempNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYID"));
    			oTempNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTOR"));
    			oTempNewRatingDto.setLtBroughtOutTerminals(rs.getString("QRD_LT_BROUGHTOUTTERMINALS"));
    			oTempNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADID"));
    			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLID"));
    			oTempNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXID"));
    			oTempNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEID"));
    			oTempNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oTempNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			
    			/* ********************************************  Mechanical Fields Section *************************************** */
    			oTempNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGID"));
    			oTempNewRatingDto.setLtMainTB(rs.getString("QRD_LT_MAINTERMBOX"));
    			oTempNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEID"));
    			oTempNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOX"));
    			oTempNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEID"));
    			oTempNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALID"));
    			oTempNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDING"));
    			oTempNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMID"));
    			oTempNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEID"));
    			oTempNewRatingDto.setLtBearingNDESize(rs.getString("QRD_LT_BEARINGNDESIZE"));
    			oTempNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEID"));
    			oTempNewRatingDto.setLtBearingDESize(rs.getString("QRD_LT_BEARINGDESIZE"));
    			oTempNewRatingDto.setLtCableEntry(rs.getString("QRD_LT_CABLEENTRY"));
    			oTempNewRatingDto.setLtLubrication(rs.getString("QRD_LT_LUBRICATION"));
    			
    			/* ********************************************  Performance Fields Section ************************************** */
    			oTempNewRatingDto.setLtPerfRatedSpeed(rs.getString("QRD_LT_RATEDSPEED"));
    			oTempNewRatingDto.setLtPerfRatedCurr(rs.getString("QRD_LT_RATEDCURRENT"));
    			oTempNewRatingDto.setLtPerfNoLoadCurr(rs.getString("QRD_LT_NOLOADCURRENT"));
    			oTempNewRatingDto.setLtPerfLockRotorCurr(rs.getString("QRD_LT_LOCKROTORCURRENT"));
    			oTempNewRatingDto.setLtPerfNominalTorque(rs.getString("QRD_LT_NOMINALTORQUE"));
    			oTempNewRatingDto.setLtPerfMaxTorque(rs.getString("QRD_LT_MAXIMUMTORQUE"));
    			oTempNewRatingDto.setLtPerfLockRotorTorque(rs.getString("QRD_LT_LOCKROTORTORQUE"));
    			oTempNewRatingDto.setLtPerfSSTHot(rs.getString("QRD_LT_SSTHOT"));
    			oTempNewRatingDto.setLtPerfSSTCold(rs.getString("QRD_LT_SSTCOLD"));
    			oTempNewRatingDto.setLtPerfStartTimeRatedVolt(rs.getString("QRD_LT_STARTTIME_VOLT"));
    			oTempNewRatingDto.setLtPerfStartTime80RatedVolt(rs.getString("QRD_LT_STARTTIME_VOLTAT80"));
    			oTempNewRatingDto.setLtMotorGD2Value(rs.getString("QRD_LT_MOTORGD2"));
    			oTempNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oTempNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATION"));
    			oTempNewRatingDto.setLtMotorWeight(rs.getString("QRD_LT_TOTALMOTORWEIGHT"));
    			oTempNewRatingDto.setLtLoadEffPercent100Above(rs.getString("QRD_LT_LOADEFF100ABOVE"));
    			oTempNewRatingDto.setLtLoadEffPercentAt100(rs.getString("QRD_LT_LOADEFFAT100"));
    			oTempNewRatingDto.setLtLoadEffPercentAt75(rs.getString("QRD_LT_LOADEFFAT75"));
    			oTempNewRatingDto.setLtLoadEffPercentAt50(rs.getString("QRD_LT_LOADEFFAT50"));
    			oTempNewRatingDto.setLtLoadPFPercentAt100(rs.getString("QRD_LT_LOADPFAT100"));
    			oTempNewRatingDto.setLtLoadPFPercentAt75(rs.getString("QRD_LT_LOADPFAT75"));
    			oTempNewRatingDto.setLtLoadPFPercentAt50(rs.getString("QRD_LT_LOADPFAT50"));
    			
    			/* ********************************************  Accessories Fields Section ************************************** */
    			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTB"));
    			oTempNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTB"));
    			oTempNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWARE"));
    			oTempNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATER"));
    			oTempNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERID"));
    			oTempNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDID"));
    			oTempNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDID"));
    			oTempNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOX"));
    			oTempNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOX"));
    			oTempNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATE"));
    			oTempNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSION"));
    			oTempNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATE"));
    			oTempNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATE"));
    			oTempNewRatingDto.setLtVibrationProbe(rs.getString("QRD_LT_VIBRPROBEMOUNTPAD"));
    			oTempNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTING"));
    			oTempNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTING"));
    			oTempNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFAN"));
    			
    			/* ********************************************  Certificate/Spares Fields Section ******************************* */
    			oTempNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINE"));
    			oTempNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1"));
    			oTempNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2"));
    			oTempNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3"));
    			oTempNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETEST"));
    			oTempNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATION"));
    			oTempNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPID"));
    			oTempNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1"));
    			oTempNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2"));
    			oTempNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3"));
    			oTempNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4"));
    			oTempNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5"));
    			oTempNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEET"));
    			oTempNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oTempNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVE"));
    			oTempNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oTempNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			oTempNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			oTempNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    			oTempNewRatingDto.setIsDesignComplete(rs.getString("QRD_LT_ISDESIGNCOMPLETE"));
    			oTempNewRatingDto.setMtoHighlightFlag(rs.getString("QRD_LT_MTOHIGHLIGHT"));
    			
    			alRatingsList.add(oTempNewRatingDto);
    		}
    		
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return alRatingsList;
	}
	
	@Override
	public String saveMTOTechCompleteDetails(Connection conn, ArrayList<NewMTORatingDto> alMTOTechCompleteDetails, String loggedInUserId) throws Exception {
		String sMethodName = "saveMTOTechCompleteDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        int iMTOAddOnId = 0, iMTORatingId = 0, iResult = 0;
        String sCheckQuery = "";
        String sDeleteQuery = "DELETE FROM " + SCHEMA_QUOTATION +".RFQ_LT_MTOADDON_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? AND QRD_LT_MTOADDONPAGETYPE = 'DESIGN' ";
        String sResult = QuotationConstants.QUOTATION_STRING_Y;
        
		try {
			for(NewMTORatingDto oMTORating : alMTOTechCompleteDetails) {
				// Deleting Existing Design Add-On Records - Based on EnquiryId and RatingNo.
	        	pstmt = conn.prepareStatement(sDeleteQuery);
	        	pstmt.setInt(1, oMTORating.getMtoEnquiryId());
	        	pstmt.setInt(2, oMTORating.getMtoRatingNo());
	        	pstmt.executeUpdate();
	        	oDBUtility.releaseResources(pstmt);
	        	
	        	if(CollectionUtils.isNotEmpty(oMTORating.getNewMTOAddOnsList())) {
	        		ArrayList<Integer> alUpdateResult = new ArrayList<>();
	        		for(NewMTOAddOnDto oAddOnDto : oMTORating.getNewMTOAddOnsList()) {
	        			pstmt1 = conn.prepareStatement(FETCH_LT_MTOADDONID_NEXTVAL);	    
	    	    		rs = pstmt1.executeQuery();
	    	    		if (rs.next()) {
	    	    			iMTOAddOnId = Integer.parseInt(rs.getString("MTOADDON_ID"));
	    	    		}
	    	    		oDBUtility.releaseResources(rs, pstmt1);
	    	    		
	    	    		pstmt1 = conn.prepareStatement(INSERT_LT_MTOADDON_DETAILS);
	    	    		pstmt1.setInt(1, iMTOAddOnId);
	    	    		pstmt1.setInt(2, oMTORating.getMtoEnquiryId());
	    	    		pstmt1.setInt(3, oMTORating.getMtoRatingNo());
	    	    		pstmt1.setString(4, oAddOnDto.getMtoAddOnType());
	    	    		pstmt1.setInt(5, oAddOnDto.getMtoAddOnQty());
	    	    		pstmt1.setDouble(6, Double.parseDouble(oAddOnDto.getMtoAddOnValue()));
	    	    		pstmt1.setString(7, oAddOnDto.getMtoAddOnCalcType());
	    	    		pstmt1.setString(8, oAddOnDto.getMtoAddOnReferDesign());
	    	    		pstmt1.setString(9, QuotationConstants.QUOTATIONLT_ADDONPAGETYPE_DESIGN);
	    	    		pstmt1.setString(10, loggedInUserId);
	    	    		iResult = pstmt1.executeUpdate();
	    	    		alUpdateResult.add(iResult);
	    	    		
	    	    		oDBUtility.releaseResources(pstmt1);
	        		}
	        		for(Integer iUpdateVal : alUpdateResult) {
						if(iUpdateVal == 0) {
							sResult = QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
	        	}
	        	
	        	// Saving the NewMTORatingDto.
        		oDBUtility.releaseResources(rs, pstmt1);
        		sCheckQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
        		pstmt1 = conn.prepareStatement(sCheckQuery);
	    		pstmt1.setInt(1, oMTORating.getMtoEnquiryId());
	    		pstmt1.setInt(2, oMTORating.getMtoRatingNo());
	    		rs = pstmt1.executeQuery();
	    		
	    		if (rs.next()) {
	    			pstmt2 = conn.prepareStatement(UPDATE_LT_MTORATING_DESIGNDETAILS);
	    			pstmt2.setInt(1, oMTORating.getMtoQuantity());
	        		pstmt2.setString(2, oMTORating.getMtoProdLineId());
	        		pstmt2.setString(3, oMTORating.getMtoKWId());
	        		pstmt2.setInt(4, Integer.parseInt(oMTORating.getMtoPoleId()));
	        		pstmt2.setString(5, oMTORating.getMtoFrameId());
	        		pstmt2.setString(6, oMTORating.getMtoFrameSuffixId());
	        		pstmt2.setString(7, oMTORating.getMtoMountingId());
	        		pstmt2.setString(8, oMTORating.getMtoTBPositionId());
	        		pstmt2.setString(9, oMTORating.getMtoMfgLocation());
	        		pstmt2.setString(10, oMTORating.getMtoSpecialAddOnPercent());
	        		pstmt2.setString(11, oMTORating.getMtoSpecialCashExtra());
	        		pstmt2.setString(12, oMTORating.getMtoPercentCopper());
	        		pstmt2.setString(13, oMTORating.getMtoPercentStamping());
	        		pstmt2.setString(14, oMTORating.getMtoPercentAluminium());
	        		pstmt2.setString(15, oMTORating.getMtoTotalAddOnPercent());
	        		pstmt2.setString(16, oMTORating.getMtoTotalCashExtra());
	        		pstmt2.setString(17, oMTORating.getMtoDesignComplete());
	        		pstmt2.setString(18, oMTORating.getMtoSpecialFeatures());
	        		// For WHERE Condition
	        		pstmt2.setInt(19, oMTORating.getMtoEnquiryId());
	        		pstmt2.setInt(20, oMTORating.getMtoRatingNo());
	        		
	        		iResult = pstmt2.executeUpdate();
	    		} else {
	    			pstmt2 = conn.prepareStatement(FETCH_LT_MTORATING_NEXTVAL);	    
	        		rs = pstmt2.executeQuery();
	        		if (rs.next()) {
	        			iMTORatingId = Integer.parseInt(rs.getString("MTORATING_ID"));
	        		}
	        		oDBUtility.releaseResources(rs, pstmt2);
	        		
	        		pstmt2 = conn.prepareStatement(INSERT_LT_MTORATING_DESIGNDETAILS);
	        		pstmt2.setInt(1, iMTORatingId);
	        		pstmt2.setInt(2, oMTORating.getMtoEnquiryId());
	        		pstmt2.setInt(3, oMTORating.getMtoRatingNo());
	        		pstmt2.setInt(4, oMTORating.getMtoQuantity());
	        		pstmt2.setString(5, oMTORating.getMtoProdLineId());
	        		pstmt2.setString(6, oMTORating.getMtoKWId());
	        		pstmt2.setString(7, oMTORating.getMtoPoleId());
	        		pstmt2.setString(8, oMTORating.getMtoFrameId());
	        		pstmt2.setString(9, oMTORating.getMtoFrameSuffixId());
	        		pstmt2.setString(10, oMTORating.getMtoMountingId());
	        		pstmt2.setString(11, oMTORating.getMtoTBPositionId());
	        		pstmt2.setString(12, oMTORating.getMtoMfgLocation());
	        		pstmt2.setString(13, oMTORating.getMtoSpecialAddOnPercent());
	        		pstmt2.setString(14, oMTORating.getMtoSpecialCashExtra());
	        		pstmt2.setString(15, oMTORating.getMtoPercentCopper());
	        		pstmt2.setString(16, oMTORating.getMtoPercentStamping());
	        		pstmt2.setString(17, oMTORating.getMtoPercentAluminium());
	        		pstmt2.setString(18, oMTORating.getMtoTotalAddOnPercent());
	        		pstmt2.setString(19, oMTORating.getMtoTotalCashExtra());
	        		pstmt2.setString(20, oMTORating.getMtoDesignComplete());
	        		pstmt2.setString(21, oMTORating.getMtoSpecialFeatures());
	        		
	        		iResult = pstmt2.executeUpdate();
	    		}
	    		if(iResult == 0) {
	    			sResult = QuotationConstants.QUOTATION_STRING_N;
	    			break;
	    		}
	    		
	    		oDBUtility.releaseResources(pstmt2);
			}
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt1);
			oDBUtility.releaseResources(pstmt2);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	@Override
	public ArrayList<String> getPDFProdLinesList(Connection conn) throws Exception {
		String sMethodName = "saveMTOTechCompleteDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		ArrayList<String> alPDFProdLinesList = new ArrayList<>();
		
		try {
			ArrayList<KeyValueVo> arlOptions = getAscendingOptionsbyAttributeList(conn, QuotationConstants.QPDF_LT_PRODLINES);
			for(KeyValueVo oKV : arlOptions) {
				alPDFProdLinesList.add(oKV.getValue());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return alPDFProdLinesList;
	}
	
	public String getOptionsbyAttributeKey(Connection conn, String sLookupField, String sLookupKey) throws Exception {
		String sMethodName = "getOptionsbyAttributeList";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		String sValue = "";
		
		try {
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_BY_ATTRIBUTE_KEY);
			pstmt.setString(1, sLookupField);
			pstmt.setString(2, sLookupKey);
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				sValue = rs.getString("QEM_VALUE");
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return sValue;
	}
	
	public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception {
		String sMethodName = "getAscendingOptionsbyAttributeList";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {			
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_ASC);
			pstmt.setString(1, sLookupField);			
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString("QEM_KEY"));
					oKeyValueVo.setValue(rs.getString("QEM_VALUE"));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return arlOptions;
	}

	@Override
	public String savePDFRatingsList(Connection conn, NewEnquiryDto oNewEnquiryDto, ArrayList<NewTechnicalOfferDto> alRatingPDFList) throws Exception {
		String sMethodName = "savePDFRatingsList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null ;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();	 
		
		NewTechnicalOfferDto oTempTechOfferDto = null;
		ArrayList<Integer> alInsertResult = new ArrayList<>();
		
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		String sSQLPDFRatingQuery = null;
		String sDeleteQuery = "DELETE FROM " + SCHEMA_QUOTATION + ".RFQ_LT_PDFRATING_DETAILS WHERE QPD_LT_ENQUIRYID = ? ";
		int iIndex = 1, iPDFRatingId = 0, iCountVal = 0, iInsertRes = 0, iUpdateRes = 0;
		
		try {
			pstmt = conn.prepareStatement(sDeleteQuery);
        	pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
        	pstmt.executeUpdate();
        	
			for(int iCount = 0; iCount < alRatingPDFList.size(); iCount++) {
				oTempTechOfferDto = (NewTechnicalOfferDto) alRatingPDFList.get(iCount);
				
				pstmt1 = conn.prepareStatement(FETCH_LT_PDFRATINGID_NEXTVAL);	    
	    		rs1 = pstmt1.executeQuery();
	    		if (rs1.next()) {
	    			iPDFRatingId = Integer.parseInt(rs1.getString("PDF_ID"));
	    		}
	    		oDBUtility.releaseResources(rs1, pstmt1);
	        	
	    		sSQLPDFRatingQuery = buildPDFRatingInsertQuery(oTempTechOfferDto, iPDFRatingId);
	    		pstmt1 = conn.prepareStatement(sSQLPDFRatingQuery);
	    		iInsertRes = pstmt1.executeUpdate();
	    		alInsertResult.add(iInsertRes);
			}
			if(alInsertResult.size() > 0 ) {
				for(Integer iInsertVal : alInsertResult) {
					if(iInsertVal == 0) {
						sResult = QuotationConstants.QUOTATION_STRING_N;
						break;
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	private String buildPDFRatingInsertQuery(NewTechnicalOfferDto oTechOfferDto, int iPDFRatingId) {
		String sMethodName = "buildMTORatingInsertQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		String sInsertQuery = null;
		StringBuffer sbFieldQuery = new StringBuffer();
		StringBuffer sbValuesQuery = new StringBuffer();
		
		if (oTechOfferDto != null) {
			sbFieldQuery.append(INSERT_PDFRATING_DETAILS);
			
			sbFieldQuery.append("(");
			sbValuesQuery.append(" VALUES (");
			
			/* PDF ID */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "PDF RatingId -  " + iPDFRatingId);
			sbFieldQuery.append("QPD_LT_ID, ");
			sbValuesQuery.append(iPDFRatingId + ", ");
			/* Enquiry Id */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Enquiry Id -  " + oTechOfferDto.getEnquiryId());
			sbFieldQuery.append("QPD_LT_ENQUIRYID, ");
			sbValuesQuery.append(oTechOfferDto.getEnquiryId() + ", ");
			/* Rating no. */
			sbFieldQuery.append("QPD_LT_RATINGNO, ");
			sbValuesQuery.append(oTechOfferDto.getRatingNo() + ", ");
			/* Quantity */
			sbFieldQuery.append("QPD_LT_QUANTITY, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			
			sbFieldQuery.append("QPD_LT_PRODGROUPID, ");
			sbValuesQuery.append("'"+oTechOfferDto.getProductGroup() + "', ");
			sbFieldQuery.append("QPD_LT_PRODLINEID, ");
			sbValuesQuery.append("'"+oTechOfferDto.getProductLine() + "', ");
			//sbFieldQuery.append("QPD_LT_MOTORTYPEID, ");
			//sbValuesQuery.append(" " + ", ");
			sbFieldQuery.append("QPD_LT_KW, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerRating_KW() + "', ");
			sbFieldQuery.append("QPD_LT_POLE, ");
			sbValuesQuery.append(oTechOfferDto.getNoOfPoles() + ", ");
			sbFieldQuery.append("QPD_LT_FRAME, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrameType() + "', ");
			sbFieldQuery.append("QPD_LT_FRAME_SUFFIX, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrameSuffix() + "', ");
			sbFieldQuery.append("QPD_LT_MOUNTINGID, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMountingType() + "', ");
			sbFieldQuery.append("QPD_LT_TBPOSITIONID, ");
			sbValuesQuery.append("'"+oTechOfferDto.getTbPosition() + "', ");
			sbFieldQuery.append("QPD_LT_ISREFERDESIGN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getIsReferDesign() + "', ");
			sbFieldQuery.append("QPD_LT_MODELNUMBER, ");
			sbValuesQuery.append("'"+oTechOfferDto.getModelNumber() + "', ");
			sbFieldQuery.append("QPD_LT_VOLTAGE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getVoltage() + "', ");
			sbFieldQuery.append("QPD_LT_FREQUENCY, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrequency() + "', ");
			sbFieldQuery.append("QPD_LT_POWER_KW, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerRating_KW() + "', ");
			sbFieldQuery.append("QPD_LT_POWER_HP, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerRating_HP() + "', ");
			sbFieldQuery.append("QPD_LT_CURRENT, ");
			sbValuesQuery.append("'"+oTechOfferDto.getCurrent_A() + "', ");
			sbFieldQuery.append("QPD_LT_RPM, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMaxSpeed() + "', ");
			sbFieldQuery.append("QPD_LT_TORQUE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getTorque() + "', ");
			sbFieldQuery.append("QPD_LT_IECLASS, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEfficiencyClass() + "', ");
			sbFieldQuery.append("QPD_LT_LOADEFF100ABOVE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEfficiency_100Above() + "', ");
			sbFieldQuery.append("QPD_LT_LOADEFFAT100, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEfficiency_100() + "', ");
			sbFieldQuery.append("QPD_LT_LOADEFFAT75, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEfficiency_75() + "', ");
			sbFieldQuery.append("QPD_LT_LOADEFFAT50, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEfficiency_50() + "', ");
			sbFieldQuery.append("QPD_LT_LOADPFAT100, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerFactor_100() + "', ");
			sbFieldQuery.append("QPD_LT_LOADPFAT75, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerFactor_75() + "', ");
			sbFieldQuery.append("QPD_LT_LOADPFAT50, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPowerFactor_50() + "', ");
			sbFieldQuery.append("QPD_LT_IAIN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getStartingCurrent() + "', ");
			sbFieldQuery.append("QPD_LT_TATN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getStarting_torque() + "', ");
			sbFieldQuery.append("QPD_LT_TKTN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getPullout_torque() + "', ");
			sbFieldQuery.append("QPD_LT_RA, ");
			sbValuesQuery.append("'"+oTechOfferDto.getRaValue() + "', ");
			sbFieldQuery.append("QPD_LT_RV, ");
			sbValuesQuery.append("'"+oTechOfferDto.getRvValue() + "', ");
			//sbFieldQuery.append("QPD_LT_MOTORTYPE, ");
			//sbValuesQuery.append("'"+oTechOfferDto.getQuantity() + "', ");
			sbFieldQuery.append("QPD_LT_ENCLOSURE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getEnclosureType() + "', ");
			sbFieldQuery.append("QPD_LT_FRAME_MATERIAL, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrameMaterial() + "', ");
			sbFieldQuery.append("QPD_LT_FRAME_SIZE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrameSize() + "', ");
			sbFieldQuery.append("QPD_LT_DUTY, ");
			sbValuesQuery.append("'"+oTechOfferDto.getDuty() + "', ");
			sbFieldQuery.append("QPD_LT_VOLT_ADDVAR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getVoltageAddVariation() + "', ");
			sbFieldQuery.append("QPD_LT_VOLT_REMVAR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getVoltageRemoveVariation() + "', ");
			sbFieldQuery.append("QPD_LT_FREQ_ADDVAR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrequencyAddVariation() + "', ");
			sbFieldQuery.append("QPD_LT_FREQ_REMVAR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getFrequencyRemoveVariation() + "', ");
			sbFieldQuery.append("QPD_LT_COMB_VAR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getCombinedVariation() + "', ");
			sbFieldQuery.append("QPD_LT_DESIGN_REF, ");
			sbValuesQuery.append("'"+oTechOfferDto.getDesign() + "', ");
			sbFieldQuery.append("QPD_LT_SERVICE_FACTOR, ");
			sbValuesQuery.append("'"+oTechOfferDto.getServiceFactor() + "', ");
			sbFieldQuery.append("QPD_LT_INSULATIONCLASS, ");
			sbValuesQuery.append("'"+oTechOfferDto.getInsulationClass() + "', ");
			sbFieldQuery.append("QPD_LT_AMB_ADDTEMP, ");
			sbValuesQuery.append("'"+oTechOfferDto.getAmbientAddTemp() + "', ");
			sbFieldQuery.append("QPD_LT_AMB_REMTEMP, ");
			sbValuesQuery.append("'"+oTechOfferDto.getAmbientRemoveTemp() + "', ");
			sbFieldQuery.append("QPD_LT_TEMPRISE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getTempRise() + "', ");
			sbFieldQuery.append("QPD_LT_ALTITUDE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getAltitude() + "', ");
			sbFieldQuery.append("QPD_LT_HAZARDAREATYPE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getHazArea() + "', ");
			sbFieldQuery.append("QPD_LT_HAZARDZONE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getZoneClassification() + "', ");
			sbFieldQuery.append("QPD_LT_GASGROUP, ");
			sbValuesQuery.append("'"+oTechOfferDto.getGasGroup() + "', ");
			sbFieldQuery.append("QPD_LT_TEMPCLASS, ");
			sbValuesQuery.append("'"+oTechOfferDto.getTemperatureClass() + "', ");
			sbFieldQuery.append("QPD_LT_ROTORTYPE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getRotortype()+ "', ");
			sbFieldQuery.append("QPD_LT_BEARINGNDE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getOdeBearingType() + "', ");
			sbFieldQuery.append("QPD_LT_BEARINGDE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getDeBearingType() + "', ");
			sbFieldQuery.append("QPD_LT_BEARINGNDESIZE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getOdeBearingSize() + "', ");
			sbFieldQuery.append("QPD_LT_BEARINGDESIZE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getDeBearingSize() + "', ");
			sbFieldQuery.append("QPD_LT_LUBRICATION, ");
			sbValuesQuery.append("'"+oTechOfferDto.getLubMethod() + "', ");
			sbFieldQuery.append("QPD_LT_GREASETYPE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getTypeOfGrease() + "', ");
			sbFieldQuery.append("QPD_LT_MOTORWEIGHT, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMotorWeight() + "', ");
			sbFieldQuery.append("QPD_LT_GROSSWEIGHT, ");
			sbValuesQuery.append("'"+oTechOfferDto.getGrossWeight() + "', ");
			
			sbFieldQuery.append("QPD_LT_STATORCONN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getStatorConnection() + "', ");
			sbFieldQuery.append("QPD_LT_ROTORCONN, ");
			sbValuesQuery.append("'"+oTechOfferDto.getRotorConnection() + "', ");
			sbFieldQuery.append("QPD_LT_CDF, ");
			sbValuesQuery.append("'"+oTechOfferDto.getCdf() + "', ");
			sbFieldQuery.append("QPD_LT_NOOFSTARTS, ");
			sbValuesQuery.append("'"+oTechOfferDto.getNoOfStarts_HR() + "', ");
			sbFieldQuery.append("QPD_LT_IP, ");
			sbValuesQuery.append("'"+oTechOfferDto.getIpCode() + "', ");
			sbFieldQuery.append("QPD_LT_MOUNTING, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMountingType() + "', ");
			sbFieldQuery.append("QPD_LT_METHODOFCOOLING, ");
			sbValuesQuery.append("'"+oTechOfferDto.getCoolingMethod() + "', ");
			sbFieldQuery.append("QPD_LT_MOTORGD2, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMotorInertia() + "', ");
			sbFieldQuery.append("QPD_LT_LOADGD2, ");
			sbValuesQuery.append("'"+oTechOfferDto.getLoadInertia() + "', ");
			sbFieldQuery.append("QPD_LT_VIBRATIONLEVEL, ");
			sbValuesQuery.append("'"+oTechOfferDto.getVibration() + "', ");
			sbFieldQuery.append("QPD_LT_NOISELEVEL, ");
			sbValuesQuery.append("'"+oTechOfferDto.getNoise() + "', ");
			sbFieldQuery.append("QPD_LT_NOOFSTARTS_HOTCOLD, ");
			sbValuesQuery.append("'"+oTechOfferDto.getNoOfStarts() + "', ");
			sbFieldQuery.append("QPD_LT_STARTINGMETHOD, ");
			sbValuesQuery.append("'"+oTechOfferDto.getStartingType() + "', ");
			sbFieldQuery.append("QPD_LT_METHODOFCOUPLING, ");
			sbValuesQuery.append("'"+oTechOfferDto.getMethodOfCoupling() + "', ");
			sbFieldQuery.append("QPD_LT_LRWITHSTANDTIME, ");
			sbValuesQuery.append("'"+oTechOfferDto.getLrWithstandTime() + "', ");
			sbFieldQuery.append("QPD_LT_DIROFROTATION, ");
			sbValuesQuery.append("'"+oTechOfferDto.getDirectionOfRotation() + "', ");
			sbFieldQuery.append("QPD_LT_STANDARDROTATION, ");
			sbValuesQuery.append("'"+oTechOfferDto.getRotation() + "', ");
			sbFieldQuery.append("QPD_LT_MAXCABLESIZE, ");
			sbValuesQuery.append("'"+oTechOfferDto.getCableSize() + "', ");
			
			// These values are Fetched from Rating Table.
			/*
			sbFieldQuery.append("QPD_LT_PAINTSHADE, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			sbFieldQuery.append("QPD_LT_RTD, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			sbFieldQuery.append("QPD_LT_BTD, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			sbFieldQuery.append("QPD_LT_THERMISTER, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			sbFieldQuery.append("QPD_LT_SPACEHEATER, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			sbFieldQuery.append("QPD_LT_AUXTERMINALBOX, ");
			sbValuesQuery.append(oTechOfferDto.getQuantity() + ", ");
			*/
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

			if (sbValuesQuery.toString().trim().endsWith(","))
				sbValuesQuery = new StringBuffer(sbValuesQuery.toString().trim().substring(0, sbValuesQuery.toString().trim().lastIndexOf(",")));

			sbFieldQuery.append(")");
			sbValuesQuery.append(")");
			
			sInsertQuery = sbFieldQuery.toString() + sbValuesQuery.toString();
		}
		
		//System.out.println("sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		//System.out.println("sbValuesQuery ...... " + sbValuesQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbValuesQuery ...... " + sbValuesQuery);
		//System.out.println("Final Query -  " + sInsertQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Final Query -  " + sInsertQuery);
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sInsertQuery;
	}
	
}
