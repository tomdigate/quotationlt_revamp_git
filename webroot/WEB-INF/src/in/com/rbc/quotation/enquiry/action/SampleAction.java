/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: SampleAction.java
 * Package: in.com.rbc.quotation.enquiry.action
 * Desc: 
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 7, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.enquiry.action;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dao.WorkflowDao;

public class SampleAction extends BaseAction {
    public ActionForward sample(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        String sMethodName =QuotationConstants.QUOTATION_SAMPLE ;
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        WorkflowDao oWorkflowDao =null;
        try {
            /*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            /* Retrieving the WorkflowDao object */
            oWorkflowDao = oDaoFactory.getWorkflowDao();
            
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto);

            
        } catch(Exception e) {
            /*
            * Logging any exception that raised during this activity in log files using Log4j
            */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    }
}
