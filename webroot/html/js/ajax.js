// Variable to hold the xmlHTTPRequest obj
var req;
var toggleId;

// to retrieve options for the selected drop down
function invokeAjaxFunctionality(url, data, toggleName){
	toggleId = toggleName
	toggleWork(true, '');
	
	encode(data);
	
	return retrieveURL(url, data);
}

function invokeCommonAjaxFunctionality(url, data) {
  	encode(data);
	return retrieveURL(url, data);
}

// Function to get browser specific XMLHttpRequest obj
function retrieveURL(url, data) {
  if (window.XMLHttpRequest) { // Non-IE browsers
    // Set up the request
    req = new XMLHttpRequest();
    req.onreadystatechange = processGetOptionsUsingXML;
    try {
      req.open("POST", url, true);
    } catch (e) {
      alert(e);
    }
    req.send(null);
  }
  else if (window.ActiveXObject) { // IE  	
    req = new ActiveXObject("Microsoft.XMLHTTP");
    if (req) {
	  // Send the POST request
	  req.onreadystatechange = processGetOptionsUsingXML;
      req.open("POST", url, true);
      req.setRequestHeader("Connection", "close");
      req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      req.setRequestHeader("Method", "POST" + url + "HTTP/1.1");
      if ( !data ) data="";
      req.send(data);
    }
  }
}

// function to encode uri
function encode( uri ) {
    if (encodeURIComponent) {
        return encodeURIComponent(uri);
    }
    if (escape) {
        return escape(uri);
    }
}

// function to decode the uri
function decode( uri ) {
    uri = uri.replace(/\+/g, ' ');

    if (decodeURIComponent) {
        return decodeURIComponent(uri);
    }
    if (unescape) {
        return unescape(uri);
    }
    return uri;
}

// function to enable the selected dropdown list
function enableDDL(obj){
	document.getElementById(obj).disabled = false; //enabling the dropdown
}

function processGetOptionsUsingXML() {
   if (req.readyState == 4) {
        if (req.status == 200) {
            xml = req.responseXML;            
		    // parse the XML
            if (xml.documentElement) {
            	//get the target list
	            var targetList = xml.documentElement.getElementsByTagName("target");	   	                    
	            if (targetList != null && targetList.length > 0) {	            	             	  
						for (var j = 0; j < targetList.length; j++) {
							// retrieving dropdown field name
			             	var target = xml.documentElement.getElementsByTagName("target")[j].firstChild.nodeValue;					             	
			             	var objType = xml.documentElement.getElementsByTagName("objType")[j].firstChild.nodeValue;			             	
			             	
			             	if (objType == "comboBox") {
	        			     	// retrieving options
				             	var opts = xml.documentElement.getElementsByTagName("options-"+target);				             	
	        			     	var optionsList = new Array();
								if (opts != null) {									
									for (var i = 0; i < opts.length; i++) {
						                var name = decode(opts[i].firstChild.firstChild.nodeValue);						                
										optionsList[i] = name;
			            			}
				    			}				    			
		    				    setRetrievedDatafromAjax(optionsList, target);		    				
		    				    //toggleWork(false,  '');
	    					}else if (objType == "textData") {
	    		    			var opts = xml.documentElement.getElementsByTagName("options-"+target);
	         					var list = new Array();
	         					for (var i = 0; i < opts.length; i++) {
    	         		    		var name;
    	         		   			if (opts[i].firstChild.firstChild != null){
    		                	 		name = decode(opts[i].firstChild.firstChild.nodeValue);
    		               			} else {
    		                    		name = "";
    		               			}
        		                	list[i] = name;
    		    			    }
    		    			    setRetrievedTextDatafromAjax(list, target);
		    			}else if (objType =="statusupdate") {
    	             	     var updated = xml.documentElement.getElementsByTagName("updated")[0].firstChild.nodeValue;
    	             	     var action = xml.documentElement.getElementsByTagName("action")[0].firstChild.nodeValue;
		    		         setStatusUpdateDetails(target, updated, action);
		    		         toggleWork(false,  '');
	    		     }
		    		toggleWork(false,  '');
	    		  }
	    		}

				else {
				    //alert("Failed to receive the XML document from the request");
	                toggleWork(false, '');
				}
        }
        else {
           	alert("There was a problem retrieving the XML data:\n" + req.statusText);
           	toggleWork(false,  '');
        }
    }// status
   }// ready state
}



function toggleWork(onOff, num) {
	if (toggleId == ""){
		toggleId = "divWorking";
	}
	workMsg = document.getElementById(toggleId);
	if (onOff) {
		workMsg.style.visibility = 'visible';
		//alert("Please wait its in processing.........");
	} else {
		workMsg.style.visibility = 'hidden';
	}
}

function setRetrievedDatafromAjax(retrieveList, targetObjName) 
{
    var targetObj = document.getElementById(targetObjName);  
	targetObj.disabled = false; 
	setComboValues(retrieveList, targetObj);
	return;
}

function setComboValues( retrieveList, targetObj) {
      targetObj.options.length=0;
	  targetObj.options[0] = new Option("Select"); 
	  targetObj.options[0].value = "0";
	  targetObj.options[0].selected;
	  var counter = 1;
	  
	  if ( retrieveList.length>0 && (retrieveList[0].split("&&")).length==3 ) {
	      for (var i = 0; i < retrieveList.length; i++) {
	          var valArray = retrieveList[i].split("&&");
	          
              var key = valArray[1] + ':' + valArray[2];
              var value = valArray[0];
    	      var newOpt = new Option( value, key );
    	      targetObj.options[counter] = newOpt; // setting options
    	      //targetObj.options[counter].value = retrieveList[i];
    	      counter++;
    	  }
	  } else {
	      for (var i = 0; i < retrieveList.length; i++) {
              var key = retrieveList[i].substring(retrieveList[i].lastIndexOf("&&")+2);
              var value = retrieveList[i].substring(0, retrieveList[i].lastIndexOf("&&") );
    	      var newOpt = new Option( value, key );
    	      targetObj.options[counter] = newOpt; // setting options
    	      //targetObj.options[counter].value = retrieveList[i];
    	      counter++;
    	  }
	  }
	  
	  return;
}

function setRetrievedTextDatafromAjax(retrieveData, targetObjName) 
{
    var targetObj = document.getElementById(targetObjName);
	targetObj.disabled = false; 
	setTextData(retrieveData, targetObj);
	return;
}

function setTextData(retrieveData, targetObj) {
	targetObj.value  = retrieveData;
}

function setStatusUpdateDetails(target, updated, action) {
    var objStatus = document.getElementById(target.split(",")[0]);
	var objStatusAction = document.getElementById(target.split(",")[1]);
	
	if ( updated == "true" ) {
	    if ( action == "A" ) {
    	    objStatus.value = "Active";
    	    objStatusAction.value = "Turn Inactive";
    	}
    	if ( action == "I" ) {
    	    objStatus.value = "Inactive";
    	    objStatusAction.value = "Turn Active";
    	}
    	//alert("Status updated");
	} else {
	    alert("Status could not be updated");
	}
}

function onRegionChange(regionObj, targetObjs, canIncludeInactive, toggleName){	
	if (Trim(regionObj.value) != "" &&  Trim(regionObj.value) != "Select") {		
		clearOption(targetObjs);
		//alert('val'+regionObj.value)
		var url = "masters.do?invoke=loadSalesMangerlookup";
		data = "";
		data += "&selectedOption=" + Trim(regionObj.value)+"&targetObjects=" +Trim(targetObjs);		
		data += "&canIncludeInactive=" + canIncludeInactive;
	    url += data;
	    invokeAjaxFunctionality(url, data, toggleName);		
	}else {
         clearOption(targetObjs);
	} 
}

function onLocationChange(locObj, targetObjs, canIncludeInactive, toggleName){	
	if (Trim(locObj.value) != "" &&  Trim(locObj.value) != "Select") {		
		clearOption(targetObjs);
		var url = "masters.do?invoke=loadCustomerlookup";
		data = "";
		data += "&selectedOption=" + Trim(locObj.value)+"&targetObjects=" +Trim(targetObjs);		
		data += "&canIncludeInactive=" + canIncludeInactive;
	    url += data;
	    invokeAjaxFunctionality(url, data, toggleName);		
	}else {
         clearOption(targetObjs);
	} 
}

function clearOption(targetObjOriginal){
	var mySplitResult = targetObjOriginal.split(",");
	for(i = 0; i < mySplitResult.length; i++){
		var splitObject = mySplitResult[i];
		var targetObj = document.getElementById(mySplitResult[i]);		
		var mySplitResult1 = splitObject.split(":");
		if (mySplitResult1.length > 0){
			var targetObj1 = document.getElementById(mySplitResult1[0]);
			if ( targetObj1.type=="select-one" ) {
				targetObj1.options.length=0;
            	targetObj1.options[0] = new Option("Select"); 
    	    	targetObj1.options[0].value = "";
    	    	targetObj1.options[0].selected;
			} else {
		    	targetObj1.value='';
			}					
		}
	}
}
