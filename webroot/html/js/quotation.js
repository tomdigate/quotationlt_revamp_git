function postAutocomplete(){	
		//if (document.getElementById("customerName") != null)
			//(document.getElementById("customerName"), 'location', 'divWorking', 'YES')
	
}
function preAutocomplete(){	
	document.getElementById("customerId").value ="0";
		

}
function searchPostAutocomplete(){
	if (document.getElementById("location") != null){
		document.getElementById("location").focus();
		if (document.getElementById("customerName") != null)
			onCustomerChange(document.getElementById("customerName"), 'location', 'divWorking', 'NO')
	}
}
/*
 * Functions related to Menu Items - START
 */
 
 /* Function to view Enquiry Based on its Status  */
function viewEnquiryDetails(enquiryId,statusId,isNewEnquiry) {
   
   var url = "";
   if(isNewEnquiry != null && isNewEnquiry == 'Y') {
	   url = "newPlaceAnEnquiry.do?invoke=validateRequestAction&enquiryId="+enquiryId+"&statusId="+statusId;
   } else {
	   url ="createEnquiry.do?invoke=validateRequestAction&enquiryId="+enquiryId+"&statusId="+statusId;
   }
   window.location = url;
}
 /* Function to view Enquiry Based on its Status  */
function viewSearchEnquiryDetails(enquiryId,statusId,idx,isNewEnquiry) {
   
   var url = "";
   if(isNewEnquiry != null && isNewEnquiry == 'Y') {
	   url="newPlaceAnEnquiry.do?invoke=validateRequestAction&enquiryId="+enquiryId+"&statusId="+statusId+"&index="+idx;
   } else {
	   url="createEnquiry.do?invoke=validateRequestAction&enquiryId="+enquiryId+"&statusId="+statusId+"&index="+idx;
   }
   window.location = url;
}
 
/* Function to view Home page */
function home() {
    var url = 'home.do?invoke=home';
	window.location = url;
}
/* Function to view Enquiry Creation page */
function placeAnEnquiry() {
    var url = 'createEnquiry.do?invoke=viewCreateEnquiry';
	window.location = url;
}
/* Function to view Enquiry Search page */
function searchEnquiry() {
    var url = 'searchenquiry.do?invoke=viewSearchEnquiryResults';
	window.location = url;
}
/* Function to view Enquiry Search page */
function searchMyEnquiry() {
    var url = 'searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromview';
	window.location = url;
}


/* Function to view Masters list page */
function viewMasters() {
    var url = 'masters.do?invoke=viewSearchMasterList';
	window.location = url;
}
/* Function to view Masters list page */
function viewTechnicaldata() {
    var url = 'technicaldata.do?invoke=viewbulkdata';
	window.location = url;
}
/* Function to view Masters list page */
function viewCustomerDiscount() {
    var url = 'customerdiscount.do?invoke=viewdiscount';
	window.location = url;
}
/* Function to view Master Data list page */
function viewMasterData() {
     var url = 'mastersdata.do?invoke=viewMasterList';
	window.location = url;
}

/* Function to view Master Data list page */
function viewReport() {
     var url = 'viewreport.do?invoke=viewGraph';
	window.location = url;
}

function checkDeliveryData(){
	var idx= document.getElementById("hidRowIndex").value;
	var delLoc = document.getElementById("deliveryLocation"+idx).value;
	var delAdd = document.getElementById("deliveryAddress"+idx).value;
	var mu = document.getElementById("noOfMotors"+idx).value;
	
	if(delLoc != null && delLoc!='' || delAdd != null && delAdd!='' || mu!=null && mu!='' ){
		if(delLoc == null || delLoc==''){
			alert("Please enter Delivery Location");
			document.getElementById("deliveryLocation"+idx).focus();
  			return false;
		}
		else if(delAdd == null || delAdd==''){
			alert("Please enter DeliveryAddress");
			document.getElementById("deliveryAddress"+idx).focus();
  			return false;
		}
		else if(mu == null || mu==''){
			alert("Please enter No. of Motors ");
			document.getElementById("noOfMotors"+idx).focus();
  			return false;
		}
	}
}

/* Function to view Master Data list page */
function validateSearchEnquiry() {
  	var warrantystartdate = document.SearchEnquiryForm.createdStartDate.value;
  	var warrantyenddate =document.SearchEnquiryForm.createdEndDate.value;
  	var orderclsstartdate = document.SearchEnquiryForm.orderClsStartDate.value;
  	var orderclsenddate =document.SearchEnquiryForm.orderClsEndDate.value;
  		if(!(warrantystartdate=="")) {
			if(!(warrantyenddate=="")) {
				if(compareDates(warrantyenddate,warrantystartdate, "/") == "-1") {
		  			alert("Created range to date should be greater than or equal to the from date.");
		  			return false;
		  		}
		  	}
		  	else {
			  	alert("Please select created range end date");
	  			return false;
		  	}
  		}
	  	else {
	  		if(!(warrantyenddate=="")) {
					alert("Please select created range start date");
		  			return false;
			}
	  	
	  	}
  		if(!(orderclsstartdate=="")) {
			if(!(orderclsenddate=="")) {
				if(compareDates(orderclsenddate,orderclsstartdate, "/") == "-1") {
		  			alert("Order Closed range to date should be greater than or equal to the from date.");
		  			return false;
		  		}
		  	}
		  	else {
			  	alert("Please select Order Closed range end date");
	  			return false;
		  	}
  		}
	  	else {
	  		if(!(orderclsenddate=="")) {
					alert("Please select Order Closed range start date");
		  			return false;
			}
	  	
	  	}
  		
	  	if(!document.SearchEnquiryForm.location[document.SearchEnquiryForm.location.selectedIndex].value=="0")
	  	{
	  		
			document.SearchEnquiryForm.locationName.value = document.SearchEnquiryForm.location[document.SearchEnquiryForm.location.selectedIndex].text;  	

	  	}
	  document.SearchEnquiryForm.dispatch.value = "new search";	
}

/* Function to view Customers list page */
function viewCustomers() {
    var url = 'searchcustomer.do?invoke=viewcustomer';
	window.location = url;
}
/* Function to view Locations list page */
function viewLocations() {
    var url = 'searchlocation.do?invoke=viewlocationsearch';
	window.location = url;
}
/* Function to view Replace Managers page */
function viewReplaceMasters() {
    var url = 'updatemanagers.do?invoke=updateManagers';
	window.location = url;
}
/* Function to view Sales Managers page */
function viewSalesManagers() {
    var url = 'searchsalesmanager.do?invoke=viewsalesmanagerSearchResult';
	window.location = url;
}

function viewDesignEngineers(){
    var url = 'searchdesignengineer.do?invoke=viewDesignEngineer';
	window.location = url;
}
/* Function to view Help Items list page */
function viewHelpItems() {
    var url = '';
	window.location = url;
}
/*
 * Functions related to Menu Items - END
 */
 
function updateCustomerLocation(objSelect) {
    var selectVal = objSelect.value;

    if ( selectVal.indexOf(":")>0 ) {
        document.forms[0].locationId.value = (selectVal.split(":"))[0];
        document.forms[0].customerId.value = (selectVal.split(":"))[1];
    } else {
        document.forms[0].locationId.value = selectVal;
    }

}
function update1(enquiryId){
	
	var orderClosingDt= document.getElementById('orderClosingMonth').value;
	var targeted= document.getElementById('targeted').value;
	var winningChance= document.getElementById('winningChance').value;	
	var mat = saveEnquiryBasicInfo.updateBasicInfo(enquiryId,orderClosingDt,targeted,winningChance, callBackFun);
	function callBackFun(str){
		if(str!='' && str=='YES'){					
			alert('Update Success');
			return;			
		}
	}  
		
}


function saveAsDraft(type, ratingId){		

	var specialCharacter = /^[a-zA-Z0-9()-]+(\s([a-zA-Z0-9()-])+)*$/;		
	
	var validationFailed = "false";
	
	if (document.getElementById("fileTypestd") != null){
		if (! checkTxtNull(document.enquiryForm.theFile)){		 	
		 	if (! isFileAcceptable(document.enquiryForm.theFile.value, document.getElementById("fileTypestd").value) ) {		
      		 return false;    
  			}
		}
	}	
	else if ( Trim(document.enquiryForm.kw.value) == "" || isNaN(Trim(document.enquiryForm.kw.value)) )	
		validationFailed = "true";	
	else if (document.enquiryForm.poleId.value == "0")	
		validationFailed = "true";	
	else if (document.enquiryForm.scsrId.value == "0")	
		validationFailed = "true";	
	else if ( Trim(document.enquiryForm.volts.value) == "0") 	
		validationFailed = "true";	
	else if (document.enquiryForm.enclosureId.value == "0")	
		validationFailed = "true";
	else if (document.enquiryForm.mountingId.value == "0")	
		validationFailed = "true";	
	else if ( Trim(document.enquiryForm.quantity.value) == "")
		validationFailed = "true";
	else if (document.enquiryForm.salesStage.value == "0")	
		validationFailed = "true";	
	else if (document.enquiryForm.applicationId.value == "0")	
		validationFailed = "true";	
	
	if (document.enquiryForm.enquiryType.value == "0"){
	 	alert("Please select Enquiry Type");
	 	document.enquiryForm.enquiryType.focus();
	}
	else if (document.enquiryForm.customerId.value == "0"){
		alert("Please select Customer Name");	
		document.enquiryForm.customerName.focus();
	} 	
	else if ( Trim(document.enquiryForm.totalOrderValue.value) != "" && isNaN(Trim(document.enquiryForm.totalOrderValue.value)) ){
		alert("Please enter valid Total Order Value");	
		document.enquiryForm.totalOrderValue.focus();
	}
	else if ( Trim(document.enquiryForm.winlossPrice.value) != "" && isNaN(Trim(document.enquiryForm.winlossPrice.value)) ){
		alert("Please enter valid Win Loss Price Value");	
		document.enquiryForm.winlossPrice.focus();
	}
	
	else if (document.enquiryForm.materialCost.value != "" && isNaN(Trim(document.enquiryForm.materialCost.value))){
		alert("Please enter valid Material Cost Value"); 	
		document.enquiryForm.materialCost.focus();
	}
	else if (document.enquiryForm.mcnspRatio.value != "" && isNaN(Trim(document.enquiryForm.mcnspRatio.value))){
	 	alert("Please enter valid MC/NSP Ratio value");
	 	document.enquiryForm.mcnspRatio.focus();
	}
	else if (document.enquiryForm.totalPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPrice.value))){
		alert("Please enter valid Total Price Value"); 
		document.enquiryForm.totalPrice.focus();
	}
	else if (document.enquiryForm.warrantyCost.value != "" && isNaN(Trim(document.enquiryForm.warrantyCost.value))){
	 	alert("Please enter valid Warranty Cost value");
	 	document.enquiryForm.warrantyCost.focus();
	}
	
	else if (document.enquiryForm.ecSupervisionCost.value != "" && isNaN(Trim(document.enquiryForm.ecSupervisionCost.value))){
		alert("Please enter valid Supervision for eraction and commissioning cost Value"); 	
		document.enquiryForm.ecSupervisionCost.focus();
	}
	else if (document.enquiryForm.sparesCost.value != "" && isNaN(Trim(document.enquiryForm.sparesCost.value))){
	 	alert("Please enter valid Spares Cost value");
	 	document.enquiryForm.sparesCost.focus();
	}
	else if (document.enquiryForm.transportationCost.value != "" && isNaN(Trim(document.enquiryForm.transportationCost.value))){
		alert("Please enter valid Transportation Cost (FOR) value"); 
		document.enquiryForm.transportationCost.focus();
	}
	else if (document.enquiryForm.hzAreaCertCost.value  != "" && isNaN(Trim(document.enquiryForm.hzAreaCertCost.value))){
	 	alert("Please enter valid Hazardous area certificate cost value");
	 	document.enquiryForm.hzAreaCertCost.focus();
	}
	else if (document.enquiryForm.totalPkgPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPkgPrice.value))){
	 	alert("Please enter valid Total Package Price value");
	 	document.enquiryForm.totalPkgPrice.focus();
	}
	
	else if ( Trim(document.enquiryForm.quantity.value) != "" && isNaN(Trim(document.enquiryForm.quantity.value)))	
	{
		alert("Please enter a valid quantity value");
		document.enquiryForm.quantity.focus();
		document.enquiryForm.quantity.value = Trim(document.enquiryForm.quantity.value);
	}
	else if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
		alert("Please enter a valid KW value");	
		document.enquiryForm.kw.focus();
	}
	
	else if ( Trim(document.enquiryForm.frequency.value) != "" && isNaN(Trim(document.enquiryForm.frequency.value)) ){
		alert("Please enter a valid Frequency value");
		document.enquiryForm.frequency.focus();
	}
	else if ( Trim(document.enquiryForm.ambience.value) != "" && isNaN(Trim(document.enquiryForm.ambience.value)) ) 	{
		alert("Please enter a valid Ambience value");
		document.enquiryForm.ambience.focus();
	}
	else if ( Trim(document.enquiryForm.altitude.value) != "" && isNaN(Trim(document.enquiryForm.altitude.value)) ) 	{
		alert("Please enter a valid Altitude value");
		document.enquiryForm.altitude.focus();
	}
	else if ( Trim(document.enquiryForm.tempRaise.value) != "" && isNaN(Trim(document.enquiryForm.tempRaise.value)) ) 	{
		alert("Please enter a valid Temperature Rise value");
		document.enquiryForm.tempRaise.focus();
	}
	
	else if ( Trim(document.enquiryForm.coupling.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.coupling.value)) )	{
		alert("Please enter a valid Coupling value, special characters not allowed");
		document.enquiryForm.coupling.focus();
	}
		
	else if ( Trim(document.enquiryForm.paint.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.paint.value)) )	{
		alert("Please enter a valid paint value, special characters not allowed");
		document.enquiryForm.paint.focus();
	}	
	
	/* TODO else if ( Trim(document.enquiryForm.splFeatures.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.splFeatures.value)) )	
		{
			alert("Please enter a valid Special Features, special characters not allowed");
			document.enquiryForm.splFeatures.focus();
		}		*/
	
	
	
	else{		
		alert("Current rating changes will be saved.");	
		
		if (validationFailed == "true")
			document.enquiryForm.isValidated.value = "N";
		else
			document.enquiryForm.isValidated.value = "Y";
		
		if (type == "DRAFT"){
			document.enquiryForm.operationType.value = "DRAFT";				
			document.enquiryForm.submit();
		}else if (type == "ADDNEWRATING"){
			document.enquiryForm.operationType.value = "ADDNEWRATING";	
			
			
			if (ratingId != '')
			{

				document.enquiryForm.ratingOperationId.value = ratingId;
				document.enquiryForm.submit();					
			}else{
				if(confirm("Do you want to copy current rating? Click OK to copy current rating.Click Cancel to create new rating. ")) {
					document.enquiryForm.ratingOperation.value = "COPY";			
				}else{
					document.enquiryForm.ratingOperation.value = "NEW";			
				}						
				document.enquiryForm.submit();		
			}
		}else if (type == "SAVEONLY"){
			document.enquiryForm.operationType.value = "SAVEONLY";				
			document.enquiryForm.submit();
		}else if (type == "NEXTRATING"){
			document.enquiryForm.operationType.value = "NEXTRATING";				
			document.enquiryForm.submit();
		}else if (type == "ADDRATING"){
			

			document.enquiryForm.ratingOperation.value = "NEW";			
			document.enquiryForm.operationType.value = "ADDNEWRATING";				
			document.enquiryForm.submit();
		}

		else if (type == "ADDNEWRATING_RETURNDM"){
			document.getElementById("commentOperation").value="DM_RETURN";
			document.enquiryForm.operationType.value = "ADDNEWRATING";	
			

			
			if (ratingId != '')
			{

				document.enquiryForm.ratingOperationId.value = ratingId;
				document.enquiryForm.submit();					
			}else{
				if(confirm("Do you want to copy current rating? Click OK to copy current rating.Click Cancel to create new rating. ")) {
					document.enquiryForm.ratingOperation.value = "COPY";			
				}else{
					document.enquiryForm.ratingOperation.value = "NEW";			
				}						
				document.enquiryForm.submit();		
			}
		}else if (type == "SAVEONLY_RETURNDM"){
			document.getElementById("commentOperation").value="DM_RETURN";
			document.enquiryForm.operationType.value = "SAVEONLY";				
			document.enquiryForm.submit();
		}else if (type == "NEXTRATING_RETURNDM"){
			document.getElementById("commentOperation").value="DM_RETURN";
			document.enquiryForm.operationType.value = "NEXTRATING";				
			document.enquiryForm.submit();
		}
	}	
}


function saveAsDrafts(type, ratingId){		

	var specialCharacter = /^[a-zA-Z0-9()-]+(\s([a-zA-Z0-9()-])+)*$/;		
	
	var validationFailed = "false";
	
	
	
	if (document.getElementById("fileTypestd") != null){
		if (! checkTxtNull(document.enquiryForm.theFile)){		 	
		 	if (! isFileAcceptable(document.enquiryForm.theFile.value, document.getElementById("fileTypestd").value) ) {		
      		 return false;    
  			}
		}
	}	
	
		
	else if ( Trim(document.enquiryForm.kw.value) == "" || isNaN(Trim(document.enquiryForm.kw.value)) )	
		validationFailed = "true";	
	else if (document.enquiryForm.poleId.value == "0")	
		validationFailed = "true";	
	else if (document.enquiryForm.scsrId.value == "0")	
		validationFailed = "true";	
	else if ( Trim(document.enquiryForm.volts.value) == "0") 	
		validationFailed = "true";	
	/*else if ( Trim(document.enquiryForm.voltsVar.value) == "" || isNaN(Trim(document.enquiryForm.voltsVar.value)) )	
		validationFailed = "true";	*/
	else if (document.enquiryForm.enclosureId.value == "0")	
		validationFailed = "true";
	else if (document.enquiryForm.mountingId.value == "0")	
		validationFailed = "true";	
	else if ( Trim(document.enquiryForm.quantity.value) == "")
		validationFailed = "true";
	else if (document.enquiryForm.salesStage.value == "0")	
		validationFailed = "true";	
 if ( Trim(document.enquiryForm.totalOrderValue.value) != "" && isNaN(Trim(document.enquiryForm.totalOrderValue.value)) ){
		alert("Please enter valid Total Order Value");	
		document.enquiryForm.totalOrderValue.focus();
	}
	else if ( Trim(document.enquiryForm.winlossPrice.value) != "" && isNaN(Trim(document.enquiryForm.winlossPrice.value)) ){
		alert("Please enter valid Win Loss Price Value");	
		document.enquiryForm.winlossPrice.focus();
	}
	
	else if (document.enquiryForm.materialCost.value != "" && isNaN(Trim(document.enquiryForm.materialCost.value))){
		alert("Please enter valid Material Cost Value"); 	
		document.enquiryForm.materialCost.focus();
	}
	else if (document.enquiryForm.mcnspRatio.value != "" && isNaN(Trim(document.enquiryForm.mcnspRatio.value))){
	 	alert("Please enter valid MC/NSP Ratio value");
	 	document.enquiryForm.mcnspRatio.focus();
	}
	else if (document.enquiryForm.totalPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPrice.value))){
		alert("Please enter valid Total Price Value"); 
		document.enquiryForm.totalPrice.focus();
	}
	else if (document.enquiryForm.warrantyCost.value != "" && isNaN(Trim(document.enquiryForm.warrantyCost.value))){
	 	alert("Please enter valid Warranty Cost value");
	 	document.enquiryForm.warrantyCost.focus();
	}
	
	else if (document.enquiryForm.ecSupervisionCost.value != "" && isNaN(Trim(document.enquiryForm.ecSupervisionCost.value))){
		alert("Please enter valid Supervision for eraction and commissioning cost Value"); 	
		document.enquiryForm.ecSupervisionCost.focus();
	}
	else if (document.enquiryForm.sparesCost.value != "" && isNaN(Trim(document.enquiryForm.sparesCost.value))){
	 	alert("Please enter valid Spares Cost value");
	 	document.enquiryForm.sparesCost.focus();
	}
	else if (document.enquiryForm.transportationCost.value != "" && isNaN(Trim(document.enquiryForm.transportationCost.value))){
		alert("Please enter valid Transportation Cost (FOR) value"); 
		document.enquiryForm.transportationCost.focus();
	}
	else if (document.enquiryForm.hzAreaCertCost.value  != "" && isNaN(Trim(document.enquiryForm.hzAreaCertCost.value))){
	 	alert("Please enter valid Hazardous area certificate cost value");
	 	document.enquiryForm.hzAreaCertCost.focus();
	}
	else if (document.enquiryForm.totalPkgPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPkgPrice.value))){
	 	alert("Please enter valid Total Package Price value");
	 	document.enquiryForm.totalPkgPrice.focus();
	}
	
	else if ( Trim(document.enquiryForm.quantity.value) != "" && isNaN(Trim(document.enquiryForm.quantity.value)))	
	{
		alert("Please enter a valid quantity value");
		document.enquiryForm.quantity.focus();
		document.enquiryForm.quantity.value = Trim(document.enquiryForm.quantity.value);
	}
	else if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
		alert("Please enter a valid KW value");	
		document.enquiryForm.kw.focus();
	}
	
	else if ( Trim(document.enquiryForm.frequency.value) != "" && isNaN(Trim(document.enquiryForm.frequency.value)) ){
		alert("Please enter a valid Frequency value");
		document.enquiryForm.frequency.focus();
	}
	else if ( Trim(document.enquiryForm.ambience.value) != "" && isNaN(Trim(document.enquiryForm.ambience.value)) ) 	{
		alert("Please enter a valid Ambience value");
		document.enquiryForm.ambience.focus();
	}
	else if ( Trim(document.enquiryForm.altitude.value) != "" && isNaN(Trim(document.enquiryForm.altitude.value)) ) 	{
		alert("Please enter a valid Altitude value");
		document.enquiryForm.altitude.focus();
	}
	else if ( Trim(document.enquiryForm.tempRaise.value) != "" && isNaN(Trim(document.enquiryForm.tempRaise.value)) ) 	{
		alert("Please enter a valid Temperature Rise value");
		document.enquiryForm.tempRaise.focus();
	}
	
	else if ( Trim(document.enquiryForm.coupling.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.coupling.value)) )	{
		alert("Please enter a valid Coupling value, special characters not allowed");
		document.enquiryForm.coupling.focus();
	}
		
	else if ( Trim(document.enquiryForm.paint.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.paint.value)) )	{
		alert("Please enter a valid paint value, special characters not allowed");
		document.enquiryForm.paint.focus();
	}	
	
	/*TODO else if ( Trim(document.enquiryForm.splFeatures.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.splFeatures.value)) )	
		{
			alert("Please enter a valid Special Features, special characters not allowed");
			document.enquiryForm.splFeatures.focus();
		}	*/	
	
				
	
	
	else{		
		if (validationFailed == "true")
			document.enquiryForm.isValidated.value = "N";
		else
			document.enquiryForm.isValidated.value = "Y";
		
		 if (type == "ADDNEWRATING"){
			
			document.enquiryForm.operationType.value = "ADDNEWRATING";	
			

			
			if (ratingId != ''){
				alert("Current rating changes will be saved.");
				document.enquiryForm.ratingOperationId.value = ratingId;
				document.enquiryForm.submit();					
			}
		}
		if(type=="ADDNEWRATING_RETURNDM")
		{
			document.enquiryForm.operationType.value = "ADDNEWRATING";	
			
			document.getElementById("commentOperation").value="DM_RETURN";
			
			if (ratingId != ''){
				alert("Current rating changes will be saved.");
				document.enquiryForm.ratingOperationId.value = ratingId;
				document.enquiryForm.submit();					
			}
	
		}
	}//Else Part	
}

function submitEnquiry(value){	

	var specialCharacter = /^[a-zA-Z0-9()-]+(\s([a-zA-Z0-9()-])+)*$/;	
	 var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;

	
	var errorMessage = Trim(document.enquiryForm.ratingsValidationMessage.value);
	
	if (document.getElementById("fileTypestd") != null){
		if (! checkTxtNull(document.enquiryForm.theFile)){		 	
		 	if (! isFileAcceptable(document.enquiryForm.theFile.value, document.getElementById("fileTypestd").value) ) {		
      		 return false;    
  			}
		}
	}

	if (document.enquiryForm.enquiryType.value == "0"){
		alert("Please select Enquiry Type");
		document.enquiryForm.enquiryType.focus();
	}	
	else if (document.enquiryForm.customerId.value == "0"){
		alert("Please select Customer Name");	
		document.enquiryForm.customerName.focus();
	}
	else if (document.enquiryForm.customerType.value == ""){
		alert("Please select Customer Type");	
		document.enquiryForm.customerType.focus();
	}
	else if ( Trim(document.enquiryForm.totalOrderValue.value) != "" && isNaN(Trim(document.enquiryForm.totalOrderValue.value)) ){
		alert("Please enter valid Total Order Value");	
		document.enquiryForm.totalOrderValue.focus();
	}

	else if(Trim(document.enquiryForm.endUser.value) != "" &&  !(Trim(document.enquiryForm.endUser.value).match(alpha)))
	{
	   alert("End User Must Allows Only [A-Z][Space Characters]");
		document.enquiryForm.endUser.focus();
	}
	else if ( Trim(document.enquiryForm.winlossPrice.value) != "" && isNaN(Trim(document.enquiryForm.winlossPrice.value)) ){
		alert("Please enter valid Win Loss Price Value");	
		document.enquiryForm.winlossPrice.focus();
	}
	
	else if (document.enquiryForm.materialCost.value != "" && isNaN(Trim(document.enquiryForm.materialCost.value))){
		alert("Please enter valid Material Cost Value"); 	
		document.enquiryForm.materialCost.focus();
	}
	else if (document.enquiryForm.mcnspRatio.value != "" && isNaN(Trim(document.enquiryForm.mcnspRatio.value))){
	 	alert("Please enter valid MC/NSP Ratio value");
	 	document.enquiryForm.mcnspRatio.focus();
	}
	else if (document.enquiryForm.totalPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPrice.value))){
		alert("Please enter valid Total Price Value"); 
		document.enquiryForm.totalPrice.focus();
	}
	else if (document.enquiryForm.warrantyCost.value != "" && isNaN(Trim(document.enquiryForm.warrantyCost.value))){
	 	alert("Please enter valid Warranty Cost value");
	 	document.enquiryForm.warrantyCost.focus();
	}
	
	else if (document.enquiryForm.advancePayment.value== "" ||  document.enquiryForm.advancePayment.value== 0){
	 	
		document.enquiryForm.advancePayment.value=20;
	}
	
	else if (document.enquiryForm.paymentTerms.value== "" ||   document.enquiryForm.paymentTerms.value== 0){
	 	
		document.enquiryForm.paymentTerms.value=80;
	}	
	
	else if (document.enquiryForm.ecSupervisionCost.value != "" && isNaN(Trim(document.enquiryForm.ecSupervisionCost.value))){
		alert("Please enter valid Supervision for eraction and commissioning cost Value"); 	
		document.enquiryForm.ecSupervisionCost.focus();
	}
	else if (document.enquiryForm.sparesCost.value != "" && isNaN(Trim(document.enquiryForm.sparesCost.value))){
	 	alert("Please enter valid Spares Cost value");
	 	document.enquiryForm.sparesCost.focus();
	}
	else if (document.enquiryForm.transportationCost.value != "" && isNaN(Trim(document.enquiryForm.transportationCost.value))){
		alert("Please enter valid Transportation Cost (FOR) value"); 
		document.enquiryForm.transportationCost.focus();
	}
	else if (document.enquiryForm.hzAreaCertCost.value  != "" && isNaN(Trim(document.enquiryForm.hzAreaCertCost.value))){
	 	alert("Please enter valid Hazardous area certificate cost value");
	 	document.enquiryForm.hzAreaCertCost.focus();
	}
	else if (document.enquiryForm.totalPkgPrice.value != "" && isNaN(Trim(document.enquiryForm.totalPkgPrice.value))){
	 	alert("Please enter valid Total Package Price value");
	 	document.enquiryForm.totalPkgPrice.focus();
	}
		else if ( Trim(document.enquiryForm.quantity.value) == "") {
		alert("Please enter Quantity value");
		document.enquiryForm.quantity.focus();
	}else if ( Trim(document.enquiryForm.quantity.value) != "" && isNaN(Trim(document.enquiryForm.quantity.value))) {
		alert("Please enter a valid Quantity value");		
		document.enquiryForm.quantity.focus();
	}
	else if ( Trim(document.enquiryForm.kw.value) == ""){
		alert("Please enter KW value");
		document.enquiryForm.kw.focus();
	}
	else if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
		alert("Please enter a valid KW value");	
		document.enquiryForm.kw.focus();
	}else if (document.enquiryForm.poleId.value == "0"){
		alert("Please select Pole");
		document.enquiryForm.poleId.focus();
	}
	else if (document.enquiryForm.scsrId.value == "0"){
		alert("Please select SC/SR");	
		document.enquiryForm.scsrId.focus();
	}else if ( Trim(document.enquiryForm.volts.value) == "0"){
		alert("Please select Volts");	
		document.enquiryForm.volts.focus();
	}
	else if (document.enquiryForm.enclosureId.value == "0")	{
		alert("Please select Enclosure");		
		document.enquiryForm.enclosureId.focus();
	}	
	else if (document.enquiryForm.mountingId.value == "0")	 {
		alert("Please select Mounting ");				
		document.enquiryForm.mountingId.focus();
	}
	else if (document.enquiryForm.applicationId.value == "0")	 {
		alert("Please select Application ");				
		document.enquiryForm.applicationId.focus();
	}

	else if ( Trim(document.enquiryForm.frequency.value) != "" && isNaN(Trim(document.enquiryForm.frequency.value)) ){
		alert("Please enter a valid Frequency value");
		document.enquiryForm.frequency.focus();
	}
	else if ( Trim(document.enquiryForm.ambience.value) != "" && isNaN(Trim(document.enquiryForm.ambience.value)) ) 	{
		alert("Please enter a valid Ambience value");
		document.enquiryForm.ambience.focus();
	}
	else if ( Trim(document.enquiryForm.altitude.value) != "" && isNaN(Trim(document.enquiryForm.altitude.value)) ) 	{
		alert("Please enter a valid Altitude value");
		document.enquiryForm.altitude.focus();
	}
	else if ( Trim(document.enquiryForm.tempRaise.value) != "" && isNaN(Trim(document.enquiryForm.tempRaise.value)) ) 	{
		alert("Please enter a valid Temperature Rise value");
		document.enquiryForm.tempRaise.focus();
	}
	
	else if ( Trim(document.enquiryForm.coupling.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.coupling.value)) )	{
		alert("Please enter a valid Coupling value, special characters not allowed");
		document.enquiryForm.coupling.focus();
	}
		
	else if ( Trim(document.enquiryForm.paint.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.paint.value)) )	{
		alert("Please enter a valid paint value, special characters not allowed");
		document.enquiryForm.paint.focus();
	}	
	
	/*TODO else if ( Trim(document.enquiryForm.splFeatures.value) != "" && ! specialCharacter.test(Trim(document.enquiryForm.splFeatures.value)) )	
		{
			alert("Please enter valid Special Features, special characters not allowed");
			document.enquiryForm.splFeatures.focus();
	}*/	
	
	else if (errorMessage != '' && errorMessage.length > 0){
		var temp = Trim(errorMessage).split(",")	
		alert(temp);
		if (temp.length > 1){
			alert(errorMessage+" are incomplete. Please enter all values for these ratings, before submitting the enquiry");	
		}else {
			alert(errorMessage+" is incomplete. Please enter all values for this rating, before submitting the enquiry");	
		}		
	}else{
		document.enquiryForm.isValidated.value = "Y";
		document.enquiryForm.quantity.value = Trim(document.enquiryForm.quantity.value);
		document.enquiryForm.operationType.value = value;	
		document.enquiryForm.submit();
	}
}

function submitViewEnquiry(){		
	var errorMessage = Trim(document.enquiryForm.ratingsValidationMessage.value);
	
	if (errorMessage != '' && errorMessage.length > 0){		
		var temp = Trim(errorMessage).split(",")		
		if (temp.length > 1){
			alert(errorMessage+" are incomplete. Please enter all values for these ratings, before submitting the enquiry");	
		}else {
			alert(errorMessage+" is incomplete. Please enter all values for this rating, before submitting the enquiry");	
		}		
	}
	else{
		document.enquiryForm.submit();
	}
}

function deleteRating(ratingId){
	var enquiryId = document.enquiryForm.enquiryId.value;
	if (ratingId != '' && enquiryId != ''){
		if(confirm("Are you sure to delete the rating?")) {
			window.location = 'createEnquiry.do?invoke=deleteRating&ratingId='+ratingId+'&enquiryId='+enquiryId;
		}
	}
}

function editRating(ratingId){
	var enquiryId = document.enquiryForm.enquiryId.value;	
	window.location = 'createEnquiry.do?invoke=getEnquiryDetails&type=edit&ratingId='+ratingId+'&enquiryId='+enquiryId;
	
}

function Next()
		{

			document.form1.submit();
			alert('Ok');
		}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function clearEnquirySearch(){
document.SearchEnquiryForm.enquiryNumber.value="";
document.SearchEnquiryForm.createdStartDate.value="";
document.SearchEnquiryForm.createdEndDate.value="";
document.SearchEnquiryForm.customerName.value="";
document.SearchEnquiryForm.customerId.value="";
document.SearchEnquiryForm.createdBy.value="";
document.SearchEnquiryForm.assignTo.value="";
document.SearchEnquiryForm.designedBy.value="";
document.SearchEnquiryForm.statusId.value="";
document.SearchEnquiryForm.poleId.value="";
document.SearchEnquiryForm.volts.value="";
document.SearchEnquiryForm.kw.value="";
document.SearchEnquiryForm.mounting.value="";
document.SearchEnquiryForm.frameName.value="";
document.SearchEnquiryForm.createdBySSO.value="";
document.SearchEnquiryForm.savingIndent.value="";
document.SearchEnquiryForm.scsrId.value="";
document.SearchEnquiryForm.orderClsStartDate.value="";
document.SearchEnquiryForm.orderClsEndDate.value="";
document.SearchEnquiryForm.enquiryReferenceNumber.value="";
document.SearchEnquiryForm.projectName.value="";
document.SearchEnquiryForm.endUserIndustry.value="";
document.SearchEnquiryForm.applicationId.value="";
document.SearchEnquiryForm.enclosureId.value="";

}

/* Function to copy the Enquiry */
function copyEnquiry(enquiryId, isNewEnquiry) {
	document.SearchEnquiryForm.method="POST";
	document.SearchEnquiryForm.invoke.value="copyEnquiry";
	document.SearchEnquiryForm.enquiryId.value=enquiryId;
	if(isNewEnquiry != null && isNewEnquiry == 'Y') {
		document.SearchEnquiryForm.action="newPlaceAnEnquiry.do";
	} else {
		document.SearchEnquiryForm.action="createEnquiry.do";
	}
	document.SearchEnquiryForm.submit();
}

/* Function to copy the Enquiry */
function editEnquiry(enquiryId) {

document.SearchEnquiryForm.method="POST";
document.SearchEnquiryForm.action="createEnquiry.do?invoke=getEnquiryDetails&type=edit&enquiryId="+enquiryId;
document.SearchEnquiryForm.submit();
}

/* Function to delete the Enquiry */
function deleteEnquiry(enquiryId) {
if(confirm("Are you sure to delete the Enquiry?")) {
document.SearchEnquiryForm.invoke.value="deleteEnquiry";
document.SearchEnquiryForm.enquiryId.value=enquiryId;
document.SearchEnquiryForm.method="POST";
document.SearchEnquiryForm.action="createEnquiry.do"
document.SearchEnquiryForm.submit();
}
}
/* Function to print or Export the Enquiry */
function printOrExportResults(sFunctionType) 
{
var searchType = document.SearchEnquiryForm.searchType.value;
var printSearch="searchenquiry.do?functionType="+sFunctionType+"&invoke=printOrExportEnquiryResults&searchType="+searchType;
	props='toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1';	
	
	if(sFunctionType == "print")
		//window.open(printSearch);
		//window.location  = printSearch;
	window.open(printSearch, '_blank');
	else
		window.location  = printSearch;
	
	

}
function clearMaster()
{
	document.MasterForm.master.value="";
	document.MasterForm.master.focus();
	
}

function addorupdateMaster(addUpdateFlag)
{
	var specialCharacter = /^[0-9()-]+(\s([0-9()-])+)*$/;
	var specialCharacter1 = /^[a-zA-Z()-]+(\s([a-zA-Z()-])+)*$/;	

	var MasterId = document.MasterForm.masterId.value;
	var MasterName=document.MasterForm.master.value;
	
	var firstCharecter = MasterName.charAt(0);
	if (Trim(document.MasterForm.master.value) == "" ){
  		alert("Please enter Master Name");
  		document.MasterForm.master.focus();
  		return false;      	
	} 
	if(!specialCharacter1.test(firstCharecter))
	{
		alert("Please enter Valid Master Name");
  		document.MasterForm.master.focus();
  		return false; 
	}
	
	document.MasterForm.invoke.value="addorUpdateMaster";
	document.MasterForm.addOrUpdate.value=addUpdateFlag;
	document.MasterForm.method="POST";
	document.MasterForm.action="masters.do";
	document.MasterForm.submit();
	
}
function addMaster()
{

	document.MasterForm.invoke.value="viewMaster";
	document.MasterForm.master.value="";
	document.MasterForm.method="POST";
	document.MasterForm.action="masters.do";
	document.MasterForm.submit();

}
function editMaster(MasterId)
{ 
	document.MasterForm.invoke.value="viewMaster";
	document.MasterForm.masterId.value=MasterId;
	document.MasterForm.method="POST";
	document.MasterForm.action="masters.do";
	document.MasterForm.submit();
	

}
function deleteMaster(masterId,master)
{
if(confirm("Are you sure to delete the Master?")) {
	document.MasterForm.invoke.value="deleteMaster";
	document.MasterForm.masterId.value=masterId;
	document.MasterForm.master.value=master;
	document.MasterForm.method="POST";
	document.MasterForm.action="masters.do";
	document.MasterForm.submit();
	}
	

	
}
/* Function to print or Export the Master */
function printOrExportMaster(sFunctionType) 
{

var printSearch="masters.do?functionType="+sFunctionType+"&invoke=printOrExportMasterResults";
	props='toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1';	
	if(sFunctionType == "print")
		window.open(printSearch);
	else
		window.location  = printSearch;

}
function loadEngineeringData(masterId,master)
{
	document.MastersDataForm.invoke.value="viewSearchMastersData";
	document.MastersDataForm.masterId.value=masterId;
	document.MastersDataForm.master.value=master;
	document.MastersDataForm.method="POST";
	document.MastersDataForm.action="mastersdata.do";
	document.MastersDataForm.submit();
	

	
}

function clearMastersDataSearch()
{
	 document.MastersDataForm.engineeringName.value="";
	

	
}
function loadMasterData(masterId,master)
{
	document.MasterForm.invoke.value="viewSearchMastersData";
	
	document.MasterForm.masterId.value=masterId;
	document.MasterForm.master.value=master;
	document.MasterForm.method="POST";
	document.MasterForm.action="mastersdata.do";
	document.MasterForm.submit();
	

	
}
function addEngineeringData()
{
     document.MastersDataForm.engineeringName.value="";
	document.MastersDataForm.invoke.value="viewMastersData";
	document.MastersDataForm.method="POST";
	document.MastersDataForm.action="mastersdata.do";
	document.MastersDataForm.submit();
	

	
}
function editEngineeringData(engId)
{

	document.MastersDataForm.invoke.value="viewMastersData";
	document.MastersDataForm.method="POST";
	document.MastersDataForm.engineeringId.value=engId;
	document.MastersDataForm.action="mastersdata.do";
	document.MastersDataForm.submit();
	

	
}
function deleteEngineeringData(engId,engName)
{

if(confirm("Are you sure to delete the Master Data?")) {
	document.MastersDataForm.invoke.value="deleteMasterData";
	document.MastersDataForm.method="POST";
	document.MastersDataForm.engineeringId.value=engId;
	document.MastersDataForm.engineeringName.value=engName;
	
	
	document.MastersDataForm.action="mastersdata.do";
	document.MastersDataForm.submit();
	}

	
}
function addorupdateEngineeringData(addUpdateFlag)
{
	var specialCharacter = /^[0-9()-]+(\s([0-9()-])+)*$/;
	var specialCharacter1 = /^[a-zA-Z()-]+(\s([a-zA-Z()-])+)*$/;
	var numerics=/^[0-9]+$/;
   		
	
	var MasterName = document.MastersDataForm.engineeringName.value;
	
	
	var master_Name=document.MastersDataForm.masterName.value;
	
	if (Trim(document.MastersDataForm.engineeringName.value) == "" ){
  		alert("Please enter Master Data Name");
  		document.MastersDataForm.engineeringName.focus();
  		return false;      	
	} 
	
	var firstCharecter = MasterName.charAt(0);


	if(master_Name=="Pole" && !numerics.test(MasterName))
	 {
		alert("Please enter Valid Master Date Name It Accepts Only Numbers");
  		document.MastersDataForm.engineeringName.focus();
  		return false; 

	 }
	
	document.MastersDataForm.invoke.value="addorUpdateMasterData";
	document.MastersDataForm.addOrUpdate.value=addUpdateFlag;
	document.MastersDataForm.method="POST";
	document.MastersDataForm.action="mastersdata.do";
	document.MastersDataForm.submit();
	
}

/* Function to print or Export the Master's Data */
function printOrExportMasterData(sFunctionType) 
{
var masterId=document.MastersDataForm.masterId.value;
var master=document.MastersDataForm.master.value;
var printSearch="mastersdata.do?functionType="+sFunctionType+"&invoke=printOrExportMasterData&masterId="+masterId+"&master="+master;
	props='toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1';	
	
	if(sFunctionType == "print")
		window.open(printSearch);
	else
		window.location  = printSearch;

}
function clearOption(targetObjOriginal){
	var mySplitResult = targetObjOriginal.split(",");
	for(i = 0; i < mySplitResult.length; i++){
		var splitObject = mySplitResult[i];
		var targetObj = document.getElementById(mySplitResult[i]);		
		var mySplitResult1 = splitObject.split(":");
		if (mySplitResult1.length > 0){
			var targetObj1 = document.getElementById(mySplitResult1[0]);
			if ( targetObj1.type=="select-one" ) {
				targetObj1.options.length=0;
            	targetObj1.options[0] = new Option("Select"); 
    	    	targetObj1.options[0].value = "";
    	    	targetObj1.options[0].selected;
			} else {
		    	targetObj1.value='';
			}					
		}
	}
}

function onCustomerChange(customerObj, targetObjs, toggleName, displayContactPerson){
	if (Trim(customerObj.value) != "" &&  Trim(customerObj.value) != "Select") {		
		clearOption(targetObjs);
		//var url = "createEnquiry.do?invoke=loadCustomerLookUp&selectedOption=" + Trim(customerObj.value)+ "&targetObjects=" +Trim(targetObjs);
		var url = "createEnquiry.do?invoke=loadCustomerLookUp";
		data = "";
		data += "&selectedOption=" + Trim(customerObj.value)+ "&targetObjects=" +Trim(targetObjs);		
		data += "&displaycontactperson=" + displayContactPerson;
		url += data;
	    invokeAjaxFunctionality(url, data, toggleName);		
	}else {
         clearOption(targetObjs);
	} 
}



function nextRating(){	
	ratingId = document.enquiryForm.nextRatingId.value;
	var validationFailed = "false";		
	selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");
	var objRatedSpeed = document.enquiryForm.rpm;
	var objRatedCurrent = document.enquiryForm.flcAmps;
	var objNlCurrent = document.enquiryForm.nlCurrent;	
	var objNominalTorque = document.enquiryForm.fltSQCAGE;
	var objStrtgCurrentSel = document.getElementById('strtgCurrentSelect');
	var objStrtgCurrentText = document.getElementById('strtgCurrentText');
	var objLockedRotorTorque = document.enquiryForm.lrTorque;
	var objMaxTorque = document.enquiryForm.potFLT;
	var objsstHot = document.enquiryForm.sstHot;
	var objsstCold = document.enquiryForm.sstCold;
	var objStartTimeRV = document.enquiryForm.startTimeRV;	
	var objStartTimeRV80 = document.enquiryForm.startTimeRV80;	


									
		document.enquiryForm.currentRating.value = ratingId;	
		document.enquiryForm.operationType.value = 'updaterating';	

		if (checkTxtNull(document.enquiryForm.areaClassification)){
			validationFailed = "true";
		}
		
		if (checkTxtNull(document.enquiryForm.frameSize)){
			validationFailed = "true";

		}

		if (!checkValidText(document.enquiryForm.frameSize)) {
			
				alert("Please enter valid Frame Size. ");
				document.enquiryForm.frameSize.focus();
				validationFailed = "true";
			
		}
		if (checkTxtNull(document.enquiryForm.mountingId)){
			validationFailed = "true";

		}
		
		if (checkTxtNull(document.enquiryForm.kw)){
			validationFailed = "true";

		}
		if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
			alert("Please enter a valid Rated Output value");	
			document.enquiryForm.kw.focus();
			validationFailed = "true";
		}
		if (checkTxtNull(document.enquiryForm.serviceFactor)){
			validationFailed = "true";

		}
		
		if (checkTxtNull(document.enquiryForm.dutyId)){
			validationFailed = "true";

		}
		if (checkTxtNull(document.enquiryForm.volts)){
			validationFailed = "true";

		}
		if (checkTxtNull(document.enquiryForm.frequency)){
			validationFailed = "true";

		}
		
		if (checkTxtNull(document.enquiryForm.poleId)){
			validationFailed = "true";

		}
		if (checkTxtNull(document.enquiryForm.insulationId)){
			validationFailed = "true";

		}
		
		if (checkTxtNull(document.enquiryForm.tempRaise)){
			validationFailed = "true";

		}
		var ambienceVal ="";
		var objAmbienceSel = document.getElementById('ambienceSel');
		  var objAmbienceText = document.getElementById('ambienceText');
		  if (checkTxtNull(objAmbienceSel)){
			  if (checkTxtNull(objAmbienceText)){
				  validationFailed = "true";

			}
		  }
		  if(objAmbienceSel.value!=null){
			  ambienceVal =objAmbienceSel.value;
		  }
		  else
			  {
			  ambienceVal = objAmbienceText.value
			  }
		
		if (  ambienceVal != null && isNaN(ambienceVal) ) {

		        alert('Please enter only numbers to Ambient temperature.');
		        objAmbienceText.focus();
		}
		
		
		var objAltitude = document.enquiryForm.altitude;
		if (checkTxtNull(objAltitude)) {
			validationFailed = "true";

		}
		if (  objAltitude != null && isNaN(objAltitude.value) ) {

		        alert('Please enter only numbers to Altitude.');
		        objAmbience.focus();
		}
		if (objAltitude != null && parseInt(objAltitude.value) < 0 ) {

				alert('Please specify Altitude greater than 0.');
				objAltitude.focus();
	}
		if (checkTxtNull(document.enquiryForm.degreeOfProId)){

			validationFailed = "true";

		}

		if (!checkValidText(document.enquiryForm.degreeOfProId)) {

				alert("Please enter valid Degree of protection. ");
				document.enquiryForm.degreeOfProId.focus();
				validationFailed = "true";
		}
		
		if (checkTxtNull(document.enquiryForm.coolingSystem)){
			validationFailed = "true";

		}
		if (checkTxtNull(document.enquiryForm.spaceHeater)){
			validationFailed = "true";

		}
		
		var objwindingRTDSel = document.getElementById('windingRTDSel');
		  var objwindingRTDText = document.getElementById('windingRTDText');
		  if (checkTxtNull(objwindingRTDSel)){
			  if (checkTxtNull(objwindingRTDText)){
				  validationFailed = "true";

			}
		  }
		  
		  var objbearingRTDSel = document.getElementById('bearingRTDSel');
		  var objbearingRTDText = document.getElementById('bearingRTDText');
		  if (checkTxtNull(objbearingRTDSel)){
			  if (checkTxtNull(objbearingRTDText)){
				  validationFailed = "true";

			}
		  }
		  
		  var objmethodOfStgSel = document.getElementById('methodOfStgSel');
		  var objmethodOfStgText = document.getElementById('methodOfStgText');
		  if (checkTxtNull(objmethodOfStgSel)){
			  if (checkTxtNull(objmethodOfStgText)){
				  validationFailed = "true";

			}
		  }
		

		  var commercialMCUnit=document.getElementById('commercialMCUnit').value;
		  if (commercialMCUnit==""){
			  validationFailed = "true";
			}

		  
		  if((parseFloat(commercialMCUnit)==0 ) || isNaN(commercialMCUnit.replace(/,/g, "")))
		  {
			  validationFailed = "true";
			  
		  }
		if (selectedValue == "D"){
			var objRowIndex = document.forms[0].hidRowIndex;
			for ( var i=1; i<=parseInt(objRowIndex.value); i++ ) {
			  var obj = eval("document.forms[0].addOnType"+i);
			  var objQuantity = eval("document.forms[0].addOnQuantity"+i);
			  var objUnitPrice = eval("document.forms[0].addOnUnitPrice"+i);
			  
			  		  
			  if(obj!=null){
		   	  if ( objQuantity != null && isNaN(objQuantity.value) ) {
			   	  
/*	  TODO  	        alert('Please enter only numbers to specify quantity.');
	        	    objQuantity.focus();
*/	        	     }
	          if ( objQuantity != null && parseInt(objQuantity.value) < 0 ) {
	          		
/*	TODO					alert('Please specify quantity greater than 0.');
						objQuantity.focus();
*/				 }		
			  if (objUnitPrice != null && !checkValidDoubleText(objUnitPrice)) {

/*		TODO			alert("Please enter valid Unit price");
					objUnitPrice.focus();
*/			  } // If Loop Ending
			  } //Object Checking
			}		  
		}

		
		

		
		
		
		
if (ratingId != ''){	
		

	if (validationFailed == "true")

			document.enquiryForm.isValidated.value = 'N';
		else
			document.enquiryForm.isValidated.value = 'Y';
		
		
		if ( (selectedValue == "D") && (! validateAddOns()) )
			alert("Add-on type values cannot be same, please enter a different value");
		else
			document.enquiryForm.submit();
	}
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode < 48 || charCode > 57) {
        return false;
    }
    return true;
}
function AllowAlphabet(evt) {
	 var charCode = (evt.which) ? evt.which : event.keyCode;
	 if ((charCode > 64 && charCode < 91) 
		  || 
		 (charCode > 96 && charCode < 123)
		 ||
		 (charCode > 47 && charCode < 58)
		 ||
		 (charCode==32)
	     )
	 return true;
	 else
	return false;
	}

function setNumber(fieldName)
{
	
	var number=0;
	var number2=0;
	if(fieldName!="" && fieldName=="voltsVar")
	{
		number=document.getElementById("voltsVar").value;	
		number2=10;
	}
	if(fieldName!="" && fieldName=="frequencyVar")
	{
		number=document.getElementById("frequencyVar").value;	
		number2=5;
	}
	
	if(number<1 || number>10)
	{
		if(fieldName!="" && fieldName=="voltsVar")
		{
			document.getElementById("voltsVar").value=number2;
		}
		if(fieldName!="" && fieldName=="frequencyVar")
		{
			document.getElementById("frequencyVar").value=number2;
		}
			
	}

}
function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    
    
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }

    if(dotPos==0)
    {
    	
       return false;	
    }
    //Single Decimal Is Accepted After Decimal Point
    if(dotPos>0)
    {
    var checkdot=(caratPos + 1) - dotPos;
    
    if(checkdot>2)
    	{
    	   return false;
    	}
    }


    
    return true;
}


function getSelectionStart(o) {
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', o.value.length)
		if (r.text == '') return o.value.length
		return o.value.lastIndexOf(r.text)
	} else return o.selectionStart
}


function updateRatingsData(){	
	selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");		
	document.enquiryForm.operationType.value = 'updaterating';
	if (validationOfferDataPage("update"))
		document.enquiryForm.isValidated.value = 'Y';
	else
		document.enquiryForm.isValidated.value = 'N';
	
	
	
	if ( (selectedValue == "D") && (! validateAddOns()) )
		alert("Add-on type values cannot be same, please enter a different value");
	else
		document.enquiryForm.submit();
}

function submitToManager(){	
	selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");				
	if (validationOfferDataPage("submit")){
		document.enquiryForm.operationType.value = 'updaterating';
		document.enquiryForm.dispatch.value = 'manager';
		document.enquiryForm.isValidated.value = 'Y';	
		if ( (selectedValue == "D") && (! validateAddOns()) )			
			alert("Add-on type values cannot be same, please enter a different value");
		else
			document.enquiryForm.submit();	
	}
}




function getNextRating(ratingId)
{
	var validationFailed = "false";	
	

selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");

var objRatedSpeed = document.enquiryForm.rpm;
var objRatedCurrent = document.enquiryForm.flcAmps;
var objNlCurrent = document.enquiryForm.nlCurrent;	
var objNominalTorque = document.enquiryForm.fltSQCAGE;
var objStrtgCurrentSel = document.getElementById('strtgCurrentSelect');
var objStrtgCurrentText = document.getElementById('strtgCurrentText');
var objLockedRotorTorque = document.enquiryForm.lrTorque;
var objMaxTorque = document.enquiryForm.potFLT;
var objsstHot = document.enquiryForm.sstHot;
var objsstCold = document.enquiryForm.sstCold;
var objStartTimeRV = document.enquiryForm.startTimeRV;	
var objStartTimeRV80 = document.enquiryForm.startTimeRV80;	


	if (checkTxtNull(document.enquiryForm.areaClassification)){
		validationFailed = "true";
	}
	
	if (checkTxtNull(document.enquiryForm.frameSize)){
		validationFailed = "true";

	}

	if (!checkValidText(document.enquiryForm.frameSize)) {
		
			alert("Please enter valid Frame Size. ");
			document.enquiryForm.frameSize.focus();
			validationFailed = "true";
		
	}
	if (checkTxtNull(document.enquiryForm.mountingId)){
		validationFailed = "true";

	}
	
	if (checkTxtNull(document.enquiryForm.kw)){
		validationFailed = "true";

	}
	
	if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
		alert("Please enter a valid Rated Output value");	
		document.enquiryForm.kw.focus();
		validationFailed = "true";
	}
	if (checkTxtNull(document.enquiryForm.serviceFactor)){
		validationFailed = "true";

	}
	
	if (checkTxtNull(document.enquiryForm.dutyId)){
		validationFailed = "true";

	}
	if (checkTxtNull(document.enquiryForm.volts)){
		validationFailed = "true";

	}
	if (checkTxtNull(document.enquiryForm.frequency)){
		validationFailed = "true";

	}
	
	if (checkTxtNull(document.enquiryForm.poleId)){
		validationFailed = "true";

	}
	
	
	if (checkTxtNull(document.enquiryForm.insulationId)){
		validationFailed = "true";

	}
	
	if (checkTxtNull(document.enquiryForm.tempRaise)){
		validationFailed = "true";

	}
	
	var ambienceVal ="";
	var objAmbienceSel = document.getElementById('ambienceSel');
	  var objAmbienceText = document.getElementById('ambienceText');
	  if (checkTxtNull(objAmbienceSel)){
		  if (checkTxtNull(objAmbienceText)){
			  validationFailed = "true";

		}
	  }
	  if(objAmbienceSel.value!=null){
		  ambienceVal =objAmbienceSel.value;
	  }
	  else
		  {
		  ambienceVal = objAmbienceText.value
		  }
	
	if (  ambienceVal != null && isNaN(ambienceVal) ) {

	        alert('Please enter only numbers to Ambient temperature.');
	        objAmbienceText.focus();
	}
	
	
	

	var objAltitude = document.enquiryForm.altitude;
	if (checkTxtNull(objAltitude)) {
		validationFailed = "true";

	}
	if (  objAltitude != null && isNaN(objAltitude.value) ) {

	        alert('Please enter only numbers to Altitude.');
	        objAmbience.focus();
	}
	if (objAltitude != null && parseInt(objAltitude.value) < 0 ) {

			alert('Please specify Altitude greater than 0.');
			objAltitude.focus();
}
	if (checkTxtNull(document.enquiryForm.degreeOfProId)){

		validationFailed = "true";

	}

	if (!checkValidText(document.enquiryForm.degreeOfProId)) {

			alert("Please enter valid Degree of protection. ");
			document.enquiryForm.degreeOfProId.focus();
			validationFailed = "true";
	}
	
	if (checkTxtNull(document.enquiryForm.coolingSystem)){
		validationFailed = "true";

	}
	
	var res = String.fromCharCode(47);
	var p="1"+res+"4";

	var objMigD2Load = document.enquiryForm.migD2Load;	
	if (  objMigD2Load != null && isNaN(objMigD2Load.value) ) {

	      /* TODO alert('Please enter valid Moment of inertia J='+p+' GD2 (Load).');
	        objMigD2Load.focus();*/

	}

	
	
	
	if (checkTxtNull(document.enquiryForm.coolingSystem)){
		validationFailed = "true";

	}
	
	
	
	
	
	if (checkTxtNull(document.enquiryForm.spaceHeater)){
		validationFailed = "true";

	}
	
	var objwindingRTDSel = document.getElementById('windingRTDSel');
	  var objwindingRTDText = document.getElementById('windingRTDText');
	  if (checkTxtNull(objwindingRTDSel)){
		  if (checkTxtNull(objwindingRTDText)){
			  validationFailed = "true";

		}
	  }
	  
	  var objbearingRTDSel = document.getElementById('bearingRTDSel');
	  var objbearingRTDText = document.getElementById('bearingRTDText');
	  if (checkTxtNull(objbearingRTDSel)){
		  if (checkTxtNull(objbearingRTDText)){
			  validationFailed = "true";

		}
	  }
	  
	  var objmethodOfStgSel = document.getElementById('methodOfStgSel');
	  var objmethodOfStgText = document.getElementById('methodOfStgText');
	  if (checkTxtNull(objmethodOfStgSel)){
		  if (checkTxtNull(objmethodOfStgText)){
			  validationFailed = "true";

		}
	  }
	

	  var commercialMCUnit=document.getElementById('commercialMCUnit').value;
	  if (commercialMCUnit==""){
		  validationFailed = "true";
		}

	  
	  if((parseFloat(commercialMCUnit)==0 ) || isNaN(commercialMCUnit.replace(/,/g, "")))
	  {
		  validationFailed = "true";
		  
	  }

	
	if (selectedValue == "D"){
		var objRowIndex = document.forms[0].hidRowIndex;
		for ( var i=1; i<=parseInt(objRowIndex.value); i++ ) {
		  var obj = eval("document.forms[0].addOnType"+i);
		  var objQuantity = eval("document.forms[0].addOnQuantity"+i);
		  var objUnitPrice = eval("document.forms[0].addOnUnitPrice"+i);
		  
		  		  
		  if(obj!=null){
	   	  if ( objQuantity != null && isNaN(objQuantity.value) ) {
		   	  
    	       /* TODOalert('Please enter only numbers to specify quantity.');
        	    objQuantity.focus();*/
        	     }
          if ( objQuantity != null && parseInt(objQuantity.value) < 0 ) {
          		
				/* TODO	alert('Please specify quantity greater than 0.');
					objQuantity.focus();*/
						  }		
		  if (objUnitPrice != null && !checkValidDoubleText(objUnitPrice)) {

			/*TODO	alert("Please enter valid Unit price");
				objUnitPrice.focus();*/
		  } // If Loop Ending
		  } //Object Checking
		}		  
	}
//Validation Ending
	if (ratingId != ''){								
		document.enquiryForm.currentRating.value = ratingId;	
		document.enquiryForm.operationType.value = 'updaterating';	
		
		if (validationFailed == "true")
			
			document.enquiryForm.isValidated.value = 'N';
			
		else
			
			document.enquiryForm.isValidated.value = 'Y';
			
	

		if ( (selectedValue == "D") && (! validateAddOns()) )
			alert("Add-on type values cannot be same, please enter a different value");
		else
			document.enquiryForm.submit();



}
}

function submitToEstimate(){	
	selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");		
	if (validationOfferDataPage("submit")){
		document.enquiryForm.operationType.value = 'updaterating';
		document.enquiryForm.dispatch.value = 'estimate';
		document.enquiryForm.isValidated.value = 'Y';
		if ( (selectedValue == "D") && (! validateAddOns()) )			
			alert("Add-on type values cannot be same, please enter a different value");
		else
			document.enquiryForm.submit();	
			
	}
}



function reassign(){
	if (document.enquiryForm.assignToId.value == "0")	{
		alert("Please select Re-assign to value");
	}
	else{		
		document.enquiryForm.assignToName.value = Trim(document.enquiryForm.assignToId.options[document.enquiryForm.assignToId.selectedIndex].text);
		document.enquiryForm.operationType.value = 'reassign';	
		document.enquiryForm.submit();	
	}
}


function validationOfferDataPage(type){
	
	selectedValue = getSelectedRadioValue("enquiryForm", "commercialOfferType");	
	
	if (checkTxtNull(document.enquiryForm.areaClassification)){
		if (type=="submit"){
	  		alert("Please select Area Classification value");
  			document.enquiryForm.areaClassification.focus();
  		}
		  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.frameSize)){
		if (type=="submit"){
	  		alert("Please enter Frame Size.");
  			document.enquiryForm.frameSize.focus();
  		}
		

  		return false;
	}

	if (!checkValidText(document.enquiryForm.frameSize)) {
		if (type=="submit"){
			alert("Please enter valid Frame Size. ");
			document.enquiryForm.frameSize.focus();
		}
		

  		return false;
	}
	if (checkTxtNull(document.enquiryForm.mountingId)){
		if (type=="submit"){
	  		alert("Please select Mounting value");
  			document.enquiryForm.mountingId.focus();
  		}
		

  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.kw)){
		if (type=="submit"){
	  		alert("Please enter Rated Output.");
  			document.enquiryForm.kw.focus();
  		}
		

  		return false;
	}
	
	if ( Trim(document.enquiryForm.kw.value) != "" && isNaN(Trim(document.enquiryForm.kw.value)) ){
		if (type=="submit"){
		alert("Please enter a valid Rated Output value");	
		document.enquiryForm.kw.focus();
		}
		
  		return false;
	}
	if (checkTxtNull(document.enquiryForm.serviceFactor)){
		if (type=="submit"){
	  		alert("Please select Service factor value");
  			document.enquiryForm.serviceFactor.focus();
  		}
		

  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.dutyId)){
		if (type=="submit"){
	  		alert("Please select Duty Class value");
  			document.enquiryForm.dutyId.focus();
  		}
		

  		return false;
	}
	if (checkTxtNull(document.enquiryForm.volts)){
		if (type=="submit"){
	  		alert("Please select Rated Voltage value");
  			document.enquiryForm.volts.focus();
  		}
		
  		return false;
	}
	if (checkTxtNull(document.enquiryForm.frequency)){
		if (type=="submit"){
	  		alert("Please select Rated frequency value");
  			document.enquiryForm.frequency.focus();
  		}
		

  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.poleId)){
		if (type=="submit"){
	  		alert("Please select Pole value");
  			document.enquiryForm.poleId.focus();
  		}
		

  		return false;
	}
	
	  
    var objNlCurrent = document.enquiryForm.nlCurrent;	
	if (  isNaN(objNlCurrent.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter only numbers to specify No Load Current.');
	        objNlCurrent.focus();
	  }
	   	 

      return false;
  }
  if (objNlCurrent != null && parseInt(objNlCurrent.value) < 0 ) {
  		if (type=="submit"){
				alert('Please specify No Load Current greater than 0.');
				objNlCurrent.focus();
			}
  		

			return false;
  }	
  	
  var objNominalTorque = document.enquiryForm.fltSQCAGE;
  
  if ( objNominalTorque != null && isNaN(objNominalTorque.value) ) {
   	  if (type=="submit"){
        alert('Please enter only numbers to specify Nominal Torque.');
        objNominalTorque.focus();
  }
   	 

  return false;
}
  if ( objNominalTorque != null &&  parseInt(objNominalTorque.value) < 0)  {
   	  if (type=="submit"){
        alert('Please specify Nominal Torque greater than 0.');
        objNominalTorque.focus();
   	  }
   	 
   	  return false;
  }  
	var objStartTimeRV = document.enquiryForm.startTimeRV;	
	if ( objStartTimeRV != null && isNaN(objStartTimeRV.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter only numbers to Starting time at rated voltage.');
	        objStartTimeRV.focus();
	  }
	   	
	   	  return false;
	}
	if (objStartTimeRV != null && parseInt(objStartTimeRV.value) < 0 ) {
			if (type=="submit"){
				alert('Please specify Starting time at rated voltage greater than 0.');
				objStartTimeRV.focus();
			}
			
			return false;
	}
	var objStartTimeRV80 = document.enquiryForm.startTimeRV80;	
	if ( objStartTimeRV80 != null && isNaN(objStartTimeRV80.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter only numbers to Starting time at 80% of rated voltage.');
	        objStartTimeRV80.focus();
	  }
	   	 
	   	  return false;
	}
	if (objStartTimeRV80 != null && parseInt(objStartTimeRV80.value) < 0 ) {
			if (type=="submit"){
				alert('Please specify Starting time at 80% of rated voltage greater than 0.');
				objStartTimeRV80.focus();
			}
			
			return false;
	}
	
	if (checkTxtNull(document.enquiryForm.insulationId)){
		if (type=="submit"){
	  		alert("Please select Insulation class value");
  			document.enquiryForm.insulationId.focus();
  		}
		
  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.tempRaise)){
		if (type=="submit"){
	  		alert("Please select temperature Rise value");
  			document.enquiryForm.tempRaise.focus();
  		}
	
  		return false;
	}
	
	var ambienceVal ="";
	var objAmbienceSel = document.getElementById('ambienceSel');
	  var objAmbienceText = document.getElementById('ambienceText');
	  if (checkTxtNull(objAmbienceSel)){
		  if (checkTxtNull(objAmbienceText)){
			if (type=="submit"){
		  		alert("Please select/enter Ambient temperature value");
				document.getElementById('ambienceSel').focus();
			}
		
			return false;
		}
	  }
	  if(objAmbienceSel.value!=null){
		  ambienceVal =objAmbienceSel.value;
	  }
	  else
		  {
		  ambienceVal = objAmbienceText.value
		  }
	
	if (  ambienceVal != null && isNaN(ambienceVal) ) {
	   	  if (type=="submit"){
	        alert('Please enter only numbers to Ambient temperature.');
	        objAmbienceText.focus();
	  }
	   	
	   	  return false;
	}
	
	
	

	var objAltitude = document.enquiryForm.altitude;
	if (checkTxtNull(objAltitude)) {
		if (type=="submit"){
			alert("Please enter valid Altitude. ");
			objAltitude.focus();
		}
		
		return false;
	}
	if (  objAltitude != null && isNaN(objAltitude.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter only numbers to Altitude.');
	        objAmbience.focus();
	  }
	   	
	   	  return false;
	}
	if (objAltitude != null && parseInt(objAltitude.value) < 0 ) {
		if (type=="submit"){
			alert('Please specify Altitude greater than 0.');
			objAltitude.focus();
		}
	

		return false;
}
	if (checkTxtNull(document.enquiryForm.degreeOfProId)){
		if (type=="submit"){
	  		alert("Please enter Degree of protection.");
  			document.enquiryForm.degreeOfProId.focus();
  		}
		

  		return false;
	}

	if (!checkValidText(document.enquiryForm.degreeOfProId)) {
		if (type=="submit"){
			alert("Please enter valid Degree of protection. ");
			document.enquiryForm.degreeOfProId.focus();
		}
	
  		return false;
	}
	
	if (checkTxtNull(document.enquiryForm.coolingSystem)){
		if (type=="submit"){
	  		alert("Please select Cooling system value");
  			document.enquiryForm.coolingSystem.focus();
  		}
		
  		return false;
	}
	
	var res = String.fromCharCode(47);
	var p="1"+res+"4";

	var objMigD2Load = document.enquiryForm.migD2Load;	
	if (  objMigD2Load != null && isNaN(objMigD2Load.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter valid Moment of inertia J='+p+' GD2 (Load).');
	        objMigD2Load.focus();
	  }
	   	 
	   	  return false;
	}

	
	
	var objMtWgt = document.enquiryForm.motorTotalWgt;
	
	if (  objMtWgt != null && isNaN(objMtWgt.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter valid Total weight of motor.');
	        objMtWgt.focus();
	  }
	   	 
	   	  return false;
	}
	
	if (  document.enquiryForm.minStartVolt != null && isNaN(document.enquiryForm.minStartVolt.value) ) {
	   	  if (type=="submit"){
	        alert('Please enter valid Minimum Starting Voltage.');
	        document.enquiryForm.minStartVolt.focus();
	  }
	   	
	   	  return false;
	}
	
	
	
	
	if (checkTxtNull(document.enquiryForm.coolingSystem)){
		if (type=="submit"){
	  		alert("Please select Cooling system value");
  			document.enquiryForm.coolingSystem.focus();
  		}
	
  		return false;
	}
	
	
	
	
	if (checkTxtNull(document.enquiryForm.spaceHeater)){
		if (type=="submit"){
	  		alert("Please select/enter Space Heater value");
  			document.enquiryForm.spaceHeater.focus();
  		}
		
  		return false;
	}
	
	var objwindingRTDSel = document.getElementById('windingRTDSel');
	  var objwindingRTDText = document.getElementById('windingRTDText');
	  if (checkTxtNull(objwindingRTDSel)){
		  if (checkTxtNull(objwindingRTDText)){
			if (type=="submit"){
		  		alert("Please select/enter Winding RTD`s  No. & type value");
				document.getElementById('objwindingRTDSel').focus();
			}
			
			return false;
		}
	  }
	  
	  var objbearingRTDSel = document.getElementById('bearingRTDSel');
	  var objbearingRTDText = document.getElementById('bearingRTDText');
	  if (checkTxtNull(objbearingRTDSel)){
		  if (checkTxtNull(objbearingRTDText)){
			if (type=="submit"){
		  		alert("Please select/enter bearing RTD`s  No. & type value");
				document.getElementById('objbearingRTDSel').focus();
			}
			
			return false;
		}
	  }
	  
	  var objmethodOfStgSel = document.getElementById('methodOfStgSel');
	  var objmethodOfStgText = document.getElementById('methodOfStgText');
	  if (checkTxtNull(objmethodOfStgSel)){
		  if (checkTxtNull(objmethodOfStgText)){
			if (type=="submit"){
		  		alert("Please select/enter Method of starting value");
				document.getElementById('objmethodOfStgSel').focus();
			}
		
			return false;
		}
	  }
	  
	  var commercialMCUnit=document.getElementById('commercialMCUnit').value;
	 /*TODO if (commercialMCUnit==""){
			if (type=="submit"){
		  		alert("Please Enter MC/Unit Value");
		  		document.getElementById('commercialMCUnit').focus();
			}
		
			return false;
		}*/

	  
	  if((parseFloat(commercialMCUnit)==0 ) || isNaN(commercialMCUnit.replace(/,/g, "")))
	  {
			if (type=="submit"){

		  alert("Please Enter Valid  MC/Unit Value  ");
		  document.getElementById("commercialMCUnit").value="";
		  document.getElementById('commercialMCUnit').focus();
			}
		  return false;
	 }

	
	
	if (selectedValue == "D"){
		var objRowIndex = document.forms[0].hidRowIndex;
		for ( var i=1; i<=parseInt(objRowIndex.value); i++ ) {
		  var obj = eval("document.forms[0].addOnType"+i);
		  var objQuantity = eval("document.forms[0].addOnQuantity"+i);
		  var objUnitPrice = eval("document.forms[0].addOnUnitPrice"+i);
		  
		   		  
		  if(obj!=null){
	   	  if ( objQuantity != null && isNaN(objQuantity.value) ) {
		   	  if (type=="submit"){
    	        alert('Please enter only numbers to specify quantity.');
        	    objQuantity.focus();
        	  }
		   	 
              return false;
          }
          if ( objQuantity != null && parseInt(objQuantity.value) < 0 ) {
          		if (type=="submit"){
					alert('Please specify quantity greater than 0.');
					objQuantity.focus();
				}
          	

				return false;
		  }		
		  if (objUnitPrice != null && !checkValidDoubleText(objUnitPrice)) {
		  	if (type=="submit"){
				alert("Please enter valid Unit price");
				objUnitPrice.focus();
			}
		  	

		  	return false;
		  }
		  }
		}		  
	} // D or P Value Checking
	var errorMessage = document.enquiryForm.ratingsValidationMessage.value;

	if (type=="submit" && document.enquiryForm.ratingsValidationMessage.value != ''){
		alert("Technical Offer details are in-complete for "+errorMessage+" ratings.  Please enter all values for these ratings before submitting the enquiry.");	
		return false;
	}
	/*TODO errorMessage = document.enquiryForm.ratingsValidationMessage.value;
	alert('errorMessage::'+errorMessage);
	if (type=="submit" && document.enquiryForm.ratingsValidationMessage.value != ''){
		alert("Technical Offer details are in-complete for "+errorMessage+" ratings.  Please enter all values for these ratings before submitting the enquiry.");	
		return false;
	}*/

	return true;
} //End Of Validation

function validateAddOns(){
	var count = 1;
	var exists = false;

	var objRowIndex = document.forms[0].hidRowIndex;
	for ( var i=count; i<=parseInt(objRowIndex.value); i++ ) {
	
		var obj = eval("document.forms[0].addOnTypeText"+i);
		for (var j=1; j<=parseInt(objRowIndex.value); j++){				
			if (j != count){		

				var obj1 = eval("document.forms[0].addOnTypeText"+j);
				if (obj != null && obj1 != null && obj.value == obj1.value){					
					exists = true;				
				}
			}
		}	 
		count++			
	}	
	
	if (exists){		
		return false;
	}else{
		return true;
	}	
}


function checkValidDoubleText(obj){
	
	var specialCharacter = /^[0-9().]+(\s([0-9().])+)*$/;
	if (obj != null) {
		if (Trim(obj.value) != "" && ! specialCharacter.test(Trim(obj.value))) {
			return false;
		}else
			return true;
	}
}
function changeMasterDataStatus(objStatusName, objStatusActionName, engineeringId, workingDiv,mastername,masterId) {

	var objStatus = document.getElementById(objStatusName);
	var objStatusAction = document.getElementById(objStatusActionName);

	workingDivId = workingDiv;
	
	if ( objStatus.value == "Inactive" ) {
	
	    updateMasterDataStatus(engineeringId, 'A', objStatusName, objStatusActionName, workingDiv,mastername,masterId);
	} else if ( objStatus.value == "Active" ) {
	
	    updateMasterDataStatus(engineeringId, 'I', objStatusName, objStatusActionName, workingDiv,mastername,masterId);
	}
}

function updateMasterDataStatus(engineeringId, action, objStatusName, objStatusActionName, workingDiv,mastername,masterId){
 
    if (Trim(engineeringId) != "" && Trim(action) != "") {
  
    	var url = "mastersdata.do?invoke=updateMastersDataStatus";
		data = "";
		data += "&engineeringId=" + engineeringId;
		data +="&mastername="+mastername;
		data +="&masterId="+masterId;
		data += "&action=" + action;
		data += "&targetObjects=" + objStatusName + "," + objStatusActionName;
		url += data;
		

	    invokeAjaxFunctionality(url, data, workingDiv);
    } else{
        alert("Invalid inputs.");
    }
}

function submitEstimate(){
	
    var numeric = /^\d+(?:\.\d{1})?$/;


	var customerNameMultiplier=document.getElementById("customerNameMultiplier").value;

	if(customerNameMultiplier!="" && parseFloat(customerNameMultiplier)==0)
    {
		customerNameMultiplier=0;
		alert("Enter Valid Customer Name Multiplier Value");
		document.getElementById("customerNameMultiplier").value="";
		document.getElementById("customerNameMultiplier").focus();
		
		return false;
	}
	if(customerNameMultiplier!="" && !numeric.test(customerNameMultiplier))
	{
		alert("Enter Valid Customer Name Multiplier Value");
		document.getElementById("customerNameMultiplier").value="";
		document.getElementById("customerNameMultiplier").focus();
	
		return false;
	}
	
	var customerTypeMultiplier=document.getElementById("customerTypeMultiplier").value;
	if(customerTypeMultiplier!="" && parseFloat(customerTypeMultiplier)==0)
	{
		customerTypeMultiplier=0;
		alert("Enter Valid Customer Type Multiplier Value");
		document.getElementById("customerTypeMultiplier").value="";
		document.getElementById("customerTypeMultiplier").focus();
		return false;
	}
	if(customerTypeMultiplier!="" && !numeric.test(customerTypeMultiplier))
	{
		customerTypeMultiplier=0;
		alert("Enter Valid Customer Type Multiplier Value");
		document.getElementById("customerTypeMultiplier").value="";
		document.getElementById("customerTypeMultiplier").focus();
		return false;
	}

	
	var endUserMultiplier=document.getElementById("endUserMultiplier").value;
	if(endUserMultiplier!="" && parseFloat(endUserMultiplier)==0)
	{
		endUserMultiplier=0;
		alert("Enter Valid EndUser Multiplier Value");
		document.getElementById("endUserMultiplier").value="";
		document.getElementById("endUserMultiplier").focus();
		return false;
	}
	if(endUserMultiplier!="" && !numeric.test(endUserMultiplier))
	{
		endUserMultiplier=0;
		alert("Enter Valid EndUser Multiplier Value");
		document.getElementById("endUserMultiplier").value="";
		document.getElementById("endUserMultiplier").focus();
		return false;
	}
	var warrantyDispatchMultiplier=document.getElementById("warrantyDispatchMultiplier").value;
	if(warrantyDispatchMultiplier!="" && parseFloat(warrantyDispatchMultiplier)==0)
	{
		warrantyDispatchMultiplier=0;
		alert("Enter Valid Warranty After Dispatch Multiplier Value");
		document.getElementById("warrantyDispatchMultiplier").value="";
		document.getElementById("warrantyDispatchMultiplier").focus();
		return false;
	}
	if(warrantyDispatchMultiplier!="" && !numeric.test(warrantyDispatchMultiplier))
	{
		warrantyDispatchMultiplier=0;
		alert("Enter Valid Warranty After Dispatch Multiplier Value");
		document.getElementById("warrantyDispatchMultiplier").value="";
		document.getElementById("warrantyDispatchMultiplier").focus();
		return false;
	}
	var warrantycommissioningMultiplier=document.getElementById("warrantycommissioningMultiplier").value;
	if(warrantycommissioningMultiplier!="" && parseFloat(warrantycommissioningMultiplier)==0)
	{
		warrantycommissioningMultiplier=0;	
		alert("Enter Valid Warranty After Commissioning Multiplier Value");
		document.getElementById("warrantycommissioningMultiplier").value="";
		document.getElementById("warrantycommissioningMultiplier").focus();
		return false;
	}
	if(warrantycommissioningMultiplier!="" && !numeric.test(warrantycommissioningMultiplier))
	{
		warrantycommissioningMultiplier=0;	
		alert("Enter Valid Warranty After Commissioning Multiplier Value");
		document.getElementById("warrantycommissioningMultiplier").value="";
		document.getElementById("warrantycommissioningMultiplier").focus();
		return false;
	}

	var advancePaymMultiplier=document.getElementById("advancePaymMultiplier").value;
	if(advancePaymMultiplier!="" && parseFloat(advancePaymMultiplier)==0)
	{
		advancePaymMultiplier=0;	
		alert("Enter Valid Advance Payment Multiplier Value");
		document.getElementById("advancePaymMultiplier").value="";
		document.getElementById("advancePaymMultiplier").focus();
		return false;
	}
	if(advancePaymMultiplier!="" && !numeric.test(advancePaymMultiplier))
	{
		advancePaymMultiplier=0;	
		alert("Enter Valid Advance Payment Multiplier Value");
		document.getElementById("advancePaymMultiplier").value="";
		document.getElementById("advancePaymMultiplier").focus();
		return false;
	}

	var payTermMultiplier=document.getElementById("payTermMultiplier").value;
	if(payTermMultiplier!="" && parseFloat(payTermMultiplier)==0)
	{
		payTermMultiplier=0;
		alert("Enter Valid Payment Terms Multiplier Value");
		document.getElementById("payTermMultiplier").value="";
		document.getElementById("payTermMultiplier").focus();
		return false;
	}
	if(payTermMultiplier!="" && !numeric.test(payTermMultiplier))
	{
		payTermMultiplier=0;
		alert("Enter Valid Payment Terms Multiplier Value");
		document.getElementById("payTermMultiplier").value="";
		document.getElementById("payTermMultiplier").focus();
		return false;
	}
	var expDeliveryMultiplier=document.getElementById("expDeliveryMultiplier").value;
	if(expDeliveryMultiplier!="" && parseFloat(expDeliveryMultiplier)==0)
	{
		expDeliveryMultiplier=0;	
		alert(" Enter Valid Exp Delivery Date Multiplier Value");
		document.getElementById("expDeliveryMultiplier").value="";
		document.getElementById("expDeliveryMultiplier").focus();
		return false;
	}
	if(expDeliveryMultiplier!="" && !numeric.test(expDeliveryMultiplier))
	{
		expDeliveryMultiplier=0;	
		alert(" Enter Valid Exp Delivery Date Multiplier Value");
		document.getElementById("expDeliveryMultiplier").value="";
		document.getElementById("expDeliveryMultiplier").focus();
		return false;
	}
	var financeMultiplier=document.getElementById("financeMultiplier").value;
	if(financeMultiplier!="" && parseFloat(financeMultiplier)==0)
	{
		financeMultiplier=0;	
		alert("Enter Valid Finance Multiplier Value");
		document.getElementById("financeMultiplier").value="";
		document.getElementById("financeMultiplier").focus();
		return false;
	}
	if(financeMultiplier!="" && !numeric.test(financeMultiplier))
	{
		financeMultiplier=0;	
		alert("Enter Valid Finance Multiplier Value");
		document.getElementById("financeMultiplier").value="";
		document.getElementById("financeMultiplier").focus();
		return false;
	}

	var finalRatingList=document.getElementById("finalRatingList").value;
	//alert(finalRatingList);
	if(finalRatingList!="")
	{
		
	    if(finalRatingList.indexOf(",")==-1)
	    {
	    	var isvalidated=true;
	      var quantityMultiplier=document.getElementById("quantityMultiplier_"+finalRatingList).value;
	  	 if(quantityMultiplier!="" && parseFloat(quantityMultiplier)==0)
		 {
	  		quantityMultiplier=0;	
	  		alert("Please Enter Valid Quality Required Multiplier Value");
	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	  		document.getElementById("quantityMultiplier_"+finalRatingList).focus();
	  		isvalidated=false;
	  		return false;
		 }
	  	 if(quantityMultiplier!="" && !numeric.test(quantityMultiplier))
		 {
	  		quantityMultiplier=0;	
	  		alert("Please Enter Valid Quality Required Multiplier Value");
	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	  		document.getElementById("quantityMultiplier_"+finalRatingList).focus();
	  		isvalidated=false;
	  		return false;
		 }


	      var ratedOutputPNMulitplier=document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value;
		  if(ratedOutputPNMulitplier!="" && parseFloat(ratedOutputPNMulitplier)==0)
			 {
		  		ratedOutputPNMulitplier=0;
		  		alert("Please Enter Valid Rated output PN Multiplier Value");
		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

		  		
			 }
		  if(ratedOutputPNMulitplier!="" && !numeric.test(ratedOutputPNMulitplier))
			 {
		  		ratedOutputPNMulitplier=0;
		  		alert("Please Enter Valid Rated output PN Multiplier Value");
		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

		  		
			 }



	      var areaCMultiplier=document.getElementById("areaCMultiplier_"+finalRatingList).value;
	      if(areaCMultiplier!="" && parseFloat(areaCMultiplier)==0)
			 {
	    	  areaCMultiplier=0;	
		  		alert("Please Enter Valid Area Classification Multiplier Value");
		  		document.getElementById("areaCMultiplier_"+finalRatingList).value="";
		  		document.getElementById("areaCMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
	      if(areaCMultiplier!="" && !numeric.test(areaCMultiplier))
			 {
	    	  areaCMultiplier=0;	
		  		alert("Please Enter Valid Area Classification Multiplier Value");
		  		document.getElementById("areaCMultiplier_"+finalRatingList).value="";
		  		document.getElementById("areaCMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }


	      var frameMultiplier=document.getElementById("frameMultiplier_"+finalRatingList).value;
	      if(frameMultiplier!=""  && parseFloat(frameMultiplier)==0)
			 {
	    	  frameMultiplier=0;
		  		alert("Please Enter Valid Frame Multiplier Value");
		  		document.getElementById("frameMultiplier_"+finalRatingList).value="";
		  		document.getElementById("frameMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
	      if(frameMultiplier!=""  && !numeric.test(frameMultiplier))
			 {
	    	  frameMultiplier=0;
		  		alert("Please Enter Valid Frame Multiplier Value");
		  		document.getElementById("frameMultiplier_"+finalRatingList).value="";
		  		document.getElementById("frameMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }

	      var typeMultiplier=document.getElementById("typeMultiplier_"+finalRatingList).value;
	      if(typeMultiplier!=""  && parseFloat(typeMultiplier)==0)
			 {
	    	  typeMultiplier=0;	
		  		alert("Please Enter Valid Type Multiplier Value");
		  		document.getElementById("typeMultiplier_"+finalRatingList).value="";

		  		document.getElementById("typeMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
	      if(typeMultiplier!=""  && !numeric.test(typeMultiplier))
			 {
	    	  typeMultiplier=0;	
		  		alert("Please Enter Valid Type Multiplier Value");
		  		document.getElementById("typeMultiplier_"+finalRatingList).value="";

		  		document.getElementById("typeMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }


	      var applicationMultiplier=document.getElementById("applicationMultiplier_"+finalRatingList).value;
	      if(applicationMultiplier!="" && parseFloat(applicationMultiplier)==0)
			 {
	    	  applicationMultiplier=0;	
		  		alert("Please Enter Valid Application Multiplier Value");
		  		document.getElementById("applicationMultiplier_"+finalRatingList).value="";
		  		document.getElementById("applicationMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
	      if(applicationMultiplier!="" && !numeric.test(applicationMultiplier))
			 {
	    	  applicationMultiplier=0;	
		  		alert("Please Enter Valid Application Multiplier Value");
		  		document.getElementById("applicationMultiplier_"+finalRatingList).value="";
		  		document.getElementById("applicationMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }

	      
	      var factorMultiplier=document.getElementById("factorMultiplier_"+finalRatingList).value; 
	      if(factorMultiplier!="" && parseFloat(factorMultiplier)==0)
			 {
	    	  factorMultiplier=0;
		  		alert("Please Enter Valid Factor Multiplier Value");
		  		document.getElementById("factorMultiplier_"+finalRatingList).value="";
		  		document.getElementById("factorMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
	      if(factorMultiplier!="" && !numeric.test(factorMultiplier))
			 {
	    	  factorMultiplier=0;
		  		alert("Please Enter Valid Factor Multiplier Value");
		  		document.getElementById("factorMultiplier_"+finalRatingList).value="";
		  		document.getElementById("factorMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }


	      var netmcMultiplier=document.getElementById("netmcMultiplier_"+finalRatingList).value;
	      if(netmcMultiplier!="" && parseFloat(netmcMultiplier)==0)
			 {
	    	  netmcMultiplier=0;	
		  		alert("Please Enter Valid Net MC Multiplier Value");
		  		document.getElementById("netmcMultiplier_"+finalRatingList).value="";
		  		document.getElementById("netmcMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

			 }
          if(isvalidated)
          {

        	document.enquiryForm.submit();
          }

	    }//If Loop Ending
	    if(finalRatingList.indexOf(",")!=-1)
	    {
	    	var isvalidated=true;
	    	var res = finalRatingList.split(",");
	    	
	    	for(var j=0;j<res.length;j++)
	    	{
	    		
	    		finalRatingList=res[j];
	    		
	  	      var quantityMultiplier=document.getElementById("quantityMultiplier_"+finalRatingList).value;
	 	  	 if(quantityMultiplier!="" && parseFloat(quantityMultiplier)==0)
	 		 {
	 	  		quantityMultiplier=0;	
	 	  		alert("Please Enter Valid Quality Required Multiplier Value In Rating "+(j+1));
	 	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	 	  		document.getElementById("quantityMultiplier_"+finalRatingList).focus();
	 	  		isvalidated=false;
	 	  		return false;
	 		 }
	 	  	 if(quantityMultiplier!="" && !numeric.test(quantityMultiplier))
	 		 {
	 	  		quantityMultiplier=0;	
	 	  		alert("Please Enter Valid Quality Required Multiplier Value In Rating "+(j+1));
	 	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	 	  		document.getElementById("quantityMultiplier_"+finalRatingList).focus();
	 	  		isvalidated=false;
	 	  		return false;
	 		 }


	 	      var ratedOutputPNMulitplier=document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value;
	 		  if(ratedOutputPNMulitplier!="" && parseFloat(ratedOutputPNMulitplier)==0)
	 			 {
	 		  		ratedOutputPNMulitplier=0;
	 		  		alert("Please Enter Valid Rated output PN Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
	 		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;
	 		  		
	 		  		
	 			 }
	 		 if(ratedOutputPNMulitplier!="" && !numeric.test(ratedOutputPNMulitplier))
 			 {
 		  		ratedOutputPNMulitplier=0;
 		  		alert("Please Enter Valid Rated output PN Multiplier Value In Rating "+(j+1));
 		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
 		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).focus();
 		  		isvalidated=false;
 		  		return false;
 		  		
 		  		
 			 }

	 	      var areaCMultiplier=document.getElementById("areaCMultiplier_"+finalRatingList).value;
	 	      if(areaCMultiplier!="" && parseFloat(areaCMultiplier)==0)
	 			 {
	 	    	  areaCMultiplier=0;	
	 		  		alert("Please  Enter Valid Area Classification Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("areaCMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("areaCMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;
	 		  		
	 			 }
	 	      if(areaCMultiplier!="" && !numeric.test(areaCMultiplier))
	 			 {
	 	    	  areaCMultiplier=0;	
	 		  		alert("Please  Enter Valid Area Classification Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("areaCMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("areaCMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;
	 		  		
	 			 }


	 	      var frameMultiplier=document.getElementById("frameMultiplier_"+finalRatingList).value;
	 	      if(frameMultiplier!="" && parseFloat(frameMultiplier)==0)
	 			 {
	 	    	  frameMultiplier=0;
	 		  		alert("Please Enter Valid Frame Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("frameMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("frameMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }
	 	     if(frameMultiplier!="" && !numeric.test(frameMultiplier))
 			 {
 	    	  frameMultiplier=0;
 		  		alert("Please Enter Valid Frame Multiplier Value In Rating "+(j+1));
 		  		document.getElementById("frameMultiplier_"+finalRatingList).value="";
 		  		document.getElementById("frameMultiplier_"+finalRatingList).focus();
 		  		isvalidated=false;
 		  		return false;

 			 }

	 	      var typeMultiplier=document.getElementById("typeMultiplier_"+finalRatingList).value;
	 	      if(typeMultiplier!="" && parseFloat(typeMultiplier)==0)
	 			 {
	 	    	  typeMultiplier=0;	
	 		  		alert("Please Enter Valid Type Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("typeMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("typeMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }
	 	      if(typeMultiplier!="" && !numeric.test(typeMultiplier))
	 			 {
	 	    	  typeMultiplier=0;	
	 		  		alert("Please Enter Valid Type Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("typeMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("typeMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }


	 	      var applicationMultiplier=document.getElementById("applicationMultiplier_"+finalRatingList).value;
	 	      if(applicationMultiplier!="" && parseFloat(applicationMultiplier)==0)
	 			 {
	 	    	  applicationMultiplier=0;	
	 		  		alert("Please Enter Valid Application Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("applicationMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("applicationMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }
	 	      if(applicationMultiplier!="" && !numeric.test(applicationMultiplier))
	 			 {
	 	    	  applicationMultiplier=0;	
	 		  		alert("Please Enter Valid Application Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("applicationMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("applicationMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }


	 	      
	 	      var factorMultiplier=document.getElementById("factorMultiplier_"+finalRatingList).value; 
	 	      if(factorMultiplier!="" && parseFloat(factorMultiplier)==0)
	 			 {
	 	    	  factorMultiplier=0;
	 		  		alert("Please Enter Valid Factor Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("factorMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("factorMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }
	 	      if(factorMultiplier!="" && !numeric.test(factorMultiplier))
	 			 {
	 	    	  factorMultiplier=0;
	 		  		alert("Please Enter Valid Factor Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("factorMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("factorMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }


	 	      var netmcMultiplier=document.getElementById("netmcMultiplier_"+finalRatingList).value;
	 	      if(netmcMultiplier!="" && parseFloat(netmcMultiplier)==0)
	 			 {
	 	    	  netmcMultiplier=0;	
	 		  		alert("Please Enter Valid  Net MC Multiplier Value In Rating "+(j+1));
	 		  		document.getElementById("netmcMultiplier_"+finalRatingList).value="";
	 		  		document.getElementById("netmcMultiplier_"+finalRatingList).focus();
	 		  		isvalidated=false;
	 		  		return false;

	 			 }

	    		
	    	}//For Loop Ending
	    	if(isvalidated==true)
	    		{

		    	document.enquiryForm.submit();

	    		}
	    }


	}//Tech Rates Checking

	

}
function calculateMultipliers(finalRatingList)
{
	var customerNameMultiplier=document.getElementById("customerNameMultiplier").value;
	if(customerNameMultiplier=="" || customerNameMultiplier=="." || (parseFloat(customerNameMultiplier)==0 ))
    {
		customerNameMultiplier=0;
		document.getElementById("customerNameMultiplier").value="";
	}
	
	var customerTypeMultiplier=document.getElementById("customerTypeMultiplier").value;
	if(customerTypeMultiplier=="" || customerTypeMultiplier=="." || (parseFloat(customerTypeMultiplier)==0 ))
	{
		customerTypeMultiplier=0;	
		document.getElementById("customerTypeMultiplier").value="";
		
	}
	var endUserMultiplier=document.getElementById("endUserMultiplier").value;
	if(endUserMultiplier=="" || endUserMultiplier=="." || (parseFloat(endUserMultiplier)==0 ))
	{
		endUserMultiplier=0;	
		document.getElementById("endUserMultiplier").value="";
	}
	var warrantyDispatchMultiplier=document.getElementById("warrantyDispatchMultiplier").value;
	if(warrantyDispatchMultiplier=="" || warrantyDispatchMultiplier=="." || (parseFloat(warrantyDispatchMultiplier)==0 ))
	{
		warrantyDispatchMultiplier=0;
		document.getElementById("warrantyDispatchMultiplier").value="";
	}
	var warrantycommissioningMultiplier=document.getElementById("warrantycommissioningMultiplier").value;
	if(warrantycommissioningMultiplier=="" || warrantycommissioningMultiplier=="." || (parseFloat(warrantycommissioningMultiplier)==0 ))
	{
		warrantycommissioningMultiplier=0;	
		document.getElementById("warrantycommissioningMultiplier").value="";
	}
	var advancePaymMultiplier=document.getElementById("advancePaymMultiplier").value;
	if(advancePaymMultiplier=="" || advancePaymMultiplier=="." || (parseFloat(advancePaymMultiplier)==0 ))
	{
		advancePaymMultiplier=0;
		document.getElementById("advancePaymMultiplier").value="";
	}
	var payTermMultiplier=document.getElementById("payTermMultiplier").value;
	if(payTermMultiplier=="" || payTermMultiplier=="." || (parseFloat(payTermMultiplier)==0 ))
	{
		payTermMultiplier=0;	
		document.getElementById("payTermMultiplier").value="";
	}
	var expDeliveryMultiplier=document.getElementById("expDeliveryMultiplier").value;
	if(expDeliveryMultiplier=="" || expDeliveryMultiplier=="." || (parseFloat(expDeliveryMultiplier)==0 ))
	{
		expDeliveryMultiplier=0;	
		document.getElementById("expDeliveryMultiplier").value="";
	}
	var financeMultiplier=document.getElementById("financeMultiplier").value;
	if(financeMultiplier=="" || financeMultiplier=="." || (parseFloat(financeMultiplier)==0 ))
	{
		financeMultiplier=0;
		document.getElementById("financeMultiplier").value="";
	}
	var finalValue=(parseFloat(customerNameMultiplier)+parseFloat(customerTypeMultiplier)+parseFloat(endUserMultiplier)+parseFloat(advancePaymMultiplier)+parseFloat(payTermMultiplier)+parseFloat(expDeliveryMultiplier)+parseFloat(financeMultiplier));


	document.getElementById("commonMultiplierCount").value=finalValue.toFixed(1);
    var quantityMultiplier=document.getElementById("quantityMultiplier_"+finalRatingList).value;
 	 if(quantityMultiplier=="" || quantityMultiplier=="." || (parseFloat(quantityMultiplier)==0 ))
	 {
 		quantityMultiplier=0;	
 		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	 }

     var ratedOutputPNMulitplier=document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value;
	  if(ratedOutputPNMulitplier=="" || ratedOutputPNMulitplier=="." || (parseFloat(ratedOutputPNMulitplier)==0 ))
		 {
	  		ratedOutputPNMulitplier=0;
	  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
		 }

     var typeMultiplier=document.getElementById("typeMultiplier_"+finalRatingList).value;
     if(typeMultiplier=="" || typeMultiplier=="." || (parseFloat(typeMultiplier)==0 ))
		 {
   	  typeMultiplier=0;	
   	document.getElementById("typeMultiplier_"+finalRatingList).value="";
		 }

     var areaCMultiplier=document.getElementById("areaCMultiplier_"+finalRatingList).value;
     if(areaCMultiplier=="" || areaCMultiplier=="." || (parseFloat(areaCMultiplier)==0 ))
		 {
   	  areaCMultiplier=0;
   	document.getElementById("areaCMultiplier_"+finalRatingList).value="";
		 }

     var frameMultiplier=document.getElementById("frameMultiplier_"+finalRatingList).value;
     if(frameMultiplier=="" || frameMultiplier=="." || (parseFloat(frameMultiplier)==0 ))
		 {
   	  frameMultiplier=0;
   	document.getElementById("frameMultiplier_"+finalRatingList).value="";
		 }

     var applicationMultiplier=document.getElementById("applicationMultiplier_"+finalRatingList).value;
     if(applicationMultiplier=="" || applicationMultiplier=="." || (parseFloat(applicationMultiplier)==0 ))
		 {
   	  applicationMultiplier=0;	
   	document.getElementById("applicationMultiplier_"+finalRatingList).value="";
		 }

     
     var factorMultiplier=document.getElementById("factorMultiplier_"+finalRatingList).value; 
     if(factorMultiplier=="" || factorMultiplier=="." || (parseFloat(factorMultiplier)==0 ))
		 {
   	  factorMultiplier=0;
   	document.getElementById("factorMultiplier_"+finalRatingList).value="";
		 }

     var netmcMultiplier=document.getElementById("netmcMultiplier_"+finalRatingList).value;
     if(netmcMultiplier=="" || netmcMultiplier=="." || (parseFloat(netmcMultiplier)==0 ) || isNaN(netmcMultiplier.replace(/,/g, "")))
		 {
   	  netmcMultiplier=0;
   	document.getElementById("netmcMultiplier_"+finalRatingList).value="0";
		 }

     var markupMultiplier=document.getElementById("markupMultiplier_"+finalRatingList).value; 
     if(markupMultiplier=="" || markupMultiplier=="." )
		 {
   	  markupMultiplier=0;	
   	document.getElementById("markupMultiplier_"+finalRatingList).value="";
		 }

     var transferCMultiplier=document.getElementById("transferCMultiplier_"+finalRatingList).value; 
     if(transferCMultiplier=="" || transferCMultiplier=="." )
		 {
   	  transferCMultiplier=0;
   	document.getElementById("transferCMultiplier_"+finalRatingList).value="";
		 }

 	var techRates=(parseFloat(quantityMultiplier)+parseFloat(ratedOutputPNMulitplier)+parseFloat(typeMultiplier)+parseFloat(areaCMultiplier)+parseFloat(frameMultiplier)+parseFloat(applicationMultiplier)+parseFloat(factorMultiplier));
   var commonMultipliercount=document.getElementById("commonMultiplierCount").value;
   var TotalFinalRateCount=(parseFloat(techRates)+parseFloat(commonMultipliercount));
 	document.getElementById("markupMultiplier_"+finalRatingList).value=TotalFinalRateCount.toFixed(1);

 	var TotalTransferCount=0;
 	if(netmcMultiplier==0)
 	{
 		TotalTransferCount=netmcMultiplier.replace(/,/g, "")*((parseFloat(1)+parseFloat(TotalFinalRateCount/100)));
 	 	document.getElementById("netmcMultiplier_"+finalRatingList).value=netmcMultiplier;

 	 	TotalTransferCount=Math.round(TotalTransferCount);
 	 	document.getElementById("transferCMultiplier_"+finalRatingList).value=TotalTransferCount;	

 	}
 	else
 	{
 		TotalTransferCount=netmcMultiplier.replace(/,/g, "")*((parseFloat(1)+parseFloat(TotalFinalRateCount/100)));	
 	 	document.getElementById("netmcMultiplier_"+finalRatingList).value=netmcMultiplier.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
 	 	TotalTransferCount=Math.round(TotalTransferCount);
 	 	document.getElementById("transferCMultiplier_"+finalRatingList).value=TotalTransferCount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");	

 	}


	

	return true;
	
	
	
}
function calculateCommonMultipliers()
{
	var customerNameMultiplier=document.getElementById("customerNameMultiplier").value;
	if(customerNameMultiplier=="" || customerNameMultiplier=="." || (parseFloat(customerNameMultiplier)==0 ))
    {
		customerNameMultiplier=0;	
		document.getElementById("customerNameMultiplier").value="";
	}
	
	var customerTypeMultiplier=document.getElementById("customerTypeMultiplier").value;
	if(customerTypeMultiplier=="" || customerTypeMultiplier=="." || (parseFloat(customerTypeMultiplier)==0 ))
	{
		customerTypeMultiplier=0;	
		document.getElementById("customerTypeMultiplier").value="";
	}
	
	var endUserMultiplier=document.getElementById("endUserMultiplier").value;
	if(endUserMultiplier=="" || endUserMultiplier=="." || (parseFloat(endUserMultiplier)==0 ))
	{
		endUserMultiplier=0;
		document.getElementById("endUserMultiplier").value="";
	}
	
	var warrantyDispatchMultiplier=document.getElementById("warrantyDispatchMultiplier").value;
	if(warrantyDispatchMultiplier=="" || warrantyDispatchMultiplier=="." || (parseFloat(warrantyDispatchMultiplier)==0 ))
	{
		warrantyDispatchMultiplier=0;	
		document.getElementById("warrantyDispatchMultiplier").value="";
	}
	
	var warrantycommissioningMultiplier=document.getElementById("warrantycommissioningMultiplier").value;
	if(warrantycommissioningMultiplier=="" || warrantycommissioningMultiplier=="." || (parseFloat(warrantycommissioningMultiplier)==0 ))
	{
		warrantycommissioningMultiplier=0;
		document.getElementById("warrantycommissioningMultiplier").value="";
	}
	
	var advancePaymMultiplier=document.getElementById("advancePaymMultiplier").value;
	if(advancePaymMultiplier=="" || advancePaymMultiplier=="." || (parseFloat(advancePaymMultiplier)==0 ))
	{
		advancePaymMultiplier=0;
		document.getElementById("advancePaymMultiplier").value="";
	}
	
	var payTermMultiplier=document.getElementById("payTermMultiplier").value;
	if(payTermMultiplier=="" || payTermMultiplier=="." || (parseFloat(payTermMultiplier)==0 ))
	{
		payTermMultiplier=0;
		document.getElementById("payTermMultiplier").value="";
	}
	
	var expDeliveryMultiplier=document.getElementById("expDeliveryMultiplier").value;
	if(expDeliveryMultiplier=="" || expDeliveryMultiplier=="." || (parseFloat(expDeliveryMultiplier)==0 ))
	{
		expDeliveryMultiplier=0;	
		document.getElementById("expDeliveryMultiplier").value="";
	}
	
	var financeMultiplier=document.getElementById("financeMultiplier").value;
	if(financeMultiplier=="" || financeMultiplier=="." || (parseFloat(financeMultiplier)==0 ))
	{
		financeMultiplier=0;	
		document.getElementById("financeMultiplier").value="";
	}

	var finalValue=(parseFloat(customerNameMultiplier)+parseFloat(customerTypeMultiplier)+parseFloat(endUserMultiplier)+parseFloat(advancePaymMultiplier)+
			parseFloat(payTermMultiplier)+parseFloat(expDeliveryMultiplier)+parseFloat(financeMultiplier));
	
	
	document.getElementById("commonMultiplierCount").value=finalValue.toFixed(1);
	
	var finalRatingList=document.getElementById("finalRatingList").value;

	if(finalRatingList!="")
	{
	    if(finalRatingList.indexOf(",")==-1)
	    {
	      var quantityMultiplier=document.getElementById("quantityMultiplier_"+finalRatingList).value;
	  	 if(quantityMultiplier=="" || quantityMultiplier=="." || (parseFloat(quantityMultiplier)==0 ))
		 {
	  		quantityMultiplier=0;	
	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
		 }
	  	
	      var ratedOutputPNMulitplier=document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value;
		  if(ratedOutputPNMulitplier=="" || ratedOutputPNMulitplier=="." || (parseFloat(ratedOutputPNMulitplier)==0 ))
			 {
		  		ratedOutputPNMulitplier=0;
		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
			 }
		  
	      var typeMultiplier=document.getElementById("typeMultiplier_"+finalRatingList).value;
	      if(typeMultiplier=="" || typeMultiplier=="." || (parseFloat(typeMultiplier)==0 ))
			 {
	    	  typeMultiplier=0;	
	    	  document.getElementById("typeMultiplier_"+finalRatingList).value="";
			 }
	      
	      var areaCMultiplier=document.getElementById("areaCMultiplier_"+finalRatingList).value;
	      if(areaCMultiplier=="" || areaCMultiplier=="." || (parseFloat(areaCMultiplier)==0 ))
			 {
	    	  areaCMultiplier=0;	
	    	  document.getElementById("areaCMultiplier_"+finalRatingList).value="";
			 }

	      var frameMultiplier=document.getElementById("frameMultiplier_"+finalRatingList).value;
	      if(frameMultiplier=="" || frameMultiplier=="." || (parseFloat(frameMultiplier)==0 ))
			 {
	    	  frameMultiplier=0;
	    	  document.getElementById("frameMultiplier_"+finalRatingList).value="";
			 }

	      var applicationMultiplier=document.getElementById("applicationMultiplier_"+finalRatingList).value;
	      if(applicationMultiplier=="" || applicationMultiplier=="." || (parseFloat(applicationMultiplier)==0 ))
			 {
	    	  applicationMultiplier=0;	
	    	  document.getElementById("applicationMultiplier_"+finalRatingList).value="";
			 }

	      
	      var factorMultiplier=document.getElementById("factorMultiplier_"+finalRatingList).value; 
	      if(factorMultiplier=="" || factorMultiplier=="." || (parseFloat(factorMultiplier)==0 ))
			 {
	    	  factorMultiplier=0;	
	    	  document.getElementById("factorMultiplier_"+finalRatingList).value="";
			 }

	      var netmcMultiplier=document.getElementById("netmcMultiplier_"+finalRatingList).value;

	      if(netmcMultiplier=="" || (parseFloat(netmcMultiplier)==0) || isNaN(netmcMultiplier.replace(/,/g, "")))
			 {
	    	  netmcMultiplier=0;	
	    	  document.getElementById("netmcMultiplier_"+finalRatingList).value="0";
			 }

	      var markupMultiplier=document.getElementById("markupMultiplier_"+finalRatingList).value; 
	      if(markupMultiplier=="")
			 {
	    	  markupMultiplier=0;	
			 }

	      var transferCMultiplier=document.getElementById("transferCMultiplier_"+finalRatingList).value; 
	      if(transferCMultiplier=="")
			 {
	    	  transferCMultiplier=0;	
			 }
	      

	  	var techRates=(parseFloat(quantityMultiplier)+parseFloat(ratedOutputPNMulitplier)+parseFloat(typeMultiplier)+parseFloat(areaCMultiplier)+parseFloat(frameMultiplier)+parseFloat(applicationMultiplier)+parseFloat(factorMultiplier));
	    var commonMultipliercount=document.getElementById("commonMultiplierCount").value;
	    var TotalFinalRateCount=(parseFloat(techRates)+parseFloat(commonMultipliercount));
	  	document.getElementById("markupMultiplier_"+finalRatingList).value=TotalFinalRateCount.toFixed(1); //Clear
	  	

	  	
	  	var TotalTransferCount=netmcMultiplier.replace(/,/g, "")*((parseFloat(1)+parseFloat(TotalFinalRateCount/100)));
	  	TotalTransferCount=Math.round(TotalTransferCount);
	  document.getElementById("transferCMultiplier_"+finalRatingList).value=TotalTransferCount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");	

	  	
	    }//If Loop Ending
	    if(finalRatingList.indexOf(",")!=-1)
	    {
	    	var res = finalRatingList.split(",");
	    	for(var j=0;j<res.length;j++)
	    	{
	    		
	    		finalRatingList=res[j];
	  	      var quantityMultiplier=document.getElementById("quantityMultiplier_"+finalRatingList).value;
	 	  	 if(quantityMultiplier=="" || quantityMultiplier=="." || (parseFloat(quantityMultiplier)==0 ))
	 		 {
	 	  		quantityMultiplier=0;
	 	  		document.getElementById("quantityMultiplier_"+finalRatingList).value="";
	 		 }

	 	      var ratedOutputPNMulitplier=document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value;
	 		  if(ratedOutputPNMulitplier=="" || ratedOutputPNMulitplier=="." || (parseFloat(ratedOutputPNMulitplier)==0 ))
	 			 {
	 		  		ratedOutputPNMulitplier=0;	
	 		  		document.getElementById("ratedOutputPNMulitplier_"+finalRatingList).value="";
	 			 }

	 	      var typeMultiplier=document.getElementById("typeMultiplier_"+finalRatingList).value;
	 	      if(typeMultiplier=="" || typeMultiplier=="." || (parseFloat(typeMultiplier)==0 ))
	 			 {
	 	    	  typeMultiplier=0;	
	 	    	 document.getElementById("typeMultiplier_"+finalRatingList).value="";
	 			 }

	 	      var areaCMultiplier=document.getElementById("areaCMultiplier_"+finalRatingList).value;
	 	      if(areaCMultiplier=="" || areaCMultiplier=="." || (parseFloat(areaCMultiplier)==0 ))
	 			 {
	 	    	  areaCMultiplier=0;
	 	    	 document.getElementById("areaCMultiplier_"+finalRatingList).value="";
	 			 }

	 	      var frameMultiplier=document.getElementById("frameMultiplier_"+finalRatingList).value;
	 	      if(frameMultiplier=="" || frameMultiplier=="." || (parseFloat(frameMultiplier)==0 ))
	 			 {
	 	    	  frameMultiplier=0;
	 	    	 document.getElementById("frameMultiplier_"+finalRatingList).value=""
	 			 }

	 	      var applicationMultiplier=document.getElementById("applicationMultiplier_"+finalRatingList).value;
	 	      if(applicationMultiplier=="" || applicationMultiplier=="." || (parseFloat(applicationMultiplier)==0 ))
	 			 {
	 	    	  applicationMultiplier=0;	
	 	    	 document.getElementById("applicationMultiplier_"+finalRatingList).value="";
	 			 }

	 	      
	 	      var factorMultiplier=document.getElementById("factorMultiplier_"+finalRatingList).value; 
	 	      if(factorMultiplier=="" || factorMultiplier=="." || (parseFloat(factorMultiplier)==0 ))
	 			 {
	 	    	  factorMultiplier=0;	
	 	    	 document.getElementById("factorMultiplier_"+finalRatingList).value="";
	 			 }

	 	      var netmcMultiplier=document.getElementById("netmcMultiplier_"+finalRatingList).value;
	 	      if(netmcMultiplier=="" || (parseFloat(netmcMultiplier)==0) || isNaN(netmcMultiplier.replace(/,/g, "")))
	 			 {
	 	    	  netmcMultiplier=0;
	 	    	 document.getElementById("netmcMultiplier_"+finalRatingList).value="0";
	 			 }

	 	      var markupMultiplier=document.getElementById("markupMultiplier_"+finalRatingList).value; 
	 	      if(markupMultiplier=="")
	 			 {
	 	    	  markupMultiplier=0;	
	 			 }

	 	      var transferCMultiplier=document.getElementById("transferCMultiplier_"+finalRatingList).value; 
	 	      if(transferCMultiplier=="")
	 			 {
	 	    	  transferCMultiplier=0;	
	 			 }

	 	  	var techRates=(parseFloat(quantityMultiplier)+parseFloat(ratedOutputPNMulitplier)+parseFloat(typeMultiplier)+parseFloat(areaCMultiplier)+parseFloat(frameMultiplier)+parseFloat(applicationMultiplier)+parseFloat(factorMultiplier));
	 	    var commonMultipliercount=document.getElementById("commonMultiplierCount").value;
	 	    var TotalFinalRateCount=(parseFloat(techRates)+parseFloat(commonMultipliercount));
	 	  	document.getElementById("markupMultiplier_"+finalRatingList).value=TotalFinalRateCount.toFixed(1);

	 	  	
		  	var TotalTransferCount=netmcMultiplier.replace(/,/g, "")*((parseFloat(1)+parseFloat(TotalFinalRateCount/100)));
		  	TotalTransferCount=Math.round(TotalTransferCount);;
	 	  	document.getElementById("transferCMultiplier_"+finalRatingList).value=TotalTransferCount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");	

	   

	    	}//For Loop Ending
	    }


	}//Tech Rates Checking
		
	
	
return true;
}
function commasepatedAmount(value)
{
		var x=value;
  		x=x.toString();
  		var lastThree = x.substring(x.length-3);
  		var otherNumbers = x.substring(0,x.length-3);
  		if(otherNumbers != '')
  		    lastThree = ',' + lastThree;
  		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
  		return res;
	
}


function calculateMultipliersForRSM(ratingId)
{
	
	var warrantyAfterDispatchDate=document.getElementById("warrantyAfterDispatchDate").value;
	if(warrantyAfterDispatchDate=="" || warrantyAfterDispatchDate=="." || (parseFloat(warrantyAfterDispatchDate)==0 ) || isNaN(warrantyAfterDispatchDate.replace(/,/g, "")))
	{
		warrantyAfterDispatchDate=0;	
	}
	var warrantyAfterCommissionDate=document.getElementById("warrantyAfterCommissionDate").value;
	if(warrantyAfterCommissionDate=="" || warrantyAfterCommissionDate=="." || (parseFloat(warrantyAfterCommissionDate)==0 ) || isNaN(warrantyAfterCommissionDate.replace(/,/g, "")))
	{
		warrantyAfterCommissionDate=0;	
	}
	var quantitymultiplier=document.getElementById("quantitymultiplier_"+ratingId).value;
	if(quantitymultiplier=="" || quantitymultiplier=="." || (parseFloat(quantitymultiplier)==0 ) || isNaN(quantitymultiplier.replace(/,/g, "")))
	{
		quantitymultiplier=0;	
	}
	var transferCMultiplier=document.getElementById("transferCMultiplier_"+ratingId).value;
	if(transferCMultiplier=="" || transferCMultiplier=="." || (parseFloat(transferCMultiplier)==0 ) || isNaN(transferCMultiplier.replace(/,/g, "")))
	{
		transferCMultiplier=0;
	}
	var mcornspratioMultiplier=document.getElementById("mcornspratioMultiplier_"+ratingId).value;
	if(mcornspratioMultiplier=="" || mcornspratioMultiplier=="." || (parseFloat(mcornspratioMultiplier)==0 ) || isNaN(mcornspratioMultiplier.replace(/,/g, "")))
	{
		mcornspratioMultiplier=0;
		document.getElementById("mcornspratioMultiplier_"+ratingId).value="";
		

		document.getElementById("unitpriceMultiplier_rsm_"+ratingId).value="";
		document.getElementById("totalpriceMultiplier_rsm_"+ratingId).value="";
		
	}
	var transportationperunit_rsm=document.getElementById("transportationperunit_rsm_"+ratingId).value;
	if(transportationperunit_rsm=="" || transportationperunit_rsm=="." || (parseFloat(transportationperunit_rsm)==0 ) || isNaN(transportationperunit_rsm.replace(/,/g, "")))
	{
		transportationperunit_rsm=0;	
		document.getElementById("transportationperunit_rsm_"+ratingId).value="";
	}
	var warrantyperunit=document.getElementById("warrantyperunit_rsm_"+ratingId).value;

	if(warrantyperunit=="" || warrantyperunit=="." || (parseFloat(warrantyperunit)==0 ) || isNaN(warrantyperunit.replace(/,/g, "")))
	{
		warrantyperunit=0;
	document.getElementById("warrantyperunit_rsm_"+ratingId).value="";
	}
	var totalunitprice=0;
    if(mcornspratioMultiplier!=0)
    {

    	mcornspratioMultiplier = mcornspratioMultiplier.replace(/,/g, "");
    	
    	if(transferCMultiplier==0)
    	{
    		transferCMultiplier=0;
    	}
    	else
    	{
    		transferCMultiplier=transferCMultiplier.replace(/,/g, "");
    		
    	}
    	if(transportationperunit_rsm==0)
    	{
    		transportationperunit_rsm=0;	
    	}
    	else
    	{
    		transportationperunit_rsm=transportationperunit_rsm.replace(/,/g, "");
    		
    	}
    	if(warrantyperunit==0)
    	{
    		warrantyperunit=0;
    	}
    	else
    	{
    		warrantyperunit=warrantyperunit.replace(/,/g, "");	
    		
    	}
    	
    	totalunitprice=(parseFloat(transferCMultiplier)+(parseFloat(transferCMultiplier)*(parseFloat(transportationperunit_rsm)/100)))*(1-(parseFloat(mcornspratioMultiplier)/100))+parseFloat(warrantyperunit);	
    }// If Loop Ending
    if(totalunitprice!=0)
    	{

    	totalunitprice=Math.round(totalunitprice);
	    document.getElementById("unitpriceMultiplier_rsm_"+ratingId).value=totalunitprice.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    	}
    var unit_price=document.getElementById("unitpriceMultiplier_rsm_"+ratingId).value;
    if(unit_price=="" || unit_price=="." || parseFloat(unit_price)==0 || isNaN(unit_price.replace(/,/g, "")))
    {
    	unit_price=0;	
    }
    if(unit_price==0)
    {
    	unit_price=0;	
    }
    else
    {
    	unit_price=unit_price.replace(/,/g, "");
    }
    if(mcornspratioMultiplier!=0)
    {
    
    document.getElementById("mcornspratioMultiplier_"+ratingId).value=mcornspratioMultiplier.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    if(transportationperunit_rsm!=0)
    {
    
    document.getElementById("transportationperunit_rsm_"+ratingId).value=transportationperunit_rsm.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    if(warrantyperunit!=0)
    {
    	
    document.getElementById("warrantyperunit_rsm_"+ratingId).value=warrantyperunit.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    
	var totalpricemultipliers=(parseFloat(quantitymultiplier))*parseFloat(unit_price);
	totalpricemultipliers=Math.round(totalpricemultipliers);

    

    document.getElementById("totalpriceMultiplier_rsm_"+ratingId).value=totalpricemultipliers.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    

return true;
}
function submitEstimateForRSM()
{
	var finalRatingList=document.getElementById("finalRatingList").value;
	var numeric = /^\d+(?:\.\d{1})?$/;

	if(finalRatingList!="")
	{
		
	    if(finalRatingList.indexOf(",")==-1)
	    {
	    	var isvalidated=true;
	    	
	    	var listPrice=document.getElementById("transferCMultiplier_"+finalRatingList).value;
		  	
	      	if(listPrice=="")
	      	{
		  		alert("Please Enter List Price Multiplier Value");
		  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;

	    	 }

	      	if(listPrice!="" && parseFloat(listPrice)==0)
			 {
		  		alert("Please Enter Valid List Price Multiplier Value ");
		  		document.getElementById("transferCMultiplier_"+finalRatingList).value="";
		  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;
			 }
		  	 if(listPrice!="" && !numeric.test(listPrice))
			 {
		  		alert("Please Enter Valid  List Price Multiplier Value ");
		  		document.getElementById("transferCMultiplier_"+finalRatingList).value="";
		  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
		  		isvalidated=false;
		  		return false;
			 }
		      var mcornspratioMultiplier=document.getElementById("mcornspratioMultiplier_"+finalRatingList).value;
		       if(mcornspratioMultiplier=="")
		    	{
			  		alert("Please Enter Discount Multiplier Value");
			  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
			  		isvalidated=false;
			  		return false;

		    	 }
			  	 if(mcornspratioMultiplier!="" && parseFloat(mcornspratioMultiplier)==0)
				 {
			  		alert("Please Enter Valid Discount Multiplier Value");
			  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).value="";
			  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
			  		isvalidated=false;
			  		return false;
				 }
			  	 if(mcornspratioMultiplier!="" && !numeric.test(mcornspratioMultiplier))
				 {
			  		alert("Please Enter Valid Discount Multiplier Value");
			  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).value="";
			  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
			  		isvalidated=false;
			  		return false;
				 }
			  var transportationperunit=document.getElementById("transportationperunit_rsm_"+finalRatingList).value;
			  if(transportationperunit!="" && parseFloat(transportationperunit)==0)
			   {
				  			
				  		alert("Please Enter Valid Addon Percent Per Unit Multiplier Value");
				  		
				  		document.getElementById("transportationperunit_rsm_"+finalRatingList).value="";
				  		document.getElementById("transportationperunit_rsm_"+finalRatingList).focus();

				  		isvalidated=false;
				  		return false;
			  }
			  var warrantyperunit=document.getElementById("warrantyperunit_rsm_"+finalRatingList).value;
			  if(warrantyperunit!="" && parseFloat(warrantyperunit)==0)
			   {
				  			
				  		alert("Please Enter Valid Addon Value Per Unit Multiplier Value");
				  		document.getElementById("warrantyperunit_rsm_"+finalRatingList).value="";
				  		document.getElementById("warrantyperunit_rsm_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
			  }
			  var unitpriceMultiplier=document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).value;
			  if(unitpriceMultiplier!="" && parseFloat(unitpriceMultiplier)==0)
			   {
				  			
				  		alert("Please Enter Valid Unit Price Multiplier Value");
				  		document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).value="";
				  		document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
			  }
			  var totalpriceMultiplier=document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).value;
			  if(totalpriceMultiplier!="" && parseFloat(totalpriceMultiplier)==0)
			   {
				  			
				  		alert("Please Enter Valid Total Prices Multiplier Value");
				  		document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).value="";
				  		document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
			  }
		          if(isvalidated)
		          {
 
		        	document.enquiryForm.submit();
		          }


	    }
	    if(finalRatingList.indexOf(",")!=-1)
	    {
	    	var isvalidated=true;
	    	var res = finalRatingList.split(",");
	    	
	    	for(var j=0;j<res.length;j++)
	    	{
	    		
	    		finalRatingList=res[j];
	    		
		    	var isvalidated=true;
		    	
			      var listPrice=document.getElementById("transferCMultiplier_"+finalRatingList).value;
				  	
			      	if(listPrice=="")
			      	{
				  		alert("Please Enter List Price Multiplier Value");
				  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;

			    	 }

			      	if(listPrice!="" && parseFloat(listPrice)==0)
					 {
				  		alert("Please Enter Valid List Price Multiplier Value For Rating "+(j+1));
				  		document.getElementById("transferCMultiplier_"+finalRatingList).value="";
				  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
					 }
				  	 if(listPrice!="" && !numeric.test(listPrice))
					 {
				  		alert("Please Enter Valid  List Price Multiplier Value For Rating "+(j+1));
				  		document.getElementById("transferCMultiplier_"+finalRatingList).value="";
				  		document.getElementById("transferCMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
					 }
					 
		    	
			      var mcornspratioMultiplier=document.getElementById("mcornspratioMultiplier_"+finalRatingList).value;
				  	
			      	if(mcornspratioMultiplier=="")
			      	{
				  		alert("Please Enter  Discount Multiplier Value");
				  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;

			    	 }

			      	if(mcornspratioMultiplier!="" && parseFloat(mcornspratioMultiplier)==0)
					 {
				  		alert("Please Enter Valid Discount Multiplier Value For Rating "+(j+1));
				  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).value="";
				  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
					 }
				  	 if(mcornspratioMultiplier!="" && !numeric.test(mcornspratioMultiplier))
					 {
				  		alert("Please Enter Valid  Discount Multiplier Value For Rating "+(j+1));
				  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).value="";
				  		document.getElementById("mcornspratioMultiplier_"+finalRatingList).focus();
				  		isvalidated=false;
				  		return false;
					 }

				  var transportationperunit=document.getElementById("transportationperunit_rsm_"+finalRatingList).value;
				  if(transportationperunit!="" && parseFloat(transportationperunit)==0)
				   {
					  			
					  		alert("Please Enter Valid Addon Percent Per Unit Multiplier Value For Rating "+(j+1));
					  		document.getElementById("transportationperunit_rsm_"+finalRatingList).value="";
					  		document.getElementById("transportationperunit_rsm_"+finalRatingList).focus();
					  		isvalidated=false;
					  		return false;
				  }
				  var warrantyperunit=document.getElementById("warrantyperunit_rsm_"+finalRatingList).value;
				  if(warrantyperunit!="" && parseFloat(warrantyperunit)==0)
				   {
					  			
					  		alert("Please Enter Valid Addon Value Per Unit Multiplier Value For Rating "+(j+1));
					  		document.getElementById("warrantyperunit_rsm_"+finalRatingList).value="";
					  		document.getElementById("warrantyperunit_rsm_"+finalRatingList).focus();
					  		isvalidated=false;
					  		return false;
				  }
				  var unitpriceMultiplier=document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).value;
				  if(unitpriceMultiplier!="" && parseFloat(unitpriceMultiplier)==0)
				   {
					  			
					  		alert("Please Enter Valid Unit Price Multiplier Value For Rating "+(j+1));
					  		document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).value="";
					  		document.getElementById("unitpriceMultiplier_rsm_"+finalRatingList).focus();
					  		isvalidated=false;
					  		return false;
				  }
				  var totalpriceMultiplier=document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).value;
				  if(totalpriceMultiplier!="" && parseFloat(totalpriceMultiplier)==0)
				   {
					  			
					  		alert("Please Enter Valid Total Prices Multiplier Value For Rating "+(j+1));
					  		document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).value="";
					  		document.getElementById("totalpriceMultiplier_rsm_"+finalRatingList).focus();
					  		isvalidated=false;
					  		return false;
				  }
	    	}//For Loop Ending
	    	if(isvalidated==true)
    		{
	    	
	    	document.enquiryForm.submit();

    		}

	    }//IF Loop Ending
	}//First If Loop Ending

}

function returnEnquiry(){
	document.enquiryForm.invoke.value = "returnEnquiry";
	document.enquiryForm.submit();
}
/* This function is to mail the quote to Custmer */
function emailQuoteToCustomer()
{
	/*  TODO Blocked By EGR 26-11-2018*/
/* 	if(document.enquiryForm.termsVarFormula.value=="")
 	{   	alert("Please Enter Terms Variable Formula in Terms No.1");
	 	document.enquiryForm.termsVarFormula.focus();
	 	return false;
	}
		if(document.enquiryForm.termsDeliveryLT.value=="")
 	{
 	alert("Please Enter Terms Delivery LT in Terms No.2");
	 	document.enquiryForm.termsDeliveryLT.focus();
	 	return false;
	}
		if(document.enquiryForm.termsDeliveryHT.value=="")
 	{
 	alert("Please Enter Terms Delivery HT in Terms No.2");
	 	document.enquiryForm.termsDeliveryHT.focus();
	 	return false;
	}
	if(document.enquiryForm.termsPercentAdv.value=="" || isNaN(document.enquiryForm.termsPercentAdv.value)){
	    alert("Please Enter valid Terms Percentege Advacnce in Terms No.3");
	 	document.enquiryForm.termsPercentAdv.focus();
	 	return false;
	}
	
	if(document.enquiryForm.termsPercentBal.value=="" || isNaN(document.enquiryForm.termsPercentBal.value)){
 	     alert("Please Enter valid Terms Percentege Balence in Terms No.3");
	 	document.enquiryForm.termsPercentBal.focus();
	 	return false;
	}
	var pValue=parseInt(document.enquiryForm.termsPercentBal.value)+parseInt(document.enquiryForm.termsPercentAdv.value);
	
	if(pValue != 100){
	    document.enquiryForm.termsPercentAdv.focus();
	    alert("Sum of Balance percentage and Advance percentage should be 100.");
	    return false;
	}
*/	
	
	document.enquiryForm.action="createEnquiry.do";
	document.enquiryForm.invoke.value="emailQuoteToCustomer";
	document.enquiryForm.submit();
}

function backToViewQuotation() {
 	document.enquiryForm.action="createEnquiry.do?operationType=quotationData";
	document.enquiryForm.invoke.value="viewEnquiry";
	document.enquiryForm.submit();

}

function viewPDF() {
    var enquiryId=document.enquiryForm.enquiryId.value;
    
	document.enquiryForm.action="viewQuotatiionPdf?enquiryId="+enquiryId;
	document.enquiryForm.method="POST";
	document.enquiryForm.submit();
}

function viewNewQuotationPDF() {
	var enquiryId = document.newenquiryForm.enquiryId.value;
	
	document.newenquiryForm.action="viewNewQuotationPdf?enquiryId="+enquiryId;
	document.newenquiryForm.method="POST";
	document.newenquiryForm.submit();
}


/*function viewIndentPage(value, enquiryId) {
	
    
    if(IndentPage=="Won")
    {
    	
    	document.getElementById("operationType").value="WON";
    	document.enquiryForm.submit();
    }
    if(IndentPage=="Lost")
    {
    	
    	document.getElementById("operationType").value="LOST";
    	document.enquiryForm.submit();
    }
    if(IndentPage=="Abonded")
    {
    	
    	document.getElementById("operationType").value="ABONDED";
    	document.enquiryForm.submit();
    }

    

}*/

function createRevision() {
    document.enquiryForm.invoke.value="createRevision"
    
    document.enquiryForm.method="POST";
    document.enquiryForm.action="createEnquiry.do"
    document.enquiryForm.submit();
}

function download(attachmentId, fileName, fileType){
	window.location = "viewEnquiry.do?invoke=downloadAttachment&attachmentId="+attachmentId+"&fileName="+fileName+"&fileType="+fileType;	
}
function viewAttachments()
{
	
	
	  var x = document.getElementById("attachmentDiv");
	  
	  if (x.style.display === "none") {
	    x.style.display = "block";
	  } else {
	    x.style.display = "none";
	  }

}

function manageAttachments(applicationId, uniqueId, attachedBy){
  	
	var strURL = document.getElementById('quotationUrl').value+"/attachments/home.do?invoke=home&applicationid="+applicationId+"&uniqueid="+uniqueId+"&attachedBy="+attachedBy;"" 
			
	//var strURL = "http://172.16.22.246:11001/attachments/home.do?invoke=home&applicationid="+applicationId+"&uniqueid="+uniqueId+"&attachedBy="+attachedBy;
	popwindow = window.open(strURL, "theMainWin", "scrollbars=yes,resizable=yes,toolbar=no,status=no,height=450,width=705");	
	popwindow.focus();
  	



  /*
   * 
   * TODO
   var strURL = "http://172.16.22.246:11001/attachments/home.do?invoke=home&applicationid="+applicationId+"&uniqueid="+uniqueId+"&attachedBy="+attachedBy;	
	window.open(strURL, "theMainWin", "scrollbars=yes,resizable=yes,toolbar=no,status=no,height=450,width=705");
	
*/

}

function calculateNTorque(){
	
	if(document.enquiryForm.kw.value != null){
		var ratedOutput = parseFloat(document.enquiryForm.kw.value);		
		if(document.enquiryForm.rpm.value != null && !isNaN(document.enquiryForm.rpm.value)){			
			var speed = parseFloat(document.enquiryForm.rpm.value);
			if(document.enquiryForm.ratedOutputUnit.value == 'kW')
				document.enquiryForm.fltSQCAGE.value = Math.ceil(9.81*(973*ratedOutput/speed));
			else
				document.enquiryForm.fltSQCAGE.value = Math.ceil(0.746*9.81*(973*ratedOutput/speed));
			if(isNaN(document.enquiryForm.fltSQCAGE.value)){
				document.enquiryForm.fltSQCAGE.value="";
			}
		}
	}
}

function calCurrent(){	
	
	
	if(document.enquiryForm.kw.value != null){
		var ratedOutput = parseFloat(document.enquiryForm.kw.value);
		var ratedVoltage = document.enquiryForm.volts.value;	
		if(ratedVoltage.indexOf('KV')!=-1){
			ratedVoltage = parseFloat(ratedVoltage.substring(0,ratedVoltage.length-2));
		}
		else{
			ratedVoltage = parseFloat(ratedVoltage.substring(0,ratedVoltage.length-1))/1000;
		}
	
		if( ratedVoltage!=0.0 ){
			//for 100%load
			var efficiency =  document.enquiryForm.pllEff1.value;
			var powerFactor = document.enquiryForm.pllPf1.value;
            if(efficiency=="")
            {
            	efficiency=0.0;
            }
            else
            {
            	efficiency =  parseFloat(document.enquiryForm.pllEff1.value);
            }
            if(powerFactor=="")
            {
            	powerFactor=0.0;
            }
            else
            {
            	powerFactor =  parseFloat(document.enquiryForm.pllPf1.value);
            }
			if(efficiency!=0.0 && powerFactor!= 0.0){	
				if(document.enquiryForm.ratedOutputUnit.value == 'kW')
					document.enquiryForm.pllCurr1.value = Math.ceil(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor)*100);
				else
					document.enquiryForm.pllCurr1.value = Math.ceil(0.745 *(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				efficiency=0.0;
				powerFactor=0.0;
			}
			efficiency =  document.enquiryForm.pllEff2.value;
			powerFactor = document.enquiryForm.pllPf2.value;
            if(efficiency=="")
            {
            	efficiency =0.0;
            }
            else
            {
            	efficiency =  parseFloat(document.enquiryForm.pllEff2.value);	
            }
            if(powerFactor=="")
            {
    			powerFactor = 0.0;
            }
            else
            {
    			powerFactor =  parseFloat(document.enquiryForm.pllPf2.value);

            }
			
			
			if(efficiency!=0.0 && powerFactor!= 0.0){				
				if(document.enquiryForm.ratedOutputUnit.value == 'kW')
		
					document.enquiryForm.pllCurr2.value = Math.ceil(0.75*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				else
		
					document.enquiryForm.pllCurr2.value = Math.ceil(0.745*0.75*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				efficiency=0.0;
				powerFactor=0.0;
			}
			//for 50%load
			efficiency = document.enquiryForm.pllEff3.value;
			powerFactor = document.enquiryForm.pllpf3.value;
			if(efficiency=="")
			{
				efficiency=0.0;
			}
			else
			{

				efficiency =  parseFloat(document.enquiryForm.pllEff3.value);

			}
			if(powerFactor=="")
			{
				powerFactor = 0.0;
			}
			else
			{
				powerFactor =  parseFloat(document.enquiryForm.pllpf3.value);
			}

		
			if(efficiency!=0.0 && powerFactor!= 0.0){
				if(document.enquiryForm.ratedOutputUnit.value == 'kW')
	
					document.enquiryForm.pllCurr3.value = Math.ceil(0.50*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				else 
			
					document.enquiryForm.pllCurr3.value = Math.ceil(0.745*0.50*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				efficiency=0.0;
				powerFactor=0.0;
			}
			
			if(document.enquiryForm.pllCurr1.value!="")
				updateCurrent(document.enquiryForm.pllCurr1.value);
	}
	}
	
		
		
}
function ChangeMCUnit(mcvalue)
{
	if(mcvalue=="" || mcvalue=="." || parseFloat(mcvalue)==0 || isNaN(mcvalue.replace(/,/g, "")))
	{
		document.getElementById("commercialMCUnit").value="";
		document.getElementById("amountconversiontowords").innerHTML="";
	}
	else
	{

  	document.getElementById("commercialMCUnit").value=mcvalue;
  	var wordamount=convertNumberToWords(mcvalue);
  	document.getElementById("amountconversiontowords").innerHTML=wordamount+" Rupees Only";
	}
}


function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
function revisionForsm()
{
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
}

function dereturntosm()
{
	
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
	document.getElementById("operationType").value="DE";
	document.getElementById("pagechecking").value="de";
		
			
}
function estimateForsm(){
	obj=document.getElementById('estimate')
	obj.style.display="block"
	obj1=document.getElementById('return')
	obj1.style.display="none"
	}

function checkMaxLenghtText(limitField, limitNum) {
    if (limitField.value.length > limitNum) {
    	alert("Your text length is exceeding maximum length "+limitNum)
        limitField.value = limitField.value.substring(0, limitNum);
    } 
}

//Indent Data Added By Egr on 15-March-2018

function viewIndentPage(value, enquiryId) {
    
  
    var IndentPage=value;
    var dt = new Date();
    
    document.getElementById('orderClosingMonth').value =(dt.getDate()+"-"+(dt.getMonth()+1)+"-"+dt.getFullYear());    
    update1(enquiryId);
    
    if(IndentPage=="Won")
    {
    	
        document.enquiryForm.method="POST";
    	document.getElementById("operationType").value="WON";
        document.enquiryForm.action="viewEnquiry.do"
        document.enquiryForm.submit();

    }
    if(IndentPage=="Lost")
    {
    	
        document.enquiryForm.method="POST";
    	document.getElementById("operationType").value="LOST";
        document.enquiryForm.action="viewEnquiry.do"
        document.enquiryForm.submit();

    }
    if(IndentPage=="Abonded")
    {
    	
        document.enquiryForm.method="POST";
    	document.getElementById("operationType").value="ABONDED";
        document.enquiryForm.action="viewEnquiry.do"
        document.enquiryForm.submit();

    }


    

}


function viewCommercialEngineers(){
    var url = 'searchcommercialengineer.do?invoke=viewCommercialEngineer';
	window.location = url;
}

/*
 * JavaScript Added By Gangadhar on 14-Feb-2019
 * For Disable Textboxes Under Financial Section in Indent Won Page
 * */
function getFinancialValues(styleId,svalue)
{
	if(svalue=="0")
	{
		var x = document.getElementById(styleId+"Val");
		document.getElementById(styleId+"Val").value=""
		  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
				  x.style.display = "none";  
			  }
	}
	else
	{
		var x = document.getElementById(styleId+"Val");
		  if (x.style.display === "none") {
			    x.style.display = "block";
			  } else {
				  x.style.display = "block";  
			  }
		
		
	}

	
}

function getNextRatingWonIndentPage(ratingId)
{
	
	if(document.getElementById("savingIndent").value == null || document.getElementById("savingIndent").value ==""){
		 alert("Please Enter Indent No.");
		 document.getElementById("savingIndent").focus();
		 return false;
	}
		
	document.getElementById("currentRating").value = ratingId;	
	
	document.getElementById("operationType").value = 'updateratingwonindentpage';	
	
	document.getElementById("isValidated").value = 'Y';
	
	document.enquiryForm.submit();

}


function wonSubmitToEstimate(){	
	
	 if (document.enquiryForm.advancePayment.value== "" ||  document.enquiryForm.advancePayment.value== 0){
	 	
		document.enquiryForm.advancePayment.value=20;
	 	
	}
	
	if (document.enquiryForm.paymentTerms.value== "" ||   document.enquiryForm.paymentTerms.value== 0){
	 	
		document.enquiryForm.paymentTerms.value=80;
	 	
	}

	document.enquiryForm.operationType.value = 'updateratingwonindentpage';
	document.enquiryForm.dispatch.value = 'wontoestimate';
	document.enquiryForm.isValidated.value = 'Y';
	document.enquiryForm.submit();	

}
function lostSubmitToEstimate(){	
	var lossPrice=document.getElementById("lossPrice").value;
	var lossReason1=document.getElementById("lossReason1").value;
	var lossReason2=document.getElementById("lossReason2").value;
	var lossComment=document.getElementById("lossComment").value;
	var lossCompetitor=document.getElementById("lossCompetitor").value;
	if(lossPrice=="")
	{
	   alert("Please Enter Loss Price");
	   return false;
	}
	else if(lossReason1=="")
	{
	  alert("Please  Select Loss Reason-I");
	  return false;
	}
	else if(lossReason2=="")
	{
	  alert("Please Select Loss Reason-II ");
	  return false;
	}
	else if(lossComment=="")
	{
	  alert("Please Enter Loss Comment");
	  return false;
	}
	else if(lossCompetitor=="")
	{
	  alert("Please Select Loss Competitor");
	  return false;
	}
	else
		{
	
	document.enquiryForm.operationType.value = 'updateratinglostindentpage';
	document.enquiryForm.dispatch.value = 'losttoestimate';
	document.enquiryForm.isValidated.value = 'Y';
	document.enquiryForm.submit();	
		}

}
function abondentSubmitToEstimate()
{
	var abondentComment=document.getElementById("abondentComment").value;
	if(abondentComment=="")
	{
	  alert("Please Enter Comment");
	  return false;
	}
	else
	{
	document.enquiryForm.operationType.value = 'updateratingabondentindentpage';
	document.enquiryForm.dispatch.value = 'abondmenttoestimate';
	document.enquiryForm.isValidated.value = 'Y';
	document.enquiryForm.submit();	
	}

}

/* Function to view Enquiry Based on its Status  */
function viewIndentPageDetails(enquiryId,statusId,statusName) {
   var IndentPage=statusName;
   
   if(IndentPage=="Won")
   {
   	
       var url="viewEnquiry.do?invoke=updateFactor&enquiryId="+enquiryId+"&statusId="+statusId+"&operationType="+statusName+"&returnType="+IndentPage;
       window.location = url;


   }

}

function dmreturntosalesmanager()
{
	
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
	document.getElementById("operationType").value="DMWON";
	document.getElementById("pagechecking").value="dmwon";
		
			
}
function dereturntosalesmanager()
{
	
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
	document.getElementById("operationType").value="DEWON";
	document.getElementById("pagechecking").value="dewon";
		
			
}

function submitToWonDesignMgr()
{
		document.enquiryForm.operationType.value = 'updaterating';
		document.enquiryForm.dispatch.value = 'manager';
		document.enquiryForm.isValidated.value = 'Y';	
		document.enquiryForm.submit();	

}
function WON()
{
		document.enquiryForm.operationType.value = 'updaterating';
		document.enquiryForm.dispatch.value = 'estimate';
		document.enquiryForm.isValidated.value = 'Y';
		document.enquiryForm.submit();	
}

function cereturntosalesmanager()
{
	
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
	document.getElementById("operationType").value="CEWON";
	document.getElementById("pagechecking").value="cewon";
		
			
}
function submitToCommercialManager(){	
	document.enquiryForm.operationType.value = 'updaterating';
	document.enquiryForm.dispatch.value = 'commercialmanager';
	document.enquiryForm.isValidated.value = 'Y';	
	document.enquiryForm.submit();	

}
function cmreturntosalesmanager()
{
	
	obj=document.getElementById('return')
	obj.style.display="block"
	obj1=document.getElementById('estimate')
	obj1.style.display="none"
	document.getElementById("operationType").value="CMWON";
	document.getElementById("pagechecking").value="cmwon";
		
			
}
function submitToDesignManager(){	
	document.enquiryForm.operationType.value = 'updaterating';
	document.enquiryForm.dispatch.value = 'estimate';
	document.enquiryForm.isValidated.value = 'Y';
	document.enquiryForm.submit();	
}

function estimateForcm(){
	obj=document.getElementById('estimate')
	obj.style.display="block"
	obj1=document.getElementById('return')
	obj1.style.display="none"
	document.getElementById("operationType").value="CM";
	document.getElementById("pagechecking").value="cm";

}