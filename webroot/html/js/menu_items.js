/*
  --- menu items --- 
*/
var MENU_ITEMS = [
	/*
	['Level-1', 'http://www.regalbeloit.com/',null, 
		['LEVEL-2', 'http://www.regalbeloit.com/',
			['Level-3 - A', 'http://www.regalbeloit.com/',
				['Level-4 - 1','http://www.regalbeloit.com/'],
				['Level-4 - 2','http://www.regalbeloit.com/'],
				['Level-4 - 3','http://www.regalbeloit.com/'],
				['Level-4 - 4','http://www.regalbeloit.com/'],
				['Level-4 - 5','http://www.regalbeloit.com/']
			],
			['Level-3 -B', 'http://www.regalbeloit.com/', null,
				['Level-4 - 1','http://www.regalbeloit.com/'],
				['Level-4 - 2','http://www.regalbeloit.com/'],
				['Level-4 - 3','http://www.regalbeloit.com/'],
				['Level-4 - 4','http://www.regalbeloit.com/'],
				['Level-4 - 5','http://www.regalbeloit.com/']
			],
			['Level-3 -C', 'http://www.regalbeloit.com/', null,
				['Level-4 - 1','http://www.regalbeloit.com/'],
				['Level-4 - 2','http://www.regalbeloit.com/']
			]
		],		
	],
	*/
	['Home', 'javascript:home()'],
	['Place an Enquiry', 'javascript:placeAnEnquiry()'],
	['Search Enquiry', 'javascript:searchEnquiry()'],
	['Administration', null, null,
			['Manage Masters', 'javascript:viewMasters()'],
			['Manage Master Data', 'javascript:viewMasterData()'],
			['Manage Customers', 'javascript:viewCustomers()'],
			['Manage Locations', 'javascript:viewLocations()'],
			['Replace Managers', 'javascript:viewReplaceMasters()'],
	 		['Manage Sales Managers', 'javascript:viewSalesManagers()'],
	 		['Manage Help Items', 'javascript:viewHelpItems()'],
	 		['Manage Technical Data', 'javascript:viewTechnicaldata()'],
	 		['Manage Customer Discount', 'javascript:viewCustomerDiscount()'],
	 		['Manage List Price', 'javascript:viewListPrice()'],
	],
	['Feedback', 'javascript:openViewFeedbackForm(\'\')']
];

