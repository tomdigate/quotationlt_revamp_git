
function calcNetPrice(){
    var list = document.getElementsByName("hidelist");
    var addonlist = document.getElementsByName("addonhidelist");    
    var objDiscount = document.enquiryForm.discount;    
    var netfield  = new Array();
    var hidefield = new Array();
    var addonfield = new Array();
    var addonhidefield = new Array();
    
    document.getElementById("ratingMaxSize").value = list.length;
    document.getElementById("addonMaxSize").value = addonlist.length;
    var count = 1;
    for(i=1;i<=list.length; i++) {
        netfield[i]=document.getElementById('netPrice_'+i);        
        hidefield[i]=document.getElementById('hidelist_'+i);
        
        for(j=1;j<=addonlist.length; j++) {
        	if (document.getElementById('addonnetPrice_'+i+'_'+j) != null){
	     	   addonfield[count]=document.getElementById('addonnetPrice_'+i+'_'+j);  
    	    	addonhidefield[count]=document.getElementById('addonhidelist_'+i+'_'+j);  
    	    	count++;                       
    	    }
    	}
    }
    
    
    
    if ( objDiscount.value =="" || isNaN(objDiscount.value) ) {
        alert("Enter valid factor value in %");
        objDiscount.value="";
		objDiscount.focus();
    } else {     	
        for ( i=1 ; i<netfield.length ; i++ ) {        	
        	if (hidefield[i].value != null)
           		 netfield[i].value = ( parseInt(hidefield[i].value) + parseInt((hidefield[i].value * objDiscount.value)/100) );						
        }
        
        for ( i=1 ; i<addonfield.length ; i++ ) {        	
        	if (addonhidefield[i].value != null)
           		 addonfield[i].value = ( parseInt(addonhidefield[i].value) + parseInt((addonhidefield[i].value * objDiscount.value)/100) );						
        }
        
        
    }
}