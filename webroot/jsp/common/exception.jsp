<%@page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ include file="../common/nocache.jsp" %>
<%@ include file="../common/library.jsp" %>
<%@ page isErrorPage="true" %>

<style type="text/css">
	BODY {MARGIN: 0px 0px 0px 0px;}
	.bluebar-whitetext {font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #FFFFFF;
	background-color: #4F78AF;}
	.blueheader {FONT-WEIGHT: bolder;FONT-SIZE: 16px;COLOR: #00458A;font-family: Arial, Helvetica, sans-serif;}
</style>
<center>
	<table border="0" cellpadding="2" cellspacing="4" width="480" height="270" >
		<tr ><td height="100%" valign="top" bgcolor="#4F78AF">
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" bordercolordark="#000099" bgcolor="#FFFFFF">
				<tr>
				  <td align="center" class="bluebar-whitetext" height="23px"><div align="left">&nbsp;&nbsp;&nbsp;Error</div></td>
				  <td align="center" class="bluebar-whitetext" height="23px"><div align="right">
				  	<a href="javascript:window.close()"><!-- javascript:window.close() -->
				  		<img src="html/images/close.gif" width="21" height="21" border="0" align="absmiddle" ALT='Close'>
				  	</a>
				  </div></td>
				</tr>
				<tr>
					<td valign="top" height="100%">
						<table width="100%"  border="0">
						  <tr>
							<td width="30" ><div align="right"><img src="html/images/erroricon.gif" vspace="2"></div></td>
							<td class="blueheader"><bean:message key="quotation.messages.exception.heading"/> </td>
						  </tr>
						  <tr><td colspan="2">&nbsp;</td></tr>
						  <tr>
							<td valign="top"><div align="right"><img src="html/images/dot.gif" width="7" height="7"></div></td>
							<td valign="top">
								<logic:present name="<%=QuotationConstants.QUOTATION_ERRORMESSAGE%>" scope="request">
									<bean:write name="<%=QuotationConstants.QUOTATION_ERRORMESSAGE%>" scope="request" filter="false" />
								</logic:present>
							</td>
						  </tr>
						</table>
					</td>
				</tr>
			</table>
	</table>
</center>
