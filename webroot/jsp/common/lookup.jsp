<%@ include file="../common/nocache.jsp" %>
<%@ include file="../common/library.jsp" %>
<%@page import="java.util.ArrayList, in.com.rbc.quotation.user.dto.UserDto" %>

<html:form method="post" action="lookupEmployee.do" onsubmit="return validateSearchEmployee();" >
    <html:hidden property="invoke" />
    <html:hidden property="selectType" />
    <html:hidden property="submitted" />
    <html:hidden property="parentUserIdObj"  />
    <html:hidden property="parentNameObj" />
    <html:hidden property="parentEmailObj" />
    <html:hidden property="parentLocObj" />
    <html:hidden property="parentListObj" />
    <html:hidden property="employeeFilter" />
    <html:hidden property="sizeOfList" />
    
    <%-- 
	Listing taglib hidden fields tag that includes the necessary hidden fields
	related to pagination, at runtime
	 --%>
	<listing:hiddenfields />
    
    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
        <tr><td width="62%" height="25">&nbsp;</td></tr>
        <tr>
            <td class="section-head">
                <fieldset>
                    <legend class="blueheader"> <bean:message key="quotation.lookup.heading" /> </legend>
                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="4" bgcolor="#FFFFFF">
                        <tr>
                            <td height="35">
                                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table width="95%" border="0" align="center" cellpadding="2" cellspacing="0" class="DotsTable">
                                                <tr><td><bean:message key="quotation.lookup.message" /></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formLabel">
                                            <div align="center">
                                                <bean:message key="quotation.lookup.name" />
                                                <html:text property="employeeNameFilter" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#CCD8F2">
                                <div style="height:2px" class="blue-light" align="right">
                                <div style="color:red;font-size:8pt;text-align:right">
                                    <input name="Close" type="button" class="BUTTON" value="Close" onClick="window.close()">&nbsp;
									<input name="Clear" type="button" class="BUTTON" value="Clear" onClick="javascript:clearForm();">&nbsp;
									<html:submit value="Submit" styleClass="BUTTON"></html:submit>
                                    
                                    
									
                                </div></div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <div id="divResults">
    <logic:equal name="userForm" property="submitted" value="1">
    	<!-- Starting of table to display the search results -->
    	<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0">
            <tr>
            	<td>
            		<%-- Displaying 'No Results Found' message if size of list is zero --%>
                    <logic:equal name="userForm" property="sizeOfList" value="0">
                    	<fieldset>
                    		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#F7F7F7">
								<tr>
									<td class="other" style="text-align:center"><bean:message key="quotation.label.admin.noresultsfound"/></td>
								</tr>
							</table>
                    	</fieldset>
                    </logic:equal>
                    <logic:notPresent name="userForm" property="searchResultsList">
                    	<fieldset>
                    		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#F7F7F7">
								<tr>
									<td class="other" style="text-align:center"><bean:message key="quotation.label.admin.noresultsfound"/></td>
								</tr>
							</table>
                    	</fieldset>
                    </logic:notPresent>
                    
                    <%-- 
					Starting of condition to display the rows retrieved from database depending
					on the filter criteria specified
					 --%>
                    <logic:present name="userForm" property="searchResultsList">
                    <logic:notEqual name="userForm" property="sizeOfList" value="0">
                    	<table width="95%" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#F5F5F5"
                    	       style="border-left: 1 solid #316DB0; border-right: 1 solid #316DB0; border-top: 1 solid #316DB0; border-bottom: 4 solid #316DB0" valign="top">
				            <tr>
				                <td>
				                    <table width="95%" border="0" align="center" cellpadding="2" cellspacing="0" class="DotsTable">
				                        <tr>
					                        <td>
					                        	&nbsp;
					                        	<logic:equal name="userForm" property="isSingleSelect" value="true">
					                        		<bean:message key="quotation.lookup.selectmessage1" />
					                        	</logic:equal>
					                        	<logic:notEqual name="userForm" property="isSingleSelect" value="true">
					                        		<bean:message key="quotation.lookup.selectmessage2" />
					                        	</logic:notEqual>
					                        </td>
				                        </tr>
				                    </table>
				                </td>
				            </tr>
				            <tr>
				                <td>
				                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
				                        <tr>
											<%-- 
											Listing taglib's listing summary field tag, which displays the
											summary of records retrieved from database and those being
											displayed in this page
											 --%>
											<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
											
											<%-- 
											Listing taglib's pagelist field tag, which displays the list 
											of pages in dropdown selecting which, user will be redirected 
											to the respective page
											 --%>
											<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
										</tr>
				                    </table>
				                </td>
				            </tr>
				            
				            <tr>
				                <td>
				                    <table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#EEEEEE">
				                        <%-- Starting of row to display column headings --%>
				                        <tr>
				                            <td class="formLabelTop"><listing:sort dbfieldid="1"><bean:message key="quotation.lookup.ssoid" /></listing:sort></td>
				                            <td class="formLabelTop"><listing:sort dbfieldid="3"><bean:message key="quotation.lookup.firstname" /></listing:sort></td>
				                            <td class="formLabelTop"><listing:sort dbfieldid="4"><bean:message key="quotation.lookup.lastname" /></listing:sort></td>
				                            <td class="formLabelTop"><bean:message key="quotation.lookup.select" /></td>
				                        </tr>
				                        <%-- End of row to display column headings --%>
				                        
				                        <%-- Starting of loop to display the search results for the criteria specified --%>
				                        <logic:present name="userForm" property="searchResultsList">
					                        <logic:iterate name="userForm" property="searchResultsList" id="oUserDto" 
					                                       type="in.com.rbc.quotation.user.dto.UserDto"
					                                       indexId="idx">
					                            <% if(idx.intValue() % 2 == 0)  { %>
													<tr class="light" id='item<bean:write name="idx"/>'
				                                    	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
												<% } else { %>
													<tr class="dark" id='item<bean:write name="idx"/>'
				                                    	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
									    		<% } %>
					                                <td class="bordertd" id='userid<bean:write name="idx"/>'>
					                                	<bean:write name="oUserDto" property="userId"/>
					                                </td>
					                                <td class="bordertd" id='firstname<bean:write name="idx"/>'>
					                                	<bean:write name="oUserDto" property="firstName"/>
					                                </td>
					                                <td class="bordertd" id='lastname<bean:write name="idx"/>'>
					                                	<bean:write name="oUserDto" property="lastName"/>
					                                </td>
					                                <td class="bordertd" style="text-align:center">
					                                    <logic:equal name="userForm" property="isSingleSelect" value="true">
							                        		<input type="radio" name="selectedEmployee" id='selectedEmployee<bean:write name="idx"/>'
					                                               onClick="javascript:returnEmployeeInfo(this);" value="<bean:write name="oUserDto" property="userId"/>:<bean:write name="oUserDto" property="fullName"/>:<bean:write name="oUserDto" property="emailId"/>:<bean:write name="oUserDto" property="location"/>" />
							                        	</logic:equal>
							                        	<logic:notEqual name="userForm" property="isSingleSelect" value="true">
							                        		<input name="selectedEmployee" id="selectedEmployee<bean:write name="idx"/>" 
							                        		       type="checkbox" value="<bean:write name="oUserDto" property="userId"/>:<bean:write name="oUserDto" property="fullName"/>:<bean:write name="oUserDto" property="emailId"/>:<bean:write name="oUserDto" property="location"/>" />
							                        	</logic:notEqual>
					                                </td>
					                            </tr>
					                        </logic:iterate>
				                        </logic:present>
				                        <%-- End of loop to display the search results for the criteria specified --%>
				                        <tr><td colspan="4">&nbsp;</td></tr>
				                    </table>
				                </td>
				            </tr>
				            
				            <tr>
				                <td>
				                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0">
				                        <tr>
											<%-- 
											Listing taglib's listing summary field tag, which displays the
											summary of records retrieved from database and those being
											displayed in this page
											 --%>
											<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
											
											<%-- 
											Listing taglib's pagelist field tag, which displays the list 
											of pages in dropdown selecting which, user will be redirected 
											to the respective page
											 --%>
											<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
										</tr>
				                    </table>
				                </td>
				            </tr>
				            
				            <logic:notEqual name="userForm" property="isSingleSelect" value="true">
                        		<tr>
					                <td>
					                    <table width="100%" class="blue-light">
											<tr>
												<td align="right">
													<input type="button" name="Submit" value="Add Member(s)" class="BUTTON" 
														   onClick="javascript:returnEmployeeInfo(this);">
												</td>
											</tr>
										</table>
					                </td>
					            </tr>
                        	</logic:notEqual>
				        </table>
                    </logic:notEqual>
                    </logic:present>
            	</td>
            </tr>
        </table>
        <!-- End of table to display the search results -->
    </logic:equal>
    </div>
</html:form>

<script language="javascript">
  loadingLookup();
</script>
