<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<html:form action="/searchsalesmanager.do" method="post">
<html:hidden property="invoke" styleId="invoke"/>
<html:hidden property="salesMgrId" styleId="salesMgrId"/>
<html:hidden property="salesManagerSSO" styleId="salesManagerSSO"/>
<html:hidden property="primaryRSMSSO" styleId="primaryRSMSSO"/>
<html:hidden property="additionalRSMSSO" styleId="additionalRSMSSO"/>
<input type="hidden" name="regionIds" id="regionIds"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
<!-------------------------- main table start  -------------------------------->

	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
	    	<td height="100%" valign="top">     
				<!----------------- --------- Title & Search By Block start  --------------------------------->
	        	<br><div class="sectiontitle"><bean:message key="quotation.label.admin.sales.searchsalesmanager"/> </div>
	  	  		<br>
	        
		        <fieldset>
		        	<legend class="blueheader"><bean:message key="quotation.label.admin.searchby" /></legend>
					<table  border="0" align="center" cellpadding="0" cellspacing="2">
				    	<tr>
				        	<td class="formLabel"><bean:message key="quotation.label.admin.sales.salesmanager"/> </td>
		                	 <td>
			                	<html:text property="salesManager" styleId="salesManager" styleClass="readonly"  readonly="true" />
					            <img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'salesManagerSSO', 'salesManager', '', '', '', '');">
			                </td>
			                <td style="width:50px ">&nbsp;</td>
			                <td class="formLabel"><bean:message key="quotation.label.admin.location.region"/></td>
			                <td>
					        	<html:select property="regionId" styleId="regionId" styleClass="normal">
						            <html:option value="0">Select</html:option>
						            <logic:present name="salesManagerForm" property="regionList">
						            	<bean:define name="salesManagerForm" property="regionList"
							                	             id="regionList" type="java.util.Collection" />
						                <html:options name="salesManagerForm" collection="regionList"
									 				              property="key" labelProperty="value" />
						            </logic:present>
					            </html:select>   
				            </td>
		                	<td>&nbsp;</td>
		              	</tr>
				      	<tr>
				        	<td class="formLabel"><bean:message key="quotation.label.admin.sales.primaryrsm"/> </td>
			                <td>
			                	<html:text property="primaryRSM" styleId="primaryRSM" styleClass="readonly"  readonly="true" />
					            <img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'primaryRSMSSO', 'primaryRSM', '', '', '', '');">
			                </td>
			                <td style="width:20px ">&nbsp;</td>
			                <td class="formLabel"><bean:message key="quotation.label.admin.sales.additionalrsm"/> </td>
			                <td>
			                	<html:text property="additionalRSM" styleId="additionalRSM" styleClass="readonly"  readonly="true" />
					            <img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'additionalRSMSSO', 'additionalRSM', '', '', '', '');">
			                </td>
		              	</tr>
			        </table>
				    <br>
			          <div class="blue-light" align="right">
				          <html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetSalesManagerSearch();" />
					      <html:submit property="" styleClass="BUTTON" value="Search" />
				      </div>
				</fieldset>
				<br>
				 <!-------------------------- Title & Search By Block End  --------------------------------->
 	             <!-------------------------- Search Results Block start  --------------------------------->
	        	<div class="sectiontitle"><bean:message key="quotation.search.searchresults" /> </div><br>
	            	<br>
		      	<%-- 
				Listing taglib's hidden fields tag that includes the necessary hidden fields
				related to pagination, at runtime
			   	--%>
			 	<listing:hiddenfields />
			  	<%-- Displaying 'No Results Found' message if size of list is zero --%>
			 	<logic:equal name="sizeOfList" value="0">
					<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
						<tr class="other" align="center">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
			 	</logic:equal>
			 	<logic:notEqual name="sizeOfList" value="0">
					<logic:notPresent name="oSalesManagerDto" property="searchResultsList" scope="request">
						<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
							<tr align="center" class="other">
									<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
							</tr>
						</table>
				 	</logic:notPresent>
				 	<logic:present name="oSalesManagerDto" property="searchResultsList" scope="request">
					 	<logic:empty name="oSalesManagerDto" property="searchResultsList">
						 	<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
								<tr align="center" class="other">
									<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
								</tr>
							</table>
					 	</logic:empty>
				 		<logic:notEmpty name="oSalesManagerDto" property="searchResultsList">
						 	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
						       	<tr>
									<%-- 
									Listing taglib's listingsummary field tag, which displays the
									summary of records retrieved from database and those being
									displayed in this page
									 --%>
									<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
												
									<%-- 
									Listing taglib's pagelist field tag, which displays the list 
									of pages in dropdown selecting which, user will be redirected 
									to the respective page
									 --%>
									<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
								</tr>
						  	</table>	
				  			<br>
		  					<table  cellspacing="0" cellpadding="2" width="100%" ID="table0" class="sortable" >
				    			<tr id="top"> 
							       	<td >
							       		<listing:sort dbfieldid="1" >
											<bean:message key="quotation.label.admin.sales.salesmanager"/>
										</listing:sort>	
							        </td>   
							        <td  >
							        	<listing:sort dbfieldid="2" >
											<bean:message key="quotation.label.admin.location.region"/>
										</listing:sort>	
							        </td>
							        <td >
							        	<listing:sort dbfieldid="3" >
											<bean:message key="quotation.label.admin.sales.primaryrsm"/>
										</listing:sort>	
									</td>
									<td >
							        	<listing:sort dbfieldid="4" >
											<bean:message key="quotation.label.admin.sales.additionalrsm"/>
										</listing:sort>	
									</td>
									<td >
							        	<listing:sort dbfieldid="5" >
											<bean:message key="quotation.label.admin.sales.smcontactno"/>
										</listing:sort>	
									</td>
									
							        <td  style="width:5% " align="center"><bean:message key="quotation.search.edit"/></td>
							        <td  style="width:5% " ><bean:message key="quotation.search.delete"/></td>
				        		</tr>
				        		<% String sTextStyleClass = "textnoborder-light"; %>
				        		<logic:iterate name="oSalesManagerDto" property="searchResultsList"
					         		id="salesManagerDetails" indexId="idx"
					         		type="in.com.rbc.quotation.admin.dto.SalesManagerDto">
						        <% if(idx.intValue() % 2 == 0) { %>
									<tr class="light" id='item<bean:write name="idx"/>'" 	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
									<% sTextStyleClass = "textnoborder-light"; %>
								<% } else { %>
									<tr class="dark" id='item<bean:write name="idx"/>' 	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
									<% sTextStyleClass = "textnoborder-dark"; %>
						   		<% } %>	
					   				<%int id1= idx.intValue();
					   						id1=(int)id1+1;%>
						   		  		<td class="bordertd"><bean:write name="salesManagerDetails" property="salesManager"/>&nbsp;</td>		  
								  		<td ><bean:write name="salesManagerDetails" property="regionName"/>&nbsp;</td>
								  		<td ><bean:write name="salesManagerDetails" property="primaryRSM"/>&nbsp;</td>		  
								  		<td ><bean:write name="salesManagerDetails" property="additionalRSM"/>&nbsp;</td>	
								  		<td ><bean:write name="salesManagerDetails" property="smContactNo"/>&nbsp;</td>	
							          	<td ><div align="center"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " onClick="javascript:editSalesManager('<bean:write name="salesManagerDetails" property="salesMgrId"/>')"></div></td>
							          	<td align="center"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteSalesManager('<bean:write name="salesManagerDetails" property="salesManagerSSO"/>','<bean:write name="salesManagerDetails" property="salesManager"/>','<bean:write name="salesManagerDetails" property="regionName"/>')"></td>
					        		</tr>
				        		</logic:iterate>
				      		</table>
					
			      <!-------------------------------------- search results block end --------------------------->
					      	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light"  >
								<tr>
									<%-- 
									Listing taglib's listingsummary field tag, which displays the
									summary of records retrieved from database and those being
									displayed in this page
									 --%>
									<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
												
									<%-- 
									Listing taglib's pagelist field tag, which displays the list 
									of pages in dropdown selecting which, user will be redirected 
									to the respective page
									 --%>
									<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
								</tr>
						  	</table>	
					  		<BR>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="blue-light">
				        		<tr>
					          		<td>
					          			<html:button property="" value="Add New Sales Manager" styleClass="BUTTON" style="width:150px " onclick="javascript:addNewSalesManager();"/>          	
					          		</td>
				          			<td align="right">
					          			<html:button property="" value="Print" styleClass="BUTTON" onclick="javascript:printSalesManagerSearchResult();"/>
							          	<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportSalesManagerSearchResult();"/>
				          			</td>
				        		</tr>
				      		</table>
						</logic:notEmpty>
					</logic:present>
				</logic:notEqual>
		  		<br>
	            <!-------------------------------------- search results block end --------------------------->
		        <!--------------------------------------buttons block start --------------------------->
		       <logic:equal name="sizeOfList" value="0">
				  	<table width="100%" class="blue-light">
				      <tr>
				        <td>
				        	<html:button property="" value="Add New Sales Manager" styleClass="BUTTON" style="width:150px " onclick="javascript:addNewSalesManager();"/>          	
				        </td>
				       </tr>
				       </table>
				 </logic:equal>
	  	 <!--------------------------------------buttons block End --------------------------->
			</td>
		</tr>
	</table>
<!-------------------------- main table start  --------------------------------->
</html:form>