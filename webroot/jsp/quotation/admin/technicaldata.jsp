<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<html:form action="technicaldata.do" method="POST" enctype="multipart/form-data">
<html:hidden property="invoke"/>
<html:hidden property="SERIAL_ID" styleId="SERIAL_ID"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><form id="technicalDataForm" name="technicalDataForm" method="post" action="">
      <br>
	    <!-------------------------- Title & Search By Block start  --------------------------------->
      <div class="sectiontitle"><bean:message key="quotation.label.admin.technical.data"/> </div>
     <br>
     
     <fieldset>
        <legend class="blueheader"><bean:message key="quotation.label.admin.searchby"/>  </legend>
        <table  border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td class="formLabel" style="width: 137px;"><bean:message key="quotation.label.admin.field.manufacturing.location"/></td>
            <td ><html:text property="MANUFACTURING_LOCATION" styleClass="normal" maxlength="100" /> </td>
            <td  style="width:20px ">&nbsp;</td>
          </tr>
        </table>
        <br>
	        <div class="blue-light" align="right">
	          <html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetManufacturingSearch();" />
		      <html:button property="" styleClass="BUTTON" value="Search" onclick="javascript:searchTechnicalData();" />
		      <html:file name="technicalDataForm" property="technicalDataFile"/>
		      <html:button property="" value="Bulk Upload" styleClass="button" onclick="vaidateTechnicalDataBulkLoad();"/>
	        </div>
        </fieldset>
        
        <div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			      <br>
			      <%-- 
					Listing taglib's hidden fields tag that includes the necessary hidden fields
					related to pagination, at runtime
				   --%>
				 <listing:hiddenfields />
			  	 <%-- Displaying 'No Results Found' message if size of list is zero --%>
			
				 <logic:equal name="sizeOfList" value="0">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr class="other" align="center">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:equal>
				 
				 <logic:notEqual name="sizeOfList" value="0">
				 <logic:notPresent name="oTechnicalDataDto" property="searchResultsList" scope="request">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
								<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:notPresent>
				 <logic:present name="oTechnicalDataDto" property="searchResultsList" scope="request">
				 <logic:empty name="oTechnicalDataDto" property="searchResultsList">
				 	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:empty>
				 <logic:notEmpty name="oTechnicalDataDto" property="searchResultsList">
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
			       <tr>
						<%-- 
						Listing taglib's listingsummary field tag, which displays the
						summary of records retrieved from database and those being
						displayed in this page
						 --%>
						<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
									
						<%-- 
						Listing taglib's pagelist field tag, which displays the list 
						of pages in dropdown selecting which, user will be redirected 
						to the respective page
						 --%>
						<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
					</tr>
				  </table>	
				  <br>
     <!-------------------------------------- search results block end --------------------------->
	        <!--------------------------------------buttons block start --------------------------->

       			<table  cellspacing="0" cellpadding="2" width="100%" ID="table0" class="sortable" >
				   		
				    	<tr id="top"> 
					       	<td class="bordertd"><listing:sort dbfieldid="1" ><bean:message key="quotation.label.admin.field.manufacturing.location"/></listing:sort></td>   
					        <td class="bordertd"><listing:sort dbfieldid="2" ><bean:message key="quotation.label.admin.field.product.group"/></listing:sort></td>
					        <td class="bordertd"><listing:sort dbfieldid="3" ><bean:message key="quotation.label.admin.field.product.line"/></listing:sort></td>
							<td class="bordertd" style="width:20% " align="center"><bean:message key="quotation.label.admin.field.Standard"/></td>						
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.frame.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.number.of.poles"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.mounting.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.tB.position"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.model.no"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.voltage.v"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.power.rating.hp"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.frequency.hz"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.current.a"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.speed.max"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.power.factor"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.efficiency"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.electrical.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.ip.code"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.insulation.class"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.service.factor"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.duty"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.number.of.phases"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.max.ambient.c"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.de.bearing.size"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.de.bearing.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.ode.bearing.size"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.ode.bearing.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.enclosure.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.motor.orientation"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.frame.material"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.frame.length"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.rotation"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.number.of.speeds"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.shaft.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.shaft.diameter.mm"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.shaft.extension.mm"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.outline.dwg"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.connection.drawing.no"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.overall.length.mm"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.starting.type"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.thru.bolts.extension"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.type.of.overload.protection"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.ce"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.csa"/></td>
							<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.ul"/></td>
							
					        <td  style="width:5% " align="center"><bean:message key="quotation.search.edit"/></td>
					        <td  style="width:5% " ><bean:message key="quotation.search.delete"/></td>
				        </tr>
				        <% String sTextStyleClass = "textnoborder-light"; %>
				        <logic:iterate name="oTechnicalDataDto" property="searchResultsList"
					         id="technicalDataForm" indexId="idx"
					         type="in.com.rbc.quotation.admin.dto.TechnicalDataDto">
					   		<% int id1= idx.intValue();
					   		       id1=(int)id1+1; %>
					        <% if(idx.intValue() % 2 == 0) { %>
					        	<% sTextStyleClass = "textnoborder-light"; %>
								<tr class="light" id='item<bean:write name="idx"/>'" onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
							<% } else { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
					   		<% } %>	
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="MANUFACTURING_LOCATION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="PRODUCT_GROUP"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="PRODUCT_LINE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="STANDARD"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="POWER_RATING_KW"/></td>
						   		  
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="FRAME_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="NUMBER_OF_POLES"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="MOUNTING_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="TB_POSITION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="MODEL_NO"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="VOLTAGE_V"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="POWER_RATING_HP"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="FREQUENCY_HZ"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="CURRENT_A"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="SPEED_MAX"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="POWER_FACTOR"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="EFFICIENCY"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="ELECTRICAL_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="IP_CODE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="INSULATION_CLASS"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="SERVICE_FACTOR"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="DUTY"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="NUMBER_OF_PHASES"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="MAX_AMBIENT_C"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="DE_BEARING_SIZE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="DE_BEARING_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="ODE_BEARING_SIZE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="ODE_BEARING_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="ENCLOSURE_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="MOTOR_ORIENTATION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="FRAME_MATERIAL"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="FRAME_LENGTH"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="ROTATION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="NUMBER_OF_SPEEDS"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="SHAFT_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="SHAFT_DIAMETER_MM"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="SHAFT_EXTENSION_MM"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="OUTLINE_DWG_NO"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="CONNECTION_DRAWING_NO"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="OVERALL_LENGTH_MM"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="STARTING_TYPE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="THRU_BOLTS_EXTENSION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="TYPE_OF_OVERLOAD_PROTECTION"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="CE"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="CSA"/></td>
						   		  <td class="bordertd"><bean:write name="technicalDataForm" property="UL"/></td>
						   		  
						          <td ><div align="center"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " onClick="javascript:editTechnicalData('<bean:write name="technicalDataForm" property="SERIAL_ID"/>')"></div></td>
						          <td align="center"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteTechnicalData('<bean:write name="technicalDataForm" property="SERIAL_ID"/>', '<bean:write name="technicalDataForm" property="MANUFACTURING_LOCATION"/>')"></td>
					        </tr>
				        </logic:iterate>
				      </table>
					
			      <!-------------------------------------- search results block end --------------------------->
				      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="blue-light"  >
						 <tr>
							<%-- 
							Listing taglib's listingsummary field tag, which displays the
							summary of records retrieved from database and those being
							displayed in this page
							 --%>
							<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										
							<%-- 
							Listing taglib's pagelist field tag, which displays the list 
							of pages in dropdown selecting which, user will be redirected 
							to the respective page
							 --%>
							<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
						</tr>
					  </table>	
					  <BR>
					</logic:notEmpty>
				</logic:present>
			</logic:notEqual>
				 
				 <table width="100%" class="blue-light" cellpadding="0" cellspacing="0" border="0">
				        <tr>
				          <td>
				          	<html:button property="" value="Add New" styleClass="BUTTON" onclick="javascript:addNewTechnicalData();"/>          	
				          </td>
				          <td align="right">
				          	<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportLocationResult();"/>
				          
				          </td>
				        </tr>
				      </table>
     
     </td>
  </tr>
</table>


</html:form>