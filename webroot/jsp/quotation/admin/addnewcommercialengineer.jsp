<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<html:form action="/searchcommercialengineer.do" method="post" onsubmit="return validateCommercialEngineerValue()">
<html:hidden property="invoke" value="updateCommercialEngineerValue"/>
<html:hidden property="operation" styleId="operation"/>
<html:hidden property="designEngineerId" styleId="designEngineerId"/>
<html:hidden property="designEngineerSSO" styleId="designEngineerSSO"/>

<input type="hidden" name="existingDesignEngineerSSO" id="existingDesignEngineerSSO" value="<bean:write name="commercialEngineerForm" property="designEngineerSSO"/>">
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><br>
        <div class="sectiontitle">
        	<logic:equal name="commercialEngineerForm" property="operation" value="update">
          		<bean:message key="quotation.label.admin.updatecommercialengineer"/>
          	</logic:equal>
          	<logic:notEqual name="commercialEngineerForm"	property="operation" value="update">
		  		<bean:message key="quotation.label.admin.addnewcommercialengineer"/>
            </logic:notEqual> </div>
	    <br>
		<div class="note" align="right" style="width:100%">
        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
		</div>
		<fieldset>
          <table  align="center" >
            <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.commercialengineer"/></td>
            	<td>
            		<html:text property="designEngineerName" styleId="designEngineerName" styleClass="readonly" readonly="true" /> 
            		<img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'designEngineerSSO', 'designEngineerName', '', '', '', '');">
            	</td>
		    </tr>
          </table>
		    <br>
          <div class="blue-light" align="right">
            <logic:equal name="commercialEngineerForm" property="operation" value="update">
            <html:reset property="" styleClass="BUTTON" value="Reset" />
          		<html:submit property=""	value="Submit" styleClass="BUTTON"/>
          	</logic:equal>
          	<logic:notEqual name="commercialEngineerForm"	property="operation" value="update">
		  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearCommercialEngineerForm();" />
            	<html:submit value="Add" styleClass="BUTTON" />
            </logic:notEqual>
         </div>
	    </fieldset>
	</td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>