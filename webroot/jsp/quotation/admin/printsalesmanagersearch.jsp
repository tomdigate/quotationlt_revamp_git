<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
     <logic:present name="alAdminSearchResultsList" scope="request">
	 <logic:empty name="alAdminSearchResultsList">
	 	<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
			<tr class="other">
				<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
			</tr>
		</table>
	 </logic:empty>
	 <logic:notEmpty name="alAdminSearchResultsList">	 
      <!-------------------------------------- search results block start --------------------------->
      <table width="100%" cellpadding="2" cellspacing="0" border=1>
        <tr id="top">         	
	       	<td class="formLabelTop">
					<b><bean:message key="quotation.label.admin.sales.salesmanager"/></b>
	        </td>   
	        <td class="formLabelTop" >
					<b><bean:message key="quotation.label.admin.location.region"/></b>
	        </td>
	        <td class="formLabelTop">
					<b><bean:message key="quotation.label.admin.sales.primaryrsm"/></b>
			</td>
			<td class="formLabelTop">
					<b><bean:message key="quotation.label.admin.sales.additionalrsm"/></b>
			</td>
			<td class="formLabelTop">
					<b><bean:message key="quotation.label.admin.sales.smcontactno"/></b>
			</td>
        </tr>
        <logic:iterate name="alAdminSearchResultsList" 
					         id="salesManagerDetails" indexId="idx"
					         type="in.com.rbc.quotation.admin.dto.SalesManagerDto">
	       	<tr>	       		
	   		   	<td class="bordertd"><bean:write name="salesManagerDetails" property="salesManager"/>&nbsp;</td>		  
		  		<td class="bordertd"><bean:write name="salesManagerDetails" property="regionName"/>&nbsp;</td>
		  		<td class="bordertd"><bean:write name="salesManagerDetails" property="primaryRSM"/>&nbsp;</td>		  
		  		<td class="bordertd"><bean:write name="salesManagerDetails" property="additionalRSM"/>&nbsp;</td>	
		  		<td class="bordertd"><bean:write name="salesManagerDetails" property="smContactNo"/>&nbsp;</td>
	        </tr>
        </logic:iterate>
      </table>
	  </logic:notEmpty>
	  </logic:present>
    </td>
  </tr>
</table>
<script type="text/javascript">
<!--
  window.print();
//-->
</script>