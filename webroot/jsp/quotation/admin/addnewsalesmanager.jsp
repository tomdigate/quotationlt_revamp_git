<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<html:form action="/searchsalesmanager.do" method="post"  onsubmit="return validateSalesManager();">
<html:hidden property="invoke" value="updateSalesManager"/>
<html:hidden property="salesMgrId" styleId="salesMgrId"/>
<html:hidden property="salesManagerSSO" styleId="salesManagerSSO"/>
<input type="hidden" name="oldsalesManagerSSO" id="oldsalesManagerSSO" value="<bean:write name="salesManagerForm" property="salesManagerSSO" />"/>
<html:hidden property="primaryRSMSSO" styleId="primaryRSMSSO"/>
<html:hidden property="additionalRSMSSO" styleId="additionalRSMSSO"/>
<html:hidden property="operation" styleId="operation"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
<!-------------------------- main table start  --------------------------------->
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
	    	<td height="100%" valign="top">      
			<br>
	      <!-------------------------- Title Block start  --------------------------------->
		    	<div class="sectiontitle">
		        	<logic:equal name="salesManagerForm" property="operation" value="update">
		          		<bean:message key="quotation.label.admin.sales.updatesalesmanager"/>
		          	</logic:equal>
		          	<logic:notEqual name="salesManagerForm"	property="operation" value="update">
				  		<bean:message key="quotation.label.admin.sales.addsalesmanager"/>
		            </logic:notEqual> 
		        </div>
		        <br>
				<div class="note" align="right" style="width:100%">
				<font color="red"><bean:message key="quotation.label.manadatory"/></font>
				</div>
	        	<fieldset>
	        		<table  align="center" >
	            		<tr>
	              			<td height="23" colspan="3" class="message">&nbsp;</td>
		      			</tr>
			            <tr>
			            	<td class="formLabel"><bean:message key="quotation.label.admin.sales.salesmanager"/> </td>
				            <td><span class="mandatory">&nbsp;</span><html:text property="salesManager" styleId="salesManager" readonly="true" /><img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'salesManagerSSO', 'salesManager', '', '', '', '');">
					   		</td>
			         	</tr>
			         	<tr>
			            	<td class="formLabel"><bean:message key="quotation.label.admin.location.region"/></td>
					       	<td><span class="mandatory">&nbsp;</span><html:select property="regionId" styleId="regionId" styleClass="normal">
								    <html:option value="0">Select</html:option>
								 	<logic:present name="salesManagerForm" property="regionList">
								        <bean:define name="salesManagerForm" property="regionList"
									    	id="regionList" type="java.util.Collection" />
								    	<html:options name="salesManagerForm" collection="regionList"
											property="key" labelProperty="value" />
									</logic:present>
								</html:select>   
						    </td>
					     </tr>
					     <tr>
			          		<td class="formLabel"><bean:message key="quotation.label.admin.sales.primaryrsm"/> </td>
							<td><span class="mandatory">&nbsp;</span><html:text property="primaryRSM" styleId="primaryRSM" readonly="true" /><img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'primaryRSMSSO', 'primaryRSM', '', '', '', '');">
					        </td>
					      	<td class="message"><bean:message key="quotation.label.admin.sales.primarysalesmgrlabel"/> </td>
					     </tr>
				         <tr>
			              	<td class="formLabel"><bean:message key="quotation.label.admin.sales.additionalrsm"/> </td>
					      	<td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="additionalRSM" styleId="additionalRSM"  readonly="true" /><img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'additionalRSMSSO', 'additionalRSM', '', '', '', '');">
					      	</td>
					      	<td class="message"><bean:message key="quotation.label.admin.sales.additionlsalesmgrlabel"/></td>
				     	</tr>
				     	<tr>
			            	<td class="formLabel"><bean:message key="quotation.label.admin.sales.smcontactno"/> </td>
				            <td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="smContactNo" styleId="smContactNo" maxlength="10" onkeypress="return isNumber(event)"/>
					   		</td>
			         	</tr>
				     	
					</table>
				    <br>
		          	<div class="blue-light" align="right">
			            <logic:equal name="salesManagerForm" property="operation" value="update">

			          		<html:submit value="Update" styleClass="BUTTON" />
			          	</logic:equal>
			          	<logic:notEqual name="salesManagerForm"	property="operation" value="update">
					  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearSalesManagerForm();" />
			            	<html:submit value="Add" styleClass="BUTTON" />
			            </logic:notEqual>
	         		</div>
		    	</fieldset>
	         </td>
	  	</tr>
	</table>
<!--------------------------------------buttons block start --------------------------->
</html:form>