<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<table width="75%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
    <html:form method="post" action="mastersdata.do"  >
    
	    <html:hidden property="invoke" value="viewSearchMastersData" />
	    <html:hidden name="MastersDataForm" property="masterId" />
	    <html:hidden name="MastersDataForm" property="master" />
	     <html:hidden name="MastersDataForm" property="engineeringId" />
	    <html:hidden property="dispatch" value="search" />
	    <input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
 		
	<br>
	
    <div class="sectiontitle">Search&nbsp;<bean:write name="MastersDataForm" property="master"/></div>
      <br>
	  	<!-------------------------- Search By Block Start --------------------------------->
      <fieldset>
        <legend class="blueheader"><bean:message key="quotation.search.searchby" /> </legend>
        <table align="center" cellpadding="2" cellspacing="3" style="width:auto">
          <tr>
            <td class="formLabel"  ><bean:write name="MastersDataForm" property="master"/></td>
            <td ><html:text property="engineeringName" maxlength="150" styleClass="normal"/></td>
          </tr>
        </table>
        <!-------------------------------------- search block end --------------------------->
		<div class="blue-light" align="right">
        <html:button property="reset"  styleClass="BUTTON" value="Clear" onclick="javascript:clearMastersDataSearch();"/>
        <html:submit property="Search" styleClass="BUTTON" value="Search"/>
      </div>
        <!--------------------------------------buttons block start --------------------------->
        </fieldset>
      
      <br>
      <!-------------------------------------- buttons Block End ------------------------->
		<!-------------------------------------- search results block start ------------------------>
	<div class="sectiontitle"><bean:message key="quotation.admin.mastersearchresults"/></div><br>
	
	<logic:notEqual name="sizeOfList" value="0">	
  	 	
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
	<tr>
	<%-- 
	Listing taglib's listingsummary field tag, which displays the
	summary of records retrieved from database and those being
	displayed in this page
	 --%>
	<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
				
	<%-- 
	Listing taglib's pagelist field tag, which displays the list 
	of pages in dropdown selecting which, user will be redirected 
	to the respective page
	 --%>
	<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
	</tr>
</table><br />

	</logic:notEqual>
<table width="100%" cellpadding="2"cellspacing="0" id="table0"  align="center">
	
	<%-- 
	Listing taglib's hidden fields tag that includes the necessary hidden fields
	related to pagination, at runtime
	 --%>
	<listing:hiddenfields />
	
  <%-- Displaying 'No Results Found' message if size of list is zero --%>
			<logic:equal name="sizeOfList" value="0">				
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
					<tr class="color3" align="center">
						<td class="other" style="text-align:center;"><bean:message key="quotation.messages.noresultsfound"/></td>
					</tr>
				</table>
			</logic:equal>

			<logic:notEqual name="sizeOfList" value="0">
			 <tr id="top">
                
                <td  align="center">
                	<listing:sort dbfieldid="1" >
	                					<bean:write name="MastersDataForm" property="master"/>				
				    </listing:sort> 
				    </td>
				    <td  align="center">
				    	<bean:message key="quotation.admin.status"/>
					</td>
				    <td  align="center">
				    	<bean:message key="quotation.admin.changestatus"/>
					</td>
					 <td  style="width:5% " align="center">&nbsp;</td>
					
					
				<td  style="width:5%;text-align:center">Edit</td>
				<td  style="width:5%;text-align:center">Delete</td>
			 </tr>
	 		 <% String sTextStyleClass = "textnoborder-light"; %>
			 <logic:iterate name="resultsList" scope="request" id="searchMasterData"
				            indexId="idx" type="in.com.rbc.quotation.admin.dto.MastersDataDto">
   				<% int id1= idx.intValue();
   					   id1=(int)id1+1; %>
				<% if(idx.intValue() % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<tr class="light" id='item<bean:write name="idx"/>'" onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_2', 'loc_<%=id1%>_3');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_2', 'loc_<%=id1%>_3', '<%=sTextStyleClass%>');">
				<% } else { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_2', 'loc_<%=id1%>_3');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_2', 'loc_<%=id1%>_3', '<%=sTextStyleClass%>');">
		   		<% } %>	
				<td class="bordertd" id="loc_<%=id1 %>_1">
	          		<bean:write name="searchMasterData" property="engineeringName"/>
				</td>
             
         	<logic:equal name="searchMasterData" property="status" value="A">
		  		<td >
		  			<input type="text" name="loc_<%=id1%>_2" id="loc_<%=id1%>_2" readonly="true" class="<%=sTextStyleClass%>"
		  			       value="<bean:message key="quotation.label.admin.location.active"/>">
		  		</td>
		  		<td style="border-right:0px;">
		  			<input type="text" name="loc_<%=id1%>_3" id="loc_<%=id1 %>_3" readonly="true" class="<%=sTextStyleClass%>-a" 
		  			       onClick="javascript:changeMasterDataStatus('loc_<%=id1%>_2', 'loc_<%=id1%>_3', '<bean:write name="searchMasterData" property="engineeringId"/>', 'working_<%=id1%>','<bean:write name="MastersDataForm" property="master"/>','<bean:write name="MastersDataForm" property="masterId"/>');"
		  			       value="<bean:message key="quotation.label.admin.location.turninactive"/>">
		  		</td>
		  	</logic:equal>
		  	<logic:equal name="searchMasterData" property="status" value="I">
		  		<td>
		  			<input type="text" name="loc_<%=id1%>_2" id="loc_<%=id1 %>_2" readonly="true" class="<%=sTextStyleClass%>"
		  			       value="<bean:message key="quotation.label.admin.location.inactive"/>">
		  		</td>
		  		<td  style="border-right:0px;">
		  			<input type="text" name="loc_<%=id1%>_3" id="loc_<%=id1 %>_3" readonly="true" class="<%=sTextStyleClass%>-a"
		  			       onClick="javascript:changeMasterDataStatus('loc_<%=id1%>_2', 'loc_<%=id1%>_3', '<bean:write name="searchMasterData" property="engineeringId"/>', 'working_<%=id1%>','<bean:write name="MastersDataForm" property="master"/>','<bean:write name="MastersDataForm" property="masterId"/>');"
		  			       value="<bean:message key="quotation.label.admin.location.turnactive"/>">
				  		</td>
			</logic:equal>
								
         	  <td >
			  		<div id="working_<%=id1%>" class="working">
						<img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand;">
			  		</div>
			  </td>
			 <!-- '<bean:write name="searchMasterData" property="engineeringId"/>' -->
				<td ><div align="center"><a href="javascript:editEngineeringData('<bean:write name="searchMasterData" property="engineeringId"/>');"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" ></a></div></td>
     			<td ><div align="center"><a href="javascript:deleteEngineeringData('<bean:write name="searchMasterData" property="engineeringId"/>','<bean:write name="searchMasterData" property="engineeringName"/>');"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" ></a></div></td>
     </tr>
	</logic:iterate>		

	</table>
	
	
			<!-------------------------------------- search results block end --------------------------->
			<table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<%-- 
	Listing taglib's listingsummary field tag, which displays the
	summary of records retrieved from database and those being
	displayed in this page
	 --%>
	<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
				
	<%-- 
	Listing taglib's pagelist field tag, which displays the list 
	of pages in dropdown selecting which, user will be redirected 
	to the respective page
	 --%>
	<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
	</tr>
</table>
			<br />
			</logic:notEqual>
<table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
				<tr>
				
					<td align="left" style="width:20% ">
						<input name="Add Master"  value="Add New <bean:write name="MastersDataForm" property="master"/>" type="button" class="BUTTON" onClick="addEngineeringData();">
					</td>
					<td>&nbsp;</td>
					<logic:notEqual name="sizeOfList" value="0">
					<td style="width:20% ">
<input name="Print" type="button" class="BUTTON"  onClick="printOrExportMasterData('print')" value="Print"><input name="Export" type="button" class="BUTTON"  onClick="printOrExportMasterData('export')" value="Export To Excel">
					</td>
					</logic:notEqual>
				</tr>
			</table>


 </html:form><br>

 </td>
</tr>
</table>

<!--main content  table end--> 
