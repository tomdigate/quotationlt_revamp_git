<!-------------------------- main table Start  --------------------------------->
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<html:form action="/searchdesignengineer.do" method="post">
<html:hidden property="invoke" value="viewDesignEngineer"/>
<html:hidden property="designEngineerId" styleId="designEngineerId"/>
<html:hidden property="designEngineerSSO" styleId="designEngineerSSO"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><form id="form1" name="form1" method="post" action="">
      <br>
	    <!-------------------------- Title & Search By Block start  --------------------------------->
      <div class="sectiontitle"><bean:message key="quotation.label.admin.design.searchdesignengineer"/> </div>
     <br>

      <fieldset>
        <legend class="blueheader"><bean:message key="quotation.label.admin.searchby"/>  </legend>
        <table  border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td class="formLabel"><bean:message key="quotation.label.admin.design.designengineer"/></td>
            <td >
            	<html:text property="designEngineerName" styleId="designEngineerName" styleClass="readonly" readonly="true" /> 
            	<img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'designEngineerSSO', 'designEngineerName', '', '', '', '');">
           </td>
            <td  style="width:20px ">&nbsp;</td>
          </tr>
        </table>
        <br>
	        <div class="blue-light" align="right">
	          <html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetDesignEngineerSearch();" />
		      <html:submit property="" styleClass="BUTTON" value="Search" />
	        </div>
        </fieldset>
		 <!-------------------------- Title & Search By Block End  --------------------------------->
		  <!-------------------------- Search Results Block start  --------------------------------->
      			<div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			      <br>
			      <%-- 
					Listing taglib's hidden fields tag that includes the necessary hidden fields
					related to pagination, at runtime
				   --%>
				 <listing:hiddenfields />
			  	 <%-- Displaying 'No Results Found' message if size of list is zero --%>
			
				 <logic:equal name="sizeOfList" value="0">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr class="other" align="center">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:equal>
				 
				 <logic:notEqual name="sizeOfList" value="0">
				 <logic:notPresent name="oDesignEngineerDto" property="searchResultsList" scope="request">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
								<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:notPresent>
				 <logic:present name="oDesignEngineerDto" property="searchResultsList" scope="request">
				 <logic:empty name="oDesignEngineerDto" property="searchResultsList">
				 	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:empty>
				 <logic:notEmpty name="oDesignEngineerDto" property="searchResultsList">
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
			       <tr>
						<%-- 
						Listing taglib's listingsummary field tag, which displays the
						summary of records retrieved from database and those being
						displayed in this page
						 --%>
						<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
									
						<%-- 
						Listing taglib's pagelist field tag, which displays the list 
						of pages in dropdown selecting which, user will be redirected 
						to the respective page
						 --%>
						<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
					</tr>
				  </table>	
				  <br>
     <!-------------------------------------- search results block end --------------------------->
	        <!--------------------------------------buttons block start --------------------------->

       			<table  cellspacing="0" cellpadding="2" width="100%" id="table0"  style="border-left:0px">
				    	<tr id="top"> 
					       	<td >
					       		<listing:sort dbfieldid="1" >
									<bean:message key="quotation.label.admin.design.designengineername"/>
								</listing:sort>	
					        </td>   
					        <td  width="5%"><bean:message key="quotation.search.edit"/></td> 
					        <td  width="5%"><bean:message key="quotation.search.delete"/></td>
				        </tr>
				        <logic:iterate name="oDesignEngineerDto" property="searchResultsList"
					         id="designEngineerDetails" indexId="idx"
					         type="in.com.rbc.quotation.admin.dto.DesignEngineerDto">
					        <% if(idx.intValue() % 2 == 0) { %>
								<tr class="light" id='item<bean:write name="idx"/>'" 	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
							<% } else { %>
								<tr class="dark" id='item<bean:write name="idx"/>' 	onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
					   		<% } %>	
						   		  <td class="bordertd" id="deName_<%=idx.intValue()%>"><bean:write name="designEngineerDetails" property="designEngineerName"/></td>		  
						           <td class="bordertd"><div align="center"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " onClick="javascript:editDesignEngineer('<bean:write name="designEngineerDetails" property="designEngineerId" />')"></div></td>
						          <td align="center"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteDesignEngineer('<bean:write name="designEngineerDetails" property="designEngineerSSO" />','deName_<%=idx.intValue()%>')"></td>
					        </tr>
				        </logic:iterate>
				      </table>
					
			      <!-------------------------------------- search results block end --------------------------->
				      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light"  >
						 <tr>
							<%-- 
							Listing taglib's listingsummary field tag, which displays the
							summary of records retrieved from database and those being
							displayed in this page
							 --%>
							<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										
							<%-- 
							Listing taglib's pagelist field tag, which displays the list 
							of pages in dropdown selecting which, user will be redirected 
							to the respective page
							 --%>
							<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
						</tr>
					  </table>	
					  <BR>
				      <table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td>
				          	<html:button property="" value="Add New Design Engineer" styleClass="BUTTON" style="width:160px;" onclick="javascript:addNewDesignEngineer();"/>          	
				          </td>
				          <td align="right">					        
					          <html:button property="" value="Print" styleClass="BUTTON" onclick="javascript:printDesginEngineerResult();"/>
	   			          	<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportDesginEngineerResult();"/>				          	
				          	
				          </td>
				        </tr>
				      </table>
					</logic:notEmpty>
				</logic:present>
			</logic:notEqual>
		  
				<logic:equal name="sizeOfList" value="0">
				  	<table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
				      <tr>
				        <td>
				        	<html:button property="" value="Add New Design Engineer" styleClass="BUTTON" onclick="javascript:addNewDesignEngineer();"/>          	
				        </td>
				       </tr>
				       </table>
				 </logic:equal>
	 <!--------------------------------------buttons block End --------------------------->
    </td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>