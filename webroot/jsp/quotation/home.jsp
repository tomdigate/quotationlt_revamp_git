<%@ include file="../common/nocache.jsp" %>
<%@ include file="../common/library.jsp" %>
<%@ taglib uri="/ajax-tag" prefix="ajax" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>

 <html:form action="home.do" method="POST">
<html:hidden property="invoke" value="setRoles"/>
<html:hidden property="assignToId"/>
<!-------------------------- Main Table Start  --------------------------------->
<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
        <td height="100%" valign="top">
            <br>
            <div class="sectiontitle">
            <bean:message key="quotation.home.welcomeheading" />&nbsp;
            <%=(String)request.getSession().getAttribute(QuotationConstants.QUOTATIONLT_LOGINUSER_FULLNAME) %>
            </div><br />
                <table style="height:100%" width="100%" border="0">
                    <tr>
                    	<%-- 
                        <td style="vertical-align:top; width:30%">
	                        <br>
                            <!-------------------------- Left Navgation Block Start  --------------------------------->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            	<logic:present name="isSalesManager" scope="session">
                            		<logic:equal name="isSalesManager" value="true">
                                 		<tr>
                                    		<td width="75"><img src="html/images/ico_create.jpg" width="61" height="61"></td>
                                    		<td>
                                        		<span class="blueheader"><a class="blueheader" href="javascript:newplaceAnEnquiry();"> <bean:message key="quotation.home.placeanenquiry" /> </a></span>
                                        		<br /><bean:message key="quotation.home.placeanenquiry.msg" />
                                    		</td>
                                 		</tr>
                                 	</logic:equal>
                                </logic:present>
                                <tr>
                                    <td height="1" colspan="2" bgcolor="#E5E5E5"><img src="html/images/spacer.gif" width="1" height="1" /></td>
                                </tr>
                                <tr>
                                    <td><img src="html/images/ico_search.jpg" width="61" height="61" /></td>
                                    <td>
                                        <p><span class="blueheader"><a class="blueheader" href="javascript:searchMyEnquiry();"><bean:message key="quotation.home.viewmyenquires" /></a></span>
                                        <br /><bean:message key="quotation.home.viewmyenquires.msg" /></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="2" bgcolor="#E5E5E5"><img src="html/images/spacer.gif" width="1" height="1" /></td>
                                </tr>
                                <tr>
                                    <td><img src="html/images/ico_help.jpg" width="61" height="61" /></td>
                                    <td>
                                        <span class="blueheader"><a class="blueheader" href="#"><bean:message key="quotation.home.helpandsupport" /></a></span>
                                        <br /><bean:message key="quotation.home.helpandsupport.msg" />
                                    </td>
                                </tr>
                            </table>
                            <!-------------------------- Left Navgation Block End  --------------------------------->
                        </td>
                        --%>
                        <td style="vertical-align:top">
                            <!-------------------------- Right Navgation Block Start  --------------------------------->
                            <table align="right" width="98%" cellpadding="0" cellspacing="0">
                         		<logic:present name="arlMyRecentEnquires" scope="request">
                         			<tr>
                            			<td>
			                                <fieldset>
			                                    <legend class="blueheader">
			                                    	<bean:message key="quotation.home.heading.myrecentenquires1" />&nbsp;<%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DASHBOARD_RECENTENQUIRYSCOUNT)%>&nbsp;
			                                    	<bean:message key="quotation.home.heading.myrecentenquires2" />
													
			                                    </legend>
			                                    <table align="right" width="100%" cellpadding="1" cellspacing="0">
		                                        	<tr>
			                                            <td style="width:20px">&nbsp;</td>
			                                            <td class="formLabelTophome" style="width:15%"><bean:message key="quotation.home.colheading.rfqnumber" /></td>
			                                            <td class="formLabelTophome" style="width:20%"><bean:message key="quotation.home.colheading.cusname" /></td>
			                                            <td class="formLabelTophome" style="width:10%"><bean:message key="quotation.home.colheading.projname" /></td>
			                              			    <td class="formLabelTophome" style="width:20%"><bean:message key="quotation.create.label.createdby" /></td>
			                                            <td class="formLabelTophome" style="width:9%"><bean:message key="quotation.home.colheading.createddate" /></td>
			                                            <td class="formLabelTophome" style="width:10%"><bean:message key="quotation.home.colheading.status" /></td>
			                                       	    <td class="formLabelTophome" ><bean:message key="quotation.home.colheading.assignto" /></td>
			                                       
			                                        </tr>
		                                        	<logic:iterate name="arlMyRecentEnquires" id="oWorkflowDto" scope="request">
			                                        	<tr>
				                                            <td class="bullet" style="width:20px">&nbsp;</td>
				                                            <td style="width:15%">
																<a href="javaScript:viewEnquiryDetails('<bean:write name="oWorkflowDto" property="enquiryId" />','<bean:write name="oWorkflowDto" property="statusId" />','<bean:write name="oWorkflowDto" property="isNewEnquiry" />');"><bean:write name="oWorkflowDto" property="enquiryNo" /></a>
				                                            </td>
				                                            <td style="width:20%"><bean:write name="oWorkflowDto" property="customerName" /></td>
				                                            <td style="width:10%"><bean:write name="oWorkflowDto" property="projName" /></td>
				                                            <td style="width:20%"><bean:write name="oWorkflowDto" property="createdByName" /></td>
				                                            <td style="width:9%"><bean:write name="oWorkflowDto" property="createdDate" /></td>
				                                            <td style="width:10%"><bean:write name="oWorkflowDto" property="status" /></td>
				                                            <td ><bean:write name="oWorkflowDto" property="assignedTo" /></td>
				                                        </tr>
			                                        </logic:iterate>
			                                    </table>
			                                </fieldset><br>
	                            		</td>
	                            	</tr>
		                        </logic:present>
		                        <!-- CE Dash Board Starts From Here  -->
	                            	<tr>
	                            		<td>
			                                <fieldset>
			                                    <legend class="blueheader"><bean:message key="quotation.home.heading.myawaitingapprovalenquires" /></legend>
			                                    <table align="right" width="100%" cellpadding="1" cellspacing="0">
			                                        <tr>
			                                            <td style="width:20px">&nbsp;</td>
			                                            <td class="formLabelTophome" style="width:15%"><bean:message key="quotation.home.colheading.rfqnumber" /></td>
			                                            <td class="formLabelTophome" style="width:20%"><bean:message key="quotation.home.colheading.cusname" /></td>
			                                            <td class="formLabelTophome" style="width:10%"><bean:message key="quotation.home.colheading.projname" /></td>
			                                            <td class="formLabelTophome" style="width:20%"><bean:message key="quotation.create.label.createdby" /></td>
			                                            <td class="formLabelTophome" style="width:9%"><bean:message key="quotation.home.colheading.createddate" /></td>
			                                            <td class="formLabelTophome" style="width:10%"><bean:message key="quotation.home.colheading.status" /></td>
			                                            <td class="formLabelTophome" ><bean:message key="quotation.home.colheading.assignto" /></td>
			                                        </tr>
			                                        <logic:present name="arlMyAwaitingApprovalEnquires" scope="request">
		                                        	<logic:iterate name="arlMyAwaitingApprovalEnquires" id="oWorkflowDto" scope="request">
			                                        	<tr>
				                                            <td class="bullet" style="width:20px">&nbsp;</td>
				                                            <td style="width:15%">
				                                            	<logic:greaterEqual name="oWorkflowDto" property="statusId" value="9">
				                                            		<logic:equal name="oWorkflowDto" property="statusId" value="9">
				                                            			<a href="javaScript:viewIndentPageDetails('<bean:write name="oWorkflowDto" property="enquiryId" />','<bean:write name="oWorkflowDto" property="statusId" />','Won');"><bean:write name="oWorkflowDto" property="enquiryNo" /></a>
				                                           			</logic:equal>
					                                           		<logic:greaterThan name="oWorkflowDto" property="statusId" value="9">
																		<a href="javaScript:viewEnquiryDetails('<bean:write name="oWorkflowDto" property="enquiryId" />','<bean:write name="oWorkflowDto" property="statusId" />','<bean:write name="oWorkflowDto" property="isNewEnquiry" />');"><bean:write name="oWorkflowDto" property="enquiryNo" /></a>
					                                           		</logic:greaterThan>
				                                            	</logic:greaterEqual>
				                                          		<logic:lessThan name="oWorkflowDto" property="statusId" value="9">
																	<a href="javaScript:viewEnquiryDetails('<bean:write name="oWorkflowDto" property="enquiryId" />','<bean:write name="oWorkflowDto" property="statusId" />','<bean:write name="oWorkflowDto" property="isNewEnquiry" />');"><bean:write name="oWorkflowDto" property="enquiryNo" /></a>
				                                           		</logic:lessThan>
				                                            </td>
				                                            <td style="width:20%"><bean:write name="oWorkflowDto" property="customerName" /></td>
				                                            <td style="width:10%"><bean:write name="oWorkflowDto" property="projName" /></td>
				                                            <td style="width:20%"><bean:write name="oWorkflowDto" property="createdByName" /></td>
				                                            <td style="width:9%"><bean:write name="oWorkflowDto" property="createdDate" /></td>
				                                            <td style="width:10%"><bean:write name="oWorkflowDto" property="status" /></td>
				                                        	<td ><bean:write name="oWorkflowDto" property="assignedTo" /></td>
				                                        </tr>
			                                        </logic:iterate>
			                                        </logic:present>
			                                    </table>
			                                </fieldset><br>
	                            		</td>
	                            	</tr>
		                        
                            	<logic:present name="arlMyRecentReleasedEnquires" scope="request">
	                            	<tr>
	                            		<td>
			                                <fieldset>
			                                    <legend class="blueheader">
			                                    	<bean:message key="quotation.home.heading.myrecentreleasedenquires1" />&nbsp;<%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DASHBOARD_RELEASEDENQUIRYSCOUNT)%>&nbsp;
			                                    	<bean:message key="quotation.home.heading.myrecentreleasedenquires2" />
													
			                                    </legend>
			                                    <table align="right" width="100%" cellpadding="1" cellspacing="0">
			                                        <tr>
			                                            <td style="width:20px">&nbsp;</td>
			                                            <td class="formLabelTophome" style="width:15%"><bean:message key="quotation.home.colheading.rfqnumber" /></td> 
			                                           	<td class="formLabelTophome" style="width:20%"><bean:message key="quotation.home.colheading.cusname" /></td>
			                                            <td class="formLabelTophome" style="width:10%"><bean:message key="quotation.home.colheading.projname" /></td>
			                                            <td class="formLabelTophome" style="width:20%"><bean:message key="quotation.create.label.createdby" /></td>
			                                            <td class="formLabelTophome" style="width:9%"><bean:message key="quotation.home.colheading.createddate" /></td>
			                                            <td class="formLabelTophome"><bean:message key="quotation.home.colheading.status" /></td>
			                                        </tr>
		                                        	<logic:iterate name="arlMyRecentReleasedEnquires" id="oWorkflowDto" scope="request">
			                                        	<tr>
				                                            <td class="bullet" style="width:20px">&nbsp;</td>
				                                            <td style="width:15%">
																<a href="javaScript:viewEnquiryDetails('<bean:write name="oWorkflowDto" property="enquiryId" />','<bean:write name="oWorkflowDto" property="statusId" />','<bean:write name="oWorkflowDto" property="isNewEnquiry" />');"><bean:write name="oWorkflowDto" property="enquiryNo" /></a>
				                                            </td>
				                                            <td style="width:20%"><bean:write name="oWorkflowDto" property="customerName" /></td>
				                                            <td style="width:10%"><bean:write name="oWorkflowDto" property="projName" /></td>
														    <td style="width:20%"><bean:write name="oWorkflowDto" property="createdByName" /></td>
				                                            <td style="width:9%"><bean:write name="oWorkflowDto" property="createdDate" /></td>
				                                            <td><bean:write name="oWorkflowDto" property="status" /></td>
				                                        </tr>
			                                        </logic:iterate>
			                                    </table>
			                                </fieldset>			                                
	                            		</td>
	                            	</tr>
		                        </logic:present>
                            </table>
                            <!-------------------------- Right Navgation Block End  --------------------------------->
                        </td>
                    </tr>
                </table>
                
                <%	if( QuotationConstants.QUOTATION_YES.equalsIgnoreCase(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_SHOW_SWITCH_USER))) {	%>
                
                	<fieldset>
						<table border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
						    	<td> Set User</td>
							 	<td>: &nbsp;
							 		<input type="text" name="accesstoId" id="accesstoId" >
							 		<input type="text" name="accesstoName" id="accesstoName" readonly="readonly">
							 		<img src="html/images/lookup.gif" onclick="javascript:searchLookup('accesstoName','accesstoId','');"  class="hand" width="20" height="18" border="0" align="absmiddle">
							 		<input type="button" value="Submit" onclick="javascript:setUser();" >
								</td>
						   	</tr>
						</table>
					</fieldset>
                
                <%	}	%>
        </td>
    </tr>
</table>
</html:form>
<!-------------------------- Main Table End  --------------------------------->