<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 
<table width="95%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td>
			<div class="sectiontitle">
				<logic:present name="pageHeadingType" scope="request">
					<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE %>">
						<bean:message key="quotation.viewenquiry.estimate.label.heading"/> 		
					</logic:equal>
					<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM %>">
						<bean:message key="quotation.refactor.label.heading"/> 		
					</logic:equal>
					<logic:equal name="pageHeadingType" value="view">
						<bean:message key="quotation.viewenquiry.label.heading"/> 		
					</logic:equal>
				</logic:present>
			</div>
			
			<logic:present name="displayPaging" scope="request">
				<table align="right">
					<tr>
						<td>
							<fieldset  style="width:100%">
								<table cellpadding="0" cellspacing="2" border="0">
									<tr>
										<td style="text-align: center">
											<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch"> Return to search<img src="html/images/back.gif" border="0" align="absmiddle" /></a>
										</td>
									</tr>
									<tr class="other" align="right">
										<td align="right"><logic:present name="previousId" scope="request">
											<bean:define name="previousId" id="previousId" scope="request" />
											<bean:define name="previousIndex" id="previousIndex" scope="request" />
											<bean:define name="previousStatus" id="previousStatus" scope="request" />
											<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>">
												<img src="html/images/PrevPage.gif" alt="" border="0" align="absmiddle" />
											</a>
											</logic:present> <logic:present name="displayMessage" scope="request">
												<bean:write name="displayMessage" scope="request" />
											</logic:present> <logic:present name="nextId" scope="request">
											<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>">
												<img src="html/images/NextPage.gif" alt="Next Request" border="0" align="absmiddle" />
											</a>
											</logic:present>
										</td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
			</logic:present>
			
		</td>
	</tr>
	
	<tr>
		<td height="100%" valign="top">
			<script language="javascript" src="html/js/tooltip.js"></script>
			
			<logic:present name="newEnquiryDto" scope="request">
			
				<table width="1170" border="0" cellpadding="0" cellspacing="3">
					<tr>
						<td rowspan="2" >
						<table width="100%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable" align="center">
				        	<tr>
				             	<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
				             	<td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newEnquiryDto" property="enquiryNumber"/><br>
				            	<bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
				           	</tr>
				           	<tr>
				             	<td class="other">
				             	<table width="100%">
					                 <tr>
					                   <td height="16" class="section-head" align="center">
					                   	<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="newEnquiryDto" property="statusName"/> </td>
					                 </tr>
				             	</table>
				             	</td>
				           </tr>
				       </table>
				       </td>
				       <logic:lessThan name="newEnquiryDto" property="statusId" value="6"> 
					       <td colspan="2" align="right" >
						   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
							  <tr>
								<td  class="other" align="right">
								<bean:message key="quotation.attachments.message"/>
								</td>
								<td>
								<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"></a>
								</td>
							  </tr>
							</table>
				    	   </td>
				       </logic:lessThan>
				       <logic:present name="attachmentList" scope="request">
				       <logic:notEmpty name="attachmentList">
				              <logic:equal name="newEnquiryDto" property="statusId" value="6"> 
					       <td colspan="2" align="right" >
						   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
							  <tr>
								<td  class="other" align="right"  colspan="2">
								<bean:message key="quotation.attachments.enquirydocument"/>
								</td>
							  </tr>
							</table>
				    	   </td>
				       </logic:equal>
				       </logic:notEmpty>
				       </logic:present>
					</tr>
					
					<tr>
						<td  style="width:45% ">
					       <logic:present name="attachmentList" scope="request">
					       <logic:notEmpty name="attachmentList">
					       <fieldset>
					         <table width="100%" class="other">
					           <logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
									<tr><td>
									<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
										<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
									</a>
									</td></tr>					
							   </logic:iterate>
					         </table>
					       </fieldset>
					       </logic:notEmpty>
					       </logic:present>
						</td>
					</tr>
					
					<tr>
						<td colspan="2" align="right">
							<!-- DM Uploading Files  --> 
							<logic:lessThan name="newEnquiryDto" property="statusId" value="6">
								<table>
									<tr>
										<td class="other" align="right"><bean:message key="quotation.attachments.dmuploadedmessage" /></td>
										<td class="other" align="right">
											<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
												<img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right">
											</a>
										</td>
									</tr>
								</table>
							</logic:lessThan> 
							<logic:present name="dmattachmentList" scope="request">
								<logic:notEmpty name="dmattachmentList">
									<logic:equal name="newEnquiryDto" property="statusId" value="6">
										<table>
											<tr>
												<td class="other" align="right" colspan="2"><bean:message key="quotation.attachments.designdocument" /></td>
											</tr>
										</table>
									</logic:equal>
								</logic:notEmpty>
							</logic:present>
						</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<td colspan="2" align="right" style="width: 45%"><logic:present
								name="dmattachmentList" scope="request">
								<logic:notEmpty name="dmattachmentList">
									<fieldset>
										<table width="100%" class="other">
											<logic:iterate name="dmattachmentList" id="attachmentData"
												indexId="idx"
												type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<tr>
													<td><a
														href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
															<bean:write name="attachmentData" property="value" />.<bean:write
																name="attachmentData" property="value2" />
													</a></td>
												</tr>
											</logic:iterate>
										</table>
									</fieldset>
								</logic:notEmpty>
							</logic:present>
						</td>
					</tr>
				</table>
				<br>
				
				<!----------------------- Employee Information block start -------------------------------------->
				
				<fieldset>
					<legend class="blueheader"><bean:message key="quotation.create.label.requestinfo"/></legend>
					<table width="1130">
						<tr>
			  				<td valign="top" style="width:32%;" >
								<table style="width:100%;">
								 	<tr>	
								 		<td colspan="2"> <label class="blue-light" style="font-weight:bold; width:100%;"> Enquiry Information </label>
									   	</td>				    
								  	</tr>
								  	<tr>
								  		<th width="50%" class="label-text" > <bean:message key="quotation.create.label.customerenqreference"/> </th>
								  		<td width="50%" class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="customerEnqReference"/></td>
								  	</tr>
								  	<tr>
					    				<th class="label-text" > <bean:message key="quotation.create.label.customer"/> </th>
					    				<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="customerName"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > <bean:message key="quotation.label.admin.customer.location"/> </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="location"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > <bean:message key="quotation.label.concerned.person"/> </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="concernedPerson"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Project Name </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="projectName"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Is Marathon approved </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="is_marathon_approved"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Consultant </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="consultantName"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > End user </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="endUser"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > End User Industry </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="endUserIndustry"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Location of Installation </th>
					    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="locationOfInstallation"/></td>
					    			</tr>
					    		</table>
							</td>
							<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
							<td width="298" align="left" valign="top" style="width:300px;" >
								<table style="width:100%;">
								 	<tr>	
								 		<td colspan="2">			  
								  		<label class="blue-light" style="font-weight:bold; width:100%;"> 
								    		Enquiry Waitage
								    	</label>
								    	</td>
								  	</tr>
								  	<tr>
					    				<th width="50%"  class="label-text" > Enquiry Type <span class="mandatory">&nbsp;</span> </th>
					    				<td width="50%"  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="enquiryType"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Winning Chance <span class="mandatory">&nbsp;</span> </th>
					    				<logic:equal name="newenquiryForm" property="statusId" value="6">
					    					<td>
					    						<bean:define name="newEnquiryDto" property="alWinChanceList" id="alWinChanceList" type="java.util.Collection" />
									     		<html:select style="width:175px;" name="newEnquiryDto" property="winningChance" styleId="winningChance">
					                    			<option value="0"><bean:message key="quotation.option.select"/></option>
					                       	 		<html:options name="newEnquiryDto" collection="alWinChanceList" property="key" labelProperty="value" />
					                        	</html:select> 
					    					</td>
					    				</logic:equal>
					    				<logic:notEqual name="newenquiryForm" property="statusId" value="6">
					    					<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="winningChance"/></td>
					    				</logic:notEqual>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Targeted <span class="mandatory">&nbsp;</span> </th>
					    				<logic:equal name="newenquiryForm" property="statusId" value="6">
					    					<td>
					    						<bean:define name="newEnquiryDto" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:175px" name="newEnquiryDto" property="targeted" styleId="targeted">
													<option value="0" selected>Select</option>
					                    			<html:options name="newEnquiryDto" collection="alTargetedList" property="key" labelProperty="value" />
				        	            		</html:select>
				        	            	</td>
					    				</logic:equal>
					    				<logic:notEqual name="newenquiryForm" property="statusId" value="6">
					    					<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="targeted"/></td>
					    				</logic:notEqual>
					    			</tr>
					    		</table>
							</td>
     						<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
            				<td width="32%" align="left" valign="top" style="width:300px;" >
								<table align="left" style="width:100%;">
									<tr>	
										<td colspan="2">			  
									  	<label class="blue-light" style="font-weight:bold; width:100%;">
									    	<bean:message key="quotation.label.important.dates"/>
									    </label>
									    </td>
								  	</tr>
								  	<tr>
					    				<th width="50%"  class="label-text" > Enquiry Receipt date <span class="mandatory">&nbsp;</span> </th>
					    				<td width="50%"  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="receiptDate"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Required Offer Submission date <span class="mandatory">&nbsp;</span> </th>
					    				<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="submissionDate"/></td>
					    			</tr>
					    			<tr>
					    				<th  class="label-text" > Order Closing date <span class="mandatory">&nbsp;</span> </th>
					    				<logic:equal name="newenquiryForm" property="statusId" value="6">
					    					<td>
					    						<html:text name="newEnquiryDto" property="closingDate" styleId="closingDate" styleClass="readonlymini" readonly="true" style="width:150px" maxlength="12"/>			     
											</td>
					    				</logic:equal>
					    				<logic:notEqual name="newenquiryForm" property="statusId" value="6">
					    					<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="closingDate"/></td>
					    				</logic:notEqual>
					    				
					    			</tr>
					    		</table>
							</td>
						</tr>
						
					</table>
										
					<table width="100%">
						<tr>
							<td style="width:800px !important; line-height:20px;" colspan="2">
								<div style="font: italic 10px Verdana, Arial, Helvetica, sans-serif; text-align:left !important; font-weight:bold !important;">
									<html:checkbox name="newEnquiryDto" property="customerDealerLink" styleId="customerDealerLink" value="Y" disabled="true" />
		                			<label for="customerDealerLink" style="width:500px !important;"> <bean:message key="quotation.label.create.customerDealerLink"/> </label>
								</div>
							</td>
						</tr>
                		<tr>				  
						    <td width="15%" class="label-text" > <strong> <bean:message key="quotation.create.label.enquiryreferencenumber"/> </strong> &nbsp;&nbsp; </td>
							<td width="10%" class="formContentview-rating-b0x">
								<logic:equal name="newenquiryForm" property="enquiryReferenceNumber" value="">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</logic:equal>
								<logic:notEqual name="newenquiryForm" property="description" value="">
									<bean:write name="newenquiryForm" property="enquiryReferenceNumber"/>
								</logic:notEqual>
							</td>
							<td colspan="2" width="75%" align="left" valign="top" > &nbsp; </td>
						</tr>
						<tr>				  
				    		<td class="label-text" style="width:15% !important;"> <strong> <bean:message key="quotation.create.label.description"/> : </strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; </td>
											    		<td colspan="3" class="formContentview-rating-b0x" style="width:85% !important;word-break:break-all;height:40px;">
												    		<logic:equal name="newenquiryForm" property="description" value="">
																	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															</logic:equal>
															<logic:notEqual name="newenquiryForm" property="description" value="">
															    	<bean:write name="newenquiryForm" property="description"/>
															</logic:notEqual>
												    	</td>
						</tr>
                 		<tr>			
		                    <th class="label-text" style="width:15% !important;"> <bean:message key="quotation.create.label.createdby"/>:&nbsp; </th>
		                    <td class="formContentview-rating-b0x" style="width:35% !important;"> <font class="formContentview-rating-b0x"><bean:write name="newenquiryForm" property="createdBy"/></font> </td>
		                    <th class="label-text" style="width:15% !important;"> <bean:message key="quotation.create.label.createddate"/>:&nbsp; </th>
		                    <td class="formContentview-rating-b0x" style="width:35% !important;"> <font class="formContentview-rating-b0x"><bean:write name="newenquiryForm" property="createdDate"/></font> </td>
                  		</tr>	
			    	</table>
			    	
				</fieldset>
				
				<fieldset>
									<legend class="blueheader"> Commercial Terms and Conditions </legend>
									
									<!-- Commercial Terms Section -->
									<table width="100%">
										<tr>
											<td style="vertical-align: top">
												<!--  
												<table width="100%">
													<tr>
														<td>
															<label class="blue-light" style="font-weight:bold; width:100% !important;"> Commercial Terms and Conditions </label>
														</td>
													</tr>
												</table>
												-->
												<table width="100%">
													<tr>
														<th class="label-text" >Payment Terms </th>
														<th class="label-text" >%age amount of Nett. order value </th>
														<th class="label-text" >Payment Days </th>
														<th class="label-text" >Payable Terms </th>
														<th class="label-text" >Instrument </th>
													</tr>
													<tr>
														<td class="formContentview-rating-b0x" >Advance Payment 1 </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PaymentDays"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PayableTerms"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1Instrument"/> </td>
													</tr>
													<tr>
														<td class="formContentview-rating-b0x" >Advance Payment 2 </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PaymentDays"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PayableTerms"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2Instrument"/> </td>
													</tr>
													<tr>
														<td class="formContentview-rating-b0x" >Main Payment </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PaymentDays"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PayableTerms"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3Instrument"/> </td>
													</tr>
													<tr>
														<td class="formContentview-rating-b0x" >Retention Payment 1 </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PaymentDays"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PayableTerms"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4Instrument"/> </td>
													</tr>
													<tr>
														<td class="formContentview-rating-b0x" >Retention Payment 2 </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PaymentDays"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PayableTerms"/> </td>
														<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5Instrument"/> </td>
													</tr>
												</table>
												<br><br>
												<table width="100%">
													<tr>
														<th class="label-text" style="width:25% !important;">Delivery Term </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryTerm"/></td>
														<th class="label-text" style="width:25% !important;">Delivery </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryInFull"/> </td>
													</tr>
													<tr>
														<th class="label-text" style="width:25% !important;">Customer Requested Delivery (Weeks) </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>	
														<th class="label-text" style="width:25% !important;">Order Completion Lead Time </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="orderCompletionLeadTime"/> Weeks</td>
													</tr>
													<tr>
														<th class="label-text" style="width:25% !important;">Warranty from the date of dispatch </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyDispatchDate"/> Months</td>
														<th class="label-text" style="width:25% !important;">Warranty from the date of comissioning </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyCommissioningDate"/> Months</td>
													</tr>
													<tr>
														<th class="label-text" style="width:25% !important;">GST </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="gstValue"/> </td>
														<th class="label-text" style="width:25% !important;">Packaging </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="packaging"/> </td>
													</tr>
													<tr>
														<th class="label-text" style="width:25% !important;">Offer Validity </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
														<th class="label-text" style="width:25% !important;">Price Validity from PO date (for Drawing Approval / Mfg. Clearance) </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
													</tr>
													<tr>
														<th class="label-text" style="width:25% !important;">Liquidated Damages due to delayed deliveries </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="liquidatedDamagesDueToDeliveryDelay"/> </td>
														<th class="label-text" style="width:25% !important;"> &nbsp; </th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
													</tr>
												</table>
												<br><br>
												<table width="100%">
													<tr>
														<th class="label-text" style="width:25% !important;">Supervision for Erection and Comissioning</th>
														<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="supervisionDetails"/> </td>
														<td >&nbsp;</td>
														<td >&nbsp;</td>
													</tr>
												</table>
												<logic:equal name="newenquiryForm" property="supervisionDetails" value="Yes">
													<table width="50%" >
														<tr>
															<td class="label-text" style="width:25% !important;">No. of Man Days</td>
															<td class="formContentview-rating-b0x" style="width:75% !important;"> <bean:write name="newenquiryForm" property="superviseNoOfManDays"/> </td>
														</tr>
														<tr>
															<th class="label-text" colspan="2">&nbsp;</th>
														</tr>
														<tr>
															<th class="label-text" style="width:100% !important; white-space:nowrap;" colspan="2"> Note: To & Fro travelling charges, Fooding, Lodging & Local conveyance at site are at customer's account. </th>
														</tr>
													</table>	
												</logic:equal>
												<br><br>
											</td>
										</tr>
									</table>
									<!-- Commercial Terms Section -->
									
				</fieldset>
								
				<!----------------------- Employee Information block end ---------------------------------------->
				
				<br>
				
				<!----------------------- Ratings Details : Block Start ---------------------------------------->
				
				<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
					<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<div id="arrow<%=idx.intValue()+1 %>" >
						<a href="javascript:;" onClick="toggledropdown('arrow<%=idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
							<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/>
						</a>
						<span class="other">
							<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
						</span>
						</div>
						
						<fieldset id="fieldset<%=idx.intValue()+1 %>" style="display:none; ">
							<legend>
								<a href="javascript:;" onClick="toggledropdown('fieldset<%=idx.intValue() + 1%>','arrow<%=idx.intValue() + 1%>')">
									<img src="html/images/downTriArrow.gif" width="15" height="15" border="0" align="absmiddle" />
								</a> 
								<span class="blueheader"> <bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%=idx.intValue() + 1%>
								</span>
							</legend>
							<br>
							
							<!-- Rating Data : Starts Here -->
							
							<table width="100%">
			   					<tr>
			              			<td colspan="5" class="blue-light-btn" style="height:25px " align="right" >
			              				<strong> <bean:message key="quotation.create.label.quantityrequired"/> </strong>
			              				<bean:write name="ratingObj" property="quantity"/>&nbsp;
			              			</td>
           			 			</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Tag Number# </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="tagNumber"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Ambient Temp. Deg C </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAmbTempId"/> </td>
								</tr>
								<!--  
								<tr>
									<td class="label-text" style="width:25%"> <strong> Product Group </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltProdGroupId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Haz Location </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHazardAreaId"/> </td>
								</tr>
								-->
								<tr>
									<td class="label-text" style="width:25%"> <strong> Product Line </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltProdLineId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Gas Group </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGasGroupId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Type of Motor </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMotorTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Application </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltApplicationId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> KW </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltKWId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Voltage +/- variation </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVoltId"/> 
										<logic:notEmpty  name="ratingObj" property="ltVoltId">
											<logic:notEmpty  name="ratingObj" property="ltVoltAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltVoltAddVariation"/> % </logic:notEmpty>
											<logic:notEmpty  name="ratingObj" property="ltVoltRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltVoltRemoveVariation"/> % </logic:notEmpty>
										</logic:notEmpty>
									</td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Pole </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPoleId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Frequecy +/- variation </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFreqId"/> 
										<logic:notEmpty  name="ratingObj" property="ltFreqId">
											<logic:notEmpty  name="ratingObj" property="ltFreqAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltFreqAddVariation"/> % </logic:notEmpty>
											<logic:notEmpty  name="ratingObj" property="ltFreqRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltFreqRemoveVariation"/> % </logic:notEmpty>
										</logic:notEmpty>
									</td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Frame </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFrameId"/> <bean:write name="ratingObj" property="ltFrameSuffixId"/></td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Combined Variation </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCombVariation"/> % </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Mounting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMountingId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Duty </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDutyId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> TB Position </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTBPositionId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> CDF </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCDFId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Manufacturing Location </strong>  </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltManufacturingLocation"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> No. of Starts </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartsId"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Total Addon % </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalAddonPercent"> <bean:write name="ratingObj" property="ltTotalAddonPercent"/> % </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Total Cast Extra </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalCashExtra"> &#8377; <bean:write name="ratingObj" property="ltTotalCashExtra"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> List Price Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPPerUnit"/> </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> List Price with Addon Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPWithAddonPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPWithAddonPerUnit"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Price Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltPricePerUnit"> &#8377; <bean:write name="ratingObj" property="ltPricePerUnit"/> </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Total Order Price </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalOrderPrice"> &#8377; <bean:write name="ratingObj" property="ltTotalOrderPrice"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Standard Delivery (Weeks) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStandardDeliveryWks"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Earlier Supplied Motor Serial No. </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEarlierMotorSerialNo"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Customer Requested Delivery (Weeks) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCustReqDeliveryWks"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Replacement Motor </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltReplacementMotor"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Electrical </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> RV </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRVId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> RA </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRAId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> &nbsp; </td>
									<td class="label-text" style="width:24%"> &nbsp; </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Winding Treatment </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingTreatmentId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Winding Wire </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingWire"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Lead </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLeadId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Starting Current on DOL Starting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Winding Configuration </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingConfig"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Load GD2 Value referred to motor shaft </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLoadGD2Value"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method of Starting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfStarting"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> VFD Application Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDApplTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Min.) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Max.) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Service Factor </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltServiceFactor"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Overloading Duty </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltOverloadingDuty"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Constant Efficiency Range </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltConstantEfficiencyRange"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Mechanical </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Shaft Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Shaft Material </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftMaterialId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Direction of Rotation </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionOfRotation"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method of Coupling </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfCoupling"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Painting Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintingTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Paint Shade </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintShadeId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Paint Thickness </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintThicknessId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> IP </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltIPId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Insulation Class </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltInsulationClassId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Terminal Box Size </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTerminalBoxSizeId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spreader Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpreaderBoxId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Aux Terminal Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAuxTerminalBoxId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Space Heater </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpaceHeaterId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Vibration </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVibrationId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Flying Lead without TB </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Flying Lead with TB </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithTDId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Fan </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMetalFanId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Encoder Mounting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTechoMounting"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Shaft Grounding </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftGroundingId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method Of Cooling </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltForcedCoolingId"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Accessories </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Hardware </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHardware"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Gland Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGlandPlateId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Double Compression Gland </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDoubleCompressionGlandId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> SPM Mounting Provision </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSPMMountingProvisionId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Name Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddNamePlateId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Direction Arrow Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionArrowPlateId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> RTD </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRTDId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> BTD </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBTDId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Thermister </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltThermisterId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Cable Sealing Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCableSealingBoxId"/> </td>
								</tr>
           			 			<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Bearings </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> ReLubrication System </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingSystemId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Bearing NDE </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingNDEId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Bearing DE </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingDEId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td style="width:25%"> &nbsp; </td>
									<td style="width:24%"> &nbsp; </td>
								</tr>
           			 			<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Certificate / Spares / Testing </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Witness Routine </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWitnessRoutineId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Additional Test 1 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest1Id"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Test 2 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest2Id"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Additional Test 3 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest3Id"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Type Test </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTypeTestId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Certification </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltULCEId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spares 1 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares1"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Spares 2 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares2"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spares 3 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares3"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Spares 4 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares4"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spares 5 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares5"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Data Sheet </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDataSheet"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Add Drawing </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddDrawingId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td style="width:25%"> &nbsp; </td>
									<td style="width:24%"> &nbsp; </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Comments </strong> </td>
									<td colspan="4" class="formContentview-rating-b0x" style="width:75%;word-break:break-all;height:80px;"> <bean:write name="ratingObj" property="ltAdditionalComments"/> </td>
								</tr>
							</table>
							
							<!-- Rating Data : Ends Here   -->
												
						</fieldset>
												
					</logic:iterate>
					
				</logic:notEmpty>
				<!----------------------- Ratings Details : Block End ------------------------------------------>
				
				<br>
				
			</logic:present>
		</td>
	</tr>
</table>


<script>
	$("#receiptDate").datepicker({
		dateFormat : 'dd-mm-yy',
		onSelect : function(selectedDate) {
			if (this.id == 'receiptDate') {
				var arr = selectedDate.split("-");
				var date = new Date(arr[2] + "-" + arr[1] + "-" + arr[0]);
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				var minDate = new Date(y, m + 3, d);
			}
		}
	});

	$("#submissionDate").datepicker({
		dateFormat : 'dd-mm-yy',
		onSelect : function(selectedDate) {
			if (this.id == 'submissionDate') {
				var arr = selectedDate.split("-");
				var date = new Date(arr[2] + "-" + arr[1] + "-" + arr[0]);
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				var minDate = new Date(y, m + 3, d);
			}
		}
	});

	$("#closingDate").datepicker({
		dateFormat : 'dd-mm-yy',
		onSelect : function(selectedDate) {
			if (this.id == 'closingDate') {
				var arr = selectedDate.split("-");
				var date = new Date(arr[2] + "-" + arr[1] + "-" + arr[0]);
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();
				var minDate = new Date(y, m + 3, d);
			}
		}
	});
</script>