<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
	<tr class="linked" style="width:1174px !important; float:left; overflow-x: scroll;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 17; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd" style="float:left;"><strong>Winding Connection</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd" style="float:left;"><strong>Winding Connection</strong></td>
					<% } %>
						
						<%  int idElecRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="windingConfiguration<%=idElecRow1%>" id="windingConfiguration<%=idElecRow1%>" title="Winding Configuration" onfocus="javascript:processAutoSave('<%=idElecRow1%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltWindingConfigurationList">
									<bean:define name="newmtoenquiryForm" property="ltWindingConfigurationList" id="ltWindingConfigurationList" type="java.util.Collection" />
									<logic:iterate name="ltWindingConfigurationList" id="ltWindingConfiguration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltWindingConfiguration" property="key" />'> <bean:write name="ltWindingConfiguration" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('windingConfiguration<%=idElecRow1%>', '<bean:write name="ratingObj" property="ltWindingConfig" filter="false" />'); </script>
						<%	idElecRow1 = idElecRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Winding Wire</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Winding Wire</strong></td>
					<% } %>
						
						<%  int idElecRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="windingWire<%=idElecRow2%>" id="windingWire<%=idElecRow2%>" title="Winding Wire" onfocus="javascript:processAutoSave('<%=idElecRow2%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltWindingWireList">
									<bean:define name="newmtoenquiryForm" property="ltWindingWireList" id="ltWindingWireList" type="java.util.Collection" />
									<logic:iterate name="ltWindingWireList" id="ltWindingWire" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltWindingWire" property="key" />'> <bean:write name="ltWindingWire" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('windingWire<%=idElecRow2%>', '<bean:write name="ratingObj" property="ltWindingWire" filter="false" />'); </script>
						<%	idElecRow2 = idElecRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Winding Treatment</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Winding Treatment</strong></td>
					<% } %>
						
						<%  int idElecRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="windingTreatment<%=idElecRow3%>" id="windingTreatment<%=idElecRow3%>" title="Winding Treatment" onfocus="javascript:processAutoSave('<%=idElecRow3%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltWindingTreatmentList">
									<bean:define name="newmtoenquiryForm" property="ltWindingTreatmentList" id="ltWindingTreatmentList" type="java.util.Collection" />
									<logic:iterate name="ltWindingTreatmentList" id="ltWindingTreatment" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltWindingTreatment" property="key" />'> <bean:write name="ltWindingTreatment" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('windingTreatment<%=idElecRow3%>', '<bean:write name="ratingObj" property="ltWindingTreatmentId" filter="false" />'); </script>
						<%	idElecRow3 = idElecRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Duty Class</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Duty Class</strong></td>
					<% } %>
						
						<%  int idElecRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="duty<%=idElecRow4%>" id="duty<%=idElecRow4%>" title="Duty" onchange="javascript:processFieldsForDuty('<%=idElecRow4%>');" onfocus="javascript:processAutoSave('<%=idElecRow4%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present property="ltDutyList" name="newmtoenquiryForm">
																<bean:define name="newmtoenquiryForm" property="ltDutyList" id="ltDutyList" type="java.util.Collection" />
																<logic:iterate name="ltDutyList" id="ltDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltDuty" property="key" />' > <bean:write name="ltDuty" property="value" /> </option>
																</logic:iterate>
															</logic:present>
							</select>
						</td>
						<script> selectLstItemById('duty<%=idElecRow4%>', '<bean:write name="ratingObj" property="ltDutyId" filter="false" />'); </script>
						<%	idElecRow4 = idElecRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>CDF</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>CDF</strong></td>
					<% } %>
						
						<%  int idElecRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" class="normal"  name="cdf<%=idElecRow5%>" id="cdf<%=idElecRow5%>" title="CDF" onfocus="javascript:processAutoSave('<%=idElecRow5%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present property="ltCDFList" name="newmtoenquiryForm">
																<bean:define name="newmtoenquiryForm" property="ltCDFList" id="ltCDFList" type="java.util.Collection" />
																<logic:iterate name="ltCDFList" id="ltCDF" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltCDF" property="key" />' > <bean:write name="ltCDF" property="value" /> </option>
																</logic:iterate>
															</logic:present>
							</select>
						</td>
						<script> selectLstItemById('cdf<%=idElecRow5%>', '<bean:write name="ratingObj" property="ltCDFId" filter="false" />'); </script>
						<%	idElecRow5 = idElecRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>No. of Starts</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>No. of Starts</strong></td>
					<% } %>
						
						<%  int idElecRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" class="normal" name="starts<%=idElecRow6%>" id="starts<%=idElecRow6%>" title="No. of Starts" onfocus="javascript:processAutoSave('<%=idElecRow6%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present property="ltStartsList" name="newmtoenquiryForm">
																<bean:define name="newmtoenquiryForm" property="ltStartsList" id="ltStartsList" type="java.util.Collection" />
																<logic:iterate name="ltStartsList" id="ltStarts" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltStarts" property="key" />' > <bean:write name="ltStarts" property="value" /> </option>
																</logic:iterate>
															</logic:present>
							</select>
						</td>
						<script> selectLstItemById('starts<%=idElecRow6%>', '<bean:write name="ratingObj" property="ltStartsId" filter="false" />'); </script>
						<%	idElecRow6 = idElecRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Overloading Duty</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Overloading Duty</strong></td>
					<% } %>
						
						<%  int idElecRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_overloadingDuty<%=idElecRow7%>" class="bordertd">
							<select style="width: 100px;" name="overloadingDuty<%=idElecRow7%>" id="overloadingDuty<%=idElecRow7%>" title="Overloading Duty" onfocus="javascript:processAutoSave('<%=idElecRow7%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltOverloadingDutyList">
															<bean:define name="newmtoenquiryForm" property="ltOverloadingDutyList" id="ltOverloadingDutyList" type="java.util.Collection" />
															<logic:iterate name="ltOverloadingDutyList" id="ltOverloadingDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltOverloadingDuty" property="key" />'>
																	<bean:write name="ltOverloadingDuty" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('overloadingDuty<%=idElecRow7%>', '<bean:write name="ratingObj" property="ltOverloadingDuty" filter="false" />'); </script>
						<%	idElecRow7 = idElecRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Service Factor</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Service Factor</strong></td>
					<% } %>
						
						<%  int idElecRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_serviceFactor<%=idElecRow8%>" class="bordertd" >
							<select style="width: 100px;" name="serviceFactor<%=idElecRow8%>" id="serviceFactor<%=idElecRow8%>" title="Service Factor" onfocus="javascript:processAutoSave('<%=idElecRow8%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newmtoenquiryForm" property="ltServiceFactorList">
																<bean:define name="newmtoenquiryForm" property="ltServiceFactorList" id="ltServiceFactorList" type="java.util.Collection" />
																<logic:iterate name="ltServiceFactorList" id="ltServiceFactor" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltServiceFactor" property="key" />'>
																		<bean:write name="ltServiceFactor" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
							</select>
						</td>
						<script> selectLstItemById('serviceFactor<%=idElecRow8%>', '<bean:write name="ratingObj" property="ltServiceFactor" filter="false" />'); </script>
						<%	idElecRow8 = idElecRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>No. of brought out terminals</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>No. of brought out terminals</strong></td>
					<% } %>
						
						<%  int idElecRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="noBroughtOutTerm<%=idElecRow9%>" id="noBroughtOutTerm<%=idElecRow9%>" title="Service Factor" onfocus="javascript:processAutoSave('<%=idElecRow9%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltBroughtOutTerminalsList" >
									<bean:define name="newmtoenquiryForm" property="ltBroughtOutTerminalsList" id="ltBroughtOutTerminalsList" type="java.util.Collection" />
									<logic:iterate name="ltBroughtOutTerminalsList" id="ltBroughtOutTerminals" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltBroughtOutTerminals" property="key" />' > <bean:write name="ltBroughtOutTerminals" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<%	idElecRow9 = idElecRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Leads</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Leads</strong></td>
					<% } %>
						
						<%  int idElecRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_lead<%=idElecRow10%>" class="bordertd">
							<select style="width: 100px;" name="lead<%=idElecRow10%>" id="lead<%=idElecRow10%>" title="Lead" onfocus="javascript:processAutoSave('<%=idElecRow10%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltLeadList">
															<bean:define name="newmtoenquiryForm" property="ltLeadList" id="ltLeadList" type="java.util.Collection" />
															<logic:iterate name="ltLeadList" id="ltLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltLead" property="key" />'>
																	<bean:write name="ltLead" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('lead<%=idElecRow10%>', '<bean:write name="ratingObj" property="ltLeadId" filter="false" />'); </script>
						<%	idElecRow10 = idElecRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Starting current on DOL starting</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Starting current on DOL starting</strong></td>
					<% } %>
						
						<%  int idElecRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_startingCurrOnDOLStarting<%=idElecRow11%>" class="bordertd">
							<select style="width: 100px;" name="startingCurrOnDOLStarting<%=idElecRow11%>" id="startingCurrOnDOLStarting<%=idElecRow11%>" title="Starting Current On DOL Starting" onfocus="javascript:processAutoSave('<%=idElecRow11%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newmtoenquiryForm" property="ltStartingDOLCurrentList">
																<bean:define name="newmtoenquiryForm" property="ltStartingDOLCurrentList" id="ltStartingDOLCurrentList" type="java.util.Collection" />
																<logic:iterate name="ltStartingDOLCurrentList" id="ltStartingDOLCurrent" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltStartingDOLCurrent" property="key" />'>
																		<bean:write name="ltStartingDOLCurrent" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
							</select>
						</td>
						<script> selectLstItemById('startingCurrOnDOLStarting<%=idElecRow11%>', '<bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart" filter="false" />'); </script>
						<%	idElecRow11 = idElecRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>VFD Application Type</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>VFD Application Type</strong></td>
					<% } %>
						
						<%  int idElecRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_vfdType<%=idElecRow12%>" class="bordertd">
							<select style="width: 100px;" name="vfdType<%=idElecRow12%>" id="vfdType<%=idElecRow12%>" title="VDF Application Type" onfocus="javascript:processAutoSave('<%=idElecRow12%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltVFDTypeList">
															<bean:define name="newmtoenquiryForm" property="ltVFDTypeList" id="ltVFDTypeList" type="java.util.Collection" />
															<logic:iterate name="ltVFDTypeList" id="ltVFDType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltVFDType" property="key" />'>
																	<bean:write name="ltVFDType" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('vfdType<%=idElecRow12%>', '<bean:write name="ratingObj" property="ltVFDApplTypeId" filter="false" />'); </script>
						<%	idElecRow12 = idElecRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Speed range only for VFD motor (Min)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Speed range only for VFD motor (Min)</strong></td>
					<% } %>
						
						<%  int idElecRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_vfdSpeedRangeMin<%=idElecRow13%>" class="bordertd">
							<select style="width: 100px;" name="vfdSpeedRangeMin<%=idElecRow13%>" id="vfdSpeedRangeMin<%=idElecRow13%>" title="Speed Range only for VFD motor (Min.)" onfocus="javascript:processAutoSave('<%=idElecRow13%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltVFDSpeedRangeMinList">
															<bean:define name="newmtoenquiryForm" property="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMinList" type="java.util.Collection" />
															<logic:iterate name="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMin" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltVFDSpeedRangeMin" property="key" />'>
																	<bean:write name="ltVFDSpeedRangeMin" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('vfdSpeedRangeMin<%=idElecRow13%>', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin" filter="false" />'); </script>
						<%	idElecRow13 = idElecRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Speed range only for VFD motor (Max)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Speed range only for VFD motor (Max)</strong></td>
					<% } %>
						
						<%  int idElecRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_vfdSpeedRangeMax<%=idElecRow14%>" class="bordertd">
							<select style="width: 100px;" name="vfdSpeedRangeMax<%=idElecRow14%>" id="vfdSpeedRangeMax<%=idElecRow14%>" title="Speed Range only for VFD motor (Max.)" onfocus="javascript:processAutoSave('<%=idElecRow14%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltVFDSpeedRangeMaxList">
															<bean:define name="newmtoenquiryForm" property="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMaxList" type="java.util.Collection" />
															<logic:iterate name="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMax" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltVFDSpeedRangeMax" property="key" />'>
																	<bean:write name="ltVFDSpeedRangeMax" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('vfdSpeedRangeMax<%=idElecRow14%>', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax" filter="false" />'); </script>
						<%	idElecRow14 = idElecRow14+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 15) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Constant Efficiency Range</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Constant Efficiency Range</strong></td>
					<% } %>
						
						<%  int idElecRow15 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_constantEfficiencyRange<%=idElecRow15%>" class="bordertd">
							<select style="width: 100px;" name="constantEfficiencyRange<%=idElecRow15%>" id="constantEfficiencyRange<%=idElecRow15%>" title="Constant Efficeincy Range" onfocus="javascript:processAutoSave('<%=idElecRow15%>');" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newmtoenquiryForm" property="ltConstantEffRangeList">
															<bean:define name="newmtoenquiryForm" property="ltConstantEffRangeList" id="ltConstantEffRangeList" type="java.util.Collection" />
															<logic:iterate name="ltConstantEffRangeList" id="ltConstantEffRange" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltConstantEffRange" property="key" />'>
																	<bean:write name="ltConstantEffRange" property="value" />
																</option>
															</logic:iterate>
														</logic:present>
							</select>
						</td>
						<script> selectLstItemById('constantEfficiencyRange<%=idElecRow15%>', '<bean:write name="ratingObj" property="ltConstantEfficiencyRange" filter="false" />'); </script>
						<%	idElecRow15 = idElecRow15+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 16) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>RV (Slip Ring Motor)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>RV (Slip Ring Motor)</strong></td>
					<% } %>
						
						<%  int idElecRow16 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_rv<%=idElecRow16%>" class="bordertd">
							<input type="text" name="rv<%=idElecRow16%>" id="rv<%=idElecRow16%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltRVId" filter="false" />' title="RV" onfocus="javascript:processAutoSave('<%=idElecRow16%>');" maxlength="50" >
						</td>
						<%	idElecRow16 = idElecRow16+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 17) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>RA (Slip Ring Motor)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "te6tnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>RA (Slip Ring Motor)</strong></td>
					<% } %>
						
						<%  int idElecRow17 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_ra<%=idElecRow17%>" class="bordertd">
							<input type="text" name="ra<%=idElecRow17%>" id="ra<%=idElecRow17%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltRAId" filter="false" />' title="RA" onfocus="javascript:processAutoSave('<%=idElecRow17%>');" maxlength="50" >
						</td>
						<%	idElecRow17 = idElecRow17+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
	</tbody>
</table>