<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
		<tr class="linked" style="width:1100px !important; float:left; overflow-x: scroll;">
			<td valign="top">
				<table id="tableR" align="center" cellspacing="0" cellpadding="1">
				<%	String sTextStyleClass = "textnoborder-light"; %>
				<% for(int j = 1; j <= 11; j++) {  %>
					<% if(j == 1) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" style="float:left;"><strong>RV</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" style="float:left;"><strong>RV</strong></td>
							<% } %>
							
							<%  int idElecRow1 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="rv<%=idElecRow1%>" id="rv<%=idElecRow1%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltRVId" filter="false" />' title="RV" onfocus="javascript:processAutoSave('<%=idElecRow1%>');" maxlength="50" >
							</td>
							<%	idElecRow1 = idElecRow1+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 2) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>RA</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>RA</strong></td>
							<% } %>
							
							<%  int idElecRow2 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="ra<%=idElecRow2%>" id="ra<%=idElecRow2%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltRAId" filter="false" />' title="RA" onfocus="javascript:processAutoSave('<%=idElecRow2%>');" maxlength="50" > 
							</td>
							<%	idElecRow2 = idElecRow2+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 3) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Winding Connection</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Winding Connection</strong></td>
							<% } %>
							
							<%  int idElecRow3 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="windingConfiguration<%=idElecRow3%>" id="windingConfiguration<%=idElecRow3%>" title="Winding Configuration" onfocus="javascript:processAutoSave('<%=idElecRow3%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltWindingConfigurationList">
																<bean:define name="newenquiryForm" property="ltWindingConfigurationList" id="ltWindingConfigurationList" type="java.util.Collection" />
																<logic:iterate name="ltWindingConfigurationList" id="ltWindingConfiguration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltWindingConfiguration" property="key" />'>
																		<bean:write name="ltWindingConfiguration" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('windingConfiguration<%=idElecRow3%>', '<bean:write name="ratingObj" property="ltWindingConfig" filter="false" />'); </script>
							<%	idElecRow3 = idElecRow3+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 4) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Winding Treatment</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Winding Treatment</strong></td>
							<% } %>
							
							<%  int idElecRow4 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="windingTreatment<%=idElecRow4%>" id="windingTreatment<%=idElecRow4%>" title="Winding Treatment" onfocus="javascript:processAutoSave('<%=idElecRow4%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newenquiryForm" property="ltWindingTreatmentList">
										<bean:define name="newenquiryForm" property="ltWindingTreatmentList" id="ltWindingTreatmentList" type="java.util.Collection" />
										<logic:iterate name="ltWindingTreatmentList" id="ltWindingTreatment" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltWindingTreatment" property="key" />'> <bean:write name="ltWindingTreatment" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('windingTreatment<%=idElecRow4%>', '<bean:write name="ratingObj" property="ltWindingTreatmentId" filter="false" />'); </script>
							<%	idElecRow4 = idElecRow4+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 5) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Leads (No. of brought out Terminals)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Leads (No. of brought out Terminals)</strong></td>
							<% } %>
							
							<%  int idElecRow5 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="lead<%=idElecRow5%>" id="lead<%=idElecRow5%>" title="Lead" onfocus="javascript:processAutoSave('<%=idElecRow5%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltLeadList">
																<bean:define name="newenquiryForm" property="ltLeadList" id="ltLeadList" type="java.util.Collection" />
																<logic:iterate name="ltLeadList" id="ltLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltLead" property="key" />'>
																		<bean:write name="ltLead" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('lead<%=idElecRow5%>', '<bean:write name="ratingObj" property="ltLeadId" filter="false" />'); </script>
							<%	idElecRow5 = idElecRow5+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 6) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Load GD2 Value referred to motor shaft</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Load GD2 Value referred to motor shaft</strong></td>
							<% } %>
							
							<%  int idElecRow6 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="loadGD2Value<%=idElecRow6%>" id="loadGD2Value<%=idElecRow6%>" class="normal" style="width: 60px;" value='<bean:write name="ratingObj" property="ltLoadGD2Value" filter="false" />' title="Load GD2 Value referred to motor shaft" onfocus="javascript:processAutoSave('<%=idElecRow6%>');" maxlength="5" > <span style="font-size:8px; font-weight:bold;">kg-m2</span> 
							</td>
							<%	idElecRow6 = idElecRow6+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 7) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>VFD Application Type</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>VFD Application Type</strong></td>
							<% } %>
							
							<%  int idElecRow7 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="vfdType<%=idElecRow7%>" id="vfdType<%=idElecRow7%>" title="VDF Application Type" onfocus="javascript:processAutoSave('<%=idElecRow7%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltVFDTypeList">
																<bean:define name="newenquiryForm" property="ltVFDTypeList" id="ltVFDTypeList" type="java.util.Collection" />
																<logic:iterate name="ltVFDTypeList" id="ltVFDType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltVFDType" property="key" />'>
																		<bean:write name="ltVFDType" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('vfdType<%=idElecRow7%>', '<bean:write name="ratingObj" property="ltVFDApplTypeId" filter="false" />'); </script>
							<%	idElecRow7 = idElecRow7+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 8) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Speed Range only for VFD motor (Min.)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Speed Range only for VFD motor (Min.)</strong></td>
							<% } %>
							
							<%  int idElecRow8 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="vfdSpeedRangeMin<%=idElecRow8%>" id="vfdSpeedRangeMin<%=idElecRow8%>" title="Speed Range only for VFD motor (Min.)" onfocus="javascript:processAutoSave('<%=idElecRow8%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltVFDSpeedRangeMinList">
																<bean:define name="newenquiryForm" property="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMinList" type="java.util.Collection" />
																<logic:iterate name="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMin" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltVFDSpeedRangeMin" property="key" />'>
																		<bean:write name="ltVFDSpeedRangeMin" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('vfdSpeedRangeMin<%=idElecRow8%>', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin" filter="false" />'); </script>
							<%	idElecRow8 = idElecRow8+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 9) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Speed Range only for VFD motor (Max.)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Speed Range only for VFD motor (Max.)</strong></td>
							<% } %>	
							
							<%  int idElecRow9 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="vfdSpeedRangeMax<%=idElecRow9%>" id="vfdSpeedRangeMax<%=idElecRow9%>" title="Speed Range only for VFD motor (Max.)" onfocus="javascript:processAutoSave('<%=idElecRow9%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltVFDSpeedRangeMaxList">
																<bean:define name="newenquiryForm" property="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMaxList" type="java.util.Collection" />
																<logic:iterate name="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMax" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltVFDSpeedRangeMax" property="key" />'>
																		<bean:write name="ltVFDSpeedRangeMax" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('vfdSpeedRangeMax<%=idElecRow9%>', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax" filter="false" />'); </script>
							<%	idElecRow9 = idElecRow9+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 10) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Overloading Duty</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Overloading Duty</strong></td>
							<% } %>
							
							<%  int idElecRow10 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="overloadingDuty<%=idElecRow10%>" id="overloadingDuty<%=idElecRow10%>" title="Overloading Duty" onfocus="javascript:processAutoSave('<%=idElecRow10%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltOverloadingDutyList">
																<bean:define name="newenquiryForm" property="ltOverloadingDutyList" id="ltOverloadingDutyList" type="java.util.Collection" />
																<logic:iterate name="ltOverloadingDutyList" id="ltOverloadingDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltOverloadingDuty" property="key" />'>
																		<bean:write name="ltOverloadingDuty" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('overloadingDuty<%=idElecRow10%>', '<bean:write name="ratingObj" property="ltOverloadingDuty" filter="false" />'); </script>
							<%	idElecRow10 = idElecRow10+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 11) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Constant Efficiency Range</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Constant Efficiency Range</strong></td>
							<% } %>
							
							<%  int idElecRow11 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="constantEfficiencyRange<%=idElecRow11%>" id="constantEfficiencyRange<%=idElecRow11%>" title="Constant Efficeincy Range" onfocus="javascript:processAutoSave('<%=idElecRow11%>');" >
															<option value="0"><bean:message key="quotation.option.select" /></option>
															<logic:present name="newenquiryForm" property="ltConstantEffRangeList">
																<bean:define name="newenquiryForm" property="ltConstantEffRangeList" id="ltConstantEffRangeList" type="java.util.Collection" />
																<logic:iterate name="ltConstantEffRangeList" id="ltConstantEffRange" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="ltConstantEffRange" property="key" />'>
																		<bean:write name="ltConstantEffRange" property="value" />
																	</option>
																</logic:iterate>
															</logic:present>
								</select>
							</td>
							<script> selectLstItemById('constantEfficiencyRange<%=idElecRow11%>', '<bean:write name="ratingObj" property="ltConstantEfficiencyRange" filter="false" />'); </script>
							<%	idElecRow11 = idElecRow11+1; %>
							</logic:iterate>
						</tr>
					<% } %>
				<% } %>
				</table>
			</td>
		</tr>
	</tbody>
</table>

