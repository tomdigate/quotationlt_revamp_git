<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
	<tr class="linked" style="width:1174px !important; float:left;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 23; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd" style="float:left;"><strong>Rated Speed</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd" style="float:left;"><strong>Rated Speed</strong></td>
					<% } %>
						
						<%  int idPerfRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="ratedSpeed<%=idPerfRow1%>" id="ratedSpeed<%=idPerfRow1%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfRatedSpeed" filter="false" />' title="Rated Speed" onfocus="javascript:processAutoSave('<%=idPerfRow1%>');" maxlength="15" > <span style="font-weight:bold;"> r/min </span>
						</td> 
						<%	idPerfRow1 = idPerfRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Rated Current</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Rated Current</strong></td>
					<% } %>
						
						<%  int idPerfRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="ratedCurr<%=idPerfRow2%>" id="ratedCurr<%=idPerfRow2%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfRatedCurr" filter="false" />' title="Rated Current" onfocus="javascript:processAutoSave('<%=idPerfRow2%>');" maxlength="15" > <span style="font-weight:bold;"> A </span>
						</td> 
						<%	idPerfRow2 = idPerfRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>No Load Current</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>No Load Current</strong></td>
					<% } %>
						
						<%  int idPerfRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="noLoadCurr<%=idPerfRow3%>" id="noLoadCurr<%=idPerfRow3%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfNoLoadCurr" filter="false" />' title="No Load Current" onfocus="javascript:processAutoSave('<%=idPerfRow3%>');" maxlength="15" > <span style="font-weight:bold;"> A </span>
						</td>
						<%	idPerfRow3 = idPerfRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Locked Rotor Current</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Locked Rotor Current</strong></td>
					<% } %>
						
						<%  int idPerfRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="lockRotorCurr<%=idPerfRow4%>" id="lockRotorCurr<%=idPerfRow4%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfLockRotorCurr" filter="false" />' title="Locked Rotor Current" onfocus="javascript:processAutoSave('<%=idPerfRow4%>');" maxlength="15" > <span style="font-weight:bold;"> A </span>
						</td>
						<%	idPerfRow4 = idPerfRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Nominal Torque</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Nominal Torque</strong></td>
					<% } %>
						
						<%  int idPerfRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="nominalTorque<%=idPerfRow5%>" id="nominalTorque<%=idPerfRow5%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfNominalTorque" filter="false" />' title="Nominal Torque" onfocus="javascript:processAutoSave('<%=idPerfRow5%>');" maxlength="15" > <span style="font-weight:bold;"> Nm </span>
						</td>
						<%	idPerfRow5 = idPerfRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Maximum Torque</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Maximum Torque</strong></td>
					<% } %>
						
						<%  int idPerfRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="maxTorque<%=idPerfRow6%>" id="maxTorque<%=idPerfRow6%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfMaxTorque" filter="false" />' title="Max Torque" onfocus="javascript:processAutoSave('<%=idPerfRow6%>');" maxlength="15" > <span style="font-weight:bold;"> Nm </span>
						</td>
						<%	idPerfRow6 = idPerfRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Locked Rotor Torque</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Locked Rotor Torque</strong></td>
					<% } %>
						
						<%  int idPerfRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="lockRotorTorque<%=idPerfRow7%>" id="lockRotorTorque<%=idPerfRow7%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfLockRotorTorque" filter="false" />' title="Locked Rotor Torque" onfocus="javascript:processAutoSave('<%=idPerfRow7%>');" > <span style="font-weight:bold;"> Nm </span>
						</td>
						<%	idPerfRow7 = idPerfRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Safe Stall Time (Hot)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Safe Stall Time (Hot)</strong></td>
					<% } %>
						
						<%  int idPerfRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="sstHot<%=idPerfRow8%>" id="sstHot<%=idPerfRow8%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfSSTHot" filter="false" />' title="Safe Stall Time (Hot)" onfocus="javascript:processAutoSave('<%=idPerfRow8%>');" maxlength="15" > <span style="font-weight:bold;"> s </span>
						</td> 
						<%	idPerfRow8 = idPerfRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Safe Stall Time (Cold)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Safe Stall Time (Cold)</strong></td>
					<% } %>
						
						<%  int idPerfRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="sstCold<%=idPerfRow9%>" id="sstCold<%=idPerfRow9%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfSSTCold" filter="false" />' title="Safe Stall Time (Cold)" onfocus="javascript:processAutoSave('<%=idPerfRow9%>');" maxlength="15" > <span style="font-weight:bold;"> s </span>
						</td> 
						<%	idPerfRow9 = idPerfRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Started time at Rated Voltage</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Started time at Rated Voltage</strong></td>
					<% } %>
						
						<%  int idPerfRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="startTimeRatedVolt<%=idPerfRow10%>" id="startTimeRatedVolt<%=idPerfRow10%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfStartTimeRatedVolt" filter="false" />' title="Started time at Rated Voltage" onfocus="javascript:processAutoSave('<%=idPerfRow10%>');" maxlength="15" > <span style="font-weight:bold;"> s </span>
						</td> 
						<%	idPerfRow10 = idPerfRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Started time at 80% of Rated Voltage</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Started time at 80% of Rated Voltage</strong></td>
					<% } %>
						
						<%  int idPerfRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="startTimeAt80RatedVolt<%=idPerfRow11%>" id="startTimeAt80RatedVolt<%=idPerfRow11%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltPerfStartTime80RatedVolt" filter="false" />' title="Started time at 80% of Rated Voltage" onfocus="javascript:processAutoSave('<%=idPerfRow11%>');" maxlength="15" > <span style="font-weight:bold;"> s </span>
						</td> 
						<%	idPerfRow11 = idPerfRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Moment of inertia = 1/4 GD2 (Motor)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Moment of inertia = 1/4 GD2 (Motor)</strong></td>
					<% } %>
						
						<%  int idPerfRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="motorGD2<%=idPerfRow12%>" id="motorGD2<%=idPerfRow12%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltMotorGD2Value" filter="false" />' title="Moment of inertia = 1/4 GD2 (Motor)" onfocus="javascript:processAutoSave('<%=idPerfRow12%>');" maxlength="15" > <span style="font-weight:bold;"> kg-m2 </span>
						</td>
						<%	idPerfRow12 = idPerfRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Moment of inertia = 1/4 GD2 (Load)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Moment of inertia = 1/4 GD2 (Load)</strong></td>
					<% } %>
						
						<%  int idPerfRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_loadGD2<%=idPerfRow13%>" class="bordertd">
							<input type="text" name="loadGD2<%=idPerfRow13%>" id="loadGD2<%=idPerfRow13%>" class="normal" style="width:60px;" value='<bean:write name="ratingObj" property="ltLoadGD2Value" filter="false" />' title="Moment of inertia = 1/4 GD2 (Load)" onfocus="javascript:processAutoSave('<%=idPerfRow13%>');" maxlength="5"  > <span style="font-weight:bold;"> kg-m2 </span>
						</td>
						<%	idPerfRow13 = idPerfRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Vibration class (No load)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Vibration class (No load)</strong></td>
					<% } %>
						
						<%  int idPerfRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="vibration<%=idPerfRow14%>" id="vibration<%=idPerfRow14%>" title="Vibration" onfocus="javascript:processAutoSave('<%=idPerfRow14%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltVibrationList" >
									<bean:define name="newmtoenquiryForm" property="ltVibrationList" id="ltVibrationList" type="java.util.Collection" />
									<logic:iterate name="ltVibrationList" id="ltVibration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltVibration" property="key" />' > <bean:write name="ltVibration" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('vibration<%=idPerfRow14%>', '<bean:write name="ratingObj" property="ltVibrationId" filter="false" />'); </script>
						<%	idPerfRow14 = idPerfRow14+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 15) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Total Weight of Motor (Kg)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Total Weight of Motor (Kg)</strong></td>
					<% } %>
						
						<%  int idPerfRow15 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="totalMotorWeight<%=idPerfRow15%>" id="totalMotorWeight<%=idPerfRow15%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltMotorWeight" filter="false" />' title="Total Weight of Motor (Kg)" onfocus="javascript:processAutoSave('<%=idPerfRow15%>');" maxlength="20"  >
						</td>
						<%	idPerfRow15 = idPerfRow15+1; %>
						</logic:iterate>
					</tr>
				
				<% } else if(j == 16) { %>
					<tr id='item<%=j%>'>
						<td width="200px" class="dark bordertd" style="padding-top:9px; padding-bottom:9px;"><strong>Load characteristics <br/> (IEC 60034-2-1:2014)</strong></td>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd" style="padding-top:9px; padding-bottom:9px;">
							&nbsp;&nbsp;
						</td>
						</logic:iterate>
					</tr>
				
				<% } else if(j == 17) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	% Efficiency - Above 100% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	% Efficiency - Above 100% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow17 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="effPercentAbove100Load<%=idPerfRow17%>" id="effPercentAbove100Load<%=idPerfRow17%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadEffPercent100Above" filter="false" />' title="% Efficiency - Above 100% Load" onfocus="javascript:processAutoSave('<%=idPerfRow17%>');" maxlength="15" >
						</td>
						<%	idPerfRow17 = idPerfRow17+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 18) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	% Efficiency - At 100% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	% Efficiency - At 100% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow18 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="effPercentAt100Load<%=idPerfRow18%>" id="effPercentAt100Load<%=idPerfRow18%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadEffPercentAt100" filter="false" />' title="% Efficiency - At 100% Load" onfocus="javascript:processAutoSave('<%=idPerfRow18%>');" maxlength="15" >	
						</td>
						<%	idPerfRow18 = idPerfRow18+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 19) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	% Efficiency - At 75% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	% Efficiency - At 75% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow19 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="effPercentAt75Load<%=idPerfRow19%>" id="effPercentAt75Load<%=idPerfRow19%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadEffPercentAt75" filter="false" />' title="% Efficiency - At 75% Load" onfocus="javascript:processAutoSave('<%=idPerfRow19%>');" maxlength="15" >
						</td>
						<%	idPerfRow19 = idPerfRow19+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 20) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	% Efficiency - At 50% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	% Efficiency - At 50% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow20 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="effPercentAt50Load<%=idPerfRow20%>" id="effPercentAt50Load<%=idPerfRow20%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadEffPercentAt50" filter="false" />' title="% Efficiency - At 50% Load" onfocus="javascript:processAutoSave('<%=idPerfRow20%>');" maxlength="15" >
						</td>
						<%	idPerfRow20 = idPerfRow20+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 21) { %>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	Power Factor - At 100% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	Power Factor - At 100% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow21 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="powerFactorAt100Load<%=idPerfRow21%>" id="powerFactorAt100Load<%=idPerfRow21%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadPFPercentAt100" filter="false" />' title="Power Factor - At 100% Load" onfocus="javascript:processAutoSave('<%=idPerfRow21%>');" maxlength="15" >
						</td>
						<%	idPerfRow21 = idPerfRow21+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 22) { %>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	Power Factor - At 75% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	Power Factor - At 75% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow22 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="powerFactorAt75Load<%=idPerfRow22%>" id="powerFactorAt75Load<%=idPerfRow22%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadPFPercentAt75" filter="false" />' title="Power Factor - At 75% Load" onfocus="javascript:processAutoSave('<%=idPerfRow22%>');" maxlength="15" >
						</td>
						<%	idPerfRow22 = idPerfRow22+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 23) { %>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>	Power Factor - At 50% Load </strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>	Power Factor - At 50% Load </strong></td>
					<% } %>
						
						<%  int idPerfRow23 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="powerFactorAt50Load<%=idPerfRow23%>" id="powerFactorAt50Load<%=idPerfRow23%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltLoadPFPercentAt50" filter="false" />' title="Power Factor - At 50% Load" onfocus="javascript:processAutoSave('<%=idPerfRow23%>');" maxlength="15" >
						</td>
						<%	idPerfRow23 = idPerfRow23+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
	</tbody>
</table>