<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tr class="linked" style="width:1174px !important float:left;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 17; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd" style="float:left;"><strong>Flying Lead without TB</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd" style="float:left;"><strong>Flying Lead without TB</strong></td>
					<% } %>
						
						<%  int idAccRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_flyingLeadWithoutTB<%=idAccRow1%>" class="bordertd">
							<select style="width:100px;" name="flyingLeadWithoutTB<%=idAccRow1%>" id="flyingLeadWithoutTB<%=idAccRow1%>" title="Flying Lead Without TB" onfocus="javascript:processAutoSave('<%=idAccRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newmtoenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('flyingLeadWithoutTB<%=idAccRow1%>', '<bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId" filter="false" />'); </script>
						<%	idAccRow1 = idAccRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Flying Lead with TB</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Flying Lead with TB</strong></td>
					<% } %>
						
						<%  int idAccRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_flyingLeadWithTB<%=idAccRow2%>" class="bordertd">
							<select style="width:100px;" name="flyingLeadWithTB<%=idAccRow2%>" id="flyingLeadWithTB<%=idAccRow2%>" title="Flying Lead With TB" onfocus="javascript:processAutoSave('<%=idAccRow2%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newmtoenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('flyingLeadWithTB<%=idAccRow2%>', '<bean:write name="ratingObj" property="ltFlyingLeadWithTDId" filter="false" />'); </script>
						<%	idAccRow2 = idAccRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Hardware</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Hardware</strong></td>
					<% } %>
						
						<%  int idAccRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_hardware<%=idAccRow3%>" class="bordertd">
							<select style="width:100px;" name="hardware<%=idAccRow3%>" id="hardware<%=idAccRow3%>" title="Hardware" onfocus="javascript:processAutoSave('<%=idAccRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltHardwareList" >
																	<bean:define name="newmtoenquiryForm" property="ltHardwareList" id="ltHardwareList" type="java.util.Collection" />
																	<logic:iterate name="ltHardwareList" id="ltHardware" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHardware" property="key" />' > <bean:write name="ltHardware" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('hardware<%=idAccRow3%>', '<bean:write name="ratingObj" property="ltHardware" filter="false" />'); </script>
						<%	idAccRow3 = idAccRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Space Heater</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Space Heater</strong></td>
					<% } %>
						
						<%  int idAccRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_spaceHeater<%=idAccRow4%>" class="bordertd">
							<select style="width:100px;" name="spaceHeater<%=idAccRow4%>" id="spaceHeater<%=idAccRow4%>" title="Space Heater" onfocus="javascript:processAutoSave('<%=idAccRow4%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>	
						</td>
						<script> selectLstItemById('spaceHeater<%=idAccRow4%>', '<bean:write name="ratingObj" property="ltSpaceHeaterId" filter="false" />'); </script>
						<%	idAccRow4 = idAccRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Thermister</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Thermister</strong></td>
					<% } %>
						
						<%  int idAccRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_thermister<%=idAccRow5%>" class="bordertd">
							<select style="width:100px;" name="thermister<%=idAccRow5%>" id="thermister<%=idAccRow5%>" title="Thermister" onfocus="javascript:processAutoSave('<%=idAccRow5%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltThermisterList" >
																	<bean:define name="newmtoenquiryForm" property="ltThermisterList" id="ltThermisterList" type="java.util.Collection" />
																	<logic:iterate name="ltThermisterList" id="ltThermister" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltThermister" property="key" />' > <bean:write name="ltThermister" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('thermister<%=idAccRow5%>', '<bean:write name="ratingObj" property="ltThermisterId" filter="false" />'); </script>
						<%	idAccRow5 = idAccRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>RTD</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>RTD</strong></td>
					<% } %>
						
						<%  int idAccRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_rtd<%=idAccRow6%>" class="bordertd">
							<select style="width:100px;" name="rtd<%=idAccRow6%>" id="rtd<%=idAccRow6%>" title="RTD" onfocus="javascript:processAutoSave('<%=idAccRow6%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltRTDList" >
																	<bean:define name="newmtoenquiryForm" property="ltRTDList" id="ltRTDList" type="java.util.Collection" />
																	<logic:iterate name="ltRTDList" id="ltRTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltRTD" property="key" />' > <bean:write name="ltRTD" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('rtd<%=idAccRow6%>', '<bean:write name="ratingObj" property="ltRTDId" filter="false" />'); </script>
						<%	idAccRow6 = idAccRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>BTD</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>BTD</strong></td>
					<% } %>
						
						<%  int idAccRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_btd<%=idAccRow7%>" class="bordertd">
							<select style="width:100px;" name="btd<%=idAccRow7%>" id="btd<%=idAccRow7%>" title="BTD" onfocus="javascript:processAutoSave('<%=idAccRow7%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltBTDList" >
																	<bean:define name="newmtoenquiryForm" property="ltBTDList" id="ltBTDList" type="java.util.Collection" />
																	<logic:iterate name="ltBTDList" id="ltBTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBTD" property="key" />' > <bean:write name="ltBTD" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('btd<%=idAccRow7%>', '<bean:write name="ratingObj" property="ltBTDId" filter="false" />'); </script>
						<%	idAccRow7 = idAccRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spreader Box</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spreader Box</strong></td>
					<% } %>
						
						<%  int idAccRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spreaderBox<%=idAccRow8%>" id="spreaderBox<%=idAccRow8%>" title="Spreader Box" onfocus="javascript:processAutoSave('<%=idAccRow8%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spreaderBox<%=idAccRow8%>', '<bean:write name="ratingObj" property="ltSpreaderBoxId" filter="false" />'); </script>
						<%	idAccRow8 = idAccRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Cable Sealing Box</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Cable Sealing Box</strong></td>
					<% } %>
						
						<%  int idAccRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="cableSealingBox<%=idAccRow9%>" id="cableSealingBox<%=idAccRow9%>" title="Cable Sealing Box" onfocus="javascript:processAutoSave('<%=idAccRow9%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('cableSealingBox<%=idAccRow9%>', '<bean:write name="ratingObj" property="ltCableSealingBoxId" filter="false" />'); </script>
						<%	idAccRow9 = idAccRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Gland Plate</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Gland Plate</strong></td>
					<% } %>
						
						<%  int idAccRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_glandPlate<%=idAccRow10%>" class="bordertd">
							<select style="width:100px;" name="glandPlate<%=idAccRow10%>" id="glandPlate<%=idAccRow10%>" title="Gland Plate" onfocus="javascript:processAutoSave('<%=idAccRow10%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltGlandPlateList" >
																	<bean:define name="newmtoenquiryForm" property="ltGlandPlateList" id="ltGlandPlateList" type="java.util.Collection" />
																	<logic:iterate name="ltGlandPlateList" id="ltGlandPlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltGlandPlate" property="key" />' > <bean:write name="ltGlandPlate" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('glandPlate<%=idAccRow10%>', '<bean:write name="ratingObj" property="ltGlandPlateId" filter="false" />'); </script>
						<%	idAccRow10 = idAccRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Double Compression Gland</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Double Compression Gland</strong></td>
					<% } %>
						
						<%  int idAccRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="doubleCompressGland<%=idAccRow11%>" id="doubleCompressGland<%=idAccRow11%>" title="Double Compression Gland" onfocus="javascript:processAutoSave('<%=idAccRow11%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltDoubleCompressionGlandList" >
																	<bean:define name="newmtoenquiryForm" property="ltDoubleCompressionGlandList" id="ltDoubleCompressionGlandList" type="java.util.Collection" />
																	<logic:iterate name="ltDoubleCompressionGlandList" id="ltDoubleCompressionGland" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDoubleCompressionGland" property="key" />' > <bean:write name="ltDoubleCompressionGland" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('doubleCompressGland<%=idAccRow11%>', '<bean:write name="ratingObj" property="ltDoubleCompressionGlandId" filter="false" />'); </script>
						<%	idAccRow11 = idAccRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Direction Arrow Plate</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Direction Arrow Plate</strong></td>
					<% } %>
						
						<%  int idAccRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="directionArrowPlate<%=idAccRow12%>" id="directionArrowPlate<%=idAccRow12%>" title="Direction Arrow Plate" onfocus="javascript:processAutoSave('<%=idAccRow12%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('directionArrowPlate<%=idAccRow12%>', '<bean:write name="ratingObj" property="ltDirectionArrowPlateId" filter="false" />'); </script>
						<%	idAccRow12 = idAccRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Addl. Name Plate</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Addl. Name Plate</strong></td>
					<% } %>
						
						<%  int idAccRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="addlNamePlate<%=idAccRow13%>" id="addlNamePlate<%=idAccRow13%>" title="Additional Name Plate" onfocus="javascript:processAutoSave('<%=idAccRow13%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltAddNamePlateList" >
																	<bean:define name="newmtoenquiryForm" property="ltAddNamePlateList" id="ltAddNamePlateList" type="java.util.Collection" />
																	<logic:iterate name="ltAddNamePlateList" id="ltAddNamePlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAddNamePlate" property="key" />' > <bean:write name="ltAddNamePlate" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('addlNamePlate<%=idAccRow13%>', '<bean:write name="ratingObj" property="ltAddNamePlateId" filter="false" />'); </script>
						<%	idAccRow13 = idAccRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Vibration Probe Mounting Pad</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Vibration Probe Mounting Pad</strong></td>
					<% } %>
						
						<%  int idAccRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="vibrationProbeMP<%=idAccRow14%>" id="vibrationProbeMP<%=idAccRow14%>" title="Vibration Probe Mounting Pad" onfocus="javascript:processAutoSave('<%=idAccRow14%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="alTargetedList" >
									<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
									<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<%	idAccRow14 = idAccRow14+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 15) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>SPM Mounting Provision</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>SPM Mounting Provision</strong></td>
					<% } %>
						
						<%  int idAccRow15 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_spmMountingProvision<%=idAccRow15%>" class="bordertd">
							<select style="width:100px;" name="spmMountingProvision<%=idAccRow15%>" id="spmMountingProvision<%=idAccRow15%>" title="SPM Mounting Provision" onfocus="javascript:processAutoSave('<%=idAccRow15%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spmMountingProvision<%=idAccRow15%>', '<bean:write name="ratingObj" property="ltSPMMountingProvisionId" filter="false" />'); </script>
						<%	idAccRow15 = idAccRow15+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 16) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Encoder Mounting</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Encoder Mounting</strong></td>
					<% } %>
						
						<%  int idAccRow16 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_techoMounting<%=idAccRow16%>" class="bordertd">
							<select style="width:100px;" name="techoMounting<%=idAccRow16%>" id="techoMounting<%=idAccRow16%>" title="Encoder Mounting" onfocus="javascript:processAutoSave('<%=idAccRow16%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltTechoMountingList" >
																	<bean:define name="newmtoenquiryForm" property="ltTechoMountingList" id="ltTechoMountingList" type="java.util.Collection" />
																	<logic:iterate name="ltTechoMountingList" id="ltTechoMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTechoMounting" property="key" />' > <bean:write name="ltTechoMounting" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('techoMounting<%=idAccRow16%>', '<bean:write name="ratingObj" property="ltTechoMounting" filter="false" />'); </script>
						<%	idAccRow16 = idAccRow16+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 17) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Fan</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Fan</strong></td>
					<% } %>
						
						<%  int idAccRow17 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_metalFan<%=idAccRow17%>" class="bordertd">
							<select style="width:100px;" name="metalFan<%=idAccRow17%>" id="metalFan<%=idAccRow17%>" title="Fan" onfocus="javascript:processAutoSave('<%=idAccRow17%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltMetalFanList" >
																	<bean:define name="newmtoenquiryForm" property="ltMetalFanList" id="ltMetalFanList" type="java.util.Collection" />
																	<logic:iterate name="ltMetalFanList" id="ltMetalFan" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMetalFan" property="key" />' > <bean:write name="ltMetalFan" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('metalFan<%=idAccRow17%>', '<bean:write name="ratingObj" property="ltMetalFanId" filter="false" />'); </script>
						<%	idAccRow17 = idAccRow17+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
</table>