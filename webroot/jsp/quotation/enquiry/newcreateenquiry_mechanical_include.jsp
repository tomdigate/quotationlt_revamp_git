<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
	<tr class="linked" style="width:1100px !important; float:left; overflow-x: scroll;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 24; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd" style="float:left;"><strong>Shaft Type</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd" style="float:left;"><strong>Shaft Type</strong></td>
						<% } %>
						
						<%  int idMechRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="shaftType<%=idMechRow1%>" id="shaftType<%=idMechRow1%>" title="Shaft Type" onfocus="javascript:processAutoSave('<%=idMechRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltShaftTypeList" >
																	<bean:define name="newenquiryForm" property="ltShaftTypeList" id="ltShaftTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltShaftTypeList" id="ltShaftType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltShaftType" property="key" />' > <bean:write name="ltShaftType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('shaftType<%=idMechRow1%>', '<bean:write name="ratingObj" property="ltShaftTypeId" filter="false" />'); </script>
						<%	idMechRow1 = idMechRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Shaft Material</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Shaft Material</strong></td>
						<% } %>
						
						<%  int idMechRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="shaftMaterialType<%=idMechRow2%>" id="shaftMaterialType<%=idMechRow2%>" title="Shaft Material" onfocus="javascript:processAutoSave('<%=idMechRow2%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltShaftMaterialTypeList" >
																	<bean:define name="newenquiryForm" property="ltShaftMaterialTypeList" id="ltShaftMaterialTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltShaftMaterialTypeList" id="ltShaftMaterialType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltShaftMaterialType" property="key" />' > <bean:write name="ltShaftMaterialType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('shaftMaterialType<%=idMechRow2%>', '<bean:write name="ratingObj" property="ltShaftMaterialId" filter="false" />'); </script>
						<%	idMechRow2 = idMechRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Direction of Rotation</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Direction of Rotation</strong></td>
						<% } %>
						
						<%  int idMechRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="directionOfRotation<%=idMechRow3%>" id="directionOfRotation<%=idMechRow3%>" title="Direction of Rotation" onfocus="javascript:processAutoSave('<%=idMechRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltDirectionOfRotationList" >
																	<bean:define name="newenquiryForm" property="ltDirectionOfRotationList" id="ltDirectionOfRotationList" type="java.util.Collection" />
																	<logic:iterate name="ltDirectionOfRotationList" id="ltDirectionOfRotation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDirectionOfRotation" property="key" />' > <bean:write name="ltDirectionOfRotation" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('directionOfRotation<%=idMechRow3%>', '<bean:write name="ratingObj" property="ltDirectionOfRotation" filter="false" />'); </script>
						<%	idMechRow3 = idMechRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Method of Coupling</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Method of Coupling</strong></td>
						<% } %>
						
						<%  int idMechRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="methodOfCoupling<%=idMechRow4%>" id="methodOfCoupling<%=idMechRow4%>" title="Method of Coupling" onfocus="javascript:processAutoSave('<%=idMechRow4%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltMethodOfCouplingList" >
																	<bean:define name="newenquiryForm" property="ltMethodOfCouplingList" id="ltMethodOfCouplingList" type="java.util.Collection" />
																	<logic:iterate name="ltMethodOfCouplingList" id="ltMethodOfCoupling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMethodOfCoupling" property="key" />' > <bean:write name="ltMethodOfCoupling" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('methodOfCoupling<%=idMechRow4%>', '<bean:write name="ratingObj" property="ltMethodOfCoupling" filter="false" />'); </script>
						<%	idMechRow4 = idMechRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Paint Type</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Paint Type</strong></td>
						<% } %>
						
						<%  int idMechRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="paintingType<%=idMechRow5%>" id="paintingType<%=idMechRow5%>" title="Painting Type" onfocus="javascript:processAutoSave('<%=idMechRow5%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltPaintingTypeList" >
																	<bean:define name="newenquiryForm" property="ltPaintingTypeList" id="ltPaintingTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltPaintingTypeList" id="ltPaintingType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltPaintingType" property="key" />' > <bean:write name="ltPaintingType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('paintingType<%=idMechRow5%>', '<bean:write name="ratingObj" property="ltPaintingTypeId" filter="false" />'); </script>
						<%	idMechRow5 = idMechRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Paint Shade</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Paint Shade</strong></td>
						<% } %>
						
						<%  int idMechRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="paintShade<%=idMechRow6%>" id="paintShade<%=idMechRow6%>" title="Paint Shade" onfocus="javascript:processAutoSave('<%=idMechRow6%>');" onchange="javascript:processFieldsForPaintShade('<%=idMechRow6%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltPaintShadeList" >
																	<bean:define name="newenquiryForm" property="ltPaintShadeList" id="ltPaintShadeList" type="java.util.Collection" />
																	<logic:iterate name="ltPaintShadeList" id="ltPaintShade" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltPaintShade" property="key" />' > <bean:write name="ltPaintShade" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('paintShade<%=idMechRow6%>', '<bean:write name="ratingObj" property="ltPaintShadeId" filter="false" />'); </script>
						<%	idMechRow6 = idMechRow6+1; %>
						</logic:iterate>
					</tr>
					
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Non Standard Paint Shade Value</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Non Standard Paint Shade Value</strong></td>
						<% } %>
						
						<%  int idMechRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="paintShadeVal<%=idMechRow7%>" id="paintShadeVal<%=idMechRow7%>" class="readonlymini" style="width:100px;" readonly="true" value='<bean:write name="ratingObj" property="ltPaintShadeValue" filter="false" />' title="Non Standard Paint Shade Value" onfocus="javascript:processAutoSave('<%=idMechRow7%>');" maxlength="50" >
						</td>
						
						<%	idMechRow7 = idMechRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Paint Thickness</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Paint Thickness</strong></td>
						<% } %>
						
						<%  int idMechRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="paintThickness<%=idMechRow8%>" id="paintThickness<%=idMechRow8%>" title="Paint Thickness" onfocus="javascript:processAutoSave('<%=idMechRow8%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltPaintThicknessList" >
																	<bean:define name="newenquiryForm" property="ltPaintThicknessList" id="ltPaintThicknessList" type="java.util.Collection" />
																	<logic:iterate name="ltPaintThicknessList" id="ltPaintThickness" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltPaintThickness" property="key" />' > <bean:write name="ltPaintThickness" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('paintThickness<%=idMechRow8%>', '<bean:write name="ratingObj" property="ltPaintThicknessId" filter="false" />'); </script>
						<%	idMechRow8 = idMechRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>IP</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>IP</strong></td>
						<% } %>
						
						<%  int idMechRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="ip<%=idMechRow9%>" id="ip<%=idMechRow9%>" title="IP" onfocus="javascript:processAutoSave('<%=idMechRow9%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltIPList" >
																	<bean:define name="newenquiryForm" property="ltIPList" id="ltIPList" type="java.util.Collection" />
																	<logic:iterate name="ltIPList" id="ltIP" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltIP" property="key" />' > <bean:write name="ltIP" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('ip<%=idMechRow9%>', '<bean:write name="ratingObj" property="ltIPId" filter="false" />'); </script>
						<%	idMechRow9 = idMechRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Cable Size</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Cable Size</strong></td>
						<% } %>
						
						<%  int idMechRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="cableSize<%=idMechRow10%>" id="cableSize<%=idMechRow10%>" title="Cable Size" onfocus="javascript:processAutoSave('<%=idMechRow10%>');" onchange="javascript:processFieldsForCableSize('<%=idMechRow10%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltCableSizeList" >
																	<bean:define name="newenquiryForm" property="ltCableSizeList" id="ltCableSizeList" type="java.util.Collection" />
																	<logic:iterate name="ltCableSizeList" id="ltCableSize" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltCableSize" property="key" />' > <bean:write name="ltCableSize" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('cableSize<%=idMechRow10%>', '<bean:write name="ratingObj" property="ltCableSizeId" filter="false" />'); </script>
						<%	idMechRow10 = idMechRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>No. Of Runs</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>No. Of Runs</strong></td>
						<% } %>
						
						<%  int idMechRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="noOfRuns<%=idMechRow11%>" id="noOfRuns<%=idMechRow11%>" title="No. Of Runs" onfocus="javascript:processAutoSave('<%=idMechRow11%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltNoOfRunsList" >
																	<bean:define name="newenquiryForm" property="ltNoOfRunsList" id="ltNoOfRunsList" type="java.util.Collection" />
																	<logic:iterate name="ltNoOfRunsList" id="ltNoOfRuns" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltNoOfRuns" property="key" />' > <bean:write name="ltNoOfRuns" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('noOfRuns<%=idMechRow11%>', '<bean:write name="ratingObj" property="ltNoOfRuns" filter="false" />'); </script>
						<%	idMechRow11 = idMechRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>No. Of Cores</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>No. Of Cores</strong></td>
						<% } %>
						
						<%  int idMechRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="noOfCores<%=idMechRow12%>" id="noOfCores<%=idMechRow12%>" title="No. Of Cores" onfocus="javascript:processAutoSave('<%=idMechRow12%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltNoOfCoresList" >
																	<bean:define name="newenquiryForm" property="ltNoOfCoresList" id="ltNoOfCoresList" type="java.util.Collection" />
																	<logic:iterate name="ltNoOfCoresList" id="ltNoOfCores" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltNoOfCores" property="key" />' > <bean:write name="ltNoOfCores" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('noOfCores<%=idMechRow12%>', '<bean:write name="ratingObj" property="ltNoOfCores" filter="false" />'); </script>
						<%	idMechRow12 = idMechRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Cross-Sectional Area (mm2)</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Cross-Sectional Area (mm2)</strong></td>
						<% } %>
						
						<%  int idMechRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="crossSectionArea<%=idMechRow13%>" id="crossSectionArea<%=idMechRow13%>" title="Cross-Sectional Area (mm2)" onfocus="javascript:processAutoSave('<%=idMechRow13%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltCrossSectionAreaList" >
																	<bean:define name="newenquiryForm" property="ltCrossSectionAreaList" id="ltCrossSectionAreaList" type="java.util.Collection" />
																	<logic:iterate name="ltCrossSectionAreaList" id="ltCrossSectionArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltCrossSectionArea" property="key" />' > <bean:write name="ltCrossSectionArea" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('crossSectionArea<%=idMechRow13%>', '<bean:write name="ratingObj" property="ltCrossSectionAreaId" filter="false" />'); </script>
						<%	idMechRow13 = idMechRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Material of Conductor</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Material of Conductor</strong></td>
						<% } %>
						
						<%  int idMechRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="conductorMaterial<%=idMechRow14%>" id="conductorMaterial<%=idMechRow14%>" title="Material of Conductor" onfocus="javascript:processAutoSave('<%=idMechRow14%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltConductorMaterialList" >
																	<bean:define name="newenquiryForm" property="ltConductorMaterialList" id="ltConductorMaterialList" type="java.util.Collection" />
																	<logic:iterate name="ltConductorMaterialList" id="ltConductorMaterial" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltConductorMaterial" property="key" />' > <bean:write name="ltConductorMaterial" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('conductorMaterial<%=idMechRow14%>', '<bean:write name="ratingObj" property="ltConductorMaterialId" filter="false" />'); </script>
						<%	idMechRow14 = idMechRow14+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 15) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Cable Diameter</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Cable Diameter</strong></td>
						<% } %>
						
						<%  int idMechRow15 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="cableDiameter<%=idMechRow15%>" id="cableDiameter<%=idMechRow15%>" class="normal" style="width:100px;" readonly="false" value='<bean:write name="ratingObj" property="ltCableDiameter" filter="false" />' title="Cable Diameter" onfocus="javascript:processAutoSave('<%=idMechRow15%>');" maxlength="5" >
						</td>
						<%	idMechRow15 = idMechRow15+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 16) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Terminal Box Size</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Terminal Box Size</strong></td>
						<% } %>
						
						<%  int idMechRow16 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="terminalBoxSize<%=idMechRow16%>" id="terminalBoxSize<%=idMechRow16%>" title="Terminal Box Size" onfocus="javascript:processAutoSave('<%=idMechRow16%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltTerminalBoxList" >
																	<bean:define name="newenquiryForm" property="ltTerminalBoxList" id="ltTerminalBoxList" type="java.util.Collection" />
																	<logic:iterate name="ltTerminalBoxList" id="ltTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTerminalBox" property="key" />' > <bean:write name="ltTerminalBox" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('terminalBoxSize<%=idMechRow16%>', '<bean:write name="ratingObj" property="ltTerminalBoxSizeId" filter="false" />'); </script>
						<%	idMechRow16 = idMechRow16+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 17) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Spreader Box</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Spreader Box</strong></td>
						<% } %>
						
						<%  int idMechRow17 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spreaderBox<%=idMechRow17%>" id="spreaderBox<%=idMechRow17%>" title="Spreader Box" onfocus="javascript:processAutoSave('<%=idMechRow17%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('spreaderBox<%=idMechRow17%>', '<bean:write name="ratingObj" property="ltSpreaderBoxId" filter="false" />'); </script>
						<%	idMechRow17 = idMechRow17+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 18) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Space Heater</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Space Heater</strong></td>
						<% } %>
						
						<%  int idMechRow18 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spaceHeater<%=idMechRow18%>" id="spaceHeater<%=idMechRow18%>" title="Space Heater" onfocus="javascript:processAutoSave('<%=idMechRow18%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('spaceHeater<%=idMechRow18%>', '<bean:write name="ratingObj" property="ltSpaceHeaterId" filter="false" />'); </script>
						<%	idMechRow18 = idMechRow18+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 19) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Vibration</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Vibration</strong></td>
						<% } %>
						
						<%  int idMechRow19 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="vibration<%=idMechRow19%>" id="vibration<%=idMechRow19%>" title="Vibration" onfocus="javascript:processAutoSave('<%=idMechRow19%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltVibrationList" >
																	<bean:define name="newenquiryForm" property="ltVibrationList" id="ltVibrationList" type="java.util.Collection" />
																	<logic:iterate name="ltVibrationList" id="ltVibration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltVibration" property="key" />' > <bean:write name="ltVibration" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('vibration<%=idMechRow19%>', '<bean:write name="ratingObj" property="ltVibrationId" filter="false" />'); </script>
						<%	idMechRow19 = idMechRow19+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 20) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Flying Lead Without TB</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Flying Lead Without TB</strong></td>
						<% } %>
						
						<%  int idMechRow20 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="flyingLeadWithoutTB<%=idMechRow20%>" id="flyingLeadWithoutTB<%=idMechRow20%>" title="Flying Lead Without TB" onfocus="javascript:processAutoSave('<%=idMechRow20%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('flyingLeadWithoutTB<%=idMechRow20%>', '<bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId" filter="false" />'); </script>
						<%	idMechRow20 = idMechRow20+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 21) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Flying Lead With TB</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Flying Lead With TB</strong></td>
						<% } %>
						
						<%  int idMechRow21 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="flyingLeadWithTB<%=idMechRow21%>" id="flyingLeadWithTB<%=idMechRow21%>" title="Flying Lead With TB" onfocus="javascript:processAutoSave('<%=idMechRow21%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('flyingLeadWithTB<%=idMechRow21%>', '<bean:write name="ratingObj" property="ltFlyingLeadWithTDId" filter="false" />'); </script>
						<%	idMechRow21 = idMechRow21+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 22) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Fan </strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Fan </strong></td>
						<% } %>
						
						<%  int idMechRow22 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="metalFan<%=idMechRow22%>" id="metalFan<%=idMechRow22%>" title="Fan" onfocus="javascript:processAutoSave('<%=idMechRow22%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltMetalFanList" >
																	<bean:define name="newenquiryForm" property="ltMetalFanList" id="ltMetalFanList" type="java.util.Collection" />
																	<logic:iterate name="ltMetalFanList" id="ltMetalFan" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMetalFan" property="key" />' > <bean:write name="ltMetalFan" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('metalFan<%=idMechRow22%>', '<bean:write name="ratingObj" property="ltMetalFanId" filter="false" />'); </script>
						<%	idMechRow22 = idMechRow22+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 23) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Encoder Mounting</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Encoder Mounting</strong></td>
						<% } %>
						
						<%  int idMechRow23 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="techoMounting<%=idMechRow23%>" id="techoMounting<%=idMechRow23%>" title="Encoder Mounting" onfocus="javascript:processAutoSave('<%=idMechRow23%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltTechoMountingList" >
																	<bean:define name="newenquiryForm" property="ltTechoMountingList" id="ltTechoMountingList" type="java.util.Collection" />
																	<logic:iterate name="ltTechoMountingList" id="ltTechoMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTechoMounting" property="key" />' > <bean:write name="ltTechoMounting" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('techoMounting<%=idMechRow23%>', '<bean:write name="ratingObj" property="ltTechoMounting" filter="false" />'); </script>
						<%	idMechRow23 = idMechRow23+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 24) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Shaft Grounding</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Shaft Grounding</strong></td>
						<% } %>
						
						<%  int idMechRow24 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="shaftGrounding<%=idMechRow24%>" id="shaftGrounding<%=idMechRow24%>" title="Shaft Grounding" onfocus="javascript:processAutoSave('<%=idMechRow24%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
						</td>
						<script> selectLstItemById('shaftGrounding<%=idMechRow24%>', '<bean:write name="ratingObj" property="ltShaftGroundingId" filter="false" />'); </script>
						<%	idMechRow24 = idMechRow24+1; %>
						</logic:iterate>
					</tr>
				<% } %>	
			<% } %>
			</table>
		</td>
	</tr>
	</tbody>
</table>

