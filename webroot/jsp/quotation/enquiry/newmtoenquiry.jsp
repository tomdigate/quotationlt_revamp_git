<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<%
	String jsonMTOAddOnsData = (String) request.getAttribute("jsonMTOAddOns");
	jsonMTOAddOnsData = (jsonMTOAddOnsData.replaceAll("'","")).replace("\\", "\\\\");
%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<html:form action="newMTOEnquiry.do" method="POST" enctype="multipart/form-data">
	
	<html:hidden property="invoke" value="processMTOEnquiry"/>
	<html:hidden property="dispatch" />
	<html:hidden property="createdBySSO" />
	<html:hidden property="createdDate"/>
	<html:hidden property="createdBy"/>
	<html:hidden property="operationType" styleId="operationType" value="DM" />
	<html:hidden property="enquiryOperationType"/>
	<html:hidden property="currentRating"/>
	<html:hidden property="nextRatingId"/>
	<html:hidden property="enquiryNumber" />
	<html:hidden property="enquiryId" styleId="enquiryId"/>
	<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
	<html:hidden property="ratingId" styleId="ratingId"/>
	<html:hidden property="ratingNo" styleId="ratingNo"/>	
	<html:hidden property="ratingsValidationMessage" styleId="ratingsValidationMessage"/>
	<html:hidden property="assignToName" />
	<html:hidden property="statusId" styleId="statusId" />
	<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>
	<html:hidden property="mtoTotalRatingIndex" styleId="mtoTotalRatingIndex"/>
	<html:hidden property="customerId" styleId="customerId" />
	<html:hidden property="locationId" styleId="locationId"/>
	
	<input type="hidden" name="lastRatingNum" id="lastRatingNum" value='<%=(Integer)request.getSession().getAttribute("lastRatingNum") %>' >
	<input type="hidden" name="prevIdVal" id="prevIdVal" value='<%=(Integer)request.getSession().getAttribute("prevIdVal") %>' >
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
	
	<!-- ------------------------------------------------------	Top Table Section : START ------------------------------------------------------------- -->
	<table width="100%" height="auto" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td height="100%" valign="top">
			
			<!-- --------------------------------------------- Top Title ------------------------------------------------------------------------------ -->
			<br>
			<div class="sectiontitle"> <bean:message key="quotation.offerdata.label.heading"/> </div>
			
			<!-- --------------------------------------------- Status block : Attachments Block : START  ---------------------------------------------- -->
			<br>
			<table width="100%" border="0" cellpadding="0" cellspacing="3">
				<tr>
            		<td rowspan="2" style="width:40% ">
            		
            		<!-- -- Status Block -- -->
					<table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
            			<tr>
            				<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
              				<td class="other">
					        	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newmtoenquiryForm" property="enquiryNumber"/><br>
					            <bean:message key="quotation.viewenquiry.label.subheading2"/> 
                			</td>
            			</tr>
            			<tr>
            				<td class="other">
            					<table width="100%">
									<tr>
										<td height="16" class="section-head" align="center"><bean:message key="quotation.viewenquiry.label.status" /> <bean:write name="newmtoenquiryForm" property="statusName" /></td>
									</tr>
									<logic:equal name="newmtoenquiryForm" property="enquiryOperationType" value="engineer">
										<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.home.colheading.assignto" /> :&nbsp; <bean:write name="newmtoenquiryForm" property="designEngineerName" /></td>
										</tr>
										<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.create.label.salesrepresentative" /> :&nbsp; <bean:write name="newmtoenquiryForm" property="sm_name" /> </td>
										</tr>
									</logic:equal>
									<logic:equal name="newmtoenquiryForm" property="enquiryOperationType" value="manager">
										<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.home.colheading.assignto" /> :&nbsp; <bean:write name="newmtoenquiryForm" property="createdBy" /></td>
										</tr>
										<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.create.label.salesrepresentative" /> :&nbsp; <bean:write name="newmtoenquiryForm" property="sm_name" /> </td>
										</tr>
									</logic:equal>
								</table>
            					<br>
            				</td>
            			</tr>
					</table>
            		<!-- -- Status Block -- -->
            		</td>
            		
            		<logic:lessThan name="newmtoenquiryForm" property="statusId" value="6"> 
            		<td class="other" align="right" colspan="2">
            			<bean:message key="quotation.attachments.message"/>
				    	<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="newmtoenquiryForm" property="enquiryId"/>','<bean:write name="newmtoenquiryForm" property="createdBySSO"/>');">
				    		<img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right">
				    	</a>
            		</td>
            		</logic:lessThan>
            	</tr>
            	
            	<!-- -- Attachments Block -- -->
            	<tr>
            		<td colspan="2" style="width:45% ">
				    	<logic:present name="attachmentList" scope="request">
					    	<logic:notEmpty name="attachmentList">
						        <fieldset>
						        	<table width="100%"  cellpadding="0" cellspacing="0" border="0" class="other">
						            	<logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
										<tr>
											<td>
											<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
												<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
											</a>
											</td>
										</tr>					
										</logic:iterate>
						        	</table>
						    	</fieldset>
						    </logic:notEmpty>
					    </logic:present>
				    </td>
            	</tr>
            	<tr>
            		<td colspan="2" align="right" > 
			        	<!-- DM Uploading Files  -->
				    	<logic:lessThan name="newmtoenquiryForm" property="statusId" value="6">
							<table>
								<tr>
									<td class="other" align="right"><bean:message key="quotation.attachments.dmuploadedmessage" /></td>
									<td class="other" align="left">
									<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID))%>','<bean:write name="newmtoenquiryForm" property="enquiryId"/>', '<bean:write name="newmtoenquiryForm" property="createdBySSO"/>');">
										<img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right">
									</a>
									</td>
								</tr>
							</table>
						</logic:lessThan>
					</td>
				</tr>
			    <tr>
					<td>&nbsp;</td>
					<td colspan="2" align="right" style="width: 45%">
						<logic:present name="dmattachmentList" scope="request">
							<logic:notEmpty name="dmattachmentList">
								<fieldset>
									<table width="100%" class="other">
										<logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<tr>
											<td>
											<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
												<bean:write name="attachmentData" property="value" />.<bean:write name="attachmentData" property="value2" />
											</a>
											</td>
										</tr>
										</logic:iterate>
									</table>
								</fieldset>
							</logic:notEmpty>
						</logic:present>
					</td>
				</tr>
				<!-- -- Attachments Block -- -->
				
			</table>
			<!-- --------------------------------------------- Status block : Attachments Block : END  ------------------------------------------------ -->
			
			<!-- --------------------------------------------- Request Info block : Re-Assign Block : START  ------------------------------------------ -->
			<br>
			<div style="clear: both; margin-bottom: 30px; float: left; width: 100%;">
				<div style="width: 100%; float: left;">
					<table border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<!-- -- Request Info Block -- -->
								<fieldset>
									<legend class="blueheader"> <bean:message key="quotation.viewenquiry.label.request.heading" /> </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.customer" /></td>
											<td class="normal">
												<input id="customerName" name="customerName" type="text" size="50" class="readonlymini" readonly="true" autocomplete="off" value="<bean:write name="newmtoenquiryForm" property="customerName"/>" />
												<div class="note hidetable" id="invalidcustomer" style="text-align: left; padding-left: 20px">Invalid Customer Name </div> 
												<html:hidden property="customerId" styleId="customerId"/>
			                     				<html:hidden property="customerType" styleId="customerType"/>
											</td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td class="normal"><bean:message key="quotation.create.label.deptauth" /></td>
											<td class="normal"><bean:write name="newmtoenquiryForm" property="deptAndAuth" /></td>
										</tr>
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.enduser" /></td>
											<td><input id="endUser" name="endUser" type="text" class="readonlymini" readonly="true" maxlength="20" value='<bean:write name="newmtoenquiryForm" property="endUser" filter="false" />' /></td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td class="normal"> Location </td>
											<td>
												<bean:define name="newmtoenquiryForm" property="customerLocationList" id="customerLocationList" type="java.util.Collection" />
												<html:select name="newmtoenquiryForm" property="location" style="width:170px; pointer-events:none; font-weight:normal !important; BACKGROUND-COLOR: #e2e2e2;">
			                    					<option value="0"><bean:message key="quotation.option.select"/></option>
			                    					<html:options name="newmtoenquiryForm" collection="customerLocationList" property="key" labelProperty="value" />
			                    				</html:select>
			                    			</td>
										</tr>
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.enduserindustry" /></td>
											<td>
												<bean:define name="newmtoenquiryForm" property="alIndustryList" id="alIndustryList" type="java.util.Collection" /> 
						                    	<html:select name="newmtoenquiryForm" property="endUserIndustry" style="width:170px; pointer-events:none; font-weight:normal !important; BACKGROUND-COLOR: #e2e2e2;">
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newmtoenquiryForm" collection="alIndustryList" property="key" labelProperty="value" />
						                        </html:select>
											</td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td class="normal"><bean:message key="quotation.create.label.projectname" /></td>
											<td><input id="projectName" name="projectName" class="readonlymini" readonly="true" maxlength="30" value='<bean:write name="newmtoenquiryForm" property="projectName" filter="false" />' /></td>
										</tr>
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.revision" /></td>
											<td><input id="revision" name="revision" class="readonlymini" readonly="true" maxlength="5" value='<bean:write name="newmtoenquiryForm" property="revisionNumber" filter="false" />' /></td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td class="normal"><bean:message key="quotation.create.label.enquiryreferencenumber" /></td>
											<td><input id="designReferenceNumber" name="designReferenceNumber"class="normal" maxlength="30" value='<bean:write name="newmtoenquiryForm" property="designReferenceNumber" filter="false" />' /></td>
										</tr>
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.issuedate" /></td>
											<td>
												<input id="lastModifiedDate" name="lastModifiedDate" class="readonlymini" readonly="true" maxlength="30" />
												<img src="html/images/popupCalen.gif" class="hand" width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('lastModifiedDate', '%m/%d/%Y', '24', true);" />
											</td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td class="normal">Quotation Required By Date</td>
											<td>
												<input id="submissionDate" name="submissionDate" class="readonlymini" readonly="true" maxlength="30" />
												<img src="html/images/popupCalen.gif" class="hand" width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('lastModifiedDate', '%m/%d/%Y', '24', true);" />
											</td>
										</tr>
										<tr>
											<td class="normal"><bean:message key="quotation.create.label.savingindent" /></td>
											<td><input id="savingIndent" name="savingIndent" class="readonlymini" readonly="true" maxlength="30" /></td>
											<td class="dividingdots">&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="6"> &nbsp; </td>
										</tr>
									</table>
								</fieldset>
								<!-- -- Request Info Block -- -->
							</td>
							
							<td valign="top">
								<!-- -- Re-assign to Block -- -->
								<fieldset>
									<legend class="blueheader"> <bean:message key="quotation.offer.label.reassign" /> </legend>
									<table style="background-color: #FFFFFF">
										<tr>
											<td><bean:message key="quotation.offer.label.reassignto" /></td>
											<td>
												<select id="reassignToId" name="reassignToId" style="width:370px;">
													<option value="0"><bean:message key="quotation.option.select" /></option>
													<logic:present property="mtoReassignUsersList" name="newmtoenquiryForm">
														<bean:define name="newmtoenquiryForm" property="mtoReassignUsersList" id="mtoReassignUsersList" type="java.util.Collection" />
														<logic:iterate name="mtoReassignUsersList" id="mtoReassignUser" type="in.com.rbc.quotation.common.vo.KeyValueVo">
															<option value='<bean:write name="mtoReassignUser" property="key" />' > <bean:write name="mtoReassignUser" property="value" /> </option>
														</logic:iterate>
													</logic:present>
												</select>
											</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.offer.label.comments" /></td>
											<td><textarea style="width: 370px; height: 55px" name="reassignRemarks" id="reassignRemarks" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" > <bean:write name="newmtoenquiryForm"  property="reassignRemarks" filter="false" /> </textarea> </td>
										</tr>
									</table>
									<div class="blue-light" align="right">
										<button type="button" style="font-weight:bold; font-size:11px; color: #990000;" onclick="javascript:reassignTo();">Re-assign</button>
									</div>
								</fieldset>
								<!-- -- Re-assign to Block -- -->
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!-- --------------------------------------------- Request Info block : Re-Assign Block : END  -------------------------------------------- -->
			
			<!-- --------------------------------------------- Design(MTO) Details Section : START  --------------------------------------------------- -->
				<%@ include file="newmtoenquiry_mtodetails_include.jsp" %>
			<!-- --------------------------------------------- Design(MTO) Details Section : END  ----------------------------------------------------- -->
			
			<br><br>
			
			<!-- --------------------------------------------- Complete Ratings Section : START  ------------------------------------------------------ -->
				<%@ include file="newmtoenquiry_ratings_complete_include.jsp"%>
			<!-- --------------------------------------------- Complete Ratings Section : START  ------------------------------------------------------ -->
			
			<br>
			
			<!-- --------------------------------------------- Comments Section : START  -------------------------------------------------------------- -->
			
			<div style="width:100%; float:left; margin-bottom:30px;">
				<fieldset>
					<legend class="blueheader">	Comments and Deviations (External)</legend>
					<div>
						<table  border="0" cellspacing="0" cellpadding="0">
							<tr><td>&nbsp;</td></tr>
							<tr style="float:left; width:100%;">				  
								<td>
									<html:textarea property="commentsDeviations" styleId="commentsDeviations" style="width:1055px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newmtoenquiryForm" property="commentsDeviations" /> </html:textarea>
								</td>
							</tr>
						</table>
					</div>
				</fieldset>
			</div>
			<!-- --------------------------------------------- Comments Section : END  ---------------------------------------------------------------- -->
			
			<br>
			
			<!-- --------------------------------------------- Notes Section : START  ----------------------------------------------------------------- -->
			
			<div style="width:100%; float:left; margin-bottom:30px;">
				<fieldset>
					<legend class="blueheader">	Note Section (Internal) </legend>
					<div>
						<table  border="0" cellspacing="0" cellpadding="0">
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">				  
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By DM</td>
								<logic:equal name="newmtoenquiryForm" property="statusId" value="2">
								<td colspan="3">
									<html:textarea property="designNote" styleId="designNote" style="width:1055px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newmtoenquiryForm" property="designNote" /> </html:textarea> 
								</td>
								</logic:equal>
								<logic:notEqual name="newmtoenquiryForm" property="statusId" value="2">
								<td colspan="3">
									<html:textarea property="designNote" styleId="designNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="designNote" /> </html:textarea>
								</td>
								</logic:notEqual>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By QA</td>
								<logic:equal name="newmtoenquiryForm" property="statusId" value="17">
								<td colspan="3">
									<html:textarea property="qaNote" styleId="qaNote" style="width:1055px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newmtoenquiryForm" property="qaNote" /> </html:textarea> 
								</td>
								</logic:equal>
								<logic:notEqual name="newmtoenquiryForm" property="statusId" value="17">
								<td colspan="3">
									<html:textarea property="qaNote" styleId="qaNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="qaNote" /> </html:textarea>
								</td>
								</logic:notEqual>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By Sourcing</td>
								<logic:equal name="newmtoenquiryForm" property="statusId" value="18">
								<td colspan="3">
									<html:textarea property="scmNote" styleId="scmNote" style="width:1055px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newmtoenquiryForm" property="scmNote" /> </html:textarea> 
								</td>
								</logic:equal>
								<logic:notEqual name="newmtoenquiryForm" property="statusId" value="18">
								<td colspan="3">
									<html:textarea property="scmNote" styleId="scmNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="scmNote" /> </html:textarea>
								</td>
								</logic:notEqual>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">				  
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By Mfg. Plant</td>
								<td colspan="3">
									<html:textarea property="mfgNote" styleId="mfgNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="mfgNote" /> </html:textarea>
								</td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">				  
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By SM</td>
								<td colspan="3">
									<html:textarea property="smNote" styleId="smNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="smNote" /> </html:textarea>
								</td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By RSM</td>
								<td colspan="3">
									<html:textarea  property="rsmNote" styleId="rsmNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="rsmNote" /> </html:textarea>         
								</td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By NSM</td>
								<td colspan="3">
									<html:textarea  property="nsmNote" styleId="nsmNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="nsmNote" /> </html:textarea>         
								</td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By MH</td>
								<td colspan="3">
									<html:textarea property="mhNote" styleId="mhNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="mhNote" /> </html:textarea>         
								</td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr style="float:left; width:100%;">
								<td class="formLabelrating" style="width:138px; margin-right:33px;"> Note By BH</td>
								<td colspan="3">
									<html:textarea property="bhNote" styleId="bhNote" style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" > <bean:write name="newmtoenquiryForm" property="bhNote" /> </html:textarea>         
								</td>
							</tr>
						</table>
					</div>
				</fieldset>
			</div>
			<!-- --------------------------------------------- Notes Section : END  ------------------------------------------------------------------- -->
			
			<br><br>
			
			<!-- --------------------------------------------- Buttons Section : START  --------------------------------------------------------------- -->
			<div class="blue-light" align="right">
				<logic:equal name="newmtoenquiryForm" property="statusId" value="2">
					<html:button property="" styleClass="BUTTON" value="Regret Enquiry" onclick="javascript:regretMTOEnquiry();"/>
					<html:button property="" styleClass="BUTTON" value="Save" onclick="javascript:saveMTOEnquiry();"/>
					<html:button property="" styleClass="BUTTON" value="Return to Sales Manager" onclick="javascript:returnMTOEnquiry();"/>
					<html:button property="" styleClass="BUTTON" value="Submit to Sales Manager" onclick="javascript:submitMTOEnquiry('DM');"/>
				</logic:equal>
				<logic:equal name="newmtoenquiryForm" property="statusId" value="17">
					<html:button property="" styleClass="BUTTON" value="Save" onclick="javascript:saveMTOEnquiry();"/>
					<logic:equal name="newmtoenquiryForm" property="isDesignCompleteCheck" value="Y">
						<html:button property="" styleClass="BUTTON" value="Submit to Sales Manager" onclick="javascript:submitMTOEnquiry('QA');"/>
					</logic:equal>
					<logic:notEqual name="newmtoenquiryForm" property="isDesignCompleteCheck" value="Y">
						<html:button property="" styleClass="BUTTON_READONLY" value="Submit to Sales Manager" onclick="javascript:submitMTOEnquiry('QA');" disabled="true" />
					</logic:notEqual>
				</logic:equal>
				<logic:equal name="newmtoenquiryForm" property="statusId" value="18">
					<html:button property="" styleClass="BUTTON" value="Save" onclick="javascript:saveMTOEnquiry();"/>
					<logic:equal name="newmtoenquiryForm" property="isDesignCompleteCheck" value="Y">
						<html:button property="" styleClass="BUTTON" value="Submit to Sales Manager" onclick="javascript:submitMTOEnquiry('SCM');"/>
					</logic:equal>
					<logic:notEqual name="newmtoenquiryForm" property="isDesignCompleteCheck" value="Y">
						<html:button property="" styleClass="BUTTON_READONLY" value="Submit to Sales Manager" onclick="javascript:submitMTOEnquiry('SCM');" disabled="true" />
					</logic:notEqual>
					
				</logic:equal>
			</div>
			<!-- --------------------------------------------- Buttons Section : END  ----------------------------------------------------------------- -->
			
			</td>
		</tr>
	</table>
	<!-- ------------------------------------------------------	Top Table Section : END --------------------------------------------------------------- -->
	
</html:form>

<div id="screen"></div>

<div id="modal">
	<img src="html/images/loading_big.gif" alt="" />
</div>

<script type="text/javascript">

var jsonMTOAddOnsJS = <%=jsonMTOAddOnsData%>;


$(document).ready(function() {
	
	//$('#screen, #modal').hide();
	
	/*loadLocation();
	var locationIdVal = $('#locationId').val();
	if(locationIdVal != null && locationIdVal != '') {
		selectLstItemById('location', locationIdVal);
	}*/
		
	$('.ratings-table tbody').scroll(function(e) { //detect a scroll event on the tbody
	    $('.ratings-table thead').css("left", -$(".ratings-table tbody").scrollLeft()); //fix the thead relative to the body scrolling
	    $('.ratings-table thead th').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first cell of the header
	    $('.ratings-table tbody td:first-child').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first column of tdbody
	});
	
	$('#ratingsEncloseDiv').find('.accordion-toggle').click(function() {
		//Expand or collapse this panel
		$(this).next().slideToggle('fast');
		var ii = $(this).find('.icon');

		$('.icon').not(ii).addClass('icon-plus');
		ii.toggleClass('icon-plus');

		//Hide the other panels
		$(".accordion-content").not($(this).next()).slideUp('fast');
	});
	
	$('#bottomfields_mto_section').on('scroll', function () {
	    $('#topfields_mto_section').scrollLeft($(this).scrollLeft());
	});
		
	document.getElementById("basefields_mto_section").style.display = "block";
	document.getElementById("electricalfields_mto_section").style.display = "none";
	document.getElementById("mechanicalfields_mto_section").style.display = "none";
	document.getElementById("perffields_mto_section").style.display = "none";
	document.getElementById("accessoriesfields_mto_section").style.display = "none";
	document.getElementById("certificatefields_mto_section").style.display = "none";
	
	var totalRatingIdx = $('#totalRatingIndex').val();
	for(i=1; i<=totalRatingIdx; i++) {
		document.addEventListener('DOMContentLoaded', function(){ 
			autosize(document.querySelectorAll('#additionalComments'+i));
		}, false);
		
		processFieldDisplayOptions(i);
	}
		
	var old_title;
	$('.title-styled').on('mouseover', function(){
		old_title = $(this).attr('title');
		$('.title-message').html($(this).attr('title')).css('visibility', 'visible');
		$(this).attr('title', '');
	});
	  
	$('.title-styled').on('mouseleave', function(){
		$(this).attr('title', old_title);
		$('.title-message').html('').css('visibility', 'hidden');
	});
	  
});

window.onload = function(){  
	processMTOHighlightFlagValues();
}

var subCatContainer = $(".linked");
subCatContainer.scroll(function() {
    subCatContainer.scrollLeft($(this).scrollLeft());
});

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    //sticky_relocate();
});

</script>

<script type="text/javascript" src="html/js/quotation_lt_mto.js"></script>

<style>
	.linked {
  		width: 100%;
   		overflow: hidden;
	}
	#sticky {
   		background: #fff;
	}
	#sticky.stick {
	    position: fixed;
	    top: 0;
	    z-index: 10000;
	}
	/*div.sticky {
		position: -webkit-sticky;
  		position: sticky;
  		top: 0;
  		padding: 5px;
  		border: 2px solid #4C4EAF;
	}*/
	.ratingsdiv{
		overflow:hidden;
	}
	.newFieldsMargin {
		width:127px;
		float:left;
	}
	.newRatingLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 1.6px 1.5px 1.5px 1.5px !important;
	}
	.newRatingFields tr td {
		padding : 2.5px !important;
	}
	.newRatingSparesLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 2px !important;
	}
	.newRatingSparesFields tr td {
		padding : 2.5px !important;
	}
	.lablebox-td {
		width:33% !important;
		text-align: left;
		font-weight: normal;
		font-size: 10px;
		font-family: Verdana, Arial, Helvetica, sans-serif;
		padding: 2px;
		background-color: #F2F2FF;
	}
	select {
		border: 1px solid #ccc;
		color: #555;
	}
	.background-bg-box{
		background: #e0e0e0;
	}
	/* specific customizations for the data-list */
	div.data-list-input {
		position: relative;
		height: 20px;	
		display: inline-flex;
	}
	select.data-list-input
	{
		position: absolute;	
		height: 20px;
		margin-bottom: 0;
	}
	input.data-list-input
	{
		position: absolute;
		top: 0px;
		left: 0px;
		height: 19px;
	}
	.width-select{
		width:100% !important;;
	}
	.input-class{
		width:87% !important;
		padding:4px 6px;
		border-width:1px;
		margin:0;
	}
</style>