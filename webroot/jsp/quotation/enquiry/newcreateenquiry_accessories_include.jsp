<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
	<tr class="linked" style="width:1100px !important; float:left; overflow-x: scroll;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 11; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd" style="float:left;"><strong>Hardware</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd" style="float:left;"><strong>Hardware</strong></td>
						<% } %>
						
						<%  int idAccRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="hardware<%=idAccRow1%>" id="hardware<%=idAccRow1%>" title="Hardware" onfocus="javascript:processAutoSave('<%=idAccRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltHardwareList" >
																	<bean:define name="newenquiryForm" property="ltHardwareList" id="ltHardwareList" type="java.util.Collection" />
																	<logic:iterate name="ltHardwareList" id="ltHardware" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHardware" property="key" />' > <bean:write name="ltHardware" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('hardware<%=idAccRow1%>', '<bean:write name="ratingObj" property="ltHardware" filter="false" />'); </script>
						<%	idAccRow1 = idAccRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Gland Plate</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Gland Plate</strong></td>
						<% } %>
						
						<%  int idAccRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="glandPlate<%=idAccRow2%>" id="glandPlate<%=idAccRow2%>" title="Gland Plate" onfocus="javascript:processAutoSave('<%=idAccRow2%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltGlandPlateList" >
																	<bean:define name="newenquiryForm" property="ltGlandPlateList" id="ltGlandPlateList" type="java.util.Collection" />
																	<logic:iterate name="ltGlandPlateList" id="ltGlandPlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltGlandPlate" property="key" />' > <bean:write name="ltGlandPlate" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('glandPlate<%=idAccRow2%>', '<bean:write name="ratingObj" property="ltGlandPlateId" filter="false" />'); </script>
						<%	idAccRow2 = idAccRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<td width="200px" class="dark bordertd"><strong>Double Compression Gland</strong></td>
						<% } else { %>
							<td width="200px" class="light bordertd"><strong>Double Compression Gland</strong></td>
						<% } %>
						
						<%  int idAccRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="doubleCompressGland<%=idAccRow3%>" id="doubleCompressGland<%=idAccRow3%>" title="Double Compression Gland" onfocus="javascript:processAutoSave('<%=idAccRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltDoubleCompressionGlandList" >
																	<bean:define name="newenquiryForm" property="ltDoubleCompressionGlandList" id="ltDoubleCompressionGlandList" type="java.util.Collection" />
																	<logic:iterate name="ltDoubleCompressionGlandList" id="ltDoubleCompressionGland" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDoubleCompressionGland" property="key" />' > <bean:write name="ltDoubleCompressionGland" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('doubleCompressGland<%=idAccRow3%>', '<bean:write name="ratingObj" property="ltDoubleCompressionGlandId" filter="false" />'); </script>
						<%	idAccRow3 = idAccRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>SPM Mounting Provision</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>SPM Mounting Provision</strong></td>
						<% } %>
						
						<%  int idAccRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spmMountingProvision<%=idAccRow4%>" id="spmMountingProvision<%=idAccRow4%>" title="SPM Mounting Provision" onfocus="javascript:processAutoSave('<%=idAccRow4%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spmMountingProvision<%=idAccRow4%>', '<bean:write name="ratingObj" property="ltSPMMountingProvisionId" filter="false" />'); </script>
						<%	idAccRow4 = idAccRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Addl. Name Plate</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Addl. Name Plate</strong></td>
						<% } %>
						
						<%  int idAccRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="addlNamePlate<%=idAccRow5%>" id="addlNamePlate<%=idAccRow5%>" title="Additional Name Plate" onfocus="javascript:processAutoSave('<%=idAccRow5%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltAddNamePlateList" >
																	<bean:define name="newenquiryForm" property="ltAddNamePlateList" id="ltAddNamePlateList" type="java.util.Collection" />
																	<logic:iterate name="ltAddNamePlateList" id="ltAddNamePlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAddNamePlate" property="key" />' > <bean:write name="ltAddNamePlate" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('addlNamePlate<%=idAccRow5%>', '<bean:write name="ratingObj" property="ltAddNamePlateId" filter="false" />'); </script>
						<%	idAccRow5 = idAccRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Direction Arrow Plate</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Direction Arrow Plate</strong></td>
						<% } %>
						
						<%  int idAccRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="directionArrowPlate<%=idAccRow6%>" id="directionArrowPlate<%=idAccRow6%>" title="Direction Arrow Plate" onfocus="javascript:processAutoSave('<%=idAccRow6%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('directionArrowPlate<%=idAccRow6%>', '<bean:write name="ratingObj" property="ltDirectionArrowPlateId" filter="false" />'); </script>
						<%	idAccRow6 = idAccRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>RTD</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>RTD</strong></td>
						<% } %>
						
						<%  int idAccRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="rtd<%=idAccRow7%>" id="rtd<%=idAccRow7%>" title="RTD" onfocus="javascript:processAutoSave('<%=idAccRow7%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltRTDList" >
																	<bean:define name="newenquiryForm" property="ltRTDList" id="ltRTDList" type="java.util.Collection" />
																	<logic:iterate name="ltRTDList" id="ltRTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltRTD" property="key" />' > <bean:write name="ltRTD" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('rtd<%=idAccRow7%>', '<bean:write name="ratingObj" property="ltRTDId" filter="false" />'); </script>
						<%	idAccRow7 = idAccRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>BTD</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>BTD</strong></td>
						<% } %>
						
						<%  int idAccRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="btd<%=idAccRow8%>" id="btd<%=idAccRow8%>" title="BTD" onfocus="javascript:processAutoSave('<%=idAccRow8%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltBTDList" >
																	<bean:define name="newenquiryForm" property="ltBTDList" id="ltBTDList" type="java.util.Collection" />
																	<logic:iterate name="ltBTDList" id="ltBTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBTD" property="key" />' > <bean:write name="ltBTD" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('btd<%=idAccRow8%>', '<bean:write name="ratingObj" property="ltBTDId" filter="false" />'); </script>
						<%	idAccRow8 = idAccRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Thermister</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Thermister</strong></td>
						<% } %>
						
						<%  int idAccRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="thermister<%=idAccRow9%>" id="thermister<%=idAccRow9%>" title="Thermister" onfocus="javascript:processAutoSave('<%=idAccRow9%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltThermisterList" >
																	<bean:define name="newenquiryForm" property="ltThermisterList" id="ltThermisterList" type="java.util.Collection" />
																	<logic:iterate name="ltThermisterList" id="ltThermister" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltThermister" property="key" />' > <bean:write name="ltThermister" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('thermister<%=idAccRow9%>', '<bean:write name="ratingObj" property="ltThermisterId" filter="false" />'); </script>
						<%	idAccRow9 = idAccRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Aux Terminal Box</strong></td>
					<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Aux Terminal Box</strong></td>
					<% } %>
						
						<%  int idAccRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="auxTerminalBox<%=idAccRow10%>" id="auxTerminalBox<%=idAccRow10%>" title="Aux. Terminal Box" onfocus="javascript:processAutoSave('<%=idAccRow10%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltAuxTerminalBoxList" >
																	<bean:define name="newenquiryForm" property="ltAuxTerminalBoxList" id="ltAuxTerminalBoxList" type="java.util.Collection" />
																	<logic:iterate name="ltAuxTerminalBoxList" id="ltAuxTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAuxTerminalBox" property="key" />' > <bean:write name="ltAuxTerminalBox" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('auxTerminalBox<%=idAccRow10%>', '<bean:write name="ratingObj" property="ltAuxTerminalBoxId" filter="false" />'); </script>
						<%	idAccRow10 = idAccRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Cable Sealing Box</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Cable Sealing Box</strong></td>
						<% } %>
						
						<%  int idAccRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="cableSealingBox<%=idAccRow11%>" id="cableSealingBox<%=idAccRow11%>" title="Cable Sealing Box" onfocus="javascript:processAutoSave('<%=idAccRow11%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('cableSealingBox<%=idAccRow11%>', '<bean:write name="ratingObj" property="ltCableSealingBoxId" filter="false" />'); </script>
						<%	idAccRow11 = idAccRow11+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
	</tbody>	
</table>

