<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>
<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<%
	DecimalFormat df = new DecimalFormat("###,###");
	DecimalFormat df1 = new DecimalFormat("#0");
%>

<html:form action="newPlaceAnEnquiry.do" method="POST">

<html:hidden property="invoke" value="createEnquiry"/>
<html:hidden property="operationType"/>
<html:hidden property="enquiryId" styleId="enquiryId"/>
<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
<html:hidden property="ratingsValidationMessage"/>
<html:hidden property="dispatch" value="viewSubmit"/>
<html:hidden property="customerId" styleId="customerId"/>
<html:hidden property="customerType" styleId="customerType"/>
<html:hidden property="locationId" styleId="locationId"/>
<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>

<input type="hidden" name="lastRatingNum" id="lastRatingNum" value='<%=(Integer)request.getSession().getAttribute("lastRatingNum") %>' >
<input type="hidden" name="prevIdVal" id="prevIdVal" value='<%=(Integer)request.getSession().getAttribute("prevIdVal") %>' >
<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
   
   <table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
 				<div class="sectiontitle"> Edit Enquiry - Pending Mfg. Plant Details </div>	
			</td>
		</tr>
		
		<tr>
			<td height="100%" valign="top">
				<!-------------------------- Top Title and Status & Note block Start  --------------------------------->
				<table width="100%" border="0" cellspacing="1" cellpadding="0">
					<tr>
						<td rowspan="2"><table width="98%" border="0" cellspacing="2"
								cellpadding="0" class="DotsTable">
								<tr>
									<td rowspan="2" style="width: 70px"><img
										src="html/images/icon_quote.gif" width="52" height="50"></td>
									<td class="other"><bean:message
											key="quotation.viewenquiry.label.subheading1" /> <bean:write
											name="newenquiryForm" property="enquiryNumber" /><br> <bean:message
											key="quotation.viewenquiry.label.subheading2" /></td>
								</tr>
								<tr>
									<td class="other"><table width="100%">
											<tr>
												<td height="16" class="section-head" align="center"><bean:message
														key="quotation.viewenquiry.label.status" /> <bean:write
														name="newenquiryForm" property="statusName" /></td>
											</tr>
										</table></td>
								</tr>
							</table></td>
						<logic:lessThan name="newenquiryForm" property="statusId"
							value="6">
							<td align="right" class="other">
								<table>
									<tr>
										<td><bean:message key="quotation.attachments.message" />
										</td>
										<td><a
											href="javascript:manageAttachments('<%=CommonUtility.getIntValue(
							PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img
												src="html/images/ico_attach.gif" alt="Manage Attachments"
												border="0" align="right"> </a></td>
									</tr>
								</table>
							</td>
						</logic:lessThan>
					</tr>
					<tr>
						<td style="width: 45%"><logic:present name="attachmentList"
								scope="request">
								<logic:notEmpty name="attachmentList">
									<fieldset>
										<table width="100%" class="other">
											<logic:iterate name="attachmentList" id="attachmentData"
												indexId="idx"
												type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<tr>
													<td><a
														href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
															<bean:write name="attachmentData" property="value" />.<bean:write
																name="attachmentData" property="value2" />
													</a></td>
												</tr>
											</logic:iterate>
										</table>
									</fieldset>
								</logic:notEmpty>
							</logic:present></td>
					</tr>
					<logic:present name="attachmentMessage" scope="request">
						<tr>
							<td><bean:write name="attachmentMessage" /></td>
						</tr>
					</logic:present>
				</table> <br>
				<br> <br> 
				
				<div class="note-red" style="font-weight:bold !important;" align="right"> All fields marked &nbsp;&nbsp; <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> &nbsp;&nbsp; indicates mandatory fields. </div>
			
				<!----------------------- Request Information : Block Start ------------------------------------>
				
				<fieldset>
					<legend class="blueheader"> <bean:message key="quotation.create.label.requestinfo" /> </legend>
					
					<table width="100%">
						<tr>
							<td style="vertical-align:top">
								<table width="100%">
									<tr>
										<td colspan="6"><label class="blue-light"style="font-weight: bold; width: 100%;"> Enquiry Information </label></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.create.label.customerenqreference"/></th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="customerEnqReference" /></td>
										<td class="dividingdots" align="center" width="2%" height="100%" rowspan="5"></td>
						  				<td align="center" width="2%" height="100%" rowspan="5">&nbsp;</td>
						  				<th class="label-text" style="width:120px; line-height:20px;"> <bean:message key="quotation.label.is.marathon.approved"/></th>
						  				<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="is_marathon_approved" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.create.label.customer" /> <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:170px"> <bean:write name="newenquiryForm" property="customerName" /> </td>
										
										<th class="label-text" style="width:120px; line-height:20px;">Consultant</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="consultantName" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.label.admin.customer.location" /> </th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="location" /></td>
										
										<th class="label-text" style="width:120px; line-height:20px;">End user</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="endUser" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.label.concerned.person" /> <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:170px">
											<bean:write name="newenquiryForm" property="concernedPerson" />
											<html:hidden property="concernedPerson" styleId="concernedPerson"/>
										</td>
										
										<th class="label-text" style="width:120px; line-height:20px;">End User Industry</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="endUserIndustry" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;">Project Name</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="projectName" /></td>
										
										<th class="label-text" style="width:120px; line-height:20px;">Location of Installation</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="locationOfInstallation" /></td>
									</tr>
								</table>
							</td>
							<td class="dividingdots" width="2%" height="100%" align="center" style="padding:2px;"></td> 
					  		<td width="2%" height="100%" align="center" style="padding:2px;">&nbsp;</td>
							
							<td style="vertical-align:top">
								<table width="100%">
									<tr>
										<td colspan="2"><label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> Enquiry Weightage </label></td>
									</tr>
									<tr>
										<th class="label-text">Enquiry Type <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="enquiryType" />
											<html:hidden property="enquiryType" styleId="enquiryType"/>
										</td>
									</tr>
									<tr>
										<th class="label-text">Winning Chance <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="winningChance" />
											<html:hidden property="winningChance" styleId="winningChance"/>
										</td>
									</tr>
									<tr>
										<th class="label-text">Targeted <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="targeted" />
											<html:hidden property="targeted" styleId="targeted"/>
										</td>
									</tr>
									
									<tr>
				                     	<td colspan="2"><label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> <bean:message key="quotation.label.important.dates"/> </label></td>				  
						  			</tr>
						  			
						  			<tr>
										<th class="label-text" style="width:180px !important;" >Enquiry Receipt date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="receiptDate" />
											<html:hidden property="receiptDate" styleId="receiptDate"/>
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:180px !important;" >Required Offer Submission date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="submissionDate" />
											<html:hidden property="submissionDate" styleId="submissionDate"/>
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:180px !important;" >Order Closing date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"></th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="closingDate" />
											<html:hidden property="closingDate" styleId="closingDate"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<!-- Commercial Terms Section -->
						<%@ include file="newviewupdateplant_commercialterms_include.jsp"%>
					<!-- Commercial Terms Section -->
					
					<!-- --------------------- Ratings Section : Start ----------------------------------------------------------- -->
						<%@ include file="newcreateenquiry_ratings_complete_include.jsp"%>
					<!-- --------------------- Ratings Section : End ------------------------------------------------------------- -->
			   
				</fieldset>	
				<!----------------------- Request Information : Block End -------------------------------------->	
				
				<br>
				<br>
				<br>
				
				<!-- Notes Section Starting -->	
				<fieldset >
					<legend class="blueheader"> <bean:message key="quotation.create.label.notesection"/> </legend>
					     <div>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr> <td colspan="5">&nbsp;</td> </tr>
									<tr style="float: left; width: 100%;">
										<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> <bean:message key="quotation.create.label.commentsanddeviations"/> </td>
										<td colspan="4">
											<html:textarea property="commentsDeviations" styleId="commentsDeviations" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"  > <bean:write name="newenquiryForm" property="commentsDeviations" /> </html:textarea>
										</td>
									</tr>
									<tr> <td colspan="5">&nbsp;</td> </tr>
									<tr style="float: left; width: 100%;">
										<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> <bean:message key="quotation.create.label.smnote" /> (Internal) </td>
										<td colspan="4">
											<html:textarea property="smNote" styleId="smNote" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"  > <bean:write name="newenquiryForm" property="smNote" /> </html:textarea>
										</td>
									</tr>
									<logic:notEmpty name="newenquiryForm" property="designNote">
									<tr> <td colspan="5">&nbsp;</td> </tr>
									<tr style="float: left; width: 100%;">
										<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> Note By DM (Internal) </td>
										<td colspan="4">
											<html:textarea property="designNote" styleId="designNote" style="width:905px; height:50px;" readonly="true" tabindex="35" > <bean:write name="newenquiryForm" property="designNote" /> </html:textarea>
										</td>
									</tr>
									</logic:notEmpty>
																		
									<logic:equal name="newenquiryForm" property="statusId" value="21">
									<tr> <td colspan="5">&nbsp;</td> </tr>
									<tr style="float: left; width: 100%;">
										<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> Note By Mfg. Plant (Internal) </td>
										<td colspan="4">
											<html:textarea property="mfgNote" styleId="mfgNote" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newenquiryForm" property="mfgNote" /> </html:textarea>
										</td>
									</tr>
									</logic:equal>
								</table>
						</div>
				</fieldset>
				<!-- Notes Section Ending -->
				
				<!-------------------------- Bottum Input buttons block Start  --------------------------------->
				
				<!-------------------------- Bottum Input buttons block End  ----------------------------------->
				<br>
				<div class="blue-light" align="right">
					<logic:equal name="newenquiryForm" property="statusId" value="21">
        				<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:processMfgPlantDetails();"/>
        			</logic:equal>
				</div>
			</td>
		</tr>
   </table>
   
</html:form>

<div id="loadingDiv" class="modal123" style="display:none;">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="note" align="center" style="color:#009900;">
			<img src="html/images/loading_big.gif" border="0" align="absmiddle" >
		</div>
	</div>
</div>


<script>

$(document).ready(function() {
	
	$('.ratings-table tbody').scroll(function(e) { //detect a scroll event on the tbody
	    $('.ratings-table thead').css("left", -$(".ratings-table tbody").scrollLeft()); //fix the thead relative to the body scrolling
	    $('.ratings-table thead th').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first cell of the header
	    $('.ratings-table tbody td:first-child').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first column of tdbody
	});
	
	$('#bottomfields_section').on('scroll', function () {
	    $('#topfields_section').scrollLeft($(this).scrollLeft());
	});
	
	document.getElementById("basefields_section").style.display = "none";
	document.getElementById("electricalfields_section").style.display = "none";
	document.getElementById("mechanicalfields_section").style.display = "none";
	document.getElementById("accessoriesfields_section").style.display = "none";
	document.getElementById("bearingsfields_section").style.display = "none";
	document.getElementById("certificatefields_section").style.display = "none";
		
	var totalRatingIdx = $('#totalRatingIndex').val();
	for(i=1; i<=totalRatingIdx; i++) {
		document.addEventListener('DOMContentLoaded', function(){ 
		    autosize(document.querySelectorAll('#additionalComments'+i));
		}, false);
		
		//alert('operation : ' + $('#operationType').val());
		if($('#operationType').val() != 'EDITENQUIRY' && $('#operationType').val() != 'ADDNEWRATING' && $('#operationType').val() != 'COPYPREVIOUSRATING' && $('#operationType').val() != 'REMOVERATING') {
			//alert('Inside EDIT operation : ');
			processFieldsForProductLine(i);	
		} else {
			if(i != totalRatingIdx) {
				processFieldDisplayOptions(i);
			}
		}
	}
	if($('#operationType').val() == 'ADDNEWRATING' || $('#operationType').val() != 'COPYPREVIOUSRATING' ) {
		processFieldsForProductLine(totalRatingIdx);
	}
	
	var old_title;
	$('.title-styled').on('mouseover', function(){
		old_title = $(this).attr('title');
		$('.title-message').html($(this).attr('title')).css('visibility', 'visible');
		$(this).attr('title', '');
	});
	  
	$('.title-styled').on('mouseleave', function(){
		$(this).attr('title', old_title);
		$('.title-message').html('').css('visibility', 'hidden');
	});
	
});

</script>


<style>
	div.sticky {
		position: -webkit-sticky;
  		position: sticky;
  		top: 0;
  		padding: 5px;
  		border: 2px solid #4C4EAF;
	}
	
	.ratingsdiv{
		overflow:hidden;
	}
	.newFieldsMargin {
		width:127px;
		float:left;
	}
	.newRatingLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 1.6px 1.5px 1.5px 1.5px !important;
	}
	.newRatingFields tr td {
		padding : 2.5px !important;
	}
	.newRatingSparesLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 2px !important;
	}
	.newRatingSparesFields tr td {
		padding : 2.5px !important;
	}
	
	#calcPriceLoader { 
            border: 12px solid #f3f3f3; 
            border-radius: 50%; 
            border-top: 12px solid #444444; 
            width: 70px; 
            height: 70px; 
            animation: spin 1s linear infinite; 
	} 
          
	@keyframes spin { 
		100% { 
			transform: rotate(360deg); 
		} 
	} 
          
	.center { 
            position: absolute; 
            top: 0; 
            bottom: 0; 
            left: 0; 
            right: 0; 
            margin: auto; 
	}
	
	.icontemp:after {
		/* symbol for "opening" panels */
		font-family: 'Glyphicons Halflings';
		content: "\e113";
		color: #02306b;
		font-style: normal;
		padding-left: 10px;
	}

	.icontemp.collapsed:after {
		/* symbol for "collapsed" panels */
		content: "\e114"; /* adjust as needed, taken from bootstrap.css */
	}

	.icontemp {
		border-bottom: 1px solid #ccc;
	}
	
</style>