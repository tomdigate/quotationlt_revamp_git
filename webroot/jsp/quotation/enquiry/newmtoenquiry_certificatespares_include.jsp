<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tr class="linked" style="width:1174px !important; float:left;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 17; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Witness Routine Test</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Witness Routine Test</strong></td>
					<% } %>
						
						<%  int idTestRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="witnessRoutine<%=idTestRow1%>" id="witnessRoutine<%=idTestRow1%>" title="Witness Routine" onfocus="javascript:processAutoSave('<%=idTestRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('witnessRoutine<%=idTestRow1%>', '<bean:write name="ratingObj" property="ltWitnessRoutineId" filter="false" />'); </script>
						<%	idTestRow1 = idTestRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Additional Test 1</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Additional Test 1</strong></td>
					<% } %>
						
						<%  int idTestRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="additional1Test<%=idTestRow2%>" id="additional1Test<%=idTestRow2%>" title="Additional Test 1" onfocus="javascript:processAutoSave('<%=idTestRow2%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newmtoenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('additional1Test<%=idTestRow2%>', '<bean:write name="ratingObj" property="ltAdditionalTest1Id" filter="false" />'); </script>
						<%	idTestRow2 = idTestRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Additional Test 2</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="bordertd"><strong>Additional Test 2</strong></td>
					<% } %>
						
						<%  int idTestRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="additional2Test<%=idTestRow3%>" id="additional2Test<%=idTestRow3%>" title="Additional Test 2" onfocus="javascript:processAutoSave('<%=idTestRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newmtoenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('additional2Test<%=idTestRow3%>', '<bean:write name="ratingObj" property="ltAdditionalTest2Id" filter="false" />'); </script>
						<%	idTestRow3 = idTestRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Additional Test 3</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Additional Test 3</strong></td>
					<% } %>
						
						<%  int idTestRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="additional3Test<%=idTestRow4%>" id="additional3Test<%=idTestRow4%>" title="Additional Test 3" onfocus="javascript:processAutoSave('<%=idTestRow4%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newmtoenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('additional3Test<%=idTestRow4%>', '<bean:write name="ratingObj" property="ltAdditionalTest3Id" filter="false" />'); </script>
						<%	idTestRow4 = idTestRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Type Test</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Type Test</strong></td>
					<% } %>
						
						<%  int idTestRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="typeTest<%=idTestRow5%>" id="typeTest<%=idTestRow5%>" title="Type Test" onfocus="javascript:processAutoSave('<%=idTestRow5%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltTypeTestList" >
																	<bean:define name="newmtoenquiryForm" property="ltTypeTestList" id="ltTypeTestList" type="java.util.Collection" />
																	<logic:iterate name="ltTypeTestList" id="ltTypeTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTypeTest" property="key" />' > <bean:write name="ltTypeTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('typeTest<%=idTestRow5%>', '<bean:write name="ratingObj" property="ltTypeTestId" filter="false" />'); </script>
						<%	idTestRow5 = idTestRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Certification</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Certification</strong></td>
					<% } %>
						
						<%  int idTestRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="ulce<%=idTestRow6%>" id="ulce<%=idTestRow6%>" title="Certification" onfocus="javascript:processAutoSave('<%=idTestRow6%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltULCEList" >
																	<bean:define name="newmtoenquiryForm" property="ltULCEList" id="ltULCEList" type="java.util.Collection" />
																	<logic:iterate name="ltULCEList" id="ltULCE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltULCE" property="key" />' > <bean:write name="ltULCE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('ulce<%=idTestRow6%>', '<bean:write name="ratingObj" property="ltULCEId" filter="false" />'); </script>
						<%	idTestRow6 = idTestRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>QAP</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>QAP</strong></td>
					<% } %>
						
						<%  int idTestRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="qap<%=idTestRow7%>" id="qap<%=idTestRow7%>" title="Certification" onfocus="javascript:processAutoSave('<%=idTestRow7%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltQAPList" >
									<bean:define name="newmtoenquiryForm" property="ltQAPList" id="ltQAPList" type="java.util.Collection" />
									<logic:iterate name="ltQAPList" id="ltQAP" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltQAP" property="key" />' > <bean:write name="ltQAP" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('qap<%=idTestRow7%>', '<bean:write name="ratingObj" property="ltQAPId" filter="false" />'); </script>
						<%	idTestRow7 = idTestRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spares 1</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spares 1</strong></td>
					<% } %>
						
						<%  int idTestRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spare1Rating<%=idTestRow8%>" id="spare1Rating<%=idTestRow8%>" title="Spares 1" onfocus="javascript:processAutoSave('<%=idTestRow8%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltSparesList" >
									<bean:define name="newmtoenquiryForm" property="ltSparesList" id="ltSparesList" type="java.util.Collection" />
									<logic:iterate name="ltSparesList" id="ltSpares" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltSpares" property="key" />' > <bean:write name="ltSpares" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spare1Rating<%=idTestRow8%>', '<bean:write name="ratingObj" property="ltSpares1" filter="false" />'); </script>
						<%	idTestRow8 = idTestRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spares 2</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spares 2</strong></td>
					<% } %>
						
						<%  int idTestRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spare2Rating<%=idTestRow9%>" id="spare2Rating<%=idTestRow9%>" title="Spares 2" onfocus="javascript:processAutoSave('<%=idTestRow9%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltSparesList" >
									<bean:define name="newmtoenquiryForm" property="ltSparesList" id="ltSparesList" type="java.util.Collection" />
									<logic:iterate name="ltSparesList" id="ltSpares" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltSpares" property="key" />' > <bean:write name="ltSpares" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spare2Rating<%=idTestRow9%>', '<bean:write name="ratingObj" property="ltSpares2" filter="false" />'); </script>
						<%	idTestRow9 = idTestRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spares 3</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spares 3</strong></td>
					<% } %>
						
						<%  int idTestRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spare3Rating<%=idTestRow10%>" id="spare3Rating<%=idTestRow10%>" title="Spares 3" onfocus="javascript:processAutoSave('<%=idTestRow10%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltSparesList" >
									<bean:define name="newmtoenquiryForm" property="ltSparesList" id="ltSparesList" type="java.util.Collection" />
									<logic:iterate name="ltSparesList" id="ltSpares" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltSpares" property="key" />' > <bean:write name="ltSpares" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spare3Rating<%=idTestRow10%>', '<bean:write name="ratingObj" property="ltSpares3" filter="false" />'); </script>
						<%	idTestRow10 = idTestRow10+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spares 4</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spares 4</strong></td>
					<% } %>
						
						<%  int idTestRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spare4Rating<%=idTestRow11%>" id="spare4Rating<%=idTestRow11%>" title="Spares 4" onfocus="javascript:processAutoSave('<%=idTestRow11%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltSparesList" >
									<bean:define name="newmtoenquiryForm" property="ltSparesList" id="ltSparesList" type="java.util.Collection" />
									<logic:iterate name="ltSparesList" id="ltSpares" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltSpares" property="key" />' > <bean:write name="ltSpares" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spare4Rating<%=idTestRow11%>', '<bean:write name="ratingObj" property="ltSpares4" filter="false" />'); </script>
						<%	idTestRow11 = idTestRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Spares 5</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Spares 5</strong></td>
					<% } %>
						
						<%  int idTestRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="spare5Rating<%=idTestRow12%>" id="spare5Rating<%=idTestRow12%>" title="Spares 5" onfocus="javascript:processAutoSave('<%=idTestRow12%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltSparesList" >
									<bean:define name="newmtoenquiryForm" property="ltSparesList" id="ltSparesList" type="java.util.Collection" />
									<logic:iterate name="ltSparesList" id="ltSpares" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltSpares" property="key" />' > <bean:write name="ltSpares" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						<script> selectLstItemById('spare5Rating<%=idTestRow12%>', '<bean:write name="ratingObj" property="ltSpares5" filter="false" />'); </script>
						<%	idTestRow12 = idTestRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Datasheet</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Datasheet</strong></td>
					<% } %>
						
						<%  int idTestRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_dataSheet<%=idTestRow13%>" class="bordertd">
							<select style="width:100px;" name="dataSheet<%=idTestRow13%>" id="dataSheet<%=idTestRow13%>" title="Datasheet" onfocus="javascript:processAutoSave('<%=idTestRow13%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltDataSheetList" >
																	<bean:define name="newmtoenquiryForm" property="ltDataSheetList" id="ltDataSheetList" type="java.util.Collection" />
																	<logic:iterate name="ltDataSheetList" id="ltDataSheet" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDataSheet" property="key" />' > <bean:write name="ltDataSheet" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('dataSheet<%=idTestRow13%>', '<bean:write name="ratingObj" property="ltDataSheet" filter="false" />'); </script>
						<%	idTestRow13 = idTestRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Motor GA</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Motor GA</strong></td>
					<% } %>
						
						<%  int idTestRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_motorGA<%=idTestRow14%>" class="bordertd">
							<select style="width:100px;" name="motorGA<%=idTestRow14%>" id="motorGA<%=idTestRow14%>" title="Motor GA" onfocus="javascript:processAutoSave('<%=idTestRow14%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltMotorGAList" >
																	<bean:define name="newmtoenquiryForm" property="ltMotorGAList" id="ltMotorGAList" type="java.util.Collection" />
																	<logic:iterate name="ltMotorGAList" id="ltMotorGA" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMotorGA" property="key" />' > <bean:write name="ltMotorGA" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('motorGA<%=idTestRow14%>', '<bean:write name="ratingObj" property="ltMotorGA" filter="false" />'); </script>
						<%	idTestRow14 = idTestRow14+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 15) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Performance Curves</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Performance Curves</strong></td>
					<% } %>
						
						<%  int idTestRow15 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_perfCurves<%=idTestRow15%>" class="bordertd">
							<select style="width:100px;" name="perfCurves<%=idTestRow15%>" id="perfCurves<%=idTestRow15%>" title="Performance Curves" onfocus="javascript:processAutoSave('<%=idTestRow15%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltPerfCurvesList" >
																	<bean:define name="newmtoenquiryForm" property="ltPerfCurvesList" id="ltPerfCurvesList" type="java.util.Collection" />
																	<logic:iterate name="ltPerfCurvesList" id="ltPerfCurves" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltPerfCurves" property="key" />' > <bean:write name="ltPerfCurves" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('perfCurves<%=idTestRow15%>', '<bean:write name="ratingObj" property="ltPerfCurves" filter="false" />'); </script>
						<%	idTestRow15 = idTestRow15+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 16) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>T.Box GA</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>T.Box GA</strong></td>
					<% } %>
						
						<%  int idTestRow16 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_tBoxGA<%=idTestRow16%>" class="bordertd">
							<select style="width:100px;" name="tBoxGA<%=idTestRow16%>" id="tBoxGA<%=idTestRow16%>" title="T.Box GA" onfocus="javascript:processAutoSave('<%=idTestRow16%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltTBoxGAList" >
																	<bean:define name="newmtoenquiryForm" property="ltTBoxGAList" id="ltTBoxGAList" type="java.util.Collection" />
																	<logic:iterate name="ltTBoxGAList" id="ltTBoxGA" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTBoxGA" property="key" />' > <bean:write name="ltTBoxGA" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('tBoxGA<%=idTestRow16%>', '<bean:write name="ratingObj" property="ltTBoxGA" filter="false" />'); </script>
						<%	idTestRow16 = idTestRow16+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 17) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Additional Comments (External)</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Additional Comments (External)</strong></td>
					<% } %>
						
						<%  int idTestRow17 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<textarea name="additionalComments<%=idTestRow17%>" id="additionalComments<%=idTestRow17%>" style="width:100px;" rows="8" cols="30" onkeyup="checkMaxLenghtText(this, 2500);" onkeydown="checkMaxLenghtText(this, 2500);" tabindex="35" title="Additional Comments" onfocus="javascript:processAutoSave('<%=idTestRow17%>');" ></textarea>
						</td>
						<%	idTestRow17 = idTestRow17+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>	
		</td>
	</tr>
</table>