<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<html:form action="/viewEnquiry" method="POST">
<html:hidden property="enquiryId"  styleId="enquiryId" />
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">

<tr>
<td>
		<div class="sectiontitle">
			<logic:present name="pageHeadingType" scope="request">
				<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE %>">
					<bean:message key="quotation.viewenquiry.estimate.label.heading"/> 		
				</logic:equal>
				<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM %>">
					<bean:message key="quotation.refactor.label.heading"/> 		
				</logic:equal>
				<logic:equal name="pageHeadingType" value="view">
					<bean:message key="quotation.viewindent.label.heading"/> 		
				</logic:equal>
			</logic:present>
			
		  </div>
	<logic:present name="displayPaging" scope="request">
						     
							  <table align="right"> 
							  <tr >
						      <td>
							  <fieldset  style="width:100%">
							  <table cellpadding="0" cellspacing="2" border="0"> 
							  <tr >
						            <td style="text-align:center">
	      								<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch">
	      								Return to search<img src="html/images/back.gif" border="0" align="absmiddle" /></a>
	      							</td>
						        </tr>
						        <tr class="other" align="right">
						            <td align="right">
							            <logic:present name="previousId" scope="request">	
							            	<bean:define name="previousId" id="previousId" scope="request"/>
											<bean:define name="previousIndex" id="previousIndex" scope="request"/>	
											<bean:define name="previousStatus" id="previousStatus" scope="request"/>				
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img src="html/images/PrevPage.gif" alt=""  border="0" align="absmiddle" /></a>
							            </logic:present>
							
							            <logic:present name="displayMessage" scope="request">
							            	<bean:write name="displayMessage" scope="request"/>
							            </logic:present>
							
							            <logic:present name="nextId" scope="request">
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img src="html/images/NextPage.gif" alt="Next Request"  border="0" align="absmiddle" /></a>
							            </logic:present> 
										
									</td>
						       	</tr>
						       	</table>
								</fieldset>
								</td>
								</tr>
								</table>
</logic:present>

</td>
</tr>
<tr>
  <td height="100%" valign="top">
  
  <script language="javascript" src="../html/js/tooltip.js"></script>

  <logic:present name="enquiryDto" scope="request">

   <table width="100%" border="0" cellpadding="0" cellspacing="3">
     <tr>
       <td rowspan="2" ><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
           <tr>
             <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
             <td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryDto" property="enquiryNumber"/><br>
            <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
           </tr>
           <tr>
             <td class="other"><table width="100%">
                 <tr>
                   <td height="16" class="section-head" align="center">
                   	<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="enquiryDto" property="statusName"/> </td>
                 </tr>
             </table></td>
           </tr>
       </table></td>
       <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right">
				<bean:message key="quotation.attachments.message"/>
				</td>
				<td>
				<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"></a>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:lessThan>
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
              <logic:equal name="enquiryForm" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right"  colspan="2">
				<bean:message key="quotation.attachments.enquirydocument"/>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:equal>
       </logic:notEmpty>
       </logic:present>
       
     </tr>
     <tr>
       <td  style="width:45% ">
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
       <fieldset>
         <table width="100%" class="other">
           <logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
			<tr><td>
				<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
					<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
				</a>
			</td></tr>					
		   </logic:iterate>
         </table>
       </fieldset>
        </logic:notEmpty>
        </logic:present>
      </td>
     </tr>
          <tr><td colspan="2" align="right" > 
        <!-- DM Uploading Files  -->
            <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    <td class="other" align="right">
    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:lessThan>
        
       <logic:present name="dmattachmentList" scope="request">
       <logic:notEmpty name="dmattachmentList">
      <logic:equal name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right" colspan="2">
        	<bean:message key="quotation.attachments.designdocument"/></td>
            </tr>
            </table>
        </logic:equal>
        </logic:notEmpty>
        </logic:present>
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
     
     
          <!-- Indent Page Uploads Starts Here -->
     
          <tr><td colspan="2" align="right" > 

            <logic:equal name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.indentuploadedmessage"/></td>
    	    <td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_INTENDATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:equal>
      
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="intendattachmentList" scope="request">
            <logic:notEmpty name="intendattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="intendattachmentList" id="attachmentData" indexId="idx2"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
     
     <!-- Indent Page Uploads Ends Here -->
     
     
   </table>
   <br>
 
    <!----------------------- Employee Information block start -------------------------------------->
	<fieldset>
    <legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			  <td width="340"><label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.basicinformation"/>
				   </label></td>
                   </tr>
			  
	

	
              <tr>
                <td>
                
                <div class="tablerow">
                <div class="tablecolumn" >
                 <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.indentno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryDto" property="savingIndent" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.label.admin.customer.customer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryDto" property="customerName" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.sapcusomercode"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                          <bean:write name="enquiryDto" property="sapCustomerCode" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.orderno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                         <bean:write name="enquiryDto" property="orderNumber" />  

                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.quotationrefno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                               <bean:write name="enquiryDto" property="enquiryReferenceNumber" />  
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.pocopy"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="poiCopy" />  
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.loicopy"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                          <bean:write name="enquiryDto" property="loiCopy" />  
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliverytype"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryDto" property="deliveryType" /> 
                       
                      </td>
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliveryreqcustomer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryDto" property="deliveryReqByCust" /> 
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.expecteddeliverymonth"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                   <bean:write name="enquiryDto" property="expectedDeliveryMonth" /> 
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryDto" property="advancePayment" /> 
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                      <bean:write name="enquiryDto" property="paymentTerms" /> 
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.ld"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                                            <bean:write name="enquiryDto" property="ld" /> 
                      
                       
                      </td>
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfd"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                                            <bean:write name="enquiryDto" property="warrantyDispatchDate" /> 
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfc"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryDto" property="warrantyCommissioningDate" />                        
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.drawingapproval"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryDto" property="drawingApproval" /> 
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.qapapproval"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryDto" property="qapApproval" />
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.typetest"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="typeTest" />
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.routinetest"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryDto" property="routineTest" />
                       
                      </td>
                    
                    </tr>
                    
                    
                    
                </table>
                </div><!-- Left Table Column  -->
                <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtest"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                        <bean:write name="enquiryDto" property="specialTest" />
                      </td>    
                    
                    </tr>
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtestdet"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                         <bean:write name="enquiryDto" property="specialTestDetails" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.thirdpartyinspection"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                         <bean:write name="enquiryDto" property="thirdPartyInspection" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.stageinspection"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryDto" property="stageInspection" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduser"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="endUser" />

                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserlocation"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="enduserLocation" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserindustry"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                          <bean:write name="enquiryDto" property="endUserIndustry" />

                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.consultant"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                                                <bean:write name="enquiryDto" property="consultantName" />

                      </td>    
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.epcname"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryDto" property="epcName" />
					
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.projectname"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                                           <bean:write name="enquiryDto" property="projectName" />

                      </td>    
                    
                    </tr>
                    
                       <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.packing"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="packing" />
			
                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.insurance"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryDto" property="insurance" />
                      </td>    
                    
                    </tr>
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.replacement"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="replacement" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.oldserialno"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="oldSerialNo" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.customerpartno"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                        <bean:write name="enquiryDto" property="customerPartNo" />

                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.winreason"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
				     <bean:write name="enquiryDto" property="winlossReason" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.commentsifany"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                      <bean:write name="enquiryDto" property="winlossComment" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.competitor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="winningCompetitor" />
                      </td>    
                    
                    </tr>
                    </table>
				</div><!--Right Table End  -->
                </div><!--Table Row End  -->
                
                <!--Financial  Start-->
                 <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					<div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.totalmotorprice"/>
                   <bean:write name="enquiryDto" property="totalMotorPrice" />
						  
				  </td>
				   <td align="right">
				  <bean:message key="quotation.create.label.totalpackageprice" />
				   <bean:write name="enquiryDto" property="totalPkgPrice" />
				  
				  </td>
				  
				  </tr>
				  
				  
				  </table>
				  </div>
				  		      <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.includes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
		          <bean:write name="enquiryDto" property="includeFrieght" />
				  </td>
				  </tr>
		          <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				  <bean:write name="enquiryDto" property="includeExtraWarrnt" />
				  
				  </td>
				  </tr>
				  <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
					<bean:write name="enquiryDto" property="includeSupervisionCost" />
				  </td>
				  </tr>
					<tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.typetestcharge" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
		           <bean:write name="enquiryDto" property="includetypeTestCharges" />
				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				  <bean:write name="enquiryDto" property="extraFrieght" />
				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.extrawarrantycharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				   <bean:write name="enquiryDto" property="extraExtraWarrnt" />
				  </td>
				  </tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
					<bean:write name="enquiryDto" property="extraSupervisionCost" />
				  </td>
				  </tr>
				   <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.typetestcharge" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				 <bean:write name="enquiryDto" property="extratypeTestCharges" />
				  
				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.spares" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				 <bean:write name="enquiryDto" property="sparesCost" />
				  
				  </td>
				  </tr>
				  
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
                    </div><!--Final Div Ending  -->
		                    
		                    
		                    
				  
				  
				  </fieldset>
                
                <!--Financial End  -->
                
                
                
        
        </td>
        </tr></table>
        
		</fieldset>
        
        <!-- Above Part Ending  -->
        
        
        <!--  Pradip--> 
        
        <!-- Required Data Starts From Here  IDX -->
        <br>
		<logic:notEmpty name="enquiryDto" property="ratingObjects">
         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
			<bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>				
			<div id="arrow<%= idx.intValue()+1 %>" > 
				<a href="javascript:;" onClick="toggledropdown('arrow<%= idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
				<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/></a> 
				<span class="other">
					<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
				</span></div>
				<fieldset id="fieldset<%= idx.intValue()+1 %>" style="display:none; ">
				 <legend>
				 	<a href="javascript:;" onClick="toggledropdown('fieldset<%= idx.intValue()+1 %>','arrow<%= idx.intValue()+1 %>')">
				 	<img src="html/images/downTriArrow.gif"  width="15" height="15"  border="0" align="absmiddle"/></a>
				 	<span class="blueheader">
				 		<bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%= idx.intValue()+1 %>
				 	</span>
				 </legend>
				 <!-- Indent Rating Data Starts Here, We Have To Do Changess  -->
				             <fieldset>
			<div class="blue-light" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td>
				
				  <bean:message key="quotation.create.label.quantityrequired"/>
                  <bean:write name="ratingObject" property="quantity"/>					</td>
					<td>
					&nbsp;
				</td>
				  </tr>
				</table>
		        	</div> <!--Quantity Ending  -->
		        	
				 <br>
			   	  <div class="tablerow">
                  <div class="tablecolumn" >
              	  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
			      <tr>
			       <td >
                    	<bean:message key="quotation.create.label.kw"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

                    	 <bean:write name="ratingObject" property="kw"/>
                    	 <logic:notEqual name="ratingObject" property="kw" value="" >
              			<bean:write name="ratingObject" property="ratedOutputUnit"/>
              			</logic:notEqual>
                    	 

                    </td>
                    </tr>
                    <tr>
			       <td >
                    	<bean:message key="quotation.create.label.pole"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="poleName"/>
                    </td>
                    </tr>
                    
                   <tr>
			       <td >
                    	<bean:message key="quotation.create.label.scsr"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="scsrName"/>
                    </td>
                    </tr>
                    
                     <tr>
                    <td>
			        	<bean:message key="quotation.offerdata.technical.label.framesize"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="technicalObject" property="frameSize" />
                   	</td>
                    
                    </tr>
                    
                    <tr>
			       <td >
                    	<bean:message key="quotation.create.label.volts"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <logic:notEqual name="ratingObject" property="volts" value="">
              			<bean:write name="ratingObject" property="volts"/>
              		</logic:notEqual>
              		<logic:notEqual name="ratingObject" property="voltsVar" value="">
	              		 +/-<bean:write name="ratingObject" property="voltsVar"/> %
	              	</logic:notEqual>
                    
                    </td>
                    </tr>
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.enclosure"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="enclosureName"/>
                    </td>
                    </tr>	
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.mounting"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="mountingName"/>
                    </td>
                    </tr>	
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.application"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="applicationName"/>
                    </td>
                    </tr>	
               <%String mainTermBoxPS="",neutralTermBox="",spaceHeater="",windingRTD="",
                 bearingRTD="",rtGD2="",strtgCurrent="",unitpriceMultiplier_rsm="",
                priceincludeFrieght="",priceincludeFrieghtVal="",priceincludeExtraWarn="",
                priceincludeExtraWarnVal="",priceincludeSupervisionCost="",priceincludeSupervisionCostVal="",
                priceincludetypeTestCharges="",priceincludetypeTestChargesVal="",priceextraFrieght="",priceextraFrieghtVal="",
                priceextraExtraWarn="",priceextraExtraWarnVal="",priceextraSupervisionCost="",priceextraSupervisionCostVal="",
                priceextratypeTestCharges="",priceextratypeTestChargesVal=""; %>     
                    
                  				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
               
                    	<bean:define id="tech1" name="technicalObject1" property="mainTermBoxPS" />
                    	<bean:define id="tech2" name="technicalObject1" property="neutralTermBox" />
                    	<bean:define id="tech3" name="technicalObject1" property="spaceHeater"/>
                    	<bean:define id="tech4" name="technicalObject1" property="windingRTD"/>
                    	<bean:define id="tech5" name="technicalObject1" property="bearingRTD"/>
                    	<bean:define id="tech6" name="technicalObject1" property="rtGD2"/>
                    	<bean:define id="tech7" name="technicalObject1" property="strtgCurrent"/>
                    	<bean:define id="tech8" name="technicalObject1" property="unitpriceMultiplier_rsm"/>
                       <bean:define id="tech9" name="technicalObject1" property="priceincludeFrieght"/>
                       <bean:define id="tech10" name="technicalObject1" property="priceincludeFrieghtVal"/>
                       <bean:define id="tech11" name="technicalObject1" property="priceincludeExtraWarn"/>
                       <bean:define id="tech12" name="technicalObject1" property="priceincludeExtraWarnVal"/>
                       
                       <bean:define id="tech13" name="technicalObject1" property="priceincludeSupervisionCost"/>
                       <bean:define id="tech14" name="technicalObject1" property="priceincludeSupervisionCostVal"/>
                        <bean:define id="tech15" name="technicalObject1" property="priceincludetypeTestCharges"/>
                       <bean:define id="tech16" name="technicalObject1" property="priceincludetypeTestChargesVal"/>
                       <bean:define id="tech17" name="technicalObject1" property="priceextraFrieght"/>
                       <bean:define id="tech18" name="technicalObject1" property="priceextraFrieghtVal"/>
                       <bean:define id="tech19" name="technicalObject1" property="priceextraExtraWarn"/>
                       <bean:define id="tech20" name="technicalObject1" property="priceextraExtraWarnVal"/>
                       <bean:define id="tech21" name="technicalObject1" property="priceextraSupervisionCost"/>
                       <bean:define id="tech22" name="technicalObject1" property="priceextraSupervisionCostVal"/>                      
                       <bean:define id="tech23" name="technicalObject1" property="priceextratypeTestCharges"/>
                       <bean:define id="tech24" name="technicalObject1" property="priceextratypeTestChargesVal"/>    
                    	<%
                    	mainTermBoxPS=String.valueOf(tech1);
                    	neutralTermBox=String.valueOf(tech2);
                    	spaceHeater=String.valueOf(tech3);
                    	windingRTD=String.valueOf(tech4);
                    	bearingRTD=String.valueOf(tech5);
                    	rtGD2=String.valueOf(tech6);
                    	strtgCurrent=String.valueOf(tech7);
                    	unitpriceMultiplier_rsm=String.valueOf(tech8);
                    	priceincludeFrieght=String.valueOf(tech9);
                    	priceincludeFrieghtVal=String.valueOf(tech10);
                    	priceincludeExtraWarn=String.valueOf(tech11);
                    	priceincludeExtraWarnVal=String.valueOf(tech12);
                    	priceincludeSupervisionCost=String.valueOf(tech13);
                    	priceincludeSupervisionCostVal=String.valueOf(tech14);
                    	priceincludetypeTestChargesVal=String.valueOf(tech15);
                    	priceincludetypeTestChargesVal=String.valueOf(tech16);
                    	priceextraFrieght=String.valueOf(tech17);
                    	priceextraFrieghtVal=String.valueOf(tech18);
                    	priceextraExtraWarn=String.valueOf(tech19);
                    	priceextraExtraWarnVal=String.valueOf(tech20);
                    	priceextraSupervisionCost=String.valueOf(tech21);
                    	priceextraSupervisionCostVal=String.valueOf(tech22);
                    	priceextratypeTestCharges=String.valueOf(tech23);
                    	priceextratypeTestChargesVal=String.valueOf(tech24);
                    	%>
                    	<%} %>
                    	</logic:iterate>
                    	</logic:notEmpty>
                    	
                    	
                    <tr>
			       <td>
                    	<bean:message key="quotation.offer.label.maintbps"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=mainTermBoxPS %>
              			
                    </td>
                    </tr>	
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.neutraltb"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=neutralTermBox %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.spaceheater"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=spaceHeater %>
              			
                    </td>
                    </tr>	
                    
                     <tr>
			        <td>
                    	<bean:message key="quotation.create.label.windingrtd"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=windingRTD %>
              			
                    </td>
                    </tr>	
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.bearingrtd"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=bearingRTD %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.create.label.momentofInertiaload"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=rtGD2 %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.create.label.staringcurrent"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=strtgCurrent %>
              			
                    </td>
                    </tr>
                    
                    	
                    
                    	
                    
                    
                    
                    </table>
                    </div> <!--Left Column End  -->
                    <div class="tablecolumn" >
				   <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
				  <tr>
				<td>
			        	<bean:message key="quotation.create.label.htlt"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="ratingObject" property="htltName"/>
					</td>				
					</tr>
				     <tr>
	                 <td>
                    	<bean:message key="quotation.create.label.frequency"/>
                    </td>
                       <td class="formContentview-rating-b0x"> 
                       <logic:notEqual name="ratingObject" property="frequency" value="">
              				<bean:write name="ratingObject" property="frequency"/>
              			</logic:notEqual>
              			<logic:notEqual name="ratingObject" property="frequencyVar" value="">
	              			+/-<bean:write name="ratingObject" property="frequencyVar"/>%
	              		</logic:notEqual>
                    
					</td>
					</tr>
					
					<tr>
				     <td>
			        	<bean:message key="quotation.create.label.duty"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="ratingObject" property="dutyName"/>
					</td>				
					</tr>
					
		            <tr>
				     <td>
			        	<bean:message key="quotation.create.label.ambience"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                  	<logic:notEqual name="ratingObject" property="ambience" value="">
                  		<bean:write name="ratingObject" property="ambience"/> 
                  	</logic:notEqual>
                  	
                  	<logic:notEqual name="ratingObject" property="ambienceVar" value="">
	                  	+/-<bean:write name="ratingObject" property="ambienceVar"/>
	                </logic:notEqual>

					</td>				
					</tr>
					
				   <tr>
				     <td>
			        	<bean:message key="quotation.create.label.degpro"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                 	<bean:write name="ratingObject" property="degreeOfProName"/> 
                    </td>				
					</tr>
					
			        <tr>
				     <td>
			        	<bean:message key="quotation.create.label.altitude"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                 	<logic:notEqual name="ratingObject" property="altitude" value="">
                 		<bean:write name="ratingObject" property="altitude"/> 
                 	</logic:notEqual>
                 	<logic:notEqual name="ratingObject" property="altitudeVar" value="">
                 		+/-<bean:write name="ratingObject" property="altitudeVar"/> 
                 	</logic:notEqual>

                    </td>				
					</tr>
					
				    <tr>
				     <td>
			        	<bean:message key="quotation.create.label.stgmethod"/>
			        </td>
                    <td class="formContentview-rating-b0x">
             	 	<bean:write name="ratingObject" property="methodOfStg"/>                    
             	  </td>				
					</tr>
					
                   <tr>
				     <td>
			        	<bean:message key="quotation.create.label.temprise"/>
			        </td>
                    <td class="formContentview-rating-b0x">
	              	 	<bean:write name="ratingObject" property="tempRaise"/> 
             	  </td>				
					</tr>
					
					<tr>
				     <td>
			        	<bean:message key="quotation.create.label.coupling"/>
			        </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="coupling"/>
             	  </td>				
					</tr>
					
			        <tr>
				     <td>
              	<bean:message key="quotation.create.label.insulationclass"/>
			        </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="insulationName"/>
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.termbox"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="termBoxName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.rotdirection"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="directionOfRotName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.shaftext"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="shaftExtName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.paint"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="paint"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.ratingunitprice"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="ratingUnitPrice"/> 
             	  </td>				
					</tr>
	
					
					
					
					
					
					
					
					</table>
					</div><!--Right Column End  -->
					</div><!--Total Div Tag Ending  -->
				</fieldset> <!-- Technical Data End  -->
				<!-- Commercial Data Starts -->
				                     <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					<div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.unitpricemotor" />
                   <%=unitpriceMultiplier_rsm %>

				  </td>
				  
				  <td align="right">
				  <bean:message key="quotation.create.label.totalpackageprice" />
				   <bean:write name="ratingObject" property="totalPackagePrice" />
				  
				  </td>
				  </tr>
				  </table>
				  </div>
				  		      <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.priceincludes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludeFrieght.equals("0")?"No":"Yes" %>
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <%if(priceincludeFrieghtVal!=""){ %>
				  <%=priceincludeFrieghtVal %>
				  <%} %>

				  </td>
				   
				  </tr>
		          <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludeExtraWarn.equals("0")?"No":"Yes" %>
				 
				 </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
		         <%if(priceincludeExtraWarnVal!=""){ %>
				  <%=priceincludeExtraWarnVal %>
				  <%} %>
		     

				  </td>
				  </tr>
				  <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
		        <%=priceincludeSupervisionCost.equals("0")?"No":"Yes" %>  
				  
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
                 <% if(priceincludeSupervisionCostVal!=""){ %>
				  <%=priceincludeSupervisionCostVal %>
				  <%} %>
		     				  
				  </td>
				  </tr>
					<tr>
				  <td colspan="3 style="width:50%">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludetypeTestCharges.equals("0")?"No":"Yes" %> 
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				   <% if(priceincludetypeTestChargesVal!=""){ %>
				  <%=priceincludetypeTestChargesVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  
				 <td class="formContentview-rating-b0x" style="width:15% ">
			    <%=priceextraFrieght.equals("0")?"No":"Yes" %> 
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <% if(priceextraFrieghtVal!=""){ %>
				  <%=priceextraFrieghtVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceextraExtraWarn.equals("0")?"No":"Yes" %>				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <% if(priceextraExtraWarnVal!=""){ %>
				  <%=priceextraExtraWarnVal %>
				  <%} %>
				  
			  </td>
				  </tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width: 15%">
				  <%=priceextraSupervisionCost.equals("0")?"No":"Yes" %>
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				 <% if(priceextraSupervisionCostVal!=""){ %>
				  <%=priceextraSupervisionCostVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width: 15%">
				  <%=priceextratypeTestCharges.equals("0")?"No":"Yes" %>
				  
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
			     <% if(priceextratypeTestChargesVal!=""){ %>
				  <%=priceextratypeTestChargesVal %>
				  <%} %>
				  
			
				  </td>
				  </tr>
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
		                    
		                    
				  
				  
                
                <!--Financial End  -->
				
				<!-- Commercial Data Ends  -->
				
			<!-- Delivery Details Starts -->	
            <fieldset style="width:100%">
		   <legend class="blueheader">	
			<bean:message key="quotation.create.label.deliverydetails"/>
		  </legend>
		    <logic:notEmpty name="ratingObject" property="deliveryDetailsList">
		  
		  			  <table width="100%" cellpadding="0" cellspacing="0"  >
		  			  <%int k=0; %>
   	  
 	  
					  
					  	 <logic:iterate name="ratingObject" property="deliveryDetailsList"
									               id="deliveryDetails" indexId="deliveryid1" 
									               type="in.com.rbc.quotation.enquiry.dto.DeliveryDetailsDto">	
							<% k = deliveryid1.intValue()+1;
							
							if(k==1)
							{
							%>
		<tr><td colspan="4">&nbsp;</td></tr>
							
							                  <tr>
                  
			    <tr>
			     <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.delivery.label.sno"/>
                  </td>
			    
			      <td height="100%" style="width:130px " class="formLabelTophome" >
			      	<bean:message key="quotation.create.label.deliverylocation"/> 
			      </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.deliveryaddress"/>
                  </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.noofmotors"/> 
                  </td>
                  
                </tr>
                <%} %>
               <tr> 
               <td class="formContentview-rating-b0x"><%=k %></td>    
		        <td class="formContentview-rating-b0x">
		       <bean:write name="deliveryDetails" property="deliveryLocation" />
		         </td>
		         <td align="left" class="formContentview-rating-b0x">
			   <bean:write name="deliveryDetails" property="deliveryAddress" />		         
			   </td>
		       <td class="formContentview-rating-b0x">
				<bean:write name="deliveryDetails" property="noofMotors" />		      
				</td>
                
                </logic:iterate>
		  </table>
		  </logic:notEmpty>
		  
                    
                
				 
				 
</fieldset>

<!-- Delivery Details End  -->

	<fieldset style="width:90%">
		    <legend class="blueheader">	
			<bean:message key="quotation.create.label.notesection"/>
			</legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="indentnote"  value="">
                                        <bean:write name="enquiryForm" property="indentnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name"/>
                    
                    </logic:notEqual>
                    
                  </tr>
                  <tr><td colspan="3">&nbsp;</td></tr>
			      <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.cmnote"/>
				    </td>
                    <td colspan="4" class="formLabelrating">
                    <logic:notEqual name="enquiryForm" property="cmNote"  value="">
                                        <bean:write name="enquiryForm" property="cmNote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="cmName"/>
                    
                    </logic:notEqual>
                    </td>
                  </tr>
                  				  <tr><td colspan="3">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.dmnote"/>
				    </td>
                    <td colspan="4"  class="formLabelrating">
                     <logic:notEqual name="enquiryForm" property="dmnote"  value="">
                                        <bean:write name="enquiryForm" property="dmnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="dm_name"/>
                    
                    </logic:notEqual>
                    </td>
                    	
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  
</div>
</fieldset>
</fieldset>

</logic:iterate>
</logic:notEmpty>
</logic:present>
</td>
</tr>

              <tr>
                <td class="blue-light" align="right" colspan="4">

                			<div class="blue-light" align="right">
				<bean:define id="statusId" name="enquiryDto" property="statusId" />
				<bean:define id="createdSSO" name="enquiryDto" property="createdBySSO" />
				
					 <logic:present name="salesManager" scope="request">
					  <logic:greaterThan name="enquiryDto" property="statusId" value="5">
					 	<input name="viewpdf" type="button" onclick="viewPDF();" class="BUTTON" value="View PDF"/>
					 	</logic:greaterThan>
					 </logic:present>
					    
				</div>
			
               </td>
               </tr>

</table>
</html:form>