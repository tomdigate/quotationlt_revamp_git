<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
	<tr>
		<td width="20%" class="label-text"> Witness Routine Test </td>
		<td width="20%" class="normal">
			<select name="witnessRoutine" id="witnessRoutine" title="Witness Routine" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Additional Test 1 </td>
		<td width="20%" class="normal">
			<select name="additional1Test" id="additional1Test" title="Additional Test 1" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
	</tr>
	<script> selectLstItemById('witnessRoutine', '<bean:write name="ratingObj" property="ltWitnessRoutineId" filter="false" />'); </script>
	<script> selectLstItemById('additional1Test', '<bean:write name="ratingObj" property="ltAdditionalTest1Id" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Additional Test 2 </td>
		<td width="20%" class="normal">
			<select name="additional2Test" id="additional2Test" title="Additional Test 2" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Additional Test 3 </td>
		<td width="20%" class="normal">
			<select name="additional3Test" id="additional3Test" title="Additional Test 3" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltAdditionalTestList" >
																	<bean:define name="newenquiryForm" property="ltAdditionalTestList" id="ltAdditionalTestList" type="java.util.Collection" />
																	<logic:iterate name="ltAdditionalTestList" id="ltAdditionalTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAdditionalTest" property="key" />' > <bean:write name="ltAdditionalTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
	</tr>
	<script> selectLstItemById('additional2Test', '<bean:write name="ratingObj" property="ltAdditionalTest2Id" filter="false" />'); </script>
	<script> selectLstItemById('additional3Test', '<bean:write name="ratingObj" property="ltAdditionalTest3Id" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Type Test </td>
		<td width="20%" class="normal">
			<select name="typeTest" id="typeTest" title="Type Test" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltTypeTestList" >
																	<bean:define name="newenquiryForm" property="ltTypeTestList" id="ltTypeTestList" type="java.util.Collection" />
																	<logic:iterate name="ltTypeTestList" id="ltTypeTest" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTypeTest" property="key" />' > <bean:write name="ltTypeTest" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Certification </td>
		<td width="20%" class="normal">
			<select name="ulce" id="ulce" title="Certification" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltULCEList" >
																	<bean:define name="newenquiryForm" property="ltULCEList" id="ltULCEList" type="java.util.Collection" />
																	<logic:iterate name="ltULCEList" id="ltULCE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltULCE" property="key" />' > <bean:write name="ltULCE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
	</tr>
	<script> selectLstItemById('typeTest', '<bean:write name="ratingObj" property="ltTypeTestId" filter="false" />'); </script>
	<script> selectLstItemById('ulce', '<bean:write name="ratingObj" property="ltULCEId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Spares </td>
		<td width="20%" class="normal">
			<select name="spares" id="spares" title="Spares" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Add Drawing </td>
		<td width="20%" class="normal">
			<input type="file" name="addDrawing" id="addDrawing" style="width:100px; font-size:8px; padding:0px !important;" title="Add Drawing" >
		</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
	
</table>