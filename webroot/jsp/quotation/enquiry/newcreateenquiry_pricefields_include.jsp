<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
		<tr class="linked" style="width:1100px !important; float:left; overflow-x:scroll;">
			<td valign="top">
				<table id="tableR" align="center" cellspacing="0" cellpadding="1">
				<%	String sTextStyleClass = "textnoborder-light"; %>
				<% for(int j = 1; j <= 34; j++) {  %>
					<% if(j == 1) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" style="float:left;"><strong>Tag Number#</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" style="float:left;"><strong>Tag Number#</strong></td>
							<% } %>
							
							<%  int idBaseRow1 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="tagNumber<%=idBaseRow1%>" id="tagNumber<%=idBaseRow1%>" class="normal" style="width: 100px;" value='<bean:write name="ratingObj" property="tagNumber" filter="false" />' onfocus="javascript:processAutoSave('<%=idBaseRow1%>');" title="Tag Number" maxlength="50" >
							</td>
							<%	idBaseRow1 = idBaseRow1+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 2) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" ><strong>Manufacturing Location</strong></td>							
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" ><strong>Manufacturing Location</strong></td>
							<% } %>
							
							<%  int idBaseRow2 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="mfgLocation<%=idBaseRow2%>" name="mfgLocation<%=idBaseRow2%>" title="Manufacturing Location" onchange="javascript:processMfgLocationOnChange('<%=idBaseRow2%>');" onfocus="javascript:processAutoSave('<%=idBaseRow2%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newenquiryForm" property="ltMfgLocationList" >
										<bean:define name="newenquiryForm" property="ltMfgLocationList" id="ltMfgLocationList" type="java.util.Collection" />
										<logic:iterate name="ltMfgLocationList" id="ltMfgLocation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltMfgLocation" property="key"/>'> <bean:write name="ltMfgLocation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('mfgLocation<%=idBaseRow2%>', '<bean:write name="ratingObj" property="ltManufacturingLocation" filter="false" />'); </script>
							<%	idBaseRow2 = idBaseRow2+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 3) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd" ><strong>Efficiency class</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd" ><strong>Efficiency class</strong></td>
						<% } %>
							
							<%  int idBaseRow3 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="effClass<%=idBaseRow3%>" id="effClass<%=idBaseRow3%>" title="Efficiency Class" onfocus="javascript:processAutoSave('<%=idBaseRow3%>');">
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltEfficiencyClassList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltEfficiencyClassList" id="ltEfficiencyClassList" type="java.util.Collection" />
										<logic:iterate name="ltEfficiencyClassList" id="ltEfficiencyClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltEfficiencyClass" property="key" />' > <bean:write name="ltEfficiencyClass" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('effClass<%=idBaseRow3%>', '<bean:write name="ratingObj" property="ltEffClass" filter="false" />'); </script>
							<%	idBaseRow3 = idBaseRow3+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 4) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Voltage</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Voltage</strong></td>
							<% } %>
							
							<%  int idBaseRow4 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="volt<%=idBaseRow4%>" id="volt<%=idBaseRow4%>" title="Voltage" onfocus="javascript:processAutoSave('<%=idBaseRow4%>');">
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltVoltageList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltVoltageList" id="ltVoltageList" type="java.util.Collection" />
										<logic:iterate name="ltVoltageList" id="ltVoltage" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltVoltage" property="key" />' > <bean:write name="ltVoltage" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('volt<%=idBaseRow4%>', '<bean:write name="ratingObj" property="ltVoltId" filter="false" />'); </script>
							<%	idBaseRow4 = idBaseRow4+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 5) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Voltage Variation (+)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Voltage Variation (+)</strong></td>
							<% } %>
							
							<%  int idBaseRow5 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<span style="font-weight:bold;">+</span>&nbsp;&nbsp;
								<select style="width: 40px;" name="voltAddVariation<%=idBaseRow5%>" id="voltAddVariation<%=idBaseRow5%>" title="Voltage Add Variation" onfocus="javascript:processAutoSave('<%=idBaseRow5%>');">
									<logic:present property="ltVoltVariationList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
										<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('voltAddVariation<%=idBaseRow5%>', '<bean:write name="ratingObj" property="ltVoltAddVariation" filter="false" />'); </script>
							<%	idBaseRow5 = idBaseRow5+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 6) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Voltage Variation (-)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Voltage Variation (-)</strong></td>
							<% } %>
							
							<%  int idBaseRow6 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<span style="font-weight:bold;">-</span>&nbsp;&nbsp;
								<select style="width: 40px;" name="voltRemoveVariation<%=idBaseRow6%>" id="voltRemoveVariation<%=idBaseRow6%>" title="Voltage Remove Variation" onfocus="javascript:processAutoSave('<%=idBaseRow6%>');">
																<logic:present property="ltVoltVariationList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
																	<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('voltRemoveVariation<%=idBaseRow6%>', '<bean:write name="ratingObj" property="ltVoltRemoveVariation" filter="false" />'); </script>
							<%	idBaseRow6 = idBaseRow6+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 7) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Frequency</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Frequency</strong></td>
							<% } %>
							
							<%  int idBaseRow7 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="freq<%=idBaseRow7%>" id="freq<%=idBaseRow7%>" title="Frequency" onfocus="javascript:processAutoSave('<%=idBaseRow7%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltFrequencyList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltFrequencyList" id="ltFrequencyList" type="java.util.Collection" />
																	<logic:iterate name="ltFrequencyList" id="ltFrequency" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFrequency" property="key" />' > <bean:write name="ltFrequency" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('freq<%=idBaseRow7%>', '<bean:write name="ratingObj" property="ltFreqId" filter="false" />'); </script>
							<%	idBaseRow7 = idBaseRow7+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 8) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Frequency Variation (+)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Frequency Variation (+)</strong></td>
							<% } %>
							
							<%  int idBaseRow8 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<span style="font-weight:bold;">+</span>&nbsp;&nbsp;
								<select style="width: 40px;" name="freqAddVariation<%=idBaseRow8%>" id="freqAddVariation<%=idBaseRow8%>" title="Frequency Add Variation" onfocus="javascript:processAutoSave('<%=idBaseRow8%>');" >
																<logic:present property="ltFreqVariationList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
																	<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('freqAddVariation<%=idBaseRow8%>', '<bean:write name="ratingObj" property="ltFreqAddVariation" filter="false" />'); </script>
							<%	idBaseRow8 = idBaseRow8+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 9) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Frequency Variation (-)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Frequency Variation (-)</strong></td>
							<% } %>
							
							<%  int idBaseRow9 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<span style="font-weight:bold;">-</span>&nbsp;&nbsp;
								<select style="width: 40px;" name="freqRemoveVariation<%=idBaseRow9%>" id="freqRemoveVariation<%=idBaseRow9%>" title="Frequency Remove Variation" onfocus="javascript:processAutoSave('<%=idBaseRow9%>');" >
																<logic:present property="ltFreqVariationList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
																	<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('freqRemoveVariation<%=idBaseRow9%>', '<bean:write name="ratingObj" property="ltFreqRemoveVariation" filter="false" />'); </script>
							<%	idBaseRow9 = idBaseRow9+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 10) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Combined Variation</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Combined Variation</strong></td>
							<% } %>
							
							<%  int idBaseRow10 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:60px;" name="combinedVariation<%=idBaseRow10%>" id="combinedVariation<%=idBaseRow10%>" title="Combined Variation" onfocus="javascript:processAutoSave('<%=idBaseRow10%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltCombinedVariationList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltCombinedVariationList" id="ltCombinedVariationList" type="java.util.Collection" />
																	<logic:iterate name="ltCombinedVariationList" id="ltCombinedVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltCombinedVariation" property="key" />' > <bean:write name="ltCombinedVariation" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('combinedVariation<%=idBaseRow10%>', '<bean:write name="ratingObj" property="ltCombVariation" filter="false" />'); </script>
							<%	idBaseRow10 = idBaseRow10+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 11) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Service Factor</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Service Factor</strong></td>
							<% } %>
							
							<%  int idBaseRow11 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="serviceFactor<%=idBaseRow11%>" id="serviceFactor<%=idBaseRow11%>" title="Service Factor" onfocus="javascript:processAutoSave('<%=idBaseRow11%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltServiceFactorList">
																	<bean:define name="newenquiryForm" property="ltServiceFactorList" id="ltServiceFactorList" type="java.util.Collection" />
																	<logic:iterate name="ltServiceFactorList" id="ltServiceFactor" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltServiceFactor" property="key" />'>
																			<bean:write name="ltServiceFactor" property="value" />
																		</option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('serviceFactor<%=idBaseRow11%>', '<bean:write name="ratingObj" property="ltServiceFactor" filter="false" />'); </script>
							<%	idBaseRow11 = idBaseRow11+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 12) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Method of Starting</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Method of Starting</strong></td>
							<% } %>
							
							<%  int idBaseRow12 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="methodOfStarting<%=idBaseRow12%>" id="methodOfStarting<%=idBaseRow12%>" title="Method of Starting" onfocus="javascript:processAutoSave('<%=idBaseRow12%>');" onchange="javascript:processFieldsForStartingMethod('<%=idBaseRow12%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltMethodOfStartingList">
																	<bean:define name="newenquiryForm" property="ltMethodOfStartingList" id="ltMethodOfStartingList" type="java.util.Collection" />
																	<logic:iterate name="ltMethodOfStartingList" id="ltMethodOfStarting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMethodOfStarting" property="key" />'>
																			<bean:write name="ltMethodOfStarting" property="value" />
																		</option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('methodOfStarting<%=idBaseRow12%>', '<bean:write name="ratingObj" property="ltMethodOfStarting" filter="false" />'); </script>
							<%	idBaseRow12 = idBaseRow12+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 13) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Starting Current on DOL Starting</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Starting Current on DOL Starting</strong></td>
							<% } %>
							
							<%  int idBaseRow13 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;" name="startingCurrOnDOLStarting<%=idBaseRow13%>" id="startingCurrOnDOLStarting<%=idBaseRow13%>" title="Starting Current On DOL Starting" onfocus="javascript:processAutoSave('<%=idBaseRow13%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltStartingDOLCurrentList">
																	<bean:define name="newenquiryForm" property="ltStartingDOLCurrentList" id="ltStartingDOLCurrentList" type="java.util.Collection" />
																	<logic:iterate name="ltStartingDOLCurrentList" id="ltStartingDOLCurrent" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltStartingDOLCurrent" property="key" />'>
																			<bean:write name="ltStartingDOLCurrent" property="value" />
																		</option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('startingCurrOnDOLStarting<%=idBaseRow13%>', '<bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart" filter="false" />'); </script>
							<%	idBaseRow13 = idBaseRow13+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 14) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Ambient Temp. Deg C (+) </strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Ambient Temp. Deg C (+) </strong></td>
							<% } %>
							
							<%  int idBaseRow14 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="ambientTemp<%=idBaseRow14%>" id="ambientTemp<%=idBaseRow14%>" title="Ambient Temp. Deg C (+)" onfocus="javascript:processAutoSave('<%=idBaseRow14%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltAmbientTemperatureList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltAmbientTemperatureList" id="ltAmbientTemperatureList" type="java.util.Collection" />
																	<logic:iterate name="ltAmbientTemperatureList" id="ltAmbientTemperature" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAmbientTemperature" property="key" />' > <bean:write name="ltAmbientTemperature" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('ambientTemp<%=idBaseRow14%>', '<bean:write name="ratingObj" property="ltAmbTempId" filter="false" />'); </script>
							<%	idBaseRow14 = idBaseRow14+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 15) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Ambient Temp. Deg C (-) </strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Ambient Temp. Deg C (-) </strong></td>
							<% } %>
							
							<%  int idBaseRow15 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="ambientTempSub<%=idBaseRow15%>" id="ambientTempSub<%=idBaseRow15%>" title="Ambient Temp. Deg C (-)" onfocus="javascript:processAutoSave('<%=idBaseRow15%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltAmbientRemTempList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltAmbientRemTempList" id="ltAmbientRemTempList" type="java.util.Collection" />
																	<logic:iterate name="ltAmbientRemTempList" id="ltAmbientRemTemp" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAmbientRemTemp" property="key" />' > <bean:write name="ltAmbientRemTemp" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('ambientTempSub<%=idBaseRow15%>', '<bean:write name="ratingObj" property="ltAmbTempRemoveId" filter="false" />'); </script>
							<%	idBaseRow15 = idBaseRow15+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 16) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Temperature Rise</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Temperature Rise</strong></td>
							<% } %>
							
							<%  int idBaseRow16 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="tempRise<%=idBaseRow16%>" id="tempRise<%=idBaseRow16%>" title="Temperature Rise" onfocus="javascript:processAutoSave('<%=idBaseRow16%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltTemperatureRiseList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltTemperatureRiseList" id="ltTemperatureRiseList" type="java.util.Collection" />
																	<logic:iterate name="ltTemperatureRiseList" id="ltTemperatureRise" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTemperatureRise" property="key" />' > <bean:write name="ltTemperatureRise" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('tempRise<%=idBaseRow16%>', '<bean:write name="ratingObj" property="ltTempRise" filter="false" />'); </script>
							<%	idBaseRow16 = idBaseRow16+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 17) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Insulation Class / Temp. Rise Class</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Insulation Class / Temp. Rise Class</strong></td>
							<% } %>
							
							<%  int idBaseRow17 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="insulationClass<%=idBaseRow17%>" id="insulationClass<%=idBaseRow17%>" title="Insulation Class" onfocus="javascript:processAutoSave('<%=idBaseRow17%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltInsulationClassList" >
																	<bean:define name="newenquiryForm" property="ltInsulationClassList" id="ltInsulationClassList" type="java.util.Collection" />
																	<logic:iterate name="ltInsulationClassList" id="ltInsulationClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltInsulationClass" property="key" />' > <bean:write name="ltInsulationClass" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('insulationClass<%=idBaseRow17%>', '<bean:write name="ratingObj" property="ltInsulationClassId" filter="false" />'); </script>
							<%	idBaseRow17 = idBaseRow17+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 18) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Method Of Cooling</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Method Of Cooling</strong></td>
							<% } %>
							
							<%  int idBaseRow18 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="methodOfCooling<%=idBaseRow18%>" id="methodOfCooling<%=idBaseRow18%>" title="Method of Cooling" onfocus="javascript:processAutoSave('<%=idBaseRow18%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltForcedCoolingList" >
																	<bean:define name="newenquiryForm" property="ltForcedCoolingList" id="ltForcedCoolingList" type="java.util.Collection" />
																	<logic:iterate name="ltForcedCoolingList" id="ltForcedCooling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltForcedCooling" property="key" />' > <bean:write name="ltForcedCooling" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('methodOfCooling<%=idBaseRow18%>', '<bean:write name="ratingObj" property="ltForcedCoolingId" filter="false" />'); </script>
							<%	idBaseRow18 = idBaseRow18+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 19) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Altitude (<= 1000) </strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Altitude (<= 1000) </strong></td>
							<% } %>
							
							<%  int idBaseRow19 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="altitude<%=idBaseRow19%>" id="altitude<%=idBaseRow19%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltAltitude" filter="false" />' onfocus="javascript:processAutoSave('<%=idBaseRow19%>');" title="Altitude" maxlength="10">
							</td>
							<%	idBaseRow19 = idBaseRow19+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 20) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<td width="200px" class="dark bordertd"><strong>Area Classification</strong></td>
							<% } else { %>
								<td width="200px" class="light bordertd"><strong>Area Classification</strong></td>
							<% } %>
							
							<%  int idBaseRow20 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardAreaType<%=idBaseRow20%>" id="hazardAreaType<%=idBaseRow20%>" title="Area Classification" onchange="javascript:processFieldsForHazard('<%=idBaseRow20%>');" onfocus="javascript:processAutoSave('<%=idBaseRow20%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltHazAreaTypeList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltHazAreaTypeList" id="ltHazAreaTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltHazAreaTypeList" id="ltHazAreaType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHazAreaType" property="key" />' > <bean:write name="ltHazAreaType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardAreaType<%=idBaseRow20%>', '<bean:write name="ratingObj" property="ltHazardAreaTypeId" filter="false" />'); </script>
							<%	idBaseRow20 = idBaseRow20+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 21) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Hazard Protection</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Hazard Protection</strong></td>
							<% } %>
							
							<%  int idBaseRow21 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardArea<%=idBaseRow21%>" id="hazardArea<%=idBaseRow21%>" title="Hazard Protection" onfocus="javascript:processAutoSave('<%=idBaseRow21%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltHazAreaList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltHazAreaList" id="ltHazAreaList" type="java.util.Collection" />
																	<logic:iterate name="ltHazAreaList" id="ltHazArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHazArea" property="key" />' > <bean:write name="ltHazArea" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardArea<%=idBaseRow21%>', '<bean:write name="ratingObj" property="ltHazardAreaId" filter="false" />'); </script>
							<%	idBaseRow21 = idBaseRow21+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 22) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"><strong>Gas Group</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"><strong>Gas Group</strong></td>
						<% } %>
							
							<%  int idBaseRow22 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="gasGroup<%=idBaseRow22%>" id="gasGroup<%=idBaseRow22%>" title="Gas Group" onchange="javascript:processGasGroupChange('<%=idBaseRow22%>');" onfocus="javascript:processAutoSave('<%=idBaseRow22%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltGasGroupList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltGasGroupList" id="ltGasGroupList" type="java.util.Collection" />
																	<logic:iterate name="ltGasGroupList" id="ltGasGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltGasGroup" property="key" />' > <bean:write name="ltGasGroup" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('gasGroup<%=idBaseRow22%>', '<bean:write name="ratingObj" property="ltGasGroupId" filter="false" />'); </script>
							<%	idBaseRow22 = idBaseRow22+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 23) { %> 
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Hazard Zone</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Hazard Zone</strong></td>
							<% } %>
							
							<%  int idBaseRow23 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardZone<%=idBaseRow23%>" id="hazardZone<%=idBaseRow23%>" title="Hazard Zone" onfocus="javascript:processAutoSave('<%=idBaseRow23%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltHazardZoneList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltHazardZoneList" id="ltHazardZoneList" type="java.util.Collection" />
																	<logic:iterate name="ltHazardZoneList" id="ltHazardZone" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHazardZone" property="key" />' > <bean:write name="ltHazardZone" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardZone<%=idBaseRow23%>', '<bean:write name="ratingObj" property="ltHazardZoneId" filter="false" />'); </script>
							<%	idBaseRow23 = idBaseRow23+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 24) {  // Dust Group   %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Dust Group</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Dust Group</strong></td>
							<% } %>
							
							<%  int idBaseRow24 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="dustGroup<%=idBaseRow24%>" id="dustGroup<%=idBaseRow24%>" title="Dust Group" onchange="javascript:processDustGroupChange('<%=idBaseRow24%>');" onfocus="javascript:processAutoSave('<%=idBaseRow24%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltDustGroupList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltDustGroupList" id="ltDustGroupList" type="java.util.Collection" />
																	<logic:iterate name="ltDustGroupList" id="ltDustGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDustGroup" property="key" />' > <bean:write name="ltDustGroup" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('dustGroup<%=idBaseRow24%>', '<bean:write name="ratingObj" property="ltDustGroupId" filter="false" />'); </script>
							<%	idBaseRow24 = idBaseRow24+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 25) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Temperature Class</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Temperature Class</strong></td>
							<% } %>
							
							<%  int idBaseRow25 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="tempClass<%=idBaseRow25%>" id="tempClass<%=idBaseRow25%>" title="Temperature Class" onfocus="javascript:processAutoSave('<%=idBaseRow25%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltTemperatureClassList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltTemperatureClassList" id="ltTemperatureClassList" type="java.util.Collection" />
										<logic:iterate name="ltTemperatureClassList" id="ltTemperatureClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltTemperatureClass" property="key" />' > <bean:write name="ltTemperatureClass" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('tempClass<%=idBaseRow25%>', '<bean:write name="ratingObj" property="ltTempClass" filter="false" />'); </script>
							<%	idBaseRow25 = idBaseRow25+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 26) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Application</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Application</strong></td>
							<% } %>
							
							<%  int idBaseRow26 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="application<%=idBaseRow26%>" id="application<%=idBaseRow26%>" title="Application" onfocus="javascript:processAutoSave('<%=idBaseRow26%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltApplicationList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltApplicationList" id="ltApplicationList" type="java.util.Collection" />
																	<logic:iterate name="ltApplicationList" id="ltApplication" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltApplication" property="key" />' > <bean:write name="ltApplication" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('application<%=idBaseRow26%>', '<bean:write name="ratingObj" property="ltApplicationId" filter="false" />'); </script>
							<%	idBaseRow26 = idBaseRow26+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 27) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Duty</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Duty</strong></td>
							<% } %>
							
							<%  int idBaseRow27 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="duty<%=idBaseRow27%>" id="duty<%=idBaseRow27%>" title="Duty" onchange="javascript:processFieldsForDuty('<%=idBaseRow27%>');" onfocus="javascript:processAutoSave('<%=idBaseRow27%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltDutyList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltDutyList" id="ltDutyList" type="java.util.Collection" />
																	<logic:iterate name="ltDutyList" id="ltDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDuty" property="key" />' > <bean:write name="ltDuty" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('duty<%=idBaseRow27%>', '<bean:write name="ratingObj" property="ltDutyId" filter="false" />'); </script>
							<%	idBaseRow27 = idBaseRow27+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 28) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>CDF</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>CDF</strong></td>
							<% } %>
							
							<%  int idBaseRow28 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" class="normal"  name="cdf<%=idBaseRow28%>" id="cdf<%=idBaseRow28%>" title="CDF" onfocus="javascript:processAutoSave('<%=idBaseRow28%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltCDFList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltCDFList" id="ltCDFList" type="java.util.Collection" />
																	<logic:iterate name="ltCDFList" id="ltCDF" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltCDF" property="key" />' > <bean:write name="ltCDF" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('cdf<%=idBaseRow28%>', '<bean:write name="ratingObj" property="ltCDFId" filter="false" />'); </script>
							<%	idBaseRow28 = idBaseRow28+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 29) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>No. of Starts</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>No. of Starts</strong></td>
							<% } %>
							
							<%  int idBaseRow29 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" class="normal" name="starts<%=idBaseRow29%>" id="starts<%=idBaseRow29%>" title="No. of Starts" onfocus="javascript:processAutoSave('<%=idBaseRow29%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltStartsList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltStartsList" id="ltStartsList" type="java.util.Collection" />
																	<logic:iterate name="ltStartsList" id="ltStarts" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltStarts" property="key" />' > <bean:write name="ltStarts" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('starts<%=idBaseRow29%>', '<bean:write name="ratingObj" property="ltStartsId" filter="false" />'); </script>
							<%	idBaseRow29 = idBaseRow29+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 30) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Noise Level (Subject to Tolerance)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Noise Level (Subject to Tolerance)</strong></td>
							<% } %>
							
							<%  int idBaseRow30 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" class="normal" name="noiseLevel<%=idBaseRow30%>" id="noiseLevel<%=idBaseRow30%>" title="Noise Level" onfocus="javascript:processAutoSave('<%=idBaseRow30%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltNoiseLevelList" name="newenquiryForm">
																	<bean:define name="newenquiryForm" property="ltNoiseLevelList" id="ltNoiseLevelList" type="java.util.Collection" />
																	<logic:iterate name="ltNoiseLevelList" id="ltNoiseLevel" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltNoiseLevel" property="key" />' > <bean:write name="ltNoiseLevel" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('noiseLevel<%=idBaseRow30%>', '<bean:write name="ratingObj" property="ltNoiseLevel" filter="false" />'); </script>
							<%	idBaseRow30 = idBaseRow30+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 31) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Replacement Motor</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Replacement Motor</strong></td>
							<% } %>
							
							<%  int idBaseRow31 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="isReplacementMotor<%=idBaseRow31%>" id="isReplacementMotor<%=idBaseRow31%>" title="Replacement Motor" onfocus="javascript:processAutoSave('<%=idBaseRow31%>');" onchange="javascript:processFieldsForReplaceMotor('<%=idBaseRow31%>');">
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('isReplacementMotor<%=idBaseRow31%>', '<bean:write name="ratingObj" property="ltReplacementMotor" filter="false" />'); </script>
							<%	idBaseRow31 = idBaseRow31+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 32) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Earlier Supplied Motor Serial No.</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Earlier Supplied Motor Serial No.</strong></td>
							<% } %>
							
							<%  int idBaseRow32 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="earlierSuppliedMotorSerialNo<%=idBaseRow32%>" id="earlierSuppliedMotorSerialNo<%=idBaseRow32%>" class="readonlymini" style="width:100px;" readonly="true" value='<bean:write name="ratingObj" property="ltEarlierMotorSerialNo" filter="false" />' title="Earlier Supplied Motor Serial No." onfocus="javascript:processAutoSave('<%=idBaseRow32%>');" maxlength="20" >
							</td>
							<%	idBaseRow32 = idBaseRow32+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 33) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Standard Delivery (Weeks)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Standard Delivery (Weeks)</strong></td>
							<% } %>
							
							<%  int idBaseRow33 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="standardDelivery<%=idBaseRow33%>" id="standardDelivery<%=idBaseRow33%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltStandardDeliveryWks" filter="false" />' title="Standard Delivery (Weeks)" onfocus="javascript:processAutoSave('<%=idBaseRow33%>');" maxlength="5" >
							</td>
							<%	idBaseRow33 = idBaseRow33+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 34) { %>
						<tr id='item<%=j%>'>
							<% if(j % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd"><strong>Customer Requested Delivery (Weeks)</strong></td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd"><strong>Customer Requested Delivery (Weeks)</strong></td>
							<% } %>
							
							<%  int idBaseRow34 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="customerRequestedDelivery<%=idBaseRow34%>" id="customerRequestedDelivery<%=idBaseRow34%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltCustReqDeliveryWks" filter="false" />' title="Customer Requested Delivery (Weeks)" onfocus="javascript:processAutoSave('<%=idBaseRow34%>');" maxlength="5" >
							</td>
							<%	idBaseRow34 = idBaseRow34+1; %>
							</logic:iterate>
						</tr>
					<% } %>
				<% } %>
				</table>
			</td>
		</tr>
	</tbody>
</table>	
															
								