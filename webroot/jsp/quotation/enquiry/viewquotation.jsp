<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>

<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>
<%
DecimalFormat df = new DecimalFormat("###,###");
DecimalFormat df1 = new DecimalFormat("#0");
%>

<!-------------------------- main table Start  --------------------------------->

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
<td>
<logic:present name="displayPaging" scope="request">
						       <table align="right"> 
						       <tr>
						            <td>
									<fieldset style="width:100%">
									 <table cellpadding="0" cellspacing="2" border="0"> 
						      		 <tr>
						            <td style="text-align:center">
	      								<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch">
	      								Return to search<img src="html/images/back.gif" border="0" align="absmiddle" /></a>
	      							</td>
						        </tr>
						        <tr class="other" align="right">
						            <td align="right">
							            <logic:present name="previousId" scope="request">	
							            	<bean:define name="previousId" id="previousId" scope="request"/>
											<bean:define name="previousIndex" id="previousIndex" scope="request"/>	
											<bean:define name="previousStatus" id="previousStatus" scope="request"/>				
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img src="html/images/PrevPage.gif" alt="" border="0" align="absmiddle" /></a>
							            </logic:present>
							
							            <logic:present name="displayMessage" scope="request">
							            	<bean:write name="displayMessage" scope="request"/>
							            </logic:present>
							
							            <logic:present name="nextId" scope="request">
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img src="html/images/NextPage.gif" alt="Next Request" border="0" align="absmiddle" /></a>
							            </logic:present>            	
									 </td>
						       		</tr>
						       		</table> 										
								</fieldset>           	
									</td>
						       	</tr>
						       	</table>
	</logic:present>
</td>
</tr>
  <tr>
    <td height="100%" valign="top">    
	<html:form  method="post" action="createEnquiry.do"><br>
	<html:hidden property="invoke" value="viewReturnEnquiry"/>
	<html:hidden property="dispatch" />
	<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
	
    <logic:present name="enquiryDto" scope="request">    
    <html:hidden property="enquiryId" name="enquiryForm"/>
    <html:hidden property="enquiryNumber" name="enquiryForm" />
    
		<table width="800"  border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#003366">
		 <logic:notEmpty name="enquiryDto" property="ratingObjects">
		  <tr>
            <td><table width="800"  border="0" align="center" cellpadding="1" cellspacing="0">
              <tr>
                <td><img src="html/images/melogo.jpg" style="width:350px;"></td>
              </tr>
              <tr>
                <td class="blueheader" style="font-size:24px " align="right"><bean:message key="quotation.create.label.quotationtocustomer"/></td>
              </tr>
              <tr>
                <td class="other" align="right"><bean:write name="enquiryDto" property="enquiryNumber"/> &nbsp;&nbsp;</td>
              </tr>
              <tr>
              <td colspan="5"  >&nbsp;</td>
          	  </tr>

         	  <tr class="section-head" >
              <td class="grey-dark" colspan="5"> &nbsp; </td>
               </tr>
              
              <tr>
                <td>
                
                  <div class="tablerow">
  					<div class="tablecolumn" >
  					<table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>
                    <logic:notEqual name="enquiryDto" property="customerName" value="">
                    <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.customer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="customerName"/>
                      </td>
                    
                    </tr>
                    </logic:notEqual>
                    <logic:notEqual name="enquiryDto" property="endUser" value="">
                    <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.enduser"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="enquiryDto" property="endUser"/>
                       </td>
                    
                    </tr>
                    </logic:notEqual>
                    <logic:notEqual name="enquiryDto" property="revision" value="">
  					<tr>
  	                  <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.revision"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="enquiryDto" property="revision"/>
                       </td>
  					
  					</tr>
  					</logic:notEqual>
					</table>
			        </div>
  				   <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>
                    <logic:notEqual name="enquiryDto" property="projectName" value="">
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.projectname"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="projectName"/>
                      </td>    
                    
                    </tr>
                    </logic:notEqual>
                    <logic:notEqual name="enquiryDto" property="enquiryReferenceNumber" value="">
                    <tr>
                     <td class="label-text" style="width:50% ">
                      	<bean:message key="quotation.create.label.enquiryreferencenumber"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="enquiryReferenceNumber"/>
                      </td>    
                    
                    </tr>
                    </logic:notEqual>
                    <logic:notEqual name="enquiryDto" property="lastModifiedDate" value="">
                    <tr>
                      <td class="label-text" style="width:50% ">
                      	<bean:message key="quotation.create.label.issuedate"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryDto" property="lastModifiedDate"/>
                      </td>    
                    
                    </tr>
                    </logic:notEqual>
  					
					</table>
				  </div>
				</div> 
				
                
                
                </td>
                </tr>
                <tr>
                <td>
				<!-----------------------Rating block start -------------------------------------->
				
				
				<table width="100%"  border="0" cellspacing="0" cellpadding="2">
				 <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
					<logic:notEmpty name="ratingObject" property="technicalDto">
				   <bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>	
				  
                  
				  <tr class="section-head" >
                    <td class="grey-dark" > <bean:message key="quotation.create.label.rating"/>&nbsp;<bean:write name="ratingObject" property="ratingNo"/> </td>
                      <td colspan="2" class="grey-dark" >

                      &nbsp;
                      </td>
                      <td align="right" class="grey-dark">
                      &nbsp;
                      </td>
                      </tr>
                  <tr>
                    <td colspan="2" class="section-head"><span class="blueheader"><bean:message key="quotation.create.label.technicalrequirement"/></span></td>
                      <td class="section-head">&nbsp;</td>
                      <td align="right" class="section-head">&nbsp;</td>
                      </tr>
                  <tr>
                    <td colspan="4">
 
                    <%String MCUnit="",AreaClassification="",frameSize="",rpm="",flcAmps="",nlCurrent="",strtgCurrent="",fltSQCAGE="",serviceFactor="",
                    		lrTorque="",bkw="",potFLT="",rv="",ra="",pllCurr1="",pllEff1="",pllPf1="",
                    				pllCurr2="",pllEff2="",pllPf2="",
                    				pllCurr3="",pllEff3="",pllPf3="",
                    				pllCurr4="",pllPf4="",pllCurr5="",pllEff4="",pllPf5="",sstHot="",
                    				vibrationLevel="",sstCold="",mainTermBoxPS="",
                    			    startTimeRV="",neutralTermBox="",startTimeRV80="",
                    			    cableEntry="",auxTermBox="",statorConn="",noBoTerminals="",
                    			    rotation="",coolingSystem="",motorTotalWgt="",
                    			    spaceHeater="",bearingDENDE="",lubrication="",windingRTD="",
                    			    noiseLevel="",bearingRTD="",rtGD2="",minStartVolt="",
                    			    migD2Load="",balancing="",bearingThermoDT="",spmNipple="",
                    			    airThermo="",keyPhasor="",vibeProbMPads="",encoder="",
                    			    paintColor="",speedSwitch="",surgeSuppressors="",currTransformers="",vibProbes="",
                    			    termBoxName="",zone="",gasgroup="",deviation_clarification="";
                    				 %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
  				 <bean:define id="tech1" name="technicalObject1" property="formattedCommercialMCUnit"/>
  				 <bean:define id="tech2" name="technicalObject1" property="areaClassification"/>
  				 <bean:define id="tech3" name="technicalObject1" property="frameSize"/>
  				 <bean:define id="tech4" name="technicalObject1" property="rpm"/>
  				 <bean:define id="tech5" name="technicalObject1" property="flcAmps"/>
  		         <bean:define id="tech6" name="technicalObject1" property="nlCurrent"/>
  		         <bean:define id="tech7" name="technicalObject1" property="strtgCurrent"/>
  		         <bean:define id="tech8" name="technicalObject1" property="fltSQCAGE" />
  		         <bean:define id="tech9" name="technicalObject1" property="serviceFactor"/>
  		         <bean:define id="tech10" name="technicalObject1" property="lrTorque"/>
  		         <bean:define id="tech11" name="technicalObject1" property="bkw"/>
  		         <bean:define id="tech12" name="technicalObject1" property="potFLT"/>
  		         <bean:define id="tech13" name="technicalObject1" property="rv" />
  		         <bean:define id="tech14" name="technicalObject1" property="ra" />
  		         
  		         <bean:define id="tech15" name="technicalObject1" property="pllCurr1" />
  		         <bean:define id="tech16" name="technicalObject1" property="pllEff1" />
  		         <bean:define id="tech17" name="technicalObject1" property="pllPf1" />
  		         
  		         <bean:define id="tech18" name="technicalObject1" property="pllCurr2" />
  		         <bean:define id="tech19" name="technicalObject1" property="pllEff2" />
  		         <bean:define id="tech20" name="technicalObject1" property="pllPf2" />
  				 
  				 <bean:define id="tech21" name="technicalObject1" property="pllCurr3" />
  		         <bean:define id="tech22" name="technicalObject1" property="pllEff3" />
  		         <bean:define id="tech23" name="technicalObject1" property="pllpf3" />
  				 
  				 <bean:define id="tech24" name="technicalObject1" property="pllCurr4" />

  		         <bean:define id="tech25" name="technicalObject1" property="pllpf4" />
  		         
  		         <bean:define id="tech26" name="technicalObject1" property="sstHot" />
  		         
  				<bean:define id="tech27" name="technicalObject1" property="vibrationLevel" />
  				<bean:define id="tech28" name="technicalObject1" property="sstCold" />
  				<bean:define id="tech29" name="technicalObject1" property="mainTermBoxPS" />
  				
  				 <bean:define id="tech30" name="technicalObject1" property="startTimeRV" />
  			     <bean:define id="tech31" name="technicalObject1" property="neutralTermBox" />
  			     <bean:define id="tech32" name="technicalObject1" property="startTimeRV80" />
  		  	     <bean:define id="tech33" name="technicalObject1" property="cableEntry" />	
  		  	     	 
  		  	     <bean:define id="tech34" name="technicalObject1" property="statorConn" />
  		  	     <bean:define id="tech35" name="technicalObject1" property="auxTermBox" />
  		  	     <bean:define id="tech36" name="technicalObject1" property="noBoTerminals" />
  		  	     <bean:define id="tech37" name="technicalObject1" property="rotation" />
  		  	     <bean:define id="tech38" name="technicalObject1" property="coolingSystem"/>
  		  	     <bean:define id="tech39" name="technicalObject1" property="motorTotalWgt"/>
  		  	     <bean:define id="tech40" name="technicalObject1" property="spaceHeater"/>
  		  	     <bean:define id="tech41" name="technicalObject1" property="bearingDENDE"/>
  		  	     
  		  	     <bean:define id="tech42" name="technicalObject1" property="lubrication"/>
  		  	     <bean:define id="tech43" name="technicalObject1" property="windingRTD"/>

  		  	     <bean:define id="tech44" name="technicalObject1" property="noiseLevel"/>
  		  	     <bean:define id="tech45" name="technicalObject1" property="bearingRTD"/>
  		  	     
  		  	     <bean:define id="tech46" name="technicalObject1" property="rtGD2"/>
  		  	     <bean:define id="tech47" name="technicalObject1" property="minStartVolt"/>
  		  	     
  		  	     <bean:define id="tech48" name="technicalObject1" property="migD2Load"/>
  		  	     <bean:define id="tech49" name="technicalObject1" property="balancing"/>
  		  	    
  		  	      <bean:define id="tech50" name="technicalObject1" property="bearingThermoDT"/>
  		  	     <bean:define id="tech51" name="technicalObject1" property="spmNipple"/>

  		  	      <bean:define id="tech52" name="technicalObject1" property="airThermo"/>
  		  	     <bean:define id="tech53" name="technicalObject1" property="keyPhasor"/>
  		  	     
  		  	     <bean:define id="tech54" name="technicalObject1" property="vibeProbMPads" />
  		  	      <bean:define id="tech55" name="technicalObject1" property="encoder" />
  		  	      
  		  	       <bean:define id="tech56" name="technicalObject1" property="paintColor" />
  		  	      <bean:define id="tech57" name="technicalObject1" property="speedSwitch" />
  		  	      
  		  	      <bean:define id="tech58" name="technicalObject1" property="surgeSuppressors" />
  		  	      <bean:define id="tech59" name="technicalObject1" property="currTransformers" />
  		  	      <bean:define id="tech60" name="technicalObject1" property="vibProbes" />
  		  	     
  		  	     <bean:define id="tech61" name="technicalObject1" property="termBoxName" />

				<bean:define id="tech62" name="technicalObject1" property="zone" />
  		  	    <bean:define id="tech63" name="technicalObject1" property="gasGroup" /> 
  		  	    
  		  	    <bean:define id="tech64" name="technicalObject1" property="deviation_clarification" />
  		  	    
  		  	    <bean:define id="tech65" name="technicalObject1" property="pllCurr5" />
  		  	    <bean:define id="tech66" name="technicalObject1" property="pllEff4" /> 
  		  	    
  		  	    <bean:define id="tech67" name="technicalObject1" property="pllPf5" />
  		  	    
  		  	     
  		  	     
  		  	     
  				 <%
  				 
  				 MCUnit=String.valueOf(tech1);
  				AreaClassification=String.valueOf(tech2);
  				frameSize=String.valueOf(tech3);
  				rpm=String.valueOf(tech4);
  				flcAmps=String.valueOf(tech5);
  				nlCurrent=String.valueOf(tech6);
  				strtgCurrent=String.valueOf(tech7);
  				fltSQCAGE=String.valueOf(tech8);
  				serviceFactor=String.valueOf(tech9);
  				lrTorque=String.valueOf(tech10);
  				bkw=String.valueOf(tech11);
  				potFLT=String.valueOf(tech12);
  				rv=String.valueOf(tech13);
  				ra=String.valueOf(tech14);
  				pllCurr1=String.valueOf(tech15);
  				pllEff1=String.valueOf(tech16);
  				pllPf1=String.valueOf(tech17);
  				
  				pllCurr2=String.valueOf(tech18);
  				pllEff2=String.valueOf(tech19);
  				pllPf2=String.valueOf(tech20);
  				
  				pllCurr3=String.valueOf(tech21);
  				pllEff3=String.valueOf(tech22);
  				pllPf3=String.valueOf(tech23);
  				
  				pllCurr4=String.valueOf(tech24);
  				
  				pllPf4=String.valueOf(tech25);
  				sstHot=String.valueOf(tech26);
  				
  				vibrationLevel=String.valueOf(tech27);
  				sstCold=String.valueOf(tech28);
  				mainTermBoxPS=String.valueOf(tech29);
  				startTimeRV=String.valueOf(tech30);
  				neutralTermBox=String.valueOf(tech31);
  				startTimeRV80=String.valueOf(tech32);
  				cableEntry=String.valueOf(tech33);
  				statorConn=String.valueOf(tech34);
  				auxTermBox=String.valueOf(tech35);
  				noBoTerminals=String.valueOf(tech36);
  				rotation=String.valueOf(tech37);
  				coolingSystem=String.valueOf(tech38);
  				motorTotalWgt=String.valueOf(tech39);
  				spaceHeater=String.valueOf(tech40);
  				bearingDENDE=String.valueOf(tech41);
  				lubrication=String.valueOf(tech42);
  				windingRTD=String.valueOf(tech43);
  				noiseLevel=String.valueOf(tech44);
  				bearingRTD=String.valueOf(tech45);
  				rtGD2=String.valueOf(tech46);
  				minStartVolt=String.valueOf(tech47);
  				migD2Load=String.valueOf(tech48);
  				
  				balancing="2.5 grade as per ISO 1940";
  				bearingThermoDT=String.valueOf(tech50);
  				spmNipple=String.valueOf(tech51);
  				
  				airThermo=String.valueOf(tech52);
  				keyPhasor=String.valueOf(tech53);
  				vibeProbMPads=String.valueOf(tech54);
  				
  				encoder=String.valueOf(tech55);
  				
  				paintColor=String.valueOf(tech56);
  				speedSwitch=String.valueOf(tech57);
  				surgeSuppressors=String.valueOf(tech58);
  				currTransformers=String.valueOf(tech59);
  				vibProbes=String.valueOf(tech60);
  				
  				termBoxName=String.valueOf(tech61);
  				
  				zone=String.valueOf(tech62);
  				gasgroup=String.valueOf(tech63);
  				deviation_clarification=String.valueOf(tech64);
  				pllCurr5=String.valueOf(tech65);
  				pllEff4=String.valueOf(tech66);
  				pllPf5=String.valueOf(tech67);
  				
  				

  				
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	

  				 
  		<div class="tablerow"> 
  				 <div class="tablecolumn" > 
  				  <table width="100%">
				   <tr>
                       <td colspan="2"  >&nbsp;</td>
          			 </tr>
          			  <logic:notEqual name="ratingObject" property="quantity" value="">
          			  <tr>
                      
  					 <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.quantityrequired"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      	<bean:write name="ratingObject" property="quantity"/>
                      </td>
          			 </tr>
          			 </logic:notEqual>
          			 <logic:notEqual name="ratingObject" property="applicationName" value="">
          			 <tr>
          			  <td class="label-text" style="width:50%">
                      	<bean:message key="quotation.create.label.application"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%">
                      	<bean:write name="ratingObject" property="applicationName"/> 
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <logic:notEqual name="ratingObject" property="scsrName" value="">
          			 <tr>
          			   <td class="label-text" style="width: 50%;">
                      	<bean:message key="quotation.create.label.scsr"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width: 50%;">
                      	<bean:write name="ratingObject" property="scsrName"/> 
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <%if(AreaClassification!=""){ %>
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offer.label.areaclassification"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<%=AreaClassification %> 
                      </td>
          			 
          			 </tr>
          			 <%} if(zone!=""){ %>
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.zone"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<%=zone %> 
                      </td>
          			 
          			 </tr>
          			 <%} if(gasgroup!=""){ %>
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.gasgroup"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<%=gasgroup %> 
                      </td>
          			 
          			 </tr>
          			 <%} %>
          			 
          			 <logic:notEqual name="ratingObject" property="enclosureName" value="">
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.enclosure"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	
                      	<bean:write name="ratingObject" property="enclosureName"/>
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <%if(frameSize!=""){ %>
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.framesize"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	
                      	<%=frameSize %>
                      </td>
          			 
          			 </tr>
          			 <%} %>
          			 <logic:notEqual name="ratingObject" property="mountingName" value="">
          			 <tr>
          			  <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.mounting"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	
                      <bean:write name="ratingObject" property="mountingName" />
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <logic:notEqual name="ratingObject" property="kw" value="">
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.kw"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	
                      <bean:write name="ratingObject" property="kw" />
                      <logic:notEqual  name="ratingObject" property="kw" value="">
                       <bean:write name="ratingObject" property="ratedOutputUnit" />
                      
                      </logic:notEqual>
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <%if(serviceFactor!=""){ %>
          			 <tr>
          			  <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offer.label.servicefactor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<%=serviceFactor %>
                      </td>
          			 
          			 </tr>
          			 <%} %>
          			 <%if(bkw!=""){ %>
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offer.label.bkw"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%;">
                      	<%=bkw %>
                      	<%if(bkw!=""){ %>
                      	<bean:message key="quotation.option.kw"/>
                      	<%} %>
                      </td>
          			 
          			 </tr>
          			 <%} %>
          			 <logic:notEqual name="ratingObject" property="dutyName" value="">
          			 <tr>
          			 <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.duty"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%;">
                      <bean:write name="ratingObject" property="dutyName" />
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
  				  </table>

        		 </div> 
  				 <div class="tablecolumn" > 
  				   <table width="100%">
				   <tr>
                       <td colspan="2"  >&nbsp;</td>
          			 </tr>
          			 <logic:notEqual name="ratingObject" property="volts" value="">
          			 <tr>
          			   <td class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.volts"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<logic:notEqual name="ratingObject" property="volts" value="">
                  			<bean:write name="ratingObject" property="volts"/> 
                  		</logic:notEqual>
                  	
                  		<logic:notEqual name="ratingObject" property="voltsVar" value="">
                  			+/-<bean:write name="ratingObject" property="voltsVar"/> %
                  		</logic:notEqual>
                      </td>    
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <logic:notEqual name="ratingObject" property="frequency" value="">
          			 <tr>
          			 <td class="label-text">
                      	<bean:message key="quotation.create.label.frequency"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<logic:notEqual name="ratingObject" property="frequency" value="">
                  			<bean:write name="ratingObject" property="frequency"/>
                  		</logic:notEqual>
                  		<logic:notEqual name="ratingObject" property="frequency" value="">
	                  		<bean:message key="quotation.units.hz"/>
	               		</logic:notEqual>
                      </td>
            
          			 </tr>
          			 </logic:notEqual>
          			 <logic:notEqual name="ratingObject" property="poleName" value="">
          			 <tr>
          			   <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.create.label.pole"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<bean:write name="ratingObject" property="poleName"/> 
                      </td>
          			 
          			 </tr>
          			 </logic:notEqual>
          			 <%if(rpm!=""){ %>
          			 <tr>
          			    <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.rpm"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      	<%=rpm %> 
                      	<%if(rpm!=""){ %>
                      	<bean:message key="quotation.units.rmin"/>
                      	<%} %>
                     
                      </td>
          			 
          			 </tr>
          			 <%} %>
          			 <%if(flcAmps!=""){ %>
          			 <tr>
          			   <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.flcamps"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                      <%=flcAmps %>
                      <%if(flcAmps!="") {%>
                      <bean:message key="quotation.units.a"/>
                      <%} %>
                      </td>
          			 
          			 </tr>
          			 <%}if(nlCurrent!=""){%>
          			 <tr>
          			 <td height="100%" class="label-text" style="width:50%;">
                      <bean:message key="quotation.offer.label.noloadcurrent"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=nlCurrent %>
                     <%if(nlCurrent!=""){ %>
                     <bean:message key="quotation.units.a"/>
                     <%} %>
                      </td>
          			 
          			 </tr>
          			 <%}if(strtgCurrent!=""){ %>
          			 <tr>
          			   <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offer.label.startingcurrent"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=strtgCurrent %>
                      </td>
          			 
          			 </tr>
          			 <%}if(fltSQCAGE!=""){ %>
          			 <tr>
          			 <td height="100%" class="label-text" style="width:50%;">
                      <bean:message key="quotation.offerdata.technical.label.flt"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=fltSQCAGE %>
                     <%if(fltSQCAGE!=""){ %>
                     <bean:message key="quotation.units.nm"/>
                     <%} %>
                      </td>
          			 
          			 </tr>
          			 <%}if(lrTorque!=""){ %>
          			 <tr>
          			  <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offer.label.lockedrotortorque"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=lrTorque %>
                      </td>
          			 
          			 </tr>
          			 <%}if(potFLT!=""){ %>
          			 <tr>
          			   <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.potflt"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=potFLT %>
                      </td>
          			 
          			 </tr>
          			 <%}if(rv!=""){ %>
          			 <tr>
          			 <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.rvslip"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=rv %>
                      </td>
          			 </tr>
          			 <%}if(ra!=""){ %>
          			 <tr>
          	           <td height="100%" class="label-text" style="width:50%;">
                      	<bean:message key="quotation.offerdata.technical.label.raslip"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50%;">
                      
                     <%=ra %>
                      </td>
          			 
          			 </tr>
          			 <%} %>
  				  </table>
  				 
				 </div> 
             </div>
     
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
				
<%if(pllCurr1!="" || pllEff1!="" || pllPf1!="" || pllCurr2!="" || pllEff2!="" || pllPf2!="" || pllCurr3!="" || pllEff3!="" || pllPf3!="" || pllCurr4!="" && pllPf4!=""){  %>				
<hr />
<tr>
<td width="30%" class="label-text"><bean:message key="quotation.offer.label.loadcharacteristics"/></td>
<td width="2%"></td>
<td width="13%" class="label-text"><bean:message key="quotation.offer.label.load"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.currenta"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.efficiency"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.powerfactor"/></td>
</tr>
<%} if(pllCurr1!="" || pllEff1!="" || pllPf1!=""){  %>
<tr>
<td ><bean:message key="quotation.offer.label.plldesc"/></td>
<td></td>
<td ><bean:message key="quotation.offer.label.load100"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr1%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff1%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf1%></td>
</tr>
<%} 
if(pllCurr2!="" || pllEff2!="" || pllPf2!="")
{
%>
<tr>
<td>&nbsp;</td>
<td></td>
<td><bean:message key="quotation.offer.label.load75"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr2%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff2%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf2%></td>
</tr>
<%}
if(pllCurr3!="" || pllEff3!="" || pllPf3!="")
{
%>

<tr>
<td>&nbsp;</td>
<td></td>
<td ><bean:message key="quotation.offer.label.load50"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr3%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff3%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf3%></td>
</tr>
<%} 
if(pllCurr4!="" || pllPf4!="")
{
%>

<tr>
<td>&nbsp;</td>
<td></td>
<td><bean:message key="quotation.offer.label.loadstart"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr4%></td>
<td></td>
<td >&nbsp;</td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf4%></td>
</tr>
<%}
if(pllCurr5!="" || pllEff4!="" || pllPf5!="")
{
%>

<tr>
<td>&nbsp;</td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.loaddutypoint"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr5%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff4%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf5%></td>
</tr>
</table> 
<%}%>

<hr/>
<div class="tablerow">
  <div class="tablecolumn" >
  <table width="100%">
  <tr><td colspan="5"></td></tr>
  <%if(sstHot!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.safestalltimeh"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=sstHot%>
                      	<%if(sstHot!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  </tr>
  <%}if(sstCold!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.safestalltimec"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=sstCold%>
                      	<%if(sstCold!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
 
  </tr>
  <%}if(startTimeRV!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.startingtimerv"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=startTimeRV%>
                      	<%if(startTimeRV!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  
  </tr>
  <%}if(startTimeRV80!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.startingtimerv80"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=startTimeRV80%>
                      	<%if(startTimeRV80!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="insulationName" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.insulationclass"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	
                      <bean:write name="ratingObject" property="insulationName"/>
                      </td>
  
  </tr>
  </logic:notEqual>
    <logic:notEqual name="ratingObject" property="tempRaiseDesc" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.temprise"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	
                      <bean:write name="ratingObject" property="tempRaiseDesc"/>
                      </td>
  
  </tr>
  </logic:notEqual>
     <logic:notEqual name="ratingObject" property="ambience" value="">
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.ambience"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="ambience"/>
                     <logic:notEqual name="ratingObject" property="ambience" value="">
                     &nbsp;<bean:message key="quotation.units.c"/>
                     </logic:notEqual> 
                      </td>  
  </tr>
  </logic:notEqual>
       <logic:notEqual name="ratingObject" property="altitude" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.altitude"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="altitude"/>
                    <logic:notEqual name="ratingObject" property="altitude" value="">
                    &nbsp;<bean:message key="quotation.units.masl"/>
                    </logic:notEqual>
                      
                      </td>                     
  
  </tr>
  </logic:notEqual>
         <logic:notEqual name="ratingObject" property="degreeOfProName" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.degpro"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="degreeOfProName"/>
                      
                      </td>                     
  
  </tr>
  </logic:notEqual>
  <%if(coolingSystem!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.coolingsystem"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=coolingSystem %>
                      
                      </td>                     
  
  </tr>
  <%}if(bearingDENDE!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.bearingdende"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=bearingDENDE %>
                      
                      </td>
  </tr>
  <%}if(lubrication!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.lubrication"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=lubrication %>
                      
                      </td>
  </tr>
  <%}if(!noiseLevel.equals("0") && !noiseLevel.equals("")){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offerdata.technical.label.noiselevel"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=noiseLevel %>
                      <%if(!noiseLevel.equals("0") && !noiseLevel.equals("")){ %>
                      &nbsp;<bean:message key="quotation.units.dba"/>
                      <%} %>
                      
                      </td>                     
  
  </tr>
  <%}if(rtGD2!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offerdata.technical.label.rtgd"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=rtGD2 %>
                      <%if(rtGD2!=""){ %>
                      &nbsp;<bean:message key="quotation.units.kgm2"/>
                      <%} %>
                      
                      </td>                     
  
  </tr>
  <%}if(migD2Load!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.moi"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=migD2Load %>
                      
                      </td>                     
  
  </tr>
  <%}if(balancing!=""){ %>
                       <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.balancing"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=balancing %>
                      
                      </td>                     
                    </tr> 
  </table>
</div>
<div class="tablecolumn" >
  <table width="100%">
  <tr><td colspan="5"></td></tr>
  <%if(vibrationLevel!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offerdata.technical.label.vibrationlevel"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=vibrationLevel %>
                     </td>    
  
  </tr>
  <%}if(mainTermBoxPS!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.maintbps"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=mainTermBoxPS %>
                     </td>    
  
  </tr>
  <%}if(neutralTermBox!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.neutraltb"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=neutralTermBox %>
                     </td>    
  
  </tr>
  <%}if(cableEntry!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.cableentry"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=cableEntry %>
                     </td>    
  
  </tr>
  <%}if(auxTermBox!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.auxterminalbox"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=auxTermBox %>
                     </td>    
  
  </tr>
  <%}if(statorConn!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.statorconnection"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	    <%=statorConn %>
                     </td>    
  
  </tr>
  <%}if(noBoTerminals!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.nboterminals"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  
              <%=noBoTerminals %>
                     </td> 
                        
  
  </tr>
  <%}if(rotation!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.rotation"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  
              <%=rotation %>
                     </td>    
  
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="directionOfRotName" value="">
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.rotdirection"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <bean:write name="ratingObject" property="directionOfRotName"/>
                     </td>    
  
  </tr>
  </logic:notEqual>
  <%if(motorTotalWgt!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.motortotalwt"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=motorTotalWgt %>
                  	  <%if(motorTotalWgt!=""){ %>
                  	  <bean:message key="quotation.units.kg"/>
                  	  <%} %>
                     </td>  
                       
  
  </tr>
  <%}if(spaceHeater!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.spaceheater"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=spaceHeater %>
                     </td>    
  
  </tr>
  <%}if(windingRTD!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.windingrtd"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=windingRTD %>
                     </td>    
  
  </tr>
  <%}if(bearingRTD!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.bearingrtd"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=bearingRTD %>
                     </td>    

  
  </tr>
  <%}if(!minStartVolt.equals("0") && !minStartVolt.equals("")){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.minstartvoltage"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=minStartVolt %>
                     </td>    
  
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="methodOfStg" value="">
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.stgmethod"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                <bean:write name="ratingObject" property="methodOfStg"/>
                     </td>    
  
  </tr>
  
  </logic:notEqual>
    <%}if(termBoxName!=""){%>
            
                    <tr>
                    <td class="label-text"  style="width:25%">
                    <bean:message key="quotation.create.label.termbox"/>
                    </td>
                    <td class="formContentview-rating-b0x"  style="width:25%">
                    <%=termBoxName %>
                    </td>
                    </tr>
        <%} %>
  
  
  </table>

</div>
</div>


  <hr />  
  <div class="tablerow">
  <div class="tablecolumn" >
         <table width="100%">
				<tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>
          			 <%if(bearingThermoDT!=""){ %>
          			 <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.dialtypebearingthermometer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=bearingThermoDT%>
                      	
                      </td>
                      
                      
 </tr>
 <%} if(airThermo!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:40%">
                      	<bean:message key="quotation.offer.label.airthermometer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:40%">
                      	<%=airThermo%>
                      </td>
                      
 
 </tr>
 <%} if(vibeProbMPads!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.vibprobemountingpads"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=vibeProbMPads %>
                      </td>
 
 </tr>
 <%} if(encoder!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.encoder"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=encoder %>
                      </td>
 
 </tr>
 <%} if(speedSwitch!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.speedswitch"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=speedSwitch %>
                      </td>
 
 </tr>
 <%} if(surgeSuppressors!=""){ %>
                     <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.surgesuppressors"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=surgeSuppressors %>
                      </td>
                     </tr>
 <%}if(currTransformers!=""){ %>
                  <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.currenttransformers"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=currTransformers %>
                      </td>
                     </tr>
                     <%} if(vibProbes!=""){ %>  
                     <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.vibprobes"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=vibProbes %>
                      </td>
                     </tr>
                     <%} %>
 
 </table>
  </div>
  <div class="tablecolumn" >
           <table width="100%">
				<tr>
                       <td colspan="2"  >&nbsp;</td>
          			 </tr>
          		<%if(spmNipple!=""){ %>	 
          			 <tr>
                      
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.spmnipple"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=spmNipple %>
                     </td>  
                       

 </tr>
 <%} if(keyPhasor!=""){ %>	 
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.keyphasor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=keyPhasor %>
                     </td>   
                      
 
 </tr>
 <%} %>
 <logic:notEqual name="ratingObject" property="shaftExtName" value="">
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.shaftext"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  			<bean:write name="ratingObject" property="shaftExtName"/>
                     </td>    
 
 </tr>
 </logic:notEqual>
  <logic:notEqual name="ratingObject" property="paint" value="">
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.paint"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  			<bean:write name="ratingObject" property="paint"/>
                     </td>    
 
 </tr>
 </logic:notEqual>
 <%if(paintColor!=""){ %>
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.paintcolor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  		<%=paintColor %>
                     </td>    
 
 </tr>
 <%} %>
 </table>
  
  </div>
</div>
  
                   
                      </td>
                    </tr>
                    
    
    <%if(!deviation_clarification.equals("")){ %>  
    
     <tr class="section-head" >
                    <td class="grey-dark" > &nbsp;&nbsp; </td>
                      <td colspan="2" class="grey-dark" >
                      &nbsp;
                      </td>
                      <td align="right" class="grey-dark">
                      &nbsp;
                      </td>
                      </tr>
                  <tr>
                    <td colspan="2" class="section-head"><span class="blueheader"><bean:message key="quotation.create.label.deviationandclarification"/></span></td>
                      <td class="section-head">&nbsp;</td>
                      <td align="right" class="section-head">&nbsp;</td>
                      </tr>
                  <tr>
                  <td  class="formContentview-rating-b0x" ><%=deviation_clarification %> </td>
          	   <td colspan="3" class="formContentview-rating-b0x">&nbsp;</td>
              </tr>
    
    
               <%} %>
 
                  
          				</logic:notEmpty>
					    </logic:iterate>
                  </table>
				  
				  </td>
              </tr>
              <tr>
			  <td>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr class="section-head" >
<td class="grey-dark" colspan="16"> &nbsp; </td>
</tr>
                                   
<tr>
<td colspan="16" class="section-head"><span class="blueheader"><bean:message key="quotation.offerdata.commercial.label.heading"/></span></td>
</tr>
<tr>
<td colspan="16"  >&nbsp;</td>
</tr>
                                   
<%
Double quantity=new Double(0);
BigDecimal totalprice=new BigDecimal("0");
%>
				    <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
				
                      <bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>
                      <tr>
<td  width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.units"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.pole"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.search.frame"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.unitprice"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>

<td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.totalprice"/></td>


</tr>  
                      
                      	
                      <tr>
<td   width="10%" class="formContentview-rating-b0x"><%=idx+1 %></td>

<td width="10%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="kw" /></td>

<td width="10%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ratedOutputUnit" /></td>

<td width="10%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="poleName" /></td>

<td width="10%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="volts"/></td>

                    <%String frameSize="",unitpriceMultiplier_rsm="",totalpriceMultiplier_rsm=""; %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
  				 <bean:define id="tech1" name="technicalObject1" property="frameSize"/>
  				 <bean:define id="tech2" name="technicalObject1" property="unitpriceMultiplier_rsm"/>
  	  			 <bean:define id="tech3" name="technicalObject1" property="totalpriceMultiplier_rsm"/>	  	     
  				 <%
  				 
  				frameSize=String.valueOf(tech1);
  				unitpriceMultiplier_rsm=String.valueOf(tech2);
  				totalpriceMultiplier_rsm=String.valueOf(tech3);
  				
  				

  				
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	



<td width="10%" class="formContentview-rating-b0x"><%=frameSize %></td>

<td width="10%" class="formContentview-rating-b0x"><%=unitpriceMultiplier_rsm %></td>

<td width="10%" class="formContentview-rating-b0x"><strong><bean:write name="ratingObject" property="quantity"/></strong>
<bean:define id="qnt" name="ratingObject" property="quantity"/>
</td>

<td width="10%" class="formContentview-rating-b0x"><strong><%=totalpriceMultiplier_rsm %></strong></td>

</tr>  
   <%
   
    if(totalpriceMultiplier_rsm!="" && totalpriceMultiplier_rsm!="0")
    {
        if(totalpriceMultiplier_rsm.indexOf(",") != -1){
      	 String unitprice=totalpriceMultiplier_rsm.replaceAll(",", "");
      	 BigDecimal a=new BigDecimal(unitprice);
      	totalprice=totalprice.add(a);
        }

    	
    }
    if(qnt!="" && qnt!="0")
   {
	   quantity=quantity+Double.parseDouble(String.valueOf(qnt));
   }
    %>    
    
 
 <logic:notEmpty name="ratingObject" property="addOnList">
 	 <tr>
 	 
 	 <td colspan="16">
 	 <hr/>
 <fieldset>
 <legend class="blueheader"><bean:message key="quotation.offerdata.addon.label.heading"/></legend>
 	 
 	 <table style="width: 100%;" > 
 	  <tr>
 <td width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.addon"/></td>
 <td width="2%"></td>
<td width="30%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.addontype"/></td>
<td width="2%"></td>
<td width="25%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>
<td width="2%"></td>
<td width="25%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.unitprice"/></td>
</tr>
 	  <%int j=0; %>
 	  
					  
					  	 <logic:iterate name="ratingObject" property="addOnList"
									               id="addOnDto" indexId="idx1" 
									               type="in.com.rbc.quotation.enquiry.dto.AddOnDto">	
							<% j = idx1.intValue()+1; %>
							<tr>
							<td  width="10%" class="formContentview-rating-b0x"><%=j %></td>
							<td width="2%"></td>
							<td class="formContentview-rating-b0x" width="35%">
							
							
								<bean:write name="addOnDto" property="addOnTypeText" />
							</td>
							<td width="2%"></td>
							<td class="formContentview-rating-b0x"  width="25%" >							
								<bean:write name="addOnDto" property="addOnQuantity"/>
					  			<input type="hidden" class="short" name="quantitylist"value="<bean:write name="addOnDto" property="addOnQuantity"/>" readonly  maxlength=10>					  			
					 		</td>
							
							
							<td  width="2%">
							</td>
							<td class="formContentview-rating-b0x" width="25%" >							
								<bean:write name="addOnDto" property="formattedAddOnUnitPrice"/>
					  			<input type="hidden" class="short" name="unitlist"  value="<bean:write name="addOnDto" property="addOnUnitPrice"/>" readonly  maxlength=10>					  			
					 		</td>
							</tr>	
											  
				  		</logic:iterate>
				  	
	
				 <bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>
</table>
</fieldset>
</td>
</tr>
</logic:notEmpty>
                      
</logic:iterate>
				<tr>
				       <td colspan="6" class="formContentview-rating-b0x">&nbsp;</td>
                       <td align="left" class="formContentview-rating-b0x"><b>Total</b></td>
                       <td class="formContentview-rating-b0x">
                       <b>
                       <%=df1.format(quantity)%>
                       </b>
                       </td>
                       <td class="formContentview-rating-b0x"><b><%=df.format(totalprice) %></b></td>
          	   </tr>
          	   	<tr>
				       <td colspan="1" class="formContentview-rating-b0x"><b>In Words</b></td>
                       <td colspan="8" class="formContentview-rating-b0x" align="right">
                       <b>   
                       <%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(totalprice)) %>
                       </b>
                       </td>
          	   </tr>
          	   

</table>
		
			  </td>
			  </tr>
			  <tr>
			  <td>
			   <table width="100%">
				<tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>
          			 
          			 <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.expecteddeliverymonth"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="expectedDeliveryMonth"/>
                       </td>
                 
                      <td rowspan="15"  >&nbsp;</td>
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.deliveriestype"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="deliveryType"/>

                      </td>    
                    </tr>
                    
                    
                    
                    
                        <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.warrantydispatchdate"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="warrantyDispatchDate"/>
                       	 <bean:message key="quotation.create.label.months"/>
                       </td>
                 
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.warrantycommissioningdate"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="warrantyCommissioningDate"/>
                        <bean:message key="quotation.create.label.months"/>
                      </td>    
                    </tr>
                    
                    
                                            <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="advancePayment"/>
                       	%
                       </td>
                 
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="paymentTerms"/>
                       %&nbsp;<bean:message key="quotation.create.label.beforedispatch" />
                      </td>    
                    </tr>
                    
                    
                    
                    
                    </table>
			  
			  </td>
			  </tr>
                         <tr>
                <td>&nbsp;</td>
              </tr>
                    <tr class="section-head" >
                    <td class="grey-dark" colspan="5"> &nbsp; </td>
                    </tr>
              
              <tr>

                <td class="blueheader" ><bean:message key="quotation.create.label.notes"/></td>
              </tr>
              <tr class="section-head" >
              <td  colspan="5"> &nbsp; </td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14)%></span></td>
              </tr>
              
              
              
              
              
              
              
              
              
              
              
              
              
             
           


              <tr>
                <td>&nbsp;</td>
              </tr>
                       			<tr class="section-head" >
                    <td class="grey-dark" colspan="5"> &nbsp; </td>
                     </tr>
              
              <tr>

                <td class="blueheader" ><bean:message key="quotation.create.label.generalnote"/></td>
              </tr>
               
               <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
              
                <td  ><bean:message key="quotation.create.label.defintions"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
             <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
             
               
               <tr>
                <td  ><bean:message key="quotation.create.label.validity"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               
              <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

               <tr>
                <td  ><bean:message key="quotation.create.label.scope"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.priceandbasis"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
             <%--  <tr>
                <td  ><bean:message key="quotation.create.label.taxessduties"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIES)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.statutoryvariation"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STATUTORY_VARIATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr> --%>
              
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.freightandinsurance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td  ><bean:message key="quotation.create.label.packingandforwarding"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
             
             
             
              
              <tr>
                <td><bean:message key="quotation.create.label.termsofpayments"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.deliveries"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
               <tr>
                <td><bean:message key="quotation.create.label.drawingsanddocsapproval"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
                <tr>
                <td><bean:message key="quotation.create.label.delayinmanufclearance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.forcemeasures"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.storages"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.warranty"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.wghtsanddimension"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.tests"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.standards"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.limitationofliability"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.consequentiallosses"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.arbitration"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.language"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

                <tr>
                <td><bean:message key="quotation.create.label.governinglaw"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.variationinquantity"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.transferoftitle"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.confidentialtreatmentsecrecy"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.passingofbenefitrisk"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.compensionduetobuyersdefault"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              <tr>
                <td><bean:message key="quotation.create.label.suspensionsandtermination"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.suspensions"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

               <tr>
                <td><bean:message key="quotation.create.label.terminations"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                 <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

				<tr>
                <td><bean:message key="quotation.create.label.bankruptcy"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              


<tr>
                <td><bean:message key="quotation.create.label.acceptance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.limitofsupply"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
                 <tr>
                <td><bean:message key="quotation.create.label.changeinscope"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              

              <tr>
                <td><bean:message key="quotation.create.label.channelsforcommunications"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.general"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              











              
                      



              <tr>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td class="blue-light" align="right">
                <input name="Button" type="submit" class="BUTTON" value="Return Enquiry" />
                <!-- Adding a View PDF Button -->
                 <logic:greaterEqual name="enquiryDto" property="statusId" value="5">
				 
				   <logic:present name="dmattachmentList" scope="request">
				  <logic:notEmpty name="dmattachmentList">
				  <input name="downloadattachment" type="button" 
				  onclick="viewAttachments();" 
				  class="BUTTON" value="Click To See Attachment Links"/>
				  </logic:notEmpty>
				  </logic:present>
			     </logic:greaterEqual>
			     <logic:greaterEqual name="enquiryDto" property="statusId" value="6">
			      <input name="viewpdf" type="button" onclick="viewPDF();" class="BUTTON" value="View PDF"/>
			      </logic:greaterEqual>
                
                <input name="Button" type="button" class="BUTTON" onclick="return emailQuoteToCustomer();"  value="Email Quote to Sales Engineer"/>
                </td>
              </tr>
            </table></td>
        </tr>
		</logic:notEmpty>
        </table>
		</logic:present>
		
		
		
		
      </html:form>    </td>
  </tr>
</table>
<div id="attachmentDiv" style="display: none;" align="center">
 
 <table width="70%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
 
<tr>
<td colspan="2">
 <fieldset>
 <legend class="blueheader"><bean:message key="quotation.attachments.dmdownloadedmessage"/></legend>

 <table >
 
<tr >

		         
           <td colspan="2" align="left">
           &nbsp;
           </td>
           <td colspan="2" align="left" style="width:100% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            
              <table width="100%" class="other">
              <tr><td colspan="2"></td></tr>
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td colspan="2">
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
       
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
    </table> 
        </fieldset>
    </td>
    </tr>
    </table>
    
    

 </div>
    
<!-------------------------- main table End  --------------------------------->
