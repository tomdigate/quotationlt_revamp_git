<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
							<tr>
								<td width="20%" class="label-text"> Shaft Type</td>
								<td width="20%" class="normal">
									<select name="shaftType" id="shaftType" title="Shaft Type" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltShaftTypeList" >
											<bean:define name="newenquiryForm" property="ltShaftTypeList" id="ltShaftTypeList" type="java.util.Collection" />
											<logic:iterate name="ltShaftTypeList" id="ltShaftType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltShaftType" property="key" />' > <bean:write name="ltShaftType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Shaft Material</td>
								<td width="20%" class="normal">
									<select name="shaftMaterialType" id="shaftMaterialType" title="Shaft Material" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltShaftMaterialTypeList" >
											<bean:define name="newenquiryForm" property="ltShaftMaterialTypeList" id="ltShaftMaterialTypeList" type="java.util.Collection" />
											<logic:iterate name="ltShaftMaterialTypeList" id="ltShaftMaterialType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltShaftMaterialType" property="key" />' > <bean:write name="ltShaftMaterialType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('shaftType', '<bean:write name="ratingObj" property="ltShaftTypeId" filter="false" />'); </script>
							<script> selectLstItemById('shaftMaterialType', '<bean:write name="ratingObj" property="ltShaftMaterialId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Paint Type</td>
								<td width="20%" class="normal">
									<select name="paintingType" id="paintingType" title="Painting Type">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltPaintingTypeList" >
											<bean:define name="newenquiryForm" property="ltPaintingTypeList" id="ltPaintingTypeList" type="java.util.Collection" />
											<logic:iterate name="ltPaintingTypeList" id="ltPaintingType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPaintingType" property="key" />' > <bean:write name="ltPaintingType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Paint Shade</td>
								<td width="20%" class="normal">
									<select name="paintShade" id="paintShade" title="Paint Shade" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltPaintShadeList" >
											<bean:define name="newenquiryForm" property="ltPaintShadeList" id="ltPaintShadeList" type="java.util.Collection" />
											<logic:iterate name="ltPaintShadeList" id="ltPaintShade" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPaintShade" property="key" />' > <bean:write name="ltPaintShade" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('paintingType', '<bean:write name="ratingObj" property="ltPaintingTypeId" filter="false" />'); </script>
							<script> selectLstItemById('paintShade', '<bean:write name="ratingObj" property="ltPaintShadeId" filter="false" />'); </script>
							<tr>
								<logic:equal name="ratingObj" property="flg_Txt_NonSTDPaint_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> Non Standard Paint Shade Value </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Txt_NonSTDPaint_MTO" value="Y">
									<td width="20%" class="label-text"> Non Standard Paint Shade Value </td>
								</logic:notEqual>
								<td width="20%" class="normal">
									<input type="text" name="paintShadeVal" id="paintShadeVal" class="normal" readonly="true" value='<bean:write name="ratingObj" property="ltPaintShadeValue" filter="false" />' title="Non Standard Paint Shade Value" >
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Paint Thickness</td>
								<td width="20%" class="normal">
									<select name="paintThickness" id="paintThickness" title="Paint Thickness">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltPaintThicknessList" >
											<bean:define name="newenquiryForm" property="ltPaintThicknessList" id="ltPaintThicknessList" type="java.util.Collection" />
											<logic:iterate name="ltPaintThicknessList" id="ltPaintThickness" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPaintThickness" property="key" />' > <bean:write name="ltPaintThickness" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('paintThickness', '<bean:write name="ratingObj" property="ltPaintThicknessId" filter="false" />'); </script>		
							<tr>
								<td width="20%" class="label-text"> Method of Coupling </td>
								<td width="20%" class="normal">
									<select name="methodOfCoupling" id="methodOfCoupling" title="Method of Coupling" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltMethodOfCouplingList" >
											<bean:define name="newenquiryForm" property="ltMethodOfCouplingList" id="ltMethodOfCouplingList" type="java.util.Collection" />
											<logic:iterate name="ltMethodOfCouplingList" id="ltMethodOfCoupling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltMethodOfCoupling" property="key" />' > <bean:write name="ltMethodOfCoupling" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Degree of Protection </td>
								<td width="20%" class="normal">
									<select name="ip" id="ip" title="IP" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltIPList" >
											<bean:define name="newenquiryForm" property="ltIPList" id="ltIPList" type="java.util.Collection" />
											<logic:iterate name="ltIPList" id="ltIP" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltIP" property="key" />' > <bean:write name="ltIP" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('methodOfCoupling', '<bean:write name="ratingObj" property="ltMethodOfCoupling" filter="false" />'); </script>
							<script> selectLstItemById('ip', '<bean:write name="ratingObj" property="ltIPId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Cable Size </td>
								<td width="20%" class="normal">
									<select name="cableSize" id="cableSize" title="Cable Size" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltCableSizeList" >
											<bean:define name="newenquiryForm" property="ltCableSizeList" id="ltCableSizeList" type="java.util.Collection" />
											<logic:iterate name="ltCableSizeList" id="ltCableSize" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltCableSize" property="key" />' > <bean:write name="ltCableSize" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> No. of Runs </td>
								<td width="20%" class="normal">
									<select name="noOfRuns" id="noOfRuns" title="No. Of Runs" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltNoOfRunsList" >
											<bean:define name="newenquiryForm" property="ltNoOfRunsList" id="ltNoOfRunsList" type="java.util.Collection" />
											<logic:iterate name="ltNoOfRunsList" id="ltNoOfRuns" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltNoOfRuns" property="key" />' > <bean:write name="ltNoOfRuns" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('cableSize', '<bean:write name="ratingObj" property="ltCableSizeId" filter="false" />'); </script>
							<script> selectLstItemById('noOfRuns', '<bean:write name="ratingObj" property="ltNoOfRuns" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> No. of Cores </td>
								<td width="20%" class="normal">
									<select name="noOfCores" id="noOfCores" title="No. Of Cores" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltNoOfCoresList" >
																	<bean:define name="newenquiryForm" property="ltNoOfCoresList" id="ltNoOfCoresList" type="java.util.Collection" />
																	<logic:iterate name="ltNoOfCoresList" id="ltNoOfCores" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltNoOfCores" property="key" />' > <bean:write name="ltNoOfCores" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Cross-Sectional Area(mm2) </td>
								<td width="20%" class="normal">
									<select name="crossSectionArea" id="crossSectionArea" title="Cross-Sectional Area (mm2)" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltCrossSectionAreaList" >
																	<bean:define name="newenquiryForm" property="ltCrossSectionAreaList" id="ltCrossSectionAreaList" type="java.util.Collection" />
																	<logic:iterate name="ltCrossSectionAreaList" id="ltCrossSectionArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltCrossSectionArea" property="key" />' > <bean:write name="ltCrossSectionArea" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
							</tr>
							<script> selectLstItemById('noOfCores', '<bean:write name="ratingObj" property="ltNoOfCores" filter="false" />'); </script>
							<script> selectLstItemById('crossSectionAre', '<bean:write name="ratingObj" property="ltCrossSectionAreaId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Material of Conductor </td>
								<td width="20%" class="normal">
									<select name="conductorMaterial" id="conductorMaterial" title="Material of Conductor" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltConductorMaterialList" >
																	<bean:define name="newenquiryForm" property="ltConductorMaterialList" id="ltConductorMaterialList" type="java.util.Collection" />
																	<logic:iterate name="ltConductorMaterialList" id="ltConductorMaterial" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltConductorMaterial" property="key" />' > <bean:write name="ltConductorMaterial" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Cable Diameter(mm) </td>
								<td width="20%" class="normal">
									<input type="text" name="cableDiameter" id="cableDiameter" class="normal" value='<bean:write name="ratingObj" property="ltCableDiameter" filter="false" />' title="Cable Diameter" >
								</td>
							</tr>
							<script> selectLstItemById('conductorMaterial', '<bean:write name="ratingObj" property="ltConductorMaterialId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Spreader Box</td>
								<td width="20%" class="normal">
									<select name="spreaderBox" id="spreaderBox" title="Spreader Box">
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Flying Lead without TB</td>
								<td width="20%" class="normal">
									<select name="flyingLeadWithoutTB" id="flyingLeadWithoutTB" title="Flying Lead Without TB" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
							</tr>
							<script> selectLstItemById('spreaderBox', '<bean:write name="ratingObj" property="ltSpreaderBoxId" filter="false" />'); </script>
							<script> selectLstItemById('flyingLeadWithoutTB', '<bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Space Heater</td>
								<td width="20%" class="normal">
									<select name="spaceHeater" id="spaceHeater" title="Space Heater" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Flying Lead with TB</td>
								<td width="20%" class="normal">
									<select name="flyingLeadWithTB" id="flyingLeadWithTB" title="Flying Lead With TB" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltFlyingLeadList" >
																	<bean:define name="newenquiryForm" property="ltFlyingLeadList" id="ltFlyingLeadList" type="java.util.Collection" />
																	<logic:iterate name="ltFlyingLeadList" id="ltFlyingLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltFlyingLead" property="key" />' > <bean:write name="ltFlyingLead" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
							</tr>
							<script> selectLstItemById('spaceHeater', '<bean:write name="ratingObj" property="ltSpaceHeaterId" filter="false" />'); </script>
							<script> selectLstItemById('flyingLeadWithTB', '<bean:write name="ratingObj" property="ltFlyingLeadWithTDId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Encoder Mounting</td>
								<td width="20%" class="normal">
									<select name="techoMounting" id="techoMounting" title="Encoder Mounting" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltTechoMountingList" >
																	<bean:define name="newenquiryForm" property="ltTechoMountingList" id="ltTechoMountingList" type="java.util.Collection" />
																	<logic:iterate name="ltTechoMountingList" id="ltTechoMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTechoMounting" property="key" />' > <bean:write name="ltTechoMounting" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Shaft Grounding</td>
								<td width="20%" class="normal">
									<select name="shaftGrounding" id="shaftGrounding" title="Shaft Grounding" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="alTargetedList" >
																	<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
															</select>
								</td>
							</tr>
							<script> selectLstItemById('techoMounting', '<bean:write name="ratingObj" property="ltTechoMounting" filter="false" />'); </script>
							<script> selectLstItemById('shaftGrounding', '<bean:write name="ratingObj" property="ltShaftGroundingId" filter="false" />'); </script>
							<tr>
								<logic:equal name="ratingObj" property="flg_Addon_TBoxSize_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> Terminal Box Size </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Addon_TBoxSize_MTO" value="Y">
									<td width="20%" class="label-text"> Terminal Box Size </td>
								</logic:notEqual>
								<td width="20%" class="normal">
									<select name="terminalBoxSize" id="terminalBoxSize" title="Terminal Box Size" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltTerminalBoxList" >
											<bean:define name="newenquiryForm" property="ltTerminalBoxList" id="ltTerminalBoxList" type="java.util.Collection" />
											<logic:iterate name="ltTerminalBoxList" id="ltTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTerminalBox" property="key" />' > <bean:write name="ltTerminalBox" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Total weight of Motor(kg) </td>
								<td width="20%" class="normal">
									<input type="text" name="motorTotalWgt" id="motorTotalWgt" class="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.kg"/>
								</td>
							</tr>
							<script> selectLstItemById('terminalBoxSize', '<bean:write name="ratingObj" property="ltTerminalBoxSizeId" filter="false" />'); </script>
							<tr>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="10%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
						</table>