<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html:form action="/viewEnquiry" method="POST" >
<html:hidden property="invoke" value="updateFactor"/>
<html:hidden property="createdBySSO" />
<html:hidden property="operationType" styleId="operationType"/> 
<html:hidden property="revisionNumber"/>
<html:hidden property="createdDate"/>
<html:hidden property="createdBy"/>
<html:hidden property="enquiryOperationType" styleId="enquiryOperationType" />
<html:hidden property="dispatch" styleId="dispatch"/>
<html:hidden property="currentRating" styleId="currentRating"/>
<html:hidden property="enquiryId" styleId="enquiryId" />
<html:hidden property="ratingId" styleId="ratingId" />
<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
<html:hidden property="nextRatingId" styleId="nextRatingId"/>
<html:hidden property="updateCreateBy" styleId="updateCreateBy" />
<html:hidden property="isDataValidated" styleId="isDataValidated"/>
<html:hidden property="isValidated" styleId="isValidated"/>
<html:hidden property="ratingsValidationMessage" styleId="ratingsValidationMessage"/>
<html:hidden property="assignToName" styleId="assignToName" />
<html:hidden property="indentOperation" styleId="indentOperation" value="SMINDENT"/>
<html:hidden property="returnEnquiry" />


<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" 
value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">



<head>
<style>
.lablebox-td{
width:33% !important;
    text-align: left;
    font-weight: normal;
    font-size: 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    padding: 2px;
    background-color: #F2F2FF;
}
select
{
 
  border: 1px solid #ccc;
  color: #555;
 
}
.background-bg-box{
background: #e0e0e0;
}
/* specific customizations for the data-list */
div.data-list-input
{
	position: relative;
	height: 20px;	
	display: inline-flex;
}
select.data-list-input
{
	position: absolute;	
	height: 20px;
	margin-bottom: 0;
}
input.data-list-input
{
	position: absolute;
	top: 0px;
	left: 0px;
	height: 19px;
}
.width-select{
width:100% !important;;
}
.input-class{
width:87% !important;
padding:4px 6px;
border-width:1px;
margin:0;
}
</style>
</head>

<bean:define id="latestId"  name="enquiryForm" property="currentRating"/>
<%!int count=0; %>
<%! int countval = 0; %>
<br>
<br>
<br>
<br>
<br>

 <table width="900"  border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#003366">
  <tr>
    <td height="100%" valign="top">     

	<br>
        <div class="sectiontitle">
        <logic:equal name="enquiryForm" property="dispatch" value="LOST">
        	<bean:message key="quotation.indentpage.label.heading"/>
        </logic:equal>
	  </div>
    <!--Comment End Here  -->

   <table width="100%" border="0" cellpadding="0" cellspacing="3">
     <tr>
       <td rowspan="2" >
       <table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
           <tr>
             <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
             <td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
            <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
           </tr>
           <tr>
             <td class="other"><table width="100%">
                 <tr>
                   <td height="16" class="section-head" align="center">
                   	<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="enquiryForm" property="statusName"/> </td>
                 </tr>
             </table></td>
           </tr>
       </table></td>
     </tr>
</table>

             
             <!--Ratings List Ending  --> 
            <fieldset>
			<div class="blue-light" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td>
				
				  &nbsp;
					</td>
					<td>
					&nbsp;
				</td>
				  </tr>
				</table>
		        	</div> <!--Quantity Ending  -->
		        	
		      
			    <br>
			   	  <div class="tablerow">
                  <div class="tablecolumn" >
              	  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
			      <tr>
			       <td >
                    	<bean:message key="quotation.create.label.enquirytype"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="enquiryType" />
                    </td>
                    </tr>	
                    <tr>
                    <td>
			        <bean:message key="quotation.create.label.customer"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="customerName" />
                    </td>
                    
                    </tr>
                    <tr>
                    <td>
			        	<bean:message key="quotation.create.label.projectname"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="projectName" />
                   	</td>
                    
                    </tr>
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.salesstage"/> 
                    </td>
                    <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="salesStage"/>
                    </td>
                    
                    </tr>
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.approval"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="approval"/>
                    </td>
                    
                    </tr> 
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.customertype"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="customerType"/>
                    </td>
                    </tr>   
                    <tr>
                    <td>
			        	<bean:message key="quotation.create.label.enduserindustry"/>
			        	
			        </td>
					<td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="endUserIndustry"/>
                    </td>
                    
                    </tr>   
                    <tr>
                 	<td><bean:message key="quotation.create.label.enduser"/></td>
					<td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="endUser"/>
                    </td>
			        </tr> 
                    <tr>
                 <td><bean:message key="quotation.create.label.orderclosingmonth"/></td>
               <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="orderClosingMonth"/>
                    </td>
                    </tr>
                    <tr>
                   <td><bean:message key="quotation.create.label.targeted" />
				  </td>
				   <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="targeted"/>
                    </td>
				  
                    
                    </tr>
                    <tr>
                    <td>
                    
                   <bean:message key="quotation.create.label.winningchance" />
				  </td>
				  <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="winningChance"/>
                    </td>

				  </tr>
				  
				   <tr>
				  <td  >
				  <bean:message key="quotation.create.label.deliveriestype" />
				  </td>
				  <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="deliveryType"/>
                    </td>
				  </tr>
				  
				  <tr>
				  <td  >
				  <bean:message key="quotation.create.label.expecteddeliverymonth" />
				  </td>
				    <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="expectedDeliveryMonth"/>
                    </td>
				  </tr>
				  
				  				  <tr>
				  <td>
				  <bean:message key="quotation.create.label.totalordervalue" />
				  </td>
				    <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="totalOrderValue"/>
                    </td>
							  </tr>
                    
                    
                    
				  </table>
				  </div><!--Left Column Ending  -->
				  <div class="tablecolumn" >
				   <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
				  <tr>
				<td>
			        	<bean:message key="quotation.create.label.lossprice"/>
			        	<span class="mandatory">&nbsp;</span>
			        </td>
			        <td >
			        
				     <html:text property="lossPrice" styleId="lossPrice" styleClass="normal" style="width:170px" maxlength="9"/>
                    </td>
					</tr>
					<tr>
					 <td>
                    	<bean:message key="quotation.create.label.lossreason1"/>
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                    <td >

				     <bean:define name="enquiryForm" property="alWinlossReasonList" id="alWinlossReasonList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="lossReason1" styleId="lossReason1">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinlossReasonList" property="key" labelProperty="value" />
                        </html:select>  
              			
                    </td>
                    
					</tr>
					<tr>
					 <td >
			        	<bean:message key="quotation.create.label.lossreason2"/>
			        	<span class="mandatory">&nbsp;</span>
			        </td>
                    <td >
				     <bean:define name="enquiryForm" property="alWinlossReasonList" id="alWinlossReasonList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="lossReason2" styleId="lossReason2" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinlossReasonList" property="key" labelProperty="value" />
                        </html:select>  

					</td>				
					
					</tr>
					<tr>
					<td >
                    	<bean:message key="quotation.create.label.losscomments"/>
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                    <td >
				     <html:text property="lossComment" styleId="lossComment" styleClass="normal" style="width:170px" maxlength="30"/> 
					</td>				
					
					</tr>
					<tr>
				   <td  ><bean:message key="quotation.create.label.losscompetitor"/>
				   <span class="mandatory">&nbsp;</span>
				    </td>
                    <td >
                   <bean:define name="enquiryForm" property="alWinCompetitorList" id="alWinCompetitorList" type="java.util.Collection" />
                    	<html:select style="width:170px;" name="enquiryForm" property="lossCompetitor" styleId="lossCompetitor">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinCompetitorList" property="key" labelProperty="value" />
                        </html:select>  

                    </td>				

					</tr>
				  </table>
				  
				  </div>			
				  </div><!--Table Row Ending  -->
			                  <!--Financial  Start-->
			                  
			
		   
		    
            
            
            
            
            

             <div class="blue-light-btn" style="width:100%" align="right">
             <input type="button" class="BUTTON" value="Back"  onclick="javascript:viewEnquiryDetails('<bean:write name="enquiryForm" property="enquiryId" />','6');" />
   	         <html:button property="" styleClass="BUTTON" value="Submit"  onclick="javascript:lostSubmitToEstimate()"/>
  
             </div>
              </fieldset>
           </td>
</tr>
</table>
                 


</html:form>
<script type="text/javascript">
$(document).ready(function () { 
	
	

	
});
jQuery(function() {
	//keep the focus on the input box so that the highlighting
  //I'm using doesn't give away the hidden select box to the user
	$('select.data-list-input').focus(function() {
		$(this).siblings('input.data-list-input').focus();
	});
  //when selecting from the select box, put the value in the input box
	$('select.data-list-input').change(function() {
		$(this).siblings('input.data-list-input').val($(this).val());
	});
  //When editing the input box, reset the select box setting to "free
  //form input". This is important to do so that you can reselect the
  //option you had selected if you want to.
	$('input.data-list-input').change(function() {
		$(this).siblings('select.data-list-input').val('');
	});
  
  
});

</script>
  
  