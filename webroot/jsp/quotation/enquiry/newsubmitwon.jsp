<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>
<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script language="javascript" src="html/js/quotation_lt.js"></script>

<html:form action="newPlaceAnEnquiry.do" method="POST">
	
	<html:hidden property="invoke" value="processWonEnquiry"/>
	<html:hidden property="operationType"/>
	<html:hidden property="enquiryId" styleId="enquiryId"/>
	<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
	<html:hidden property="statusId" styleId="statusId"/>
	<html:hidden property="ratingsValidationMessage"/>
	<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>
	<html:hidden property="dispatch" />
	
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
	
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
 				<div class="sectiontitle"><bean:message key="quotation.viewenquiry.label.heading"/> </div>	
			</td>
		</tr>
		
		<tr>
			<td height="100%" valign="top">
				<!---------------------------------		Top Title and Status & Note block : START  -------------------------------------------------------->
				<table width="100%" border="0" cellspacing="1" cellpadding="0">
					<tr>
						<td rowspan="2">
							<table width="98%" border="0" cellspacing="2" cellpadding="0" class="DotsTable">
								<tr>
									<td rowspan="2" style="width: 70px"><img src="html/images/icon_quote.gif" width="52" height="50"></td>
									<td class="other">
										<bean:message key="quotation.viewenquiry.label.subheading1" /> <bean:write name="newenquiryForm" property="enquiryNumber" /><br> 
										<bean:message key="quotation.viewenquiry.label.subheading2" />
									</td>
								</tr>
								<tr>
									<td class="other">
										<table width="100%">
											<tr>
												<td height="16" class="section-head" align="center">
													<bean:message key="quotation.viewenquiry.label.status" /> <bean:write name="newenquiryForm" property="statusName" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<logic:lessThan name="newenquiryForm" property="statusId" value="6">
						<td align="right" class="other">
							<table>
								<tr>
									<td> <bean:message key="quotation.attachments.message" /> </td>
									<td>
										<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
											<img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"> 
										</a>
									</td>
								</tr>
							</table>
						</td>
						</logic:lessThan>
					</tr>
					<tr>
						<td style="width: 45%">
						<logic:present name="attachmentList" scope="request">
							<logic:notEmpty name="attachmentList">
								<fieldset>
									<table width="100%" class="other">
										<logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<tr>
												<td>
													<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
														<bean:write name="attachmentData" property="value" />.<bean:write name="attachmentData" property="value2" />
													</a>
												</td>
											</tr>
										</logic:iterate>
									</table>
								</fieldset>
							</logic:notEmpty>
						</logic:present>
						</td>
					</tr>
					<logic:present name="attachmentMessage" scope="request">
					<tr>
						<td><bean:write name="attachmentMessage" /></td>
					</tr>
					</logic:present>
				</table>
				<!---------------------------------		Top Title and Status & Note block : END  ---------------------------------------------------------->
				
				<br> <br> <br>
				
				<!---------------------------------		Request Information and Lost Details Block : START  ----------------------------------------------->
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<!-- -- Request Information Block -- -->
								<fieldset>
									<legend class="blueheader"> <bean:message key="quotation.viewenquiry.label.request.heading" /> </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<td><label class="blue-light"style="font-weight: bold; width: 100%;"> Basic Information </label></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customerenqreference"/></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerEnqReference" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customer" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.admin.customer.location" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="location" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.concerned.person" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="concernedPerson" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Project Name</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="projectName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Consultant</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="consultantName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End user</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUser" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End User Industry</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUserIndustry" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Location of Installation</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="locationOfInstallation" /></td>
										</tr>
									</table>
								</fieldset>
							</td>
							
							<!-- -- Won Details Block -- -->
							<td valign="top">
								<fieldset>
									<legend class="blueheader"> Won Information </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Indent No.</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:text property="wonIndentNo" styleId="wonIndentNo" styleClass="normal" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">PO / LOI</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltWonPOOptionsList" id="ltWonPOOptionsList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonIsPOorLOI" styleId="wonIsPOorLOI">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="ltWonPOOptionsList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">PO No.</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:text property="wonPONo" styleId="wonPONo" styleClass="normal" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">PO Date</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:text property="wonPODate" styleId="wonPODate" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">PO Receipt Date</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:text property="wonPOReceiptDate" styleId="wonPOReceiptDate" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">PO Copy</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												&nbsp;
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Won Reason 1</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltWonReasonList" id="ltWonReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonReason1" styleId="wonReason1">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="ltWonReasonList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Won Reason 2</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltWonReasonList" id="ltWonReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonReason2" styleId="wonReason2">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="ltWonReasonList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Won Remarks</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:textarea property="wonRemarks" styleId="wonRemarks" style="width:200px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Total Quoted Price</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												&#8377;  <html:text property="totalQuotedPrice" styleId="totalQuotedPrice" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Total Won Price</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												&#8377;  <html:text property="totalWonPrice" styleId="totalWonPrice" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<!---------------------------------		Request Information and Lost Details Block : END  ------------------------------------------------->
				
				<br> <br> 
				
				<!---------------------------------		Commercial Information Related Details Block : START  --------------------------------------------->
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<fieldset>
									<legend class="blueheader"> Commercial Information </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
										<tr>
											<th class="label-text" >Payment Terms </th>
											<th class="label-text" >%age amount of Nett. order value </th>
											<th class="label-text" >Payment Days </th>
											<th class="label-text" >Payable Terms </th>
											<th class="label-text" >Instrument </th>
										</tr>
										<tr>
											<td class="formContentview-rating-b0x" >Advance Payment 1 </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PaymentDays"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PayableTerms"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1Instrument"/> </td>
										</tr>
										<tr>
											<td class="formContentview-rating-b0x" >Advance Payment 2 </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PaymentDays"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PayableTerms"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2Instrument"/> </td>
										</tr>
										<tr>
											<td class="formContentview-rating-b0x" >Main Payment </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PaymentDays"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PayableTerms"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3Instrument"/> </td>
										</tr>
										<tr>
											<td class="formContentview-rating-b0x" >Retention Payment 1 </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PaymentDays"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PayableTerms"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4Instrument"/> </td>
										</tr>
										<tr>
											<td class="formContentview-rating-b0x" >Retention Payment 2 </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PaymentDays"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PayableTerms"/> </td>
											<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5Instrument"/> </td>
										</tr>
									</table>
									
									<br>
									
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
										<tr>
											<th class="label-text" style="width:25% !important;">Delivery Term </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryTerm"/></td>
											<th class="label-text" style="width:25% !important;">Delivery </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryInFull"/> </td>
										</tr>
										<tr>
											<th class="label-text" style="width:25% !important;">Customer Requested Delivery (Weeks) </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>	
											<th class="label-text" style="width:25% !important;">Order Completion Lead Time </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="orderCompletionLeadTime"/> Weeks</td>
										</tr>
										<tr>
											<th class="label-text" style="width:25% !important;">Warranty from the date of dispatch </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyDispatchDate"/> Months</td>
											<th class="label-text" style="width:25% !important;">Warranty from the date of comissioning </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyCommissioningDate"/> Months</td>
										</tr>
										<tr>
											<th class="label-text" style="width:25% !important;">GST </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="gstValue"/> </td>
											<th class="label-text" style="width:25% !important;">Packaging </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="packaging"/> </td>
										</tr>
										<tr>
											<th class="label-text" style="width:25% !important;">Offer Validity </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
											<th class="label-text" style="width:25% !important;">Price Validity from PO date (for Drawing Approval / Mfg. Clearance) </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
										</tr>
										<tr>
											<th class="label-text" style="width:25% !important;">Liquidated Damages due to delayed deliveries </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="liquidatedDamagesDueToDeliveryDelay"/> </td>
											<th class="label-text" style="width:25% !important;"> &nbsp; </th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> &nbsp; </td>
										</tr>
									</table>
									
									<br>
									
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
										<tr>
											<th class="label-text" style="width:25% !important;">Supervision for Erection and Comissioning</th>
											<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="supervisionDetails"/> </td>
											<td >&nbsp;</td>
											<td >&nbsp;</td>
										</tr>
									</table>
									<logic:equal name="newenquiryForm" property="supervisionDetails" value="Yes">
										<table width="50%" >
											<tr>
												<td class="label-text" style="width:25% !important;">No. of Man Days</td>
												<td class="formContentview-rating-b0x" style="width:75% !important;"> <bean:write name="newenquiryForm" property="superviseNoOfManDays"/> </td>
											</tr>
											<tr>
												<th class="label-text" colspan="2">&nbsp;</th>
											</tr>
											<tr>
												<th class="label-text" style="width:100% !important; white-space:nowrap;" colspan="2"> Note: To & Fro travelling charges, Fooding, Lodging & Local conveyance at site are at customer's account. </th>
											</tr>
										</table>	
									</logic:equal>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<!---------------------------------		Commercial Information Related Details Block : END  ----------------------------------------------->
				
				<br> <br> 
				
				<!---------------------------------		Technical / Indent Information Related Details Block : START  ------------------------------------->
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<fieldset>
									<legend class="blueheader"> Technical Information </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
										<tr>
											<th class="label-text" > Approval Required on Documents </th>
											<td class="formContentview-rating-b0x" > &nbsp; </td>											
										</tr>
										<tr>
											<th class="label-text" align="right"> 1. Drawing </th>
											<td class="formContentview-rating-b0x" > 
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonRequiresDrawing" styleId="wonRequiresDrawing">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" align="right"> 2. Datasheet </th>
											<td class="formContentview-rating-b0x" > 
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonRequiresDatasheet" styleId="wonRequiresDatasheet">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" align="right"> 3. QAP </th>
											<td class="formContentview-rating-b0x" > 
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonRequiresQAP" styleId="wonRequiresQAP">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
										<tr>
											<th class="label-text" align="right"> 4. Perf. Curves </th>
											<td class="formContentview-rating-b0x" > 
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="wonRequiresPerfCurves" styleId="wonRequiresPerfCurves">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
												</html:select>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<!---------------------------------		Technical / Indent Information Related Details Block : END  --------------------------------------->
				
				<br> <br> 
				
				<!---------------------------------		Rating Details Block : START  --------------------------------------------------------------------->
				<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
				<%  int iRatingIndex = 1;  %>
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<fieldset>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<th class="formLabelTophome"> Rating Details </th>
											<th class="formLabelTophome"> Quoted Price </th>
											<th class="formLabelTophome"> Won Price </th>
										</tr>
										<logic:iterate property="newRatingsList" name="newEnquiryDto" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
										<tr>
											<td class="formContentview-rating-b0x">
												<bean:write name="ratingObj" property="ltProdLineId"/>  |
												<bean:write name="ratingObj" property="ltFrameId"/><bean:write name="ratingObj" property="ltFrameSuffixId"/>  |
												<bean:write name="ratingObj" property="ltPoleId"/> Pole  |
												<bean:write name="ratingObj" property="ltKWId"/>kW  |
												<bean:write name="ratingObj" property="ltVoltId"/>  |
												<bean:write name="ratingObj" property="ltFreqId"/>  |
												<bean:write name="ratingObj" property="ltMountingId"/>
												
												<input type="hidden" id="ratingNo<%=iRatingIndex%>"  name="ratingNo<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingNo" filter="false" />' >
												<input type="hidden" name="quantity<%=iRatingIndex%>" id="quantity<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="quantity" filter="false" />' >
											</td>
											<td class="formContentview-rating-b0x">
												&#8377; <input type="text" name="quotedPrice<%=iRatingIndex%>" id="quotedPrice<%=iRatingIndex%>" class="readonlymini" readonly="true" value='<bean:write name="ratingObj" property="ltQuotedTotalPrice" />' maxlength="15" >
											</td>
											<td class="formContentview-rating-b0x">
												&#8377; <input type="text" name="wonPrice<%=iRatingIndex%>" id="wonPrice<%=iRatingIndex%>" class="normal" value="<bean:write name="ratingObj" property="ltWonTotalPrice" />" onchange="javascript:calculateTotalWonPrice('<%=iRatingIndex%>');" maxlength="15" >
											</td>
										</tr>
										<%	iRatingIndex = iRatingIndex+1; %>
										</logic:iterate>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				</logic:notEmpty>
				<!---------------------------------		Rating Details Block : END  ----------------------------------------------------------------------->
				
				<br>
				
				<!---------------------------------		Buttons Section : START  -------------------------------------------------------------------------->
				<div class="blue-light" align="right" style="font-weight:bold; width:100% !important;">
					<html:submit value="Submit" styleClass="BUTTON" />
				</div>
				<!---------------------------------		Buttons Section : END  ---------------------------------------------------------------------------->
				
			</td>
		</tr>
		
	</table>
	
</html:form>

<script>

$("#wonPODate").datepicker({
    dateFormat: 'dd-mm-yy',
    onSelect: function (selectedDate) {
        if (this.id == 'wonPODate') {
            var arr = selectedDate.split("-");
            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var minDate = new Date(y, m + 3, d);
        }
    }
});

$("#wonPOReceiptDate").datepicker({
    dateFormat: 'dd-mm-yy',
    onSelect: function (selectedDate) {
        if (this.id == 'wonPOReceiptDate') {
            var arr = selectedDate.split("-");
            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var minDate = new Date(y, m + 3, d);
        }
    }
});


</script>