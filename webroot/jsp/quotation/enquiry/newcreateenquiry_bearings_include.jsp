<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
	<tr class="linked" style="width:1100px !important; float:left; overflow-x: scroll;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 3; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd" style="float:left;"><strong>ReLubrication System</strong></td>
						<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd" style="float:left;"><strong>ReLubrication System</strong></td>
						<% } %>
						
						<%  int idBeaRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="bearingSystem<%=idBeaRow1%>" id="bearingSystem<%=idBeaRow1%>" title="ReLubrication System" onfocus="javascript:processAutoSave('<%=idBeaRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltBearingSystemList" >
																	<bean:define name="newenquiryForm" property="ltBearingSystemList" id="ltBearingSystemList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingSystemList" id="ltBearingSystem" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingSystem" property="key" />' > <bean:write name="ltBearingSystem" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingSystem<%=idBeaRow1%>', '<bean:write name="ratingObj" property="ltBearingSystemId" filter="false" />'); </script>
						<%	idBeaRow1 = idBeaRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Bearing NDE</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Bearing NDE</strong></td>
						<% } %>
						
						<%  int idBeaRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="bearingNDE<%=idBeaRow2%>" id="bearingNDE<%=idBeaRow2%>" title="Bearing NDE" onfocus="javascript:processAutoSave('<%=idBeaRow2%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltBearingNDEList" >
																	<bean:define name="newenquiryForm" property="ltBearingNDEList" id="ltBearingNDEList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingNDEList" id="ltBearingNDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingNDE" property="key" />' > <bean:write name="ltBearingNDE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingNDE<%=idBeaRow2%>', '<bean:write name="ratingObj" property="ltBearingNDEId" filter="false" />'); </script>
						<%	idBeaRow2 = idBeaRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Bearing DE</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Bearing DE</strong></td>
						<% } %>
						
						<%  int idBeaRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="bearingDE<%=idBeaRow3%>" id="bearingDE<%=idBeaRow3%>" title="Bearing DE" onchange="javascript:processBearingDEChange('<%=idBeaRow3%>');" onfocus="javascript:processAutoSave('<%=idBeaRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newenquiryForm" property="ltBearingDEList" >
																	<bean:define name="newenquiryForm" property="ltBearingDEList" id="ltBearingDEList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingDEList" id="ltBearingDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingDE" property="key" />' > <bean:write name="ltBearingDE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingDE<%=idBeaRow3%>', '<bean:write name="ratingObj" property="ltBearingDEId" filter="false" />'); </script>
						<%	idBeaRow3 = idBeaRow3+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
	</tbody>
</table>
