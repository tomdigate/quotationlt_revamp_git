<%@ taglib uri="/struts-tiles" prefix="tiles" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page contentType = "application/vnd.ms-excel" %> 
<% 
	String sAttachmentFileName = "Request For Quotation.xls";
	String sExportType = "";
	if(request.getAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTTYPE) != null) {
		sExportType = (String)request.getAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTTYPE);
		if(QuotationConstants.QUOTATION_ADMIN_EXPORT_LISTPRICE.equals(sExportType)) {
			sAttachmentFileName = "RFQ ListPrice Export.xls";
		}
		if(QuotationConstants.QUOTATION_ADMIN_EXPORT_CUSTDISCOUNT.equals(sExportType)) {
			sAttachmentFileName = "RFQ CustomerDiscount Export.xls";
		}
	}
	
	response.setContentType("application/vnd.ms-excel"); 
	response.setHeader("Content-Disposition", "attachment;filename=" + sAttachmentFileName); 
%>
 <tiles:insert attribute="body" /> 