<%@ include file="../common/nocache.jsp" %>
<%@ taglib uri="/struts-tiles" prefix="tiles" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
		<link rel="stylesheet" href="html/css/main.css" type="text/css" />
		<link rel="stylesheet" href="html/css/menu.css" type="text/css" />
		<link rel="stylesheet" href="html/css/results.css" type="text/css" />
		<link rel="stylesheet" href="html/css/theme.css" type="text/css" />
		
		<style type="text/css">
			body {margin-left: 0px;margin-top: 0px;margin-right: 0px;margin-bottom: 0px;}
		</style>
		<script type="text/javascript">qmad=new Object();qmad.bvis="";qmad.bhide="";</script>
		<script language="javascript" src="html/js/fracas.js"></script>
		<script language="javascript" src="html/js/common.js"></script>
		<script language="javascript" src="html/js/listing.js"></script>
		<script language="javascript" src="html/js/jquery.js"></script>
		<script language="javascript" src="html/js/menu.js"></script>
		<script language="javascript" src="html/js/results.js"></script>
		<script language="javascript" src="html/js/messages.js"></script>
		<script language="javascript" src="html/js/calendar_part1.js"></script>
		<script language="javascript" src="html/js/calendar_part2.js"></script>
		<script language="javascript" src="html/js/calendar-en.js"></script>
		<script language="javascript" src="html/js/ajax.js"></script>
		<script language="javascript" src="html/js/lookup.js"></script>
		<script language="javascript" src="html/js/show_hide.js"></script>
		<script language="javascript" src="html/js/addrows.js"></script>
		<script language="javascript" src="html/js/signuptoggle.js"></script>
		<script language="javascript" src="html/js/tableH.js"></script>
		<script language="javascript" src="html/js/quotation.js"></script>
		<script language="javascript" src="html/js/quotation_lt.js"></script>
	</head>
	<body>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td valign="top"><tiles:insert attribute="header" /></td>
		  </tr>
		</table>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td valign="top"><tiles:insert attribute="menu" /></td>
		  </tr>
		</table>
		<table style="height:65%; width:100% " border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  	<td style="width:5% ">&nbsp;</td>
		    <td valign="top"><tiles:insert attribute="body" /></td>
			<td style="width:5% ">&nbsp;</td>
		  </tr>
		</table>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td valign="bottom"><tiles:insert attribute="footer" /></td>
		  </tr>
		</table>
	</body>
</html>