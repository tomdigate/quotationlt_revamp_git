<%@ include file="../common/nocache.jsp" %>
<%@ taglib uri="/struts-tiles" prefix="tiles" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
		<link rel="stylesheet" href="html/css/main.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="html/css/print.css" type="text/css" media="print" />
	</head>
	<body>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td>&nbsp;</td>
				        <td valign="top"><tiles:insert attribute="body" /></td>
				        <td >&nbsp;</td>
				 </tr>
		</table>
	</body>
</html>