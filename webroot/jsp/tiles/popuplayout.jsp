<%@ include file="../common/nocache.jsp" %>
<%@ taglib uri="/struts-tiles" prefix="tiles" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
		
		<link rel="stylesheet" href="html/css/main.css" type="text/css" />
		<link rel="stylesheet" href="html/css/menu.css" type="text/css" />
		<link rel="stylesheet" href="html/css/results.css" type="text/css" />
		<link rel="stylesheet" href="html/css/theme.css" type="text/css" />
		<style type="text/css">
			body {margin-left: 0px;margin-top: 0px;margin-right: 0px;margin-bottom: 0px;}
		</style>
		
		<script language="javascript" src="html/js/compliance.js"></script>
		<script language="javascript" src="html/js/complianceadmin.js"></script>
		<script type="text/javascript" src="html/js/common.js"></script>
		<script type="text/javascript" src="html/js/listing.js"></script>
		<script type="text/javascript" src="html/js/results.js"></script>
		<script type="text/javascript" src="html/js/messages.js"></script>
		<script type="text/javascript" src="html/js/calendar_part1.js"></script>
		<script type="text/javascript" src="html/js/calendar_part2.js"></script>
		<script type="text/javascript" src="html/js/calendar-en.js"></script>
		<script type="text/javascript" src="html/js/ajax.js"></script>
		<script language="javascript" src="html/js/lookup.js"></script>
		<script language="javascript" src="html/js/show_hide.js"></script>
	</head>
	<body>
		<tiles:insert attribute="header" />
				    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td width="36" class="bg-left">&nbsp;</td>
				        <td valign="top"><tiles:insert attribute="body" /></td>
				        <td width="44" class="bg-rt">&nbsp;</td>
				      </tr>
				    </table>
		<tiles:insert attribute="footer" />
	</body>
</html>