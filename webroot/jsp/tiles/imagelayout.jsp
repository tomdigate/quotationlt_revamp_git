<%@ include file="../common/nocache.jsp" %>
<%@ taglib uri="/struts-tiles" prefix="tiles" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
		<link rel="stylesheet" href="html/css/main.css" type="text/css" >
		<link rel="stylesheet" href="html/css/menu.css" type="text/css" >
		<link rel="stylesheet" href="html/css/results.css" type="text/css" >
		<link rel="stylesheet" href="html/css/theme.css" type="text/css" >
		
		
		<script language="javascript" src="html/js/compliance.js"></script>
		<script language="javascript" src="html/js/complianceadmin.js"></script>
		<script language="javascript" src="html/js/common.js"></script>
		<script language="javascript" src="html/js/listing.js"></script>
		<script language="javascript" src="html/js/menu.js"></script>
		<script language="javascript" src="html/js/results.js"></script>
		<script language="javascript" src="html/js/messages.js"></script>
		<script language="javascript" src="html/js/calendar_part1.js"></script>
		<script language="javascript" src="html/js/calendar_part2.js"></script>
		<script language="javascript" src="html/js/calendar-en.js"></script>
		<script language="javascript" src="html/js/ajax.js"></script>
		<script language="javascript" src="html/js/lookup.js"></script>
		<script language="javascript" src="html/js/show_hide.js"></script>
		<script language="javascript" src="html/js/signuptoggle.js"></script>
		<script language="javascript" src="html/js/tableH.js"></script>
	</head>
	<body>
		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td height="100%" valign="top">
			    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">				
			      <tr>
			        <td width="36" >&nbsp;</td>
			        <td valign="top"><tiles:insert attribute="body" /></td>
			        <td width="44">&nbsp;</td>
			      </tr>
			    </table>
		    </td>
		  </tr>		 
		</table>
	</body>
</html>